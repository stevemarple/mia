# Multi-instrument Analysis toolbox for Matlab

MIA is a toolbox for Matlab to support data processing, analysis and
visualisation of Solar-Terrestrial Physics data such as from
magnetometers, riometers and all-sky cameras. It also incorporates
knowledge of other key instruments, such as the EISCAT and SuperDARN
radars, so that their locations can be plotted on maps alongside the
supported MIA datasets and instruments.



# Authors

MIA was written by Steve Marple with contributions from Martin Grill
and Andrew Senior.

