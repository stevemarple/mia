rem Install program for windows version of MIA
@echo off

if "%MIA_INSTALLDIR%"=="" set MIA_INSTALLDIR=%HOMEDRIVE%\MIA

mkdir "%MIA_INSTALLDIR%"
mkdir "%MIA_INSTALLDIR%\win32"
mkdir "%MIA_INSTALLDIR%\mia_snapshots"
mkdir "%MIA_INSTALLDIR%\data"
mkdir "%MIA_INSTALLDIR%\summary"

rem copy executables
copy win32 "%MIA_INSTALLDIR%\win32"

rem install the latest MIA snapshot
"%MIA_INSTALLDIR%\win32\update_mia_snapshot" -v -v
