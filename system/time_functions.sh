# -*-sh-*-

# A set of time functions to handle time/data parsing in a common way
#
# Requirements:
# Bourne-compatible shell 
# date
# expr


# valid input: "yesterday", "today", "tomorrow", YYYY-mm-dd,
# "yesterday +10 days", "today +10 days", "tomorrow -5 days", 
# 'YYYY-mm-dd -5 days'
# output: seconds since 1970-01-01 00:00:00+00
func_parse_date() {
    FLOOR=0
    OPTIND=1
    while getopts "f" option; do
	case $option in 
	    f)
		FLOOR=1
		;;
	    *)
		echo "unknown option in func_parse_date"
		exit 1;
		;;
	esac
    done
    shift $(($OPTIND - 1))

    T=`date --utc -d "$1" '+%s'`
    if [ "$?" != 0 ]; then
	echo "bad time format: '$1'"
	exit 1
    fi

    if [ "$FLOOR" = 1 ]; then
	T=`expr $T '-' '(' $T '%' 86400 ')'`
    fi
    echo $T
}


# param 1: format string, see date(1)
# param 2: seconds since 1970-01-01
func_strftime() {
    date --utc --date="1970-01-01 +$2 seconds" "+$1"
}
