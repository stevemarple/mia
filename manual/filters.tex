\chapter{Filters}
\section{Introduction}
It is frequently necessary to process data with some kind of filter
method. For some data sets the filtering may be very similar to
analogue filtering methods, such as band-pass filtering magnetometer
data to detect Pi2 oscillations. In MIA filters are more generic,
anything which processes a data object and returns an object of the
same type with the same resolution, start and end times can be
considered a filter.

There are a number of places within MIA where it is useful to apply
filters. However, each filter method takes different parameters. If
MIA used the traditional functional methods then every function which
used filtering would need to be aware of all the filter methods, and
how to call each one. Such an approach is not scalable as the addition
of one new filter method would require many functions to be
updated. Although users could create their own filter methods they
would have to create customised versions of all functions which used
filters.

MIA overcomes this problem by using an object-oriented technique,
which in C++ is called \emph{functors}. In MIA different filter
methods are implemented as classes derived from a base class
(\filename{mia_filter_base}). Each class has its own filter
method. Objects of a filter class should be created with their correct
parameters (or configured later using the \filename{configure} method
in \filename{mia_filter_base}). As the object stores the parameters
required to filter data the \filename{filter} method takes only two
parameters, the filter object and the data object.

The filter parameters should be defined generically. This means for a
sliding mean filter its \filename{windowsize} should be defined as a
temporal resolution (not a particular number of samples) so that the
behaviour is controlled by the filter object, not the resolution of
the data. It is acceptable for the filter to modify its behaviour
based upon the data, for example, some filters require an odd number
of samples. If the filter is not able to process the data it should
return it unchanged. The filter should clearly indicate what
processing has been performed by adding a new processing comment (see
\filename{help mia_base/addprocessing}).

The functor approach allows any function to filter data by simply
calling the \filename{filter} method. When using GUI functions it is
still necessary to know which filters are available, a list is
maintained within the \filename{mia_filters} function. If users wish
to be able to select their filter methods within the GUI they should
create a customised version of \filename{mia_filters}. 

\helpbox{%
  When users or adminstrators create their own customised versions
  they should probably call the original version of
  \filename{mia_filters} and add to that list. See section
  \ref{section:calling-specific-function} for information on how to do
  this.
}

\section{Requirements for Filter Classes}

All filter classes should provide the following functions (as these
are member functions the first parameter is the filter object):
\begin{description}
  \item[char] Output basic information in a readable form

  \item[getname] Return the name of the filter in a readable form. An optional parameter indicates how the string should be formatted, see the gettype description more information.

  \item[validdata] When given a list of class names (or an object of
    the class) indicate if the data can be filtered by the class. For
    the convenience of class authors classes which can filter all data
    types may symbolically link \filename{validdata.m} to
    \filename{@mia_filter_base/validata_all.m}
\end{description}

If the filter contains optional parameters then it should be possible
to configure them using the GUI. Callback functions do not work well
as member functions, so the class should provide a function called
\filename{<CLASS_NAME>_cfg} located outside of the class directory. If
the function does not exist \filename{mia_filter_base} will provide a
dummy \filename{configure} function indicating that there are no
configuration options for the filter.

If necessary the following functions may be overridden if the versions
in \filename{mia_filter_base} are not appropriate (as these are member
functions the first parameter is the filter object):

\begin{description}
\item[getresidue] Return a $1 \times 2$ vector indicating how many
  samples will not be filtered at the start and end of the data.
\end{description}

\section{Despike Classes}

Similar to the filter functors are the methods for removing spikes
from data. \filename{mia_despike_base} is a base class for all despike
classes, and provides a common \filename{filter} method. The despike
classes are expected to provide a function called
\filename{findspikes}, which should return the locations of all spikes
in the data. \filename{mia_despike_base/filter} will use this
information to remove the spikes by either interpolating or setting
the spikes to \filename{NaN}. As the despike methods are also filters
they should also follow conform to the standards for MIA filters.

