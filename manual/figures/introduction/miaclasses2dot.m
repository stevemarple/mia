function r = miaclasses2dot(filename, varargin)

options.type = '';
[options unvi] = interceptprop(varargin, options);
% Don't warn about ignored parameters because they are passed on mat2dot


if isempty(options.type)
  options.type = 'all';
end

% Only look in MIA-generated paths
p = split(':', path);
p2 = {};

% check those paths which are subdirectories of mia_basedir 
idx_mbd = strmatch(mia_basedir, p);

% now do the same but for mia_personaldir
idx_mpd = strmatch(mia_personaldir, p);

p2 = {p{unique([idx_mbd; idx_mpd])}};

if strcmp(options.type, 'gui')
  % For GUI classes restrict search based on path alone
  p2 = p2{strmatch(fullfile(mia_basedir, 'gui'), p2)};
elseif strcmp(options.type, 'gui')
  % And for misc always exclude GUI
  p2{strmatch(fullfile(mia_basedir, 'gui'), p2)} = {};
end

c = getclasses('path', p2);
miac = {};


% Only keep selected classes
for n = 1:numel(c)
  try
    obj = feval(c{n});
    ok = true;

    switch options.type
     case {'all', 'gui'}
      % Accept all (gui already filtered based on path)
      miac{end+1} = c{n};
      
     case 'data'
      if isa(obj, 'mia_base')
        miac{end+1} = c{n};
      end

     case 'filter'
      if isa(obj, 'mia_filter_base')
        miac{end+1} = c{n};
      end

     case 'instrument'
      if isa(obj, 'mia_instrument_base')
        miac{end+1} = c{n};
      end
      
     case 'misc'
      % Anything which isn't data, instrument or filter classes (GUI
      % classes already excluded by modified search path)
      if ~isa(obj, 'mia_base') & ~isa(obj, 'mia_filter_base') ...
            & ~isa(obj, 'mia_instrument_base')
        miac{end+1} = c{n};
      end
     
     otherwise
      error(sprintf('unknown type (was ''%s'')', options.type));
    end      

  catch
    % do nothing
  end
  
end
close all

switch options.type
 case 'all'
  title = 'All MIA classes';
  
 case 'data'
  title = 'MIA data classes';
  
 case 'filter'
  title = 'MIA filter classes';
  
 case 'gui'
  title = 'MIA GUI classes';
  
 case 'instrument'
  title = 'MIA instrument classes';
  
 case 'misc'
  title = 'MIA miscellaneous classes';
  
 otherwise
  error(sprintf('unknown type (was ''%s'')', options.type));
end

r = mat2dot(filename, ...
            'classes', miac, ...
            'title', title, ...
            varargin{unvi});

