function r = mat2dot(filename, varargin)

options.graphname = '';
options.showmembers = 1;
options.showbaseclasses = 0;
options.showfunctions = 0;

% options for classes to include on graph
options.classes = {};
options.excludedirs = {}; % ignore some directories in search

options.showbaseclasses = logical(options.showbaseclasses);

% display options in DOT
options.title = [];
options.concentrate = '';

options.connector_attrs = '[arrowhead = "none" arrowtail = "none"]';

[options unvi] = interceptprop(varargin, options);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(options.title) & isnumeric(options.title)
  options.title = 'Class Hierarchy';
  if logical(options.showfunctions)
    options.title = [options.title ' (functions)'];
  elseif logical(options.showmembers)
    options.title = [options.title ' (members)'];
  end
end


if isempty(options.graphname)
  options.graphname = options.title;
end
if isempty(options.classes)
  % only do if necessary (can be slow)
  options.classes = getclasses('excludedirs', options.excludedirs);
end


[fid message] = fopen(filename, 'w');
if fid == -1
  error(sprintf('cannot open %s for writing: %s', filename, message));
end


nodes = {}; % lines of nodes to output
conns = {};

classInfo = [];
classNum = 0;

a = options.classes;
% process input list
for n = 1:length(a)
  haveObj = 1;
  if strcmp(class(a{n}), 'char');
    cls = a{n};
    % try making object of class
    cmd = sprintf('obj = feval(''%s'');', cls);
    eval(cmd, 'obj=[]; haveObj = 0;');
  else
    obj = a{n};
    cls = class(a{n});
  end
  if haveObj
    ance = getancestors(obj);
  else
    ance = {cls};
  end
  if 0
    disp(' ');
    disp(cls);
    disp(sprintf('%s ', ance{:}));
  end
  
  % some classes don't have default constructors (or return handles
  % instead of the real object) so it is possible the user has passed
  % actual objects because s/he has the knowledge to create them. If that
  % is the case then preserve that object
  if haveObj
    if isfield(classInfo, cls)
      tmp = getfield(classInfo, cls);
    else
      tmp = [];
    end
    tmp.object = obj;
    classInfo = setfield(classInfo, cls, tmp);
  end
  
  % insert the ancestor information
  for m = 1:length(ance)
    if isfield(classInfo, ance{m})
      tmp = getfield(classInfo, ance{m});
    else
      tmp = [];
    end
    tmp.ancestors = {ance{m+1:end}};
    
    classInfo = setfield(classInfo, ance{m}, tmp);
  end
end

classNames = fieldnames(classInfo);
% if any field hasn't had an object of it created (possbily for parent
% classes) try making one.
for n = 1:length(classNames)
  cls = classNames{n};
  tmp = getfield(classInfo, cls);

  if ~isfield(tmp, 'object');
    haveObj = 1;
    cmd = sprintf('obj = feval(''%s'');', cls);
    eval(cmd, 'obj=[]; haveObj = 0;');
    if haveObj
      tmp.object = obj;
      classInfo = setfield(classInfo, cls, tmp);
    end
  end
end


for n = 1:length(classNames)
  % get details of the data members
  cls = classNames{n};
  tmp = getfield(classInfo, cls);

  classNum = classNum + 1;
  tmp.classnumber = classNum;

  tmp.members = {};
  tmp.parents = {};
  if isfield(tmp, 'object')
    str = struct(getfield(tmp, 'object'));
    fn = fieldnames(str);
    for m = 1:length(fn)
      if ~isa(tmp.object, fn{m})
	tmp.members{end+1} = fn{m};
      else
	tmp.parents{end+1} = fn{m};
      end
      
    end
  else
    % cannot ascertain. Since matlab insists on each class having at least
    % one field member then leaving datamembers as an empty matrix is
    % unambigous 
    ;
  end
  
  % get details of member functions
  funcs = what(['@' cls]);
  tmp.functions = {};
  for m = 1:length(funcs)
    % copy m-files, but remove ".m", etc
    ext = {'m' 'mat' 'mex' 'mdl' 'p'};
    for k = 1:length(ext)
      tmp2 = getfield(funcs(m), ext{k});
      ext2 = ext{k};
      if strcmp(ext2, 'mex')
	ext2 = mexext;
      end
      for j = 1:length(tmp2)
	tmp.functions{end+1} = tmp2{j}(1:(end-length(ext2)-1));
      end
    end
    tmp.functions = unique(tmp.functions);
  end
  classInfo = setfield(classInfo, cls, tmp);
end

for n = 1:length(classNames)
  cls = classNames{n};
  ci = getfield(classInfo, cls);

  nodeDesc = ['shape=record,label="{<f0> ' cls];
  di = 0; % dot identifier
  if options.showfunctions
    for k = 1:length(ci.functions)
      di = di + 1;
      nodeDesc = sprintf('%s|<f%d> %s%s', nodeDesc, di, ci.functions{k});
    end
  else
    if options.showmembers
      for k = 1:length(ci.members)
	di = di + 1;
	nodeDesc = sprintf('%s|<f%d> %s%s', nodeDesc, di, ci.members{k});
      end
    end
    if options.showbaseclasses
      for k = 1:length(ci.members)
	di = di + 1;
	nodeDesc = sprintf('%s|<f%d> %s%s', nodeDesc, di, ci.parents{k});
      end
    end
  end
  nodeDesc = [nodeDesc '}"'];
  
  nodes{ci.classnumber} = sprintf('%s [%s];', cls, nodeDesc);
  if ~isempty(ci.ancestors)
    % conns{end+1} = sprintf('%s -> %s', ance{m}, ance{m+1});
    conns{end+1} = sprintf('%s -> %s', ci.ancestors{1}, cls);
    if ~isempty(options.connector_attrs)
      conns{end} = [conns{end} ' ' options.connector_attrs];
    end
  end
  
  
end



fprintf(fid, 'digraph "%s" {\n', options.graphname);
if ~isempty(options.title)
  fprintf(fid, 'label="%s"\n', options.title);
end
if ~isempty(options.concentrate)
  conc = {'false', 'true'}';
  fprintf(fid, 'concentrate="%s"\n', conc{1 + logical(options.concentrate)});
end

fprintf(fid, 'node [shape=record];\nedge [dir=back];\n');

for n = 1:classNum
  fprintf(fid, '    %s\n', nodes{n});
end
for n = 1:length(conns)
  fprintf(fid, '    %s\n', conns{n});
end

fprintf(fid, '}\n');


fclose(fid);


r = classInfo;
