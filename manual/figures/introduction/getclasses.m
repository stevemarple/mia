function r = getclasses(varargin)

options.path = path;
options.excludedirs = {};

[options unvi] = interceptprop(varargin, options);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~iscell(options.path)
  p2 = options.path;
  colons = [0 find(p2 == ':') length(p2)+1]; % pretend colons at both ends
  num = length(colons) - 1;
  options.path = cell(num, 1);
  for n = 1:num
    options.path{n} = p2([(colons(n)+1):(colons(n+1)-1)]);
  end
end

r = {};
for n = 1:length(options.path)
  % look for .m files and mex files
  d = dir(options.path{n});
  
  if ~isempty(options.excludedirs)
    % does directory match any of the exclude directories?
    if iscell(options.excludedirs)
      for h = 1:length(options.excludedirs)
	tmp = strmatch(options.excludedirs{h}, options.path{n});
	if ~isempty(tmp)
	  break;
	end
      end
    else
      tmp = strmatch(options.excludedirs, options.path{n});
    end
  else
    tmp = [];
  end
  
  if isempty(tmp)
    for m = 1:length(d)
      if d(m).isdir & d(m).name(1) == '@'
	% class directory, look for constructor
	cls = d(m).name(2:end);
	d2 = [dir(fullfile(options.path{n}, d(m).name, [cls '.m'])); ...
	      dir(fullfile(options.path{n}, d(m).name, [cls '.' mexext]))];
	for k = 1:length(d2)
	  if ~d2(k).isdir & d2(k).bytes
	    r{end+1} = cls;
	    break;
	  end
	end
      end
    end
  end
end

r = unique(r);
