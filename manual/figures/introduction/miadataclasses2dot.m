function r = miadataclasses2dot(filename, varargin)

c = getclasses;
miac = {};

% Only keep selected classes
for n = 1:numel(c)
  try
    disp(c{n});
    obj = feval(c{n});
    ok = true;
    
    if isa(obj, 'mia_base')
      miac{end+1} = c{n};
    end

  catch
    % do nothing
  end
  
    
end
close all

r = mat2dot(filename, ...
            'classes', miac, ...
            'title', 'MIA data classes', ...
            varargin{:});

