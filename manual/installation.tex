%% Define some URLs

%$ \urldef{\miaGitUrl}{\url}{git@bitbucket.org:stevemarple/mia.git}
\newcommand{\miaGitUrl}{git@bitbucket.org:stevemarple/mia.git}

\chapter{Installing MIA}

\section{Linux installation instructions}

\subsection{Requirements}
The first stage of the installation is to ensure the target computer
has the required software installed.
\begin{description}
\item[Computer running Linux or Unix operating system] Any version of
  Linux is sufficient provided the other requirements are met. Other
  Unix operatating systems are also compatible (\eg, Solaris).
\item[X11 windows] Normally installed with desktop versions of Linux,
  but may be  missing from server versions. A physical display is not
  needed as \filename{vncserver} can be used.
\item[Matlab] version 5.3 or later.
\item[Bash shell] Normally installed by default on Linux systems.
\item[make] Makefile processor for controlling compilation.
\item[C compiler] \filename{gcc} or similar.
\item[C++ compiler] \filename{g++} or similar.
\item[\filename{git}] Version 2.0.0 or higher.
\item[\filename{curl}] Required for the \filename{url_tools} toolbox.
\item[\filename{gzip}, \filename{gunzip} and \filename{zcat}] Required to
  compress\slash uncompress \filename{.gzip} files, (\eg, for SAMNET data).
\item[\filename{wget}] Required for certain
  operations. 
\item[ImageMagick] Image processing package containing
  \filename{animate}, \filename{convert}, \etc.
\end{description}

\helpbox{%
  Installation on Windows computers is possible but \textbf{not}
  supported and some features are not available.}

\subsubsection{Recommended software}
The following software is not required but it highly recommended.
\begin{description}
\item[CDF] libraries, used for loading data in CDF format. Not
  required unless you wish to use the \filename{cdf_tools}. In order
  for the \filename{cdf_tools} to be compiled you must ensure that
  \envVar{CDF_INC} and \envVar{CDF_LIB} environment variables are
  set. For more information about CDF see \url{http://cdf.gsfc.nasa.gov/}.
\item[\filename{gifsicle}] for animated GIF movies.
\item[VNC] used for automatic creation of summary plots \etc
\item[\filename{zip}] used in creation of GAIA summary plots.
\end{description}

\subsubsection{Ubuntu or Debian linux}
The following commands should ensure all of the necessary requirements
are installed:
\begin{LinuxCmd}
  sudo apt-get install make\myreturn
  sudo apt-get install curl\myreturn
  sudo apt-get install imagemagick\myreturn
  sudo apt-get install git\myreturn
  sudo apt-get install gifsicle\myreturn
  sudo apt-get install zip\myreturn
\end{LinuxCmd}

\subsection{Matlab installation}

It is recommended that the Matlab installation(s) be made available as
\filename{matlab-<VERSIONNUMBER>}, \eg, \filename{matlab-6.5},
\filename{matlab-7.3}. This is easily achieved by creating the
appropriate symbolic links in \filename{/usr/local/bin}.

The following Matlab toolboxes should be installed. These are freely
available.
\begin{description}
\item[\filename{ephem}] Ephemeris toolbox.
\item[\filename{m_map}] Toolbox for mapping data; be sure to also
  install the \filename{gshhs} map databases.
\end{description}

\helpbox{%
  \filename{m_map} requires the location of the map databases to be
  set in \filename{m_tbase.m} by configuring the \filename{PATHNAME}
  setting.
}

% \subsection{Default file locations}

% \begin{description}
% \item[\filename{/usr/local/bin}] Location of the \filename{mia}
%   startup script and its associated \filename{mia.config} file.
% \item[\filename{/data}] Base directory for 
% \item[\filename{/summary}] Location where summary plots are written
% \end{description}



\subsection{MIA installation}

\helpbox{%
  Previous versions of MIA used \filename{subversion} for version
  control and allowed multiple versions (snapshots) to be
  installed. MIA now uses \filename{git} for version control, and the
  installation directory is a \filename{git} repository; snapshots are
  no longer supported.
}

\subsubsection{Clone the git repository}

Cloning the \filename{git} repository will create a directory called
\filename{mia}. Change into the directory in which the \filename{mia}
directory should be created.
\begin{LinuxCmd}
  git clone \miaGitUrl
\end{LinuxCmd}

\subsubsection{Create required symbolic links}

Generate symbolic links so that base class functions (\eg,
\filename{@mia_base/plot.m}) can be called on derived objects. MIA
will not function correctly if these symbolic links are missing. On
Windows systems the \filename{mia_makelinks} command will make exact
copies of the files instead.
\begin{LinuxCmd}
  ./mia/system/mia -o "-nojvm -r mia_makelinks(true)"
\end{LinuxCmd}

\subsubsection{Compile helper programs}
Some functions are implemented in native code for efficiency reasons,
such as the sliding median filter.
\begin{LinuxCmd}
  cd mia/matlab
  make install
\end{LinuxCmd}


\subsubsection{Create data and summary plot directories}
Create the data and summary plot directories, as \filename{root}
\begin{LinuxCmd}
  mkdir /data\myreturn
  mkdir /summary\myreturn
\end{LinuxCmd}

\helpbox{%
  You must change the access permissions of \filename{/data} and
  \filename{/summary} to suit your local requirements. 

  \item It is recommended that all users have read access to the filename{/data} and
  \filename{/summary} directories, but write access is limited to a
  special data processing account (or group).
}

\subsubsection{Configure MIA}

If you have used different file locations from the default then you
must adjust create a configuration file for all users (use
\filename{/etc/mia.config}) or a single-user configuration file (use
\filename{\$HOME/.mia}). The file(s) should set the script environment
variables that require adjustment. For more information see section
\ref{section:environment-variables}.


\section{Windows installation instructions}

There are no specific instructions for installing MIA on Windows as I
am not able to test on this platform. Please use the Linux
instructions as a guide and adjust to suit. Windows 10 users may find
the \emph{Windows Subsystem for Linux} (WSL) useful.

\section{Updating MIA}

MIA and the accompanying startup and data transfer scripts can be
updated by changing into the root of the local MIA installation
directory and running the commands
\begin{LinuxCmd}
  git pull --rebase
  ./system/mia -o "-nojvm -r mia_makelinks(true)"
  cd matlab
  make install
\end{LinuxCmd}

\warningbox{%
  Older installations which were created from the installation CD or
  used the subversion installation method cannot be updated with this
  method, it is necessary to reinstall MIA following the intructions above.

  \filename{git} installations can be identified by the presence of a
  hidden \filename{.git} directory located in the root of the
  installation directory.%
}
\section{Uninstalling MIA}

To remove MIA simply remove the installation directory and all files
inside it. You may also wish to uninstall \filename{git}.

\section{Environment variables}
\label{section:environment-variables}
The MIA shell script and the data transfer scripts read and\slash or
set several environment variables which modify their default action.

\begin{description}
\item[\envVar{DISPLAY}] The display used by X11 programs, including
  Matlab. This should not normally be set by the user, however the MIA
  startup script may set this variable, such as when the \filename{-d}
  option has been passed to MIA. See also
  page~\pageref{page:mia-with-vnc}. Not used in Windows installation
  of MIA.

\item[\envVar{MIA_BASEDIR}] The base directory where the MIA files are
  located.

\item[\envVar{MIA_MATLABVERSION}] The Matlab version which should be
  started. The version is appended to the string \filename{matlab-} to
  form the executable name.

\item[\envVar{MIA_MATLAB}] The name of the Matlab executable,
  including any version part. 

\item[\envVar{MIA_MATLABOPTIONS}] Any options which should be passed
  to Matlab. Normally this is set to \filename{-nojvm} to disable the
  Java virtual machine (for performance reasons).

\item[\envVar{MIA_LOCALDIR}] Location of the local configuration
  directory for MIA. This must be set in order for the local
  customisations file, \filename{startup.m}, to be read.

\item[\envVar{MIA_PERSONALDIR}] The location of the user's personal
  MIA directory. On Linux\slash Unix systems this is usually
  \envVar{HOME/MIA} and should not normally be changed.

\item[\envVar{MIA_USER}] The owner of the MIA installation. Only this
  user has permission to update the MIA installation.

\item[\envVar{TMP}] The location of the temporary directory. If not
  set it defaults to \filename{/tmp}. MIA will create a temporary
  directory inside \envVar{TMP}, of the form \envVar{TMP/mia.<PID>}
  where \filename{<PID>} is the process ID. This ensures that
  different instances of MIA do not share the same temporary directory
  and eliminates problems where two instances of Matlab would
  otherwise access the same temporary file. All programs called by MIA
  inherit the modified version of \envVar{TMP}.

\item[\envVar{MIA_DATADIR}] The base directory where the data is
  located. Defaults to \filename{/data} if not set.

\item[\envVar{MIA_SUMMARYPLOTDIR}] The base directory where the
  summary plots are saved. Defaults to \filename{/summary} if not set.
  
\item[\envVar{MIA_TIME_LIB}] The location of the
  \filename{time_functions.sh} library used by the data transfer
  scripts. If \filename{time_functions.sh} is not located in the
  standard location (filename{/usr/local/lib}) then
  \envVar{MIA_TIME_LIB} \textbf{must} be set in order for the data
  transfer scripts to load the time functions library.
\end{description}
