\chapter{Riometer data formats}

\section{IRIS riometer format}

The IRIS riometer format, as created by the University of Maryland is
based around \bit{8} or packed \bit{12} data samples stored in blocks of
4096 bytes, with a binary header.

\tablefirsthead{%
  \hline
  \multicolumn{1}{|c|}{\textbf{Offset}} &
  \multicolumn{1}{c|}{\textbf{Meaning}} \\ \hline
}
\tablehead{%
  \hline
  \multicolumn{2}{|l|}{\textsl{\small{continued from previous page}}}\\
  \hline
  \multicolumn{1}{|c|}{\textbf{Offset}} &
  \multicolumn{1}{c|}{\textbf{Meaning}} \\ \hline
}
\tabletail{%
  \hline
  \multicolumn{2}{|r|}{\textsl{\small{continued on next page}}}\\ \hline
}
\tablelasttail{\hline}
\bottomcaption{IRIS file format}
\begin{supertabular}{|d|l|}
  0 & TYR, tens digit of year \\
  1 & UYR, units digit of year\\
  2 & HDY, hundreds digit of day\\
  3 & TDY, tens digit of day\\
  4 & UDY, units digit of day\\
  5 & THR, tens digit of hour (UT)\\
  6 & UHR, units digit of hour\\
  7 & TMN, tens digit of minute\\
  8 & UMN, units digit of minute\\
  9 & TSC, tens digit of second\\
  10 & USC, units digit of second\\
  11 & MSC, milliseconds\\
  12 & Vault temperature\\
  13 & +5v monitor\\
  14 & +12v monitor\\
  15 & -12v monitor\\
  16 & Receiver step 1\\
  17 & Receiver step 2\\
  18 & Receiver step 3\\
  19 & Receiver step 4\\
  20 & Receiver step 5\\
  21 & Receiver step 6\\
  22 & Receiver step 7\\
  23 & Receiver step 8\\
  24 & Station number\\
  25 & Steps per second\\
  26 & Spare receiver switch setting\\
  27 & Noise generator flag\\
  28 & System error message number 1\\
  29 & System error message number 2\\
  30 & System error message number 3\\
  31 & System error message number 4\\
  32 & GPS/IRIG-B time quality byte\\
  33 & ADC resolution (8 = 8-bits; 12 = 12-bits)\\
  34 & Status of step-0 recording (0 = not recording step 0 data)\\
  35 & Logical record size, bytes (one complete scan, data only)\\
  36-39 & unused\\
  40-63 & Program version/revision date (24 bytes total) \\ \hline
\end{supertabular}

System error messages are limited to 44 bytes (45 including
terminating NULL). Error messages are described below:

\tablefirsthead{%
  \hline
  \multicolumn{1}{|c|}{\textbf{Number}} &
  \multicolumn{1}{c|}{\textbf{Error message}} \\ \hline
}
\tablehead{%
  \hline
  \multicolumn{2}{|l|}{\textsl{\small{continued from previous page}}}\\
  \hline
  \multicolumn{1}{|c|}{\textbf{Number}} &
  \multicolumn{1}{c|}{\textbf{Error message}} \\ \hline
}
\tabletail{%
  \hline
  \multicolumn{2}{|r|}{\textsl{\small{continued on next page}}}\\ \hline
}
\tablelasttail{\hline}
\bottomcaption{IRIS error messages}
\begin{supertabular}{|d|l|}
0 &  No error (blank string)\\
1 & System clock has FAILED\\
2 & No response from antenna CPU\\
3 & Write timeout on COM1 serial port\\
4 & Read timeout on COM2 serial port\\
5 & Write timeout on COM2 serial port\\
6 & Noise generator is ON\\
7 & Riometer n is NOT working\\
8 & System is in READ mode\\
9 & System is in STOP mode\\
10 & Keyboard is LOCKED\\
11 & Keyboard is UNLOCKED\\
12 & Operating system error from MS-DOS\\
13 & Input buffer OVERFLOW on COM1\\
14 & Input buffer OVERFLOW on COM2\\
15 & 1/8th second marker was MISSED\\
16 & ILLEGAL keyboard scan code received\\
17 & Input buffer OVERFLOW from the keyboard\\
18 & Error on reading disk\\
19 & Error on writing disk\\
20 & UNKNOWN error number\\
21 & Error reading parameter file\\
22 & Error opening directory: \filename{D:\A_00_000}\\
23 & Error opening directory: \filename{D:\A_00_000}\\
24 & Error opening file: \filename{F_00_000.000}\\
25 & Error opening file: \filename{F_00_000.000}\\
26 & Error writing file: \filename{F_00_000.000}\\
27 & Error writing file: \filename{F_00_000.000}\\
28 & GPS/IRIG-B clock board not responding! \\ \hline
\end{supertabular}

Following from the header is a 4032 byte data block, making each
record 4096 bytes in length. The first sample is from row 1 column 1,
the second sample from row 1 column 1 and so on, and may include the
output from the spare riometer in the final column. Note that in
\bit{12} recording mode two samples are written into 3 bytes.

\subsection{Bugs}
In some early versions (prior to c.\ 1995) the first 12 data bytes are
corrupt. This affects the first row of riometers (in \bit{12}
recording mode). The workaround is to replace the affected values by
the valid data from the second set of samples in the data block.

It appears that in some cases the ADC resolution is written out twice,
causing the logical record size to be written into offset 36 instead
of offset 35. Therefore if the logical record size is identical to the
ADC resolution use the value of offset 36 as the real logical record
size.

\section{NIPR riometer data format}

The `NIPR' riometer data format is used by the National Institute of
Polar Research, Japan (NIPR), and also by Polar Research Institute of
China (PRIC) and the Danish Meteorological Institute (DMI), for
riometers at DMI, HUS, LYR, NAL, SJO, TJO, and ZHS.

The 16 byte header is followed by one or more frames (normally 8), and
each beam data is expressed by a 2 byte binary value (little-endian
format). The header format is:

\tablefirsthead{%
  \hline
  \multicolumn{1}{|c|}{\textbf{Offset}} &
  \multicolumn{1}{c|}{\textbf{Meaning}} \\ \hline
}
\tablehead{%
  \hline
  \multicolumn{2}{|l|}{\textsl{\small{continued from previous page}}}\\
  \hline
  \multicolumn{1}{|c|}{\textbf{Offset}} &
  \multicolumn{1}{c|}{\textbf{Meaning}} \\ \hline
}
\tabletail{%
  \hline
  \multicolumn{2}{|r|}{\textsl{\small{continued on next page}}}\\ \hline
}
\tablelasttail{\hline}
\bottomcaption{NIPR file format}
\begin{supertabular}{|d|l|}
0 &  2 digit year\\
1 & Month\\
2 & Day of month\\
3 & Hour\\
4 & Minute\\
5 & Second\\
6,7 & Sampling interval\\
8 & Unused\\
9 & Unused\\
10 & Unused\\
11 & Unused\\
12 & Unused\\
13 & Unused\\
14 & Unused\\
15 & Number of frames \\ \hline
\end{supertabular}

It is summised from the data files that offset 15 indicates how many
frames are stored in the record, where the number of frames is 0--7
inclusive, and that zero means 8 frames.

Each frame is written out as:

\begin{tabular}{*{8}{c}}
  N1E1 & N2E1 & N3E1 & N4E1 & N5E1 & N6E1 & N7E1 & N8E1\\
  N1E2 & N2E2 & N3E2 & N4E2 & N5E2 & N6E2 & N7E2 & N8E2\\
  N1E3 & N2E3 & N3E3 & N4E3 & N5E3 & N6E3 & N7E3 & N8E3\\
  N1E4 & N2E4 & N3E4 & N4E4 & N5E4 & N6E4 & N7E4 & N8E4\\
  N1E5 & N2E5 & N3E5 & N4E5 & N5E5 & N6E5 & N7E5 & N8E5\\
  N1E6 & N2E6 & N3E6 & N4E6 & N5E6 & N6E6 & N7E6 & N8E6\\
  N1E7 & N2E7 & N3E7 & N4E7 & N5E7 & N6E7 & N7E7 & N8E7\\
  N1E8 & N2E8 & N3E8 & N4E8 & N5E8 & N6E8 & N7E8 & N8E8\\
\end{tabular}

\section{ARCOM file format}

For details of the ARCOM file format please see the Imaging Riometer
Operations and Data Processing Manual for the appropriate riometer,
\citet{manual-and-2, manual-mai-2, manual-nal-2}.


