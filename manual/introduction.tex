\chapter{Introduction}

\section{About this document}

This document describes the multi-instrument analysis toolbox (MIA)
for processing and visualisation of solar-terrestrial physics
data. This includes, but is not limited to, riometer datasets (IRIS,
AIRIS, ARIES, SGO and CANOPUS), magnetometer datasets (SAMNET, IMAGE)
and optical datasets (DASI, DASI2).

For an introduction to MIA the reader is directed to \citet{art:619}.

\section{Conventions used in this document}

Hexadecimal numbers are printed with a leading ``0x'', following the
standard convention used in the C programing language. Computer code,
and filenames are printed in a Courier font \code{like this}. When a
list of commands to type at the prompt is given they
will be be formatted as below:
\begin{LinuxCmd}
  cd {\mytilde}rioadmin/ARCOM\myreturn
  ls\myreturn
\end{LinuxCmd}
where the \myreturn symbol is used to indicate that the \code{Return}
key should be pressed. Other keypresses (\eg, ``q'') are indicated as
\keypress{q}\hspace{-.5ex}. The Linux logo (\linuxLogo) is used to
indicate Linux-specific commands, the Windows logo (\windowsLogo) is
used to indicate Windows-specific commands, \eg:
\begin{WindowsCmd}
  cd \cMiaWin\myreturn
  dir\myreturn
\end{WindowsCmd}

References to Unix help pages are given as \filename{man crontab},
which is the command to type at the Unix prompt (in this case for the
\filename{crontab} command). Reference to Matlab help pages are given
as \filename{help rio_abs}, which is the command to type at the Matlab
prompt (for \filename{rio_abs} in this case.

Environment variables are prefixed with a \envVar{} sign, just as
they appear in the Unix shell.

Menu options and buttons from graphical users interfaces are shown
quoted \myquote{like this}.

Warning boxes are formatted as below:\\
\warningbox{A warning box!}

Help boxes containing helpful hints are formatted as:\\
\helpbox{A useful hint.}

% Riometer systems are described as \nByN, that is having an antenna
% array comprised of \nByN\ crossed dipoles (it does not refer to the
% arrangement of beams).

This PDF documents contains attachments. Within the document they are
identified as:\\

\noattachfile

They can also listed in the attachments pane when this PDF file is
viewed in Adobe Acrobat. Other PDF viewers may not support attachments
or may not display the attachment icons.

\helpbox{%
  The attachments use Unix end-of-line markers. To view or edit the
  files on Windows computers you may find
  \href{http://notepad-plus.sourceforge.net/}{Notepad++} helpful.}

\section{An overview of MIA}

The Multi-Instrument Analysis (MIA) program is a sophisticated
analysis package for Matlab. The package aims to provide a common
environment for processing data from a variety of instruments used in
solar-terrestrial physics. MIA currently supports the following
instrument types: 
\begin{itemize}
\item riometers and imaging riometers
\item magnetometers
\item all-sky cameras.
\end{itemize}
MIA also contains preliminary details of other instruments (e.g.,
radars). 

MIA aims to provide:
\begin{itemize}
\item data visualisation, with a common look and feel
\item data processing tools using a common API, which can be called
  from Matlab scripts and functions, thereby allowing large datasets
  to be processed automatically
\item a graphical user interface (GUI)
\item mapping facilities (using the freely available \filename{m_map}
  toolbox) to display both the data and also the instrument locations.
\end{itemize}

\subsection{Toolboxes}

MIA is separated into a number of smaller toolboxes. The toolboxes are
subdirectories located below \envVar{MIA_BASEDIR}. Each toolbox is
listed described below, along with brief descriptions of any important
classes.

\subsubsection{General MIA tools (\protect\filename{mia})}

Functions and classes common to all toolboxes are placed in this
location. In Matlab class directories are prefixed with a
\filename{@}. Important subdirectories are:
\begin{description}
\item[\filename{@location}] A class to describe locations.
  
\item[\filename{@mia_base}] The base class for all MIA data
  classes. This ensures all data objects contain information
  relating to their start and end times, temporal resolution,
  creator, creation time etc.
\item[\filename{@mia_image_base}] A base class for all MIA data
  classes which contain image data. 
  
\item[\filename{@mia_instrument_base}] The base class from which all
  instrument classes are derived. This directory defines functions
  such as map which allow instruments to be plotted onto
  geographical maps (using the free \filename{m_map} toolbox).
\end{description}

\subsubsection{Time classes (\protect\filename{time})}

Virtually all calculations involving dates and time are performed by
two time classes, \filename{timestamp} and \filename{timespan}. This
approach ensures that all details concerning day, month and year
boundaries are handled by common code. \filename{timestamp} is used
for dates (anything which contains a year) while \filename{timespan}
is used for intervals. The time classes allow all sensible operations,
some operations are not permitted, such as the addition of two
\filename{timestamp} objects (the addition of 1st January 1970 to 3rd
February 1901 is not a meaningful operation).

\warningbox{%
  Times in MIA are defined with the start time \textbf{inclusive} and
  the end time \textbf{exclusive}. This arrangement means that the end
  time does not vary if the resolution of the dataset is changed. The
  duration is simply the start time subtracted from the end time, and
  the number of samples is given by dividing the duration by the
  temporal resolution.  }

\subsubsection{Riometer toolbox (\protect\filename{riometer})}

This directory contains functions for processing and visualisation of
riometer and imaging riometer data. There are two main data types:
\emph{beam data} and \emph{image data}. Riometer instruments are
described by a class called \filename{riometer}. The riometer data
classes are:
\begin{description}
\item[\filename{@rio_base}] Base class for all riometer beam data classes.

\item[\filename{@rio_qdc_base}] Base class for all riometer QDC
  classes. Note that the QDC classes always store data as a whole
  sidereal day, starting from sidereal midnight. This enables
  substitution of QDCs from different periods.

\item[\filename{@rio_rawpower}] Data class for unlinearised riometer
  power data, typically used to store the data as the original ADC
  samples.

\item[\filename{@rio_power}] Data class for linearised riometer power
  data. The linearised power may still be in ADC units, or may be
  stored in dBm. 

\item[\filename{@rio_rawqdc}] A QDC created from raw power values, and
  must be linearised before use. The advantage of creating QDCs in
  this way is that the linearisation function can be improved at a
  later date without requiring the QDCs to be recalculated. 

\item[\filename{@rio_qdc}] A QDC created from linearised power
  values. The advantage of creating QDCs with linearised power is that
  functions such as mean operate on a linear scale. 

\item[\filename{@rio_abs}] A class to hold absorption data.

\item[\filename{@rio_image}] A data class derived from
  \filename{@mia_image_base} to hold images of riometer data. Unlike
  the irregular pattern of the riometer beam locations the
  \filename{@rio_image} class stores data on a regular grid, which
  allows keograms, image plots and movies to be generated. The images
  can be generated from any of the beam data formats:
  \filename{@rio_rawpower}, \filename{@rio_power},
  \filename{@rio_rawqdc}, \filename{@rio_qdc} and \filename{@rio_abs}.
\end{description}


\subsubsection{Magnetometer toolbox (\protect\filename{magnetometer})}

This directory contains functions for processing and visualisation of
magnetometer data. Magnetometer instruments are described by a class
called \filename{magnetometer}. The magnetometer data classes are:
\begin{description}
  \item[\filename{@mag_data}] Data class for magnetometer data. Also
    functions as a base class for other magnetometer classes. 

\item[\filename{@mag_qdc}] Data class for magnetometer quiet-day
  curves. It is not required that the object contains an entire day of
  data (though this is normally the case). 

\item[\filename{@mag_qdc_fft}] A data class for magnetometer quiet-day
  curves where the curve is stored on disk as a set of FFT
  coefficients, allowing a significant data compression. The FFT is
  also used to smooth the QDC and to ensure that it is cyclic.
\end{description}

\subsection{Creating and loading data}

Data is created by calling the class constructor (a function with the
same name as the class, but without the \filename{@} prefix). The
class constructor is responsible for loading the data from file(s),
joining data from multiple files if necessary, changing the temporal
resolution etc. The method and location where data is stored on disk
should be transparent to the user.


\section{MIA class diagrams}
Figure~\ref{fig:mia-data-classes} shows the hierarchy of the various
data classes in MIA. Figure~\ref{fig:mia-instrument-classes} shows the
hierarchy of the various instrument classes in
MIA. Figure~\ref{fig:mia-filter-classes} shows the hierarchy of the
various filter classes in MIA.
\begin{landscape}

\begin{figure}[p]
  \centering
  \includegraphics[width=20cm]{%
    figures/introduction/mia-data}
  \caption{MIA data classes.}
  \label{fig:mia-data-classes}
\end{figure}

\begin{figure}[p]
  \centering
  \includegraphics[width=20cm]%
  {figures/introduction/mia-instrument}
  \caption{MIA instrument classes.}
  \label{fig:mia-instrument-classes}
\end{figure}

\begin{figure}[p]
  \centering
  \includegraphics[width=20cm]{figures/introduction/mia-filter}
  \caption{MIA filter classes.}
  \label{fig:mia-filter-classes}
\end{figure}

\end{landscape}

\section{Getting help}

All of the scripts associated with MIA accept the options
\filename{-?} and \filename{-h}, either of which respond by printing
out some basic help information about the command-line parameters
accepted.

Within Matlab help information about both Matlab's own
functions and those provided by MIA can be obtained by typing
\filename{help} followed by the name of the function. Note that MIA
uses classes and some functions are overloaded so it may be necessary
to prefix the function name with \filename{CLASSNAME/}, where
CLASSNAME should be replaced by the appropriate class. At the end of
the help information Matlab will indicate if there are overloaded
methods, and what they are.

Help on Unix commands can be obtained from the Unix man pages, which
should be accessed with the \filename{man} command.
