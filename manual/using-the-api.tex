\chapter{Using the MIA API }
\label{chapter:using-the-api}

\warningbox{%
  Users should not attempt to load files directly, use the appropriate
  function instead.}

Although MIA provides a GUI for data processing
(chapter~\ref{chapter:using-the-gui}) advanced usage requires the
flexibility and additional options provided by the API. MIA should be
considered as a framework which enables complex data processing, not
as a graphical processing tool.


\section{Getting help}

The Matlab commands \code{help} and \code{lookfor} can be used to
obtain help information in the normal way. Note that MIA is
object-oriented and therefore there may be multiple functions of the
same name, you may need to prefix the function name with the name of
the appropriate class, for example:
\begin{Code}
  help mia_base/save
\end{Code}

\section{API example}

This example will show how to load riometer absorption data and then
find the difference between each sample to locate spike events. First
define some basic parameters:
\begin{Code}
  st = timestamp([2009 01 30 20 0 0]);
  et = timestamp([2009 01 30 22 0 0]);
  res = timespan(10, 's');
  rio = rio_nal_2;
  beams = [3 7 11]; % 3 different beams
\end{Code}

Then create the absorption object:
\begin{Code}
  mia = rio_abs('starttime', st, ...
                'endtime', et, ...
                'resolution', res, ...
                'beams', beams, ...
                'instrument', rio);
\end{Code}

Note how we can set only the parameters we need. We could have changed
the obliquity method, or used a non-standard QDC, if we had set the
appropriate parameters, but the default ones are suitable. MIA, like
Matlab, frequently uses \emph{parameter name/value pairs} when passing
parameters to a function. Although this technique is slightly slower
it does allow for additional functionality to be added at a later data
without breaking existing code. It also means that default parameters
to a function can be defined in one place (inside the function).

\helpbox{%
  If you need to implement a function which takes parameter name/value
  pairs you may wish to use the \code{interceptprop} function. The
  \code{makeprop} function is sometimes useful when calling such
  functions. The \code{warnignoredparameters} function may also be
  useful.}

The \code{mia} object contains all sorts of information. We can
extract the information we want by calling functions. We don't know
what row the data for beam 7 is
stored in, so we have to ask:
\begin{Code}
  ri = getparameterindex(mia, 7); % find row index
  data7 = getdata(mia, ri, ':');
\end{Code}

To get the absorption values for beam 7:
\begin{Code}
  data7 = getdata(mia, ri, ':');  
\end{Code}

We want samples for all times, but since we are calling a function,
not subscripting an array we cannot use the colon operator (:), we
have to pass the colon as a string, ':'. To get the entire data matrix
we could just have called:
\begin{Code}
  alldata = getdata(mia);
\end{Code}

Back to the problem of detecting spike events, the difference between
successive samples is easily calculated using Matlab's own \code{diff}
function:
\begin{Code}
  diffdata = diff(data7);
\end{Code}
