% 25 hourly values
t = timestamp('2001-01-01'):timespan(1, 'h'):timestamp('2001-01-02'); 

y = rand(1, 25) * 10;  % 25 random values, in range 0 to 10

% this calls timetick2 if the plot isn't already labelled on the X axis
plot(t, y); 
