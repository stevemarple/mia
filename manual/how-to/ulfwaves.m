% Define some common start/end times
st = timestamp([2007 1 17 0 0 0]);
et = timestamp([2007 1 17 12 0 0]);

% Create a filter object which will filter with the short period cutoff
% of 40s (250mHz) and the long period cutoff of 400s (2.5mHz)
fil = mia_filter_samnet('short', timespan(40, 's'), ...
			'long', timespan(400, 's'));

% Load riometer data from IRIS at Kilpisjarvi
mia = rio_abs('instrument', rio_kil_1, ...
	      'starttime', st, ...
	      'endtime', et, ...
	      'beams', 25);

% Filter the riometer data
miafil = filter(fil, mia)

% Load magnetometer data from SAMNET's Kilpisjarvi magnetometer. There are
% two archives for SAMNET data, 5s which starts in 1987 and 1s which starts
% in 1997. The archive must be specified.
md = mag_data('instrument', mag_kil_1, ...
	      'starttime', st, ...
	      'endtime', et, ...
	      'loadoptions', {'archive', '1s'});

% Filter the magnetometer data
magfil = filter(fil, md)	      

% Load some more magnetometer data, this time from the IMAGE magnetometer
% in Kilpisjarvi. (It is actually from the same instrument as the SAMNET
% one, but uses XYZ coordinates instead of HDZ, and the temporal
% resolution is 10s).
md2 = mag_data('instrument', mag_kil_2, ...
	       'starttime', st, ...
	       'endtime', et);

% Filter the other magnetometer data
magfil2 = filter(fil, md2)	      


% Now plot the before and after data sets
plot(mia);
plot(miafil);

plot(md);
plot(mdfil);

plot(md2);
plot(mdfil2);
