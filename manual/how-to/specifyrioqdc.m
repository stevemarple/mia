% qdc is some riometer QDC you have already loaded using rio_qdc
mia = rio_abs('instrument', rio, ...
              'starttime', st, ...
              'endtime', et, ...
              'beams', beams, ...
              'qdc', qdc);
