% 25 hourly values
t = timestamp('2001-01-01'):timespan(1, 'h'):timestamp('2001-01-02'); 
x = getcdfepochvalues(t);

y = rand(1, 25) * 10;  % 25 random values, in range 0 to 10

plot(x, y); % this calls the standard Matlab plot function
timetick2;
