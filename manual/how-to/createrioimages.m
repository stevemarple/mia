% using default pixel values, mapped to geographic degrees
id = rio_image(mia, 'pixelunits', 'deg'); 

% using default pixel values, mapped to geomagnetic coordinates using
% altitude-adjusted CGM (AACGM). This requires the geocgm function found in
% the cgm toolbox (not supplied with MIA).
id = rio_image(mia, 'pixelunits', 'aacgm'); 

