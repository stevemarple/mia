% define magnetometer, could be an array, eg [mag_yor_1 mag_bor_1]
% IMAGE magnetometer data can also be loaded in this way
mag = mag_yor_1; 
st = timestamp([2006 1 1 0 0 0]); % define start time 
et = timestamp([2006 1 3 0 0 0]); % define end time

mia = mag_data('instrument', mag, ...
               'starttime', st, ...
               'endtime', et);

