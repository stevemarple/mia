st = timestamp('2000-01-01'); % start time
et = timestamp('2004-01-01'); % end time
dur = timespan(1, 'd'); % duration by which loop should increment
t = st; % 
while t < et
    % t is the beginning of one interval
    t2 = t + dur; % the end of the current interval

    % code to do processing goes here ...

    t = t2; % the next period starts at the end of the current one
end

