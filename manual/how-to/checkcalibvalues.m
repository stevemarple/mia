% Load data, keeping calibration values intact
st = timestamp('2009-07-16');
et = st + timespan(1, 'd');

% No need to check all beams, just a few
beams = [18 25 32];
mia = rio_power_calib('instrument', rio_and_2, ...
		      'starttime', st, ...
		      'endtime', et, ...
		      'beams', beams);

for b = beams
  plot(mia, 'beams', b);
end

