st = timestamp([2009 03 02 0 0 0]);
et = timestamp('2009-03-16');
rio = rio_and_2;

% Load data from the ARCOM files, ignoring any .mat files.
% Ensure that the class is rio_rawpower_calib.

mia = rio_rawpower_calib('instrument', rio, ...
                         'starttime', st, ...
                         'endtime', et, ...
                         'loadoptions', ...
                         {'archive', 'original_format', ...
                         'failiffilemissing', false});
save cal_data
plot(mia);
[c t] = getcalibrationblockdata(mia);
plot(t, c);

% Calculate the median value (rounded to an integer). 
% This is the value you should use
m = round(median(c))

% Plot calibration values and the median value
plot(t([1 end]), [m m], 'g');
