\chapter{Customising MIA}

\section{Introduction}

Matlab uses a search path to determine which instance of a function to
execute. The MIA start-up script includes user and
installation-specific directories at the start of the search path,
thereby allowing both user and installation-level customisation of
MIA (section \ref{section:local-and-user-custimisation}).

It is also necessary for customisation to occur based on a given
instrument. This has been acheived by careful design of the instrument
\filename{info} functions which enables users to add their own
instrument definitions, and if necessary provide additional or
alternative information about each instrument. Thus, by design, there
is not a central repository responsible for assigning instrument
definitions, a decision taken to ease adoption of MIA by other
institutions. Instrument-level customisation is described in section
\ref{section:instrument-customisation}.

\section{Local and user customisations}
\label{section:local-and-user-custimisation}

System administrators and users should not modify standard MIA
functions as this will make upgrading more difficult (and in the case
of shared installations may cause problems for other users). If the
system administrator wishes to alter the behaviour of MIA then he/she
should place the new or modified files in the \filename{local}
directory. Ensure that class members functions (those in a directory
beginning with \filename{@}) are placed in a directory of the same
name within \filename{local}. Similarly, users can modify the standard
behaviour for themselves by placing the appropriate files in their
 \filename{MIA/custom} directory.

\subsection{Customised startup files}

When Matlab starts if a file called \filename{startup.m} exists in the
current directory that function or script is executed. MIA uses this
behaviour to setup the Matlab search path. Part of the standard MIA
startup procedure is to test if \envVar{MIA_LOCALDIR/startup.m}
exists. If so the function or script is executed. If a file called
\envVar{MIA_PERSONALDIR/startup.m} exists then that function or script
is executed after any local \filename{startup.m} file.

\subsection{Changing the default logo}


System administrators who wish to alter the default logo should do so
by overriding the \filename{defaultmakeplotfiglogo} function,
installing the new version into the \filename{local} directory. Users
may do the same but installing the new
\filename{defaultmakeplotfiglogo} function into their custom
directory.  Functions which generate plots call \filename{makeplotfig}
to create a standard figure decorated with a logo. All plot functions
should accept a \filename{logo} parameter to alter the logo used on
that plot. When creating a group of plots it is convenient to be able
to alter the default logo on a temporary basis. This may be done by
calling \filename{makeplotfiglogo} with the name of the new logo
function (or a handle to that function). \filename{makeplotfiglogo}
provides methods for reverting back to the previous or default logo,
as well as setting no logo; see \filename{help makeplotfiglogo} for
more details.


\section{Instrument customisations}
\label{section:instrument-customisation}

It is possible to modify the information returned by the instrument
\filename{info} functions, or even add support for extra information
requests. These customisations can be performed per instrument, for
all instruments within a facility or for all instruments. The later
option in particular may be of interest to system administrators
wishing to convert URLs to filenames for loading data. For more
information see section \ref{section:info-functions-implementation}.

To customise the location or method used to load data for a given
instrument it is necessary to create your own \filename{info_local.m}
file, which is saved either into the users own custom directory
(typically \filename{~/MIA/custom}) or to the \filename{local}
customisation directory to affect all users. An example file of modifying
the location from which data is loaded is given below.

\IncludeMatlabFile{example-info-local.m}{Example customisation file.}

\section{Calling a specific member function}
\label{section:calling-specific-function}
Matlab no longer allows users to \filename{feval} a specific function
by including a partial path. This is a problem when multiple versions
of the same name exist; this case can arise as a result of
\begin{itemize}
\item a derived class overloading the behaviour of its ancestor
  classes or
\item a customised version of the function exists.
\end{itemize}

The trick to resolving this problem is to create a unique name for the
function. This can be done with the use of symbolic links; when this
is necessary MIA creates a smybolic file by prepending the class name
and an underscore (\filename{_}) to the function name. The target of
the link is the original file.

\warningbox{%
  Microsoft Windows does not allow standard users to create soft links
  so copies of the original file are created instead. Do not edit these
  files since the upgrade process will create new copies, be sure to
  make use of the custom directories for your modifications.
}
