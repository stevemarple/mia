\chapter{How to \ldots}

Below is a selection of useful code excerpts for various
administrative or scientific purposes. You may need to modify the code
to suit your purposes. The Matlab files are attached to this PDF
document after each example.

\section{General how to \ldots}

\subsection{How to process every day of data}

Start defining the start and end times of the period you wish to loop
over. Then use a while loop.
\IncludeMatlabFile{how-to/processeveryday.m}{Source code for processeveryday.m}

\helpbox{%
  If this method is used for riometer absorption data then the QDC
  will be reloaded every day, see section
  \ref{section:process-riometer-data-without-reloading-qdc} for a
  revised method which is more efficient.}

\subsection{How to label plots with time}

Method 1: use the \filename{timestamp/plot} function (easiest)
\IncludeMatlabFile{how-to/labelplotswithtime1.m}{Source code for labelplotswithtime1.m}

Method 2: use \filename{getcdfepochvalue} to plot the X values as milliseconds since 0000-01-01, then call \filename{timetick2}.
\IncludeMatlabFile{how-to/labelplotswithtime2.m}{Source code for labelplotswithtime2.m}


\subsection{How to ignore missing files when loading data}
In the \filename{loadoptions} parameter include 
\filename{'failiffilemissing', 0}. 
\IncludeMatlabFile{how-to/ignoremissingfiles.m}{Source code for ignoremissingfiles.m}

\subsection{How to ignore nans when changing the resolution}
If you are changing the resolution of some data already loaded then
add an extra parameter
\IncludeMatlabFile{how-to/ignorenanswhenchangingres1.m}{Source code for ignorenanswhenchangingres1.m}

If the resolution is being changed whilst the data is loaded then
specify the \filename{resolutionmethod} option in the
\filename{loadoptions} parameter.
\IncludeMatlabFile{how-to/ignorenanswhenchangingres2.m}{Source code for ignorenanswhenchangingres2.m}

\helpbox{%
  Various methods to set the resolution are possible, including
  \filename{mean} (the default), \filename{nonanmean},
  \filename{median}, and \filename{nonanmedian}. If data is being
  interpolated to a higher temporal resolution then interpolation
  options to \filename{interp1} can given instead.}

\subsection{Filter for ULF waves}

This example makes use of the \filename{mia_filter_samnet}. It is so
named as it follows the filter design originally implemented by
University of York when they operated SAMNET. The filter can be
applied to any type of data including riometer and magnetometers.
\IncludeMatlabFile{how-to/ulfwaves.m}{Source code for ulfwaves.m}

\subsection{How to plot a keogram at some other value of latitude}
Assuming the variable \filename{mia} contains image data.
\IncludeMatlabFile{how-to/createkeogram.m}{Source code for createkeogram.m}

where \filename{x} is a value contained in the \filename{xpixelpos}
property (use \filename{getpixels} to get the details of which X and Y
pixels are contained in the image) and similar for \filename{y}. It is
necessary to provide both values (or relevant range of values).  

\section{Riometer how to \ldots}

\subsection{How to specify which riometer QDC to use}
\label{section:how-to-specify-riometer-qdc}
Pass the QDC object as the \filename{qdc} option to the
\filename{rio_abs} constructor call.
\IncludeMatlabFile{how-to/specifyrioqdc.m}{Source code for specifyrioqdc.m}


\subsection{How to create absorption data using the mean of two QDCs}
Load the QDCs separately. Then use 
\IncludeMatlabFile{how-to/meanoftwoqdcs.m}{Source code for meanoftwoqdcs.m}

Then load absorption data using the specified QDC, see section
\ref{section:how-to-specify-riometer-qdc}.


\subsection{How to process riometer absorption data without reloading the QDC every time}
\label{section:process-riometer-data-without-reloading-qdc}
MIA normally creates riometer absorption data on demand, this method
allows alternative QDCs to be used and for QDCs to be revised more
easily. For optimum performance when processing large datasets you
will need to avoid loading the QDC unnecessarily. The
\filename{rio_abs_with_qdc} function provides a method to do this.
\IncludeMatlabFile{how-to/processriodatawithoutreloadingqdc.m}{Source code for processriodatawithoutreloadingqdc.m}

\subsection{How to create absorption images}
Assuming the variable \filename{mia} contains absorption data (\ie,
it's class is \filename{rio_abs}).
\IncludeMatlabFile{how-to/createrioimages.m}{Source code for createrioimages.m}

\helpbox{%
You can also create images of raw power, linearised power and quiet
day curves in the same way, but starting with a different input
class.} 

\subsection{Find the strength of the calibration signal}

For correct operation the strength of the calibration signal must be
known. Below is some code which will load the data directly from the
ARCOM files and allow it to be plotted.
\IncludeMatlabFile{how-to/calibsig.m}{Source code for calibsig.m}

\subsection{Plot data with calibration values}

It is sometimes useful to plot data and the calibration signal, \eg,
to check that the calibration signal is still functioning correctly.
\IncludeMatlabFile{how-to/checkcalibvalues.m}{Source code for
  checkcalibvalues.m}

\subsection{How to read data directly from ARCOM files}

For normal data processing riometer data is read from Matlab files as
reading ARCOM files is much slower. However it is sometimes useful or
necessary to read the ARCOM files directly (\eg, to create the Matlab
files).
\IncludeMatlabFile{how-to/loadfromarcomfiles.m}{Source code for
  loadfromarcomfiles.m}

\helpbox{%
  This \emph{how to} is specific to riometers which record ARCOM data,
  but the same technique of setting \filename{archive} to
  \filename{original_format} applies. However, the
  \filename{loadoptions} will need to be specific to that instrument.}

\section{Magnetometers how to \ldots}

\subsection{How to load magnetometer data}

\IncludeMatlabFile{how-to/loadmagnetometerdata.m}{Source code for loadmagnetometerdata.m}

\subsection{How to specify which archive SAMNET magnetometer data is loaded from}

SAMNET data is available from several archives:
\begin{description}
\item[\filename{1s}] The 1 second archive (\units{1}{s} temporal
  resolution).
\item[\filename{5s}] The 5 second archive (\units{5}{s} temporal
  resolution).
\item[\filename{preliminary}] The preliminary archive (\units{1}{s} temporal
  resolution).
\item[\filename{incoming}] The incoming archive which is not validated
  (\units{1}{s} temporal resolution).
\item[\filename{realtime2}] The new realtime archive which is not validated
  (\units{1}{s} temporal resolution).
\end{description}
Select the archive(s) you wish to use.
\IncludeMatlabFile{how-to/specifysamnetarchive.m}{Source code for specifysamnetarchive.m}

\helpbox{%
  You can also pass a cell array for the archive which will load data
  from any or all of the indicated archives, see \filename{help
    mia_base/loaddata} for more information. }
