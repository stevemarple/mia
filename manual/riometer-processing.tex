\chapter{Converting riometer data files to MIA format}

\section{Introduction}

The riometer data files are optimised for the recording process. It is
normally necessary to convert them to Matlab files which are optimised
for the data processing. Problems found with the original format files
are:
\begin{itemize}
\item They may not be aligned to the computer time. Frequently the
  data files store the GPS time but the computer clock is not set to
  GPS time and so the samples from one hour (or day) may actually be
  stored in the incorrect file.
\item The riometer data format may consist of records which can cross
  hour or day boundaries, thus data for a given time may be found in
  the subsequent file from where it might be expected.
\end{itemize}

After converting the data files to Matlab format data is stored in the
correct file, and in a format which can be easily read.

\helpbox{%
  \label{helpbox:start-end-date-formats}
  As with MIA, the data transfer and processing scripts described
  below define start time\slash date as \textbf{inclusive} ($>=$) and
  end time\slash date and \textbf{exclusive} ($<$).
  
  \item The start and end dates can take several formats:
    \begin{itemize}
    \item \filename{2001-10-31}, an ISO format date
    \item \filename{yesterday}
    \item \filename{today}
    \item \filename{tomorrow}
    \item \filename{yesterday +10 days}
    \item \filename{today +1 day}
    \item \filename{tomorrow -5 days}
    \end{itemize}
}

\section{Transferring data}
Included with MIA are some scripts for transferring riometer data.

\subsection{Transferring ARCOM data files}
ARCOM data files are best transferred with the \filename{arcom_rsync}
script. A feature of ARCOM is that it files are in an open or
finalised state. When ARCOM has finished using an open file it is
closed and the \filename{_o.arcom} suffix is renamed to a
\filename{_f.arcom} suffix. The file renaming does not affect the
transfer of older data but for the current day it causes problems with
naive attempts to transfer data. The \filename{arcom_rsync} script is
able to work around this issue. Data for a given day can be
transferred with the command
\begin{Cmd}
  arcom_rsync -r RIO -s STARTDATE\myreturn
\end{Cmd}

Data for a range of dates day can be transferred with the command
\begin{Cmd}
  arcom_rsync -r RIO -s STARTDATE -e ENDDATE\myreturn
\end{Cmd}
Where \filename{RIO} is the MIA code for the riometer (\eg,
\filename{rio_mai_2}), STARTDATE and ENDDATE are the start and end
dates respectively, remembering that end date is \textbf{exclusive}.

Data for the current day should be transferred using the real time
mode (\filename{-R}) to enable the work arounds for the open\slash
finalised files. The real time mode will not remove \filename{_o.arcom}
files (otherwise \filename{rsync} might transfer them multiple times,
slowing down data transfers and increasing download size). Old
\filename{_o.arcom} files an be safely removed by using the clean up
mode (\filename{-c}).

Automated data transfers, including conversion to MIA format and
creation of summary plots, can be performed by adding the following
lines to your \filename{cron} entry (see \filename{man crontab} for
more details). Adjust the riometer name to suit your installation.
%\IncludeShellFile{riometer-processing/crontab.txt}{Example
%  cron jobs for automated ARCOM data transfer}
{\relsize{-3}\IncludeMatlabFileVerb{riometer-processing/crontab.txt}}
\embedfile[mimetype=text/plain,desc=Example cron jobs for automated ARCOM data transfer]{riometer-processing/crontab.txt}

\filename{arcom_rsync} attempts to connect to the host \filename{RIO}
with SSH. You will need to create an entry in your
\envVar{HOME/.ssh/config} or \filename{/etc/ssh/sshd_config} file on
the data processing computer to map \filename{RIO} to the correct
hostname. An example excerpt is given below.
\IncludeShellFile{riometer-processing/example-ssh-config.txt}{Example SSH config excerpt}


\helpbox{%
  Times in \filename{cron} are in local time. If your computer is not
  set to use the \filename{UT} or \filename{UTC} timezone you must
  take into account both the timezone offset and any daylight saving
  which may occur.
  \item the \filename{> /dev/null 2>&1} part is to redirect standard
    output and standard error to \filename{/dev/null} where it is
    ignored. For debugging the \filename{cron} jobs you may wish to
    remove those parts.
}

\warningbox{
  It is possible for several \filename{rsync} processes to transfer
  the same file at the same time. \filename{arcom_rsync} includes a
  lock mechanism to prevent this happening (\filename{-L}); it is
  recommended you use this facility for automated data transfers for
  the current day.
}

\section{Converting ARCOM format data files}

If the ARCOM files are transferred with the \filename{arcom_rsync}
script and the options given in the example \filename{crontab} then it
should not normally be necessary to manually convert the ARCOM files
to Matlab's \filename{.mat} files. However, if some error occurs it
may be necessary to do this manually, which can be done with the
\filename{arcom2mia} script. The following command-line options can be
used to modify the default behaviour.

\begin{description}
\item[\filename{-e ENDDATE}]
\item[\filename{-r RIO}]
\item[\filename{-s STARTDATE}]
\end{description}
Where \filename{RIO} is the MIA code for the riometer (\eg,
\filename{rio_nal_2}), STARTDATE and ENDDATE are the start and end
dates respectively, remembering that end date is
\textbf{exclusive}.

\helpbox{%
  The script supports additional options not listed here but they are
  not useful in normal operation.
}

\section{Converting IRIS format data files}

IRIS data files are converted to MIA objects stored in Matlab's
\filename{.mat} files with the command \filename{iris2mia}. This
command is specific to Lancaster's data transfer needs and will need
modification for use elsewhere.


\section{Customising the data transfer\slash processing scripts}

The data transfer and processing and scripts will read their own
configuration file, if it exists. The configuration file should be
saved into the same directory as the shell script, with the same name
as the script but with \filename{.config} appended. The file is
sourced by the script and can contain any valid Bourne shell
(\filename{sh}) commands.

\warningbox{Do not edit the original data transfer\slash processing
  scripts, they are overwritten when a newer MIA version is
  installed. Use the configuration files to set any necessary environment
  variables or to configure an appropriate \filename{umask}
  setting. 
\item If you really must use an editted version of a script be sure to
  rename it so that is isn't overwritten.}
