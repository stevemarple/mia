function r = info_local(in, vout, s, req, varargin)
%INFO_LOCAL Example to customise the data location for RIO_KAR_1

% This function acts to intercept the normal result from INFO and has the
% opportunity to modify it before the value is returned to the user. By
% default all data should be returned unchanged. (So r = vout)
r = vout;

% Assume all requests are valid
val = 1;

% Check the request type passed to INFO.
switch req
  % Only intercept those for 'defaultfilename'
 case 'defaultfilename'
  
  % Only process requests which have a non-empty result.
  if ~isempty(r.varargout{1})
	
    % Convert URLs to a local filename
    if isfield(r.varargout{1}, 'fstr')
      
	  % If all requirements are met (including a matching fstr value)
      % then the returned result is modified.
      r.varargout{1}.fstr = ...
	  strrep(r.varargout{1}.fstr, ...
		 'http://www.dcs.lancs.ac.uk/iono/miadata', mia_datadir);
    end
  end
  
 otherwise
  val = 0; % not valid here
end

% By setting validrequest to true it is possible to extend INFO to handle
% requests that the original command was not able to process (or
% conversely, to block requests). However we don't wish to do that here.
% r.validrequest = r.validrequest | val;
