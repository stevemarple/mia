\chapter{\protect\filename{info} functions}
\label{chapter:info-functions}

\section{Introduction}
In order for a multi-instrument analysis approach to succeed it is
vital that the specific details of instruments can be obtained via a
standard interface. It is not useful to hardcode certain information
into a function, (\eg, the number of beams an imaging riometer
contains), as this information varies between instruments and would
prevent a common implementation. One solution would be for the
instrument objects to contain all of the necessary information,
however it is not desirable for the instrument object to contain data
which may change over time or which would consume large amounts of
memory.  Instead an \filename{info} function exists to provide
information which is necessary but is not appropriate to be stored
inside the instrument object.

\section{Implementation}
\label{section:info-functions-implementation}
The behaviour of the \filename{info} functions is intended to be as
future-proof as possible, so that additional information can be added
in a way which minimises the number of modifications which must be
made to existing files. This is achieved by several techniques.

\begin{itemize}
\item Firstly as much of the instrument-specific data is stored in
  data files, which can be recreated from a PostgreSQL database which
  contains all the relevant information. (See \filename{help
    makeinstrumentdatafunctions}).

\item \filename{mia_instrument_base/info} is responsible for loading
  the appropriate data file. The data files are deliberately not
  stored in private directories so that each user may customise the
  details by providing his/her own version of the data file in his/her
  \filename{MIA/custom} directory. Requests which are common to all
  instrument classes (see below) are processed by
  \filename{mia_instrument_base/info}.
  
\item \filename{mia_instrument_base/info} then calls
  \filename{<instrument class>/info2}, so that each instrument class
  is then given a chance to modify the return value, and to check for
  additional information requests.  

\item Next, the information request can be modified for any particular
  instrument. If the functions \filename{info_<I>_<F>} and\slash or
  \filename{info_<I>_<A>_<SN>} exist anywhere on Matlab's search path
  they are called to allow modification based on the facility and
  instrument, where
  \begin{description}
  \item[\filename{<I>}]is replaced by the instrument abbreviation
    (e.g., \filename{rio} for riometer, see
    \filename{getinstrumenttypeinfo} for more details),
  \item[\filename{<F>}] is replaced by the facility name in lower case,
  \item[\filename{<A>}] is replaced the the instrument's abbreviation (e.g.,
    \filename{kil} for Kilpisj\"arvi imaging riometer)
  \item[\filename{<SN>}] is replaced by the instrument's serial number.
  \end{description}

\item Finally, if a localisation function \filename{info_local} exists
  then it is called. This allows a given installation the opportunity
  to customise its behaviour, for instance, changing URL locations to
  filenames.
\end{itemize}

The information available depends on the instrument type, but all
types should support the requests below: 
\begin{description}
\item[bestresolution] The best
  temporal resolution of the instrument, as a \filename{timespan}.

\item[defaultfilename] How to load data from a given
  instrument. Required argument is the data class name (or better
  still, an object of that class containing any required information,
  e.g., beam number). Additional required or optional arguments should
  be passed as parameter name/value pairs. The return argument is a
  structure, with the following fields:
  
  \begin{description}
  \item[fstr] normally a \filename{strftime} format string, though for
    riometer QDC classes it is a \filename{beamstrftime} format string 
    \item[duration] a \filename{timespan} object indicating how much
      data each file contains.
    \item[failiffilemissing] a logical flag indicating whether it is
      acceptable for data files to be missing.
    \item[format] a \filename{char} describing the name of the data
      format (\filename{mat} for Matlab format files, \filename{ascii}
      for ASCII format files). Alternative formats are also possible,
      and if specified the existence of a function
      \filename{<format>_file_to_mia} is assumed.
      \item[size] the correct size of the data format, \filename{NaN}s may be
        used for row/column sizes which are variable.
      \item[loadfunction] name of the load function, if data cannot be
        loaded by loaddata.
        \item [savefunction] name of the save function if data is not
          saved as Matlab files.
\end{description}

Other fields may be present depending upon the instrument.

\item[endtime] End of operations for the instrument. See
  \filename{operatingtimes} for more details.

\item[operatingtimes]
  A two element array of timestamp objects containing the start
  (inclusive) and end (exclusive) times of operation. An invalid start
  time signifies that operating times are not known. A valid start
  time but invalid end time indicates that the instrument is currently
  operational.

\item[starttime] Start of operations for the instrument. See
  \filename{operatingtimes} for more details.

\item[url] A URL for the instrument, or an empty string if not known.

\end{description}

Each instrument class is free to implement additional information
requests. It is also permissible that the \filename{info} functions
specific to a given instrument or facility implement additional
information requests (this is most useful when dedicated load or save
functions are required). Note that the requests should not include a
space, this restriction is so that information requests can be stored
in structures, and Matlab's structures do not allow spaces in the
field names.

\section{Parameters for \protect\filename{info} helper functions}

The \filename{info} function is permitted to return a variable number
of parameters, and is defined to return a variable argument list (see
\filename{help varargout}). Returning empty matrices is permitted. The
helper functions \filename{<instrument class>/info2},
\filename{info_<I>_<F>}, and \filename{info_<I>_<A>_<SN>} are required
to ignore unknown information requests, but \filename{info} must not,
it signals an error (the information request may have been misspelt
and the user must be made aware of the problem). To fulfil these
requirements the helper functions take 5 parameters:
\begin{description}
\item[in] The instrument object (always a scalar)

\item[vout] The default output variables, a scalar structure
  containing two fields:
  \begin{description}
  \item[varargout] a cell array which will become the variable output list (varargout) from \filename{info}.
  \item[validrequest] a logical value indicating if the request is
    valid.
  \end{description}

\item[s] A structure containing details about the instrument.

\item[req] The name of the information request, which must be valid as
  a structure fieldname.

\item[varargin] Addtional parameters which may be optional or required
  by the given information request. 
\end{description}

The function must return a structure of the same type as
\filename{vout}. If the information request is not known then the
return value should be the same as \filename{vout}. If the information
requested is valid then the \filename{validrequest} field must be set
to a true value.
