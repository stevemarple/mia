\chapter{Interpretation of riometer data}

\section{Introduction}
A riometer measures the intensity of cosmic radio noise at the surface
of the Earth. When an electromagnetic wave passes through the
ionosphere collisions between charged particles (usually electrons)
and neutral gases remove energy from the wave. By measuring the
received signal intensity at the Earth's surface and comparing it to
the expected value (the quiet-day curve) a riometer can deduce the
absorption (attenuation) of the trans-ionospheric signal. Thus the
absorption measurements provide an indication of ionisation changes
occurring in the ionosphere. To avoid the need for orbiting sounders
riometers use the cosmic noise background as a signal source. In
theory only one QDC is needed as the cosmic background is constant on
a human timescale. In practice instrumental and seasonal effects
(temperature, snowfall, changes in ground conductivity \etc) all alter
the expected signal level.  Various factors can interfere with the
absorption measurements. Lightning and solar radio emissions can cause
unexpected increases in the background noise level which can manifest
themselves as negative absorption values. Radio stars (in the Northern
hemisphere most commonly Cassiopeia~A and Cygnus~A, and Sagittarius~A
for the Southern hemisphere) act as point sources and frequently cause
scintillation.

\section{Data interpretation}
Phenomena commonly observed in the summary plots are described
below. In each case the same example is represented by an absorption
plot, a power/QDC plot and a keogram.
\renewcommand{\figscale}{0.5}

\subsection{Quiet day}
\begin{figure}[p]
  \centering
  \subfigure[Power plot.]{
    \label{fig:interpret-quiet-p}
    \includegraphics[scale=0.5]{figures/riometry/quiet-p}}\\
  \subfigure[Absorption plot.]{
    \label{fig:interpret-quiet-a}
    \includegraphics[scale=\figscale]{figures/riometry/quiet-a}}\\
  \subfigure[Keogram.]{
    \label{fig:interpret-quiet-k}
    \includegraphics[scale=\figscale]{figures/riometry/quiet-k}}\\
  \caption{Quiet day.}
  \label{fig:interpret-quiet}
\end{figure}

A typical quiet day, with an absence of any particle precipitation,
solar radio emissions and lighting, is shown in
figure~\ref{fig:interpret-quiet}. Note that the QDC varies according to
sidereal time, season and beam. 

The absorption plot (figure~\ref{fig:interpret-quiet-a}) shows very low
values, comparable with the resolution of IRIS (about \dB{0.05}),
hence the plot appears to be very noisy. Only around 22:15~UT is there
any indication of any absorption.

The received power plot (figure~\ref{fig:interpret-quiet-p}) clearly shows
how little activity is present. Except for around 22:15~UT the quiet
day curve is in the middle of the noise level of the received
power. This is an example of an excellent quiet-day curve.

The keogram (figure~\ref{fig:interpret-quiet-k}) shows almost no spatial
or temporal structure. The upper limit of about \dB{0.8} is very
low. The coloured patch around 06:00~UT is scintillation.


\subsection{Auroral absorption}
\begin{figure}[p]
  \centering
  \subfigure[Power plot.]{
    \label{fig:interpret-aa-p}
    \includegraphics[scale=0.5]{figures/riometry/aa-p}}\\
  \subfigure[Absorption plot.]{
    \label{fig:interpret-aa-a}
    \includegraphics[scale=\figscale]{figures/riometry/aa-a}}\\
  \subfigure[Keogram.]{
    \label{fig:interpret-aa-k}
    \includegraphics[scale=\figscale]{figures/riometry/aa-k}}\\
  \caption{Auroral absorption.}
  \label{fig:interpret-aa}
\end{figure}

This example shows 3 well defined substorms, highlighted in
yellow. The substorms are clearly seen in the absorption plot
(figure~\ref{fig:interpret-aa-a}) as periods of high absorption. From the
power plot (figure~\ref{fig:interpret-aa-p}) it is obvious that the
absorption is due to a drop in received power, and is not due to
problems with the quiet-day curve.

The keogram (figure~\ref{fig:interpret-aa-k}) shows the absorption extends
over the entire north-south range of IRIS. Since the coloured stripes
are vertical it is possible to deduce that the absorption occurred
over the IRIS field of view simultaneously and did not move into view
from the north or south.

\subsection{Polar cap absorption}
\begin{figure}[p]
  \centering
  \subfigure[Power plot.]{
    \label{fig:interpret-pca-p}
    \includegraphics[scale=0.5]{figures/riometry/pca-p}}\\
  \subfigure[Absorption plot.]{
    \label{fig:interpret-pca-a}
    \includegraphics[scale=\figscale]{figures/riometry/pca-a}}\\
  \subfigure[Keogram.]{
    \label{fig:interpret-pca-k}
    \includegraphics[scale=\figscale]{figures/riometry/pca-k}}\\
  \caption{Polar cap absorption.}
  \label{fig:interpret-pca}
\end{figure}

Figure~\ref{fig:interpret-pca} shows a typical example of polar cap
absorption. PCA events often last several days. The absorption is
significantly larger when the D~region is exposed to solar UV
radiation, and this day\slash night transition is a key feature for
identifying polar cap absorption in riometer data. PCA events are
normally very strong, often \dB{\ensuremath{> 3}} for sustained
periods. Auroral absorption events are often less intense and normally
last only a few hours. Sustained high levels of absorption are a good
indication of a PCA event. The received power plot
(figure~\ref{fig:interpret-pca-p}) clearly shows a large loss in
received signal power.

\subsection{Sudden ionospheric disturbance}
\begin{figure}[p]
  \centering
  \subfigure[Power plot.]{
    \label{fig:interpret-sid-p}
    \includegraphics[scale=0.5]{figures/riometry/sid-p}}\\
  \subfigure[Absorption plot.]{
    \label{fig:interpret-sid-a}
    \includegraphics[scale=\figscale]{figures/riometry/sid-a}}\\
  \subfigure[Keogram.]{
    \label{fig:interpret-sid-k}
    \includegraphics[scale=\figscale]{figures/riometry/sid-k}}\\
  \caption{Sudden ionospheric disturbance.}
  \label{fig:interpret-sid}
\end{figure}

A sudden ionospheric disturbance (figure~\ref{fig:interpret-sid}) is
sometimes known as a \emph{Delinger fade} or short-wave fadeout. The
cause of the intense ionisation which produces the absorption is not
energetic particles but hard X-rays entering the atmosphere, normally
from the sun. A distinct signature of some of the SIDs seen in the
riometer data is a short period of strong absorption immediately after
a short solar radio emission. In such circumstances the absorption
cannot be due to energetic particles due to the short time delay,
therefore the likely explanation is that the radio emission at
\MHz{38.2} was accompanied with hard X-ray radiation. The data should
be compared with X-ray flux to rule out coincidental particle
precipitation. The American Association of Variable Star Observers has
a SID programme and also maintains a database of sudden ionospheric
disturbances.  In the example below the solar radio emission lasts
approximately 13~minutes, and is immediately followed by a period of
absorption lasting approximately 9~minutes.


\subsection{Scintillation}
\begin{figure}[p] \centering \subfigure[Power plot.]{
    \label{fig:interpret-scint-p}
    \includegraphics[scale=0.5]{figures/riometry/scint-p}}\\
\subfigure[Absorption plot.]{
    \label{fig:interpret-scint-a}
    \includegraphics[scale=\figscale]{figures/riometry/scint-a}}\\
\subfigure[Keogram.]{
    \label{fig:interpret-scint-k}
    \includegraphics[scale=\figscale]{figures/riometry/scint-k}}\\
  \caption{Scintillation.}
  \label{fig:interpret-scint}
\end{figure}

Ionospheric scintillation (figure~\ref{fig:interpret-scint}) is
characterised by large variations in the received signal power. It is
not often seen in wide beam data, or at lower time resolutions (such
as 2~minutes used by the summary plots). This is because wide beams
and long sampling intervals both average out the variation in cosmic
noise level. It is unusual to see ionospheric scintillation in the
summary line plots, even though it is common in requested data. Any
scintillation seen in the wide beam antenna is a clue that the
ionosphere was very irregular at that time.  Scintillation is
sometimes seen in keogram plots such as in
figure~\ref{fig:interpret-scint-k}).

The cause of ionospheric scintillation is due to irregularities in the
ionosphere being illuminated by a strong point source. The
irregularities act as a diffraction grating and the riometer measures
large variations in signal strength, corresponding to whether the wave
fronts are reinforcing or cancelling each other. In the Northern
hemisphere scintillation is only usually apparent when Cassiopeia~A
(or occasionally Cygnus~A) is in view.

\subsection{Solar radio emissions}
\begin{figure}[p] \centering \subfigure[Power plot.]{
    \label{fig:interpret-emissions-p}
    \includegraphics[scale=0.5]{figures/riometry/emissions-p}}\\
\subfigure[Absorption plot.]{
    \label{fig:interpret-emissions-a}
    \includegraphics[scale=\figscale]{figures/riometry/emissions-a}}\\
\subfigure[Keogram.]{
    \label{fig:interpret-emissions-k}
    \includegraphics[scale=\figscale]{figures/riometry/emissions-k}}\\
  \caption{Solar radio emissions.}
  \label{fig:interpret-emissions}
\end{figure}
During active periods the sun may produce powerful radio emissions
(figure~\ref{fig:interpret-emissions}). The radio emissions are more
prevalent near solar maximum. Solar radio emissions are seen only
during daylight hours.  The increase in received power from solar
radio emissions is seen in the absorption plot as negative values with
a large magnitude.

The keogram (figure~\ref{fig:interpret-emissions-k}) has a fixed lower
limit of \dB{0}, so negative absorption levels appear as dark blue
stripes.

\subsection{Lightning}
\begin{figure}[p] \centering \subfigure[Power plot.]{
    \label{fig:interpret-lightning-p}
    \includegraphics[scale=0.5]{figures/riometry/lightning-p}}\\
\subfigure[Absorption plot.]{
    \label{fig:interpret-lightning-a}
    \includegraphics[scale=\figscale]{figures/riometry/lightning-a}}\\
\subfigure[Keogram.]{
    \label{fig:interpret-lightning-k}
    \includegraphics[scale=\figscale]{figures/riometry/lightning-k}}\\
  \caption{Lightning.}
  \label{fig:interpret-lightning}
\end{figure}
Lightning (figure~\ref{fig:interpret-lightning}) may be confused with
short, spiky solar radio emissions. However this is not normally
important as both are processes which are usually discarded in the
interpretation of absorption data.


