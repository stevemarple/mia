\chapter{Creating quiet day curves (QDCs)}

The creation of accurate quiet-day curves is the most important stage
of riometer data processing as the QDC is the baseline from which all
absorption measurements are made. In theory only one QDC is needed as
the cosmic background is constant on a human timescale. In practice
instrumental and seasonal effects (temperature, snowfall, changes in
ground conductivity etc.) all alter the expected signal
level. Therefore quiet day curves must be made frequently enough to
account for such changes but include sufficient days that 5 or 6 quiet
days are included. Experience has shown that making new quiet day
curves every 14 days is an acceptable compromise.  In MIA QDCs are
valid for a 14 day period, the start of the valid period is known as
the nominal start time, the start and end times of the data used to
make the QDC do not have to match the nominal start/end times. It is
often beneficial to use data from two or three days before the nominal
start time and two or three days after the nominal end time,
particularly during geomagnetically active periods. The nominal start
times are fixed and repeat every 14 days, allowing the nominal start
time to be computed for any given date. For data periods which cross
QDC boundaries the QDC in effect for the middle of the period will be
used.  

\section{Check summary plots of the required period}

View summary plots for the period over which the QDC is to be
made. The nominal start and end times for a given time can be
calculated with the following code:
\begin{Code}
  [starttime, endtime] = calcqdcstarttime(rio, t);
\end{Code}
where \code{starttime} is the nominal start time, \code{endtime} is
the nominal end time, \code{rio} is the riometer instrument (\eg,
\code{rio_kil_1} for Kilpisj\"arvi) and \code{t} is the time to be queried.

\begin{itemize}
\item Check that the data is processed; transfer and process any missing
  received power data.
\item Check the data is not corrupted; corrupted periods should be
  excluded (see below).
\item Count the number of quiet days; there should be 4 or more quiet
  days in order to get a reliable QDC.
\item Decide on the start and end dates/times for the period of data
  you use. Note that the actual start and end times do not have to
  match the period for which the QDC will apply.
\end{itemize}

\section{Create the script file}

Find the correct data/time for the start of the QDC period for which
you wish to create a QDC. Copy the script template file
\filename{local/pric/templates/make_qdc_rio_nal_2.m} and save it based on the
nominal start date/time of the QDC, \eg,
\filename{make_qdc_rio_nal_2_YYYYMMDD.m} (where \code{YYYY} is the
year, \code{MM} the month and \code{DD} the day of month).  Edit the
script file to include the correct date/time for the nominal start
time of the QDC period, and the start and end times of the data that
will be used to create the QDC. Other options which may be set in the
\code{make\_qdc} script are given in
table~\ref{tab:create-qdcs-other-options}.

\begin{table}[!h]
  \begin{center}
    \begin{tabular}{p{3cm}p{10cm}}
      \textbf{Variable} & \textbf{Effect} \\
      \code{beamsatonce} & The number of beams to process
      simultaneously. Processing more beams simultaneously is more efficient
      but requires more memory.\\
      \code{exclude} & An $n \times 2$ matrix listing periods
      which should be excluded from use when creating the QDC. The first
      column is the start time of each period and the second column the end
      time.\\
      \code{createmethod} & The name of the function used to create the
      QDC. May be a cell array containing the name of the function and any
      optional parameters to be passed to that function. The default
      \code{upperenvelopetoqdc} QDC method accepts a name/value pair
      \code{outputrows} which selects which sorted rows should be
      output for the QDC data; \code{[3 4]} means use the mean of 3rd
      and 4th highest values at any given sidereal time. \\
      \code{prefilter} & A cell array of filters which should be
      applied to the power data before the QDC is created.\\
      \code{postfilter} & A cell array of filter which should be applied to
      the newly-created QDC object before it is saved.\\
      \code{rio} & Select the riometer instrument to use.
    \end{tabular}
    \caption{Other options in the \code{make\_qdc} script.}
    \label{tab:create-qdcs-other-options}
  \end{center}
\end{table}

Once the script file has been created it can be run inside MIA:
\begin{Code}
  make_qdc_RIO_YYYYMMDD
\end{Code}
where RIO is the riometer abbreviation (\eg, \filename{rio_kil_1} for Kilpisj\"arvi), YYYY is the year, MM the month and DD the day of month.

\section{Checking the QDC}
The processed QDC must be visually checked in order to confirm there
are no problems. The check program also removes the preliminary data
quality labels from the QDC. Run the following type in the Matlab
terminal window:
\begin{Code}
  checkqdc('init', 'time', t, 'instrument', rio)
\end{Code}

If \code{checkqdc} is run from the same instance of MIA which was used
to create the QDCs then the variables \code{t} and \code{rio} will be
set to the correct values, otherwise set them to the values defined in
the \filename{make_qdc} script.

The \code{checkqdc} program will create a window similar to
figure~\ref{fig:check-qdc}.\\
\helpbox{The exact view will be dependent upon the riometer
  selected. The shape of the QDCs is dependent upon location; the QDCs
  may look very different to those shown in figure~\ref{fig:check-qdc}.
}

\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/create-qdcs/checkqdc1}
  \caption{Absorption data.}
  \label{fig:check-qdc}
\end{figure}

In figure~\ref{fig:check-qdc} the red line shows the original quiet
day curve and the blue line is a smoothed curve (based on a truncated
Fourier transform to ensure cyclicity). The solid green line is the
mean QDC based on a number of good QDCs, and the dashed green lines
show $\pm 1$ standard deviations from the mean.  If the QDC appears
acceptable then you should press the \myquote{Accept} button (or use
the keyboard shortcut \keypress{a}). If you do not wish to accept the
QDC then you can press the \myquote{Fail} button and remake the QDC
after amending values in the \filename{make_qdc} script. If there are
minor errors (\eg, interference spikes) then it is possible to edit
the data (see section~\ref{sec:qdc-data-editor}).

\section{How to identify good and bad QDCs}

Good QDCs should be free from interference spikes (lightning, solar
radio emissions, man-made interference etc.); if inference spikes are
seen it may be necessary to increase the values used in the
\code{outputrows} option or to include pre-filtering to remove the
interference. The \filename{mia_filter_qdc_mean_sd} filter, which is
designed to remove outliers more than $n$ standard deviations from the
mean, is particularly useful for such occasions. Another remedial
action is to use the \code{exclude} option to remove periods
containing interference from the QDC generating algorithm.  

Any sudden dropout in the signal level probably indicates that parts
of the QDC have been contaminated by cosmic noise absorption events;
it may be necessary to increase the number of days used or to decrease
the values used in the outputrows option.

Good QDCs should follow the general shape of the mean QDC, but it is
not necessary they lie entirely within the dashed green curves
denoting $\pm 1$ standard deviations. Figure~\ref{fig:check-qdc} shows
a good QDC.

When a sufficient number of good QDCs have been generated it is
possible to calculate their mean and standard deviations, and to plot
such data as a guide for identifying the correct shape.

\warningbox{Until a sufficient number of good QDCs have been generated
  it is not possbile to display the mean and standard deviation
  curves, nor is it possible to use the
  \filename{mia_filter_qdc_mean_sd} filter.}

\section{Using the data editor to repair minor errors}
\label{sec:qdc-data-editor}

It is always preferable to select appropriate parameters for the
\filename{make_qdc} script which result in good QDCs rather than
editing a poor QDC; however during geomagnetically-active periods or
times of high solar activity (with interference from solar radio
emissions) it is sometimes necessary to edit the resulting QDCs.

The \myquote{Edit} button allows direct editing of the smoothed
QDC. After editing no further smoothing is applied, the QDC is not
guaranteed to be cyclic. You should normally use the \myquote{Edit
  orig.} option in preference to \myquote{Edit}.

The \myquote{Edit orig.} button enables the original QDC data to be
edited. After the data has been edited a truncated Fourier transform
of the QDC is recalculated, which ensures the final QDC is smoothed
and cyclic. \textbf{This should be your preferred option.}

The \myquote{Edit bad data} button enables a section of the QDC to be
marked bad, and is replaced by NaNs without affecting the smoothing of
the remaining valid parts of the QDC. This option can be used multiple
times to mark multiple sections as bad. This option should be a last
resort as no valid absorption data will be output for the affected
sidereal times; however, during times of repeated solar radio
emissions, such as found at the maximum of the 11 year solar cycle,
this may be the only sensible option.
