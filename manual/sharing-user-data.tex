\chapter[Guidelines for sharing the GUI \texttt{UserData}
property]{Guidelines for sharing the GUI \texttt{UserData} property
  between Matlab functions and callbacks}

The Matlab graphical user interface objects contain a property called
\filename{UserData} where where user functions and callbacks can store
and exchange internal state information. Using \filename{UserData} is
a much better approach than using global variables. If a function or
callback is implemented using global variables then only one instance
of that function\slash callback can be active at any time. For
example, if a zoom function was implemented using global variables
then zooming would work on only one axis at once, attempting to have
more than one axis zoomed at any one time would cause confusion as
there would be only one global variable in which to store the original
axes limits. \filename{UserData} provides a method to avoid this
problem.

In a sophisticated GUI, such as that used by MIA, it will be found
that multiple functions and callbacks will need to share access to
\filename{UserData} in a mutually-acceptable way. This can be achieved
by only assigning structs to \filename{UserData}, with each
function\slash callback using its own field inside that
structure. (There is still a possibility of name clashes but if the
field names are based on the function names then the problem can be
minimised.)

It is important to note that Matlab functions are interrupted by
callbacks, and that callbacks can be interrupted by later
callbacks. This is a dangerous situation when sharing
\filename{UserData} since the interrupting callback may modify the
\filename{UserData}. The interrupted callback would be unaware of the
changes, saving back incorrect data. To prevent this happening it is
necessary to set the \filename{Interruptible} property to
\filename{'off'} on any callbacks which access
\filename{UserData}. (Even read access is affected; the interrupting
callback might read the original data values before the interrupted
callback has saved the modified values, which could lead to the
appearance that the second callback executed before the first, even
though it was initiated afterwards!)

By default \filename{UserData} is created as an empty double
matrix. However, it is important that the initial \filename{UserData}
value is fetched from an object, even if just created by that function
or callback. This allows other functions\slash callbacks the
opportunity to set \filename{UserData} with the \filename{Default...}
or \filename{CreateFcn} properties.

The guidelines for sharing \filename{UserData} are summarised below:
\begin{itemize}
\item When setting callbacks set the \filename{Interruptible} property
  to \filename{'off'}.
\item Every time the function\slash callback is executed fetch the
  \filename{UserData} value, even if the GUI object has only just been
  created.
\item Treat the \filename{UserData} value as a struct (signal an error if it
  isn't empty and and isn't a struct). Choose a meaningful name for
  the field in that structure (preferably based on the function name)
  and store all data in that field (itself being a struct, if
  necessary).
\item Update the \filename{UserData} value after use.
\item If making calls to functions which may change
  \filename{UserData} save the value first, and reread
  \filename{UserData} afterwards.
\end{itemize}

By following these guidelines users are able to safely extend the
functionatilty of MIA.

