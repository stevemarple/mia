\chapter{Using the MIA graphical user interface}
\label{chapter:using-the-gui}

The following examples demonstrate the graphical user interface
(GUI). Please note that additional functionality is available by using
the API method described in chapter~\ref{chapter:using-the-api}.

\warningbox{Users should not attempt to load data files directly, use
  the appropriate function instead.}

\renewcommand{\figscale}{0.5}

\section{Loading absorption data}

Absorption data is computed on-the-fly when requested; this approach
allows alternative QDCs or obliquity factors to be selected. The
following example shows how to load absorption data. In Matlab's
command window type \code{newriodata}. A window should appear
(figure~\ref{fig:new-rio-data-1})

\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/newriodata-1}
  \caption{Loading absorption data.}
  \label{fig:new-rio-data-1}
\end{figure}

Complete the various options and press \myquote{OK}. Assuming the
necessary data files and QDCs exist then after a short while a window
should appear (figure~\ref{fig:abs-data}).
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/absdata}
  \caption{Absorption data.}
  \label{fig:abs-data}
\end{figure}


The window as shown in figure~\ref{fig:abs-data} contains the
processed data; from which it is possible to save, plot or further
process the data. If the window is closed then the processed data is
lost (the data files are not affected). If the data took longer than
2~minutes to create a dialog box will check that you really have
finished with the data (figure~\ref{fig:close-query-dialog}).
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/close-query-dialog}
  \caption{Dialog box questioning if the user has really finished with
  the data.}
  \label{fig:close-query-dialog}
\end{figure}


\subsection{Saving the processed data}

The processed data can be saved for use later by selecting
\myquote{Save} from the \myquote{File} menu. Data can be exported to
an ASCII data file by selecting the \myquote{Export} option from the
\myquote{File} menu.

\warningbox{%
  It is not possible to load data which has been exported as ASCII
  format back into MIA. If you are saving data for later reprocessing
  always save in Matlab format.}


\section{Line plots}
From the \myquote{Tools} menu select the \myquote{Line} plot
option. You will be presented with a dialog box where you can set some
basic parameters (figure~\ref{fig:lineplot-dialog}).
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/lineplot-dialog}
  \caption{Dialog box to enter line plotting parameters.}
  \label{fig:lineplot-dialog}
\end{figure}

It is possible to define how many plot windows the plotting figure
should contain, it should normally be 1 row and 1 column unless
plotting multiple beams. The beam(s) chosen for plotting can be
selected, either graphically or by direct textual entry. The plot
limits can be set automatically or to manually-defined values. The
resulting plot is shown in figure~\ref{fig:abs-lineplot}.
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/abs-lineplot}
  \caption{Example line plot of absorption data.}
  \label{fig:abs-lineplot}
\end{figure}


\section{Image data}
The absorption data can be interpolated onto a regular grid with the
\myquote{Image data} option on the \myquote{Tools} menu. A dialog box
will enable the user to select various options
(figure~\ref{fig:imagedata-dialog}).
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/imagedata-dialog}
  \caption{Dialog box to enter image data parameters.}
  \label{fig:imagedata-dialog}
\end{figure}

The pixel units for the interpolation grid can be set to the values
given in table~\ref{tab:imagedata-grid}.
\begin{table}[!h]
  \begin{minipage}{1.0\linewidth}
    \centering
    \begin{tabular}{|l|l|}
      \hline
      \textbf{Value} & \textbf{Meaning} \\ \hline
      \myquote{deg} & Geographic degrees (latitude/longitude) \\ \hline
      \myquote{km} & Distance from riometer in km \\ \hline
      \myquote{aacgm} & Altitude adjusted CGM\footnote{Requires the
        \filename{cgm} toolbox (not supplied with MIA).} \\ \hline
    \end{tabular}
    \caption{Permitted values for the interpolation grid.}
    \label{tab:imagedata-grid}
  \end{minipage}
\end{table}

On changing the grid units the X and Y grid points will change to
their default values. If necessary adjust the grid by entering new
values. A regular grid can be entered most easily as using the
standard Matlab notation for a number range
(\code{start:step:end}). It is not necessary for the grid steps to be
constant.  The mapping height is the height at which the absorption is
assumed to occur and can be adjusted. The conversion from beam data to
image data takes into account the curvature of the Earth. The
resulting plot is shown in figure~\ref{fig:imagedata}.
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/imagedata}
  \caption{Example of absorption image data.}
  \label{fig:imagedata}
\end{figure}

\section{Image plots}
Once image data has been created it is then possible to create an
image plot by selecting the \myquote{Image plot} option from the
\myquote{Tools} menu on the \textbf{image data window} (\ie,
figure~\ref{fig:imagedata}). A dialog box
(figure~\ref{fig:imageplot-dialog}) allows the number of images per
page to be chosen, and also the start and end samples. Usually
succesive images are displayed but it is possible to increase the
\myquote{step} parameter to show only the first of every $n$
images. All the images are displayed with the same color limits, which
by default are chosen automatically. It is possible to manually
override the upper color limit.
\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/imageplot-dialog}
  \caption{Image plot dialog.}
  \label{fig:imageplot-dialog}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/imageplot}
  \caption{Example of absorption image plot.}
  \label{fig:imageplot}
\end{figure}

\helpbox{%
  The lower color limit is always fixed at \dB{0}. The lower limit can
  ben set manually when the API interface is used
  (chapter~\ref{chapter:using-the-api}).}

\section{Keograms}
Once image data has been created it is then possible to create a
keogram by selecting the \myquote{Keogram} option from the
\myquote{Tools} menu on the \textbf{image data window} (\ie,
figure~\ref{fig:imagedata}). A dialog box
(figure~\ref{fig:keogram-dialog}) allows the default choice of grid
points to be ammended. The default options generate the conventional
North-South keogram but East-West keograms are also possible. An
example keogram is shown in figure~\ref{fig:keogram}.

\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/keogram-dialog}
  \caption{Keogram dialog.}
  \label{fig:keogram-dialog}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[scale=\figscale]{figures/using-the-gui/keogram}
  \caption{Example of absorption keogram.}
  \label{fig:keogram}
\end{figure}
