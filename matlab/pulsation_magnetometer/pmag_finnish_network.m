function r = pmag_finnish_network
%PMAG_FINNISH_NETWORK  Return a list of the Finnish pulsation magnetometers
%
%   r = PMAG_FINNISH_NETWORK
%   r: vector of PULSATION_MAGNETOMETER objects
%
% See also PULSATION_MAGNETOMETER.

r = [pmag_iva_2 pmag_kev_2 pmag_kil_3 pmag_nur_4 pmag_oul_2 pmag_rov_1 ...
     pmag_sod_2];
