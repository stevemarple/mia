function r = pmag_instrumenttypeinfo_data
%PMAG_INSTRUMENTTYPEINFO_DATA  Type information data function for pulsation_magnetometer class.
% Do not access this function directly, use the INSTRUMENTTYPEINFO
% function instead.
%
% This function was created automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument_type_information=pulsation_magnetometer
%
% See INSTRUMENTTYPEINFO

r.aware = [pmag_iva_2 pmag_kev_2 pmag_kil_3 pmag_nur_4 pmag_oul_2 ...
    pmag_rov_1 pmag_sod_2];
r.supported = [];

% end of function
