function r = pmag_iva_2
%PMAG_IVA_2  PULSATION_MAGNETOMETER object for Ivalo, Finland.

% Automatically generated by makeinstrumentdatafunctions
r = pulsation_magnetometer('abbreviation', 'iva', ...
    'serialnumber', 2, ...
    'name', '', ...
    'facility', 'Finnish pulsation magnetometer chain', ...
    'location', location('Ivalo', 'Finland', ...
                         68.600000, 27.400000));

