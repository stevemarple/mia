function r = pmag_kil_3
%PMAG_KIL_3  PULSATION_MAGNETOMETER object for Kilpisjarvi, Finland.

% Automatically generated by makeinstrumentdatafunctions
r = pulsation_magnetometer('abbreviation', 'kil', ...
    'serialnumber', 3, ...
    'name', '', ...
    'facility', 'Finnish pulsation magnetometer chain', ...
    'location', location('Kilpisjarvi', 'Finland', ...
                         69.000000, 20.700000));

