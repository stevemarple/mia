function r = info_pmag_kil_3_data
%INFO_PMAG_KIL_3_DATA Return basic information about pmag_kil_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about pmag_kil_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=pulsation_magnetometer;abbreviation=kil;serialnumber=3

r.limits = [];
r.abbreviation = 'kil';
r.bibliography = '';
r.comment = '';
r.components = {'''X' '''' 'Y' 'Z'};
r.coordinatescheme = '';
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.elevation = [];
r.elevationmethod = '';
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = 'Finnish pulsation magnetometer chain';
r.facility_url = 'http://spaceweb.oulu.fi/projects/pulsations/';
r.facilityid = 7;
r.groupids = [];
r.id = 51;
r.latitude = 69;
r.location1 = 'Kilpisjarvi';
r.location1_ascii = 'Kilpisjarvi';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 20.7;
r.modified = timestamp([2006 12 12 10 21 05.069418]);
r.name = '';
r.piid = [];
r.pulsation = true;
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 3;
r.starttime = timestamp([1995 11 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
