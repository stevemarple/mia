function r = loadobj(a)
%LOADOBJ  Convert TIME objects to TIMESTAMP.
%

r = timestamp(a.dv);

