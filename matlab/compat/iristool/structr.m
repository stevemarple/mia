function s = structr(in)
%STRUCTR  Recursively convert an object or structure into structures.
%
%   s = STRUCTR(in)
%   s: structure
%   in: input structure or object.
%
%   See also STRUCT.

s = in;

% cannot look inside fields of objects so convert to struct
if isobject(s)
  s = struct(s);
end

fn = fieldnames(s);
sz = size(s);
m = 1:prod(sz);

for n = 1:length(fn)
  f = fn{n};
  val = getfield(s, {m}, f);
  if isstruct(val) | isobject(val)
    val = structr(val);
    if length(m) == 1
      s = setfield(s, f, val);
    else
      s = setfield(s, {m}, f, {val});
    end
  end
end

