function r = loadobj(a)
%LOADOBJ  Convert IRISABS objects to RIO_ABS.
%
r = ifb2rio_base(a, 'irisabs');

% check conversion
if ~strcmp(class(r), 'rio_abs')
  error('Loaded IRISABS, but could not convert to RIO_ABS');
end
  
