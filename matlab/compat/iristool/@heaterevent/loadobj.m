function r = loadobj(a)
%LOADOBJ  Convert HEATEREVENT objects to HEATER_DATA.
%

a = struct(a);
a.irisfilebase = struct(a.irisfilebase);

s = rmfield(a, 'irisfilebase');

% check and set instrument
if a.irisfilebase.location == getlocation(eiscat_heater)
  s.instrument = eiscat_heater;
else
  error(sprintf('unknown instrument, (location was %s)', ...
		char(a.irisfilebase.location))); 
end

% convert ontimes/offtimes to vectors to timestamps
s.ontimes = [s.ontimes{:}];
s.offtimes = [s.offtimes{:}];

s.starttime = a.irisfilebase.starttime;
s.endtime = a.irisfilebase.endtime;

if isempty(s.transmitters)
  s.transmitters = nan;
end

c = makeprop(s);
r = heater_data(c{:});

