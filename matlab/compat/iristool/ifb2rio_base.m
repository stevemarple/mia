function r = ifb2rio_base(in, origfiletype)
%IN2RIO_BASE  Convert iristool files to RIO_BASE objects.
%
%   r = IN2RIO_RAWPOWER(in)
%   r: RIO_RAWPOWER object
%   in: irispowertime, irisabs or irisqdc object

% already converted. Maybe loadobj functions have done this for us
if isa(in, 'rio_base')
  r = in;
  return;
end


% if strcmp(origfiletype, 'irisqdcmeansd')
%   in = struct(in)
%   d = getdata(in.irisqdc);
%   switch class(d)
%    case {'double' 'single'}
%     cls = 'rio_qdc_mean_sd';
%    case {'int8' 'uint8' 'int16' 'uint16' 'int32' 'uint32'}
%     cls = 'rio_rawqdc_mean_sd';
%    otherwise
%     error(sprintf('unknown data class (was ''%s'')', class(in.power)));
%   end
%   in
%   r = feval(cls, in.irisqdc, in.stddev);
%   return;
% end

in = structr(in);


if ~isfield(in, 'beamdata')
  if isfield(in, 'beams')
    in.beamdata.beams = in.beams;
    in = rmfield(in, 'beams');
  else
    % in.beamdata.beams = 1:50;
    in.beamdata.beams = []; % defer
  end
  in.beamdata.irisfilebase = in.irisfilebase;
  in = rmfield(in, 'irisfilebase');
end

if size(in.beamdata.beams, 1) > 1
  if length(in.beamdata.beams) == 2
    in.beams = colon(in.beamdata.beams(1), in.beams(2));
  elseif length(in.beamdata.beams) == 3
    in.beams = colon(in.beamdata.beams(1), in.beamdata.beams(2), ...
		     in.beamdata.beams(3));
  else
    error('bad beams field');
  end
end


ifb = in.beamdata.irisfilebase; % for convenience
instrument = [];
oldLoc = [];

% list of riometers we know about
riometerlist = [rio_kil_1 rio_nal_1 rio_sgo rio_hal_1];

oldLoc = location('geolong', ifb.location.geolong, ...
		  'geolat', ifb.location.geolat, ...
		  'name', ifb.location.name, ...
		  'country', ifb.location.country);
for r = riometerlist
  % locations are tested by comparing lat/long, but with tolerance EPS
  if oldLoc == getlocation(r)
    instrument = r;
    break;
  end
end
if isempty(instrument)
  oldLoc
  error(sprintf('Could not find instrument for location %s', ...
		char(oldLoc)));
end

st =  timestamp(ifb.starttime);
et = timestamp(ifb.endtime);
res = timespan(ifb.resolution);
res_2 = res ./ 2;
sampletime = (st + res_2):res:(et- res_2);
integrationtime = repmat(res, size(sampletime));

mia_base_args = {'starttime', st, ...
		 'endtime', et, ...
		 'sampletime', sampletime, ...
		 'integrationtime', integrationtime, ...
		 'creator', ifb.creator, ...
		 'createtime', timestamp(ifb.createtime), ...
		 'processing', ifb.processing, ...
		 'instrument', instrument};
% 		 'resolution', timespan(ifb.resolution), ...


% switch ifb.filetype.name
switch origfiletype
 case {'irispower' 'irispowertime'}
  % class name was irispowertime, but filetype was irispower
  if strcmp(class(in.power), 'double')
    % convert to rio_power
    switch getabbreviation(instrument)
     case 'kil' 
      units = 'dBm';
     case 'nal'
      units = 'raw';
     otherwise
      error('unknown instrument')
    end

    if isempty(in.beamdata.beams)
      in.beamdata.beams = colon(1, size(in.power, 1));
    end
    r = rio_power('beams', in.beamdata.beams, ...
		  'data', in.power, ...
		  mia_base_args{:});
  else
    %  convert to rio_rawpower
    % convert from irisint16 to uint16
    in.power.i16 = double(in.power.i16);
    nans = find(in.power.i16 == -32768);
    nansps = printseries(nans);
    in.power.i16(nans) = 0;
    in.power.i16 = uint16(in.power.i16);
    if isempty(in.beamdata.beams)
      if ~isempty(in.power.i16)
	in.beamdata.beams = colon(1, size(in.power.i16, 1));
      else
	% have to guess, based on the riometer
	[ib wb] = info(instrument, 'beams');
	in.beamdata.beams = sort([ib wb]);
	in.power.i16 = uint16(zeros(length(in.beamdata.beams), ...
				    (timestamp(ifb.endtime) - ...
				     timestamp(ifb.starttime))/...
				    timespan(ifb.resolution)));
      end
    end
    r = rio_rawpower('beams', in.beamdata.beams, ...
		     'data', in.power.i16, ...
		     'units', 'ADC', ...
		     mia_base_args{:});
  end
  
 case 'irisabs'
  if isempty(in.beamdata.beams)
    in.beamdata.beams = colon(1, size(in.abs, 1));
  end
  
  r = rio_abs('beams', in.beamdata.beams, ...
	      'units', 'dB', ...
	      'data', in.abs, ...
	      mia_base_args{:});
  
 case 'irisqdc'
  % for QDC ensure filetype.version is checked carefully...
  % QDCs which are < 1s res for kil should also be changed
  if isempty(in.beamdata.beams)
    in.beamdata.beams = colon(1, size(in.power, 1));
  end

  switch getabbreviation(instrument)
   case 'kil' 
    units = 'dBm';
   case 'nal'
    units = 'ADC';
   otherwise
    error('unknown instrument')
  end

  if strcmp(ifb.filetype.name, 'irisqdc') & ifb.filetype.version == 2
    % Beams stored in separate files. Power is converted to the strange
    % units of dB relative to -150dBm.
    r = rio_qdc('beams', in.beamdata.beams, ...
		'data', in.power-150, ...
		'units', units, ...
		mia_base_args{:});

  elseif (strcmp(ifb.filetype.name, 'irisqdc') ...
	  & ifb.filetype.version == 3) | ...
	(strcmp(ifb.filetype.name, 'irisqdcmeansd') ...
	 & ifb.filetype.version == 2)    
    % Identical to version irisqdc/2 other than the linearisation table was
    % changed. Now files are stored in dBm.
    r = rio_qdc('beams', in.beamdata.beams, ...
		'data', in.power, ...
		'units', units, ...
		mia_base_args{:});

  else
    error(sprintf('Not able to process, filetype: %s, version: %d QDCs', ...
		  ifb.filetype.name, ifb.filetype.version));
  end
  
  samt = (0.5*res):res:(res*getdatasize(r, 'end'));
  r = setsampletime(r, samt);
 otherwise
  error(sprintf('unknown data type (was ''%s'')', origfiletype));
end
