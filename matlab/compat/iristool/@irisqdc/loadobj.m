function r = loadobj(a)
%LOADOBJ  Convert IRISQDC objects to RIO_QDC.
%

r = ifb2rio_base(a, 'irisqdc');

% check conversion
if ~isa(r, 'rio_qdc_base')
  r
  class(r)
  error('Loaded IRISQDC, but could not convert to RIO_QDC');
end
  
