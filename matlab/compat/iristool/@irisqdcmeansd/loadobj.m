function r = loadobj(a)
%LOADOBJ  Convert IRISQDCMEANSD objects to RIO_QDC_MEAN_SD.
%

% r = ifb2rio_base(a, 'irisqdcmeansd');

a = struct(a);
d = getdata(a.irisqdc);

% all irisqdcmeansd objects used QDCs created from normalised data, so
% they must be mapped to rio_qdc_mean_sd
r = rio_qdc_mean_sd(a.irisqdc, a.stddev);

if ~isa(r, 'rio_qdc_mean_sd') & ~isa(r, 'rio_rawqdc_mean_sd')
  error('Loaded IRISQDCMEANSD, but could not convert to RIO_(RAW)QDC_MEAN_SD');
end
  
