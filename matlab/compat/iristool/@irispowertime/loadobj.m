function r = loadobj(a)
%LOADOBJ  Convert IRISPOWERTIME objects to RIO_POWER.
%

r = ifb2rio_base(a, 'irispowertime');

% check conversion
if ~strcmp(class(r), 'rio_power') & ~strcmp(class(r), 'rio_rawpower')
  error('Loaded IRISPOWERTIME, but could not convert to RIO_[RAW]POWER');
end
  
