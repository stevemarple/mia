function r = loadobj(a)
%LOADOBJ  Convert IRISLOCATION objects to LOCATION.
%

% any internationalisation used ISO-8859-1
if any(a.name > 127)
  a.name = iso88591_to_ascii(a.name);
end

r = location('name', a.name, ...
	     'country', a.country, ...
	     'geolat', a.geolat, ...
	     'geolong', a.geolong);
