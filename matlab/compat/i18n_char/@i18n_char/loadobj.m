function r = loadobj(i18)
%LOADOBJ  Load filter for i18n_char class.

% Convert to a normal char
s = struct(i18);
d = i18n_conv_data(s.data, s.encoding, 'ISO-8859-1');
% r = i18n_conv_iso88591_to_ascii(d);
r = i18n_to_ascii(d, 'ISO-8859-1');

