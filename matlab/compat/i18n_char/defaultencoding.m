function r = defaultencoding
%DEFAULTENCODING Default encoding for I18N_CHAR class
%
%  DEFAULTENCODING sets the default encoding for use when the encoding
%  scheme cannot be identified. Override by creating a custom function of
%  the same name

if ispc
  r = 'cp1252';
else
  r = 'ISO-8859-1';
end

