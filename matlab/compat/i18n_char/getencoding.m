function r = getencoding
%GETENCODING  Return the current encoding setting from the locale.
%
%   r = GETENCODING
%   r: ENCODING setting
%
% If the LANG variable is not set then the default encoding is assumed to be
% "ASCII".
%
% See also I18N_CHAR.

default_encoding = defaultencoding;
lang = getlang;
if isempty(lang) 
  r = default_encoding;
  return
end
dot = find(lang == '.');
if isempty(dot)
  r = default_encoding;
  return
end
r = lang((dot+1):end);

