function data = i18n_conv_data(s, e1, e2)
%I18N_CONV  Convert a string from one encoding to another.
%
%   r = I18N_CONV(s, e1, e2)
%   r: converted string
%   s: original string
%   e1: original encoding
%   e2: desired encoding

if ispc
  % hack for windows version
  if strcmpi(e1, 'unicode')
    e1 = 'unicodelittle';
  end
  if strcmpi(e2, 'unicode')
    e2 = 'unicodelittle';
  end
end

infile = tempname2;
[fid mesg] = fopen(infile, 'w');
if fid == -1 
  error(sprintf('could not open %s: %s', infile, mesg));
end

fwrite(fid, s, 'uchar');
fclose(fid);

outfile = tempname2;
if any(strcmp(computer, {'SUN4' 'SOL2'}))
  % Sun version not very good, use piconv instead
  cmd = sprintf('piconv -f %s -t %s %s > %s', e1, e2, infile, outfile);
else
  cmd = sprintf('iconv -f %s -t %s %s > %s', e1, e2, infile, outfile);
end

[status, mesg] = system(cmd);
delete(infile);

if status ~= 0
  delete(outfile);
  cmd
  error(sprintf('could not convert: %s', mesg));
end

[fid mesg] = fopen(outfile, 'r');
if fid == -1 
  error(sprintf('could not open %s: %s', outfile, mesg));
end

data = fread(fid, inf, 'uchar')';
fclose(fid);
delete(outfile);


