function r = iso88591_to_ascii(s)

sd = double(s);

if max(sd(:)) <= 127
  % ASCII chars only
  r = char(s);
end

if any(sd > 127 & sd < 160)
  sd
  error('string contains values which are not valid in ISO8859-1');
end

%
% table made by local_make_table, then editted
t{1} = ' '; % non-breaking space
t{2} = 'i'; % inverted exclamation mark
t{3} = 'Cent'; % cent sign
t{4} = 'Pound'; % pound sign
t{5} = '*';  % currency sign
t{6} = 'Yen'; %Yen sign
t{7} = '|'; % broken bar
t{8} = 'S'; % section sign
t{9} = '"'; % diaresis
t{10} = '(C)'; % copyright sign
t{11} = 'a'; % feminine ordinal indicator
t{12} = '<<'; % left-pointing double arrow quotation indicator
t{13} = '~'; % not sign
t{14} = '-'; % soft hyphen
t{15} = '(R)'; % registered sign
t{16} = '-'; % macron
t{17} = 'deg'; % degree sign
t{18} = '+/-'; % plus-minus sign
t{19} = '^2'; % superscript two
t{20} = '^3'; % superscript three
t{21} = ''''; % acute accent
t{22} = 'u'; % micro sign
t{23} = 'P'; % pilcrow sign
t{24} = '.'; % middle dot
t{25} = ','; % cedilla
t{26} = '^1'; % superscript one
t{27} = '#'; % masculine ordinal indicator
t{28} = '>>'; % right-pointing double arrow quotation indicator
t{29} = '1/4'; % fraction one quarter
t{30} = '1/2'; % fraction one half
t{31} = '3/4'; % fraction three quarters
t{32} = '?'; % inverted question mark
t{33} = 'A'; % A grace
t{34} = 'A'; % A acute
t{35} = 'A'; % A circumflex
t{36} = 'A'; % A tilde
t{37} = 'A'; % A diaresis
t{38} = 'A'; % A ring
t{39} = 'AE'; % AE
t{40} = 'C'; % C cedilla
t{41} = 'E'; % E grave
t{42} = 'E'; % E acute
t{43} = 'E'; % E circumflex
t{44} = 'E'; % E diaresis
t{45} = 'I'; % I grave
t{46} = 'I'; % I acute
t{47} = 'I'; % I circumflex
t{48} = 'I'; % I diaresis
t{49} = '(ETH)'; % Eth
t{50} = 'N'; % N tilde
t{51} = 'O'; % O grave
t{52} = 'O'; % O acute
t{53} = 'O'; % O circumflex
t{54} = 'O'; % O tilde
t{55} = 'O'; % O diaresis
t{56} = '*'; % multiplication sign
t{57} = 'O'; % O slash
t{58} = 'U'; % U grave
t{59} = 'U'; % U acute
t{60} = 'U'; % U circumflex
t{61} = 'U'; % U diaresis
t{62} = 'Y'; % Y acute
t{63} = '(THORN)'; % Thorn
t{64} = 'ss'; % sharp s
t{65} = 'a'; % a grave
t{66} = 'a'; % a acute
t{67} = 'a'; % a circumflex
t{68} = 'a'; % a tilde
t{69} = 'a'; % a diaresis
t{70} = 'a'; % a ring
t{71} = 'ae'; % ae
t{72} = 'c'; % c cedilla
t{73} = 'e'; % e grave
t{74} = 'e'; % e acute
t{75} = 'e'; % e circumflex
t{76} = 'e'; % e diaresis
t{77} = 'i'; % i grave
t{78} = 'i'; % i acute
t{79} = 'i'; % i circumflex
t{80} = 'i'; % i diaresis
t{81} = '(eth)'; % eth
t{82} = 'n'; % n tilde
t{83} = 'o'; % o grave
t{84} = 'o'; % o acute
t{85} = 'o'; % o circumflex
t{86} = 'o'; % o tilde
t{87} = 'o'; % o diaresis
t{88} = '/'; % division sign
t{89} = 'o'; % o slash
t{90} = 'u'; % u grave
t{91} = 'u'; % u acute
t{92} = 'u'; % u circumflex
t{93} = 'u'; % u diaresis
t{94} = 'y'; % y acute
t{95} = '(thorn)'; % thorn
t{96} = 'y'; % y diaresis



r = repmat(' ', 1, 3*numel(sd));
pos = 1;
for n = 1:numel(sd)
  if sd(n) >= 160
    c = t{sd(n)-159};
    len = length(c);
    r(pos + [0:(len-1)]) = c;
    pos = pos + len;
  else
    r(pos) = char(sd(n));
    pos = pos + 1;
  end
end

% trim off any remaining padding characters
r(pos:end) = [];


function local_make_table
for n = [160:255]
  % disp(sprintf('t{%d} = ''%s''; %% %s', n-159, char(n), char(n)));
  disp(sprintf('t{%d} = ''%s''; %% ', n-159, char(n)));
  % fprintf(['''' char(n) ''', ']);
end
disp(' ');

