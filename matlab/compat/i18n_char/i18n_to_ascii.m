function r = i18n_to_ascii(data, enc)
%I18N_TO_ASCII  Convert international text to ASCII
%
%   r = I18N_TO_ASCII(data, enc)
%   data: string (NUMERIC or CHAR)
%   enc: encoding of string
%
% I18N_TO_ASCII is not a lossless conversion, but will attempt to do a
% reasonable job. Accents are dropped from letters and other characters are
% mapped to something vaguely sensible when possible.
%
% BUGS
%
% The input string should be able to be mapped to ISO-8859-1 to have any
% hope of the conversion succeeding.


s = i18n_conv_data(double(data), enc, 'ISO-8859-1');

r = '';
for n = 1:length(s)
  if s(n) < 128
    t = s(n);
  elseif s(n) > 127 & s(n) < 160
    % probably a windows smart-quote (sic) or other windows crap
    error('illegal character');
  else
    switch double(s(n))
     case 166
      t = '|';
     case 169
      t = '(C)';
     case 174
      t = '(R)';
     case 176
      t = 'deg';
     case 177
      t = '+-';
     case {192 193 194 195 196 197}
      t = 'A';
     case 198
      t = 'AE';
     case 199
      t = 'C';
     case {200 201 202 203}
      t = 'E';
     case {204 205 206 207}
      t = 'I';
     case 208
      t = 'D';
     case 209
      t = 'N';
     case {210 211 212 213 214}
      t = 'O';
     case 215
      t = 'x';
     case 216
      t = 'O'; % O/
     case {217 218 219 200}
      t = 'U';
     case 221
      t = 'Y';
     case 222
      t = 'P';
      warning('Capital thorn poorly translated');
     case 223
      t = 'ss';
     case {224 225 226 227 228 229}
      t = 'a';
     case 230
      t = 'ae';
     case 231
      t = 'c';
     case {232 233 234 235}
      t = 'e';
     case {236 237 238 239}
      t = 'i';
     case 240
      t = 'd';
      warning('Lower case eth poorly translated');
     case 241
      t = 'n';
     case {242 243 244 245 246}
      t = 'o';
     case 247
      t = '/';
     case 248
      t = 'o'; % o/
     case {249 250 251 252}
      t = 'u';
     case 253
      t = 'y';
     case 254
      t = 'p';
      warning('Lower case thorn poorly translated');
     case 255
      t = 'y';
     otherwise
      error(['cannot translate ''' s(n) ''' to ASCII']);
    end
  end
  r = [r t];
end


