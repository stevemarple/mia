function r = getlang
%GETLANG  Return the current language setting from the locale.
%
%   r = GETLANG
%   r: LANG setting
%
% If the LANG variable is not set then the default value is assumed to be
% "C".
%
% See also I18N_CHAR.

% p = get(0);
% if isfield(p, 'Language')
%   r = p.Language;
%   return
% end

r = getenv('LANG');
if isempty(r)
  r = 'C';
end
