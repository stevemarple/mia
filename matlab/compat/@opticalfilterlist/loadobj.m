function r = loadobj(a)
%LOADOBJ  Load filter for OPTICALFILTER object.
%

as = struct(a);
r = as.filters(as.data);
