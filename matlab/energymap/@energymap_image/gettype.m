function r = gettype(mia, varargin)
%GETTYPE  Return ENERGYMAP data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: ENERGYMAP object
%
%   See also ENERGYMAP, MIA_IMAGE_BASE, CAMERA.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'energy map';
  
 case 'u'
  r = 'ENERGY MAP';
  
 case 'c'
  r = 'Energy map';
 
 case 'C'
  r = 'Energy Map';
  
 otherwise
  error('unknown mode');
end
