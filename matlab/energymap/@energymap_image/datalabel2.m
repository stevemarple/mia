function [f, m] = datalabel2(mia, varargin)
%DATALABEL  Return string used for labelling data on axes, images etc.
%
%   r = DATALABEL(mia)

f = 'Characteristic energy';
units = getunits(mia);
if ~isempty(units)
  f = sprintf('%s (%%s)', f);
end
m = [];
