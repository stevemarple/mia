function mia = energymap_image(varargin)
%ENERGYMAP_IMAGE  Constructor for ENERGYMAP_IMAGE class.
%
%
%   See also MIA_IMAGE_BASE.

latestversion = 1;
mia.versionnumber = latestversion;

cls = 'energymap_image';
parent = 'camera_image_base';


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
 
elseif nargin == 1 & (isa(varargin{1}, parent) | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  p = feval(parent);
  mia = class(mia, cls, p);
 
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia;
  defaults.load = 0;
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);

  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if logical(defaults.log)
      mialog(mia);
    end
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
