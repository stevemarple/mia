function r = preallocate(mia, len)
%PREALLOCATE  Preallocate object fields to allow BUILD
%
%   r = PREALLOCATE(mia)
%   r: ENERGYMAP_IMAGE object with preallocated fields
%   mia: ENERGYMAP_IMAGE object (derived classes not allowed) 
%   len: size in time dimension
%
%   See also BUILD

if ~strcmp(class(mia), 'energymap_image')
  
  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a PREALLOCATE command to use
  % CONSTRUCTFROMFILES or BUILD. Generate a runtime error with appropriate
  % error message to indicate what needs to be done. Derived classes MUST
  % call this function on their parent field.
  
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia; % copy basic things
r.camera_image_base = preallocate(r.camera_image_base, len);

