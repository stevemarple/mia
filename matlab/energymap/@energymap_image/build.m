function [r, idx2] = build(mia, a, idx)

if ~strcmp(class(mia), 'energymap_image')

  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a BUILD command to use
  % CONSTRUCTFROMFILES. Generate a runtime error with appropriate error
  % message to indicate what needs to be done. Derived classes MUST call
  % this function on their parent field.
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia;
[r.camera_image_base idx2] = build(mia.camera_image_base, ...
				   a.camera_image_base, idx);


