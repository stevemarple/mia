% function make_energy_maps
switch 10
 case 0
  st = timestamp([1996 02 13 00 0 0]);
  et = timestamp([1996 02 15 00 0 0]);
 case 1
  st = timestamp([1997 02 8 19 42 0]);
  et = timestamp([1997 02 8 20 02 0]);
 case 2
  st = timestamp([1997 1 1 0 0 0]);
  % st = timestamp([1997 10 1 21 0 0]);
  et = timestamp([1998 1 1 0 0 0]);
 case 3
  st = timestamp([1998 1 3 0 0 0]);
  % et = timestamp([1999 1 1 0 0 0]);
  et = timestamp([1998 1 10 0 0 0]);
 case 4
  st = timestamp([1997 02 8 0 0 0]);
  et = timestamp([1997 02 9 0 0 0]);
 case 5
  st = timestamp([1998 02 20 0 0 0]);
  et = timestamp([1998 02 21 0 0 0]);
 case 6
  st = timestamp([2001 1 21 17 0 0]);
  et = timestamp([2001 1 21 18 0 0]);
 case 7
  st = timestamp([2001 1 20 0 0 0]);
  et = timestamp([2001 1 24 0 0 0]);

 case 10
  % st = timestamp([1995 09 23 0 0 0])
  % st = timestamp([1996 12 04 0 0 0])
  % st = timestamp([1997 09 30 0 0 0])
  % st = timestamp([1997 10 01 22 0 0])
  % st = timestamp([1998 10 25 0 0 0])
  % st = timestamp([2000 2 03 0 0 0])
  % st = timestamp([2000 10 01 0 0 0])
  st = timestamp([2000 10 29 0 0 0])
  et = timestamp([2002 3 11 0 0 0])
    
  
 otherwise
  error('no case found');
end

qdc = [];
irisdata = [];
t = st
while t < et
  t2 = t + timespan(1, 'h');
  % t2 = t + timespan(1, 'd');
  
  mia = [];
  irisimage = [];
  dasidata = camera_image('starttime', t, ...
			  'endtime', t2, ...
			  'resolution', timespan(10, 's'), ...
			  'instrument', cam_dasi, ...
			  'log', 0, ...
			  'load', 1);
  
  % only load iris data if we have DASI images
  if prod(getdatasize(dasidata))
    [irisdata qdc] = ...
	rio_abs_with_qdc('starttime', t, ...
			 'endtime', t2, ...
			 'resolution', timespan(10, 's'), ...
			 'instrument', rio_kil_1, ...
			 'beams', 1:50, ...
			 'log', 0, ...
			 'load', 1, ...
			 'abs', irisdata, ...
			 'qdc', qdc);
    
    irisimage  = rio_image(irisdata, ...
			   'pixelunits', 'deg');
    

    if prod(getdatasize(irisimage))
      mia = energy_map_calc(irisimage,dasidata);
    end
  end
  
  if ~isempty(mia)
    % Path = strftime(t, '/miadata/energymap/dasi_kil/%Y/%m/%d/');
    % filename = strftime(t, '%Y%m%d%H.mat');
    % mkdirhier(Path);
    
    % save(fullfile(Path, filename), 'mia');
    save(mia);
  end
  
  t = t2;
end
