function r = gettype(in, varargin)
%GETTYPE  Return instrument type.
%
%   r = GETTYPE(r)
%   r: CHAR
%   in: ENERGYMAP object
%
%   See also MIA_INSTRUMENT_BASE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'energymap';
  
 case 'u'
  r = 'ENERGYMAP';
  
 case 'c'
  r = 'Energymap';
end

return


