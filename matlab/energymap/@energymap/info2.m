function r = info2(in, vout, s, req, varargin)

r = vout;
r.validrequest = 1; % assume valid

switch req
  
 otherwise
  r.validrequest = vout.validrequest; % put back to original setting
end
  
