function in = energymap(varargin)
%ENERGYMAP  Constructor for ENERGYMAP class.
%
%   r = energymap
%
%   See also MIA_INSTRUMENT_BASE.

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

if nargin == 0
  % default constructor
  ib = mia_instrument_base;
  in = class(in, 'energymap', ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'camera')
  in = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = mia_instrument_base(varargin{unvi});
  in = class(in, 'energymap', ib);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;
