function varargout = instrumenttypeinfo(in, info, varargin)
%INSTRUMENTTYPEINFO  Return information about the instrument type.
%
%   r = INSTRUMENTTYPEINFO(ene, 'supported')
%   ene: any energymap instrument
%
%   Return a list of supported energymaps. 
%
%   See also ENERGYMAP, energymap/INFO.

switch char(info)
 case 'abbreviation'
  varargout{1} = 'ene';
  
 case 'imageclass'
  varargout{1} = 'energymap_image';
  
 case 'supported'
  % mfilename(in, 'supported')
  varargout{1} = [ene_dasi_kil];
  
 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end
  
