function [energy_data]=energy_map_calc(iris_str,dasi_str);
%
% Function calculates an the energy map from Riometer & camera data, 
% using the approach discribed in M.J. Koshs paper. Using MIA structures
% In Keeping with Kosh's paper Camera data is computed in K_rayleighs.
% At present only energy maps where Camera & Riometer times coincide
% exactly are calculated.
%
% iris_str    = Riometer data in MIA class
% dasi_str    = Camera data in MIA class
% energy_data = Computed energy map as a MIA class
% 
% Created R. Seviour Aug 2001
% Altered R. Seviour Apr 2002
%
% --------------------------------------------------------
% check iris data is riometer, and dasi is camera, error if not 
if ~isa(iris_str,'rio_image')
  error('No riometer data');
end
if ~isa(dasi_str,'camera_image')
  error('No camera data');
end
% -------------------------------------------------------

% Must be same units
if ~strcmp(getpixelunits(dasi_str), getpixelunits(iris_str))
  error(sprintf(['Camera and riometer pixel units must be the same ' ...
		 '(were ''%s'' and ''%s'')'], getpixelunits(dasi_str), ...
		 getpixelunits(iris_str)));
end

% Must be same degs or aacgm
if ~strcmp(getpixelunits(dasi_str), 'deg') & ...
      ~strcmp(getpixelunits(dasi_str), 'aacgm')
  error(sprintf('Pixel units must be ''deg'' or ''aacgm'' (was ''%s'')', ...
		getpixelunits(dasi_str)));
end
% -------------------------------------------------------		  
energy_map=[];

Tol=0.000000001;     % Tolorence value for meaningful IRIS data

% Conversion factor for R to KR for DASI images, Keeping with Mikes paper
Ral_factor=1000;

if ~strcmp(getunits(dasi_str),'Rayleighs')
  error('Non Rayleigh Units ');
end 

e_o_constant = 8.8;  % EISCAT determined constant taken from Mikes paper

% ===================================================================
% get times (at the moment assumes times are equal although done like
%            this so that can be genralised to include time deviation)
dasi_time=getimagetime(dasi_str);
iris_time=getimagetime(iris_str);

% get image data
dasi_image_tot=getdata(dasi_str);
iris_image_tot=getdata(iris_str);

%----------------------------------------------------
% get points of view 
[dasi_xbase dasi_ybase]=getpixels(dasi_str);
[iris_xbase iris_ybase]=getpixels(iris_str);

% this take care of numerical error in nth digit
dasi_xbase=(round((dasi_xbase)*1000))/1000;
dasi_ybase=(round((dasi_ybase)*1000))/1000;
iris_xbase=(round((iris_xbase)*1000))/1000;
iris_ybase=(round((iris_ybase)*1000))/1000;

% establish common points of view 
[energy_x,dasi_x_i,iris_x_i] = intersect(dasi_xbase,iris_xbase);
[energy_y,dasi_y_i,iris_y_i] = intersect(dasi_ybase,iris_ybase);

if length(energy_x) < 4
  error('No common field of view')
end

%-----------------------------------------------------
%          M a i n    L o o p

% ARRRGGGG, can't use 'continue', how primative !

energy_map_no=0;

image_used = zeros(1, getdatasize(dasi_str, 3));

for image_no =1:length(dasi_time)
  
  flag =0; % Flag used to decide weather or not to process iteration
  
  % only do for intersecting times 
  if dasi_time(image_no)~=iris_time(image_no)
    flag=1;
  end
  
  % only do for same resoulution's, steve 
  if getresolution(dasi_str,image_no)~=getresolution(iris_str,image_no)
    flag=1;
  end
  
  % % only do for non-cloudy times 
  % if getcloud(dasi_str, image_no) == 1
  %   flag = 1;
  % end
  
  if flag==0
    image_used(image_no) = 1;
    % counter for the energy map being done
    energy_map_no=energy_map_no+1;
    energy_time(energy_map_no)=dasi_time(image_no);
    energy_res(energy_map_no)= getresolution(dasi_str,image_no);
    energy_int(energy_map_no)= getintegrationtime(iris_str,image_no);
    
    % get image to work with
    dasi_image=dasi_image_tot(dasi_x_i,dasi_y_i,image_no);
    iris_image=iris_image_tot(iris_x_i,iris_y_i,image_no);
    
    % Calculate \sqrt(I)
    root_dasi_image=sqrt(dasi_image/Ral_factor);
    
    if 0 
      % Becky's way
      % =================================
      % Loop over imagedata
      for j=1:length(iris_x_i)
	for k=1:length(iris_y_i)
	  
	  % Filter out meaningless IRIS data
	  if iris_image(j,k) < Tol
	    iris_image(j,k) = nan;,
	  end 
	  
	  %    =  A / \sqrt{I}
	  temp_var=iris_image(j,k)/root_dasi_image(j,k);
	  
	  % e_o = -e_o_constant*(ln(A/I^0.5)-1), taken from Mikes paper
	  energy_map(j,k,energy_map_no)=-e_o_constant/(log(temp_var)-1);
	  
	end
      end
      % ==================================================================
    else
      % Steve's way
      % filter out meaningless IRIS data
      iris_image(iris_image < Tol) = nan;
      % temp_var = iris_image ./ root_dasi_image;
      % energy_map(:,:,energy_map_no) = -e_o_constant ./ (log(temp_var)-1);
      energy_map(:, :, energy_map_no) = -e_o_constant ./ ...
	  (log(iris_image ./ root_dasi_image) - 1);
      
      
    end
  end
end

if isempty(energy_map)
  warning('No calculable energy maps');
  energy_data = [];
  return;
end

% Create Energy map MIA class

% convert keV to eV
energy_map = energy_map * 1000;


% index of images used
idx = find(image_used);

energy_data = ...
    energymap_image('starttime',energy_time(1), ...
		    'endtime',(energy_time(end)+energy_res(end)), ...
		    'instrument', ene_dasi_kil, ...
		    'load', 0,...
		    'log', 0, ...
		    'ypixelpos',energy_y,...
		    'xpixelpos',energy_x,...
		    'data',energy_map,...
		    'units', 'eV', ...
		    'imagetime',energy_time,...
		    'resolution',energy_res,...
		    'integrationtime',energy_int,...
		    'filter', ...
		    opticalfilterlist(getfilter(dasi_str, idx)), ...
		    'cloud', getcloud(dasi_str, idx), ...
		    'pixelunits',getpixelunits(dasi_str));

% =====================================================================

return

