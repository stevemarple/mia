function r = info_ene_dasi_kil_1_data
%INFO_ENE_DASI_KIL_1_DATA Return basic information about ene_dasi_kil_1_data.
%
% This function is not intended to be called directly, use the
% INFO function to access data about ene_dasi_kil_1_data. To override
% information given in this file see the instructions in INFO.
%
% See INFO.

% This function created manually.

r.abbreviation = 'dasi_kil';
r.bibliography = '';
r.comment = '';
r.datarequestid = [];
r.defaultheight = NaN;
r.endtime = timestamp('bad');
r.latitude = nan;
r.location1 = 'Skibotn/Kilpisjarvi';
r.location2 = 'Norway/Finland';
r.logo = 'lancasterunilogo';
r.longitude = nan;
r.modified = timestamp([2006 06 15 8 45 0]);
r.name = 'DASI/IRIS (KIL)';
r.piid = [];
r.pixels.deg.x.min = 18;
r.pixels.deg.x.step = 0.25;
r.pixels.deg.x.min = 23.5;
r.pixels.deg.y.min = 68;
r.pixels.deg.y.step = 0.1;
r.pixels.deg.y.min = 70;
r.resolution = timespan(10, 's');
r.rulesoftheroad = '';
r.starttime = timestamp('bad');
r.url = '';
r.defaultfilename.energymap_image.duration = timespan(1, 'h');
r.defaultfilename.energymap_image.failiffilemissing = 0;
r.defaultfilename.energymap_image.format = 'mat';
r.defaultfilename.energymap_image.fstr = '%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.energymap_image.loadfunction = '';
r.defaultfilename.energymap_image.savefunction = '';
r.defaultfilename.energymap_image.size = [23 21 360];
r.limits = [];
