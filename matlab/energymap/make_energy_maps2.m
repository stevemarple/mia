% function make_energy_maps
switch 5
 case 0
  st = timestamp([1996 02 13 20 0 0]);
  et = timestamp([1996 02 14 00 0 0]);
 case 1
  st = timestamp([1997 02 8 19 42 0]);
  et = timestamp([1997 02 8 20 02 0]);
 case 2
  st = timestamp([1997 1 1 0 0 0]);
  % st = timestamp([1997 10 1 21 0 0]);
  et = timestamp([1998 1 1 0 0 0]);
 case 3
  st = timestamp([1998 1 3 0 0 0]);
  % et = timestamp([1999 1 1 0 0 0]);
  et = timestamp([1998 1 10 0 0 0]);
 case 4
  st = timestamp([1998 02 18 0 0 0]);
  et = timestamp([1998 02 21 0 0 0]);
 case 5
  st = timestamp([1998 1 1 0 0 0]);
  et = timestamp([1999 1 1 0 0 0]);

 otherwise
  error('no case found');
end
 
irisdata = [];
qdc = [];
 
t = st;
while t < et
  t2 = t + timespan(1, 'h');
  % t2 = t + timespan(1, 'd');
  
  mia = [];
  irisimage = [];
  dasidata = camera_image('starttime', t, ...
			  'endtime', t2, ...
			  'resolution', timespan(10, 's'), ...
			  'instrument', cam_dasi, ...
			  'load', 1);
  dasidata = extract(dasidata, 'cloud', 0);
  
  % only load iris data if we have DASI images
  % if prod(getdatasize(dasidata))
  if ~isempty(dasidata)
    [irisdata qdc tries] = ...
	rio_abs_with_qdc('starttime', t, ...
			 'endtime', t2, ...
			 'resolution', timespan(10, 's'), ...
			 'instrument', rio_kil, ...
			 'beams', 1:50, ...
			 'abs', irisdata, ...
			 'qdc', qdc, ...
			 'load', 1);
    
    irisimage  = rio_image(irisdata, ...
			   'pixelunits', 'deg');
    

    if prod(getdatasize(irisimage))
      mia = energy_map_calc(irisimage,dasidata);
    end
  end
  
  if ~isempty(mia)
    % Path = strftime(t, '/miadata/energymap/dasi_kil/%Y/%m/%d/');
    % filename = strftime(t, '%Y%m%d%H.mat');
    % mkdirhier(Path);
    
    % save(fullfile(Path, filename), 'mia');
    save(mia);
  end
  
  t = t2;
end
