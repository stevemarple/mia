function [lon2, lat2] = tc_d2ll(varargin)
%TC_D2LL Find destination given initial point, true course and distance
%
%   [lon2 lat2] = tc_d2ll(loc, tc, d, ...)
%   [lon2 lat2] = tc_d2ll(lon1, lat1, tc, d, ...)
%   loc: LOCATION object
%   lon2: final longitude
%   lat2: final latitude
%   tc: true course
%   d: distance (metres)
%
% Given the true course and distance (tc_d) compute the longitude and
% latitude of the final position. The default behaviour can be modified with
% following parameter name/value pairs:
%
%    'earthradius', CHAR or NUMERIC.
%    The value of the earth radius to use. If the value is a CHAR then it is
%    taken to be the 'type' string for EARTHRADIUS, otherwise the numerical
%    value should have units of metres.

% method taken from http://williams.best.vwh.net/avform.htm#LL


if isa(varargin{1}, 'location')
  lon1 = getgeolong(varargin{1});
  lat1 = getgeolat(varargin{1});
  tc = varargin{2};
  d = varargin{3};
  varargin = {varargin{4:end}};
else
  lon1 = varargin{1};
  lat1 = varargin{2};
  tc = varargin{3};
  d = varargin{4};
  varargin = {varargin{5:end}};
end

defaults.earthradius = '';
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ischar(defaults.earthradius)
  defaults.earthradius = earthradius('type', defaults.earthradius);
end

pi_180 = pi ./ 180;

% convert true course to radians
tc = tc * pi_180;

% convert distance from metres to radians
d = d ./ defaults.earthradius;

% convert latitude and longitude to radians
lat1 = lat1 .* pi_180;
lon1 = lon1 .* pi_180;

lat = asin(sin(lat1).*cos(d)+cos(lat1).*sin(d).*cos(tc));
dlon = atan2(sin(tc).*sin(d).*cos(lat1),cos(d)-sin(lat1).*sin(lat));
lon = mod(lon1-dlon +pi, 2.*pi)-pi;

% convert from radians to degrees
lon2 = lon ./ pi_180;
lat2 = lat ./ pi_180;
