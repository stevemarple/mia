function [lon, lat, data] = greatcirclepath(lon1, lat1, lon2, lat2, varargin)
%GREATCIRCLEPATH  Return a great circle path given two points on the path
%
% [lon lat data] = GREATCIRCLEPATH(lon1, lat1, lon2, lat2, ...)
% lon: vector of longitudes along the path (degrees)
% lat: vector of latitudes along the path (degrees)
% data: interpolated data (optional, see description)
% lat: vector of latitudes along the path
% lon1: longitude of start point (degrees)
% lat1: latitude of start point (degrees)
% lon2: longitude of end point (degrees)
% lat2: latitude of end point (degrees)
%
% GREATCIRCLEPATH returns the longitude and latitude of points which lie on
% the great circle path between the start and end points. The end points
% must be different and cannot be antipodal. GREATCIRCLEPATH will also
% interpolate data between the two points.
%
% The default behaviour can be modified with the following parameter
% name/value pairs:
%
%   'points', NUMERIC The minimum number of points used to define the
%   segment of the great circle path. More points may be used to satisfy the
%   distance criterion.
%
%   'distance', DOUBLE
%   The maximum separation of the points, measured in metres. This is
%   useful when the 'data' parameter is specified.
%
%   'data', [1x2 DOUBLE]
%   If the data values for the defining points are specified GREATCIRCLEPATH
%   will also interpolate the data for the points which lie on the great
%   circle path between those points.
%
%   'tolerance', DOUBLE
%   The absolute tolerance used when testing if points are antipodal or
%   degenerate.
%


data = [];

defaults.points = 4; % minimum number of points
defaults.distance = 100e4; % maximum separation, metres
defaults.tolerance = 4 * eps;

defaults.data = []; % optionally interpolate data along GC route

[defaults unvi] = interceptprop(varargin, defaults);

% wrap points into appropriate ranges
lon1 = mod(lon1, 360);
lon2 = mod(lon2, 360);

if abs(lat1) > 90
  error('start latitude is out of range');
elseif abs(lat2) > 90
  error('end latitude is out of range');
end

% check points are not antipodal (or degenerate)
if abs(abs(lon1 - lon2) - 180) < abs(defaults.tolerance) & ...
      abs(lat1 + lat2) < abs(defaults.tolerance)
  error('points are antipodal');
elseif lon1 == lon2 & lat1 == lat2
  error('degenerate points');
end



dist = m_lldist([lon1 lon2], [lat1 lat2]);

% caclulate number of points to use, which can be determined by the maximum
% distance criterion, or the minimum number of points criterion
points = max(ceil(dist / defaults.distance), defaults.points);

x = zeros(1,points);
y = zeros(1,points);
z = zeros(1,points);

% convert given points to cartesian coordinates
d2r = pi/180;
[x(1) y(1) z(1)] = sph2cart(lon1 * d2r, lat1 * d2r, 1);
[x(end) y(end) z(end)] = sph2cart(lon2 * d2r, lat2 * d2r, 1);

% interpolate between points as a straight line to find a set of
% intermediate points. These are not separated equally along the great
% circle route (they are separated equally on the direct path), but do lie
% on the great circle when converted back to spherical coordinates
idx = 2:(points-1);
idx2 = [1 points];
x(idx) = interp1(idx2, x(idx2), idx);
y(idx) = interp1(idx2, y(idx2), idx);
z(idx) = interp1(idx2, z(idx2), idx);

% optionally interpolate data too
switch numel(defaults.data)
 case 0
  ; % do nothing
  
 case 2
  data(idx2) = defaults.data(:)';
  data(idx) = interp1(idx2, defaults.data(:)', idx);
  
 otherwise
  error('bad size for data');
end

% convert to spherical coordinates
r2d = 180/pi;
[lon lat] = cart2sph(x, y, z);
lon = lon * r2d;
lat = lat * r2d;
