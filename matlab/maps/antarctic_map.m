function [fh, gh] = antarctic_map(varargin)

% MAP ATTRIBUTES
defaults.longitude = 0;
defaults.latitude = -90;
defaults.radius = 30;
defaults.coastline = ''; % use an empty matrix to mark default
% defaults.land = [0 0.6 0];
defaults.land = repmat(0.95, 1, 3);
defaults.water = [0 0.8 1];
defaults.landedgecolor = 'none';

defaults.legend = 1;

[defaults unvi] = interceptprop(varargin, defaults);
fh = figure;
gh = axes('Color', defaults.water);

m_proj('stereographic', ...
       'latitude', defaults.latitude, ...
       'longitude', defaults.longitude, ...
       'radius', defaults.radius);

if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor);
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor);
end



% sometimes Matlab gets upset with these m_map commands
if 1
  m_grid('xtick',12,'xticklabels', '', 'tickdir','out', ...
	 'ytick',[-66.5 66.5],'yticklabels','', 'linestyle',':');
  
  % m_grid('xtick',12,'tickdir','out','ytick',[-59 -70 -80],'linest','-');
  m_grid('xtick',12,'tickdir','out','ytick',[-80 -70], ...
	 'linest','-', 'xticklabels', '', 'yticklabels', '');
  
end

% plot south pole and south magnetic pole
m_line(0, -90, 'LineStyle', '+', 'Color', 'k');
% if ~strcmp(computer, 'PCWIN') & matlabversioncmp('>', '5.3')
%   sp = geocgm('latitude', -90, 'longitude', 0, 'year', 1998);
%   m_line(sp.magnetic_pole_longitude, sp.magnetic_pole_latitude, ...
%        'LineStyle', '+', 'Color', 'k');
% end


