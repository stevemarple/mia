function [rLon, rLat, sunLon, sunLat, points] = terminator(t, varargin)
%TERMINATOR  Calculate the day/night terminator
%
%   [lon, lat, sunLon, sunLat, N] = TERMINATOR(t, ...)
%   lon: longitude vector, in degrees [1xN DOUBLE]
%   lat: latitude vector, in degrees  [1xN DOUBLE]
%   sunLon: longitude of the sub-solar point, in degrees 
%   sunLat: latitude of the sub-solar point, in degrees 
%   N: number of points
%
%   The following parameter name/value pairs are accepted
%
%     'points', [1x1 DOUBLE]
%   The number of point used to calculate the TERMINATOR. If not a multiple
%   of 4 then the number of points used is rounded up to the nearest
%   multiple of 4. A minimum of 8 points are required.
%
%     'height', [1x1 DOUBLE] (metres)
%   Calculate the terminator for a spherical surface h metres above mean
%   sea level.
%
%     'angle', [1x1 DOUBLE] (degrees)
%   The angle of the sun above the horizon for which the terminator is
%   drawn. The default is 0 degrees. Civil, nautical and astronomical
%   twilight starts/end at -6, -12 and -18 degrees respectively.
%
%   See also PLOTTERMINATOR.

% defer setting all parameters so that plotterminator can use [] to get
% the defaults specified later
defaults.points = [];
defaults.height = [];
defaults.angle = []; % angle above horizon
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.points)
  defaults.points = 80;
end
if isempty(defaults.height)
  defaults.height = 0;
end
if isempty(defaults.angle)
  defaults.angle = 0;
end

maxGoodHeight = 250e3;
if defaults.height < 0
  error('height must be >= 0')
elseif defaults.height > maxGoodHeight
  warning(sprintf(['terminator for heights over %s is unreliable (was' ...
		   ' %s)'], ...
		  printunits(maxGoodHeight, 'm', 'multiplier', 3), ...
		  printunits(defaults.height, 'm', 'multiplier', 3)));
end


pointsPerQuad = ceil(defaults.points(1) / 4);
defaults.points = pointsPerQuad  * 4;

r2d = 180/pi;
d2r = pi/180;
% find position of the sun
jd = juliandate(t);
[raR decR radius] = suncoo(jd);

% convert coordinates to 

% initial points
pLon = zeros(1, 4);
pLat = zeros(1, 4);

haR = datevec(t, 4:6)*[3600;60;1] * (2*pi/86400);
gmstR = 2* pi * (gmst(t)/timespan(1, 'd'));

sunLonR = (gmstR - haR - raR);
sunLon = sunLonR * r2d;
sunLatR = decR;
sunLat = sunLatR * r2d;

% sunLon = (gmstR - haR - raR) * r2d;
% sunLat = decR * r2d;

% find sidereal time on Greenwich meridian
gst = ut2lmst(t, 0);
gha = 360 * (1 - gst/timespan(1, 'd')); % Greenwich hour angle


if sunLon < -180
  sunLon = sunLon + 360;
end

if sunLat >= 0
  % sun over northern hemisphere
  % top
  pLon(1) = sunLon + 180; 
  pLat(1) = 90 - sunLat;

  % bottom
  pLon(3) = sunLon; 
  pLat(3) = sunLat - 90;
else
  % sun over southern hemisphere
  % top
  pLon(1) = sunLon; 
  pLat(1) = 90 + sunLat;

  % bottom
  pLon(3) = sunLon + 180; 
  pLat(3) = -(90 + sunLat);
end

% RHS
pLon(2) = sunLon - 90; 
pLat(2) = 0;

% LHS
pLon(4) = sunLon + 90; 
pLat(4) = 0;

x = zeros(1, defaults.points);
y = zeros(1, defaults.points);
z = zeros(1, defaults.points);

% convert the positions of the 4 main points on the great circle to
% cartesian coordinates
[x(1:pointsPerQuad:end), y(1:pointsPerQuad:end), ...
 z(1:pointsPerQuad:end)] = sph2cart(pLon * d2r, pLat * d2r, 1);

% interpolate between the cartesian points
idx = 2:pointsPerQuad;
for n = 0:3
  idx2 = rem([1 pointsPerQuad+1] + (n * pointsPerQuad), defaults.points);
  x(idx + n*pointsPerQuad) = ...
      interp1([1 pointsPerQuad+1], x(idx2), idx);
  y(idx + n*pointsPerQuad) = ...
      interp1([1 pointsPerQuad+1], y(idx2), idx);
  z(idx + n*pointsPerQuad) = ...
      interp1([1 pointsPerQuad+1], z(idx2), idx);
end

% the interpolated points do not lie on the surface of the unit sphere,
% but within it. That is not important when converted back to spherical
% coordinates, since the radius is ignore, only the pointing direction is
% used
[rLonR, rLatR] = cart2sph(x, y, z);


% height adjustment, move the points back away from the sun
% firstly calculate the angular distance (at earths centre) that the
% height adjustment requires
[alphaR Re] = polarangleofray(pi/2, defaults.height);

% adjustment for setting angle
alphaR = alphaR - d2r * defaults.angle;

if alphaR ~= 0

  % linear distance as a proportion of earth radius
  dist = (1 + defaults.height/Re) * sin(alphaR);

  % need cartesian valus of the great circle points (have to compute
  % x,y,z from points on the unit sphere)
  [x y z] = sph2cart(rLonR, rLatR, 1);
  
  % now calculate the appropriate adjustments in cartesian terms
  [xAdj yAdj zAdj] = sph2cart(sunLonR + pi, -sunLatR, dist);
  
  % include height adjustments
  x = x + xAdj;
  y = y + yAdj;
  z = z + zAdj;

  % recompute points on unit sphere (no longer a great circle)
  [rLonR, rLatR] = cart2sph(x, y, z);
end


% figure
% hold on
% plot(x, 'r');
% plot(y, 'g');
% plot(z, 'b');

% rLon = pLon;
% rLat = pLat;

[rLonR, rLatR] = cart2sph(x, y, z);

rLon = rLonR * r2d;
rLat = rLatR * r2d;

rLon(rLon > 180) = rLon(rLon > 180) - 360;

points = defaults.points;


return
if defaults.height > 0 & 0
  % adjust for height

  % find direction of sun
  size(repmat([sunLonR, sunLatR], [points 1]))
  jd
  size([rLonR(:), rLatR(:)])


  sunAzAltR = horiz_coo(repmat([sunLonR, sunLatR], [points 1]), ...
			jd(:), ...
			[rLonR(:), rLatR(:)], 'h');
  azR = sunAzAltR(:, 1) + pi;
  xIono = defaults.height * cos(azR);
  yIono = defaults.height * sin(azR);
  for n = 1:points
    [rLon(n) rLat(n)] = xy2longlat(xIono(n), yIono(n), ...
				   defaults.height, rLon(n), rLat(n));
  end
end

