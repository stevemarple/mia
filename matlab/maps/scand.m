function [fh, gh] = scand(varargin)
%SCAND  Draw a map of northern Scandinavia.
%
%   [fh gh] = SCAND
%   [fh gh] = SCAND(...)
%
%   fh: FIGURE handle
%   gh: AXIS handle
%
%   SCAND uses the M_MAP toolbox. The default behaviour can be modified
%   with the following parameter name/value pairs:
%
%     'longitude', 1x2 DOUBLE
%      The lower and upper longitude limits, respectively.
%
%     'latitude', 1x2 DOUBLE
%      The lower and upper latitude limits, respectively.
%
%     'coastline', CHAR
%     The coastline database to use. Default is M_MAP's default
%     coastline. Other valid options are 'gshhs_c', 'gshhs_l', 'gshhs_i',
%     'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate, high
%     and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     The color of the land on the map.
%
%     'sea', COLORSPEC
%     The color of the sea on the map.
%
%     'lake', COLORSPEC
%     The color of the inland lakes on the map.
%
%     'water', COLORSPEC
%     If set this overrides the color of both inland lakes and the sea.
%
%     'landedgecolor', COLORSPEC
%     The edge color of the land in the map.
%
%     'axes', axes handle
%     Handle of the AXES to use. If not specified (or empty) then a new
%     FIGURE is created.
%
%   See also M_MAP, COLORSPEC.

% MAP ATTRIBUTES
defaults.longitude = [-20 40];
defaults.latitude = [58 81];
defaults.coastline = ''; % use an empty matrix to mark default
defaults.land = [0 0.6 0];
defaults.water = [];
defaults.sea = [0 0.8 1];
defaults.lake = [0 0.6 0.9];
defaults.landedgecolor = 'none';
defaults.axes = [];

defaults.cgm_latitude = 0; % plot CGM latitudes

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~isempty(defaults.water)
  if ischar(defaults.water)
    defaults.water = text2rgb(defaults.water);
  end
  defaults.sea = defaults.water;
  defaults.lake = defaults.water;
end

if ischar(defaults.sea)
  defaults.sea = text2rgb(defaults.sea);
end
if ischar(defaults.lake)
  defaults.lake = text2rgb(defaults.lake);
end
if ischar(defaults.land)
  defaults.land = text2rgb(defaults.land);
end


if isempty(defaults.axes)
  fh = figure;
  gh = axes('Parent', fh);
else
  gh = defaults.axes;
  fh = get(gh, 'Parent');
end

axes(gh);

set(gh, 'Color', defaults.sea);

% m_proj('lambert','lon', defaults.longitude, ...
%  m_proj('albers','lon', defaults.longitude, ...
% Lambert and Albers don't plot lines of magnetic latitude properly for
% some reason (but in m_map?)
m_proj('transverse mercator','lon', defaults.longitude, ...
       'lat', defaults.latitude, ...
       'rect', 'on');

if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, ...
	  'EdgeColor', defaults.landedgecolor, ...
	  'Parent', gh);
  if ~isequal(defaults.lake, defaults.sea) & ...
	~isequal(defaults.land, defaults.sea)
    set(findall(gh, ...
		'Tag', 'm_coast', ...
		'Type', 'patch', ...
		'FaceColor', defaults.sea), ...
	'FaceColor', defaults.lake, ...
	'EdgeColor', 'none');
    
    if 1
      % workaround m_map bug whereby it uses color of current axes, not
      % the one indicated by parent
      cf = get(0, 'CurrentFigure');
      ca = get(cf, 'CurrentAxes');
      set(findall(gh, ...
		  'Tag', 'm_coast', ...
		  'Type', 'patch', ...
		  'FaceColor', get(ca, 'Color')), ...
	  'FaceColor', defaults.lake, ...
	  'EdgeColor', 'none');
    end
  end
  
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor, ...
	'Parent', gh);
  if ~isequal(defaults.lake, defaults.sea)
    for n = 2:2:8
      set(findall(gh, ...
		  'Tag', ['m_' defaults.coastline], ...
		  'Type', 'patch', ...
		  'UserData', n), ...
	  'FaceColor', defaults.lake, ...
	  'EdgeColor', 'none');
    end
  end
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor, ...
	      'Parent', gh);
end

% m_grid is annoying, it only alters the current axes. Therefore remember
% current figure and axes and make gh current axes. Don't use gcf or gca as
% they can create figures/axes.
cf = get(0, 'CurrentFigure');
ca = get(cf, 'CurrentAxes');
axes(gh); % make
m_grid('box','on');
m_grid('backcolor', defaults.sea);
set(cf, 'CurrentAxes', ca);
set(0, 'CurrentFigure', cf);


if defaults.cgm_latitude
  % add lines of constant magnetic latitude

  % first find limits to use
  x1 = get(gh, 'XLim');
  y1 = get(gh, 'YLim');
  [x y] = meshgrid([x1(1) mean(x1) x1(2)], [y1(1) mean(y1) y1(2)]);
  [glon glat] = m_xy2ll(x, y, 'Parent', gh);

  [clon clat] = geo2cgm(glon, glat);

  clatmin = floor(min(clat(:))/10) * 10;
  clatmax = ceil(max(clat(:))/10) * 10;

  % use negative values for west, not 3xx
  clontmp = clon;
  clontmp(clontmp > 180) = clontmp(clontmp > 180) - 360;

  clonmin = min(clontmp(:));
  clonmax = max(clontmp(:));


  clon2 = clonmin:2:clonmax;
  % clon2 = 0:5:360;
  % clon2 = [0:5:360 5];

  for cla = clatmin:5:clatmax
    [gx gy] = cgm2geo(clon2, repmat(cla, size(clon2)));
    h = m_line(gx, gy, ...
	       'LineStyle', '-', ...
	       'Color', 'k', ...
	       'Parent', gh, ...
	       'Tag', 'cgm_latitude');
    ud = get(h, 'UserData');
    ud.cgm_latitude = cla;
  end


end
