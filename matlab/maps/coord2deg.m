function [x2, y2] = coord2deg(loc, x, y, units, varargin)
%COORD2DEG Convert coordinates to geographic degrees
%
%   [x2, y2] = COORD2DEG(loc, x, y, units, ...)
%   x2: X coordinates as geographic degrees
%   x2: Y coordinates as geographic degrees
%   units: input units
%


defaults.height = [];
defaults.antennaazimuth = [];
defaults.year = [];

[defaults unvi] = interceptprop(varargin, defaults);

switch units
 case 'deg'
  x2 = x;
  y2 = y;

 case 'km'
  if isempty(defaults.height)
    error('height not specified');
  end
  [x2 y2] = xy2longlat(x*1e3, y*1e3, defaults.height, loc);
  
 case 'm'
  if isempty(defaults.height)
    error('height not specified');
  end
  [x2 y2] = xy2longlat(x, y, defaults.height, loc);
  
  
 case {'km_antenna' 'm_antenna'}
  if isempty(defaults.height)
    error('height not specified');
  end
  % antenna direction is compass angle, different sense to trig angle
  if isempty(defaults.antennaazimuth)
    error('antennaazimuth not specified');
  end
  
  theta = -defaults.antennaazimuth * pi/180;
  tmpxx = x*cos(theta) - y*sin(theta);
  tmpyy = y*cos(theta) + x*sin(theta);
  if strcmp(units, 'km_antenna')
    tmpxx = tmpxx * 1e3;
    tmpyy = tmpyy * 1e3;
  end
  [x2 y2] = xy2longlat(tmpxx, tmpyy, defaults.height, loc);

 case 'aacgm'
  r = geocgm('latitude', y, ... 
	     'longitude', x, ...
	     'year', defaults.year, ...
	     'direction', -1);
  x2 = r.geocentric_longitude;
  y2 = r.geocentric_latitude;
 
 otherwise
  error(sprintf(['do no know how to convert from %s to deg ' ...
		 'latitude/longitude'], units));
end

