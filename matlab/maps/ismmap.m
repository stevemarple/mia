function r = ismmap(h)
%ISMMAP  Indicate if axes are controlled by M_MAP.
%
%   r = ISMMAP(h)
%   r: LOGICAL, the same size as h
%   h: AXIS handle(s)
%
%   Attempt to identify if the axis is controlled by M_MAP.
%
%   See also M_MAP.

r = logical(zeros(size(h)));
for n = 1:prod(size(h))
  if ~isempty(strfind(get(h(n), 'Tag'), 'm_grid'))
	r(n) = 1;
  elseif ~isempty([findobj(h(n), 'Tag', 'm_map'),
	       findobj(h(n), 'Tag', 'm_coast'),
	       findobj(h(n), 'Tag', 'm_gshhs_c'),
	       findobj(h(n), 'Tag', 'm_gshhs_l'),
	       findobj(h(n), 'Tag', 'm_gshhs_i'),
	       findobj(h(n), 'Tag', 'm_gshhs_h'),
	       findobj(h(n), 'Tag', 'm_gshhs_f')])
    r(n) = 1;
  end
end
