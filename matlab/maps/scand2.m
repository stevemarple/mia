function [fh, gh] = scand2(varargin)
%SCAND2  Draw a map of northern Scandinavia, around Kilpisjarvi.
%
%   See SCAND.

[fh gh] = scand('longitude', [-8 +8] + 20.79, ...
		'latitude', [66 72], ...
		'coastline', 'gshhs_l', ...
		varargin{:});

		


