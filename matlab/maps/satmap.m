function [fh, gh] = satmap(varargin)
%SCAND  Draw a map of northern Scandinavia.
%
%   [fh gh] = SCAND
%   [fh gh] = SCAND(...)
%
%   fh: FIGURE handle
%   gh: AXIS handle
%
%   SCAND uses the M_MAP toolbox. The default behaviour can be modified
%   with the following parameter name/value pairs:
%
%     'longitude', 1x2 DOUBLE
%      The lower and upper longitude limits, respectively.
%
%     'latitude', 1x2 DOUBLE
%      The lower and upper latitude limits, respectively.
%
%     'coastline', CHAR
%     The coastline database to use. Default is M_MAP's default
%     coastline. Other valid options are 'gshhs_c', 'gshhs_l', 'gshhs_i',
%     'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate, high
%     and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     The color of the land in the map.
%
%     'water', COLORSPEC
%     The color of the land in the map.
%
%     'landedgecolor', COLORSPEC
%     The edge color of the land in the map.
%
%   See also M_MAP, COLORSPEC.

% MAP ATTRIBUTES
defaults.longitude = [20.79];
defaults.latitude = [69.05];
defaults.coastline = ''; % use an empty matrix to mark default
defaults.land = [0 0.6 0];
defaults.water = [0 0.8 1];
defaults.landedgecolor = 'none';
defaults.axes = [];

[defaults unvi] = interceptprop(varargin, defaults);
if isempty(defaults.axes)
  fh = figure;
  gh = axes;
else
  gh = defaults.axes;
  fh = get(gh, 'Parent');
end

set(gh, 'Color', defaults.water);

m_proj('satellite','lon', defaults.longitude, ...
       'lat', defaults.latitude, ...
       'rect', 'on');

if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor);
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor);
end


m_grid('box','on');





