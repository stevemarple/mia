function [fh, gh] = svalbard(varargin)
%SCAND  Draw a map of northern Scandinavia.
%
%   [fh gh] = SCAND
%   [fh gh] = SCAND(...)
%
%   fh: FIGURE handle
%   gh: AXIS handle
%
%   SCAND uses the M_MAP toolbox. The default behaviour can be modified
%   with the following parameter name/value pairs:
%
%     'longitude', 1x2 DOUBLE
%      The lower and upper longitude limits, respectively.
%
%     'latitude', 1x2 DOUBLE
%      The lower and upper latitude limits, respectively.
%
%     'coastline', CHAR
%     The coastline database to use. Default is M_MAP's default
%     coastline. Other valid options are 'gshhs_c', 'gshhs_l', 'gshhs_i',
%     'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate, high
%     and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     The color of the land in the map.
%
%     'water', COLORSPEC
%     The color of the land in the map.
%
%     'landedgecolor', COLORSPEC
%     The edge color of the land in the map.
%
%   See also M_MAP, COLORSPEC.

% MAP ATTRIBUTES
defaults.longitude = [5 30];
defaults.latitude = [76 81];
defaults.coastline = ''; % use an empty matrix to mark default
defaults.land = [0 0.6 0];
defaults.water = [0 0.8 1];
defaults.landedgecolor = 'none';
defaults.axes = [];

defaults.cgm_latitude = 0; % plot CGM latitudes
[defaults unvi] = interceptprop(varargin, defaults);
if isempty(defaults.axes)
  fh = figure;
  gh = axes;
else
  gh = defaults.axes;
  fh = get(gh, 'Parent');
end

set(gh, 'Color', defaults.water);

% m_proj('lambert','lon', defaults.longitude, ...
%  m_proj('albers','lon', defaults.longitude, ...
% Lambert and Albers don't plot lines of magnetic latitude properly for
% some reason (but in m_map?)
m_proj('transverse mercator','lon', defaults.longitude, ...
       'lat', defaults.latitude, ...
       'rect', 'on');

if isempty(defaults.coastline)
  % defaults.coastline = 'default'; 
  defaults.coastline = 'gshhs_i'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor);
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor);
end


m_grid('box','on');



if defaults.cgm_latitude
  % add lines of constant magnetic latitude

  % first find limits to use
  x1 = get(gh, 'XLim');
  y1 = get(gh, 'YLim');
  [x y] = meshgrid([x1(1) mean(x1) x1(2)], [y1(1) mean(y1) y1(2)]);
  [glon glat] = m_xy2ll(x, y);

  [clon clat] = geo2cgm(glon, glat);

  clatmin = floor(min(clat(:))/10) * 10;
  clatmax = ceil(max(clat(:))/10) * 10;

  % use negative values for west, not 3xx
  clontmp = clon;
  clontmp(clontmp > 180) = clontmp(clontmp > 180) - 360;

  clonmin = min(clontmp(:));
  clonmax = max(clontmp(:));


  clon2 = clonmin:2:clonmax;
  % clon2 = 0:5:360;
  % clon2 = [0:5:360 5];

  for cla = clatmin:5:clatmax
    [gx gy] = cgm2geo(clon2, repmat(cla, size(clon2)));
    h = m_line(gx, gy, ...
	       'LineStyle', '-', ...
	       'Color', 'k', ...
	       'Tag', 'cgm_latitude');
    ud = get(h, 'UserData');
    ud.cgm_latitude = cla;
  end


end
