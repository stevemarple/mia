function r = plotterminator(t, ah, varargin)
%PLOTTERMINATOR  Plot the day/night terminator onto an M_MAP map.
%
%   h = PLOTTERMINATOR(t, ah, ...)
%
%   h: handles of graphic objects
%   t: TIME object
%   ah: AXES handle
%
%   The following parameter name/value pairs are also accepted:
%
%     'style', ['line' | 'patch' | 'transpatch']
%     Determine if the terminator is to be drawn with a LINE, PATCH or fake
%     transparent patches (see GPC_M_TRANSPATCH).
%
%     'lineargs', { }
%     CELL aray of parameters passed to the LINE or M_LINE function.
%
%     'patchargs', { }
%     CELL aray of parameters passed to the PATCH, M_PATCH,
%     GPC_TRANSPATCH, GPC_M_TRANSPATCH function.
%
%     'gpcalpha', DOUBLE, 0 <= d <= 1
%     Transparency value for GPC_M_TRANSPATCH.
%

% TO DO
% 
% Have each map projection method create x,y matrices for the visible
% terminator and the border. If patch selected then do patch, else do
% line for just the visible part of the terminator.

r = [];

defaults.style = 'patch'; % patch | line
defaults.patchargs = {'EdgeColor', 'none', ...
		    'FaceColor', 'k'};
defaults.gpcalpha = 0.7;
defaults.lineargs = {'Color', 'k', ...
		    'LineStyle', '-', ...
		    'Marker', 'none'};

% want to pass unknown options onto transpatch, so trap any other options
% for TERMINATOR
defaults.points = [];
defaults.height = [];
defaults.angle = []; % angle above horizon
defaults.zdata = [];

[defaults unvi] = interceptprop(varargin, defaults);

if ~strcmp(defaults.style, 'transpatch')
  % unknown parameters are passed onto gpc_transpatch, so if that style
  % is not in use then we don't understand the parameters. Shout.
  warnignoredparameters(varargin{unvi(1:2:end)});
end

global MAP_PROJECTION MAP_VAR_LIST;

[rLon, rLat, sunLon, sunLat, points] = ...
    terminator(t, ...
	       'points', defaults.points, ...
	       'height', defaults.height, ...
	       'angle', defaults.angle);

rLon(rLon < 0) = rLon(rLon < 0) + 360;
ppq = points / 4; % points per quadrant

switch MAP_PROJECTION.routine
  % -----------------------------------------------
 case 'mp_azim'
  if abs(MAP_VAR_LIST.ulat) ~= 90 
    warning([MAP_PROJECTION.name ' works best on a pure polar map']);
  end

  poleSign = sign(MAP_VAR_LIST.ulat); % +1 N, -1 S
  edgeLat = MAP_VAR_LIST.ulat - poleSign * MAP_VAR_LIST.uradius;

  % find longitude where terminator meets edge of map
  if MAP_VAR_LIST.ulat == 90
    rhsIdx = 1:ppq;
    lhsIdx = (3*ppq+1):points;
    visIdx = rLat > edgeLat; % which points are visible on map
  else
    rhsIdx = (ppq+1):(2*ppq);
    % rhsIdx = (2*ppq):-1:(ppq+1);
    lhsIdx = (2*ppq+1):(3*ppq);
    visIdx = rLat < edgeLat;
  end

  if localUnwrapPM180(rLon(rhsIdx(end))) < 0
    % end point in 0->180 range
      rhsLon = interp1(rLat(rhsIdx), ...
		       localUnwrap0to360(rLon(rhsIdx)), ...
		       edgeLat);
  else
    rhsLon = interp1(rLat(rhsIdx), ...
		     localUnwrapPM180(rLon(rhsIdx)), ...
		     edgeLat);
  end

  if localUnwrapPM180(rLon(rhsIdx(1))) < 0
    % end point in 0->180 range
    lhsLon = interp1(rLat(lhsIdx), ...
		     localUnwrap0to360(rLon(lhsIdx)), ...
		     edgeLat);
  else    
    lhsLon = interp1(rLat(lhsIdx), ...
		     localUnwrapPM180(rLon(lhsIdx)), ...
		     edgeLat);
  end
  
  
  clip = 'patch';
  if poleSign > 0
    % N hemisphere specific
    visIdxRhs = visIdx & [ones(1, 2*ppq) zeros(1, 2*ppq)];
    visLonRhs = [rLon(visIdxRhs) rhsLon];
    visLatRhs = [rLat(visIdxRhs) edgeLat];
    
    visIdxLhs = visIdx & [zeros(1, 2*ppq) ones(1, 2*ppq)];
    visLonLhs = [lhsLon rLon(visIdxLhs)];
    visLatLhs = [edgeLat rLat(visIdxLhs)];

    lhsLon2 = lhsLon;
    if lhsLon2 > rhsLon
      lhsLon2 = lhsLon2 - 360;
    end
    perimLon = rhsLon:-1:lhsLon2;
    perimLat = repmat(edgeLat, size(perimLon));
    
    [termX termY] = m_ll2xy([visLonLhs visLonRhs], ...
			    [visLatLhs visLatRhs], ...
			    'clip', clip);
    [perimX perimY] =  m_ll2xy(perimLon, perimLat, 'clip', clip);

  else
    visIdxRhs = visIdx & [ones(1, 2*ppq) zeros(1, 2*ppq)];
    visLonRhs = fliplr([rhsLon rLon(visIdxRhs)]);
    visLatRhs = fliplr([edgeLat rLat(visIdxRhs)]);

    visIdxLhs = visIdx & [zeros(1, 2*ppq) ones(1, 2*ppq)];
    visLonLhs = fliplr([rLon(visIdxLhs) lhsLon]);
    visLatLhs = fliplr([rLat(visIdxLhs) edgeLat]);

    lhsLon2 = lhsLon;
    if lhsLon2 > rhsLon
      lhsLon2 = lhsLon2 - 360;
    end
    perimLon = rhsLon:-1:lhsLon2;
    perimLat = repmat(edgeLat, size(perimLon));
    [termX termY] = m_ll2xy([visLonLhs visLonRhs], ...
			    [visLatLhs visLatRhs], ...
			    'clip', clip);
    [perimX perimY] =  m_ll2xy(perimLon, perimLat, 'clip', clip);
  end
  
  
  % -----------------------------------------------
 case {'mp_cyl' 'mp_tmerc'}
  % arrange terminator longitudes in ascending order from LHS of map
  rLon2 = rLon - 360;
  mask = (rLon2 < MAP_VAR_LIST.longs(1));
  rLon2(mask) = rLon2(mask) + 360;
  [rLon2 idx] = sort(rLon2);
  rLat2 = rLat(idx);
  
  % interpolate for LHS of map
  lhsLon = MAP_VAR_LIST.longs(1);

  mod(rLon2([end 1]), 360),  rLat2([end 1]), mod(lhsLon, 360)
  
  lhsLat = interp1(mod(rLon2([end 1]), 360), ...
		   rLat2([end 1]), mod(lhsLon, 360))
  
  % interpolate for RHS of map
  rhsLon = MAP_VAR_LIST.longs(2);
  rhsLat = interp1(mod(rLon2([end 1]), 360), ...
		   rLat2([end 1]), mod(rhsLon, 360))

  % interpolate for map edges
  lhsLon = MAP_VAR_LIST.longs(1);
  rhsLon = MAP_VAR_LIST.longs(2);
  % figure
  tmp = [rLon2-360 rLon2 rLon+360];
  [tmpLon idx] = unique(tmp);
  tmpLat = repmat(rLat2, [1 3]);
  tmpLat = tmpLat(idx);
  
  % plot(tmp); hold on; plot(tmp2, 'r');
  
  if 0
    tmp = interp1([rLon2-360 rLon2 rLon+360], ...
		  repmat(rLat2, [1 3]), ...
		  [lhsLon rhsLon])
  else
    tmp = interp1(tmpLon, tmpLat, [lhsLon rhsLon])
  end
  lhsLat = tmp(1);
  rhsLat = tmp(2);
  [termX termY] = m_ll2xy([lhsLon rLon2 rhsLon], ...
			  [lhsLat rLat2 rhsLat], ...
			  'clip', 'patch');
  perimX = MAP_VAR_LIST.xlims([2 1]);
  if sunLon < 0
    perimY = MAP_VAR_LIST.ylims([2 2]);
  else
    perimY = MAP_VAR_LIST.ylims([1 1]);
  end

  % -----------------------------------------------
 otherwise
  error('unsupported map projection');
end

if ~isempty(defaults.zdata)
  termZ = repmat(defaults.zdata(1), size(termX));
  perimZ = repmat(defaults.zdata(1), size(perimX));
else
  termZ = [];
  perimZ = [];
end

switch defaults.style
 case 'line'
  r = line('Parent', ah, ...
	   'XData', termX, ...
	   'YData', termY, ...
	   'ZData', termZ, ...
	   'Color', 'k', ...
	   'Marker', 'none', ...
	   'Tag', 'terminator', ...
	   defaults.lineargs{:});
   
 case 'patch'
  r = patch('Parent', ah, ...
	    'XData', [termX perimX],  ...
	    'YData', [termY perimY], ...
	    'ZData', [termZ perimZ], ...
	    'EdgeColor', 'none', ...
	    'FaceColor', 'k', ...
	    'Marker', 'none', ...
	    'Tag', 'terminator', ...
	    defaults.patchargs{:});
   
 case 'transpatch'
  r = gpc_m_transpatch(gpc([termX perimX], [termY perimY]), ...
		       ah, defaults.gpcalpha, ...
		       'convertcoordinates', 0, ...
		       'EdgeColor', 'none', ...
		       'FaceColor', 'k', ...
		       'Marker', 'none', ...
		       'Tag', 'terminator', ...
		       defaults.patchargs{:}, ...
		       'zdata',  [termZ perimZ], ...
		       varargin{unvi});
  
 otherwise
  error(sprintf('unknown style (was %s)', defaults.style));
end
  


% ---------------------------------------------------------------------
% unwrap degree phases into +/- 180 degrees
function r = localUnwrapPM180(a)

r = mod(a, 360);
mask = (r > 180);
r(mask) = r(mask) - 360;
return

% ---------------------------------------------------------------------
% unwrap degree phases into 0 -> 360 degrees
function r = localUnwrap0to360(a)

r = mod(a, 360);
return

