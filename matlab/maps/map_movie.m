function mov = map_movie(mia, varargin)

defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];
defaults.latitude = [];
defaults.longitude = [];
defaults.coastline = '';

defaults.filename = '';
defaults.fps = 15;
defaults.compression = 'none';
defaults.quality = 75;
defaults.clim = [0 4];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});


if ~iscell(mia)
  tmp = cell(1, numel(mia));
  for n = 1:numel(mia)
    tmp{n} = mia(n);
  end
  mia = tmp;
end

lat = [nan nan];
lon = [nan nan];
st = timestamp('bad');
et = timestamp('bad');


numel_mia = numel(mia);
for n = 1:numel_mia
  [tmplon tmplat] = info(getinstrument(mia{n}), 'fov', 'units', 'deg');
  tmplon = [min(tmplon) max(tmplon)];
  tmplat = [min(tmplat) max(tmplat)];
  lat = [min(lat(1), tmplat(1)) max(lat(2), tmplat(2))];
  lon = [min(lon(1), tmplon(1)) max(lon(2), tmplon(2))];
  st = max(st, getstarttime(mia{1}));
  et = min(st, getendtime(mia{1}));
end
if isempty(defaults.latitude)
  defaults.latitude = lat + (diff(lat) * [-.2 .2]);
end
if isempty(defaults.longitude)
  defaults.longitude = lon + (diff(lon) * [-.2 .2]);
end

% create a map. Don't allow user to set the axes.
[fh gh] = transverse_mercator_map('latitude', defaults.latitude, ...
				  'longitude', defaults.longitude, ...
				  'coastline', defaults.coastline, ...
				  varargin{unvi(1:2:end)}, ...
				  'axes', []);

if isempty(defaults.starttime)
  defaults.starttime = st;
end
if isempty(defaults.endtime)
  defaults.endtime = et;
end
if isempty(defaults.resolution)
  defaults.resolution = getresolution(mia{1});
  defaults.resolution = defaults.resolution(1);
end

if isempty(defaults.filename)
  defaults.filename = tempname;
  disp(['movie file is ' defaults.filename]);
end

mov = avifile(defaults.filename, ...
	      'fps', defaults.fps, ...
	      'compression', defaults.compression, ...
	      'quality', defaults.quality);

t = defaults.starttime;
h = cell(1, numel_mia);

while t < defaults.endtime
  t
  disp(strftime(t, 'frame time: %Y-%m-%d %H:%M:%S'));
  for n = 1:numel_mia
    h{n} = map(mia{n}, gh, 'time', t);
  end
  
  % write out frame
  F = getframe(gh);
  mov = addframe(mov, F);
  
  for n = 1:numel_mia
    H = h{n};
    delete(H(H ~= 0));
  end
  % advance to next frame
  t = t + defaults.resolution;
end

mov = close(mov);
