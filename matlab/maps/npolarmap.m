function [fh, gh] = npolarmap(varargin)
%SCAND  Draw a map of the northern polar regions.
%
%   [fh gh] = NPOLARMAP(...)
%
%   fh: FIGURE handle
%   gh: AXIS handle
%
%   NPOLARMAP uses the M_MAP toolbox. The default behaviour can be modified
%   with the following parameter name/value pairs:
%
%     'longitude', scalar DOUBLE
%     'latitude', scalar DOUBLE
%      Coordinates of the map center
%
%     'radius', scalar DOUBLE
%     The radius of the map, in degrees.
%
%     'coastline', CHAR
%     The coastline database to use. Default is M_MAP's default
%     coastline. Other valid options are 'gshhs_c', 'gshhs_l', 'gshhs_i',
%     'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate, high
%     and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     The color of the land on the map.
%
%     'sea', COLORSPEC
%     The color of the sea on the map.
%
%     'lake', COLORSPEC
%     The color of the inland lakes on the map.
%
%     'water', COLORSPEC
%     If set this overrides the color of both inland lakes and the sea.
%
%     'landedgecolor', COLORSPEC
%     The edge color of the land in the map.
%
%   See also M_MAP, COLORSPEC.

% MAP ATTRIBUTES
defaults.longitude = 0;
defaults.latitude = 90;
defaults.radius = 35;
defaults.rotangle = 0;
defaults.coastline = ''; % use an empty matrix to mark default
% defaults.land = [0 0.6 0];
defaults.land = [];
defaults.water = [];
defaults.sea = [0 0.8 1];
defaults.lake = [0 0.6 0.9];
defaults.landedgecolor = 'none';
defaults.grid = 1;
defaults.axes = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.axes)
  fh = figure;
  gh = axes('Parent', fh);
else
  gh = defaults.axes;
  axes(gh);
end

if isempty(defaults.land)
  if defaults.latitude < -60
    defaults.land = 'w';
  else
    defaults.land = [0 .6 0];
  end
end

if ~isempty(defaults.water)
  if ischar(defaults.water)
    defaults.water = text2rgb(defaults.water);
  end
  defaults.sea = defaults.water;
  defaults.lake = defaults.water;
end

if ischar(defaults.sea)
  defaults.sea = text2rgb(defaults.sea);
end
if ischar(defaults.lake)
  defaults.lake = text2rgb(defaults.lake);
end
if ischar(defaults.land)
  defaults.land = text2rgb(defaults.land);
end


set(gh, 'Color', defaults.sea);


m_proj('stereographic', ...
       'latitude', defaults.latitude, ...
       'longitude', defaults.longitude, ...
       'radius', defaults.radius, ...
       'rotangle', defaults.rotangle);

if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  if ~isequal(defaults.lake, defaults.sea) & ...
	~isequal(defaults.land, defaults.sea)
    set(findall(gh, ...
		'Tag', 'm_coast', ...
		'Type', 'patch', ...
		'FaceColor', defaults.sea), ...
	'FaceColor', defaults.lake, ...
	'EdgeColor', 'none');
  end
 
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor);
  if ~isequal(defaults.lake, defaults.sea)
    if isequal(defaults.lake, 'delete')
     for n = 2:8
       delete(findall(fh, ...
		      'Tag', ['m_' defaults.coastline], ...
		      'Type', 'patch', ...
		      'UserData', n));
     end
    else
      for n = 2:2:8
	set(findall(fh, ...
		    'Tag', ['m_' defaults.coastline], ...
		    'Type', 'patch', ...
		    'UserData', n), ...
	    'FaceColor', defaults.lake, ...
	    'EdgeColor', 'none');
      end
    end
  end
  
 case '1deg'
  % m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  
  [x y] = coastline1deg;
  for n = 1:length(x)
    x1 = x{n};
    y1 = y{n};
    if 0
      % not very succesful at doing patches
      [x2 y2] = m_ll2xy(x1, y1, 'clip', 'on');
      patch(x2, y2, 'r', ...
	    'Parent', gh, ...
	    'Tag', 'm_coast', ...
	    'FaceColor', defaults.land, ...
	    'EdgeColor', defaults.landedgecolor);
    else
      [x2 y2] = m_ll2xy(x1, y1, 'clip', 'on');
      line(x2, y2, ...
	   'Color', defaults.landedgecolor, ...
	   'Tag', 'm_coast');
    end
  end
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor);
end

% sometimes Matlab gets upset with these m_map commands
if logical(defaults.grid)
  m_grid('xtick',12,'xticklabels', '', 'tickdir','out', ...
	 'ytick',[-66.5 66.5],'yticklabels','', 'linestyle',':');
  
  deggrid = -80:10:80;
  if defaults.latitude > 0
    deggrid(deggrid <= defaults.latitude - defaults.radius) = [];
    deggrid(deggrid >= defaults.latitude) = [];
    m_grid('xtick',12,'tickdir','out','ytick', deggrid,'linest','-');
  else
    deggrid(deggrid >= defaults.latitude + defaults.radius) = [];
    deggrid(deggrid < defaults.latitude) = [];
    m_grid('xtick',12,'tickdir','out','ytick', deggrid,'linest','-', ...
	   'xticklabels', '');
  end
  % m_grid('xtick',12,'tickdir','out','ytick',[60 70 80],'linest','-');
  % m_grid('xtick',12,'tickdir','out','ytick', deggrid,'linest','-');
  % m_grid('xtick',12,'tickdir','out','ytick', deggrid,'linest','-');
end


% Fix annoying problem with Antarctic coast having a line to the pole A
% better way to do this would be to find out of the south pole appears on
% the map. If it does search all patch data for the point closest to the
% pole, and delete it. This problem probably has the same cause as the 180
% degree meridian problem described below, though the fix is somewhat
% easier.
if defaults.latitude == -90 & ~strcmp(defaults.coastline, '1deg')
  ph = findall(gh, 'Type', 'patch');
  for ph2 = ph(:)'
    xd = get(ph2, 'XData');
    yd = get(ph2, 'YData');

    % pole = (abs(xd) < eps) | (abs(yd) < eps);
    pole = (abs(xd) < 0.002) & (abs(yd) < 0.002);
    if any(pole)
      % disp('POLE!')
      size(pole)
      xd2 = xd(~pole);
      yd2 = yd(~pole);
      % set(ph2, 'EdgeColor', rand([1 3]))
      set(ph2, 'XData', xd2, 'YData', yd2);
    end
  end
end

% There is a problem with patches which cross the 180 meridian. This
% applies only to two patches (and possibly Antarctica, though that is
% solved).



% The point of the map is to add extra data, so allow that to happen
% automatically
set(gh, 'NextPlot', 'add');
