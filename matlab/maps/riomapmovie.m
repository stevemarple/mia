function riomapmovie(mia, filename, varargin)



defaults.starttime = getstarttime(mia(1));
defaults.endtime = getendtime(mia(1));
defaults.makedirectory = 0;


defaults.position = [];
% gifsicle options
defaults.delay = timespan(500,'ms');
defaults.scale = [1 1];
defaults.loopcount = 0;
defaults.optimize = 0;

[defaults unvi] = interceptprop(varargin, defaults);


res = getresolution(mia(1));

t = defaults.starttime;
tempfiles = {};



tri = [];
fh = [];
fhOld = [];
while t < defaults.endtime
  tStr = dateprintf(t);
  disp(tStr);
  fhOld = fh;
  [fh gh ph tri] = riomap(mia, 'geodesicmesh', tri, ...
			  'title', tStr, ...
			  'position', defaults.position, ...
			  varargin{unvi}, ...
			  'time', t);
  drawnow;
  % close old figure after obscured by new one - if windows must keep
  % popping up make it enjoyable to watch!
  close(fhOld);

  tempfiles{end+1} = tempname;
  % need GIF files, set explicitly
  figure2imagefile(fh, ['gif:' tempfiles{end}]);

  t = t + res;
end

close(fh);

% animate the gifs
if defaults.loopcount < 1
  defaults.loopcount = 'forever';
else
  defaults.loopcount = int2str(defaults.loopcount);
end

delay = round(defaults.delay/timespan(100,'ms'));

if defaults.optimize
  optimizeStr = sprintf(' -O%d ', fix(max(min(defaults.optimize, 2), 0)));
else 
  optimizeStr = '';
end
switch computer 
  case {'LNX86' 'GLNX86'}
    
    commandStr = sprintf(['gifsicle --delay %d --scale %dx%d ' ...
	  '--loopcount=%s %s %s > %s'], ... 
	delay, defaults.scale([1 2]), ...
	defaults.loopcount, ...
	optimizeStr, ...
	sprintf(' %s', tempfiles{:}), filename);

    disp(commandStr);
    if logical(defaults.makedirectory)
      mkdirhier(fileparts(filename));
    end
    
  otherwise
    error(['Do not know how to animate GIFs. Please add for ' ...
	  computer ' to ' mfilename]);
end
unix(commandStr);

delete(tempfiles{:});
