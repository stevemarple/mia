function r = plotcutlassbeams(ah, varargin)
%PLOTCUTLASSBEAMS  Plot the beams of CUTLASS.
%
%   h = PLOTCUTLASSBEAMS(ah, ...)
%   h: handles of LINE objects
%   ah: axis handle to plot into
%
%   PLOTCUTLASSBEAMS will plot the beams of CUTLASS onto a map. It will
%   attempt to identify if the axis contains M_MAP coastlines, and if so
%   will use the M_LINE function instead of the standard Matlab LINE
%   function.
%
%   The default behaviour can be modified with the following
%   parameter/value pairs:
%
%     'linefunction', CHAR
%     The function to call for adding LINE objects to an AXIS.
%
%     'lineargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the line
%     function. Use to modify the marker type and color.
%
%     'beams', DOUBLE
%     A vector of beam numbers to indicate which CUTLASS beams should be
%     plotted on the map. Default is all 16 beams (0:15).
% 
%     'bins', DOUBLE A vector of bin numbers to indicate which CUTLASS range
%     bins should be plotted on the map. Default is all 75 bins (0:74).
%
%   Information taken from http://ion.le.ac.uk/~ets/Finland_fov.html and
%   http://ion.le.ac.uk/~ets/Iceland_fov.html
%
%   See also CUTLASS, mia_instrument_base/PLOT, LOCATION.


r = [];

load('cutlass_f_pos.txt')
load('cutlass_i_pos.txt')


defaults.linefunction = [];  % defer
defaults.lineargs = {'LineStyle', '-', ...
		    'Marker', 'none'};

defaults.beams = 0:15;
defaults.bins = 0:74;

defaults.finland = 1;
defaults.iceland = 1;

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.linefunction)
  % does axis have any children which look they they belong to m_map?
  if ismmap(ah)
    defaults.linefunction = 'm_line';
  else
    defaults.linefunction = 'line';
  end
end
 
if defaults.finland
  for beam = defaults.beams(:)'
    lat = cutlass_f_pos(beam * 75 + defaults.bins + 1, 3);
    lon = cutlass_f_pos(beam * 75 + defaults.bins + 1, 4);
    h = feval(defaults.linefunction,  lon, lat, defaults.lineargs{:});
    r = [r; h];
  end
end

if defaults.iceland
  for beam = defaults.beams(:)'
    lat = cutlass_i_pos(beam * 75 + defaults.bins + 1, 3);
    lon = cutlass_i_pos(beam * 75 + defaults.bins + 1, 4);
    h = feval(defaults.linefunction,  lon, lat, defaults.lineargs{:});
    r = [r; h];
  end
end


