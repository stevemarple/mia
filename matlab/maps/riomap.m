function [fh, gh, ph, tri] = riomap(mia, varargin)
%RIOMAP  Plot riometer data on polar map
%
%   [fh, gh, ph, tri] = riomap(mia, ...)
%

fh = [];
gh = [];
ph = [];
tri = [];

r2d = 180/pi;

sz = size(mia);
N = prod(sz);
if ~isa(mia, 'rio_abs')
  error('mia object must be of type rio_base');
elseif N == 0
  error('mia cannot be empty');
end


% make all have same start and end times, and same resolution
st = getstarttime(mia(1));
et = getendtime(mia(1));
res = getresolution(mia(1));
for n = 2:N
  mia(n) = setresolution(mia(n), res);
  mia(n) = extract(mia(n), ...
		   'starttime', st, ...
		   'endtime', et);
end

defaults.layers = {'coastline1' 'grid' 'terminator1' 'interpdata2', ...
		   'coastline2' 'imagedata' 'terminator2' ...
		   'dataposition'};
defaults.setzdata = 0;
defaults.clim = [];
defaults.title = '';
defaults.position = [];

% map attributes
defaults.longitude = 0;
defaults.latitude = 90;
defaults.radius = 41;
defaults.rotangle = [];
defaults.coastline = ''; % use an empty matrix to mark default
defaults.land = [];
defaults.water = [0 0.8 1];
defaults.landedgecolor = [];

% terminator attributes
defaults.terminatorargs = {};

defaults.frequency = 38.2e6; % Hz
defaults.time = st;

% options for interp1
defaults.geodesicmeshargs = {'polyhedron', 'octahedron', ...
		    'recursiondepth', 4};
defaults.geodesicmesh = [];

% options for interp2
defaults.maximumdistance = 2200e3;
defaults.maximumdistance2 = 100e3;
defaults.points = 2;

defaults.rio_image = [];

defaults.visible = 1; % make figure window visible (off good for movies)
if isunix & matlabversioncmp('>=', '5.3')
  defaults.xdisplay = get(0,'DefaultFigureXDisplay');
end
% process options
[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

if isempty(defaults.land)
  if defaults.latitude > 0
    defaults.land = [0 0.6 0];
  else
    defaults.land = repmat(0.95, 1, 3);
  end
end
if isempty(defaults.landedgecolor)
  defaults.landedgecolor = defaults.land;
end

if isa(defaults.time, 'timestamp')
  ti = 1 + ((defaults.time - st) / res); % time index into data array
else
  ti = defaults.time;
  defaults.time = st + res * (ti-1);
end

if ~isempty(defaults.rio_image) 
  if ~isa(defaults.rio_image, 'rio_image')
    error('not riometer images');
  end
  % setfrequency also ensures images are of absorption
  setfrequency(defaults.rio_image, defaults.frequency);
end


% process data
mia = setfrequency(mia, defaults.frequency);

instruments = getinstrument(mia);
lat = repmat(nan, sz);
lon = repmat(nan, sz);

data = zeros(sz);
% For each instrument use the first widebeam available. If none (must be
% an imaging riometer) then take the mean of the most vertical beams.

imaging = zeros(sz);
for n = 1:N
  beams = getbeams(mia(n));
  [imagingBeams wideBeams] = info(instruments(n), 'beams');
  b = intersect(beams, wideBeams);
  if ~isempty(imagingBeams)
    imaging(n) = 1;
  end

  if ~isempty(b)
    % widebeams available
    b = b(1);
  elseif imaging(n)
    % imaging riometer
    imaging(n) = 1;
    zen = info(instruments(n), 'zenith', beams(:));
    b = beams(find(zen == min(zen)));
  else
    error('no suitable beams');
  end
  
  if length(b) == 1
    data(n) = getdata(mia(n), getparameterindex(mia(n), b), ti);
    [lon(n) lat(n)] = info(instruments(n), 'beamlocations', ...
			   'beams', b, ...
			   'units', 'deg');
  else
    % take mean data for the chosen beams
    data(n) = nonanmean(getdata(mia(n), getparameterindex(mia(n), b), ti));
    
    % take mean of cartesian coordinates
    [tmpLon tmpLat] = info(instruments(n), 'beamlocations', ...
			   'beams', b, ...
			   'units', 'deg');
    [tmpx tmpy tmpz] = sph2cart(tmpLon, tmpLat, 1);
    [lon(n) lat(n)] = cart2sph(mean(tmpx), mean(tmpy), mean(tmpz));
  end
end

lon2 = lon(~isnan(data));
lat2 = lat(~isnan(data));

vis = {'off', 'on'};
figureArgs = {'Visible', vis{1+logical(defaults.visible)}};
if isunix & matlabversioncmp('>=', '5.3')
  figureArgs{end+1} = 'XDisplay';
  figureArgs{end+1} = defaults.xdisplay;
end

if isempty(defaults.title)
  defaults.title = dateprintf(defaults.time);
end
[fh gh] = makeplotfig('init', figureArgs{:}, ...
		      'receivedraganddrop', 0, ...
		      'providedraganddrop', 0, ...
		      'title', defaults.title, ...
		      'position', defaults.position, ...
		      'footer', 1);
set(gh, 'Color', defaults.water);



[termLat termLon sunLon sunLat] = terminator(defaults.time, ...
					     defaults.terminatorargs{:});
if isempty(defaults.rotangle)
  defaults.rotangle = -90 - sunLon;
end

m_proj('stereographic', ...
       'latitude', defaults.latitude, ...
       'longitude', defaults.longitude, ...
       'radius', defaults.radius, ...
       'rotangle', defaults.rotangle);

if any(strcmp('interpdata', defaults.layers)) | ...
      any(strcmp('geodesicmesh', defaults.layers))
  % calculate interpolation coordinates
  if isempty(defaults.geodesicmesh)
    tri = geodesicmesh(defaults.geodesicmeshargs{:});
  else
    tri = defaults.geodesicmesh;
  end
  
  [xMesh yMesh zMesh face] = vertices(tri);
  [loni lati] = cart2sph(xMesh, yMesh, zMesh);
  loni = loni * r2d;
  lati = lati * r2d;

  if length(lat >= 3)
    % dataOut = sphinterp(lat, lon, data, lati, loni, 'linear');
    dataOut = sphinterp(lat(~isnan(data)), lon(~isnan(data)), ...
			data(~isnan(data)), lati, loni, 'linear');
  else
    disp('using random data');
    dataOut = rand(size(lati));
  end
  
  % convert geodesic mesh into x,y coordinates, taking into account map
  % projection
  [xMap yMap] = m_ll2xy(loni, lati, 'clip', 'point');
end

% annoyingly m_map doesn't return handles, so keep a list of m_map
% handles so that new handles can be found
%
% Tags are: 'm_coast', 'm_grid_color', 'm_grid_xgrid', 'm_grid_ygrid',
% 'm_grid_xticklabel', 'm_grid_yticklabel', 'm_grid_xticks-lower',
% 'm_grid_xticks-upper', 'm_grid_yticks-left', 'm_grid_yticks-right',
% 'm_grid_box', and 'm_grid_color'.
mhs = [];

for ln = 1:length(defaults.layers)
  h = []; % handle of items in this layer
  switch defaults.layers{ln}
    
   case 'coastline1'
    switch defaults.coastline
     case 'default'
      m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
      
     case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
      feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	    'EdgeColor', defaults.landedgecolor);
      
     otherwise
      m_usercoast(defaults.coastline, 'patch', defaults.land, ...
		  'EdgeColor', defaults.landedgecolor);
    end

    [mhs h] = localFindNewMMapHandles(gh, mhs, 'm_coast');
    
    % ------------------------------------
   case 'coastline2'
    switch defaults.coastline
     case 'default'
      m_coast('line', 'Color', defaults.landedgecolor);
      
     case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
      feval(['m_' defaults.coastline], 'line', ...
	    'Color', defaults.landedgecolor);
      
     otherwise
      m_usercoast(defaults.coastline, 'line', ...
		  'Color', defaults.landedgecolor);
    end

    [mhs h] = localFindNewMMapHandles(gh, mhs, 'm_coast');
    
    % ------------------------------------
   case 'grid'	
    if defaults.latitude > 0
      % artic circle
      m_grid('xtick',12,'xticklabels', '', 'tickdir','out', ...
	     'ytick',[-66.5 66.5],'yticklabels','', 'linestyle',':');
      % grid lines
      m_grid('xtick',12,'tickdir','out','ytick',[60 70 80],'linest','-');
      
    else
      % antarctic circle
      m_grid('xtick',12,'xticklabels', '', 'tickdir','out', ...
	     'ytick',[-66.5 66.5],'yticklabels','', 'linestyle',':');
      % grid lines
      m_grid('xtick',12,'tickdir','out','ytick',[-60 -70 -80],'linest','-');
    end      
    [mhs tmp1] = ...
	localFindNewMMapHandles(gh, mhs, 'm_grid_xgrid', ...
				'm_grid_ygrid', ...
				'm_grid_xticklabel', ...
				'm_grid_xticklabels', ...
				'm_grid_yticklabel', ...
				'm_grid_yticklabels', ...
				'm_grid_xticks-lower', ...
				'm_grid_xticks-upper', ...
				'm_grid_yticks-left', ...
				'm_grid_yticks-right', 'm_grid_box');
    [mhs tmp2] = localFindNewMMapHandles(gh, mhs, 'm_grid_color');
    
    if defaults.setzdata
      % set here because they need setting to different heights
     localSetZData(tmp1, ln);
     localSetZData(tmp2, -0.5);
    end
    h = [tmp1; tmp2];
    
    % ------------------------------------
   case 'cgmgrid'
    clon = [0:10:350 0];
    clat = [50; 60; 70; 80];
    [clon2 clat2] = meshgrid(clon, clat);
    [glon glat] = cgm2geo('latitude', clat2, 'longitude', clon2);
    for n = 1:length(clat)
      h(n) = m_line(glon(n, :), glat(n, :), ...
		    'ZData', repmat(ln, size(glon(n, :))), ...
		    'Linestyle', '-', ...
		    'Marker', 'none', ...
		    'Color', [0.2 0.2 0.2], ...
		    'clip', 'patch');
    end
    % do cross at magnetic pole
    [glon glat] = cgm2geo('latitude', 90, 'longitude', 0);
    h(end+1) = m_line(glon, glat, ...
		      'ZData', ln, ...
		      'Linestyle', 'none', ...
		      'Marker', '+', ...
		      'Color', 'k', ...
		      'clip', 'point');
    % ------------------------------------
   case 'geodesicmesh'
    h = line(xMap(~isnan(xMap)), yMap(~isnan(xMap)), ...
	     'Linestyle', 'none', ...
	     'Marker', '.', ...
	     'MarkerSize', 2, ...
	     'Color', 'y');
    
    % ------------------------------------
   case 'interpdata'
 
    validFaces = find(~sum(isnan(xMap(face)) | isnan(dataOut(face)), 2));
    
    h = zeros(length(validFaces), 1);
    hn = 1;
    for n = validFaces(:)'
      if all(~isnan(xMap(face(n, :)))) & ...
	    all(~isnan(yMap(face(n, :)))) & ...
	    all(~isnan(dataOut(face(n, :))))
	% line(x(face(n, :)), y(face(n, :)), 'Color', 'c');
	
	if matlabversioncmp('<=', '5.1')
	  ec = 'interp';
	else
	  ec = 'none';
	end
	h(hn) = patch('Parent', gh, ...
		      'XData', xMap(face(n, :)), ...
		      'YData', yMap(face(n, :)), ...
		      'CData', dataOut(face(n, :)), ...
		      'CDataMapping', 'scaled', ...
		      'FaceColor', 'interp', ...
		      'EdgeColor', ec);
	hn = hn + 1;
      end
    end

    % ------------------------------------
   case 'interpdata2'
    % interpolation by using delaunay triangles
    h = [];
    lon2 = lon(~isnan(data));
    lat2 = lat(~isnan(data));
    data2 = data(~isnan(data));
    
    if length(lat2) >= 3
      dtri = sphdelaunay('latitude', lat2, 'longitude', lon2);
    else
      warning('insufficient data to interpolate');
      dtri = [];
    end
    for n = 1:size(dtri, 1)
      dist = m_lldist(lon2(dtri(n, [1 2 3 1])), lat2(dtri(n, [1 2 3 1])));
      if max(dist) <= defaults.maximumdistance
	[lonSide1 latSide1 dataSide1] = ...
	    greatcirclepath(lon2(dtri(n, 1)), lat2(dtri(n, 1)), ...
			    lon2(dtri(n, 2)), lat2(dtri(n, 2)), ...
			    'data', data2(dtri(n, [1 2])), ...
			    'points', defaults.points, ...
			    'distance', defaults.maximumdistance2);
	[lonSide2 latSide2 dataSide2] = ...
	    greatcirclepath(lon2(dtri(n, 2)), lat2(dtri(n, 2)), ...
			    lon2(dtri(n, 3)), lat2(dtri(n, 3)), ...
			    'data', data2(dtri(n, [2 3])), ...
			    'points', defaults.points, ...
			    'distance', defaults.maximumdistance2);
	[lonSide3 latSide3 dataSide3] = ...
	    greatcirclepath(lon2(dtri(n, 3)), lat2(dtri(n, 3)), ...
			    lon2(dtri(n, 1)), lat2(dtri(n, 1)), ...
			    'data', data2(dtri(n, [3 1])), ...
			    'points', defaults.points, ...
			    'distance', defaults.maximumdistance2);
	lond = [lonSide1(2:end) lonSide2(2:end) lonSide3(2:end)];
	latd = [latSide1(2:end) latSide2(2:end) latSide3(2:end)];
	[xdata ydata] = m_ll2xy(lond, latd, 'clip', 'on');
	cdata = [dataSide1(2:end) dataSide2(2:end) dataSide3(2:end)];
	
	if all(~isnan(xdata))
	  h(end+1) = patch('Parent', gh, ...
			   'XData', xdata, ...
			   'YData', ydata, ...
			   'CData', cdata, ...
			   'FaceColor', 'interp', ...
			   'EdgeColor', 'none');
	end
      end
    end
    
    % ------------------------------------
   case 'delaunay'
    if length(lat2) >= 3
      dtri = sphdelaunay('latitude', lat2, 'longitude', lon2);
    else
      warning('insufficient data to interpolate');
      dtri = [];
    end
    for n = 1:size(dtri, 1)
      [lonSide1 latSide1] = ...
	  greatcirclepath(lon2(dtri(n, 1)), lat2(dtri(n, 1)), ...
			  lon2(dtri(n, 2)), lat2(dtri(n, 2)), ...
			  'points', defaults.points, ...
			  'distance', defaults.maximumdistance2);
      [lonSide2 latSide2] = ...
	  greatcirclepath(lon2(dtri(n, 2)), lat2(dtri(n, 2)), ...
			  lon2(dtri(n, 3)), lat2(dtri(n, 3)), ...
			  'points', defaults.points, ...
			  'distance', defaults.maximumdistance2);
      [lonSide3 latSide3] = ...
	  greatcirclepath(lon2(dtri(n, 3)), lat2(dtri(n, 3)), ...
			  lon2(dtri(n, 1)), lat2(dtri(n, 1)), ...
			  'points', defaults.points, ...
			  'distance', defaults.maximumdistance2);
      lond = [lonSide1(2:end) lonSide2(2:end) lonSide3(2:end)];
      latd = [latSide1(2:end) latSide2(2:end) latSide3(2:end)];
      [xdata ydata] = m_ll2xy(lond, latd, 'clip', 'on');

      if all(~isnan(xdata))
	h(end+1) = patch('Parent', gh, ...
			 'XData', xdata, ...
			 'YData', ydata, ...
			 'FaceColor', 'none', ...
			 'EdgeColor', 'm', ...
			 'Tag', 'delaunay');
      end
    end

    % ------------------------------------
   case 'dataposition'
    % mark positions of widebeams
    h = m_line(lon(~imaging), lat(~imaging), ...
	       'Linestyle', 'none', ...
	       'Marker', '+', ...
	       'Color', 'r');
    
    % ------------------------------------
   case 'widebeamdata'
    h = [];
    for n = 1:N
      [imagingBeams wideBeams] = info(instruments(n), 'beams');
      wb = intersect(getbeams(mia(n)), wideBeams);
      if ~isempty(wb)
	% plotbeamprojection(instruments(n), gh, 
	tmph = map(mia(n), gh, ...
		   'time', ti, ...
		   'beams', wb);
	if isnan(data(n))
	  set(tmph, 'FaceColor', 'w');
	end
	set(tmph, 'Tag', sprintf('wb: %s_%d', ...
				 getabbreviation(instruments(n)), ...
				 getserialnumber(instruments(n))));
	h = [h; tmph];
      end
    end
    set(h, 'EdgeColor', 'k');
    
    % ------------------------------------
   case 'imagedata'
    % add images from imaging riometers for extra detail
    h = [];
    for n = 1:prod(size(defaults.rio_image))
      tmph = map(defaults.rio_image(n), gh, 'time', ti);
      h = [h; tmph];
    end

    % ------------------------------------
   case 'terminator1'
    h = plotterminator(defaults.time, gh, 'style', 'patch', ...
		       defaults.terminatorargs{:});
    
    % ------------------------------------
   case 'terminator2'
    h = plotterminator(defaults.time, gh, 'style', 'line', ...
		       defaults.terminatorargs{:});
	
    % ------------------------------------
   case 'terminator3'
    h = plotterminator(defaults.time, gh, 'style', 'transpatch', ...
		       defaults.terminatorargs{:});

    % ------------------------------------
   otherwise
    warning(sprintf('unknown layer (was %s)', defaults.layers{ln}));
    
  end
  
  % ------------------------------------------------------------------------
  % arrange heights
  if defaults.setzdata
    localSetZData(h, ln);
  end
  
  % remember object handles added in this layer
  if isfield(ph, defaults.layers{ln})
    % allow for same layer name to be specified multiple times
    phf = getfield(ph, defaults.layers{ln});
    if iscell(phf)
      phf{end+1} = h;
    else
      phf = {phf, h};
    end
    ph = setfield(ph, defaults.layers{ln}, phf);
  else
    ph = setfield(ph, defaults.layers{ln}, h);
  end
  
end

if ~isempty(defaults.clim)
  clim = defaults.clim;
  set(gh, 'CLim', clim);
else
  clim = get(gh, 'CLim');
end
makeplotfig('addcolorbar', fh, ...
	    'clim', clim, ...
	    'xlim', clim, ...
	    'title', datalabel(mia(1)), ...
	    'shading', 'flat');
set(fh ,'Pointer', 'arrow');


% =======================================================================
function [sNew, hNew] = localFindNewMMapHandles(ah, s, varargin)

sNew = s;
hNew = [];

for n = 1:length(varargin)
  tag = varargin{n};
  % m_map has an inconsistency between m_grid_xticklabel and
  % m_grid_yticklabels. Map plural form to singular for future proofing,
  % but loo for both forms

  switch tag
   case {'m_grid_xticklabel' 'm_grid_xticklabels'}
    
    hAll = [findall(ah, 'Tag', 'm_grid_xticklabel');
	    findall(ah, 'Tag', 'm_grid_xticklabels')];
    tag = 'm_grid_xticklabel';
    
   case {'m_grid_yticklabel' 'm_grid_yticklabels'}
    
    hAll = [findall(ah, 'Tag', 'm_grid_yticklabel');
	    findall(ah, 'Tag', 'm_grid_yticklabels')];
    tag = 'm_grid_yticklabel';
    
   otherwise
    hAll = findall(ah, 'Tag', tag);
  end

  % some tags use '-', map to and underscore because matlab won't allow '-'
  % as part of a field name  
  fn = strrep(tag, '-', '_');
  
  if ~isfield(s, fn)
    s = setfield(s, fn, []); % initialise as needed to empty matrices
  end
  hOld = getfield(s, fn);
  hNew = [hNew; setdiff(hAll, hOld)];
  sNew = setfield(sNew, fn, hAll);
end
return

% =======================================================================
function localSetZData(h, z)
for n = 1:length(h)
  p = get(h(n));
  switch p.Type
   case 'text'
    % has Z dimension in position matrix
    p.Position(3) = z;
    set(h(n), 'Position', p.Position);
    
   case {'image' 'light' 'rectangle'}
    ; % no ZData
   
   case {'line' 'patch' 'surface'}
    % has ZData
    if isempty(p.ZData)
      set(h(n), 'ZData', repmat(z, size(p.XData)));
    end
   otherwise
    
    error(sprintf('unknown UI type, modify %s to handle %s', ...
		  get(h(n), 'Type'), mfilename));
  end
end
return      

