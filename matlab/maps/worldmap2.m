function [fh, gh] = worldmap2(varargin)
%WORLDMAP2  Draw a map of the world.
%
%   [fh gh] = WORLDMAP2
%   [fh gh] = WORLDMAP2(...)
%
%   fh: FIGURE handle
%   gh: AXIS handle
%
%   WORLDMAP2 uses the M_MAP toolbox. The default behaviour can be modified
%   with the following parameter name/value pairs:
%
%     'longitude', 1x2 DOUBLE
%      The lower and upper longitude limits, respectively.
%
%     'latitude', 1x2 DOUBLE
%      The lower and upper latitude limits, respectively.
%
%     'coastline', CHAR
%     The coastline database to use. Default is M_MAP's default
%     coastline. Other valid options are 'gshhs_c', 'gshhs_l', 'gshhs_i',
%     'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate, high
%     and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     The color of the land in the map.
%
%     'water', COLORSPEC
%     The color of the land in the map.
%
%     'landedgecolor', COLORSPEC
%     The edge color of the land in the map.
%
%   See also M_MAP, COLORSPEC.

% MAP ATTRIBUTES
defaults.longitude = [20.79];
defaults.latitude = [80];
defaults.coastline = ''; % use an empty matrix to mark default
defaults.land = [0 0.6 0];
defaults.water = [0 0.8 1];
defaults.landedgecolor = 'none';

defaults.antarctica = [];
[defaults unvi] = interceptprop(varargin, defaults);
fh = figure;
gh = axes('Color', defaults.water);

m_proj('miller','latitude', defaults.latitude, ...
       'longitude', defaults.longitude);


if isempty(defaults.coastline)
  defaults.coastline = 'default'; 
end

switch defaults.coastline
 case 'default'
  m_coast('patch', defaults.land, 'EdgeColor', defaults.landedgecolor);
  
 case {'gshhs_c', 'gshhs_l', 'gshhs_i', 'gshhs_h', 'gshhs_f'}
  feval(['m_' defaults.coastline], 'patch', defaults.land, ...
	'EdgeColor', defaults.landedgecolor);
  
 otherwise
  m_usercoast(defaults.coastline, 'patch', defaults.land, ...
	      'EdgeColor', defaults.landedgecolor);
end

if ~isempty(defaults.antarctica)
  [x y] = m_ll2xy(0, -60);
  h = findall(gh, 'Type', 'patch', 'Tag', 'm_coast');
  for n = 1:length(h)
    if all(get(h(n), 'YData') < y)
      set(h(n), 'FaceColor', defaults.antarctica);
    end
  end
end

% m_grid('linest','-','xticklabels',[],'yticklabels',[]);
m_grid('linestyle', 'none', ...
       'box', 'on', ...
       'tickdir', 'out', ...
       'backcolor', defaults.water);

% The point of the map is to add extra data, so allow that to happen
% automatically
set(gh, 'NextPlot', 'add');
