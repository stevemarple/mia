function [x, y] = coastline1deg(h)

if nargin == 0
  h = 1;
end;

load topo;
c = contourc(topo, [h h]);

x = {};
y = {};

idx = 1;
while idx < size(c, 2)
  hh = c(1, idx);
  n = c(2, idx);
  x{end+1} = c(1, (idx+1):(idx+n));
  y{end+1} = c(2, (idx+1):(idx+n)) - 90.5;
  idx = idx + n + 1;
end
