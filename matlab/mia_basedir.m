function r = mia_basedir
%MIA_BASEDIR  Return the name of the MIA base directory.
%
%   r = MIA_BASEDIR;
%
%   See also MIA_DATADIR.

env = 'MIA_BASEDIR';
r = getenv(env);
if isempty(r)
  % Compute from location of this file
  r = fileparts(which(mfilename));
end


