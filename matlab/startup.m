function startup

% Don't have the path for matlabversion yet, and haven't set up any
% compatibility function to avoid the persistent keyword. Define ourselves.
MV = sscanf(version, '%f',1);


local_dir_env = 'MIA_LOCALDIR';

% get custom directory into path ASAP. Later it will be moved back to the
% top of the search list
addpath(mia_basedir);
addpath(fullfile(mia_basedir, 'mia'));
personal_dir = mia_personaldir;
custom_dir = fullfile(personal_dir, 'custom');
if isdir(custom_dir)
  addpath(custom_dir);
end

% List of directories which should not be added to the path. Some should not
% be added at all and others are special cases which have already been added
% or will be add later.
ignore_dirs = {'.' '..' '.svn' 'admin' 'bin' 'deprecated' 'local' ...
	      'manual' 'mia', 'system'};
d = dir(mia_basedir);

for n = 1:length(d)
  dn = d(n);
  if dn.isdir & ~any(strcmp(dn.name, ignore_dirs))
    addpath(fullfile(mia_basedir, dn.name));
  end
end

if isdir(fullfile(mia_basedir, 'deprecated'))
  addpath(fullfile(mia_basedir, 'deprecated'), '-end');
end

if ismatlab
  % Set up some paths for compatiblity with all versions of matlab
  compatDirs = {fullfile(mia_basedir, 'compat'), ...
		fullfile(mia_personaldir, 'custom', 'compat')};
  compatVersions = [6.1 6.5 7.0 7.1 7.2];
  for cpn = 1:length(compatDirs)
    for cv = compatVersions
      cp = fullfile(compatDirs{cpn}, sprintf('%.1fle', cv));
      if isdir(cp) & MV <= cv
	addpath(cp);
      end
      cp = fullfile(compatDirs{cpn}, sprintf('%.1fe', cv));
      if isdir(cp) & MV == cv
	addpath(cp);
      end
    end
  end
end

% Add compatibility with old toolkit, and old data files.
addpath(fullfile(mia_basedir, 'compat', 'iristool'), '-end');

% Internationalisation attempts using an extended char class have been
% removed, but old data files remain. Include compatibility functions to
% convert such strings to ASCII.
addpath(fullfile(mia_basedir, 'compat', 'i18n_char'), '-end');

% system (local) customisations
local_dir = getenv(local_dir_env); % use environment variable if set
if isempty(local_dir)
  try 
    local_dir = mia_localdir;
  catch
    ;
  end
end

if ~isempty(local_dir)
  % absolutepath was not in context, must use feval
  local_dir = feval('absolutepath', local_dir);

  if isdir(local_dir)
    disp(sprintf('Adding local directory to path (%s)', local_dir));
    addpath(local_dir);
    localStartup = fullfile(local_dir, 'startup');
    
    % have to cd out of the base directory otherwise the wrong startup file is
    % used
    if exist([localStartup '.m']) | exist([localStartup '.p'])
      disp('Running local system startup script');
      cd(local_dir);
      
      % local startup file could be a script, so isolate it by running in
      % base environment
      evalin('base', 'startup');
      cd(mia_basedir);
    end
  end
else
  disp(sprintf('%s not set, local customisations not enabled', local_dir_env));
end

% initialise MIA for new users by creating MIA and MIA/custom
% directories. Also generates userdetails.m if it does not exist. 
mia_firstuse(false); % set remake to false

% get out of the mia system directory (if possible) so that we can run
% the custom startup script
cd(mia_personaldir);

% Put user's custom directory at the front of the path.
if isdir(custom_dir)
  addpath(custom_dir);
end

custom_startup = fullfile(personal_dir, 'custom', 'startup');
if strcmp(which('startup'), [custom_startup '.m']) | ...
      strcmp(which('startup'), [custom_startup '.p'])

  disp('Running user''s custom startup script');

  % user's file could be a script, so isolate it by running in base
  % environment
  evalin('base', 'startup');
end
