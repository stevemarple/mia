function r = triangle(varargin)

r.x = [nan nan nan];
r.y = [nan nan nan];
r.z = [nan nan nan];

r = class(r, 'triangle');

if nargin == 0
  return;
  
elseif nargin == 1 & isa(varargin{1}, 'triangle')
  % copy constructor
  r.x = varargin{1}.x;
  r.y = varargin{1}.y;
  r.z = varargin{1}.z;
  
elseif nargin == 3
  xyz = 'xyz';
  for n = 1:3
    if prod(size(varargin{n})) ~= 3
      error(sprintf('vertex %d must have 3 coordinates', n));
    end
    r.x(n) = varargin{n}(1);
    r.y(n) = varargin{n}(2);
    r.z(n) = varargin{n}(3);
  end
  
else
  error('incorrect parameters');
end
