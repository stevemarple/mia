function [x, y, z] = approxcenter(t)
%APPROXCENTER Return the approximate center of the triangle(s).
%
%   The approximate center is taken to be the mean of the 3
%   vertices. This is exact for equilateral triangles. For geodesic domes
%   where the triangles are approximately equilateral this is a
%   reasonable approximation to the center.

num = prod(size(t));
r = zeros(size(t));

for tn = 1:num
  x(tn) = mean(t(tn).x);
  y(tn) = mean(t(tn).y);
  z(tn) = mean(t(tn).z);

end

