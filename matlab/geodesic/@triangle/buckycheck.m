function len = buckycheck(t)


num = prod(size(t));

% check the vertices are on a sphere of unit radius
tolerance = 1e-12;
for tn = 1:num
  for n = 1:3
    radius = sqrt(power(t(tn).x(n), 2) + power(t(tn).y(n), 2) + ...
		  power(t(tn).z(n), 2));
    if radius < 1 - tolerance | radius > 1 + tolerance
      error(sprintf(['vertex %d of triangle %d not a sphere of unit' ...
		     ' radius (r=%g)'], n, tn, radius));
    end
  end
end

comb = [1 2;
	1 3;
	2 3];

% calculate the lengths of each edge
len = zeros(3 * num, 1);
for tn = 1:num
  for n = 1:3
    len((3*(tn-1)) + n) = ...
	sqrt(power(t(tn).x(comb(n,1)) - t(tn).x(comb(n,2)), 2) + ...
	     power(t(tn).y(comb(n,1)) - t(tn).y(comb(n,2)), 2) + ...
	     power(t(tn).z(comb(n,1)) - t(tn).z(comb(n,2)), 2));
  end
end
