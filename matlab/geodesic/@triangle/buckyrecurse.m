function r = buckyrecurse(t)

% check the vertices are on a sphere of unit radius
num = prod(size(t));

if 1
  tolerance = 1e-12;
  for tn = 1:num
    for n = 1:3
      radius = sqrt(power(t(tn).x(n), 2) + power(t(tn).y(n), 2) + ...
		    power(t(tn).z(n), 2));
      if radius < 1 - tolerance | radius > 1 + tolerance
	error(sprintf(['vertex %d of triangle %d not a sphere of unit' ...
		       ' radius (r=%g)'], n, tn, radius));
      end
    end
  end
end

% For every triangle break it into 4 triangles
% Consider one triangle:
%
%           V1
%           /\ 
%          /  \ 
%         /    \
%        /      \
%       /        \
%      /          \
%     /            \
% V2 /______________\ V3
%
% Break into:
%
%           V1
%           /\ 
%          /  \ 
%         / a  \
%     V4 /______\ V6
%       /\      /\
%      /  \ d  /  \
%     / b  \  / c  \
% V2 /______\/______\ V3
%
%           V5
%
% V4, V5 and V6 are just the midpoints (ie mean values) of (V1,V2), (V2,V3)
% and (V1,V3). They must be normalized back onto a sphere of unit radius.
%

V = cell(6, 1);
% r = zeros(4 * num, 1);
% r = [];

for tn = num:-1:1 
  tmp = vertices(t(tn));
  tmp = tmp{1};
  for n = 1:3
    V{n} = tmp{n};
  end
  
  V{4} = (V{1} + V{2}) / 2;
  V{5} = (V{2} + V{3}) / 2;
  V{6} = (V{1} + V{3}) / 2;
  
  xyz = [V{4}; V{5}; V{6}];
  x = xyz(:, 1);
  y = xyz(:, 2);
  z = xyz(:, 3);
  
  % map V4, V5 and V6 onto sphere of unit radius
  [th phi radius] = cart2sph(x, y, z);
  radius = ones(size(radius));
  [x, y, z] = sph2cart(th, phi, radius);
  for n = 1:3
    V{3+n} = [x(n) y(n) z(n)];
  end
  
  r(4*(tn-1) + [1:4]) = [ ...
      triangle(V{[1 4 6]}); 
      triangle(V{[4 2 5]});
      triangle(V{[6 5 3]});
      triangle(V{[4 5 6]}) ];
  
end
