function r = area(t)
num = prod(size(t));

% Heron's formula:
% Let a,b,c be the sides of a triangle, and let A be the area of the
% triangle.  Heron's formula states that A^2 = s(s-a)(s-b)(s-c), where s =
% (a+b+c)/2.

r = zeros(size(t));
for tn = 1:num

  V = vertices(t(tn)); % V is cell array of size 1, since t(tn) is scalar
  a = localDist(V{1}{1}, V{1}{2});
  b = localDist(V{1}{2}, V{1}{3});
  c = localDist(V{1}{1}, V{1}{3});
  s = (a+b+c)/2;
  
  r(tn) = sqrt(s*(s-a)*(s-b)*(s-c));
end

function r = localDist(p1, p2)
r = sqrt(sum(power(p1 - p2, 2)));
