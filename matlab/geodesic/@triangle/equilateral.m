function r = equilateral(t)
num = prod(size(t));

r = zeros(size(t));
for tn = 1:num
  V = vertices(t(tn)); % V is cell array of size 1, since t(tn) is scalar
  a = localDist(V{1}{1}, V{1}{2});
  b = localDist(V{1}{2}, V{1}{3});
  c = localDist(V{1}{1}, V{1}{3});
  tol = 1e-12;
  r(tn) = (a >= b - tol) & (a <= b + tol) & (b >= c - tol) & (b <= c + tol);
end

function r = localDist(p1, p2)
r = sqrt(sum(power(p1 - p2, 2)));
