function varargout = vertices(t)

sz = size(t);
num = prod(sz);

if nargout == 0 | nargout == 1
  r = cell(size(t));
  tmp = cell(1, 3);
  for tn = 1:num
    for n = 1:3
      tmp{n} = [t(tn).x(n) t(tn).y(n) t(tn).z(n)];
    end
    r{tn} = tmp;
  end
  varargout{1} = r;
  return

elseif nargout == 3 | nargout == 4

  xyz = zeros(3*num, 3);

  xyz(:, 1) = [t.x]';
  xyz(:, 2) = [t.y]';
  xyz(:, 3) = [t.z]';

  [xyz idx1 idx2] = unique(xyz, 'rows');
  for n = 1:3
    varargout{n} = xyz(:, n);
  end
  
  %   if nargout == 4
  %     % calculate vertices for used each face
  %     face = zeros(num, 3);
  %     for n = 1:num
  %       for m = 1:3
  % 	face(n, m) = find(xyz(:, 1) == t(n).x(m) & ...
  % 			  xyz(:, 2) == t(n).y(m) & ...
  % 			  xyz(:, 3) == t(n).z(m));
  %       end
  %     end
  %     varargout{4} = face;
  %   end

  if nargout == 4
    varargout{4} = reshape(idx2, [3 num])';
  end
  
else
  error('incorrect number of outputs');
end
