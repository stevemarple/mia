function varargout = patch(t, varargin)

if nargin >= 2 & ishandle(varargin{1})
  h = varargin{1};
  varargin = {varargin{2:length(varargin)}};
else
  h = gca;
end

defaults.edgecolor = get(h, 'DefaultPatchEdgeColor');
% defaults.facecolor = get(h, 'DefaultPatchFaceColor');
defaults.facecolor = 'y';

[defaults unvi] = interceptprop(varargin, defaults);

num = prod(size(t));
r = zeros(num, 1);
for n = 1:num
  r(n) = patch('XData', t(n).x, ...
		 'YData', t(n).y, ...
		 'ZData', t(n).z, ...
		 'Parent', h, ...
		 'FaceColor', defaults.facecolor, ...
		 'EdgeColor', defaults.edgecolor);
end


if nargout >= 1
  varargout{1} = r;
end
