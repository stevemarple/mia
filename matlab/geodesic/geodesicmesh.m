function r = geodesicmesh(varargin)

defaults.polyhedron = 'octahedron';
defaults.recursiondepth = 3;

[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(unvi)
  warning(['The following parameter names were not recognised: ' ...
	   sprintf('%s ', varargin{unvi(:)})]);
end

switch defaults.polyhedron
 case 'tetrahedron'
  rt3 = sqrt(3);
  tri = [ ...
      triangle([+1 +1 +1]/rt3, [-1 +1 -1]/rt3, [+1 -1 -1]/rt3)
      triangle([-1 +1 -1]/rt3, [-1 -1 +1]/rt3, [+1 -1 -1]/rt3)
      triangle([+1 +1 +1]/rt3, [+1 -1 -1]/rt3, [-1 -1 +1]/rt3)
      triangle([+1 +1 +1]/rt3, [-1 -1 +1]/rt3, [-1 +1 -1]/rt3)
      ];
  
 case 'octahedron'
  tri = [ ...
      triangle([0 0 +1], [+1 0 0], [0 +1 0])
      triangle([0 0 +1], [+1 0 0], [0 -1 0])
      triangle([0 0 +1], [-1 0 0], [0 +1 0])
      triangle([0 0 +1], [-1 0 0], [0 -1 0])
      triangle([0 0 -1], [+1 0 0], [0 +1 0])
      triangle([0 0 -1], [+1 0 0], [0 -1 0])
      triangle([0 0 -1], [-1 0 0], [0 +1 0])
      triangle([0 0 -1], [-1 0 0], [0 -1 0])
	];
 
 case 'icosahedron'
  phi = (1 + sqrt(5)) / 2; % so-called "golden ratio"
  a = 1 / 2;
  b = 1 / (2 * phi);

  % a and b are for a unit volume, we want points on a unit sphere. All
  % vertices contain an a, b and zero
  c = sqrt(power(a,2) + power(b,2));
  a = a / c;
  b = b / c;
  
  tri = [ ...
      triangle([ 0 +b -a], [+b +a  0], [-b +a  0])
      triangle([ 0 +b +a], [-b +a  0], [+b +a  0])
      triangle([ 0 +b +a], [ 0 -b +a], [-a  0 +b])
      triangle([ 0 +b +a], [+a  0 +b], [ 0 -b +a])
      triangle([ 0 +b -a], [ 0 -b -a], [+a  0 -b])
      triangle([ 0 +b -a], [-a  0 -b], [ 0 -b -a])
      triangle([ 0 -b +a], [+b -a  0], [-b -a  0])
      triangle([ 0 -b -a], [-b -a  0], [+b -a  0])
      triangle([-b +a  0], [-a  0 +b], [-a  0 -b])
      triangle([-b -a  0], [-a  0 -b], [-a  0 +b])
      triangle([+b +a  0], [+a  0 -b], [+a  0 +b])
      triangle([+b -a  0], [+a  0 +b], [+a  0 -b])
      triangle([ 0 +b +a], [-a  0 +b], [-b +a  0])
      triangle([ 0 +b +a], [+b +a  0], [+a  0 +b])
      triangle([ 0 +b -a], [-b +a  0], [-a  0 -b])
      triangle([ 0 +b -a], [+a  0 -b], [+b +a  0])
      triangle([ 0 -b -a], [-a  0 -b], [-b -a  0])
      triangle([ 0 -b -a], [+b -a  0], [+a  0 -b])
      triangle([ 0 -b +a], [-b -a  0], [-a  0 +b])
      triangle([ 0 -b +a], [+a  0 +b], [+b -a  0])
      ];
  
 otherwise
  error('Unknown polyhedron');
  
end
  
for n = 1:defaults.recursiondepth
  tri = buckyrecurse(tri);
end

r = tri;
