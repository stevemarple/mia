function r = gettype(in, varargin)
%GETTYPE  Return instrument type.
%
%   r = GETTYPE(r)
%   r: CHAR
%   in: HEATER object
%
%   See also MIA_INSTRUMENT_BASE, HEATER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'heater';
  
 case 'u'
  r = 'HEATER';
  
 case 'c'
  r = 'Heater';
end

return


