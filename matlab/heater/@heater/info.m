function varargout = info(heater, inform, varargin)
%INFO  Return HEATER information about a specific heater.
%
%   See also HEATER, HEATER_DATA.


narg = nargout;
if ~narg 
  % not sensible to have zero output arguments, give one return value for
  % the case of the implicit 'ans' output
  narg = 1; 
end

% inform should not contain spaces - may want to use parameter name/value
% pairs, interceptprop uses structures and their fieldnames cannot contain
% spaces.


switch char(inform)
 case 'defaultfilename'
  varargout{1} = fullfile(mia_datadir, 'heater', getabbreviation(heater), ...
			  '%Y', '%Y%m%d.mat');
  varargout{2} = timespan(1,'d');
  varargout{3} = 0; % not an error if file missing
  
 otherwise
  error('unknown information request');
  
end
  
  
  

