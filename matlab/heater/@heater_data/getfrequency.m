function r = getfrequency(mia)
%GETFREQUENCY  Return the transmitter frequency of HEATER_DATA object(s).
%
%   r = GETFREQUENCY(mia)
%   r: frequency
%   mia: HEATER_DATA object(s)
%
%   If mia is scalar then r will a [2xn] matrix where n is the number of
%   on/off times. If mia is non-scalar then r will be a CELL array of [2xn]
%   matrices.
%
%   See also HEATER_DATA.

if length(mia) == 1
  r = mia.frequency;
else
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = mia(n).frequency;
  end
end
