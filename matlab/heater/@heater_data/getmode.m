function r = getmode(mia)
%GETMODE  Return the transmitter mode of HEATER_DATA object(s).
%
%   r = GETMODE(mia)
%   r: mode
%   mia: HEATER_DATA object(s)
%
%   If mia is scalar then r will a [2xn] CELL matrix where n is the number
%   of on/off times. If mia is non-scalar then r will be a CELL array of
%   [2xn] CELL matrices.
%
%   See also HEATER_DATA.

if length(mia) == 1
  r = mia.mode;
else
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = mia(n).mode;
  end
end
