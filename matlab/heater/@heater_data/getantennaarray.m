function r = getantennaarray(mia)
%GETANTENNAARRAY  Return the transmitter antennaarray of HEATER_DATA object(s).
%
%   r = GETANTENNAARRAY(mia)
%   r: antennaarray
%   mia: HEATER_DATA object(s)
%
%   If mia is scalar then r will a [2xn] matrix where n is the number of
%   on/off times. If mia is non-scalar then r will be a CELL array of [2xn]
%   matrices.
%
%   See also HEATER_DATA.

if length(mia) == 1
  r = mia.antennaarray;
else
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = mia(n).antennaarray;
  end
end
