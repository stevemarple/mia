function r = setontimes(mia, a)
%GETONTIMES  Return the transmitter ontimes of HEATER_DATA object(s).
%
%   r = GETONTIMES(mia, a)
%   r: modified HEATER_DATA object(s)
%   mia: HEATER_DATA object(s)
%   a: ontimes (TIMESTAMP)
%
%   See also HEATER_DATA.

if length(mia) ~= 1
  if ~iscell(a)
    a = {a};
  end
  if length(a) == 1
    a = repmat(a, size(mia));
  elseif ~isequal(size(mia), size(a))
    error('a must be a CELL array the same size as mia');
  end

  r = mia;
  for n = 1:prod(size(mia))
    r(n) = setontimes(mia, a{n});
  end
  return;
end

% mia is scalar below here
r = mia

if ~isa(a, 'timestamp') | (length(a) & size(a, 1) > 1)
  error('on times must be a timestamp vector');
end
r.ontimes = a;

