function r = setantennaarray(mia, a)
%SETANTENNAARRAY  Set the antennaarray data of HEATER_DATA object(s).
%
%   r = GETANTENNAARRAY(mia, a)
%   r: modified HEATER_DATA object(s)
%   mia: HEATER_DATA object(s)
%   a: antennaarray data (CHAR)
%
%   See also HEATER_DATA.

if length(mia) ~= 1
  if ~iscell(a)
    a = {a};
  end
  if length(a) == 1
    a = repmat(a, size(mia));
  elseif ~isequal(size(mia), size(a))
    error('a must be a CELL array the same size as mia');
  end

  r = mia;
  for n = 1:prod(size(mia))
    r(n) = setantennaarray(mia, a{n});
  end
  return;
end

% mia is scalar below here

sz = size(mia.ontimes);

r = mia;
if length(a) == 1
  a = repmat(a, sz);
elseif ~isequal(size(a), sz)
  error('incorrect size');
end

r.antennaarray = a;
