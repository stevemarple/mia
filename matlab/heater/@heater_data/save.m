function save(in)
%SAVE  Save HEATER_DATA object(s).
%
%   SAVE(mia)
%
%   Save the object into the default location. This requires administrator
%   access.
%
%   See also HEATER_DATA.


[fstr interval] = info(getinstrument(in), 'defaultfilename');

t1 = floor(getstarttime(in), interval);
et = ceil(getendtime(in), interval);

while t1 < et
  t2 = t1 + interval;
  mia = extract(in, t1, t2);
  if ~isempty(getontimes(mia))
    filename = strftime(t1, fstr);
    builtin('save', filename, 'mia');
  end
  t1 = t2;
end


