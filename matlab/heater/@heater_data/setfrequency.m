function r = setfrequency(mia, f)
%GETFREQUENCY  Return the transmitter frequency of HEATER_DATA object(s).
%
%   r = GETFREQUENCY(mia, f)
%   r: modified HEATER_DATA object(s)
%   mia: HEATER_DATA object(s)
%   f: frequency data (Hz)
%
%   See also HEATER_DATA.

if length(mia) ~= 1
  if ~iscell(f)
    f = {f};
  end
  if length(f) == 1
    f = repmat(f, size(mia));
  elseif ~isequal(size(mia), size(f))
    error('f must be a CELL array the same size as mia');
  end

  r = mia;
  for n = 1:prod(size(mia))
    r(n) = setfrequency(mia, f{n});
  end
  return;
end

% mia is scalar below here
sz = size(mia.ontimes);
sz(1) = 2;
r = mia;

if length(f) == 1
  f = repmat(f, sz);
elseif ~isequal(size(f), sz)
  error('incorrect size');
end

r.frequency = f;
