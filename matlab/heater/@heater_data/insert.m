function r = insert(a, b)
%INSERT  Insert a HEATER_DATA object into another HEATER_DATA object.
%
%   r = INSERT(a, b)
%
%

% does anyone know why these two objects should not be joined together?
% speak now!
if ~strcmp(class(a), class(b))
  error(sprintf('all objects must be of the same type (were %s and %s)', ...
		class(a), class(b)));
elseif getinstrument(a) ~= getinstrument(b)
  error('heater_data objects must be from the same heater instrument');
end

% fields which should not be check or concatenated
ignoreFields = {'versionnumber' 'ontimes' 'offtimes' 'mia_base'};
fn = fieldnames(rmfield(struct(a), ignoreFields));


% sanity checks
% is the heater on at the same time in both objects? If so then period
% etc must be the same.

aCycles = length(a.ontimes);
bCycles = length(b.ontimes);
as = struct(a);
bs = struct(b);

for n = 1:bCycles
  exactMatchIdx = find(b.ontimes(n) == a.ontimes & ...
		       b.offtimes(n) == a.offtimes);
  if length(exactMatchIdx) > 1
    % multiple ON periods with same on/off times. Ooops!
    error('heater object a contains non-unique heater periods');
    
  elseif length(exactMatchIdx) == 1
    % check all other parameters are the same
    for fnn = 1:length(fn)
      af = getfield(as, fn{fnn});
      bf = getfield(bs, fn{fnn});
      if ~isequal(af(:,exactMatchIdx), bf(:,n))
	error(sprintf(['on/off times are identical but heater ' ...
		       'properties are not\n(failed on %s for %s)'], ...
		      fn{fnn}, dateprintf(b.ontimes(n), b.offtimes(n))));
      end
    end
  else
    % no exact match, check for overlap
    % 3 cases:
    % starttime between heater on/offtimes
    % endtime between heater on/offtimes
    % starttime before heater on and endtime after heater off
    overlap = (b.ontimes(n) >= a.ontimes & b.ontimes(n) < a.offtimes) | ...
	      (b.offtimes(n) > a.ontimes & b.offtimes(n) <= a.offtimes) | ...
	      (b.ontimes(n) < a.ontimes & b.offtimes(n) > a.offtimes);
    if any(overlap)
      idx = find(overlap);
      error(sprintf('Heater periods overlap\na: %s\nb: %s', ...
		    dateprintf(a.ontimes(idx), a.offtimes(idx)), ...
		    dateprintf(b.ontimes(n), b.offtimes(n))));
    end
  end
end





r = a; % take on main characteristics
r = setstarttime(r, min(getstarttime(a), getstarttime(b)));
r = setendtime(r, max(getendtime(a), getendtime(b)));

% have on times in order, delete repeated times. (Repeated times mean all
% data associated with that time is the same, since we checked for that).

a.ontimes, b.ontimes
class(a.ontimes)
class(b.ontimes)
[r.ontimes idx] = unique([a.ontimes, b.ontimes]);
tmp = [a.offtimes, b.offtimes];
r.offtimes = tmp(idx);

% concatenate fields
for fnn = 1:length(fn)
  disp(fn{fnn});
  af = getfield(as, fn{fnn});
  bf = getfield(bs, fn{fnn});
  disp(sprintf('size: %d %d   ', size(af), size(bf)));
  tmp = [af, bf];
  r = feval(['set' fn{fnn}], r, tmp(:, idx));
end

