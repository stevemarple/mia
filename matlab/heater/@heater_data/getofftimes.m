function r = getofftimes(mia)
%GETOFFTIMES  Return the on times for a heater data object.
%
%   r = GETOFFTIMES(mia)
%   r: TIMESTAMP vector
%   mia: HEATER_DATA object(s)
%
%   If mia is non-scalar then r will be a CELL array of TIMESTAMP
%   vectors.
%
%   See also HEATER_DATA, TIMESTAMP.

if length(mia) == 1
  r = mia.offtimes;
else
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = mia(n).offtimes;
  end
end

