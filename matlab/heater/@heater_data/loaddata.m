function r = loaddata(in)
%LOADDATA  Private function to load data derived from RIO_BASE.
%
r = in;

instrument = getinstrument(in);
cls = class(in);
% filenameFstr: strftime format string
% fileDur: duration of file
% fifm: fail if file missing
[filenameFstr fileDur fifm] = info(instrument, 'defaultfilename', in); 


st = getstarttime(in);
et = getendtime(in);

filesLoaded = 0;
t = floor(st, fileDur);
while t < et
  filename = strftime(t, filenameFstr);
  if ~exist(filename)
    if fifm
      error(sprintf('Cannot load %s', filename));
    end
  else
    disp(sprintf('\rloading %s', filename));
    clear mia;
    load(filename);
    filesLoaded = filesLoaded + 1;
    % --- compatibility with old iris toolkit
    if ~exist('mia', 'var')
      s = whos('-file', filename);
      if size(s) > 1
	error(sprintf('file (%s) contains more than one variable', ...
		      filename));
      end
      mia = eval(sprintf('ifb2rio_base(%s)', s.name));
    end
    % --- (end)
    if ~strcmp(class(mia), cls)
      % convert to desired class. This mean we can convert rio_rawpower to
      % rio_power transparently. The conversion has to be done here, not
      % after changing resolution since that uses mean/median and raw power
      % is not linearised.
      mia = feval(cls, mia);
    end
    
    r = insert(r, mia);

  end
  
  t = t + fileDur;
end


% chop to start and end times
r = extract(r, st, et);



