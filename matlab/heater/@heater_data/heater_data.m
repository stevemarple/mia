function mia = heater_data(varargin)
%HEATER_DATA  Constructor for HEATER_DATA class.
%
%

% Make it easy to change the class definition at a later date
latestversion = 1;
mia.versionnumber = 1; % integer

mia.ontimes = timestamp({});
mia.offtimes = timestamp({});

mia.frequency = [];      % (Hz) interpolation allowed
mia.modulation = {''};      % assumed constant when heater on
mia.power = [];          % (W) interpolation allowed
mia.transmitters = {[]}; % assumed constant when heater on
mia.mode = {''};            % assumed constant when heater on
mia.antennaarray = [];   % assumed constant when heater on
mia.zenith = [];         % interpolation allowed
mia.azimuth = [];        % interpolation allowed

cls = 'heater_data';

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  parent = mia_base;
  mia = class(mia, cls, parent);

elseif nargin == 1 & (isa(varargin{1}, 'mia_base') | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  parent = mia_base;
  mia = class(mia, cls, parent);
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  
  defaults = mia; % take on standard settings
  % define extra parameters we accept
  % load from disk or create new
  defaults.load = (nargin == 6); % reqd number to load
  defaults.onduration = [];
  defaults.offduration = [];
  
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'onduration' 'offduration'});

  % Ensure that the returned object is marked with the latest version
  % number
  mia.versionnumber = latestversion;

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  parent = mia_base(varargin{unvi});

  if defaults.load
    instrument = getinstrument(parent);
    if isempty(instrument)
      error('instrument must be specified');
    end
    
    if ~isempty(info(instrument, 'defaultfilename', mia))
      % these files are on disk, load directly
      mia = class(mia, cls, parent);
      mia = loaddata(mia);
    else
      error(sprintf('cannot load heater_data files for %s', char(mia)));
    end
    return;
  end
  
  if xor(isempty(defaults.onduration), isempty(defaults.offduration))
    % both or nothing
    error('Both onduration and offduration must be defined');
  elseif ~isempty(defaults.onduration)
    % overriding any setting for ontimes
    cycle = defaults.onduration + defaults.offduration;
    starttime = getstarttime(parent);
    endtime = getendtime(parent);
    mia.ontimes = starttime:cycle:endtime;
    mia.offtimes = (starttime+defaults.onduration):cycle:endtime;
    % it is possible for ontimes to be 1 longer than offtimes, depending
    % how things were set
    if mia.ontimes(end) >= mia.offtimes(end)
      mia.ontimes = mia.ontimes(1:(end-1));
    end
    parent = setendtime(parent, mia.offtimes(end));
  end
  
  % check the inputs are sensible. Commonly used values could be converted
  % from scalars to vectors to save time. Less common ones should stay as
  % scalars unless explicitly given as vectors
  
  
  sz = size(mia.ontimes);
  sz2 = sz;
  sz2(1) = sz(1) * 2; % must have on and off details for each period
  if sz(1) > 1
    error('ontimes must be a row vector');
  elseif ~isequal(size(mia.offtimes), sz)
    error('ontimes and offtimes must be same size')
  end

  if length(mia.frequency) == 1 
    mia.frequency = repmat(mia.frequency, sz2);
  elseif ~isequal(size(mia.frequency), sz2)
    error('bad size for frequency')
  end

  if ischar(mia.modulation)
    mia.modulation = {mia.modulation};
  end
  if length(mia.modulation) == 1 
    mia.modulation = repmat(mia.modulation, sz);
  elseif ~isequal(size(mia.modulation), sz)
    error('bad size for modulation')
  end

  if length(mia.power) == 1 
    mia.power = repmat(mia.power, sz2);
  elseif ~isequal(size(mia.power), sz2)
    error('bad size for power')
  end
  
  if ~iscell(mia.transmitters)
    mia.transmitters = {mia.transmitters};
  end
  if length(mia.transmitters) == 1 
    mia.transmitters = repmat(mia.transmitters, sz);
  elseif ~isequal(size(mia.transmitters), sz)
    matrixinfo(mia.transmitters)
    error('bad size for transmitters')
  end
  
  if ~iscell(mia.mode)
    mia.mode = {mia.mode};
  end
  if length(mia.mode) == 1 
    mia.mode = repmat(mia.mode, sz);
  elseif~isequal(size(mia.mode), sz)
    error('bad size for mode')
  end
  
  if length(mia.antennaarray) == 1 
    mia.antennaarray = repmat(mia.antennaarray, sz);
  elseif isequal(size(mia.antennaarray), sz)
    error('bad size for antennaarray')
  end
  
  if length(mia.zenith) == 1 
    mia.zenith = repmat(mia.zenith, sz2);
  elseif ~isequal(size(mia.zenith), sz2)
    error('bad size for zenith');
  end
    
  if length(mia.azimuth) == 1 
    mia.azimuth = repmat(mia.azimuth, sz2);
  elseif ~isequal(size(mia.modulation), sz)
    error('bad size for azimuth')
  end
  
  mia = class(mia, cls, parent);
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
