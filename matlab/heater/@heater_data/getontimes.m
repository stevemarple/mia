function r = getontimes(mia)
%GETONTIMES  Return the on times for a heater data object.
%
%   r = GETONTIMES(mia)
%   r: TIMESTAMP vector
%   mia: HEATER_DATA object(s)
%
%   If mia is non-scalar then r will be a CELL array of TIMESTAMP
%   vectors.
%
%   See also TIMESTAMP.

if length(mia) == 1
  r = mia.ontimes;
else
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = mia(n).ontimes;
  end
end
