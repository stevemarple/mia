function r = extract(mia, st, et)
%EXTRACT Extract a subset of data as another HEATER_DATA object
%
%   r = EXTRACT(mia, st, et);
%
%   r: HEATER_DATA object
%   mia: original HEATER_DATA object
%   st: starttime
%   et: endtime
%
%   EXTRACT returns a subset of the data.
%
%   See also HEATER_DATA, TIME

if length(mia) ~= 1
  r = mia; % preallocate
  if length(st) == 1
    st = repmat(st, size(mia));
  elseif ~isequal(size(st), size(mia))
    error('st must be same size as mia');
  end
  if length(et) == 1
    et = repmat(et, size(mia));
  elseif ~isequal(size(et), size(mia))
    error('et must be same size as mia');
  end

  for n = 1:prod(size(mia))
    r(n) = extract(mia(n), st(n), et(n));
  end
  return;
end

% below here mia is scalar


if isa(et, 'timespan')
  et = st + et;
end

d.ontimes = getontimes(mia);
d.offtimes = getofftimes(mia);

% find times which match, including ones where the on or off cycle
% straddles the boundary 
idx = (d.ontimes >= st & d.ontimes < et) | ...
      (d.offtimes > st & d.offtimes <= et);


% should the first and last times be checked and trimmed if they cross
% the boundary? Probably not (would loose information)

r = mia;
r = setstarttime(r, st);
r = setendtime(r, et);
r.ontimes = d.ontimes(idx);
r.offtimes = d.offtimes(idx);

% is this needed?
if isempty(idx) & 0
  % everything should be empty
  r.frequency = [];
  r.modulation = {};
  r.power = [];
  r.transmitters = {};
  r.mode = {};
  r.antennaarray = [];
  r.zenith = [];
  r.azimuth = [];
  return
end

r.frequency = mia.frequency(:,idx);
r.modulation = {mia.modulation{:,idx}};
r.power = mia.power(:,idx);
r.transmitters = {mia.transmitters{:,idx}};
r.mode = {mia.mode{:,idx}};
r.antennaarray = mia.antennaarray(:,idx);
r.zenith = mia.zenith(:,idx);
r.azimuth = mia.azimuth(:,idx);

