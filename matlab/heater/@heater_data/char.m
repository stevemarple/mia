function r = char(mia)
%CHAR  Convert a HEATER_DATA object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia);
  return;
end

cycles = length(mia.ontimes); % number of heater on cycles

if cycles == 0
  freqStr = '';
elseif all(mia.frequency == mia.frequency(1)) | all(isnan(mia.frequency))
  if isnan(mia.frequency(1))
    freqStr = '<unknown>';
  else
    freqStr = printunits(mia.frequency(1), 'Hz');
  end
else
  freqStr = matrixinfo(mia.frequency);
end

if cycles == 0
  modulationStr = '';
elseif isequal(mia.modulation{:})
  modulationStr = mia.modulation{1};
else
  modulationStr = matrixinfo(mia.modulation);
end

if cycles == 0
  powerStr = '';
elseif all(mia.power == mia.power(1)) & ~isnan(mia.power(1))
  powerStr = printunits(mia.power(1), 'W');
else
  powerStr = matrixinfo(mia.power);
end

if cycles == 0
  transmittersStr = '';
elseif isequal(mia.transmitters{:}) 
  transmittersStr = printseries(mia.transmitters{1});
else
  transmittersStr = matrixinfo(mia.transmitters);
end

if cycles == 0
  modeStr = '';
elseif isequal(mia.mode{:})
  modeStr = mia.mode{1};
else
  modeStr = matrixinfo(mia.mode);
end

if cycles == 0
  antStr = '';
elseif all(mia.antennaarray == mia.antennaarray(1)) & ...
      ~isnan(mia.antennaarray(1))
  antStr = sprintf('#%d', mia.antennaarray(1));
else
  antStr = matrixinfo(mia.antennaarray);
end

if cycles == 0
  zenithStr = '';
elseif all(mia.zenith == mia.zenith(1)) & ~isnan(mia.zenith(1))
  zenithStr = printunits(mia.zenith(1), 'degrees');
else
  zenithStr = matrixinfo(mia.zenith);
end

if cycles == 0
  azimuthStr = '';
elseif all(mia.azimuth == mia.azimuth(1)) & ~isnan(mia.azimuth(1))
  azimuthStr = printunits(mia.azimuth(1), 'degrees');
else
  azimuthStr = matrixinfo(mia.azimuth);
end

% char(mia.mia_base), char(mia.ontimes), char(mia.offtimes), freqStr, modulationStr, powerStr, transmittersStr,  modeStr, antStr, zenithStr, azimuthStr

r = sprintf(['%s' ...
	     'on times          : %s\n' ...
	     'off times         : %s\n' ...
	     'frequency         : %s\n' ...
	     'modulation        : %s\n' ...
	     'power             : %s\n' ...
	     'transmitters      : %s\n' ...
	     'mode              : %s\n' ...
	     'antenna array     : %s\n' ...
	     'zenith            : %s\n' ...
	     'azimuth           : %s\n' ], ...
	    char(mia.mia_base), char(mia.ontimes), char(mia.offtimes), ...
	    freqStr, modulationStr, powerStr, transmittersStr, ...
	    modeStr, antStr, zenithStr, azimuthStr);
    









