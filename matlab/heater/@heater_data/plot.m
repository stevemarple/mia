function varargout = plot(mia, varargin)
%PLOT  Plot data from a HEATER_DATA object.
%
%   PLOT(mia)
%   PLOT(mia, ...)

fh = [];
gh = [];
ph = [];

defaults.miniplots = [1 1];
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.data = 'frequency'; % what data to plot

[defaults unvi] = interceptprop(varargin, defaults);

starttime = getstarttime(mia);
endtime = getendtime(mia);

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  args = {varargin{unvi} 'plotaxes', defaults.plotaxes};
  fh = [];
else
  gh = [];
  args = {varargin{unvi}};
end

if ischar(defaults.data)
  defaults.data = {defaults.data};
end
numOfPlots = prod(size(defaults.data));

validData = {'frequency' 'power' 'azimuth' 'zenith' 'zeniths'};
% since all valid data types can be interpolated this makes things more
% consistent later
if any(~ismember(defaults.data, validData))
  idx = find(~ismember(defaults.data, validData));
  error([sprintf('''%s'' is not a valid data type\n', defaults.data{idx}) ...
	 'Valid data types are:' sprintf(' ''%s''', validData{:})]);
end

if defaults.miniplots ~= [1 1] & prod(defaults.miniplots) < numOfPlots
  error(sprintf(['%d plot windows specified, but %d plots ' ...
		 'requested!'], prod(size(defaults.miniplots)), numOfPlots));
end

% Get a figure complete with menus etc
if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', args{:}, ...
      'miniplots', defaults.miniplots, ...
      'title', defaults.title);
end

xdata = [mia.ontimes; mia.offtimes] - starttime;
xdata = gettotalseconds(xdata);
xdata(3,:) = xdata(2,:); % need nans at offtimes to switch off lines

sz = size(xdata);

% convert to vector
xdata = xdata(:)';

n = 1;
for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    if n <= numOfPlots
      keepKeys = {'Tag' 'UserData'};
      keepVal = get(gh(x,y), keepKeys); 

      switch defaults.data{n}
       case 'zeniths'
	% signed zenith
	data = getzenith(mia);
	az = getazimuth(mia);
	southIdx = find(az > 90 & az < 270);
	data(southIdx) = -data(southIdx);
	
       otherwise
	data = feval(['get' defaults.data{n}], mia);
      end

      if length(data) == 1
	data = repmat(data, sz); % shouldn't be needed
      end
      data(3,:) = nan; % switch off line drawing
      
      ph(n) = plot(xdata, data(:), ...
		   'Parent', gh(x,y), ...
		   'Color', defaults.color); 
      ph = flipud(ph); % last line is first
      set(gh(x,y), keepKeys, keepVal);
      set(get(gh(x,y), 'XLabel'), 'String', defaults.data)
      
    else
      % axis blank 
      set(gh(x,y), 'Visible', 'off');
    end
    n = n + 1;
  end
end


set(gh, 'XLim', [0 gettotalseconds(getduration(mia))]);
timetick(gh, 'X', starttime, endtime, timespan(1,'s'), 1);

set(fh, 'Name', defaults.windowtitle, ...
    'Pointer', 'arrow');


varargout{1} = fh;
varargout{2} = gh;
varargout{3} = ph;
