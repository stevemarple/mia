function h = onofftimes(mia, ah, varargin)
%ONOFFTIMES  Decorate a plot with HEATER_DATA on/off times.
%
%   h = ONOFFTIMES(mia, ah, ...)
%   h: column vector of handles
%   mia: HEATER_DATA object (scalar)
%   ah: AXIS handle(s)
%   
%   If ah is a vector then h is a CELL array of handles, where the CELL
%   array is the same size as ah.
%
%   The default behaviour of this function can be modified by the
%   following name/value pairs.
%
%     'timeaxis', CHAR
%     The time axis from which to obtain start/end times from (also the
%     axis along which on/off times are plotted

h = [];
onTimesNum = length(mia.ontimes);


% accept multiple axes
if prod(size(ah)) > 1
  h = cell(size(ah));
  for n = 1:prod(size(ah))
    h{n} = onofftimes(mia, ah(n), varargin{:});
  end
  return;
end

defaults.timeaxis = 'X';
defaults.otheraxis = 'Y';

lines = findobj(ah, 'Type', 'line')';
linesNum = length(lines);
if linesNum
  defaults.style = 'line';
else
  defaults.style = 'bars';
end

defaults.exacttiming = 1;

if all(strcmp(getmode(mia), 'X'))
  defaults.color = 'g'; % X-mode heating
elseif all(strcmp(getmode(mia), 'O'))
  defaults.color = 'r'; % O-mode heating 
else 
  % unknown mode or mixed modes
  defaults.color = 'c';
end

% for style 'bars':
defaults.lim = [];
defaults.edgecolor = 'none'; % shared with style 'patch'

% for style 'line'
defaults.linestyle = '-';
defaults.marker = 'none';
defaults.linewidth = get(ah, 'DefaultLineLineWidth');

% for style patch and imagepatch (see also defaults.edgecolor)
defaults.patchxpos = []; 
defaults.patchypos = [];
defaults.offcolor = 'k';
defaults.offedgecolor = 'w';

% for style imagepatch only
defaults.image = [];

defaults.plotstarttime = [];
defaults.plotendtime = [];
defaults.plotresolution = [];

[defaults unvi] = interceptprop(varargin, defaults);

if ~iscell(defaults.color)
  defaults.color = repmat({defaults.color}, 1, onTimesNum);
end
if ischar(defaults.edgecolor)
  defaults.edgecolor = repmat({defaults.edgecolor}, 1, onTimesNum);
end

if strcmp(defaults.style, 'patch')
  if isempty(defaults.patchxpos)
    defaults.patchxpos = [0 0.15];  % position of patch (normalized units)
  end
  if isempty(defaults.patchypos)
    defaults.patchypos = [0 0.15];  % position of patch (normalized units)
  end
elseif strcmp(defaults.style, 'imagepatch')
  if isempty(defaults.image)
    error('Cannot add ON/OFF times to image unless image supplied!');
  end
  if isempty(defaults.patchxpos)
    defaults.patchxpos = [1 (min(size(defaults.image, 2), 10))];
  end
  if isempty(defaults.patchypos)
    defaults.patchypos = [1 (min(size(defaults.image, 2), 10))];
  end
end
  
if isempty(defaults.lim) & ~isempty(ah)
  tmp = get(ah, [defaults.otheraxis 'Lim']);
  defaults.lim(1) = tmp(1) + ((tmp(2)-tmp(1))*0.05);
  defaults.lim(2) = tmp(1) + ((tmp(2)-tmp(1))*0.10);
end

% need to know the axis in the third (ie flat) dimension (normally z)
switch lower([defaults.timeaxis defaults.otheraxis])
 case {'xy', 'yx'}
  thirdaxis = 'z';
  
 case {'xz', 'zx'}
  thirdaxis = 'y';
  
 case {'yz', 'zy'}
  thirdaxis = 'x';
  
 otherwise
  error('Cannot identify ''flat'' axis');
end    

if strcmp(defaults.style, 'patch')
  % patch is for images - which are spatial not temporal, so there is no
  % timetick labelled axis
  userData = get(ah, 'UserData');
  if iscell(userData)
    disp(dateprintf(userData{:}));
  end
else
  userData = get(get(ah, [defaults.timeaxis 'Label']), 'UserData');
end

if isempty(userData) | ~iscell(userData)
  % either not labelled with timetick or some other problem
  % warning('not a timetick-labelled axis');
  % return;
  userData = cell(1,4);
elseif length(userData) < 2
  return; % wrong format

else
  % timetick sets 4 items in the user data area. The toolkit image
  % plotting function only set the first two (others not relevant)
  if length(userData) < 4
    userData{4} = []; % create remaining items blank
  end
end

% get the plot details
plotStartTime = userData{1};
plotEndTime   = userData{2};
plotRes       = userData{3};
autoLabel     = userData{4};

% if the user did not supply plot times then use the values just obtained
if isempty(defaults.plotstarttime)
  defaults.plotstarttime = plotStartTime;
end
if isempty(defaults.plotendtime)
  defaults.plotendtime = plotEndTime;
end
if isempty(defaults.plotresolution)
  defaults.plotresolution = plotRes;
end
% ignore autolabel

switch defaults.style
 case 'bars'
  h = zeros(1, onTimesNum);
  xdata = zeros(1,4);
  ydata = zeros(1,4);
  zdata = ones(1,4) * 1000;

  t = mia.ontimes;
  for n = 1:onTimesNum
    xdata(1) = (mia.ontimes(n) - plotStartTime) / plotRes;
    xdata(2) = (mia.offtimes(n) - plotStartTime) / plotRes;
    xdata(3:4) = xdata([2 1]);
    ydata([1 2]) = defaults.lim(1);
    ydata([3 4]) = defaults.lim(2);
    h(n) = patch('Parent', ah, ...
		 [defaults.timeaxis 'data'], xdata, ...
		 [defaults.otheraxis 'data'], ydata, ...
		 [thirdaxis 'data'], zdata, ...
		 'FaceColor', defaults.color{n}, ...
		 'EdgeColor', defaults.edgecolor{n});
  end

  % ----------------------------------------
 case 'lines'
  % Add one line for each time the heater is on. This allows the lines
  % representing the on times to be edited individually. If the
  % heatercycle time is the same (or similar) to the plot resolution it
  % is possible that only one data point is found in the heater on time,
  % so only a dot is printed. This is undesirable, so if necessary (and
  % optionally) interpolate between data samples to get samples at the
  % exact time the  heater went on/off.
  prop = get(lines);
  
  h = zeros(1, onTimesNum * linesNum); % max number possible
  hn = 0; % number of valid handles
  for n = 1:onTimesNum
    x1 = (mia.ontimes(n) - plotStartTime) / plotRes;
    x2 = (mia.offtimes(n) - plotStartTime) / plotRes;
    for m = 1:linesNum
      ind = find(prop(m).XData >= x1 & prop(m).XData <= x2);
      % ind = find(prop(m).XData >= x1 & prop(m).XData < x2);
      if ~isempty(ind)
	hn = hn + 1;
	xdata = prop(m).XData(ind);
	ydata = prop(m).YData(ind);
	if defaults.exacttiming & ~isempty(ind)
	  if ind(1) > 1 & prop(m).XData(ind(1)) ~= x1
	    % add point at start
	    xdata = [interp1(prop(m).XData, prop(m).XData, x1), xdata];
	    ydata = [interp1(prop(m).XData, prop(m).YData, x1), ydata];
	  end
	  lastInd = ind(length(ind));
	  if lastInd < length(prop(m).XData) & prop(m).XData(lastInd) ~= x2
	    % add point at end
	    xdata = [xdata interp1(prop(m).XData, prop(m).XData, x2)];
	    ydata = [ydata interp1(prop(m).XData, prop(m).YData, x2)];
	  end
	end
	h(hn) = line('Parent', ah, ...
		     'XData', xdata, ...
		     'YData', ydata, ...
		     'Color', defaults.color{n}, ...
		     'LineStyle', defaults.linestyle, ...
		     'LineWidth', defaults.linewidth, ...
		     'Marker', defaults.marker);
      end
    end
  end
  % trim off any unused handles
  h = h(1:hn);
  
  % ----------------------------------------
 case 'line'
  % Add only one line to indicate when the heater is on. This allows the on
  % times to be edited together.  
  prop = get(lines);
  
  h = zeros(1, linesNum);
  hn = 0; % number of valid handles
  for m = 1:linesNum
    % preallocate tmp X and Y data to max possible size
    tmpXData = zeros(size(prop(m).XData));
    tmpYData = zeros(size(prop(m).YData));
    tmpPos = 0;
    for n = 1:onTimesNum
      x1 = (mia.ontimes(n) - plotStartTime) ./ plotRes;
      x2 = (mia.offtimes(n) - plotStartTime) ./ plotRes;

      tmpInd = find(prop(m).XData >= x1 & prop(m).XData < x2);
      if ~isempty(tmpInd)
	tmpXData((tmpPos+1):(tmpPos+length(tmpInd))) = ...
	    prop(m).XData(tmpInd);
	tmpYData((tmpPos+1):(tmpPos+length(tmpInd))) = ...
	    prop(m).YData(tmpInd);	  
	
	% if added >1 ON period then use a NaN between to blank the
	% line
	if tmpPos
	  tmpXData(tmpPos) = mean(tmpXData(tmpPos + [-1 1]));
	  tmpYData(tmpPos) = nan;
	end

	tmpPos = tmpPos + length(tmpInd) + 1;
      end
    end
    
    % add line (if coincides with any heater ON periods)
    if tmpPos
      hn = hn + 1;
      h(hn) = line('Parent', ah, ...
		   'XData', tmpXData(1:(tmpPos-1)), ...
		   'YData', tmpYData(1:(tmpPos-1)), ...
		   'Color', defaults.color{n}, ...
		   'LineStyle', defaults.linestyle, ...
		   'LineWidth', defaults.linewidth, ...
		   'Marker', defaults.marker);
    end
    
  end
  % trim off any unused handles
  h = h(1:hn);
  
  % ----------------------------------------
 case 'patch'
  % Add a patch of colour in a specified location to indicate if the
  % heater was on or off. If it was partly on then add two patches to
  % indicate the proportion it was on/off for.
  
  if isempty(plotStartTime) | isempty(plotEndTime)
    return
  elseif plotStartTime >= getendtime(mia) | plotEndTime <= getstarttime(mia)
    return
  end
  plotDur = plotEndTime - plotStartTime;
  h = zeros(1, onTimesNum + 1); % max amount, trim unused later
  hn = 1; % number of valid handles
  xlim = get(ah, 'XLim');
  ylim = get(ah, 'YLim');
  ahProp = get(ah);
  
  h(hn) = localPatch(ah, ahProp, defaults, [0 1]);   % border
  set(h(hn), 'FaceColor', 'none', ...
	     'Edgecolor', defaults.offedgecolor, ...
	     'ZData', get(h(hn), 'ZData') + 1);
  hn = hn + 1;
  h(hn) = localPatch(ah, ahProp, defaults, [0 1]);   % off patch
  set(h(hn), 'FaceColor', defaults.offcolor, ...
	     'Edgecolor', 'none', ...
	     'ZData', get(h(hn), 'ZData') - 1);
  for n = 1:onTimesNum
    % if ~(mia.offtimes(n) < plotStartTime | mia.ontimes(n) > plotEndTime)
    if ~(mia.offtimes(n) < plotStartTime | mia.ontimes(n) >= plotEndTime)
      % heater on/off period at least partly matches plot period
      hn = hn + 1;
      % x1, x2 are relative positions within allocated area of plot
      x(1) = max(0, (mia.ontimes(n) - plotStartTime) / plotDur);
      x(2) = min(1, (mia.offtimes(n) - plotStartTime) / plotDur);
      h(hn) = localPatch(ah, ahProp, defaults, x);
    elseif (mia.offtimes(n) > plotEndTime)
      break;
    end
  end
  h = h(1:hn);
  
  % ----------------------------------------
 case 'imagepatch'
  % For movies we need to add a patch onto an image passed to  us (in the
  % form of a matrix)
  plotDur = defaults.plotendtime - defaults.plotstarttime;
  
  % cannot use textual representations of color - must be RGB
  [defaults.offcolor defaults.color] = text2rgb(defaults.offcolor, ...
   						defaults.color);
  
  % background patch
  if ~isequal(defaults.offcolor, 'none')
      for c = 1:3
	defaults.image(defaults.patchxpos(1):defaults.patchxpos(2), ...
		       defaults.patchypos(1):defaults.patchypos(2), ...
		       c) = defaults.offcolor(c);
      end
  end
  
  for n = 1:onTimesNum
    if ~(mia.offtimes(n) < defaults.plotstarttime ...
	 | mia.ontimes(n) > defaults.plotendtime)
      % heater on/off period at least partly matches plot period
      % x1, x2 are relative positions within allocated area of plot
      x(1) = max(0, (mia.ontimes(n) - defaults.plotstarttime) / plotDur);
      x(2) = min(1, (mia.offtimes(n) - defaults.plotstarttime) / plotDur);
      patchxposdiff = defaults.patchxpos(2) - defaults.patchxpos(1);
      xx = (defaults.patchxpos(1) + round(x(1) .* patchxposdiff)):...
	   (defaults.patchxpos(1) + round(x(2) .* patchxposdiff));
      yy = defaults.patchypos(1):defaults.patchypos(2);

      % assign R, G and B
      for c = 1:3
	defaults.image(xx, yy, c) = defaults.color(c);
      end
    elseif (mia.offtimes(n) > defaults.plotendtime)
      break;
    end
  end
  h = defaults.image;
  
  % ----------------------------------------
 otherwise
  error(['unknown style (was ''' defaults.style ''')']);
end


return;

function r = localPatch(ah, ahProp, defaults, x)
%   ah: axis handle
%   ahProp: properties of ah (structure) [for efficiency]
%   defaults: defaults structure
%   x: [1x2] DOUBLE, lower and upper x limits of patch (normalized units)

xlimDiff = ahProp.XLim(2) - ahProp.XLim(1);
ylimDiff = ahProp.YLim(2) - ahProp.YLim(1);
patchxposdiff = defaults.patchxpos(2) - defaults.patchxpos(1);
xdata = ahProp.XLim(1) +  xlimDiff * (x .* defaults.patchxpos);
ydata = ahProp.YLim(1) +  ylimDiff * ([0 1] .* defaults.patchypos);
xdata(3:4) = xdata([2 1]);
ydata = [ydata(1) ydata(1) ydata(2) ydata(2)];
r = patch('Parent', ah, ...
	  'FaceColor', defaults.color, ...
	  'Edgecolor', defaults.edgecolor, ...
	  'XData', xdata, ...
	  'YData', ydata, ...
	  'ZData', repmat(10000, size(xdata)));






