function r = heater_spear
%EISCAT_T  EISCAT heater facility.

r = heater('name', 'SPEAR', ...
	   'abbreviation', 'spear', ...
	   'location', location('Longyearbyen', 'Svalbard', 78.9, 16.4), ...
	   'serialnumber', 1);
