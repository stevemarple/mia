function r = cdf_datatypevalue(t)
%CDF_DATATYPEVALUE Convert a CDF datatype to the number which represents it.
%
%   r = CDF_DATATYPEVALUE(t)
%   r: CDF data type value, as found in cdf.h
%   t: CDF type name, without 'CDF_' prefix.
%
% CDF_DATATYPEVALUE is intended for internal use by CDF_TOOLS.


% mapping from CDF datatype names to values, taken from cdf.h

s.INT1    =  1;
s.INT2    =  2;
s.INT4    =  4;
s.UINT1   = 11;
s.UINT2   = 12;
s.UINT4   = 14;
s.REAL4   = 21;
s.REAL8   = 22;
s.EPOCH   = 31;    % Standard style.
s.EPOCH16 = 32;    % Not supported by CDF tools.
s.BYTE    = 41;    % same as CDF_INT1 (signed)
s.FLOAT   = 44;    % same as CDF_REAL4
s.DOUBLE  = 45;    % same as CDF_REAL8
s.CHAR    = 51;    % a "string" data type
s.UCHAR   = 52;    % a "string" data type

if isfield(s, t)
  r = getfield(s, t);
else
  error(sprintf('Unknown data type (was ''%s'')', t));
end

