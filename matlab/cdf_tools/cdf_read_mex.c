#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "mat.h"
#include "matrix.h"

#include "cdf_tools_mex.h"

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
  CDFid id;
  char filename[CDF_PATHNAME_LEN + 1];
  const mxArray *filename_matrix = NULL;

  char **var_names = NULL;
  long num_vars;
  int i;
  const char *err_mesg = NULL;

  /* Clear plhs to indicate no matrices have been created (needed so
   * that when errors occur can tell if there are matrices to
   * destroy) */
  for (i = 0; i < nlhs; ++i)
    plhs[i] = NULL;
  
  /* Check input and output arguments. Give an error here if incorrect. */
  if (nrhs != 1)
    mexErrMsgTxt("Incorrect number of inputs");

  if (nlhs < 1 || nlhs > 4)
    mexErrMsgTxt("Incorrect number of outputs");

  filename_matrix = prhs[0];
  
  if (!mxIsChar(filename_matrix)) {
    err_mesg = "Filename must be a char";
    goto return_error;
  }

  
  if (mxGetString(filename_matrix, filename, CDF_PATHNAME_LEN)) {
    err_mesg = "Could not copy filename matrix (name too long?)";
    goto return_error;
  }

  if (CDFlib(OPEN_, CDF_, filename, &id,
	     NULL_) != CDF_OK) {
    err_mesg = "Could not open CDF file";
    goto return_error;
  }
  
  /* Set read-only mode, and zMode 2 */ 
  cdfstatus(CDFlib(SELECT_, CDF_READONLY_MODE_, READONLYon,
		   SELECT_, CDF_zMODE_, zMODEon2,
		   NULL_));
  
  var_names = getCdfVariableNames(&num_vars);
 

  /* Return data */
  if (nlhs > 0)
    plhs[0] = cdfRead(NULL, 0); 

  /* Return info */
  if (nlhs > 1)
    plhs[1] = cdfInfo(filename, (const char **)var_names, num_vars); 

  /* Return status */
  if (nlhs > 2)
    plhs[2] = createScalar(0);

  /* Return error mesg (no error) */
  if (nlhs > 3)
    plhs[3] = mxCreateString("");
  
  
  /* close CDF file */
  CDFlib(CLOSE_,CDF_,
	 NULL_);
  
  for (i = 0; i < num_vars; ++i) 
    mxFree(var_names[i]);
  mxFree(var_names);
  
  return;

  /* ***** ***** Error handling code ***** ***** */
 return_error:
  if (err_mesg == NULL)
    err_mesg = "Unknown error";

  if (var_names != NULL) {
    for (i = 0; i < num_vars; ++i) 
      mxFree(var_names[i]);
    mxFree(var_names);
  }

  if (nlhs < 2)
    /* Too few return parameters to indicate the error */
    mexErrMsgTxt(err_mesg);
  
  /* Return no data */
  if (nlhs > 0) {
    if (plhs[0] != NULL)
      mxDestroyArray(plhs[0]);
    plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);
  }
  
  /* Return no info */
  if (nlhs > 1) {
    if (plhs[1] != NULL)
      mxDestroyArray(plhs[1]);
    plhs[1] = mxCreateDoubleMatrix(0, 0, mxREAL);
  }

  /* Return status */
  if (nlhs > 2)
    plhs[2] = createScalar(1);

  /* Return error mesg (no error) */
  if (nlhs > 3)
    plhs[3] = mxCreateString(err_mesg);
  
  /* close CDF file */
  CDFlib(CLOSE_,CDF_,
	 NULL_);
  
  return;
}

