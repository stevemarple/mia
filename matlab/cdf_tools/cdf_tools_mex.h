#ifndef _CDF_TOOLS_MEX_H
#define _CDF_TOOLS_MEX_H

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "mat.h"
#include "matrix.h"

#include "cdf.h"

typedef enum {
  writemode_overwrite = 0,
  writemode_append,
} writemode_t;

typedef enum {
  majority_default = 0,
  majority_row,
  majority_column
} majority_t;


extern int cdf_tools_debug;


/* In testing for error conditions it would be useful if the file and
 * line number are available. Therefore define a macro function to
 * call the real C function. */
#define cdfstatus(status) _cdfstatus(__FILE__, __LINE__, status)

/* Function prototypes */
extern void _cdfstatus(const char* file, long line, CDFstatus status);

void closeCdfWithError(const char *cp);

mxArray* createScalar(double d);

mxArray* cdfRead(long *selected_vars, int num_selected_vars);


mxArray* cdfInfo(const char* filename,
		 const char **variable_names,
		 long num_vars);

const char* cdfWrite(const mxArray *filename_matrix,
		     const mxArray *data_matrix,
		     const mxArray *options_matrix);

const char* cdfDelete(const char* filename);

const char* deleteVariable(const char *var_name);
const char* deleteGlobalAttribute(const char *attr_name);
const char* deleteVariableAttribute(const char *attr_name,
				    const char *var_name);

mxArray* getzVariableAttributes(void);
extern mxArray* getEntryData(long scope, long *datatype_p, long *num_elems_p);

extern char** getCdfVariableNames(long *num_vars_p);

extern mxArray* getCdfZVariableMetadata(void);

/*
mxArray* cdfDataToMatrix(void *data, long cdf_datatype, long majority,
			 long data_len, int ndim, const int *dims);
*/
mxArray* cdfDataToMatrix(void *data, long cdf_datatype,
			 int ndim, const int *dims);
  
mxClassID cdfDataTypeToMatlab(long datatype);
long matlabArrayToCdfDataType(const mxArray *matrix);
mxArray* getGlobalAttributes(void);

/* Modify the string to remove trailing space */
void removeTrailingSpaces(char *cp);

long numel(int ndim, const int *dims);
extern const char* datatype_to_string(long datatype);

double getFieldValue(const mxArray *ap, const char *fieldname, int offset);

const char* putVariable(const char* var_name, long majority,
		  const mxArray *data, const mxArray *metadata);
void putGlobalAttributes(const mxArray *data);
void putGlobalAttributeEntries(const mxArray *data, const mxArray *metadata,
			       const char *attr_name);

const char* putVariableAttributes(const mxArray *data);

/* Given a char matrix create the char data for a CDF
 * hyperwrite. Calling function is responsible for calling mxFree to
 * deallocate the buffer */
char* getCellStrData(const mxArray *matrix, char padvalue, long maxlen,
		     long *buflen); /* out */

void transpose_matrix(mxArray *ap);





/* UNUSED functions */
void putgEntry(const mxArray *data, const mxArray *metadata,
	       const char *attr_name, int entry_num);

#endif /* matches #ifndef _CDF_TOOLS_MEX_H */
