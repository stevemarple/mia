function r = cdf_lowerfields(s)

c = cell(size(s));
fn = fieldnames(s);
lfn = lower(fn);
for m = 1:numel(s)
  c{m} = [];
  for n = 1:numel(fn)
    c{m} = setfield(c{m}, lfn{n}, getfield(s(m), fn{n}));
  end
end
r = reshape([c{:}], size(s));
return
