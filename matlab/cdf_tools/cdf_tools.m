%CDF_TOOLS  A collection of tools to support reading and writing CDF files.
%
% Do not confure the CDF_TOOLS functions (which all begin 'cdf_') with
% Matlab's own CDF functions (such as CDFREAD, CDFINFO and CDFWRITE).
%
% See also CDF_READ, CDF_INFO, CDF_WRITE.
