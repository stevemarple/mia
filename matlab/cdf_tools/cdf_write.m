function [status,mesg,r1,r2] = cdf_write(filename, varargin)
%CDF_WRITE Write CDF files.
%
%   [status,mesg] = CDF_WRITE(filename, ...)
%   status: 0 for success, 1 for failure
%   mesg: error message
%
% CDF_WRITE writes variables to a CDF file. It can also be used to append
% variables and attributes to an existing CDF file or delete variables and
% attributes contained in an existing file.
%
% Without any additional parameter CDF_WRITE creates an empty CDF
% file. The following parameter name/vaue pairs may be used to add
% variables, attributes and set other details.
%
%   'variables', STRUCT or CELL array (see CDF_FORMATPARAMETERS).
%   The variables to be written to the CDF file. If given as a STRUCT the
%   variable names are generated from the fieldnames and so are restricted
%   to valid Matlab variable names (unless the variable_rename option is
%   also supplied). The field values givs the variable data.
%   
%   'variable_metadata', STRUCT

%   The STRUCT fieldnames relate to the variable names, with the field
%   values being a STRUCT of the form:

%   'writemode',  'overwrite' or 'append'
%   If writemode is set to overwrite and the CDF file already exists its
%   contents will be deleted before any new data is written. If writemode
%   is set to append and the CDF file already exists data is appended to
%   the end of the records of existing variables and attributes. If the
%   CDF files does not already exist overwrite and append work in the
%   same way.
%
%   'majority',  '', 'row' or 'column'
%   Set whether row or column majority should be used. An empty string means
%   no attempt to set the majority will be made. If the majority cannot be
%   changed (eg when appending to a CDF file which already contains
%   variables) then a WARNING is issued. When writemode is 'overwrite'
%   majority defaults to 'column', which is more efficient for
%   Matlab. When writemode is 'append' majority dfaults to ''.
%
%   'backward_compatibility', LOGICAL
%   If true then the the CDF file created is backward compatible with
%   version 2.7.2, otherwise version 3.x is used. Only used if the MEX file
%   was compiled against CDF version 3.0 or higher. The default is to enbale
%   backward compatibility.

% If CDF_WRITE is called without any output arguments then an ERROR message
% is automatically generated if the CDF file could not be written. When
% called with output arguments CDF_WRITE silently ignores errors and sets
% status and mesg.


%
% See also CDF_TOOLS_INTRO, CDF_READ, CDF_INFO, CDF_DELETE.
%
% CDF_WRITE is part of the CDF_TOOLS toolbox.

status = 0;
mesg = '';

defaults.variables = [];
defaults.variables_metadata = {};
defaults.variable_attributes = {};
defaults.variable_attributes_metadata = {};
defaults.global_attributes = [];
defaults.global_attributes_metadata = [];

defaults.delete_global_attributes = {};
defaults.delete_variable_attributes = {};
defaults.delete_variables = {};

defaults.attribute_rename = {};
defaults.variable_rename = {};

defaults.majority = [];
defaults.writemode = 'overwrite'; % overwrite | append

defaults.backward_compatibility = 1;
defaults.debug = 0;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});


% Convert to cell arrays, rename any variables then create/process metadata
variables_cell = ...
    addvariablemetadata(cdf_formatparameters(defaults.variables, ...
                                             defaults.variable_rename, ...
                                             'mat_cdf'), ...
                        cdf_formatparameters(defaults.variables_metadata, ...
                                             defaults.variable_rename, ...
                                             'mat_cdf'));



varattr_cell = cdf_formatparameters(defaults.variable_attributes, ...
                                    defaults.attribute_rename, ...
                                    'mat_cdf');

varattrmd_cell = cdf_formatparameters(defaults.variable_attributes_metadata,...
                                      defaults.attribute_rename, ...
                                      'mat_cdf');

% Convert the variable parts to our standard cell array format ...
for n = 1:size(varattr_cell, 1)
  varattr_cell{n, 2} = cdf_formatparameters(varattr_cell{n, 2}, ...
                                            defaults.variable_rename, ...
                                            'mat_cdf');
end
% ... do the same for the metadata
for n = 1:size(varattrmd_cell, 1)
  varattrmd_cell{n, 2} = cdf_formatparameters(varattrmd_cell{n, 2}, ...
                                              defaults.variable_rename, ...
                                              'mat_cdf');
end

assignin('base', 'varattr_cell', varattr_cell);
assignin('base', 'varattrmd_cell', varattrmd_cell);

variable_attributes_cell = addvariableattribmetadata(varattr_cell, ...
                                                  varattrmd_cell);

% Convert to cell arrays, rename any variables then create/process metadata
global_attributes_cell = ...
    addglobalattribmetadata(cdf_formatparameters(defaults.global_attributes,...
                                                 defaults.attribute_rename, ...
                                                 'mat_cdf'), ...
                cdf_formatparameters(defaults.global_attributes_metadata, ...
                                     defaults.attribute_rename, ...
                                     'mat_cdf'));



% remove any .cdf from the filename
[p f e] = fileparts(filename);
if strcmpi(e, '.cdf')
  e = '';
end
filename = fullfile(p, f);

% Convert writemode string into a logical value for the MEX file
switch lower(defaults.writemode)
 case 'overwrite'
  % In overwrite mode delete the old CDF file
  defaults.writemode_enum = 0;
  
 case 'append'
  defaults.writemode_enum = 1;
  
 otherwise
  error(sprintf('Bad value for writemode (was ''%s'')', defaults.writemode));
end

if isequal(defaults.majority, [])
  % user has not set majority
  if strcmpi(defaults.writemode, 'append')
    defaults.majority = ''; % accept as it is
  else
    % overwrite mode
    defaults.majority = 'column'; % more efficient for Matlab
  end
end

% Convert majority string into a logical value for the MEX file
switch lower(defaults.majority)
  % Leave majority setting to whatever it defaults to (or already is,
  % for the case of writing to an existing CDF file).
 case ''
  defaults.majority_enum = 0;
 
 case 'row'
  defaults.majority_enum = 1;
 
 case 'column'
  defaults.majority_enum = 2;
 
 otherwise
  error(sprintf('Bad value for majority (was ''%s'')', defaults.majority));
end

defaults.global_attributes_cell = global_attributes_cell;
defaults.variable_attributes_cell = variable_attributes_cell;

if strcmp(defaults.writemode, 'overwrite') 
  % is doesnt make sense to delete portions of the CDf when the file is
  % opened in overwrite mode - might be an error
  if ~isempty(defaults.delete_global_attributes)
    error('delete_global_attributes may only be set in append mode');
  elseif ~isempty(defaults.delete_variable_attributes)
    error('delete_variable_attributes may only be set in append mode');
  elseif ~isempty(defaults.delete_variables)
    error('delete_variables may only be set in append mode');
  end
end

assignin('base', 'def', defaults)
assignin('base', 'var', variables_cell)
[status mesg] = cdf_write_mex(filename, variables_cell, defaults);

r1 = variables_cell;
r2 = defaults;


if nargout == 0
  error(mesg);
end


function r = addvariablemetadata(data, metadata)

r = cell(size(data, 1), 3);
for n = 1:size(data, 1)
  var_name = data{n, 1};
  var_data = data{n ,2};
  
  % find any associated metadata
  idx = find(strcmp(var_name, metadata(:, 1)));
  switch numel(idx)
   case 0
    var_metadata = [];
   case 1
    var_metadata = metadata{idx, 2};
   otherwise
    error(sprintf('Metadata for %s defined more than once', var_name));
  end
  
  % Get/check the datatype to use
  if iscell(var_data)
    if iscellstr(var_data)
      cls = 'char';
    else
      var_data
      error('Only cell of strings are permitted');
    end
  else
    cls = class(var_data);
  end
  
  if ~isfield(var_metadata, 'datatype')
    var_metadata.datatype = cdf_matlab2cdfdatatype(cls);
  end
  
  % Check Matlab class and CDF data type use the same number of bytes
  if cdf_datatypesize(var_metadata.datatype) ~= cdf_datatypesize(cls)
    error(sprintf(['Problem with ''%s'' variable: Matlab and CDF data ' ...
                   'use different numbers of bytes to store data'], ...
                  var_name));
  end
  var_metadata.datatype_value = ...
      cdf_datatypevalue(var_metadata.datatype);
  
  % Set any pad value
  if isfield(var_metadata, 'padvalue')
    if isa(var_metadata.padvalue, 'timestamp')
      var_metadata.padvalue = getcdfepochvalue(var_metadata.padvalue)
    end
    
    if ~isempty(var_metadata.padvalue)
      % Check Matlab class and CDF data type use the same number of bytes
      % for the padvalue
      if cdf_datatypesize(class(var_metadata.padvalue)) ~= ...
            cdf_datatypesize(cls)
        error(sprintf(['Problem with ''%s'' variable: Matlab and CDF ' ...
                       'data use different numbers of bytes to store '...
                       'padvalue'], var_name));
      end
    end
  else
    var_metadata.padvalue = [];
  end
  
  % Get/check number of records and dimensions to use
  sz = size(var_data);
  if ~isfield(var_metadata, 'records') ...
        & ~isfield(var_metadata, 'dimension_sizes')
    var_metadata.records = sz(end);
    var_metadata.dimension_sizes = sz(1:(end-1));
    if isequal(var_metadata.dimension_sizes, 1)
      % make dimensionless
      var_metadata.dimension_sizes = [];
    end
  elseif isfield(var_metadata, 'records') ...
        & isfield(var_metadata, 'dimension_sizes')
    ;
  else
    error(sprintf(['Both or neither of records and dimension_sizes '...
                   'must be set for %s'], var_name));
  end
  
  % Get/check record and dimension VARY option
  if ~isfield(var_metadata, 'recvary')
    var_metadata.recvary = 1;
  end
  if ~isfield(var_metadata, 'dimvarys')
    var_metadata.dimvarys = ones(1, numel(var_metadata.dimension_sizes));
  end

  % check sizes
  if numel(var_metadata.recvary) ~= 1
    error(sprintf('bad value for recvary for variable ''%s''', ...
                  var_name));
  elseif numel(var_metadata.dimvarys) ~= numel(var_metadata.dimension_sizes)
    error(sprintf('bad value for dimvarys for variable ''%s''', ...
                  var_name));
  end
  var_metadata.recvary = double(logical(var_metadata.recvary));
  var_metadata.dimvarys = double(logical(var_metadata.dimvarys));

  % Number of elements. User cannot set this.
  if iscellstr(var_data)
    var_metadata.number_of_elements = ...
        max(max(cellfun('prodofsize', var_data(:))), 1);
  else
    var_metadata.number_of_elements = 1;
  end
  
  
  if isa(var_data, 'timestamp')
    var_data = getcdfepochvalue(var_data);
  end

  r{n, 1} = var_name;
  r{n, 2} = var_data;
  r{n, 3} = var_metadata;
end



function r = addglobalattribmetadata(data, metadata)

% skeleton for the metadata struct
md_skel.datatype = [];
md_skel.datatype_value = [];
md_skel.number_of_elements = [];
skel_fields = fieldnames(md_skel);

r = cell(size(data, 1), 3);
for n = 1:size(data, 1)
  var_name = data{n, 1};
  var_data = data{n ,2};
  var_metadata = md_skel;

  % find any associated metadata
  idx = find(strcmp(var_name, metadata(:, 1)));
  switch numel(idx)
   case 0
    ; % do nothing

   case 1

    tmp = metadata{idx, 2};
    for k = 1:numel(tmp)
      var_metadata(k) = md_skel;
      for m = 1:numel(skel_fields)
        if isfield(tmp, skel_fields{m})
          var_metadata(k) = setfield(var_metadata(k), skel_fields{m}, ...
                                     getfield(tmp(k), skel_fields{m}));
        end
      end
    end
    
   otherwise
    error(sprintf('Metadata for %s defined more than once', var_name));
  end

  % Global attributes can have multiple entries, and each can have
  % different dataspecs
  if ~iscell(var_data)
    var_data = {var_data};
  end
  
  for m = 1:numel(var_data)
    
    if numel(var_metadata) >= m
      var_md_m = var_metadata(m);
    else
      var_md_m = [];
    end
    % Get/check the datatype to use
    if iscell(var_data{m})
      error('Nested cells not permitted')
    end
    cls = class(var_data{m});
    
    if isempty(var_md_m.datatype)
      var_md_m.datatype = cdf_matlab2cdfdatatype(cls);
    end
    
    % Check Matlab class and CDF data type use the same number of bytes
    
    if cdf_datatypesize(var_md_m.datatype) ~= cdf_datatypesize(cls)
      error(sprintf(['Problem with ''%s'' global attribute: Matlab ' ...
                     'and CDF data use different numbers of bytes to ' ...
                     'store data'], var_name));
    end
    var_md_m.datatype_value = cdf_datatypevalue(var_md_m.datatype);
    
    % Number of elements. User cannot set this beyond the amount of data
    % available
    if isempty(var_md_m.number_of_elements)
      var_md_m.number_of_elements = numel(var_data{m});
    else
      var_md_m.number_of_elements = min(numel(var_data{m}), ...
                                        var_md_m.number_of_elements(1));
    end
    if var_md_m.number_of_elements == 0
      var_md_m.number_of_elements = 1;
    end
    
    if isa(var_data{m}, 'timestamp')
      var_data{m} = getcdfepochvalue(var_data{m});
    end
    
    var_metadata(m) = var_md_m;
  end
  
  r{n, 1} = var_name;
  r{n, 2} = var_data;
  r{n, 3} = var_metadata;
end


function r = addvariableattribmetadata(data, metadata)

r = cell(size(data, 1), 2);
for n = 1:size(data, 1)
  attr_name = data{n, 1};
  
  % attr_data is a cell array of the associated variable names and the
  % attribute data entry for that variable.
  attr_data = data{n ,2};

  % Find any associated metadata. attr_metadata is a cell array of the
  % associated variable names and the attribute metadata for that
  % variables's entry.
  idx = find(strcmp(attr_name, metadata(:, 1)));
  switch numel(idx)
   case 0
    attr_metadata = cell(0, 2);
   case 1
    
    attr_metadata = metadata{idx, 2};
   otherwise
    error(sprintf('Metadata for %s defined more than once', attr_name));
  end
  
  % Rows in attr_data and attr_metadata may not match up, as caller may
  % not have passed any metadata details, or only limited metadata
  % details. Pass onto other function.
  r{n, 1} = attr_name;
  r{n, 2} = addvariableattribmetadata_for_attrib(attr_name, ...
                                                 attr_data, attr_metadata);
end

function r = addvariableattribmetadata_for_attrib(attr_name, data, metadata)
r = cell(size(data, 1), 3);
for n = 1:size(data, 1)
  var_name = data{n, 1};
  var_data = data{n ,2};
  
  % find any associated metadata
  idx = find(strcmp(var_name, metadata(:, 1)));
  switch numel(idx)
   case 0
    var_metadata = [];
   case 1
    var_metadata = metadata{idx, 2};
   otherwise
    error(sprintf(['Metadata for %s in attribute %s defined more ' ...
                   'than once'], var_name, attr_name));
  end
  
    % Get/check the datatype to use
  if iscell(var_data)
    if iscellstr(var_data)
      cls = 'char';
    else
      var_data
      error('Only cell of strings are permitted');
    end
  else
    cls = class(var_data);
  end
  
  if ~isfield(var_metadata, 'datatype')
    var_metadata.datatype = cdf_matlab2cdfdatatype(cls);
  end
  
  % Check Matlab class and CDF data type use the same number of bytes
  if cdf_datatypesize(var_metadata.datatype) ~= cdf_datatypesize(cls)
    error(sprintf(['Problem with ''%s'' variable attribute: Matlab and ' ...
                   'CDF data use different numbers of bytes to ' ...
                   'store data'], var_name));
  end
  var_metadata.datatype_value = cdf_datatypevalue(var_metadata.datatype);
  
  % Number of elements. User cannot set this beyond the amount of data
  % available
  if isfield(var_metadata, 'number_of_elements')
    var_metadata.number_of_elements = min(numel(var_data), ...
                                          var_metadata.number_of_elements(1));
  else
    var_metadata.number_of_elements = numel(var_data);
  end
  if var_metadata.number_of_elements == 0
    var_metadata.number_of_elements = 1;
  end

  if isa(var_data, 'timestamp')
    var_data = getcdfepochvalue(var_data);
  end
  
  r{n, 1} = var_name;
  r{n, 2} = var_data;
  r{n, 3} = var_metadata;
end
