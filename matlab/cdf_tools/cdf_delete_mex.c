#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "mat.h"
#include "matrix.h"

#include "cdf_tools_mex.h"

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
  const mxArray *filename_matrix;
  char *filename;
  const char *error_message;
  
  /* Check input and output arguments */
  if (nrhs != 1)
    mexErrMsgTxt("Incorrect number of inputs");
  filename_matrix = prhs[0];
  
  if (!mxIsChar(filename_matrix))
    mexErrMsgTxt("Filename must be a char");
    
  if (nlhs > 2)
    mexErrMsgTxt("Incorrect number of outputs");
  
  filename = mxArrayToString(filename_matrix);
  error_message = cdfDelete(filename);
  
  mxFree(filename);

  if (nlhs > 0)
    /* Set status (0 = success, 1 = failure). Logical support changed
     * between Matlab 5.3 and 6.5, so don't bother setting logical
     * status here, let the m-file do it. */
    plhs[0] = createScalar(error_message != NULL);

  if (nlhs > 1) {
    /* Set error message */
    if (error_message == NULL)
      plhs[1] = mxCreateString(""); /* success, return  empty string */
    else
      plhs[1] = mxCreateString(error_message);
  }
  
  return;
}


			    
