function r = cdf_structify(c, varargin)
%CDF_STRUCTIFY  Convert  CELL matrix to a STRUCT
%
%   r = CDF_STRUCTIFY(c, remap)
%   r: scalar STRUCT 
%   c: [n x 2] CELL matrix
%   remap: [1 x 2n] CELL matrix ({matlabname, CDFname, ...})
%
% CDF_STRUCTIFY converts an n x 2 CELL array into a STRUCT. It differs from
% the STRUCT function when it encounters CELL arrays - it converts them
% directly to the field's value, it does not create a non-scalar STRUCT.
%
% CDF_STRUCTIFY accepts a CELL array of strings for renaming the fields. If
% a new name is given it must be a valid variable name (see
% ISVARNAME). CDF_STRUCTIFY aims to produce useful output even when the
% fieldnames are illegal variable names, by replacing illegal characters by
% '_' and inserting a 'Q' at the beginning if the first character is not a
% letter.

if nargin > 1
  remap = cdf_formatparameters(varargin{1});
  % remap is in form {matlabname cdfname; matlabname2 cdfname2; ...}
else
  remap = {};
end



r = [];
for n = 1:size(c, 1)
  fn = c{n, 1}; % fieldname
  
  % check if fieldname should be mapped to some other name
  if isempty(remap)
    idx = [];
  else
    idx = find(strcmp(fn, remap{:, 2}));
  end
  if ~isempty(idx)
    fn = remap{idx(1), 1};
  else
    fn = local_make_legal_fieldname(fn);
  end
  
  r = setfield(r, fn, c{n, 2});
end


function [r, c] = local_make_legal_fieldname(f)
%LOCAL_MAKE_LEGAL_FIELDNAME
%
% Convert a string into a legal fieldname
%   [r, s] = LOCAL_MAKE_LEGAL_FIELDNAME
%   r: Converted field name
%   c: Changed flag, c=0 if f was already legal, 1 otherwise
%   f: fieldname

r = f;
c = 0;
if isvarname(r)
  return
end
c = 1;

alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
if ~any(r(1) == alpha)
  r = ['Q' r];
end

if isvarname(r)
  return
end

alphanum_ = [alpha '0123456789_'];

% map anything not alpanumeric or _ to _
r(~ismember(r, alphanum_)) = '_';
if isvarname(r)
  return
end

error(['Cannot make legal fieldname from ' f]);

