%CDF_TOOLS_INTRO  Introductory help to CDF_TOOLS.
%
%

% LIMITATIONS
%
% CDF stores attribute numbers and variable numbers in "long" integer
% format, whereas Matlab uses "int" format to store the number of
% elements. It it therefore possible that CDF files with an absurdly large
% number of attributes or variables cannot be loaded.

% To do:

% multi-file archives

% Deleting files

% global attributes

% variable attributes

% specifying variable names and other parameters (CDF_FORMATPARAMETERS)

% renaming variables


