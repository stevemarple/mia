function [data,info,status,mesg] = cdf_read(filename, varargin)
%CDF_READ  Read the contents of CDF variables in CDF file.
%
%   [data, info, status, mesg] = CDF_READ(filename, ...)
%   data: CELL or STRUCT containing the CDF variables.
%   info: STRUCT containing details of the CDF file (as from CDF_INFO)
%   status: error status, 0 for success, 1 for error
%   mesg: CHAR error message, or empty if no errors
%
% CDF_READ reads the contents of CDF variables and returns a STRUCT whose
% FIELDNAMES are the variable names. The result of CDF_INFO can also be
% returned (more efficiently than making spearate calls to CDF_INFO and
% CDF_READ). The name/value pairs listed below can be used to modify the
% default behaviour (they are performed in the order described):
%
%   'numeric_to_double', LOGICAL
%   Flag to indicate if numeric values (see ISNUMERIC) should be converted
%   to Matlab's DOUBLE class. The default is to not convert numeric classes
%   to DOUBLE.
%
%   'check_validminmax', LOGICAL
%   Flag to indicate if data values of Matlab class SINGLE or DOUBLE should
%   be checked to ensure all values are inside the valid range indicated by
%   variable attributes 'VALIDMIN' and 'VALIDMAX'. Those which are not are
%   replaced by NAN. Note that unless 'numeric_to_double' is enabled only
%   CDF data types 'FLOAT', 'DOUBLE', 'REAL4', 'REAL8' and 'EPOCH' are
%   checked. The default behaviour is to check variables.
%
%   'convert_epoch', LOGICAL
%   Flag to indicate if values of CDF data type 'EPOCH' are converted to
%   TIMESTAMP objects, or left as DOUBLE values. The default is to convert
%   to TIMESTAMPs. Conversion to TIMESTAMPs, if selected, occurs after
%   checking a variable's values are within its valid range.
%
%   'output_structs', LOGICAL
%   Determine whether the data (and info too if it was requested) should be
%   output as STRUCTs or CELL arrays. The default is to output STRUCTs. It
%   is possible for the CDF file to contain variables or attributes whose
%   names make illegal STRUCT fieldnames, when possible the fields will be
%   mapped to a legal name, or a specific new name can be given. Note that
%   attribute and variable renaming is only performed when STRUCT output is
%   selected.
%
%   'attribute_rename', STRUCT or CELL (See CDF_FORMATPARAMETERS)
%   If desired attributes may be renamed for Matlab (possibly because they
%   are not valid as field names for Matlab STRUCTs). In CDF files
%   attributes and variables may have the same names, though global
%   attributes and variable attributes share the same namespace. See
%   CDF_FORMATPARAMETERS for details about the STRUCT or CELL format.
%
%   'variable_rename', STRUCT or CELL (See CDF_FORMATPARAMETERS)
%   If desired variables may be renamed for Matlab (possibly because they
%   are not valid as field names for Matlab STRUCTs). In CDF files
%   attributes and variables may have the same names, though global
%   attributes and variable attributes share the same namespace. See
%   CDF_FORMATPARAMETERS for details about the STRUCT or CELL format.
%
% If CDF_READ is called with less than 3 output arguments then an ERROR
% message is automatically generated if the file cannot be read. When called
% with 3 or more output arguments CDF_READ silently ignores errors and sets
% status and mesg.
%
% See also CDF_INFO, CDF_WRITE.

defaults.numeric_to_double = 0;
defaults.check_validminmax = 1;
defaults.convert_epoch = 1;

defaults.output_structs = 1;
defaults.attribute_rename = {};
defaults.variable_rename = {};

[defaults unvi] = interceptprop(varargin, defaults);

% Call the mex function, but only request info if it is needed.
if nargout < 2 & ~logical(defaults.convert_epoch) ...
      & ~logical(defaults.check_validminmax)
  % User hasn't requested info. Since convert_epoch is false and
  % check_validminmax is also false there is no need for cdf_read to
  % fetch it
  data = cdf_read_mex(filename);
  info = [];
  status = logical(0);
  mesg = '';
else
  % The user or cdf_read needs info
  % data=[] ;info=[];status=0;mesg='';
  [data info status mesg] =  cdf_read_mex(filename);
  status = logical(status);
end

if status
  % Some kind of error
  if nargout < 3
    % user not testing for errors
    error(mesg);
  else
    return
  end
end

if logical(defaults.numeric_to_double)
  for n = 1:size(data, 1)
    if isnumeric(data{n, 2})
      data{n, 2} = double(data{n, 2});
    end
  end
end

if isequal(info, [])
  % user hasn't requested info so return now
  return
end

if logical(defaults.check_validminmax)
  % find if/where VALIDMIN occurs and extract attribute data
  attr_row = find(strcmp(info.variable_attributes(:, 1), 'VALIDMIN'));
  if ~isempty(attr_row)
    % cell holding validmin details
    validmin = info.variable_attributes{attr_row, 2};
    
    % loop through all variables
    for n = 1:size(validmin, 1)
      varname = validmin{n, 1};    % current variable name
      data_row = find(strcmp(varname, data(:, 1)));  
      
      if numel(data_row) == 1 & numel(validmin{n, 2}) == 1
        % found variable in data, and have just one validmin value
        d = data{data_row, 2};
        if isa(d, 'double') | isa(d, 'single')
          d(d < validmin{n, 2}(1)) = nan;
          data{data_row, 2} = d;
        end
      end
    end
  end
  
  % find if/where VALIDMAX occurs and extract attribute data
  attr_row = find(strcmp(info.variable_attributes(:, 1), 'VALIDMAX'));
  if ~isempty(attr_row)
    % cell holding validmax details
    validmax = info.variable_attributes{attr_row, 2};
    
    % loop through all variables
    for n = 1:size(validmax, 1)
      varname = validmax{n, 1};    % current variable name
      data_row = find(strcmp(varname, data(:, 1)));  
      
      if numel(data_row) == 1 & numel(validmax{n, 2}) == 1
        % found variable in data, and have just one validmax value
        d = data{data_row, 2};
        if isa(d, 'double') | isa(d, 'single')
          d(d > validmax{n, 2}(1)) = nan;
          data{data_row, 2} = d;
        end
      end
    end
  end
end

if logical(defaults.convert_epoch)
  for n = 1:size(info.variables_metadata, 1)
    if strcmpi(info.variables_metadata{n,2}.datatype, 'epoch')
      data{n, 2} = timestamp('cdfepoch', data{n, 2});
    end
  end
end



if nargout > 1
  % Have CDF_INFO convert the info in the same way as if the user called
  % CDF_INFO separately.
  
  info = cdf_info(info, ...
                  'convert_epoch', defaults.convert_epoch, ...
                  'output_structs', defaults.output_structs, ...
                  'attribute_rename', defaults.attribute_rename, ...
                  'variable_rename', defaults.variable_rename, ...
                  varargin{unvi});
else
  % not passing on parameters, so warn
  warnignoredparameters(varargin{unvi(1:2:end)});
end

if logical(defaults.output_structs)
  data = cdf_structify(data, defaults.variable_rename);
end
