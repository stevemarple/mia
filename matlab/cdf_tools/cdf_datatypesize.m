function r = cdf_datatypesize(t)
%CDF_DATATYPESIZE  Return number of bytes a Matlab or CDF data type requires.
%
%   r = CDF_DATATYPESIZE(t)
%   r: DOUBLE, indicating number of bytes per single value
%   t: CHAR reprsenting the Matlab class or CDF data type name.
%
% CDF_DATATYPESIZE returns the number of bytes a Matlab or CDF data type
% requires to store one value. CDF names should be given without a 'CDF_'
% prefix.
%
% See CDF_TOOLS.

if ~ischar(t)
  t
  error(sprintf('type must be char (was ''%s'')', class(t)));
end

% Matlab classes
s.uint8     = 1;
s.uint16    = 2;
s.uint32    = 4;

s.int8      = 1;
s.int16     = 2;
s.int32     = 4;

s.single    = 4;
s.double    = 8;

s.logical   = 1;
s.char      = 1;

s.timestamp = s.double;
s.timespan  = s.double;

% CDF data types
s.INT1   = 1;
s.INT2   = 2;
s.INT4   = 4;

s.UINT1  = 1;
s.UINT2  = 2;
s.UINT4  = 4;

s.REAL4  = 4;
s.REAL8  = 8;
s.BYTE   = 1;
s.FLOAT  = 4;
s.DOUBLE = 8;

s.CHAR   = 1;
s.UCHAR  = 1;

s.EPOCH  = s.DOUBLE;

% not supported by CDF_TOOLS
% s.EPOCH16 = 16


if isfield(s, t)
  r = getfield(s, t);
elseif strcmp(t, 'EPOCH16')
  error('EPOCH16 is not supported by cdf_tools');
else
  error(sprintf('Unknown or unsupported data type or class (was ''%s'')', t));
end
