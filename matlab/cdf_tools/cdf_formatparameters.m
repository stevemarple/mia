function r = cdf_formatparameters(s, varargin)
%CDF_FORMATPARAMETERS Format parameters for CDF_TOOLS functions.
%
%   r = CDF_FORMATPARAMETERS(s);
%   r = CDF_FORMATPARAMETERS(s, rn, dir);
%   r: [n x 2] CELL array of parameters, {name1, value1; name2, value2; ...}
%   s: parameters, STRUCT, CELL vector or [n x 2] CELL matrix (see below)
%   rn: rename mapping, STRUCT, CELL vector or [n x 2] CELL matrix (see below)
%   dir: direction ('mat2cdf' or 'cdf2mat')
%
% CDF_FORMATPARAMETERS is a helper function for CDF_TOOLS. It is responsible
% for converting parameters into a [n x 2] CELL array. The input may be a
% STRUCT, with name as fieldnames and values being the field value; or as a
% CELL vector ({name1 value1 name2 value2 ...}) or as a [n x 2] ordered CELL
% array ({name1 value1; name2 value2; ...}).
%
% If desired the parameters names may be remapped from/to Matlab names
% to/from CDF names. The reammping details may be given the same formats as
% for s. The remapping details are always given in the order Matlab, CDF
% (thus STRUCT fields are always valid matlab variable names).
%
% The direction flag ('mat_cdf' or 'cdf_mat') indicates the direction of
% renaming (from_to).
%
% See CDF_TOOLS.

if isempty(s)
  r = cell(0, 2);
  return
end

if isstruct(s)
  if numel(s) ~= 1
    error('struct must be scalar');
  end
  fn = fieldnames(s);
  r = cell(numel(fn), 2);
  r(:, 1) = fn;
  r(:, 2) = struct2cell(s);

elseif iscell(s)
  if (size(s, 1) == 1 | size(s, 2) == 1) & numel(s) > 2
    % s is a list of name/value pairs
    % s = {name1, value1, name2, value2, ...}
    r = reshape(s, 2, 4)';
  else
    r = s;  
  end
  
else
  error(sprintf('Parameters must be a cell array or struct (was ''%s'')', ...
                class(s)));
end


% Renaming not requested
switch nargin
 case 1
  return
  
 case 3
  % Format renaming details to a cell array of the same type
  rn = cdf_formatparameters(varargin{1});
  direction = varargin{2};
  
 otherwise
  error('incorrect parameters');
end


% Remove any redundant renaming
redundant = [];
for n = 1:size(rn, 1)
  if strcmp(rn{n, 1}, rn{n, 2})
    redundant(end+1) = n;
  end
end
rn(redundant, :) = [];


if isempty(rn)
  return
end


% Check if any names are listed more than once, or if there is any
% intersection between Matlab and CDF names, which could confuse matters
% when trying to calculate the metadata details
if ~isequal(sort(rn(:, 1)), unique(rn(:, 1)))
  error('Repetitions of Matlab names in renaming details not permitted');
end
if ~isequal(sort(rn(:, 2)), unique(rn(:, 2)))
  error('Repetitions of CDF names in renaming details not permitted');
end
if ~isempty(intersect(rn(:, 1), rn(:, 2)))
  error('Intersection of Matlab and CDF names not permitted');
end

switch direction
 case 'mat_cdf'
  ; % do nothing
  
 case 'cdf_mat'
  rn = rn(:, [2 1]);
 otherwise
  error('unknown direction');
end

for n = 1:size(r, 1)
  idx = find(strcmp(r{n, 1}, rn(:, 1)));
  switch length(idx)
   case 0
    ; % nothing to rename
    
   case 1
    r{n, 1} = rn{idx, 2};
    
   otherwise
    % Having already checked for repetition of names this case should not
    % occur 
    error('multiple values for rename found');
  end
end

