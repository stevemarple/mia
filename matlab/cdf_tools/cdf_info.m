function [info,status,mesg] = cdf_info(filename, varargin)
%CDF_INFO  Return information about a CDF file.
%
%   info = CDF_INFO(filename, ...)
%   info: STRUCT containing details of the CDF file.
%   status: error status, 0 for success, 1 for error
%   mesg: CHAR error message, or empty if no errors
%
% CDF_INFO returns information about the variables, variable attributes,
% global attributes and other information contained in a CDF file. The
% name/value pairs listed below can be used to modify the default behaviour
% (they are performed in the order described):
%
%   'numeric_to_double', LOGICAL
%   Flag to indicate if numeric values (see ISNUMERIC) should be converted
%   to Matlab's DOUBLE class. The default is to not convert numeric classes
%   to DOUBLE.
%
%   'convert_epoch', LOGICAL
%   Flag to indicate if values of CDF data type 'EPOCH' are converted to
%   TIMESTAMP objects, or left as DOUBLE values. The default is to convert
%   to TIMESTAMPs.
%
%   'output_structs', LOGICAL
%   Determine whether certain information should be output as STRUCTs or
%   CELL arrays. The default is to output STRUCTs. It is possible for the
%   CDF file to contain variables or attributes whose names make illegal
%   STRUCT fieldnames, when possible the fields will be mapped to a legal
%   name, or a specific new name can be given.
%
%   'attribute_rename', STRUCT or CELL (See CDF_FORMATPARAMETERS)
%   If desired attributes may be renamed for Matlab (possibly because they
%   are not valid as field names for Matlab STRUCTs). In CDF files
%   attributes and variables may have the same names, though global
%   attributes and variable attributes share the same namespace. See
%   CDF_FORMATPARAMETERS for details about the STRUCT or CELL format.
%
%   'variable_rename', STRUCT or CELL (See CDF_FORMATPARAMETERS)
%   If desired variables may be renamed for Matlab (possibly because they
%   are not valid as field names for Matlab STRUCTs). In CDF files
%   attributes and variables may have the same names, though global
%   attributes and variable attributes share the same namespace. See
%   CDF_FORMATPARAMETERS for details about the STRUCT or CELL format.
%
% See also CDF_READ.

defaults.output_structs = 1;
defaults.numeric_to_double = 0;
defaults.convert_epoch = 1;

defaults.attribute_rename = {};
defaults.variable_rename = {};

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isstruct(filename)
  % cdf_read has asked us to format the info it already fetched
  info = filename;
  status = 0;
  mesg = '';
else
  % user called us directly
  [info status mesg] = cdf_info_mex(filename);
  
  if status
    % Some kind of error
    if nargout < 2
      % user not testing for errors
      error(mesg);
    else
      return
    end
  end
end


if logical(defaults.numeric_to_double)
  % Convert the variables_metadata values
  for m = 1:size(info.variables_metadata, 1)
    fn = fieldnames(info.variables_metadata{m, 2});
    for n = 1:numel(fn)
      tmp = getfield(info.variables_metadata{m,2}, fn{n});
      if isnumeric(tmp)
        info.variables_metadata{m, 2} = ...
            setfield(info.variables_metadata{m, 2}, fn{n}, double(tmp));
      end
    end
  end

  % Convert any values in the variable attributes
  for m = 1:size(info.variable_attributes, 1)
    for n = 1:size(info.variable_attributes{m, 2}, 1)

      % Convert the variable attribute's value
      if isnumeric(info.variable_attributes{m, 2}{n, 2})
        info.variable_attributes{m, 2}{n, 2} = ...
            double(info.variable_attributes{m, 2}{n, 2});
      end
      
      % At present no meta data needs converting 
      % % Convert the variable attribute's meta data
      % s = info.variable_attributes{m, 2}{n, 3}; % metadata struct
      % fn = fieldnames(s);
      % for p = 1:numel(fn)
      %   tmp = getfield(s, fn{p});
      %   if isnumeric(tmp)
      %     s = setfield(s, fn{p}, double(tmp));
      %   end
      % end
      
    end
  end

  % Convert the global attributes
  for m = 1:size(info.global_attributes, 1)
    for n = 1:size(info.global_attributes{m, 2})
      % Convert global attributes data. Note that each attribute can have
      % multiple entries, each of different types.
      if isnumeric(info.global_attributes{m, 2}{n})
        info.global_attributes{m, 2}{n} = ...
            double(info.global_attributes{m, 2}{n});
      end
    end
  end
end


if logical(defaults.convert_epoch)
  % Convert the pad values
  for n = 1:size(info.variables_metadata, 1)
    if strcmpi(info.variables_metadata{n,2}.datatype, 'epoch')
      info.variables_metadata{n,2}.padvalue = ...
          timestamp('cdfepoch', info.variables_metadata{n,2}.padvalue);
    end
  end
  
  % Convert any values in the variable attributes
  for m = 1:size(info.variable_attributes, 1)
    for n = 1:size(info.variable_attributes{m, 2}, 1)
      if strcmpi(info.variable_attributes{m, 2}{n, 3}.datatype, 'epoch')
        info.variable_attributes{m, 2}{n, 2} = ...
            timestamp('cdfepoch', info.variable_attributes{m, 2}{n, 2});
      end
    end
  end
  
  % Convert the global attributes
  for m = 1:size(info.global_attributes, 1)
    for n = 1:size(info.global_attributes{m, 2})
      % Note that each attribute can have multiple entries, each of different
      % types.
      if strcmpi(info.global_attributes{m, 3}(n).datatype, 'epoch')
        info.global_attributes{m, 2}{n} = ...
            timestamp('cdfepoch', info.global_attributes{m, 2}{n});
      end
    end
  end
  
end

if logical(defaults.output_structs)
  % convert version and lib_version to strings
  info.version = sprintf('%d.%d.%d', info.version);
  info.lib_version = sprintf('%d.%d.%d%c', info.lib_version);
  ga = info.global_attributes;
  info.global_attributes = cdf_structify(ga(:, 1:2), ...
                                         defaults.attribute_rename);
  info.global_attributes_metadata = cdf_structify(ga(:, [1 3]), ...
                                                  defaults.attribute_rename);
  

  for n = 1:size(info.variable_attributes, 1)
    tmp = info.variable_attributes{n, 2};
    info.variable_attributes{n, 2} = ...
        cdf_structify(tmp(:, 1:2), defaults.variable_rename);

    info.variable_attributes_metadata{n, 1} = info.variable_attributes{n, 1};
    info.variable_attributes_metadata{n, 2} = ...
        cdf_structify(tmp(:, [1 3]), defaults.variable_rename);

  end
  if isempty(info.variable_attributes)
    info.variable_attributes_metadata = [];
  end
  
  % Field names of variable_attributes are attributes, so use
  % defaults.attribute_rename
  info.variable_attributes = cdf_structify(info.variable_attributes, ...
                                           defaults.attribute_rename);

  info.variable_attributes_metadata = ...
      cdf_structify(info.variable_attributes_metadata, ...
                    defaults.attribute_rename);
  
  info.variables_metadata = cdf_structify(info.variables_metadata, ...
                                          defaults.variable_rename);

end

