/* General utility functions for CDF tools which need mex functions  */

#include "cdf_tools_mex.h"


int cdf_tools_debug = 0;

void _cdfstatus(const char* file, long line, CDFstatus status)
{
  char cdf_message[CDF_ERRTEXT_LEN+1];
  
  if (status < CDF_WARN) {
    CDFerror(status, cdf_message);
    mexPrintf("%s:%ld\nCDF library error: %s\n",
	      file, (long)line, cdf_message);

    /* Close CDF file before quitting */
    CDFlib(CLOSE_, CDF_,
	   NULL_);
    mexErrMsgTxt("Cannot continue");
  }
  
  if (status < CDF_OK) {
    CDFerror(status, cdf_message);
    mexPrintf("%s:%ld\nCDF library warning: %s\n",
	      file, (long)line, cdf_message);
  }
  return;
}


void closeCdfWithError(const char *cp)
{
  CDFlib(CLOSE_, CDF_,
	 NULL_);
  mexErrMsgTxt(cp);
}

mxArray* createScalar(double d)
{
  mxArray *ap;
  ap = mxCreateDoubleMatrix(1, 1, mxREAL);
  *(mxGetPr(ap)) = d;
  return ap;
}


/* Return a cell array with data from the currently-selected CDF file */
mxArray* cdfRead(long *selected_vars, int num_selected_vars)
{
  long ell;
  int i;
  int variables_stored = 0;
  mxArray *r = NULL;
  mxArray *ap;
  
  long vars_in_cdf;

#ifdef CDF_VAR_NAME_LEN256
  char var_name[CDF_VAR_NAME_LEN256+1];
#else
  char var_name[CDF_VAR_NAME_LEN+1];
#endif
  long cdf_majority;
  long var_maxrec, var_numdims, var_datatype, var_datatype_size, var_numelems;
  long var_dimsizes[CDF_MAX_DIMS];

  /* Dimensions for Matlab matrix. Extra space as number of records is
   * appended as a new dimension */
  int mat_dims[CDF_MAX_DIMS + 1];
  int mat_ndim;
  int transpose_needed;
  long mat_numel;
  void *vp;
  
  long dim_zeros[CDF_MAX_DIMS];
  long dim_ones[CDF_MAX_DIMS];

  /* initialise dimension arrays with zeros and ones */
  for (i = 0; i < CDF_MAX_DIMS; ++i) {
    dim_zeros[i] = 0L;
    dim_ones[i] = 1L;
  }
  
  cdfstatus(CDFlib(GET_, CDF_NUMzVARS_, &vars_in_cdf,
		   GET_, CDF_MAJORITY_, &cdf_majority,
		   NULL_));
  
  if (selected_vars == NULL)
    /* Output all variables */
    num_selected_vars = (int)vars_in_cdf;

  /* Output is a cell matrix, with variables names (as in the CDF
   * file, but with trailing spaces trimmed) in column 1, and data in
   * column 2 */
  r = mxCreateCellMatrix(num_selected_vars, 2);

  /* Loop over all zVariables in the CDF file */
  for (ell = 0; ell < vars_in_cdf; ++ell) {

    /* Is this variable in the request list? If the request list is
     * NULL then output all variables */
    
    if (selected_vars != NULL) {
      int found = 0;
      for (i = 0; i < num_selected_vars; ++i) {
	if (selected_vars[i] == ell) {
	  found = 1;
	  break;
	}
      }
      if (!found)
	continue; /* Not in list, go to next variable */
    }
    
    /* select variable, get its name and other details */
    cdfstatus(CDFlib(SELECT_, zVAR_, ell,
		     GET_, zVAR_NAME_, var_name,
		     GET_, zVAR_DATATYPE_, &var_datatype,
		     GET_, zVAR_MAXREC_, &var_maxrec,
		     GET_, zVAR_NUMDIMS_, &var_numdims,
		     GET_, zVAR_NUMELEMS_, &var_numelems,
		     NULL_));
    removeTrailingSpaces(var_name);
    
    /* Now that data type is known can get data type size */
    cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, var_datatype, &var_datatype_size,
		   NULL_));
    


    if (var_numdims) {
      cdfstatus(CDFlib(GET_, zVAR_DIMSIZES_, var_dimsizes,
		       NULL_));
      /* Copy into matlab version */
      for (i = 0; i < var_numdims; ++i) {
	mat_dims[i] = var_dimsizes[i];
      }
      mat_dims[var_numdims] = var_maxrec + 1;
      mat_ndim = var_numdims + 1;
    }
    else {
      mat_dims[0] = 1;
      mat_dims[1] = var_maxrec + 1;
      mat_ndim = 2;
    }

    if (cdf_majority == ROW_MAJOR && var_numdims > 1
	&& mat_dims[0] > 1 && mat_dims[1] > 1) {
      /* Require transpose, but also have to swap rows and columns
       * before filling the matrix */
      int tmp = mat_dims[0];
      mat_dims[0] = mat_dims[1];
      mat_dims[1] = tmp;
      transpose_needed = 1;
    }
    else 
      transpose_needed = 0;
    
    
    /* Compute number of elements the matlab array will have */
    mat_numel = numel(mat_ndim, mat_dims);
    
    /* Set variable name in column 1 */
    mxSetCell(r, variables_stored, mxCreateString(var_name));

    
    /* Initialise for hyperread. Start at first element in first
     * record and read everything */
    cdfstatus(CDFlib(SELECT_, zVAR_RECNUMBER_, 0L,
		     SELECT_, zVAR_RECCOUNT_, var_maxrec + 1L,
		     SELECT_, zVAR_RECINTERVAL_, 1L,
		     NULL_));
      
    if (var_numdims) {
      /* Setting dimension indices/counts/intervals not applicable for
       * 0-dimensional variables */
      cdfstatus(CDFlib(SELECT_, zVAR_DIMINDICES_, dim_zeros,
		       SELECT_, zVAR_DIMCOUNTS_, var_dimsizes,
		       SELECT_, zVAR_DIMINTERVALS_, dim_ones,
		       NULL_));
    }



    if (var_datatype == CDF_CHAR || var_datatype == CDF_UCHAR) {
      /* Allocate space for data, remembering that string data types
       * have multiple elements at each value. Allow space for
       * terminating character.  Then hyperread it */
      long elem;
      char *cp;
      char *cp_end;
      vp = mxMalloc(((mat_numel * var_numelems) + 1) * var_datatype_size);
      cdfstatus(CDFlib(GET_, zVAR_HYPERDATA_, vp,
		       NULL_));
      /* Convert each string to a matlab string. To avoid unnecessary
       * copying of data start with the last string. Terminate it with
       * '\0' then convert to a string. Move back to the previous
       * string and terminate it - it doesn't matter that we have
       * overwritten the first char of the following string since that
       * has already been copied into the cell array. */
      ap = mxCreateCellArray(mat_ndim, mat_dims);

      cp_end = vp;
      cp_end += mat_numel * var_numelems;

      for (elem = mat_numel - 1; elem >= 0; --elem) {
	*cp_end = '\0';
	cp = cp_end - var_numelems;
	mxSetCell(ap, elem, mxCreateString(cp));
	cp_end = cp;
      }

      mxFree(vp);
    }
    else {
      vp = mxMalloc(mat_numel * var_datatype_size * var_numelems);
      cdfstatus(CDFlib(GET_, zVAR_HYPERDATA_, vp,
		     NULL_));

      ap = cdfDataToMatrix(vp, var_datatype, mat_ndim, mat_dims);
    }

    if (transpose_needed)
      transpose_matrix(ap);
    
    
    /* Set variable data in column 2 */
    mxSetCell(r, variables_stored + num_selected_vars, ap);
    
    
    
    ++variables_stored;
  }
  

  
  return r;
}


/* Return a struct describing the currently open CDF file */
mxArray* cdfInfo(const char* filename,
		  const char **variable_names,
		  long num_vars)
{
  mxArray *r = NULL;
  
  int nfields = 0;
  /* The last element in the list should be NULL. Then the number of
   * fields used can be counted at run-time and is easy to maintain. */
  const char *info_fields[] = {
    "version", /* [1 x 3  double], eg [7 2 1] */
    "lib_version", /* [1 x 4  double], eg [7 2 1 'a'] */
    "filename",
    "majority",
    "global_attributes",
    "variables_metadata",            /* Details about variables */
    "variable_attributes",
    NULL
  };

  long ell;
  long version, release, increment, majority;
  
  /* Array of matrices holding the metadata about each variable */
  mxArray **variable_metadata = NULL;

  mxArray *matrix_p = NULL; /* temporary pointer to a matrix */
  double *dp = NULL;

  
  nfields = 0;
  while (info_fields[nfields]) 
    ++nfields;
  
  r = mxCreateStructMatrix(1, 1, nfields, info_fields);

  /* Get version/release/increment of CDF file details etc*/
  cdfstatus(CDFlib(GET_, CDF_VERSION_, &version,
		   GET_, CDF_RELEASE_, &release,
		   GET_, CDF_INCREMENT_, &increment,
		   GET_, CDF_MAJORITY_, &majority,
		   NULL_));
  
  matrix_p = mxCreateDoubleMatrix(1, 3, mxREAL);
  dp = mxGetPr(matrix_p);
  dp[0] = version;
  dp[1] = release;
  dp[2] = increment;
  mxSetField(r, 0, "version", matrix_p);

  /* Get version/release/increment of CDF file details etc*/
  cdfstatus(CDFlib(GET_, LIB_VERSION_, &version,
		   GET_, LIB_RELEASE_, &release,
		   GET_, LIB_INCREMENT_, &increment,
		   NULL_));
  
  matrix_p = mxCreateDoubleMatrix(1, 3, mxREAL);
  dp = mxGetPr(matrix_p);
  dp[0] = version;
  dp[1] = release;
  dp[2] = increment;
  mxSetField(r, 0, "lib_version", matrix_p);

  /* set filename */
  mxSetField(r, 0, "filename",
	     mxCreateString(filename));

  switch (majority) {
    case ROW_MAJOR:
      mxSetField(r, 0, "majority", mxCreateString("row"));
      break;

    case COLUMN_MAJOR:
      mxSetField(r, 0, "majority", mxCreateString("column"));
      break;

    default:
      if (cdf_tools_debug)
	mexPrintf("Majority was %ld\n", majority);

      closeCdfWithError("Unknown majority");
      break;
    };
  
  variable_metadata = mxCalloc(num_vars, sizeof(mxArray*));

  
  /* Loop through all of the variables */
  for (ell = 0; ell < num_vars; ++ell) {
    /* select variable */
    cdfstatus(CDFlib(SELECT_, zVAR_, ell,
		     NULL_));
    
    if (cdf_tools_debug)
      mexPrintf("Processing variable '%s'\n", variable_names[ell]);
    
    variable_metadata[ell] = getCdfZVariableMetadata();
    
  }


  /* Copy the stored variable metadata into the output struct */
  matrix_p = mxCreateCellMatrix(num_vars, 2);
  
  for (ell = 0; ell < num_vars; ++ell) {
    mxSetCell(matrix_p, ell, mxCreateString(variable_names[ell]));
    mxSetCell(matrix_p, num_vars + ell, variable_metadata[ell]);
  }
  
  mxSetField(r, 0, "variables_metadata", matrix_p);
  mxFree(variable_metadata);
  

  

  /* Get global attributes */
  mxSetField(r, 0, "global_attributes", getGlobalAttributes());
  
  mxSetField(r, 0, "variable_attributes", getzVariableAttributes());
  
  return r;
}






/* Get the attributes for the given zVariable */
mxArray* getzVariableAttributes(void)
{
  mxArray *r;
  long num_attr, num_v_attr;
  long attr_scope;
  long ell;
#ifdef CDF_ATTR_NAME_LEN256
  char attr_name[CDF_ATTR_NAME_LEN256 + 1];
#else
  char attr_name[CDF_ATTR_NAME_LEN + 1];
#endif
#ifdef CDF_VAR_NAME_LEN256
  char var_name[CDF_VAR_NAME_LEN256 + 1];
#else
  char var_name[CDF_VAR_NAME_LEN + 1];
#endif
  long valid_attributes = 0;

#define METADATA_FIELDNAMES_LEN 3
  const char *metadata_fieldnames[METADATA_FIELDNAMES_LEN] = {
    "datatype",
    "datatype_value",
    "number_of_elements"};
  
  /* How many attributes are there in total, and v attributes? */
  cdfstatus(CDFlib(GET_, CDF_NUMATTRS_, &num_attr,
		   GET_, CDF_NUMvATTRS_, &num_v_attr,
		   NULL_));

  r = mxCreateCellMatrix(num_v_attr, 2);
  
  /* Loop through all of the attributes, remember the number
   * of those which apply to the variable we are looking for */
  for (ell = 0; ell < num_attr; ++ell) {
    if (valid_attributes == num_v_attr)
      break;

    /* select attribute */
    cdfstatus(CDFlib(SELECT_, ATTR_, ell,
		     GET_, ATTR_SCOPE_, &attr_scope,
		     NULL_));

    if (attr_scope == VARIABLE_SCOPE) {
      long entry_num, num_z_entries, max_z_entry;
      long valid_entries = 0;
      mxArray *entry_details;

      /* Get attribute name */
      cdfstatus(CDFlib(GET_, ATTR_NAME_, &attr_name,
		       GET_, ATTR_MAXzENTRY_, &max_z_entry,
		       GET_, ATTR_NUMzENTRIES_, &num_z_entries, 
		       NULL_));
      
      mxSetCell(r, valid_attributes, mxCreateString(attr_name));
      entry_details = mxCreateCellMatrix(num_z_entries, 3);
      mxSetCell(r, num_v_attr + valid_attributes, entry_details);
      
      /* Loop through all entries */
      for (entry_num = 0; entry_num <= max_z_entry; ++entry_num) {
	long entry_datatype, entry_num_elems;
	mxArray *entry_metadata;
	
	if (valid_entries == num_z_entries)
	  break;
	
	if (CDFlib(CONFIRM_, zENTRY_EXISTENCE_, entry_num,
		   NULL_) != CDF_OK)
	  continue;

	/* Select the zEntry */
	cdfstatus(CDFlib(SELECT_, zENTRY_, entry_num,
			 NULL_));

	/* Select the zVariable which the zEntry corresponds to. */
	if (CDFlib(SELECT_, zVAR_, entry_num,
		   GET_, zVAR_NAME_, var_name,
		   NULL_) != CDF_OK)
	  strcpy(var_name, "?"); /* No corresponding variable */

	mxSetCell(entry_details, valid_entries, mxCreateString(var_name));
	mxSetCell(entry_details, num_z_entries + valid_entries,
		  getEntryData(VARIABLE_SCOPE, &entry_datatype,
			       &entry_num_elems));
	
	entry_metadata = mxCreateStructMatrix(1, 1,
					      METADATA_FIELDNAMES_LEN,
					      metadata_fieldnames);
	
	mxSetField(entry_metadata, 0, "datatype",
		   mxCreateString(datatype_to_string(entry_datatype)));
	mxSetField(entry_metadata, 0, "datatype_value",
		   createScalar(entry_datatype));
	mxSetField(entry_metadata, 0, "number_of_elements",
		   createScalar(entry_num_elems));


	mxSetCell(entry_details, (2*num_z_entries) + valid_entries,
		  entry_metadata);

	++valid_entries;
      }

      
      ++valid_attributes;
    }
  }
  
  return r;
}


mxArray* getEntryData(long scope, long *datatype_p, long *num_elems_p)
{
  mxArray *r = NULL;
  long datatype, datatype_size, num_elems;
  
  int dims[CDF_MAX_DIMS];
  char *cp = NULL;
  
  long cmd_entry_data = 0; /* CDFlib command to get the data */
  
  switch (scope) {
  case GLOBAL_SCOPE:
    cdfstatus(CDFlib(GET_, gENTRY_DATATYPE_, &datatype,
		     GET_, gENTRY_NUMELEMS_, &num_elems,
		     NULL_));

    cmd_entry_data = gENTRY_DATA_;
    break;
	
  case VARIABLE_SCOPE:
    cdfstatus(CDFlib(GET_, zENTRY_DATATYPE_, &datatype,
		     GET_, zENTRY_NUMELEMS_, &num_elems,
		     NULL_));

    cmd_entry_data = zENTRY_DATA_;
    break;

  default:
    /* Close CDF file before quitting */
    closeCdfWithError("Unknown attribute scope");
    break;
  };
	    
  cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		   NULL_));


  if (datatype == CDF_CHAR || datatype == CDF_UCHAR) {
    cp = mxCalloc(num_elems + 1, datatype_size); 
    cdfstatus(CDFlib(GET_, cmd_entry_data, cp,
		     NULL_));
    cp[num_elems] = '\0';
    r = mxCreateString(cp);
    mxFree(cp);
  }
  else {
    void *vp;
    dims[0] = 1;
    dims[1] = num_elems;
    vp = mxMalloc(datatype_size * num_elems);
    cdfstatus(CDFlib(GET_, cmd_entry_data, vp,
		     NULL_));
    r = cdfDataToMatrix(vp, datatype, 2, dims);
  }

  if (datatype_p != NULL)
    *datatype_p = datatype;
  
  if (num_elems_p != NULL)
    *num_elems_p = num_elems;

  
  return r;
}


char** getCdfVariableNames(long *num_vars_p /* out */
			   )
{
#ifdef CDF_VAR_NAME_LEN256
  char var_name[CDF_VAR_NAME_LEN256+1];
#else
  char var_name[CDF_VAR_NAME_LEN+1];
#endif
  long ell;
  long num_vars;
  char **variable_names = NULL;
  
  /* Get variable details */
  /* How many z entries are there? */
  cdfstatus(CDFlib(GET_, CDF_NUMzVARS_, &num_vars,
		   NULL_));


  /* Allocate space for all the variable names. Also terminate the
   * list with a NULL pointer. */
  variable_names = mxCalloc(num_vars + 1, sizeof(char*));

  /* Loop through all of the variables */
  for (ell = 0; ell < num_vars; ++ell) {
    /* select variable and get its name*/
    cdfstatus(CDFlib(SELECT_, zVAR_, ell,
		     GET_, zVAR_NAME_, var_name,
		     NULL_));
    removeTrailingSpaces(var_name);
    variable_names[ell] = (char *)mxMalloc(sizeof(char) *
					   (strlen(var_name) + 1));
    strcpy(variable_names[ell], var_name);
  }

  variable_names[num_vars] = NULL;

  /* Store the number of variables found */
  if (num_vars_p)
    *num_vars_p = num_vars;
  
  return variable_names;
}




/* For the currently-selected variable return the metadata as a struct */
mxArray* getCdfZVariableMetadata(void)
{
  mxArray *r, *ap;

  /* The last element in the list should be NULL. Then the number of
   * fields used can be counted at run-time and is easy to maintain. */
#define Z_VARIABLE_METDATA_FIELDS 10
  const char *fieldnames[Z_VARIABLE_METDATA_FIELDS] = {
    "name",
    "datatype",
    "datatype_value",        
    "padvalue",
    "records", /* actually zVAR_MAXREC_ + 1 */
    "number_of_dimensions",
    "dimension_sizes",
    "number_of_elements",
    "recvary",
    "dimsvary"
  };
#ifdef CDF_VAR_NAME_LEN256
  char var_name[CDF_VAR_NAME_LEN256+1];
#else
  char var_name[CDF_VAR_NAME_LEN+1];
#endif

  double *dp;
  int pad_dims[2];

  long datatype, datatype_size, data_numelems, data_maxrec, data_numdims;
  long data_recvary;
  long data_dimvarys[CDF_MAX_DIMS];
  
  r = mxCreateStructMatrix(1, 1, Z_VARIABLE_METDATA_FIELDS, fieldnames);

  cdfstatus(CDFlib(GET_, zVAR_NAME_, var_name,
		   GET_, zVAR_DATATYPE_, &datatype,
		   GET_, zVAR_NUMELEMS_, &data_numelems,
		   GET_, zVAR_MAXREC_, &data_maxrec,
		   GET_, zVAR_NUMDIMS_, &data_numdims,
		   GET_, zVAR_RECVARY_, &data_recvary,
		   GET_, zVAR_DIMVARYS_, data_dimvarys,
		   NULL_));

  cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		   NULL_));

  removeTrailingSpaces(var_name);
  mxSetField(r, 0, "name", mxCreateString(var_name));

  mxSetField(r, 0, "datatype",
	     mxCreateString(datatype_to_string(datatype)));

  mxSetField(r, 0, "datatype_value", createScalar(datatype));

  mxSetField(r, 0, "recvary", createScalar(data_recvary == VARY));

  
  
  if (CDFlib(CONFIRM_, zVAR_PADVALUE_,
	     NULL_) == CDF_OK) {
  
    /* When the data is CDF_CHAR or CDF_UCHAR CDFlib copies multiple
     * elements for the PADVALUE. Therefore must assign space for
     * zVAR_NUMELEMS_elements. Note that for strings an extra space is
     * needed for the terminating NULL which Matlab requires. */
    if (datatype == CDF_CHAR || datatype == CDF_UCHAR) {
      char *padvalue_str;
      padvalue_str = mxCalloc(data_numelems + 1, datatype_size);
      CDFlib(GET_, zVAR_PADVALUE_, padvalue_str,
	     NULL_);
      padvalue_str[data_numelems] = '\0';
      mxSetField(r, 0, "padvalue", mxCreateString(padvalue_str));
    }
    else {
      /* Note that padvalue is not de-allocated
       * until the array it is inserted into is deallocated. */
      void *padvalue_p = NULL;
      padvalue_p = mxCalloc(data_numelems, datatype_size);
      cdfstatus(CDFlib(GET_, zVAR_PADVALUE_, padvalue_p,
		       NULL_));
      pad_dims[0] = 1;
      pad_dims[1] = data_numelems;
      mxSetField(r, 0, "padvalue",
		 cdfDataToMatrix(padvalue_p, datatype, 2, pad_dims));
    }
  }
  else {
    if (datatype == CDF_CHAR || datatype == CDF_UCHAR)
      mxSetField(r, 0, "padvalue", mxCreateString(""));
    else {
      pad_dims[0] = 0;
      pad_dims[1] = 0;
      mxSetField(r, 0, "padvalue",
		 mxCreateNumericArray(2, pad_dims,
				      cdfDataTypeToMatlab(datatype), mxREAL));
    }
  }
  
  ap = mxCreateDoubleMatrix(1, 1, mxREAL);
  dp = mxGetPr(ap);
  dp[0] = data_maxrec + 1;
  mxSetField(r, 0, "records", ap);

  ap = mxCreateDoubleMatrix(1, 1, mxREAL);
  dp = mxGetPr(ap);
  dp[0] = data_numdims;
  mxSetField(r, 0, "number_of_dimensions", ap);

  if (data_numdims) {
    /* Dimensions > 0, fetch and copy into matlab array */
    int i;
    long data_dims[CDF_MAX_DIMS];
    cdfstatus(CDFlib(GET_, zVAR_DIMSIZES_, &data_dims,
		     NULL_));
    ap = mxCreateDoubleMatrix(1, data_numdims, mxREAL);
    dp = mxGetPr(ap);
    for (i = 0; i < data_numdims; ++i)
      dp[i] = data_dims[i];

    mxSetField(r, 0, "dimension_sizes", ap);
    
    /* dimvarys */
    ap = mxCreateDoubleMatrix(1, data_numdims, mxREAL);
    dp = mxGetPr(ap);
    for (i = 0; i < data_numdims; ++i)
      dp[i] = (data_dimvarys[i] == VARY);  
    
    mxSetField(r, 0, "dimsvary", ap);

    
  }

  /* number_of_elements */
  ap = mxCreateDoubleMatrix(1, 1, mxREAL);
  dp = mxGetPr(ap);
  dp[0] = data_numelems;
  mxSetField(r, 0, "number_of_elements", ap);


  
  
  return r;
}



/* Insert data into a matrix. Note that *data must have been allocated
 * by mxCalloc (see mxSetData and mxSetPr) */

mxArray* cdfDataToMatrix(void *data, long cdf_datatype,
			 int ndim, const int *dims)
{
  mxArray *r = NULL;
  /* long numel = 1;  number of elements in matlab matrix */
  mxClassID mat_datatype;
  long datatype_size;
  int emptydims[2] = {0, 0};
 
  cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, cdf_datatype, &datatype_size,
		   NULL_));
  
  mxAssert(ndim >= 2, "Incorrect value for ndim");

 
  /* map the CDF data type to something similar for Matlab */
  mat_datatype = cdfDataTypeToMatlab(cdf_datatype);
  
  switch (mat_datatype) {
  case mxINT8_CLASS:
  case mxINT16_CLASS:
  case mxINT32_CLASS:
  case mxINT64_CLASS: /* reserved by Mathworks for future */
  case mxUINT8_CLASS:
  case mxUINT16_CLASS:
  case mxUINT32_CLASS:
  case mxUINT64_CLASS: /* reserved by Mathworks for future */
  case mxSINGLE_CLASS:
  case mxDOUBLE_CLASS:
    /* create an empty matrix initially */
    r = mxCreateNumericArray(2, emptydims, mat_datatype, mxREAL);
    mxSetData(r, data);
    mxSetDimensions(r, dims, ndim);
    break;

  case mxCHAR_CLASS:
    /* Cannot easily create char matrices from arrays of chars since
     * internally the chars are stored in unsigned shorts, and the
     * functions to creates char matrices use null-terminated
     * strings. Therefore create as uint8 and use an M-file in Matlab
     * to convert. */
    r = mxCreateNumericArray(2, emptydims, mxUINT8_CLASS, mxREAL);
    mxSetData(r, data);
    mxSetDimensions(r, dims, ndim);
    break;

    
    /* these cases shouldn't happen, but handle gracefully */
  case mxCELL_CLASS:
  case mxSTRUCT_CLASS:
  case mxOBJECT_CLASS:
  case mxSPARSE_CLASS:
    /* How did it get this? Create empty double matrix */
    if (cdf_tools_debug)
      mexPrintf("mxClassID = %d\n", (int)mat_datatype);

    mexWarnMsgTxt("Unknown error in cdfDataTypeToMatlab()");
    r = mxCreateDoubleMatrix(0, 0, mxREAL);
    break;
    
  default:
    if (cdf_tools_debug)
      mexPrintf("Unknown mxClassID (%d)\n", (int)mat_datatype);

    mexWarnMsgTxt("Unknown mxClassID in cdfDataTypeToMatlab()");
    r = mxCreateDoubleMatrix(0, 0, mxREAL);
    break;
  };

  return r;
}


mxClassID cdfDataTypeToMatlab(long datatype)
{
#ifdef NDEBUG
  long datatype_size;
  cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		   NULL_));
#endif
  
  switch (datatype) {

  case CDF_BYTE:
    mxAssert(datatype_size == sizeof(UINT8_T),
	     "CDF_BYTE size does not match sizeof(UINT8_T)");
    return mxUINT8_CLASS;
    break;

  case CDF_INT1:
    mxAssert(datatype_size == sizeof(INT8_T),
	     "CDF_INT1 size does not match sizeof(INT8_T)");
    return mxINT8_CLASS;
    break;

  case CDF_UINT1:
    mxAssert(datatype_size == sizeof(UINT8_T),
	     "CDF_UINT1 size does not match sizeof(UINT8_T)");
    return mxUINT8_CLASS;
    break;
    
  case CDF_INT2:
    mxAssert(datatype_size == sizeof(INT16_T),
	     "CDF_INT2 size does not match sizeof(INT16_T)");
    return mxINT16_CLASS;
    break;

  case CDF_UINT2:
    mxAssert(datatype_size == sizeof(UINT16_T),
	     "CDF_UINT2 size does not match sizeof(UINT16_T)");
    return mxUINT16_CLASS;
    break;
    
  case CDF_INT4:
    mxAssert(datatype_size == sizeof(INT32_T),
	     "CDF_INT4 size does not match sizeof(INT32_T)");
    return mxINT32_CLASS;
    break;

  case CDF_UINT4:
    mxAssert(datatype_size == sizeof(UINT32_T),
	     "CDF_UINT4 size does not match sizeof(UINT32_T)");
    return mxUINT32_CLASS;
    break;

  case CDF_REAL4:
    mxAssert(datatype_size == sizeof(float),
	     "CDF_REAL4 size does not match sizeof(float)");
    return mxSINGLE_CLASS;
    break;

  case CDF_FLOAT:
    mxAssert(datatype_size == sizeof(float),
	     "CDF_UINT4 size does not match sizeof(float)");
    return mxSINGLE_CLASS;
    break;

  case CDF_REAL8:
    mxAssert(datatype_size == sizeof(double),
	     "CDF_REAL8 size does not match sizeof(double)");
    return mxDOUBLE_CLASS;
    break;

  case CDF_DOUBLE:
    mxAssert(datatype_size == sizeof(double),
	     "CDF_DOUBLE size does not match sizeof(double)");
    return mxDOUBLE_CLASS;
    break;

  case CDF_EPOCH:
    mxAssert(datatype_size == sizeof(double),
	     "CDF_EPOCH size does not match sizeof(double)");
    return mxDOUBLE_CLASS;
    break;

#ifdef CDF_EPOCH16
  case CDF_EPOCH16:
    mexWarnMsgTxt("CDF EPOCH16 class not supported");
    return mxUNKNOWN_CLASS;
    break;
#endif

  case CDF_CHAR:
    mxAssert(datatype_size == sizeof(char),
	     "CDF_CHAR size does not match sizeof(char)");
    return mxCHAR_CLASS;
    break;

  case CDF_UCHAR:
    mxAssert(datatype_size == sizeof(unsigned char),
	     "CDF_UCHAR size does not match sizeof(unsigned char)");
    return mxCHAR_CLASS;
    break;
    
  default:
    if (cdf_tools_debug)
      mexPrintf("Found unknown CDF data type #%ld\n", datatype);

    mexWarnMsgTxt("Unknown CDF data type");
    break;
    
  };

  /* shouldn't reach here */
  return mxUNKNOWN_CLASS;
}


long matlabArrayToCdfDataType(const mxArray *matrix)
{
  mxClassID id;
  long datatype;
#ifdef NDEBUG
  long datatype_size;
#endif
  
  id = mxGetClassID(matrix);
  
  switch (id) {
  case mxINT8_CLASS:
    datatype = CDF_INT1;
    break;

  case mxINT16_CLASS:
    datatype = CDF_INT2;
    break;

  case mxINT32_CLASS:
    datatype = CDF_INT4;
    break;
    
  case mxUINT8_CLASS:
    datatype = CDF_UINT1;
    break;

  case mxUINT16_CLASS:
    datatype = CDF_UINT2;
    break;

  case mxUINT32_CLASS:
    datatype = CDF_UINT4;
    break;

  case mxSINGLE_CLASS:
    datatype = CDF_FLOAT;
    break;

    /* Can treat sparse matrix as double */
  case mxDOUBLE_CLASS:
  case mxSPARSE_CLASS:
    datatype = CDF_DOUBLE;
    break;

  case mxCHAR_CLASS:
    /* No direct mapping since Matlab uses mxChar, which is 2 bytes
     * long, so avoid the assertion below */
    return CDF_CHAR;
    break;
    
  default:
    return -1;
    break;
  };
  
  mxGetElementSize(matrix);
  
#ifdef NDEBUG
  cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		   NULL_));
  mxAssert(datatype_size == mxGetElementSize(matrix),
	   "Matlab and CDF data sizes differ");
#endif
  
  return datatype;
}


mxArray* getGlobalAttributes(void)
{
  mxArray *r;
  long num_attr;
  long attr_scope;
  long ell;
#ifdef CDF_ATTR_NAME_LEN256
  char attr_name[CDF_ATTR_NAME_LEN256 + 1];
  #else
  char attr_name[CDF_ATTR_NAME_LEN + 1];
#endif
  long nfields = 0;
#define METADATA_FIELDNAMES_LEN 3
  const char *metadata_fieldnames[METADATA_FIELDNAMES_LEN] = {
    "datatype",
    "datatype_value",
    "number_of_elements"};
  mxArray **attribute_names;
  mxArray **attribute_data;
  mxArray **attribute_metadata;
  
  /* How many attributes are there? */
  cdfstatus(CDFlib(GET_, CDF_NUMATTRS_, &num_attr,
		   NULL_));
  
  attribute_names = mxCalloc(num_attr, sizeof(mxArray*));
  attribute_data = mxCalloc(num_attr, sizeof(mxArray*));
  attribute_metadata = mxCalloc(num_attr, sizeof(mxArray*));
  
  /* Loop through all of the attributes */
  for (ell = 0; ell < num_attr; ++ell) {
    /* select attribute */
    cdfstatus(CDFlib(SELECT_, ATTR_, ell,
		     GET_, ATTR_NAME_, &attr_name,
		     GET_, ATTR_SCOPE_, &attr_scope,
		     NULL_));

    if (attr_scope == GLOBAL_SCOPE) {
      long max_g_entry, num_g_entries, entrynum, datatype, numelems;
      long valid_entries = 0;
      attribute_names[nfields] = mxCreateString(attr_name);

      cdfstatus(CDFlib(GET_, ATTR_MAXgENTRY_, &max_g_entry,
		       GET_, ATTR_NUMgENTRIES_, &num_g_entries,
		       NULL_));
      
      /* Create cell matrix in which to store the data */
      attribute_data[nfields] = mxCreateCellMatrix(num_g_entries,
					      num_g_entries ? 1 : 0);

      /* Create struct in which to store the metadata */
      attribute_metadata[nfields]
	= mxCreateStructMatrix(num_g_entries, (num_g_entries ? 1 : 0),
			       METADATA_FIELDNAMES_LEN, metadata_fieldnames);

      for (entrynum = 0; entrynum <= max_g_entry; ++entrynum) {
	if (CDFlib(CONFIRM_, gENTRY_EXISTENCE_, entrynum,
		   SELECT_, gENTRY_, entrynum,
		   NULL_) == CDF_OK) {
	  mxSetCell(attribute_data[nfields], valid_entries,
		    getEntryData(attr_scope, &datatype, &numelems));
	  mxSetField(attribute_metadata[nfields], valid_entries,
		     "datatype",
		     mxCreateString(datatype_to_string(datatype)));
	  mxSetField(attribute_metadata[nfields], valid_entries,
		     "datatype_value",
		     createScalar(datatype));
	  mxSetField(attribute_metadata[nfields], valid_entries,
		     "number_of_elements", createScalar(numelems));
	  ++valid_entries;
	}
      }

      ++nfields;
    }
  }
  
  r = mxCreateCellMatrix(nfields, 3);
  for (ell = 0; ell < nfields; ++ell) {
    mxSetCell(r, ell, attribute_names[ell]);
    if (attribute_data[ell] != NULL) {
      mxSetCell(r, nfields + ell, attribute_data[ell]);
      mxSetCell(r, (2*nfields) + ell, attribute_metadata[ell]);
    }
  }
  
  mxFree(attribute_names);
  mxFree(attribute_data);
  mxFree(attribute_metadata);

  return r;
}


void removeTrailingSpaces(char *cp)
{
  size_t len;
  size_t n;
  len = strlen(cp);

  for (n = len-1; n >= 0; --n) 
    if (cp[n] == ' ') {
      cp[n] = '\0';
      --len;
    }
    else
      break;
  return;
}


long numel(int ndim, const int *dims)
{
  int i;
  long r = 1;
  for (i = 0; i < ndim; ++i)
    r *= dims[i];
  return r;
}

const char* datatype_to_string(long datatype)
{
  switch (datatype) {
  case CDF_INT1:
    return "INT1";
    
  case CDF_INT2:
    return "INT2";

  case CDF_INT4:
    return "INT4";

  case CDF_UINT1:
    return "UINT1";

  case  CDF_UINT2:
    return "UINT2";
    
  case CDF_UINT4:
    return "UINT4";
    
  case CDF_REAL4:
    return "REAL4";
    
  case CDF_REAL8:
    return "REAL8";
  
  case CDF_EPOCH:
    return "EPOCH";

#ifdef CDF_EPOCH16
  case CDF_EPOCH16:
    return "EPOCH16";
#endif
    
  case CDF_BYTE:
    return "BYTE";
  
  case CDF_FLOAT:
    return "FLOAT";
  
  case CDF_DOUBLE:
    return "DOUBLE";
  
  case CDF_CHAR:
    return "CHAR";
  
  case CDF_UCHAR:
    return "UCHAR";

  };
  return "unknown data type";
}



const char* cdfWrite(const mxArray *filename_matrix,
		     const mxArray *data_matrix,
		     const mxArray *options_matrix)
{
  CDFid id;
  char filename[CDF_PATHNAME_LEN + 1];
  /*mxArray *metadata_matrix; */
  
  long status = 0;
  long num_vars;
  long ell;
  writemode_t writemode;
  long majority;
  const char *err_mesg = NULL;
  
  const mxArray *ap;
  
  if (mxGetString(filename_matrix, filename, CDF_PATHNAME_LEN))
    return "Could not copy filename matrix (name too long?)";

#ifdef BACKWARDFILEon
  if ((ap = mxGetField(options_matrix, 0, "backward_compatibility")) != NULL
      && mxGetNumberOfElements(ap) && mxGetScalar(ap) == 0)
    /* set backward compatibility mode off */
    CDFsetFileBackward(BACKWARDFILEoff);
  else 
    CDFsetFileBackward(BACKWARDFILEon);
#endif
  
  /* Open in appropriate mode */
  writemode = (writemode_t)getFieldValue(options_matrix, "writemode_enum", 0);
  
  switch (writemode) {
  case writemode_overwrite:
    if (CDFlib(OPEN_, CDF_, filename, &id,
	       NULL_) == CDF_OK) {
      /* CDF file exists */
      if (CDFlib(SELECT_, CDF_READONLY_MODE_, READONLYoff,
		 DELETE_, CDF_,
		 NULL_) != CDF_OK)
	return "Cannot remove existing CDF file";
    }
    {
      /* Create the CDF file */
      long numdims = 0;
      long dimsizes = 0;
      if (CDFlib(CREATE_, CDF_, filename, numdims, &dimsizes, &id,
		 NULL_) != CDF_OK) 
	return "Could not open CDF file";
    }
      
    break;
    
  case writemode_append:
    /* Open the CDF file */
    if (CDFlib(OPEN_, CDF_, filename, &id,
	       NULL_) != CDF_OK) 
      return "Could not open CDF file";
    break;

  default:
    return "Unknown write mode";
    break;
  };
  

    
    
  /* Ensure read-only mode is off and zMode 2 is on */
  cdfstatus(CDFlib(SELECT_, CDF_READONLY_MODE_, READONLYoff,
		   SELECT_, CDF_zMODE_, zMODEon2,
		   NULL_));


  switch ((int)getFieldValue(options_matrix, "majority_enum", 0)) {
  case majority_default:
    /* Fetch current majority setting */
    cdfstatus(CDFlib(GET_, CDF_MAJORITY_, &majority,
		     NULL_));
    status = CDF_OK;
    break;
    
  case majority_row:
    majority = ROW_MAJOR;
    status = CDFlib(PUT_, CDF_MAJORITY_, majority,
		    NULL_);
    break;
    
  case majority_column:
    majority = COLUMN_MAJOR;
    status = CDFlib(PUT_, CDF_MAJORITY_, majority,
		    NULL_);
    break;

  default:
    err_mesg = "Unknown majority mode";
    goto close_cdf_file;
    break;
  };
  
  if (status != CDF_OK) {
    /* The reason is probably that variables already exist in the
     * file, and the majority mode is different to that already
     * set. Carry on regardless, but fetch the actual majority value
     * in use. */
    mexWarnMsgTxt("Couldn't set CDF majority");
    cdfstatus(CDFlib(GET_, CDF_MAJORITY_, &majority,
		     NULL_));
  }

  /* Delete any global attributes */
  if((ap = mxGetField(options_matrix, 0,
		      "delete_global_attributes")) != NULL) {
    if (mxIsCell(ap)) {
      int i, n;
      n = mxGetNumberOfElements(ap);
      for (i = 0; i < n; ++i) {
	char *var_name;
	const mxArray* attr_name_matrix;
	attr_name_matrix = mxGetCell(ap, i);
	if (mxIsChar(attr_name_matrix) &&
	    (var_name = mxArrayToString(attr_name_matrix)) != NULL) {

	  err_mesg = deleteGlobalAttribute(var_name);
	  mxFree(var_name);
	  if (err_mesg != NULL)
	    goto close_cdf_file;
	  
	}
      }
    }
    else
      mexWarnMsgTxt("delete_global_attributes must be a cell matrix");
  }
  
  /* Delete any variable attrbiutes. */
  if((ap = mxGetField(options_matrix, 0,
		      "delete_variable_attributes")) != NULL) {
    if (mxIsCell(ap)) {
      int i, n;
      n = mxGetNumberOfElements(ap);
      for (i = 0; i < n; ++i) {
	char *attr_name, *var_name;
	const mxArray* attr_name_matrix;
	attr_name_matrix = mxGetCell(ap, i);
	
	switch (mxGetClassID(attr_name_matrix)) {
	case mxCHAR_CLASS:
	  if ((attr_name = mxArrayToString(attr_name_matrix)) != NULL) {
	    err_mesg = deleteVariableAttribute(attr_name, NULL);
	    mxFree(attr_name);
	    if (err_mesg != NULL)
	      goto close_cdf_file;
	  }
	  break;

	case mxCELL_CLASS:
	  if (mxGetNumberOfElements(attr_name_matrix) == 2) {
	    const mxArray *an, *vn;
	    an = mxGetCell(attr_name_matrix, 0);
	    vn = mxGetCell(attr_name_matrix, 1);
	    if (mxIsChar(an) && mxIsChar(vn)) {
	      attr_name = mxArrayToString(an);
	      var_name = mxArrayToString(an);
	      err_mesg = deleteVariableAttribute(attr_name, var_name);
	      mxFree(attr_name);
	      mxFree(var_name);
	      if (err_mesg != NULL)
		goto close_cdf_file;
	    }

	  }
	  break;

	default:
	  mexWarnMsgTxt("Bad element type for delete_variable_attributes");
	  break;
	};
      }
      
    }
    else
      mexWarnMsgTxt("delete_variable_attributes must be a cell matrix");
  }

  /* Delete any variables */
  if((ap = mxGetField(options_matrix, 0,
		      "delete_variables")) != NULL) {
    if (mxIsCell(ap)) {
      int i, n;
      n = mxGetNumberOfElements(ap);
      for (i = 0; i < n; ++i) {
	char *var_name;
	const mxArray* var_name_matrix;
	var_name_matrix = mxGetCell(ap, i);
	if (mxIsChar(var_name_matrix) &&
	    (var_name = mxArrayToString(var_name_matrix)) != NULL) {
	  err_mesg = deleteVariable(var_name);
	  mxFree(var_name);
	  if (err_mesg != NULL) 
	    goto close_cdf_file;

	}
      }
    }
    else
      mexWarnMsgTxt("delete_variables must be a cell matrix");
  }

  
  /* Write the global attributes */
  putGlobalAttributes(mxGetField(options_matrix, 0,
				 "global_attributes_cell"));

  
  /* Number of variables is the number of rows */
  num_vars = mxGetM(data_matrix);

  for (ell = 0; ell < num_vars; ++ell) {
    char *var_name; 
    const mxArray *varname_matrix;
    varname_matrix = mxGetCell(data_matrix, ell);
    if (!mxIsChar(varname_matrix)) {
      err_mesg = "Variable name not a char";
      goto close_cdf_file;
    }
    
    var_name = mxArrayToString(varname_matrix);
    
    err_mesg = putVariable(var_name, majority,
			   mxGetCell(data_matrix, num_vars + ell),
			   mxGetCell(data_matrix, (2*num_vars) + ell));
    mxFree(var_name);
    if (err_mesg != NULL)
      goto close_cdf_file;
    
  }

  
  err_mesg = putVariableAttributes(mxGetField(options_matrix, 0,
					      "variable_attributes_cell"));
  if (err_mesg != NULL)
    goto close_cdf_file;
    
  /* Close CDF file */
 close_cdf_file:
  cdfstatus(CDFlib(CLOSE_, CDF_,
		   NULL_));
  
  return err_mesg;
}


/* Delete file. Returns NULL on success, or pointer to an error
 * message. */
const char* cdfDelete(const char *filename)
{
  long status;
  long cdfid;

  status = CDFlib(OPEN_, CDF_, filename, &cdfid,
		  NULL_);

  switch (status) {
  case CDF_OPEN_ERROR:
    return "Open access denied"; /* Open access denied */
    break;
    
  case NO_SUCH_CDF:
    return "File not found";
    break;

  case CDF_READ_ERROR:
    return "Invalid CDF file";
    break;
  };

  if (status < CDF_OK) {
    return "Could not access file";
  }

  status = CDFlib(SELECT_, CDF_READONLY_MODE_, READONLYoff,
		  DELETE_, CDF_,
		  NULL_);
    
  if (status < CDF_OK) {
    CDFlib(CLOSE_, CDF_,
	   NULL_);
    return "Could not delete file";
  }

  /* success */
  return NULL;
}


double getFieldValue(const mxArray *ap, const char *fieldname, int offset)
{
  const mxArray *valuep;
  int numel;
  void *vp;
  double d;
  
  mxClassID classid;

  if (!mxIsStruct(ap))
    closeCdfWithError("Not a struct");
  
   
  if (offset < 0) 
    closeCdfWithError("Offset < 0");
  
  if ((valuep = mxGetField(ap, 0, fieldname)) == NULL) {
    if (cdf_tools_debug)
      mexPrintf("Cannot get field '%s'\n", fieldname);

    closeCdfWithError("Cannot get field");
  }
    
  numel = mxGetNumberOfElements(valuep);
  if (offset >= numel)
    closeCdfWithError("Offset too large for array");
    
  classid = mxGetClassID(valuep);

  vp = mxGetData(valuep);
  
  
  switch (classid) {
  case mxINT8_CLASS:
    d = ((INT8_T*)vp)[offset];
    break;

  case mxINT16_CLASS:
    d = ((INT16_T*)vp)[offset];
    break;

  case mxINT32_CLASS:
    d = ((INT32_T*)vp)[offset];
    break;

  case mxUINT8_CLASS:
    d = ((UINT8_T*)vp)[offset];
    break;

  case mxUINT16_CLASS:
    d = ((UINT16_T*)vp)[offset];
    break;

  case mxUINT32_CLASS:
    d = ((UINT32_T*)vp)[offset];
    break;

  case mxSINGLE_CLASS:
    d = ((float*)vp)[offset];
    break;

  case mxDOUBLE_CLASS:
    d = ((double*)vp)[offset];
    break;
    
  default:
    d = 0;
    if (cdf_tools_debug)
      mexPrintf("Could not get value, matrix class='%s', fieldname='%s', "
		"offset=%d\n", mxGetClassName(valuep), fieldname, offset);

    closeCdfWithError("Could not get value, unknown or non-numeric matrix");
    break;
  }
  
  return d;
}


const char* putVariable(const char* var_name, long majority, 
			const mxArray *data, const mxArray *metadata)
{
  long datatype, numelems, numdims, recvary, varnum, records;
  long dimsizes[CDF_MAX_DIMS];
  long dimvarys[CDF_MAX_DIMS];
  int i, transpose_needed;

  long start_record_num; 
  long records_to_write; /* Number of CDF records */
  long values_to_write; /* Total number of values to write */
  const mxArray *ap;
  mxArray *data_copy = NULL;
  const mxArray *data_p;
  const mxArray *padvalue_matrix;
  
  datatype = getFieldValue(metadata, "datatype_value", 0);
  numelems = getFieldValue(metadata, "number_of_elements", 0);
  recvary = (getFieldValue(metadata, "recvary", 0) ? VARY : NOVARY);
  records = getFieldValue(metadata, "records", 0);


  if (cdf_tools_debug)
    mexPrintf("Putting %s (%s #%ld) from '%s'\n", var_name,
	      datatype_to_string(datatype), datatype, mxGetClassName(data));
    
    
  if ((ap = mxGetField(metadata, 0, "dimension_sizes")) == NULL) {
    if (cdf_tools_debug)
      mexPrintf("Could not get dimension_sizes field for variable '%s'\n",
		var_name);
    return "Could not get dimension_sizes field";
  }
  
  numdims = mxGetNumberOfElements(ap); 
  if (numdims > CDF_MAX_DIMS) {
    if (cdf_tools_debug)
      mexPrintf("Number of dimensions (%d) exceeds CDF_MAX_DIMS (%ld)\n",
		numdims, (long)CDF_MAX_DIMS);
    return "Number of dimensions exceeds CDF_MAX_DIMS";
  }
  
  for (i = 0; i < numdims; ++i)
    dimsizes[i] = getFieldValue(metadata, "dimension_sizes", i);

  if ((ap = mxGetField(metadata, 0, "dimvarys")) == NULL) {
    if (cdf_tools_debug)
      mexPrintf("Could not get dimvarys field for variable '%s'\n", var_name);

    return "Could not get dimvarys field";
  }
  
  if (numdims != mxGetNumberOfElements(ap)) {
    if (cdf_tools_debug)
      mexPrintf("Number of dimvarys elements differs from dimension_sizes "
		"for variable '%s'\n", var_name);

    return "Number of dimvarys elements differs from dimension_sizes";
  }
  
  for (i = 0; i < numdims; ++i)
    dimvarys[i] = (getFieldValue(metadata, "dimvarys", i) ? VARY : NOVARY);

  
  /* Select the variable (if it exists) and then find out which record
   * to append from. */
  if (CDFlib(SELECT_, zVAR_NAME_, var_name,
	     NULL_) == CDF_OK) {
    /* Variable already exists, check its details are compatible */
    long old_numdims, old_datatype, old_numelems, old_recvary;
    long old_dimvarys[CDF_MAX_DIMS];
    cdfstatus(CDFlib(GET_, zVAR_DATATYPE_, &old_datatype,
		     GET_, zVAR_NUMDIMS_, &old_numdims,
		     GET_, zVAR_MAXREC_, &start_record_num,
		     GET_, zVAR_NUMELEMS_, &old_numelems,
		     GET_, zVAR_RECVARY_, &old_recvary,
		     GET_, zVAR_DIMVARYS_, old_dimvarys,
		     NULL_));
    
    ++start_record_num; /* start after last record */
    
    if (old_numdims != numdims) {
      if (cdf_tools_debug)
	mexPrintf("Number of dimensions (%ld) cannot be changed from %ld "
		  "when appending data to variable '%s'\n",
		  numdims, old_numdims, var_name);
      return "Number of dimensions cannot be changed when appending data";
    }
    
    if (old_numelems != numelems) {
      if (cdf_tools_debug)
	mexPrintf("Number of elements (%ld) cannot be changed from %ld "
		  "when appending data to variable '%s'\n",
		  numelems, old_numelems, var_name);

      return "Number of dimensions cannot be changed when appending data "
	"to a variable";
    }

    if (old_recvary != recvary) {
      if (cdf_tools_debug)
	mexPrintf("recvary cannot be changed when appending data to "
		  "variable '%s'\n", var_name);

      return "recvary cannot be changed when appending data to a variable";
    }
    
    for (i = 0; i < numdims; ++i) {
      if (dimvarys[i] != old_dimvarys[i]) {
	if (cdf_tools_debug)
	  mexPrintf("dimsvary[%d] cannot be changed when appending data to "
		    "variable '%s'\n", i, var_name);

	return "dimsvary cannot be changed when appending data to a variable";
      }
    }

    /* Having checked numelems is the same try changing the datatype,
     * some types are considered equivalent, so it may be possible to
     * respecify it */
    if (CDFlib(PUT_, zVAR_DATASPEC_, datatype, numelems,
	       NULL_) != CDF_OK) {
      if (cdf_tools_debug)
	mexPrintf("Cannot change datatype from %s to %s when appending to "
		  "existing variable '%s'\n",
		  datatype_to_string(old_datatype),
		  datatype_to_string(datatype), var_name);

      return "Cannot change datatype when appending to existing variable";
    }
	       
  }
  else {
    /* Create variable */
    cdfstatus(CDFlib(CREATE_, zVAR_, var_name, datatype, numelems,
		     numdims, dimsizes, recvary, dimvarys, &varnum,
		     NULL_));

    start_record_num = 0L;
  }

  padvalue_matrix = mxGetField(metadata, 0, "padvalue");
  if (padvalue_matrix != NULL) {
    if (mxGetNumberOfElements(padvalue_matrix)) {
      int padvalue_elems = mxGetNumberOfElements(padvalue_matrix);
      if (cdf_tools_debug)
	mexPrintf("Pad value matrix has %d element(s)\n", padvalue_elems);
      
      if (mxIsChar(padvalue_matrix)) {
	char *padvalue;
	padvalue = mxMalloc((numelems + 1) * sizeof(char));
	mxGetString(padvalue_matrix, padvalue, numelems + 1);
	cdfstatus(CDFlib(PUT_, zVAR_PADVALUE_, padvalue,
			 NULL_));
	mxFree(padvalue);
      }
      else {
	if (mxIsNumeric(padvalue_matrix)) {
	  void *padvalue = mxGetData(padvalue_matrix);
	  cdfstatus(CDFlib(PUT_, zVAR_PADVALUE_, padvalue,
			   NULL_));
	}
	else {
	  if (cdf_tools_debug)
	    mexPrintf("Unsupported type for padvalue (was '%s')\n",
		      mxGetClassName(padvalue_matrix));

	  mexWarnMsgTxt("Unsupported type for padvalue");
	}
      }
    }
  }
  else {
    if (cdf_tools_debug)
      mexPrintf("Could not get padvalue for variable '%s'\n", var_name);

    mexWarnMsgTxt("Could not get padvalue");
  }
  
  /* Initialise for hyperwrite. Start at first element in first record
   * and write everything. Compute how many elements will be written.
   */
  values_to_write = records_to_write = (recvary == VARY ? records : 1L);

  cdfstatus(CDFlib(SELECT_, zVAR_RECNUMBER_, start_record_num,
		   SELECT_, zVAR_RECCOUNT_, records_to_write,
		   SELECT_, zVAR_RECINTERVAL_, 1L,
		   NULL_));

  if (numdims) {
    /* Setting dimension indices/counts/intervals not applicable for
     * 0-dimensional variables */
    long dim_zeros[CDF_MAX_DIMS];
    long dim_ones[CDF_MAX_DIMS];
    long dim_counts[CDF_MAX_DIMS];
    /* initialise dimension arrays with zeros and ones */
    for (i = 0; i < numdims; ++i) {
      dim_zeros[i] = 0L;
      dim_ones[i] = 1L;
      dim_counts[i] = (dimvarys[i] == VARY ? dimsizes[i] : 1);
      values_to_write *= dim_counts[i];
    }
    
    cdfstatus(CDFlib(SELECT_, zVAR_DIMINDICES_, dim_zeros,
		     SELECT_, zVAR_DIMCOUNTS_, dimsizes,
		     SELECT_, zVAR_DIMINTERVALS_, dim_ones,
		     NULL_));
  }
  /* number of elements for each value 
     values_to_write *= numelems; 
  */
  if (values_to_write < mxGetNumberOfElements(data)) {
    if (cdf_tools_debug) {
      mexPrintf("Processing variable '%s', require %ld samples, have %ld\n",
		var_name, (long)values_to_write,
		(long)mxGetNumberOfElements(data));
      mexPrintf("Too few data samples for variable '%s'\n", var_name);
    }
    
    return "Too few data samples";
  }

  if (majority == ROW_MAJOR && numdims > 1
      && dimsizes[0] > 1 && dimsizes[1] > 1) {
    transpose_needed = 1;
    data_copy = mxDuplicateArray(data);
    transpose_matrix(data_copy);
    data_p = data_copy;
  }
  else {
    transpose_needed = 0;
    data_p = data;
  }
  
  if (mxIsCell(data_p)) {
    char *padvalues, *buffer;
    long buflen;
    int num_str;
   
    /* Allocate space for all strings */
    buflen = values_to_write * numelems * sizeof(char);
    buffer = mxMalloc(buflen);

    /* Manually set pad value so that short strings are passed correctly */
    padvalues = mxMalloc(numelems * sizeof(char)); 
    CDFlib(GET_, zVAR_PADVALUE_, padvalues,
	   NULL_);
    memset(buffer, padvalues[0], buflen); 
    mxFree(padvalues);

    num_str = mxGetNumberOfElements(data_p);
    for (i = 0; i < num_str; ++i) {
      char *cp;
      char *src, *dst;
      if ((cp = mxArrayToString(mxGetCell(data_p, i))) == NULL) {
	/* Shouldn't happen, but avoid dereferencing NULL */
	if (cdf_tools_debug)
	  mexPrintf("Found non-char matrix when copying strings, ignoring\n");

	continue;
      }

      /* Copy string to the buffer, ensuring no more than numelems
       * chars are written. Don't copy null character either. Cannot
       * use strcpy for this job. */
      src = cp;
      dst = buffer + (numelems * i); 
      while (*src)
	*dst++ = *src++;
      
      mxFree(cp);
    }
    
    cdfstatus(CDFlib(PUT_, zVAR_HYPERDATA_, buffer,
		     NULL_));

    mxFree(buffer);
  }
  else {
    void *vp = mxGetData(data_p);
    cdfstatus(CDFlib(PUT_, zVAR_HYPERDATA_, vp,
		     NULL_));
  }

  if (transpose_needed)
    mxDestroyArray(data_copy);
  
  return NULL;
}


void putGlobalAttributes(const mxArray *data)
{
  int num_attr, i;
  if (data == NULL) {
    mexWarnMsgTxt("Not an array, refusing to write global attributes\n"); 
    return;
  }
  
  if (!mxIsCell(data)) {
    mexWarnMsgTxt("Not a cell array, refusing to write global attributes\n"); 
    return;
  }

  num_attr = mxGetM(data);

  for (i = 0; i < num_attr; ++i) {
    /* Test if the attribute exists, if so select it, otherwise create
     * it. Then add new data to the existing attribute, so that it is
     * possible to append to global attributes when the CDF file is
     * opened in append mode. */
    long attr_num;
    char *attr_name;
    const mxArray *attr_data;
    const mxArray *attr_metadata;
    
    attr_name = mxArrayToString(mxGetCell(data, i));
    attr_data = mxGetCell(data, num_attr + i);
    attr_metadata = mxGetCell(data, (2*num_attr) + i);
      
    if (CDFlib(CONFIRM_, ATTR_EXISTENCE_, attr_name,
	       NULL_) == CDF_OK)
      cdfstatus(CDFlib(SELECT_, ATTR_NAME_, attr_name,
		       NULL_));
    else
      cdfstatus(CDFlib(CREATE_, ATTR_,
		       attr_name, GLOBAL_SCOPE, &attr_num,
		       NULL_));
    

    if (mxIsCell(attr_data)) {
      putGlobalAttributeEntries(attr_data, attr_metadata, attr_name);
    }
    else {
      if (cdf_tools_debug)
	mexPrintf("Global attribute entries must be enclosed in a cell, "
		  "and the metadata must be a struct of the same size\n"); 
      
    }
    
    mxFree(attr_name);
    
  }
  
  return;
}


void putGlobalAttributeEntries(const mxArray *data, const mxArray *metadata,
			       const char *attr_name)
{
  int i, num_entries, mat_numel;
  long datatype, numelem, max_g_entry;
  void *vp = NULL;
#ifdef NDEBUG
  long datatype_size;
#endif
  
  if (data == NULL || !mxIsCell(data)) {
    if (cdf_tools_debug)
      mexPrintf("putGlobalAttribute(): data not cell, refusing to "
		"write entry for '%s'\n", attr_name);
    return;
  }
  
  if (metadata == NULL || !mxIsStruct(metadata)) {
    if (cdf_tools_debug)
      mexPrintf("putGlobalAttribute(): metadata is not struct, refusing to "
		"write entry for '%s'\n", attr_name);
    return;
  }

  num_entries = mxGetNumberOfElements(data);
  if (mxGetNumberOfElements(metadata) != num_entries) {
    if (cdf_tools_debug)
      mexPrintf("Number of global attribute entries for %s does not "
		"match the size of its metadata\n", attr_name); 
    return;
  }

  for (i = 0; i < num_entries; ++i) {
    const mxArray *entry_data;
    entry_data = mxGetCell(data, i);
    datatype = mxGetScalar(mxGetField(metadata, i, "datatype_value"));
    numelem = mxGetScalar(mxGetField(metadata, i, "number_of_elements")); 

    if (cdf_tools_debug)
      mexPrintf("Writing global attribute %s #%d, type=%s\n",
		attr_name, i, datatype_to_string(datatype));
    
    
    /* Create new entry, find number of highest entry associated with
     * this attribute*/
    cdfstatus(CDFlib(GET_, ATTR_MAXgENTRY_, &max_g_entry,
		   NULL_));
    /* Now that highest entry is known, select subsequent entry and
     * then write data */
    cdfstatus(CDFlib(SELECT_, gENTRY_, max_g_entry + 1,
		     NULL_));
    
    if (mxIsCell(entry_data)) {
      if (cdf_tools_debug)
	mexPrintf("Not writing global attribute %s #%d, data is cell array\n",
		  attr_name, i);
      continue;
    }
    
    if (mxIsChar(entry_data)) {
      char *cp;
      int len;
      cp = mxArrayToString(entry_data);
      len = strlen(cp);
      numelem = (numelem > len ? len : numelem);
      cdfstatus(CDFlib(PUT_, gENTRY_DATA_, datatype, numelem, cp,
		       NULL_));
      mxFree(cp);
      if (cdf_tools_debug)
	mexPrintf("Wrote global attribute %s #%d, type=%s\n",
		  attr_name, i, datatype_to_string(datatype));

      continue;
    }
    
#ifdef NDEBUG
    cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		     NULL_));
    if (datatype_size != mxGetElementSize(entry_data)) {
      mexWarnMsgTxt("CDF data size does not match Matlab data size");
      continue;
    }
#endif

    if (mxIsNumeric(entry_data)) {
      vp = mxGetData(entry_data);
      mat_numel = mxGetNumberOfElements(entry_data);
      numelem = (numelem > mat_numel ? mat_numel : numelem);
      cdfstatus(CDFlib(PUT_, gENTRY_DATA_, datatype, numelem, vp,
		       NULL_));

      if (cdf_tools_debug)
	mexPrintf("Wrote global attribute %s #%d, type=%s\n",
		  attr_name, i, datatype_to_string(datatype));
      continue;
    }

    if (cdf_tools_debug)
      mexPrintf("Could not write global attribute '%s', matlab class='%s'\n",
		attr_name, mxGetClassName(entry_data));

    mexWarnMsgTxt("Could not write global attribute details");
  }

  return;
}




const char* putVariableAttributes(const mxArray *data)
{
  int num_attr, i;
  
  if (data == NULL) 
    return "Not an array, refusing to write variable attributes"; 
  
  if (!mxIsCell(data)) 
    return "Not a cell array, refusing to write variable attributes";

  /* Column 1 contains the names of the attributes. Column 2 contains
   * [n*3] cell arrays, (columns are variable name, data, metadata,
   * repeated for each variable entry of the attribute) */

  num_attr = mxGetM(data);
  
  for (i = 0; i < num_attr; ++i) {
    /* Test if the attribute exists, if so select it, otherwise create
     * it. Then add new data to the existing attribute. */
    int entry, num_entries;
    long attr_num;
    char *attr_name = NULL;
    const mxArray *attr_name_matrix;
    const mxArray *entry_details;
    
    attr_name_matrix = mxGetCell(data, i);
    entry_details = mxGetCell(data, num_attr + i); 
    if (attr_name_matrix == NULL || !mxIsChar(attr_name_matrix)
	|| entry_details == NULL || !mxIsCell(entry_details)) {
      mexWarnMsgTxt("Bad variable attribute details");
      continue;
    }

    attr_name = mxArrayToString(attr_name_matrix);
    if (cdf_tools_debug)
      mexPrintf("Processing variable attribute '%s'\n", attr_name);
    
    /* Select or create attribute */
    if (CDFlib(CONFIRM_, ATTR_EXISTENCE_, attr_name,
	       SELECT_, ATTR_NAME_, attr_name,
	       NULL_) != CDF_OK)
      cdfstatus(CDFlib(CREATE_, ATTR_, attr_name, VARIABLE_SCOPE, &attr_num,
		       NULL_));

    num_entries = mxGetM(entry_details);
    for (entry = 0; entry < num_entries; ++entry) {
      
      long status, datatype, datatype_size, numelem;
      char *var_name = NULL;
      const mxArray *var_name_matrix;
      const mxArray *entry_data;
      const mxArray *entry_metadata;

      var_name_matrix = mxGetCell(entry_details, entry);
      entry_data = mxGetCell(entry_details, num_entries + entry);
      entry_metadata = mxGetCell(entry_details, (2*num_entries) + entry);
      
      if (var_name_matrix == NULL || !mxIsChar(var_name_matrix)
	  || entry_data == NULL
	  || entry_metadata == NULL || !mxIsStruct(entry_metadata)) {
	mexWarnMsgTxt("Bad variable attribute details");
	continue;
      } 
      var_name = mxArrayToString(var_name_matrix);
      if (cdf_tools_debug)
	mexPrintf("variable '%s', attribute '%s'\n", var_name, attr_name);
      
      datatype = mxGetScalar(mxGetField(entry_metadata, 0,
					"datatype_value"));
      numelem = mxGetScalar(mxGetField(entry_metadata, 0,
				       "number_of_elements")); 
    
      if ((status = CDFlib(SELECT_, zENTRY_NAME_, var_name,
			   NULL_)) != CDF_OK) {
	char cdf_message[CDF_ERRTEXT_LEN+1];
	CDFerror(status, cdf_message);
	if (cdf_tools_debug) {
	  mexPrintf("CDF library error: %s\n", cdf_message);
	  mexPrintf("Could not select attribute '%s' for variable '%s'\n",
		    attr_name, var_name);
	}

	mxFree(var_name);
	continue;
      }
      

      if (mxIsCell(entry_data)) {
	if (cdf_tools_debug)
	  mexPrintf("Not writing attribute '%s' for variable '%s' data "
		    "is cell array\n", attr_name, var_name);
	
	mxFree(var_name);
	continue;
      }
    
      if (mxIsChar(entry_data)) {
	char *cp;
	int len;
	cp = mxArrayToString(entry_data);
	len = strlen(cp);
	numelem = (numelem > len ? len : numelem);
	cdfstatus(CDFlib(PUT_, zENTRY_DATA_, datatype, numelem, cp,
			 NULL_));
	mxFree(cp);
	if (cdf_tools_debug)
	  mexPrintf("Wrote variable attribute %s for %s, type=%s\n",
		    attr_name, var_name, datatype_to_string(datatype));
	
	mxFree(var_name);
	continue;
      }
    
#ifdef NDEBUG
      cdfstatus(CDFlib(GET_, DATATYPE_SIZE_, datatype, &datatype_size,
		       NULL_));
      if (datatype_size != mxGetElementSize(entry_data)) {
	mexWarnMsgTxt("CDF data size does not match Matlab data size");
	mxFree(var_name);
	continue;
      }
#endif

      if (mxIsNumeric(entry_data)) {
	int mat_numel;
	void *vp;
	vp = mxGetData(entry_data);
	mat_numel = mxGetNumberOfElements(entry_data);
	numelem = (numelem > mat_numel ? mat_numel : numelem);
	cdfstatus(CDFlib(PUT_, zENTRY_DATA_, datatype, numelem, vp,
			 NULL_));

	if (cdf_tools_debug)
	  mexPrintf("Wrote variables attribute %s #%d, type=%s\n",
		    attr_name, i, datatype_to_string(datatype));
      }
      
      
      mxFree(var_name);
    }

    
    mxFree(attr_name);
  }
  return NULL;
}


char* getCellStrData(const mxArray *matrix, char padvalue, long maxlen,
		     long *buflen) /* out */
{
  long ell;
  long num_str;
  char *buffer;

  /* Allocate space for all strings */
  num_str = mxGetNumberOfElements(matrix);
  *buflen = num_str * maxlen * sizeof(char);
  buffer = mxMalloc(*buflen);

  /* Manually set pad value so that short strings are passed correctly */
  memset(buffer, padvalue, *buflen); 

  for (ell = 0; ell < num_str; ++ell) {
    char *cp, *src, *dst;
    if ((cp = mxArrayToString(mxGetCell(matrix, ell))) == NULL) {
      /* Most likely due to encountering a non-char array. Don't try
       * deferencing the NULL pointer! */
      if (cdf_tools_debug)
	mexPrintf("Found non-char matrix when copying strings, ignoring\n");

      continue;
    }
    
    /* Copy string to the buffer, ensuring no more than maxlen
     * chars are written. Don't copy null character either. Cannot
     * use strcpy for this job. */
    src = cp;
    dst = buffer + (maxlen * ell); 
    while (*src)
      *dst++ = *src++;
    
    mxFree(cp);
  }

  return buffer;
}


void transpose_matrix(mxArray *ap)
{
  int n, ndim;
  int *dims;
  const int *ip;
  
  if (mxIsSparse(ap)) {
    closeCdfWithError("Cannot transpose sparse matrices");
    return;
  }
  
  /* Copy dimensions */
  ndim = mxGetNumberOfDimensions(ap);
  ip = mxGetDimensions(ap);
  dims = mxMalloc(ndim * sizeof(int));
  for (n = 0; n < ndim; ++n)
    dims[n] = ip[n];
  
  if (dims[0] > 1 && dims[1] > 1) {
    /* Need to modify order of elements, not just swap number of rows
     * and columns */
    int i, j, k, datasize, bytes_per_layer, nd;
    int out_offset;
    char *inp, *outp;
    void *buffer;

  
    /* Merge 3rd and higher dimensions into one larger value of the 3rd
     * dimension */
    nd = 1;
    for (i = 2; i < ndim; ++i)  
      nd *= dims[i];

    datasize = mxGetElementSize(ap);
    bytes_per_layer = dims[0] * dims[1] * datasize;

    /* Allocate buffer in which to store transposed values */
    if ((buffer = mxMalloc(dims[0] * dims[1] * nd * datasize)) == NULL)
      closeCdfWithError("Out of memory");

    /* Set pointers to the start of where the read/write data */
    inp = mxGetData(ap);
    outp = buffer;

    /* Loop over rows fastest, then columns then our modified 3rd
     * dimension. In this way the read pointer reads subsequent values
     * and does not need to be computed. The write pointer must be
     * computed based on row, column and Nd. After transposing the
     * elements in one layer the write pointer is reset to the start of
     * the next Nd layer, and we can save on pointer arithmetic by not
     * including value of i every time. */

    for (i = 0; i < nd; ++i) { /* 3rd dimension */
      for (j = 0; j < dims[1]; ++j) { /* columns */
	for (k = 0; k < dims[0]; ++k) { /* rows */

	  /* Output: k = cols, j = rows */
	  out_offset = ((k * dims[1]) + j) * datasize;
	
	  /* Copy data, byte by byte */
	  for (n = 0; n < datasize; ++n)
	    *(outp + out_offset + n) = *(inp++);
	
	}
      }

      /* Completed one 2D matrix, move to next layer in 3rd
       * dimension. To minimise amount of pointer arithmetic simply
       * increment the outp pointer by the number of bytes in each
       * layer */
      outp += bytes_per_layer;
    
    }

    /* Modify the matrix to use the new data */
    mxSetData(ap, buffer);
  }
  else {
    if (cdf_tools_debug)
      mexPrintf("No need to reorder elements in transpose_matrix()\n");
  }
  
  /* Alter dimensions */
  n = dims[0];
  dims[0] = dims[1];
  dims[1] = n;
  mxSetDimensions(ap, dims, ndim);

  mxFree(dims);
  return;
}


const char* deleteVariable(const char *var_name)
{
  if (CDFlib(SELECT_, zVAR_NAME_, var_name,
	     NULL_) != CDF_OK) 
    return "Could not find variable to delete";
  
  if (CDFlib(DELETE_, zVAR_,
	     NULL_) != CDF_OK)
    return "Could not delete variable";
  
  return NULL;
}


const char* deleteGlobalAttribute(const char *attr_name)
{
  long scope;
  if (CDFlib(CONFIRM_, ATTR_EXISTENCE_, attr_name,
	     NULL_) != CDF_OK)
    return "Could not find global attribute to delete";
  
  cdfstatus(CDFlib(SELECT_, ATTR_NAME_, attr_name,
		   GET_, ATTR_SCOPE_, &scope,
		   NULL_));
  if (scope != GLOBAL_SCOPE)
    return "Not a global attribute";

  if (CDFlib(DELETE_, ATTR_,
	     NULL_) != CDF_OK)
    return "Could not delete global attribute";
  
  return NULL;
}

const char* deleteVariableAttribute(const char *attr_name,
				    const char *var_name)
{
  long scope;

  if (CDFlib(CONFIRM_, ATTR_EXISTENCE_, attr_name,
	     NULL_) != CDF_OK)
    return "Could not find variable attribute to delete (bad attribute name)";

  cdfstatus(CDFlib(SELECT_, ATTR_NAME_, attr_name,
		   GET_, ATTR_SCOPE_, &scope,
		   NULL_));
  if (scope != VARIABLE_SCOPE)
    return "Not a variable attribute";

  if (var_name == NULL) {
    /* Delete this attribute, for all variables */
    if (CDFlib(DELETE_, ATTR_,
	       NULL_) != CDF_OK)
      return "Could not delete variable attribute";
  }
  else {
    if (CDFlib(SELECT_, zENTRY_NAME_, var_name,
	       NULL) != CDF_OK) 
      return "Could not find variable attribute to delete (bad variable name)"; 
    /* Delete this attribute, for all variables */
    if (CDFlib(DELETE_, zENTRY_,
	       NULL_) != CDF_OK)
      return "Could not delete variable attribute";
  }

  return NULL;
}


