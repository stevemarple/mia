function [status,mesg] = cdf_delete(filename)
%CDF_DELETE  Delete a CDF file (or files for multi-file archives).
%
%   [status, mesg] = CDF_DELETE(filename)
%   status: LOGICAL flag, 0 for success, 1 for failure
%   mesg: ERROR message, or empty if success
%   filename: CHAR (without .cdf extension)
%
% CDF_DELETE deletes a CDF file (or multiple files for multi-file
% archives). The filename should be normally be specified without the .cdf
% extension (CDF_DELETE will fail if it is present on a multi-file archive).
%
% If CDF_DELETE is called without any output arguments then an ERROR message
% is automatically generated if the file(s) cannot be deleted. When called
% with output arguments CDF_DELETE silently ignores errors and sets status
% and mesg.
%
% See also CDF_INFO, CDF_READ, CDF_WRITE, CDF_TOOLS_HELP.

[status mesg] = cdf_delete_mex(filename);
 status = logical(status);
 
if nargout
  % user is testing for errors
  return
end

if status
  error(mesg);
end



