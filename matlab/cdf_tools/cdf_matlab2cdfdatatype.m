function r = cdf_matlab2cdfdatatype(c)
%CDF_REAL8

% mapping for Matlab class to CDF data types
s.uint8  = 'UINT1';
s.uint16 = 'UINT2';
s.uint32 = 'UINT4';

s.int8  = 'INT1';
s.int16 = 'INT2';
s.int32 = 'INT4';

s.single = 'FLOAT';
s.double = 'DOUBLE';

s.logical = s.uint8;
s.char = 'CHAR';

s.timestamp = 'EPOCH';
s.timespan = s.double;

if isfield(s, c)
  r = getfield(s, c);
else
  error(sprintf('Unknown class (was ''%s'')', c));
end
