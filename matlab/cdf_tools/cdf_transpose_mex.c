#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "mat.h"
#include "matrix.h"

#include "cdf_tools_mex.h"

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
  /* Check input and output arguments */
  if (nrhs != 1)
    mexErrMsgTxt("Incorrect number of inputs");
  
  if (nlhs != 1)
    mexErrMsgTxt("Incorrect number of outputs");

  plhs[0] = mxDuplicateArray(prhs[0]);
  transpose_matrix(plhs[0]);
  return;
}

