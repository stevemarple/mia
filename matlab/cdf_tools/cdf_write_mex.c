#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "mat.h"
#include "matrix.h"

#include "cdf_tools_mex.h"

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
  const mxArray *filename_matrix;
  const mxArray *data_matrix;
  const mxArray *options_matrix;
  const mxArray *ap;
  const char *cp;
  
  /* Check input and output arguments */
  if (nrhs != 3)
    mexErrMsgTxt("Incorrect number of inputs");

  filename_matrix = prhs[0];
  data_matrix = prhs[1];
  options_matrix = prhs[2];
  
  if (!mxIsChar(filename_matrix))
    mexErrMsgTxt("Filename must be a char");

  if (!mxIsCell(data_matrix))
    mexErrMsgTxt("Data must be a cell");

  if (!mxIsStruct(options_matrix))
    mexErrMsgTxt("Options must be a struct");

  if (nlhs != 2)
    mexErrMsgTxt("Incorrect number of outputs");

  if ((ap = mxGetField(options_matrix, 0, "debug")) != NULL)
    cdf_tools_debug = mxGetScalar(ap);
  
  cp = cdfWrite(filename_matrix, data_matrix, options_matrix);

  if (cp == NULL) {
    plhs[0] = createScalar(0);
    plhs[1] = mxCreateString("");
  }
  else {
    plhs[0] = createScalar(1);
    plhs[1] = mxCreateString(cp);
  }
  
  return;
}

