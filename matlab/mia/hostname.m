function r = hostname

persistent h;
if isempty(h)
  [status h] = unix('hostname -s');
  % remove newline
  h(h == 10) = [];
end

r = h;

