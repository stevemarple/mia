function r = getlatlonstr(loc, varargin)
%GETLATLONSTR  Convert a LOCATION latitude/longitude to a string.
%
%   r = GETLATLONSTR(loc)
%
%   See also LOCATION, location/CHAR.

if length(loc) ~= 1
  r = cell(size(loc));
  for n = 1:numel(loc)
    r{n} = feval(mfilename, loc(n), varargin{:});
  end
  return
end

% from this point on loc is scalar

if length(varargin)
  fstr = varargin{1};
else
  % fstr = ['%.2f' i18n_entity('deg')];
  fstr = ['%.2f deg'];
end

if isnan(loc.geolat)
  latStr = '';
else
  if loc.geolat >= 0
    ns = 'N';
  else
    ns = 'S';
  end
  latStr = [sprintf(fstr, abs(loc.geolat)) ' ' ns];
end
if isnan(loc.geolong) | abs(loc.geolat) == 90
  lonStr = '';
else
  if loc.geolong >= 0
    ns = 'E';
  else
    ns = 'W';
  end
  lonStr = [sprintf(fstr, abs(loc.geolong)) ' ' ns];
end

r = '';
if ~isempty(latStr)
  r = latStr;
end
if ~isempty(lonStr)
  if ~isempty(r)
    r = [r ', ' lonStr];
  else
    r = lonStr;
  end
end
