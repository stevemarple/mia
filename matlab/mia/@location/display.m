function display(a)
%DISPLAY  Display a LOCATION object.

disp(' ');
disp([inputname(1),' = ' char(a)]);
disp(' ');
