function r = eq(a, b, varargin)
%EQ  Test for equality on two LOCATION objects.
%
%   r = EQ(a, b)
%   r = EQ(a, b, tol)
%   a: LOCATION object
%   b: LOCATION object
%   tol: tolerance
%
%   To test if two locations are the same the geographic latitude and
%   longitude values are compared. If they differ by no more than a small
%   tolerance value they are deemed to be equal. The default tolerance is
%   1e-10.
%
%   See also LOCATION, location/NE, EPS.

if ~isa(a, 'location') | ~isa(b, 'location')
  error('objects must be locations');
end

if nargin > 2
  tol = varargin{3};
else
  tol = 1e-10;
end

a_geolat = zeros(size(a));
a_geolong = zeros(size(a));
for n = 1:prod(size(a))
  a_geolat(n) = a(n).geolat;
  a_geolong(n) = a(n).geolong;
end

b_geolat = zeros(size(b));
b_geolong = zeros(size(b));
for n = 1:prod(size(b))
  b_geolat(n) = b(n).geolat;
  b_geolong(n) = b(n).geolong;
end


% r = (a_geolat + tol >= b_geolat) & (a_geolat - tol <= b_geolat) & ...
%     (a_geolong + tol >= b_geolong) & (a_geolong - tol <= b_geolong);

r = logical((((a_geolat + tol >= b_geolat) & ...
	      (a_geolat - tol <= b_geolat)) | ...
	     (isnan(a_geolat) & isnan(b_geolat))) & ...
	    (((a_geolong + tol >= b_geolong) & ...
	      (a_geolong - tol <= b_geolong)) | ...
	     (isnan(a_geolong) & isnan(b_geolong))));

