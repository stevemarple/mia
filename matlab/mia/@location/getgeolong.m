function r = getgeolong(loc)
%GETGEOLONG  Return the geographic longitude of a LOCATION object.
%
%   r = GETGEOLONG(loc)
%   r: longitude (degrees)
%   loc: LOCATION object
%
%   See also GETGEOLAT.

r = zeros(size(loc));
for n = 1:prod(size(loc))
  r(n) = loc(n).geolong;
end
