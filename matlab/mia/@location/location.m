function loc = location(varargin)
%LOCATION Constructor for LOCATION class
%
%   r = LOCATION(name, country, geolat, geolong);
%   name: nearest place
%   country: country of location
%   geolat: geographic latitude
%   geolong:  geographic longitude
%

loc.name = 'unknown';
loc.country = 'unknown';
loc.geolat = nan;
loc.geolong = nan;
 
if nargin == 0
  % default constructor - needed for loading
    
elseif nargin == 1
  % copy constructor
  loc = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [loc unvi] = interceptprop(varargin, loc);
  if length(unvi)
    warning(['The following parameter(s) have been ignored:', ...
	     sprintf(' %s', varargin{unvi(1:2:end)})]);
  end
  
elseif nargin == 4
  loc.name = varargin{1};
  loc.country = varargin{2};
  loc.geolat = varargin{3};
  loc.geolong = varargin{4};

else
  disp(nargin);
  error('location() called with incorrect arguments');
end


loc.geolong = rem(loc.geolong, 360);
if loc.geolong > 180
  loc.geolong = loc.geolong - 360;
end

loc = class(loc, 'location');


