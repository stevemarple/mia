function r = char(loc)
%CHAR  Convert a LOCATION object to a string.
%
%   r = CHAR(loc)
%
%   See also LOCATION.

if length(loc) ~= 1
  r = matrixinfo(loc);
  return;
end

latLonStr = getlatlonstr(loc);

if isempty(loc.name) & isempty(loc.country)
  r = latLonStr;
elseif isempty(loc.country)
  r = sprintf('%s (%s)', loc.name, latLonStr);
elseif isempty(loc.name)
  r = sprintf('%s (%s)', loc.country, latLonStr);
elseif isempty(latLonStr)
  r = sprintf('%s, %s', loc.name, loc.country);
else
  r = sprintf('%s, %s (%s)', loc.name, loc.country, latLonStr);
end

r = char(r);

