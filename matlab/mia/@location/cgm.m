function varargout = cgm(loc, varargin)
%CGM  Get GEOCGM details for a given location.
%
%   ... = CGM(loc, ...)
%
%   This is equivalent to calling GEOCGM and specifying the latitude and
%   longitude to be that of the location object, and direction to be
%   geographic to geomagnetic. Any additional parameters are passed onto
%   GEOCGM.

narg = nargout;
if narg == 0
  % probably run on the command line with no return value, assume one is
  % needed
  narg = 1; 
end
  

for n = 1:prod(size(loc))
  [varargout{1:narg}] = geocgm(varargin{:}, ...
			       'latitude', getgeolat(loc(n)), ...
			       'longitude', getgeolong(loc(n)), ...
			       'direction', 1);
end

for n = 1:narg
  varargout{n} = reshape(varargout{n}, size(loc));
end

