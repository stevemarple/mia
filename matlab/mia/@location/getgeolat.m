function r = getgeolat(loc)
%GETGEOLAT  Return the geographic latitude of a LOCATION object.
%
%   r = GETGEOLAT(loc)
%   r: latitude (degrees)
%   loc: LOCATION object
%
%   See also GETGEOLONG.

r = zeros(size(loc));
for n = 1:prod(size(loc))
  r(n) = loc(n).geolat;
end


