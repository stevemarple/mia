function [r, idx] = sort(loc)

lat = getgeolat(loc); 
lon = getgeolong(loc);

a = [lat(:), lon(:)];

[tmp idx] = sortrows(a);
r = loc(idx);



