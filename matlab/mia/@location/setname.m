function r = setname(loc, name, varargin)
%SETNAME  Update the LOCATION name.
%
%   r = SETNAME(loc, name)
%   r = SETNAME(loc, name, country)
%   r: modified LOCATION object
%   in: LOCATION
%   name: CHAR
%   country: CHAR
%
%   See also GETNAME, LOCATION.

r = loc;

if length(r) ~= 1
  error('loc must be scalar');
elseif nargin > 3
  error('incorrect parameters');
end

r.name = name;

if nargin == 3
  r.country = varargin{1};
end
