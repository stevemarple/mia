function r = setgeolong(loc, lon)
%SETGEOLAT  Set the geographic latitude in a LOCATION object.
%
%   r = SETGEOLONG(loc, lat)
%   loc: LOCATION object   
%   lon: longitude (degrees)
%   r: updated LOCATION object
%
%   See also GETGEOLONG, SETGEOLAT.

% Wrap longitude
lon = mod(lon, 360);
lon(lon > 180) = lon(lon > 180) - 360;
r = loc;
for n = 1:numel(loc)
  r(n).geolong = lon(n);
end



