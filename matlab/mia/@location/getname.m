function varargout = getname(loc)
%GETNAME  Return the name (and optionally country) of a LOCATION object.
%
%   name = GETNAME(loc)
%   [name, country] = GETNAME(loc)
%
%   name: place name
%   country: country name
%   loc: LOCATION object
%
%   See also LOCATION, GETGEOLAT, GETGEOLONG.

varargout{1} = loc.name;
if nargout > 1
  varargout{2} = loc.country;
end

