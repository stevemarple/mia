function r = ne(a, b)
%NE  Test for inequality on two LOCATION objects.
%
%   r = NE(a, b)
%   a: LOCATION object
%   b: LOCATION object
%
%   NE inverts the result of the EQ operator. For more details see
%   location/EQ.
%
%   See also LOCATION, location/EQ.

r = ~(a==b);
