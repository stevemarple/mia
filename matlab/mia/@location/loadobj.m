function r = loadobj(a)
%LOADOBJ  Fix loading of LOCATION objects.
%

% Any internationalisation used ISO-8859-1
if ischar(a.name) & any(a.name > 127)
  a.name = iso88591_to_ascii(a.name);
end

r = a;
