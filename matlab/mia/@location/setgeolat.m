function r = setgeolat(loc, lat)
%SETGEOLAT  Set the geographic latitude in a LOCATION object.
%
%   r = SETGEOLAT(loc, lat)
%   loc: LOCATION object   
%   lat: latitude (degrees)
%   r: updated LOCATION object
%
%   See also GETGEOLAT, SETGEOLONG.

r = loc;
for n = 1:numel(loc)
  r(n).geolat = lat(n);
end


