function h = map(loc, ah, varargin)
%MAP  Plot LOCATION object(s) on a map.
%
%   h = MAP(loc, ah, ...)
%
%   h: LINE handle(s)
%   ah: AXES handle
%
%   MAP also accepts the standard LINE parameter name/values.
%
%   See also LOCATION, LINE, AXES.

defaults.linefunction = [];  % defer
[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.linefunction)
  % does axis have any children which look they they belong to m_map?
  if ismmap(ah)
    defaults.linefunction = 'm_line';
  else
    defaults.linefunction = 'line';
  end
end

sz = prod(size(loc));
h = zeros(sz, 1);
for n = 1:sz
  h(n,1) = feval(defaults.linefunction, getgeolong(loc(n)), ...
		 getgeolat(loc(n)), varargin{unvi});
end
