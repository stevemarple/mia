function r = unique(a)
%UNIQUE  Return a unique vector of LOCATIONs.
%
%   Unlike the normal UNIQUE function the LOCATION version does
%   not return a sorted list. The order is based upon their original order.
%
%   See ops/UNIQUE.

tmp = getgeolat(a(:));
tmp(:,2) = getgeolong(a(:));

[tmp2 idx] = unique(tmp, 'rows');

r = a(idx);
