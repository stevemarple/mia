function varargout = matrixinfo(m)
%MATRIXINFO  Return details of matrix size and class, as CHAR.
%
%
%   MATRIXINFO(m)
%   Print details about m, including INPUTNAME.
%
%   r = MATRIXINFO(m)
%   r: matrix size and class details as CHAR
%   m: any matrix or object
%   Return details about m

sz = size(m);
s = sprintf('x%d', sz(2:end));
if isnumeric(m) & ~isreal(m)
  r = sprintf('[%d%s %s (complex)]', sz(1), s, class(m));
else
  r = sprintf('[%d%s %s]', sz(1), s, class(m));
end

if nargout == 0
  disp(sprintf('%s: %s', inputname(1), r));
else
  varargout{1} = r;
end
