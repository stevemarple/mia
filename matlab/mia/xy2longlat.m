function [long, lat] = xy2longlat(x, y, z, varargin)
%XY2LATLONG  Convert Cartesian coordinate system to latitude and longitude.
%
%   [rlong, rlat] = XY2LONGLAT(x, y, z, long, lat)
%   [rlong, rlat] = XY2LONGLAT(x, y, z, loc)
%
%   rlong: longitude of input points
%   rlat: latitude of input points
%   x: DOUBLE (r x c) [units: metres]
%   y: DOUBLE (r x c) [units: metres]
%   z: DOUBLE, scalar or (r x c) [units: metres]
%   long: longitude of coordinate system origin
%   lat: latitude of coordinate system origin
%   loc: LOCATION object.
%
%   x and y must be matrices of the same size. z must be scalar or a
%   matrix the same size as x and y. The origin of the (x, y, z)
%   coordinate system may be specified by longitude/latitude or by a
%   LOCATION object.
%
%   See also LOCATION.

% Take a Cartesian coordinate system and place at North pole, so that -Y axis
% points out along Greenwich meridian (0 deg E), and Z axis is aligned with
% Earth's axis of rotation.
%
% (1) To convert to latitude / longitude slide coordinate system down
% Greenwich meridian (keeping -Y axis aligned with GM) until the centre of
% the new coordinate system is at the desired latitude. Calculate the
% latitude and longitude of all points for this location.
%
% (2) Lastly add (0,0) longitude value as a constant offset.

Re = earthradius; % local copy

switch nargin
  case 4
    latDeg = getgeolat(varargin{1});
    longDeg = getgeolong(varargin{1});
  
  case 5
    longDeg = varargin{1};
    latDeg = varargin{2};
    
  otherwise
    error('incorrect paramters');
end
    
latRad = pi * latDeg / 180;
longRad = pi * longDeg / 180;
% heights are relative to ground level - make absolute from centre of
% Earth
z = z + Re;

% % (1) move down along Greenwich meridian
% % x1 = (z * cos(latRad)) + (x * sin(latRad));
% % y1 = y;
% % z1 = (z * sin(latRad)) - (x * cos(latRad));

% x1 = (z .* cos(latRad - (x / Re)));
% y1 = y;
% z1 = z .* sin(latRad - (x / Re));

x1 = x;
y1 = z .* cos(latRad + (y / Re));
z1 = z .* sin(latRad + (y / Re));

[theta phi r] = cart2sph(x1, y1, z1);
thetaDeg = 180 * theta / pi;
% rotate to correct longitude, subtract 90 degrees since longitude is
% measured relative to Greenwich meridian (i.e. -Y axis, but cart2sph
% uses +X axis).

% don't understand why this appears to give x direction reversed!
% long = thetaDeg -90 + longDeg;
long = -thetaDeg + 90 + longDeg; % but this fixes the problem
lat = 180 * phi / pi; 




