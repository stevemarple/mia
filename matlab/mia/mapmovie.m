function mapmovie(varargin)

defaults.filename = 'untitled.gif';
defaults.data = {};
defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];

defaults.axes = [];
defaults.map = 'scand';

defaults.delay = timespan(100,'ms');
defaults.scale = [1 1];
defaults.loopcount = 0;
defaults.colormap = jet(256);
defaults.clim = [];
defaults.optimize = 0;

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.data)
  error('no data specified');
elseif ~iscell(defaults.data)
  defaults.data = {defaults.data(:)'};
end
numOfData = prod(size(defaults.data));

if isempty(defaults.starttime)
  defaults.starttime = getstarttime(defaults.data{1});
end
if isempty(defaults.endtime)
  defaults.endtime = getendtime(defaults.data{1});
end
if isempty(defaults.resolution)
  defaults.resolution = getresolution(defaults.data{1});
end
% check all have required resolution
for n = 1:numOfData
  if getresolution(defaults.data{n}) ~= defaults.resolution
    error('resolution of data incorrect');
  end
end

if ~isempty(defaults.axes)
  gh = defaults.axes(1);
  fh = get(gh, 'Parent');
else
  [fh, gh] = feval(defaults.map, varargin{unvi});
end

h = cell(1, numOfData);
t = defaults.starttime
while t < defaults.endtime
  % plot onto map
  for n = 1:numOfData
    [tmp tmp h{n}] = map(defaults.data{n}, ...
			 'axes', gh);
			 % 'time', t);
  end
  disp('fix time argument');
  
  % save frame

  pause(1);

  % remove data from map
  for n = 1:numOfData
    h{n}
    delete(h{n});
  end
  % step on
  t = t + defaults.resolution;
  
end
