function coef = idl_digital_filter(flow, fhigh, gibbs, nterms)
%IDL_DIGITAL_FILTER  Design coefficients for a digital filter (like IDL)
%
%   coef = IDL_DIGITAL_FILTER(fLow, fHigh, gibbs, nterms)
%
%   coef: DOUBLE, coefficients
%   fLow: Lower cut-off of the filter as a fraction of the Nyquist frequency
%   fHigh: Upper cut-off of the filter as a fraction of the Nyquist frequency
%   gibbs: Size of the Gibbs phenomenon wiggles in -dB. 50 is a good choice.
%   nterms: the order of the filter
%
% IDL_DIGITAL_FILTER returns coefficients for a digital finite impulse
% response filter using the same method and input parameters used by IDL's
% "digital_filter" function.
% 

% band stop?
fstop = (fhigh < flow);

if gibbs < 21
  alpha = 0;
elseif gibbs >= 50
  alpha = 0.1102 * (gibbs-8.7);
else
  alpha = 0.5842 * power(gibbs-21, 0.4) + (0.07886 * (gibbs-21));
end

arg = [1:nterms] ./ nterms;
coef = besseli(0, alpha * sqrt(1 - power(arg, 2))) ./ besseli(0, alpha);
t = [1:nterms] * pi;

coef = coef .* (sin(t * fhigh) - sin(t * flow)) ./ t;
% the last element comes out half the size of that in IDL. You can
% uncomment the line below if that's important, but it is due to small
% errors from the subtraction of sines. The number should probably be
% zero, but instead are only almost zero.

% coef(end) = coef(end) .* 2;

% debugging code to work out why
% Zl = sin(t * flow)
% Zh = sin(t * fhigh)
% Z = (sin(t * fhigh) - sin(t * flow))


coef = [fliplr(coef), fhigh - flow + fstop, coef]; % replicate it

