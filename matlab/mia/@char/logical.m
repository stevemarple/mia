function r = logical(c)
%LOGICAL  Convert a CHAR or string to a LOGICAL value
%
%   r = LOGICAL(c)
%   r: 0 or 1 (as LOGICAL value)
%   c: string representation ('y'|'n', 't'|'f', 'yes'|'no',
%   'true'|'false')
%
%   Case is not important.

switch lower(c)
 case {'t' 'true' 'y' 'yes' 'on' '1'}
  r = logical(1);
  
 case {'f' 'false' 'f' 'no' 'off' '0'}
  r = logical(0);
  
 otherwise
  error(['Do not know a logical value associated with ' c]);
end


