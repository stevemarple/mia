function r = autolimit(l)
%AUTOLIMIT  Return limits suitable for plotting on axes. Linear mode only.
%
%   r = AUTOLIMIT(l)
%   r: [1x2] DOUBLE
%   l: [1x2] DOUBLE
%
%   AUTOLIMIT attempts to produce sensible upper and lower limits when
%   plotting.

% cheat an use Matlab's method. Create an invisible figure and plot into
% it. Return the limits matlab used.

% figure should not make any callbacks. 
fh = figure('Visible', 'off', ...
	    'CreateFcn', '', ...
	    'CloseRequestFcn', '', ...
	    'DeleteFcn', '');
ah = axes('Parent', fh, ...
	  'CreateFcn', '', ...
	  'DeleteFcn', '');

plot(l);
r = get(ah, 'YLim');
delete(fh);
return;

% old method is below

lower = 0;

if l(1) < 0
  lowerSign = -1;
  l(1) = -l(1);
else
  lowerSign = +1;
end
if l(2) < 0
  upperSign = -1;
  l(2) = -l(2);
else
  upperSign = +1;
end

% convert numbers into range 10 -> 99.99

if l(1) == 0
  lower = 0;
else
  lowerMul = 10^-(floor(log10(l(1))) - 1);
  lower = (floor(l(1) * lowerMul)) / lowerMul;
end

if l(2) == 0
  upper = 0;
else
  upperMul = 10^-(floor(log10(l(2))) - 1);
  upper = (ceil(l(2) * upperMul)) / upperMul;
end


r = [lowerSign*lower upperSign*upper];





