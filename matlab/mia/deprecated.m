function varargout = deprecated(varargin)
%DEPRECATED Warn a function is deprecated.
%
%   DEPRECATED on
%   DEPRECATED off
%   DEPRECATED backtrace
%   DEPRECATED error
%
%   Set the WARNING level when a DEPRECATED function is encountered. If
%   set to 'error' then ERROR results.
%
%   DEPRECATED('mesg')
%   
%   Mark function as deprecated, printing out the message.
%
%   See also WARNING.

global DEPRECATED;

validOptions = {'backtrace' 'on' 'off' 'error'};
switch char(DEPRECATED)
  case validOptions
    ; % no problem
    
  otherwise
    DEPRECATED = 'on';
end

if nargin == 0
  % return current deprecated warning status
  varargout{1} = DEPRECATED;
  return;
elseif nargin == 1 
  switch varargin{1}
    case validOptions
      % set deprecated warning status
      DEPRECATED = varargin{1};
      return;
  end
end

% if got this far must be a deprecated function
stack = dbstack;

if length(stack) < 2
  % called from command line?
  mfile = '?';
else
  mfile = stack(2).name; % should be the mfilename of our caller
end

if length(stack) < 3
  caller = '?';
else
  caller = stack(3).name; % should be the name of who called the
  % deprecated file
end

usermesg = sprintf('%s', varargin{:});

mesg = sprintf('Function %s is DEPRECATED.\nCalled from %s\n%s\n', ...
    mfile, caller, usermesg);


switch char(DEPRECATED)
  case {'backtrace' 'on' 'off'}
    warningMethod = char(DEPRECATED);
    
  case 'error'
    error(mesg);
    
  otherwise
    % shouldn't happen, maybe someone played with DEPRECATED
    error('unknown error');
end

warningStatus = warning; % remember status
warning(warningMethod);
warning(mesg);
warning(warningStatus); % return to previous warning status


