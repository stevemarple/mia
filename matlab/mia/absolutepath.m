function [absPath, r] = absolutepath(Path)
%ABSOLUTEPATH  Check (and convert) path(s) to absolute paths
%
%   [absPath r] = ABSOLUTEPATH(Path)
%
%   absPath: absolute path(s)
%   r: LOGICAL matrix of same size as Path indicating if path was absolute
%   (1) or relative (0).
%   Path: CHAR array for a single path, or a CELL array of CHARs if
%   multiple paths are to be converted.
%
%   See also DIR, EXIST.


cwd = pwd;

cls = class(Path);

if iscell(Path)
  r = logical(zeros(size(Path)));
else
  Path = {Path};
  r = 0;
end

absPath = cell(size(Path));



for n = 1:prod(size(r))
  if isunix
    if Path{n}(1) == filesep
      r(n) = 1;
    end
  elseif ispc
    if lower(Path{n}(1)) >= 'a' & lower(Path{n}(1)) <= 'z'& ...
	  Path{n}(2) == ':'
      % drive specification
      r(n) = 1;
    elseif strcmp(Path{n}([1 2]), '\\')
      % windows UNC(?) path
      r(n) = 1;
    end
  else
    error('unknown system')
  end
  
  % calculate absolute path
  if ~r(n)
    absPath{n} = fullfile(cwd, Path{n});
  else
    absPath{n} = Path{n};
  end
end

% if we weren't passed a cell array don't return one
if ~strcmp(cls, 'cell')
  absPath = absPath{1};
end
