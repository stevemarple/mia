function r = getquantity(mia)
%GETENDTIME  Return the measured quantity of the MIA_BASE object
%
%   r = GETQUANTITY(mia)
%   r: CHAR
%   mia: MIA_BASE object

r = 'unknown';
if ~strcmp(class(mia), 'mia_base') & ~strcmp(class(mia), 'mia_image_base') 
  warning(sprintf('Please override getquantity for class %s', ...
	  class(mia)));
end
