function r = loaddata(in, varargin)
%LOADDATA  Private function to load data derived from MIA_BASE.
%
%
%   The following parameter name/value pairs should be set when
%   appropriate:
%
%   'cancelhandle'
%     Handle(s) of cancel button(s). During certain phases of the load
%     process the handles are checked to see if they are still valid. If
%     nay are not it is assumed that the cancel action has deleted the
%     cancel button, and thus the load process is terminated. At the same
%     time the handles are checked GUI callbacks are executed.
%
%   'createargs', CELL array
%     CELL array of additional parameters to pass when creating a new
%     object. E.g., for RIO_BASE {'beams', beams}
%
%   'extractargs', CELL array
%     CELL array of additional parameters to pass when calling EXTRACT.
%     E.g., for RIO_BASE {'beams', beams}
%
%   'failiffilemissing', LOGIC
%     Flag to indicate whether loading data should fail when data files
%     are missing. The default value is dependent upon the instrument,
%     INFO(instrument, 'defaultfilename') will indicate the default
%     behaviour.
%
%   'archive', CHAR or CELL
%   Specify the name of the archive (as CHAR) to load data from. If empty
%   the default archive for that instrument and data type is used. A CELL
%   array can also be passed in which case all archives are considered. See
%   the multiplearchives option for more details. This option can be used
%   to load data from the normal archive if it exists but falling back to
%   loading data from preliminary archives if not.
%
%   'multiplearchives', LOGICAL
%   If archive is a CHAR this option has no effect; if it is a CELL array
%   then multiplearchives affects whether data is loaded from any or all of
%   the archives specified. When multiplearchives is FALSE data is loaded
%   from the first listed archive which has some valid data, the remaining
%   archives are not checked. When multiplearchives is TRUE data is loaded
%   from all listed archives. The data is then merged so that data from the
%   archives listed later is used to fill in the data gaps of the earlier
%   listed archives.
%
%   This function requires an INFO function for the appropriate
%   instrument class. The INFO function must support 'bestresolution' and
%   'defaultfilename' information requests.

% =================================
% TEMPORARY WORKAROUND FOR RESOLUTION. Placed at start so that recursive
% function calls also get the same resolution value
global RESOLUTION_WORKAROUND;
if ~isempty(RESOLUTION_WORKAROUND)
  disp('**************************************************************');
  disp('TEMPORARY workaround for resolution: reading from global variable');
  disp('**************************************************************');
  varargin = {'resolution', RESOLUTION_WORKAROUND, varargin{:}};
  RESOLUTION_WORKAROUND = []; % reset after use
end
% =================================
r = [];
instrument = getinstrument(in);

% allow multiple instruments to create multiple data sets
switch numel(instrument)
 case 0
  error('instrument details required to load data')
  
 case 1
  ; % load as normal
  
 otherwise
  clear r;
  for n = 1:numel(instrument)
    in = setinstrument(in, instrument(n));
    r(n) = feval(basename(mfilename), in, varargin{:});
  end
  return
end

units = getunits(in);
cls = class(in);
bestRes = info(instrument, 'bestresolution');

st = getstarttime(in);
et = getendtime(in);

if et <= st
  error('endtime should be later than start time');
end

defaults.cancelhandle = [];
defaults.createargs = {};
defaults.extractargs = {};

defaults.filename = '';
defaults.failiffilemissing = [];

% allow helper functions such as samnet2mia to reuse loaddata. Eg samnet2mia
% can try loading from multiple archives, but each archive is loaded by
% calling loaddata.
defaults.loadfunction = []; % '' is valid, so use [] to indicate unset
defaults.format = '';

defaults.resolution = [];
defaults.resolutionmethod = '';
% if the data loaded didn't match the class required then indicate if it
% should be cast to that class
defaults.correctclass = 1;

defaults.archive = '';
defaults.multiplearchives = 0;

% allow user to apply transformations to the data ('filter' it) as it is
% loaded
defaults.filter = '';

[defaults unvi] = interceptprop(varargin, defaults);



if iscell(defaults.archive)
  % Cell array, so may have several archives to load, try each one in
  % turn
  r = [];
  for n = 1:numel(defaults.archive)
    df = info(getinstrument(in), 'defaultfilename', in, ...
	      'archive', defaults.archive{n});
    if ~isempty(df)
      tmp = feval(basename(mfilename), in, varargin{:}, ...
		  'failiffilemissing', 0, ...
		  'archive', defaults.archive{n});
      % data = getdata(tmp);
      % if ~isempty(data) & ~all(isnan(data(:)))
      if ~isdataempty(tmp) & ~ismissingdata(tmp, 'all')
	% current archive contains some data
	if defaults.multiplearchives
	  % Join old and new data, but ensure the data from the preferred
	  % archives is  not overwritten by data from less favourable ones
	  if isempty(r)
	    r = tmp;
	  else
	    r = nonaninsert(tmp, r);
	  end
	else
	  % found first non-empty archive
	  r = tmp;
	  return
	end
      end
    end
  end
  if isempty(r)
    r = tmp;
  end
  return
end

% from this point on archive is a scalar

df = info(instrument, 'defaultfilename', in, 'archive', defaults.archive);

if isempty(defaults.resolution)
  res = []; % don't set resolution at all
else
  if defaults.resolution > df.duration
    % resolution too large to set whilst loading, attempt to do so afterwards
    res = []; 
  else
    res = defaults.resolution;
  end
  if defaults.resolution > (et - st)
    error('requested resolution larger than duration');
  end
end

if isempty(defaults.filename)
  defaults.filename = df.fstr;
end
if isempty(defaults.failiffilemissing)
  defaults.failiffilemissing = df.failiffilemissing;
end

if isempty(defaults.loadfunction) & isnumeric(defaults.loadfunction)
  defaults.loadfunction = df.loadfunction;
end
if isempty(defaults.format)
  defaults.format = df.format;
end


if ~isempty(defaults.loadfunction)
  if strcmp(defaults.loadfunction, 'error')
    error(['do not know how to load data of ' defaults.format ' format']);
  end

  r = feval(defaults.loadfunction, in, varargin{:}, ...
            'archive', defaults.archive, ...
	    'failiffilemissing', defaults.failiffilemissing);
  return
end

our_formats = {'mat'};
if any(strcmp(defaults.format, our_formats))
  % warn about unknown parameters, but only if it is a format we are
  % loading directly, otherwise pass on to the converter function
  warnignoredparameters(varargin{unvi(1:2:end)});
end

filesLoaded = 0;
t = floor(st, df.duration);

% Allocate a cell array big enough to hold data from all of the files, join
% them in an efficient way later
mia_c = cell(1, ceil((et - t) ./ df.duration));
    
disp('loading files');
while t < et
  cancelcheck(defaults.cancelhandle);
  t2 = t + df.duration;
  
  if strcmp(defaults.filename(1), '@')
    % 'filename' is actually a function we should execute
    filename = feval(defaults.filename(2:end), in, t);
  else
    filename = strftime(t, defaults.filename);
  end

  loaded_ok = 0;

  if ~url_exist(filename)
    if defaults.failiffilemissing
      error(sprintf('Cannot load %s', filename));
    else
      disp(sprintf('\rmissing %s', filename));
    end
  else
    disp(sprintf('\rloading %s', filename));
    clear mia;
    
    % load data, if necessary convert the format whilst loading
    if any(strcmp(defaults.format, our_formats))
      url_load(filename);

      % check we got the variable we expected
      if ~exist('mia', 'var')
        whos
        error('variable called mia not found');
      end

    else 
      % Matlab already has its own user-extensible OPEN
      % command. That isn't used here because we want to pass additional
      % details to the load function. This is because some data files
      % don't contain all the details we need (eg cannot identify the
      % instrument, but the instrument is needed to get specific details
      % for checking the validity of the data file).
      mia = feval([defaults.format '_file_to_mia'], filename, ...
		  'mia', in, ...
		  'starttime', t, ...
		  'archive', defaults.archive, ...
		  'cancelhandle', defaults.cancelhandle, ...
		  varargin{unvi});
    end
    
    loaded_ok = 1;
  end

  
  if loaded_ok
    filesLoaded = filesLoaded + 1;

    if ~strcmp(class(mia), cls) & logical(defaults.correctclass)
      % convert to desired class. This mean we can convert (say)
      % rio_rawpower to rio_power transparently. The conversion has to be
      % done here, not after changing resolution since that uses mean/median
      % and raw power is not linearised.
      mia = feval(cls, mia);
    end

    if ~isempty(defaults.extractargs)
      mia = extract(mia, defaults.extractargs{:});
    end

    if ~isempty(res)
      mia = setresolution(mia, res, defaults.resolutionmethod);
    end
  
    % Allow objects to be passed through a filter as they are loaded.
    mia = mia_filter(mia, defaults.filter);
    
    mia_c{filesLoaded} = mia;
  
  end

  t = t2;
end


if filesLoaded
  mia_c([filesLoaded+1]:end) = []; % remove any unused elements
  r = insertmany(mia_c{:});
  
else
  % it may not be an error if some files cannot be loaded, but if none
  % can be loaded then that possibly should
  warning('Could not load any data files')
  
  % need to set size of data array
  r = in;
  r = setdata(r, feval(df.dataclass, []));
  r = setunits(r, df.units);
end

% copy across creator details
r = setcreator(r, getcreator(in));

% copy across createtime details
ct = getcreatetime(in);
if ~isempty(ct)
  r = setcreatetime(r, ct);
end


% chop to start and end times
r = extract(r, defaults.extractargs{:}, ...
	    'starttime', st, ...
	    'endtime', et);


% If archive was not empty then indicate which archive was used
if ~isempty(defaults.archive) & filesLoaded
  r = addprocessing(r, sprintf('Loaded from %s archive', defaults.archive)); 
end

% If some files were missing then start/end times may not reflect the
% full period for which we have tried loading data, so adjust.
r = setstarttime(r, st);
r = setendtime(r, et);

if ~isempty(defaults.resolution)
  if getdatasize(r, 'end') > 1
    % can only set a resolution if have > 1 sample
    r = setresolution(r, defaults.resolution, defaults.resolutionmethod);
  end
end



