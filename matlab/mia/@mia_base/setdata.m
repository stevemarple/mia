function r = setdata(mia, d, varargin)
%SETDATA  Set the data matrix, or part of.
%
%   r = setdata(mia, d)
%   r = setdata(mia, d, row, column);
%   r: modified MIA_BASE object
%   d: data matrix (row x col)
%   mia: MIA_BASE object
%   row: row(s)
%   col: column(s)
%
%   Set the data matrix, or part of it. Exact meaning of data array is
%   dependent on data type; e.g. rows correspond to beams (riometers) or
%   components (magnetometers), but use GETBEAMINDEX/GETCOMPINDEX to map
%   beam numbers/components into rows. Columns correspond to samples. 
%
%   If row (or col) is zero, or empty, then all rows (or columns) of the
%   data matrix are set. row and col may be vectors.
%
%   For multi-dimensional arrays time index should be the last one. For
%   images use (X, Y, t)
%
%   See also MIA_BASE, GETBEAMINDEX.

if matlabversioncmp('>', '5.1')
  sa = 'subsasgn';
else
  sa = 'subsasg2';
end


r = mia;
if nargin == 2
  r.data = d;

elseif nargin == 3 & isstruct(varargin{1})
  r.data = feval(sa, r.data, varargin{1}, d);
  
% elseif nargin == 4
%   row = varargin{1};
%   col = varargin{2};
%   if isempty(row) | row == 0
%     % do for all beams
%     row = [1:size(mia.data,1)];
%   else
%     row = varargin{1};
%   end
%   if isempty(col) | col == 0
%     % do for all samples
%     col = [1:size(mia.data,2)];
%   end
%   r.data(row, col) = d;
%  
% else
%   error('incorrect parameters');
else
  for n = 1:length(varargin)
    if isequal(varargin{n}, 0)
      warning(['Please use '':'' to signify all elements of matching' ...
	       ' index']);
      varargin{n} = ':';
    end
  end
  s.type = '()';
  s.subs = varargin;
  
  % try
  r.data = feval(sa, r.data, s, d);
  %   catch
  %     whos
  %     s
  %     for n = 1:length(varargin)
  %       disp(printseries(varargin{n}));
  %       disp(sprintf('%d', all(fix(varargin{n}) == varargin{n})));
  %     end
  
  %     error(lasterr);
  %   end
end



