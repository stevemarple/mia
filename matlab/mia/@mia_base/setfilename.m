function r = setfilename(mia, filename)
%SETFILENAME  Set the filename of a MIA_BASE object.
%
%   r = SETFILENAME(mia, filename)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object whose endtime is to be set
%   filename: new filename (CHAR)
%
%   See also GETFILENAME.

r = mia;
r.filename = filename;



