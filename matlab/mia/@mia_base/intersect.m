function r = intersect(a, b)
%INTERSECT  Test if two datasets intersect
%
%   r = intersect(a, b)
%   a: MIA_BASE object (or derived class)
%   b: MIA_BASE object (or derived class)
%
%   See also timestamp/INTERSECT, MIA_BASE.

r = intersect(a.starttime, a.endtime, b.starttime, b.endtime);
