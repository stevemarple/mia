function r = addprocessing(mia, varargin)
%ADDPROCESSING  Add a comment to the processing log.
%
%   r = ADDPROCESSING(mia, s, ...)
%   MIA: MIA object to be modified
%   s: CHAR, or CELL array of CHARS
%
%   Copy string s to the processing log of mia, and return modified
%   MIA_BASE object. Multiple parameters may be spceified.
%
%   See also MIA_BASE.

r = mia;
for n = 1:length(varargin)
  s = varargin{n};
  if ischar(s)
    if ~isempty(s)
      r.processing{end+1} = s;
    end
  elseif iscell(s)
    for n = 1:length(s)
      if ~isempty(s{n})
	r.processing{end+1} = s{n};
      end
    end
  else
    error('Incorrect parameters');
  end
end
