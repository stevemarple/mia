function r = cleardataquality(mia, varargin)
%CLEARDATAQUALITY  Clear data quality warnings for MIA_BASE object.
%
%   r = CLEARDATAQUALITY(mia)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%
%   Remove all data quality warnings
%
%   r = CLEARDATAQUALITY(mia, w)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   w: CHAR or CELL of CHARs of data quality warnings
%
%   Delete all matching data quality warnings (case-insensitive search)
%
% See also GETDATAQUALITY, ADDDATAQUALITY, MIA_BASE.

if numel(mia) ~= 1
  error('mia must be scalar');
end

r = mia;

switch nargin
 case 1
  r.dataquality = {};

 case 2
  if ischar(varargin{1})
    c = {varargin{1}};
  elseif iscell(varargin{1})
    c = varargin{1};
  else
    error('incorrect parameters');
  end
  
  for n = 1:numel(c)
    pos = strcmpi(r.dataquality, c{n});
    if any(pos)
      r.dataquality(pos) = [];
    end
  end
  
 otherwise
  
  error('incorrect parameters');
end

