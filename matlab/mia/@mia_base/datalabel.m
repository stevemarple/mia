function [r, vsu, v, vs, mul] = datalabel(mia, varargin)
%DATALABEL  Print a datalabel, and return details on units to use
%
% [r, vsu, v, vs, mul] = datalabel(mia, ...)
% r: CHAR title label
% vsu: values as string with units
% v: values used in calculation
% vs: values as string
% mul: multiplier (as LOG10 value)

r = '';
vs = {};

defaults.values = [];
defaults.parameterindex = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if length(defaults.parameterindex) > 1
  % allow empty matrix to get default case
  error('parameterindex must be a scalar or empty matrix');
end

units = getunits(mia, 'parameterindex', defaults.parameterindex);
if iscell(units) 
  if isempty(defaults.parameterindex)
    error(['when multiple units are present parameterindex must be' ...
	   ' specified']);
  end
  units = units{1};
end

[fstr multiplier] = datalabel2(mia, ...
			       'parameterindex', defaults.parameterindex);

if isempty(units)
  r = sprintf(fstr, '');
  return;
end


if ~isempty(defaults.values)
  % set units to something appropriate for the biggest value
  val = max(defaults.values(:));
  [tmp1 tmp2 multiplier mul_str] = printunits(val, units, ...
					      'multiplier', multiplier);
elseif ~isempty(multiplier)
  % don't have any values but do know the multiplier, therefore can find
  % out what mul_str to use. Use NaN as a dummy value to printunits
  [tmp1 tmp2 multiplier mul_str] = printunits(nan, units, ...
					      'multiplier', multiplier);  
else
  mul_str = '';
end

r = sprintf(fstr, [mul_str units]);
[vsu v mul tmp2 vs] = printunits(defaults.values, units, ...
				  'multiplier', multiplier);
