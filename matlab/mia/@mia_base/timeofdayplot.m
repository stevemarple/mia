function varargout = timeofdayplot(mia, varargin)
%PLOT Plot a MIA_BASE (or derived) object with labels, title etc.
%
%   [gh fh ...] = TIMEOFDAYPLOT(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   mia: MIA_BASE (or derived) object
%
% TIMEOFDAYPLOT is very similar to mia_base/PLOT (which it calls internall),
% but the TIMESTAMPs used for plotting are converted to time of day (see
% timestamp/GETTIMEOFDAY).
%
% TIMEOFDAYPLOT adds the following name/value pairs which can be used to
% modify the behaviour:
%
%   'duplicate', LOGICAL
%   Duplicate plotting objects (LINE etc) which contain an XData property
%   and split across midnight boundary. Default is TRUE.
%
%   'usecolororder', LOGICAL or empty matrix
%   Change the COLOR of plotting objects (LINE etc) to use the AXES
%   ColorOrder property. If an empty matrix is given then the AXES
%   ColorOrder property is used only if no LEGEND is present.
%
% For details on other name/value pairs which may be use see
% mia_base/PLOT.
%
% See also mia_base/PLOT.

% Duplicate plotting objects containing XData and split across midnight
% boundary.
defaults.duplicate = true;

% Changing colors to follow ColorOrder only makes sense when objects are
% duplicated.
defaults.usecolororder = [];

[defaults unvi] = interceptprop(varargin, defaults);

narg = nargout;
if narg < 2
  r = cell(1, 2);
else
  r = cell(1, nargout);
end


[r{:}] = plot(mia, varargin{unvi});

fh = r{1};
gh = r{2};


for ghn = 1:numel(gh)
  h = allchild(gh(ghn));
  axesModified = false;
  for hn = 1:numel(h)
    p = get(h(hn));
    if isfield(p, 'XData')
      axesModified = true;

      % wrap XData into timeofday
      xdata = getcdfepochvalue(gettimeofday(timestamp('cdfepoch', p.XData)));

      % Look for discontinuities in xdata
      mc = find(diff(xdata) < 0); % midnight crossings
      if isempty(mc)
	continue
      end
      
      if logical(defaults.duplicate)
	mc = [0 mc numel(xdata)];
	colororder = get(gh(ghn), 'ColorOrder');
	
	% delete read-only properties in p
	pType = p.Type;
	for prop = readOnlyProperties
	  if isfield(p, prop)
	    p = rmfield(p, prop);
	  end
	end
	
	for mcn = 1:(numel(mc)-2)
	  p2 = p;
	  p2.XData = xdata((mc(mcn)+1):mc(mcn+1));
	  p2.YData = p.YData((mc(mcn)+1):mc(mcn+1));
	  if ~isempty(p2.ZData)
	    p2.ZData = p.ZData((mc(mcn)+1):mc(mcn+1));
	  end
	  if isfield(p2, 'CData') && ~isempty(p2.CData) 
	    p2.CData = p.CData((mc(mcn)+1):mc(mcn+1));
	  end
	  
	  % use the axes ColorOrder property?
	  if isempty(defaults.usecolororder)
	    % only if not legend present
	    usecolororder = isempty(legend(gh(ghn)));
	  else
	    usecolororder = defaults.usecolororder;
	  end
	  
	  if logical(defaults.usecolororder) & isfield(p2, 'Color')
	    p2.Color = colororder(mymod(mcn, size(colororder, 1)), :);
	  end
	  feval(pType, p2);
	end
	delete(h(hn));
	
      else
	% Now insert nans at these locations. Subsequent crossing points
	% must be delayed by one sample because of the NaN inserted
	mc = mc + colon(0, numel(mc) - 1);
	
	ydata = p.YData;
	zdata = p.ZData;
	if isfield(p, 'CData')
	  cdata = p.CData;
	else
	  cdata = [];
	end
	for mcn = 1:numel(mc)
	  xdata = [xdata(1:mc(mcn)) nan xdata((mc(mcn)+1):end)];
	  ydata = [ydata(1:mc(mcn)) nan ydata((mc(mcn)+1):end)];
	  if ~isempty(zdata)
	    zdata = [zdata(1:mc(mcn)) nan zdata((mc(mcn)+1):end)];
	  end
	  if ~isempty(cdata)
	    cdata = [cdata(1:mc(mcn)) nan cdata((mc(mcn)+1):end)];
	  end
	end
	
	args = {'XData', xdata, 'YData', ydata, 'ZData', zdata};
	if isfield(p, 'CData')
	  args{end+1} = 'CData';
	  args{end+1} = cdata;
	end

	set(h(hn), args{:});
	
      end
      
    end
  end
  if axesModified
    set(gh(ghn), 'XLim', [0 getcdfepochvalue(timespan(1,'d'))]);
    timetick2(gh(ghn), 'class', 'timespan', 'label', 'time of day');
  end
end


% Return as many return values as originally requested.
if nargout
  varargout = r(1:nargout);
end

% List of read-only properties
function r = readOnlyProperties
r = {'BeingDeleted', 'Type'};
return


function r = mymod(a, b)
r = mod(a, b);
if r == 0
  r = b;
end
