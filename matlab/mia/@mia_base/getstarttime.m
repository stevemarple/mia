function r = getstarttime(mia)
%GETSTARTTIME  Return the starttime of the MIA_BASE object
%
%   r = GETSTARTTIME(mia)
%   r: TIMESTAMP object
%   mia: MIA_BASE object
%
%   See also GETMIDTIME, GETENDTIME, GETDURATION, TIMESTAMP.

if length(mia) == 1
  r = mia.starttime;
else
  r = repmat(timestamp, size(mia));
  for n = 1:prod(size(mia))
    r(n) = mia(n).starttime;
  end
end
