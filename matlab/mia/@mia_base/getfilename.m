function r = getfilename(mia)
%GETFILE  Return the filename of the MIA_BASE object.
%
%   r = GETFILENAME(mia)
%   r: filename (CHAR)
%   mia: MIA_BASE object.
%
%   See also MIA_BASE.

r = mia.filename;
