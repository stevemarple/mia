function [r, sr] = removemissingdata(mia, varargin)
%REMOVEMISSINGDATA Remove samples which correspond to missing data.
%
%   [r sr] = REMOVEMISSINGDATA(mia, ...)
%   r: MODIFIED MIA_BASE object
%   sr: STRUCT for SUBSREF
%   mia: MIA_BASE object
%
% REMOVEMISSINGDATA remove samples which correspond to missing
% data. Additional name/value pairs can be supplied to modify which missing
% samples are detected. See FINDMISSINGDATA for more details.
%
% See also FINDMISSINGDATA, MARKMISSINGDATA.

r = mia;

sr.type = '()';
sr.subs = repmat({':'}, [1 getndims(mia)]);

idx = findmissingdata(mia, varargin{:});
if isempty(idx)
  return; % no missing data
end

dsz = getdatasize(mia);
sr.subs{end} = 1:dsz(end);
sr.subs{end}(idx) = [];

r = extract(mia, sr.subs);

