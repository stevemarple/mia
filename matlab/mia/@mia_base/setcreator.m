function r = setcreator(mia, c)
%SETCREATOR Set the creator name
%
%    r = SETCREATOR(mia, c)
%    r: creator (string)
%    mia: MIA_BASE object
%    c: new creator (string)

r = mia;
for n = 1:numel(r)
  r(n).creator = c;
end

