function r = getendtime(mia)
%GETENDTIME  Return the endtime of the MIA_BASE object
%
%   r = GETENDTIME(mia)
%   r: TIMESTAMP object
%   mia: MIA_BASE object
%
%   See also GETSTARTIME, GETMIDTIME, GETDURATION, TIMESTAMP.

if length(mia) == 1
  r = mia.endtime;
else
  r = repmat(timestamp, size(mia));
  for n = 1:prod(size(mia))
    r(n) = mia(n).endtime;
  end
end
