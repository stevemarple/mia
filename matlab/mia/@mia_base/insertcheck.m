function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Check if object can be inserted into other object.
%
%   [aa, ar, ba, br, rs] = INSERTCHECK(a, b)
%
%   aa: SUBSASGN STRUCT for copying object a into final object
%   ar: SUBSREF STRUCT for copying object a into final object
%   ba: SUBSASGN STRUCT for copying object b into final object
%   br: SUBSREF STRUCT for copying object b into final object
%   rs: return object data size
%
% INSERTCHECK check to see if an object can be inserted into another
% object. If not ERROR is called with an appropriate error message. 
%
% Developers notes:
%
% If the objects are suitable then a set of return values indicate how
% objects a and b should be copied into the final object. Inside the INSERT
% function object a is first accessed with a call to SUBSREF to extract the
% appropriate parts of the data to copy; that temporary variable is then
% inserted into the return object's data matrix with a call to SUBSASGN. The
% calls to SUBSREF and SUBSASGN use ar and aa respectively in order to
% reference/assign the appropriate elements. This process is repeated for
% object b.
%
% If classes derived from MIA_BASE require a different behaviour then
% INSERTCHECK should be overloaded accordingly. See rio_base/INSERTCHECK
% as an example.
%
% See also INSERT, NONANINSERT.

if ~strcmp(class(a), class(b))
  error(sprintf('all objects must be of the same type (were %s and %s)', ...
		class(a), class(b)));
end


ndims = getndims(a);
if getndims(b) ~= ndims
  error('number of dimensions must be equal');
end

instrument = getinstrument(a);
if instrument ~= getinstrument(b)
  error('all objects should be data from the same instrument');
end

units = getunits(a);
% if units ~= getunits(b)
if ~strcmp(units, getunits(b))
  error(sprintf(['all objects should have the same units ' ...
                 '(were ''%s'' and ''%s'')'], units, getunits(b)));
end

sta = getstarttime(a);
eta = getendtime(a);
stb = getstarttime(b);
etb = getendtime(b);

if matlabversioncmp('<=', '5.1')
  rs.subsref = 'subsref2';
  rs.subsasgn = 'subsasg2';
else
  rs.subsref = 'subsref';
  rs.subsasgn = 'subsasgn';
end


rs.starttime = min(sta, stb);
rs.endtime = max(eta, etb);
rs.sampletime = unique([a.sampletime b.sampletime]);

if matlabversioncmp('<=', '5.1')
  % horzcat does not always preserve dimensions
  rs.sampletime = reshape(rs.sampletime, 1, numel(rs.sampletime));
end

% unless told otherwise (by derived insertcheck functions) will have to
% assume all rows in a and all rows in b are copied to the final object.

aa.type = '()';
aa.subs = repmat({':'}, [1 ndims]);
% find out where in final data matrix samples must go
[tmp aa.subs{end}] = ismember(a.sampletime, rs.sampletime);

ar.type = '()';
ar.subs = repmat({':'}, [1 ndims]);


ba.type = '()';
ba.subs = repmat({':'}, [1 ndims]);
% find out where in final data matrix samples must go
[tmp ba.subs{end}] = ismember(b.sampletime, rs.sampletime);

br.type = '()';
br.subs = repmat({':'}, [1 ndims]);

rs.datasize = max([getdatasize(a); getdatasize(b)], [], 1);
rs.datasize(end) = length(rs.sampletime);

