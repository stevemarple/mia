function r = setinstrument(mia, in)
%SETINSTRUMENT  Set the instrument(s) in a MIA_BASE object.
%
%   r = SETINSTRUMENT(mia, in)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   in: MIA_INSTRUMENT_BASE (scalar or vector)
%
%   See also GETINSTRUMENT.

r = mia;
r.instrument = in;
