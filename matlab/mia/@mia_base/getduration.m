function r = getduration(mia)
%GETDURATION  Return the duration of the MIA_BASE object.
%
%   r = GETDURATION(mia)
%   mia: a MIA_BASE object
%   r: the duration as a TIMESPAN

r = mia.endtime - mia.starttime;
