function r = dateprintf(mia)
%DATEPRINTF  Print the start/end times/dates for a MIA_BASE object.
%
%   r = DATEPRINTF(mia)
%   r: CHAR repsenting start/end TIMESTAMP range
%   mia: MIA_BASE object
%
%   mia/DATEPRINTF calls DATEPRINTF to neatly print the start and end
%   times.
%
%   See also DATEPRINTF.

r = dateprintf(mia.starttime, mia.endtime);
