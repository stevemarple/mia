function r = adddataquality(mia, str)
%ADDDATAQUALITY  Add extra data quality warnings to MIA_BASE object.
%
%   r = ADDDATAQUALITY(mia, str)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   str: data quality warning (CHAR or CELL)
%   Apply a data quality warning to the dataset.
%
% See also GETDATAQUALITY, SETDATAQUALITY, MIA_BASE.


if numel(mia) ~= 1
  error('mia must be scalar');
end

r = mia;

if ischar(str)
  str = {str};
elseif ~iscell(str)
  error('incorrect parameters');
elseif nargin ~= 2
  error('incorrect parameters');
end

dq = [r.dataquality str];
% dq{end + 1} = str;

r.dataquality = unique(dq);



