function save(miaIn, varargin)
%SAVE  Save a MIA_BASE object
%
%   SAVE(mia)
%   If the object's filename (see GETFILENAME) is empty the object is
%   saved into the default location (system administrators only). If the
%   filename is not empty then it is used as a format string for
%   STRFTIME, and the object saved to that location.
%
%   SAVE(mia, fstr)
%   Save into the location specified with the format string. Equivalent
%   to SAVE(mia, 'fstr', fstr)
%
%   SAVE(mia, ...)
%   Save, using additional parameter name/value pairs. The following
%   name/value pairs are recognised:
%
% 'fstr', CHAR
% Set the file location to be used when saving file(s). fstr is a STRFTIME
% format string.
%
% 'merge, LOGICAL
% Indicate if the data being saved should be merged into any existing
% data on disk. If true this means the old file is loaded, the new
% data inserted (using NONANINSERT) and then saved. False means overwrite
% any existing file.
%
% 'overwrite', LOGICAL
% Indicate if existing files should be overwritten. Default is true. If
% file exists and overwrite is false an error occurs.
%
% When STRFTIME is used to generate a filename the start time is the time
% passed to STRFTIME.
%
% See also NONANINSERT, STRFTIME.

if numel(miaIn) ~= 1
  miaIn
  error('cannot use class save function for non-scalars');
end

instrument = getinstrument(miaIn);
[s mesg] = info(instrument, 'defaultfilename', class(miaIn));
if ~isempty(s) && ~isempty(s.savefunction)
  feval(s.savefunction, miaIn, varargin{:});
  return
end

if ~isempty(s)
  defaults.fstr = s.fstr;
else
  % class does not have a default filename. Make one up
  defaults.fstr = gettype(miaIn);
  defaults.fstr(' ') = '_'; % map any spaces to underscores
end

% if the object has its filename set then use that if preference to the
% class default. however, object's filename can be overridden by a supplied
% name.
if ~isempty(getfilename(miaIn))
  defaults.fstr = getfilename(miaIn);
end

defaults.merge = 0;
defaults.overwrite = 1;
defaults.filter = '';
% defaults.archive = ''; % Not yet supported

if nargin == 1
  if isempty(s)
    error(sprintf('no default filename for ''%s'' class', class(miaIn)));
  end
  
elseif nargin == 2
  defaults.fstr = varargin{1};
  
elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
else
  error('Incorrect number of parameters');
end

if isempty(s)
  s.duration = [];
elseif ~isfield(s, 'duration');
  s.duration = [];
end

% TODO: Implement archive selection. Until then assume defaault archive
% and enable it to be passed to save formatters.
defaults.archive = '';

our_formats = {'mat'};
st = getstarttime(miaIn);
et = getendtime(miaIn);
t = st;
if ~isempty(s.duration)
  t = floor(st, s.duration);
end


while t < et
  if ~isempty(s.duration)
    t2 = t + s.duration;
    miaSave = extract(miaIn, 'starttime', t, 'endtime', t2);
  else
    t2 = et;
    miaSave = miaIn;
  end
  filename = strftime(t, defaults.fstr);
  
  filename_exists = url_exist(filename);
  if ~logical(defaults.overwrite) & filename_exists
    error(sprintf('%s exists and overwriting forbidden', filename));
  end
  
  if logical(defaults.merge) & filename_exists
	if any(strcmp(s.format, our_formats))
	  clear('mia');
	  disp(['loading ' filename]);
	  load(filename);
	  if ~exist('mia', 'var')
		error('expected to have a variable called ''mia'' but did not find one');
	  end
	else
	  mia = feval(class(miaIn), ...
				  'instrument', getinstrument(miaSave), ...
				  'starttime', getstarttime(miaSave), ...
				  'endtime', getendtime(miaSave), ...
				  'loadoptions', {'archive', defaults.archive});
	end
	
	% insert the new data over the top of existing data from disk
	miaSave = nonaninsert(mia, miaSave);
  end
  
  % Apply any filtering
  miaSave = mia_filter(miaSave, defaults.filter);
  
  disp(['saving ' filename]);
  mia = miaSave;
  tmp = url_mkdir(fileparts(filename)); % ignore any mkdir errors

  if any(strcmp(s.format, our_formats))
    % builtin('save', filename, 'mia');
  
	% Get the save options every time, allows them to be dependent on what
	% is being saved
	opts = saveopts(mia);
    url_save(filename, 'mia', opts{:});
  else
	% Order is reversed to load, so that it is easier to override the
    % function based on the data type.
	feval(['mia_to_' s.format '_file'], ...
		  mia, filename, ...
		  'archive', defaults.archive);
  end
  t = t2;
end
  

