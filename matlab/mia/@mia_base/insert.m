function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT/NONANINSERT  Insert MIA_BASE object into another MIA_BASE object.
%
%   r = INSERT(a, b)
%   r = NONANINSERT(a, b)
%
% Insert a MIA_BASE object into another, similar MIA_BASE object. If samples
% coincide then the samples from "b" will overwrite the samples from "a"
% (the original input parameters are unaffected). When called as NONANINSERT
% data then missing data samples from "b" will not overwrite valid samples
% from "a".
%
% In Unix systems NONANINSERT is a symblic link to INSERT.

[aa ar ba br rs] = insertcheck(a, b);

sta = getstarttime(a);
eta = getendtime(a);
stb = getstarttime(b);
etb = getendtime(b);

integrationtime = repmat(timespan(nan, 's'), [1 rs.datasize(end)]);

it_ar.type = '()';
it_ar.subs = {':', ar.subs{end}};
it_aa.type = '()';
it_aa.subs = {':', aa.subs{end}};

it_br.type = '()';
it_br.subs = {':', br.subs{end}};
it_ba.type = '()';
it_ba.subs = {':', ba.subs{end}};

integrationtime = feval(rs.subsasgn, integrationtime, it_aa, ...
			feval(rs.subsref, a.integrationtime, it_ar));
integrationtime = feval(rs.subsasgn, integrationtime, it_ba, ...
			feval(rs.subsref, b.integrationtime, it_br));

proc_a = getprocessing(a);
proc_b = getprocessing(b);
if isequal(proc_a, proc_b)
  proc = proc_a;
else
  proc = [proc_a proc_b];
end

r = feval(class(a), ...
	  'starttime', rs.starttime, ...
	  'endtime', rs.endtime, ...
	  'sampletime', rs.sampletime, ...
	  'integrationtime', integrationtime, ...
	  'instrument', getinstrument(a), ...
	  'units', getunits(a), ...
          'data', repmat(feval(class(getdata(a)), nan), rs.datasize), ...
	  'processing', proc, ...
	  'load', 0);

if ~isdataempty(a)
  % copy a
  r = setdata(r, getdata(a, ar.subs{:}), aa.subs{:});
end

if isdataempty(b)
  ; % do nothing
elseif ~isempty(findstr(mfilename, 'nonaninsert'))
  % do NONANINSERT behaviour
  
  % extract the data which is about to be overwritten
  r_tmp = getdata(r, ba.subs{:});

  % get the data from b which is to be inserted
  b_tmp = getdata(b, br.subs{:});
  
  % Find location of nans in the data which is about to be
  % inserted. Overwrite the nans with the values already there.
  nans = isnan(b_tmp);
  b_tmp(nans) = r_tmp(nans);

  % copy b, any nans have been substituted by the existing data
  r = setdata(r, b_tmp, ba.subs{:});
  
else
  % do normal INSERT behaviour
  
  % copy b
  r = setdata(r, getdata(b, br.subs{:}), ba.subs{:});
end

% join data quality
r.dataquality = combinedataquality(a, b);

