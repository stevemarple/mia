function r = getinstrument(mia)
%GETFILE  Return the instrument(s) of the MIA_BASE object.
%
%   r = GETINSTRUMENT(mia)
%   r: instrument(s) (MIA_INSTRUMENT_BASE)
%   mia: MIA_BASE object.
%
%   The instrument may be a scalar quantity, or a vector of
%   MIA_INSTRUMENT_BASE.
%
%   See also MIA_BASE, MIA_INSTRUMENT_BASE.

switch length(mia)
 case 0
  r = repmat(mia_instrument_base, [0 0]);
  
 case 1
  r = mia.instrument;
  
 otherwise
  tmp = mia(1);
  r = repmat(tmp.instrument, size(mia));
  for n = 2:prod(size(mia))
    tmp = mia(n);
    r(n) = tmp.instrument;
  end
end


