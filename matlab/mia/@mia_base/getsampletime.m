function r = getsampletime(mia, varargin)
%GETSAMPLETIME  Return times of data samples.
%
%   r = GETSAMPLETIME(mia);
%   r = GETSAMPLETIME(mia, n);
%   r: TIMESTAMP vector, where length(r) == number of samples
%   mia: MIA_BASE object (scalar)
%   n: NUMERIC value (scalar or vector)
%
% GETSAMPLETIME returns the (central) time of each sample, or optionally the
% time(s) of sample n (n may be a vector).
%
% See also GETSTARTTIME, GETENDTIME, GETSAMPLESTARTTIME, GETSAMPLEENDTIME.

if numel(mia) ~= 1
  error('mia must be scalar');
end

switch length(varargin)
 case 0
  r = mia.sampletime;

 case 1
  if strcmp('end', varargin{1})
    r = mia.sampletime(end);
  elseif isnumeric(varargin{1})
    r = mia.sampletime(varargin{1});
  else
    error('incorrect type for subscript');
  end
  
 otherwise
  error('incorrect parameters');
end
