function r = getcreatetime(mia)
%GETCREATETIME  Return the creation time of the MIA_BASE object.
%
%   r = GETCREATETIME(mia)
%   r: creation TIMESTAMP
%   mia: original MIA_BASE object
%
%   See also TIMESTAMP, MIA_BASE, SETCREATETIME.

r = mia.createtime;
