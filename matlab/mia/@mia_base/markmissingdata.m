function [r,sa,badt,badidx] = markmissingdata(mia, varargin)
%MARKMISSINGDATA  Mark periods of missing data by insertion of NaNs
%
%   [r sa badt] = MARKMISSINGDATA(mia, ...)
%   r: MIA_BASE object
%   sa: STRUCT suitable for SUBSREF
%   badt: list of TIMESTAMP objects for the newly-inserted NaNs
%   badidx: subscript in time dimension for the newly inserted NaNs
%
% MARKMISSINGDATA marks extended periods of missing data by inserting
% NANs. This ensures that when the data is plotted datagaps are shown as
% missing. The behaviour can be modified with the following name/value
% pairs:
%
% 'resolution', TIMESPAN
% The nominal interval for the spacing of the data. If it considered to be a
% data gap if samples are spaced at twice the resolution or more. If the
% resolution is not specified then an approximation is used, see
% GUESSRESOLUTION.
%
%
% Note To Developers
%
% When new classes are created it may be necessary to overload
% MARKMISSINGDATA, particularly if per-sample metadata is involved.
%
% See aslo FINDMISSINGDATA, REMOVEMISSINGDATA.

r = mia;
sa = [];
badt = [];
badidx = [];

defaults.resolution = [];
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.resolution)
  defaults.resolution = guessresolution(mia);
end

if ~isvalid(defaults.resolution)
  % probably don't have enough samples to guess the resolution
  return;
end

samt = getsampletime(mia);

% Check if any samples are spaced by >= 2 * resolution. Only need to know
% the first of each pair which are spaced too far apart.
badspacing = find(diff(samt) >= 2 * defaults.resolution);

if isempty(badspacing)
  % none are spaced too far apart
  return
end

% if any of the pair of samples spaced too far apart are NaNs then no
% need to insert another NaN. Need to check that all samples at a given
% time are NaN. findmissingdata will tell us the index into sampletime
% for the case that all data is nan.
nansidx = findmissingdata(mia, 'method', 'all');

% only keep badspacing values which do not correspond to missing data
badspacing = setdiff(badspacing, nansidx);

% do the same again, but looking at the second sample of each pair
badspacing_2nd = badspacing + 1;
badspacing_2nd = setdiff(badspacing_2nd, nansidx);

% are there still any samples left to process?
if isempty(badspacing_2nd)
  return
end

% update list of the first sample of each pair
badspacing = badspacing_2nd -1;

% badt = mean(samt([badspacing;badspacing+1]));
% Insert NaN exactly halfway between the samples which are spaced too far
% apart
badt = mean(samt([badspacing;badspacing_2nd]));

rst = unique([samt badt]);
if matlabversioncmp('<=', '5.1')
  % horzcat does not always preserve dimensions
  rst = reshape(rst, 1, numel(rst));
end

data = getdata(mia);
% create new data matrix
dsz = getdatasize(mia); % original size
dsz(end) = numel(rst);  % size to include new data samples
  
% ceate new data matrix; cast nan to appropriate type, then repeat using
% repmat to get the correct size matrix
rdata = repmat(feval(class(data), nan), dsz);

% copy into rdata the old samples. Any gaps will be nans.
sa.type = '()';
sa.subs = repmat({':'}, [1 getndims(mia)]);
[tmp sa.subs{end}] = ismember(samt, rst);

if matlabversioncmp('>', '5.1')
  rdata = subsasgn(rdata, sa, data);
else
  rdata = subsasg2(rdata, sa, data);
end

r = setdata(r, rdata);
r = setsampletime(r, rst);
it = timespan(repmat(nan, size(rst)), 's');
it(sa.subs{end}) = getintegrationtime(r);
r = setintegrationtime(r, it);
r = addprocessing(r, 'NaNs inserted to mark missing data');

if nargout >= 4
  % find indicies f the bad times
  [tmp badidx] = ismember(badt, rst);
end
