function r = resampledata(mia, t, varargin)
%RESAMPLEDATA  Space timestamped data to regular intervals
%
%   r = RESAMPLEDATA(t1, t, ...)
%   r: regularly-spaced data
%   t: new sample times (TIMESTAMP)
%
% The following parameter name/value pairs can be used to modify the
% behaviour:
%
% 'method', CHAR
% The method interp1 should use for its interpolation. See INTERP1 for
% details of valid methods.
%
% See also INTERP1.


r = mia;

defaults.method = ''; % defer, to allow '' to select default method
defaults.resolution = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

samt = getsampletime(mia);

if isequal(samt, t)
  % data already sampled at the times requested
  return;
end

it = getintegrationtime(mia);


if isempty(defaults.method)
  defaults.method = 'linear';
end

data = getdata(mia);
origsz = getdatasize(mia);
dsz = origsz;
dsz(end) = numel(t);
if getndims(mia) > 2
  % pretend data is only 2D
  reshapesz = [prod(origsz(1:(end-1))) origsz(end)];
  data = reshape(data, reshapesz);
end

samt_epoch = getcdfepochvalue(samt);
t_epoch = getcdfepochvalue(t);

% interpolate between samples onto desired spacing
rdata = repmat(nan, size(data,1), numel(t_epoch));
for n = 1:size(data,1)
  rdata(n, :) = interp1(samt_epoch, data(n,:), t_epoch, defaults.method);
end
rdata = feval(class(data), rdata);

% use linear interpolation to find integration time of each sample
it = getintegrationtime(mia);
it2 = timespan(interp1(samt_epoch, getcdfepochvalue(it), t_epoch), 'ms');


r = setsampletime(r, t);
if isempty(defaults.resolution)
  defaults.resolution = guessresolution(r);
end

% if there are new samples before the old first sample set them to have
% the same value but only if they are within the resolution
idx = find(t < samt(1) & t > samt(1) - defaults.resolution);
if ~isempty(idx)
  rdata(:, idx) = repmat(data(:, 1), 1, length(idx));
  it2(idx) = it(1);
end

% do the same at the end
idx = find(t > samt(end) & t < samt(end) + defaults.resolution);
if ~isempty(idx)
  rdata(:, idx) = repmat(data(:, end), 1, length(idx));
  it2(idx) = it(end);
end

if getndims(mia) > 2
  % return data to original shape
  rdata = reshape(rdata, dsz);
end

r = setdata(r, rdata);
r = setintegrationtime(r, it2);
r = addprocessing(r, sprintf('Data resampled using %s interpolation', ...
			     defaults.method));
