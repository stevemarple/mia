function [fh, gh, lh, wmH, imap, ordering] = stackplot(mia, varargin)
%STACKPLOT  Stack plot of MAG_DATA.
%
%   [fh, gh, lh, wmH, imap, ordering] = stackplot(mia, ...)
%
%   fh: FIGURE handle
%   gh: AXES handles
%   lh: LINE handles
%   wmH: watermark handles
%   imap: CELL array of positions for HTML image maps.
%   ordering: DOUBLE matrix indicating the plotting order
%   mia: vector or matrix of MAG_DATA objects
%
% STACKPLOT creates a stacked plot of data, in the order they were
% passed. The default behaviour may be modified with the following parameter
% name/value pairs:
%
%   'endtime', TIMESTAMP
%   The end of the plot interval, default is the end time of the first
%   MAG_DATA object.
%
%   'ignoreemptydatasets', LOGICAL
%   Ignore any data sets which are composed entirely of missing data
%   values (NAN). Default is 1.
%
%   'label, CHAR
%   Label to be applied to each dataset. Label is passed to EVAL and
%   return argument taken as the label. The variable 'in' may be used to
%   access the current instrument.
%
%   'order', NUMERIC or CHAR
%   When a NUMERIC value is given it is taken as the index into the
%   matrix of data objects. Valid CHAR options are: 'latitude',
%   'longitude', 'cgmlatitude', 'cgmlongitude' and 'L' ('L value').
%
%   'orderdir', CHAR ('asc' | 'ascending' | 'desc' | 'descending')
%   Order in ascending or descending order. Default is ascending.
%
%   'parameters', CELL
%   The parameters (component(s)) to plot.
%
%   'plotaxes', AXES HANDLE
%   By default STACKPLOT will create a new FIGURE (using MAKEPLOTFIG),
%   but if supplied with an AXES handle the data will be plotted into
%   that AXES instead.
%
%   'starttime', TIMESTAMP
%   The start of the plot interval, default is the start time of the first
%   MAG_DATA object
%
%   'watermark' CHAR or CELL array ([] to disable)
%   Watermarks which should be plotted behind the data, to indicate if the
%   data is preliminary, realtime etc. Default is an empty DOUBLE matrix, so
%   that watermarks are not printed.
%
%   'watermarkcolor', COLORSPEC
%   The color of the watermark text.
%
% Unknown parameters are passed onto MAKEPLOTFIG.
%
% See also MAKEPLOTFIG.

if isempty(mia)
  error('no data to plot');
end

mia = reshape(mia, numel(mia), 1);
ordering = 1:numel(mia);

defaults.starttime = max(getstarttime(mia));
defaults.endtime = min(getendtime(mia));
% defaults.resolution = max(getresolution(mia));
defaults.parameters = [];
defaults.plotaxes = [];
defaults.multiplier = []; % defer
defaults.watermark = [];
defaults.watermarkcolor = [1 .6 .6];

% label for identifying datasets
defaults.label = ''; % defer

defaults.order = []; % defer
defaults.orderdir = 'asc';

defaults.color = [];
defaults.visible = 'on';

% accept the parameter names too (beams for riometers, components for
% mag_data etc) EXPERIMENTAL
pn = getparametername(mia(1));
defaults = setfield(defaults, pn{2}, []);

defaults.title = [];
defaults.windowtitle = [];

defaults.ignoreemptydatasets = 1;
[defaults unvi] = interceptprop(varargin, defaults);

% rearrange according to chosen order
if isempty(defaults.order)
  ; % do nothing
elseif isnumeric(defaults.order)
  ordering = defaults.order;
elseif ~ischar(defaults.order)
  error('order must be numeric or char');
else
  in = getinstrument(mia);
  % instruments are allowed to have multiple locations, so take mean
  % values
  lat = zeros(size(mia));
  lon = zeros(size(mia));
  for n = 1:numel(in)
    loc = getlocation(in(n));
    lat(n) = mean(getgeolat(loc));
    lon(n) = mean(getgeolong(loc));
  end
  
  % Ensure West is < 0 degrees
  lon(lon > 180) = lon(lon > 180) - 180;
  
  switch defaults.order
   case 'alphabetical'
    [tmp orderval] = sort(getcode(in));
   case 'latitude'
    orderval = lat;
   case 'longitude'
    orderval = lon;
   case {'cgmlatitude' 'cgmlongitude', 'L'}
    cgm = geocgm('latitude', lat, ...
		 'longitude', lon, ...
		 'direction', 'geo2cgm', ...
		 'year', getyear(mean([defaults.starttime; ...
		    defaults.endtime])));
    switch defaults.order
     case 'cgmlatitude'
      orderval = cgm.cgm_latitude;
     case 'cgmlongitude'
      orderval = cgm.cgm_longitude;
     case 'L'
      orderval = cgm.rbm;
     otherwise
      error('logic error');
    end
    
   otherwise 
    error(sprintf('unknown value for order (was ''%s'')', ...
		  defaults.order));
  end
  [tmp ordering] = sort(orderval);
end
  
mia = mia(ordering);

if ~isempty(defaults.order)
  % honour orderdir only if ordering is set
  switch defaults.orderdir
   case {'asc' 'ascending'}
    mia = mia(numel(mia):-1:1);
    ordering = fliplr(ordering(:)');
    
   case {'desc' 'descending'}
    ; % do nothing
   otherwise
    error(sprintf('unknown order direction (was ''%s'')', ...
		  defaults.orderdir));
  end
end

errormesg = '';
if logical(defaults.ignoreemptydatasets)
  % delete objects where all data is blank
  missing_idx = ismissingdata(mia, 'all');
  mia(missing_idx) = [];
  ordering(missing_idx) = [];
  
  if isempty(mia)
    % error('no non-empty datasets to plot');
    errormesg = {{'No non-empty' 'datasets to plot'}};
  end
end

if isempty(defaults.parameters)
  pndata = getfield(defaults, pn{2});
  if ~isempty(pndata)
    defaults.parameters = pndata;
  else
    % tmp = getparameters(mia(1));
    % defaults.parameters = tmp(1);
  end
end

if isempty(defaults.label)
  % defaults.label = 'upper(getcode(in))';
  defaults.label = 'sprintf(''%s (#%d)'', upper(getabbreviation(in)), getserialnumber(in))';
end

if ~isempty(mia)
  [plottitle windowtitle] = maketitle(mia, ...
				      'starttime', defaults.starttime, ...
				      'endtime', defaults.endtime, ...
				      pn{2}, defaults.parameters);
else
  plottitle = '';
  windowtitle = '';
end
if isempty(defaults.title)
  defaults.title = plottitle;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

num = numel(mia);


if isempty(defaults.plotaxes)
  [fh gh] = makeplotfig('init', ...
			'title', defaults.title, ...
			'name', windowtitle, ...
			'visible', 'off', ...
			'watermark', errormesg, ...
			'tag', 'stackplot', ...
			varargin{unvi});
else
  gh = defaults.plotaxes;
  fh = get(gh, 'Parent');
end

lh = zeros(num, 1);
if ischar(defaults.watermark) | iscell(defaults.watermark)
  wmH = zeros(num, 1);
else
  wmH = [];
end

imap = {};

% check all have the same units
if ~isempty(mia)
  units = getunits(mia(1));
  for n = 2:num
    if ~strcmp(getunits(mia(n)), units)
      mia(1)
      mia(n)
      error(sprintf('units differ (were ''%s'' and ''%s'') at object %d', ...
		    units, getunits(mia(n)), n));
    end
  end
  
  if isempty(defaults.multiplier)
    % [tmp defaults.multiplier] = datalabel2(mia(1));
    [tmp defaults.multiplier] = ...
	datalabel2(mia(1), 'parameterindex', ...
		   getparameterindex(mia(1), defaults.parameters));
  end
end

% X0 = 0:(((defaults.endtime - defaults.starttime)/defaults.resolution)-1);


set(gh, 'NextPlot', 'add');

yOffset = 0;


% ytick = [];
% yticklabel = {};
yticklabel = cell(1, num);

if isempty(defaults.color)
  defaults.color = get(gh, 'ColorOrder');
  defaults.color = defaults.color(1, :);
end

xlim = getcdfepochvalue([defaults.starttime defaults.endtime]);

for n = 1:num
  m = extract(mia(n), ...
	      'starttime', defaults.starttime, ...
	      'endtime', defaults.endtime);
  
  if ~isempty(defaults.parameters)
    paramIdx = getparameterindex(m, defaults.parameters);
    if length(paramIdx) > 1
      error('can only stackplot a single parameter');
    elseif paramIdx == 0
      error(sprintf('parameter not found in %s', ...
		    char2(getinstrument(mia(n)))));
    end
    
  else
    % just use whatever comes first
    paramIdx = 1;
  end

  data = double(getdata(m, paramIdx, ':'));

  % adjust data to suit units (T -> nT etc)
  data = data .* power(10, -defaults.multiplier);
  
  x = getcdfepochvalue(getsampletime(m));

  dmin = min(data);
  dmax = max(data);
  ddiff = dmax-dmin;
  if ddiff == 0
    ddiff = 1;
  end
  alimit = autolimit([0 ddiff]);
  clear ddiff; % use alimit(2) for scaling
  
  % the watermark should appear behind the line as it has a Z value of -1,
  % but matlab 6.5 seems to have a bug, and places the last object on top,
  % so do any watermark text first. Have to do our own watermark here
  % since each must overlay the data it corresponds to.
  if ~isempty(wmH)
    if ischar(defaults.watermark)
      watermark = defaults.watermark
    elseif iscell(defaults.watermark)
      watermark = defaults.watermark{n};
    end
    if iscell(defaults.watermarkcolor)
      wmc = defaults.watermarkcolor{n};
    else
      wmc = defaults.watermarkcolor;
    end

    wmH(n) = text('Parent', gh, ...
		  'Clipping', 'on', ...
		  'Units', 'data', ...
		  'Position', [mean(xlim) n-0.5 -1], ...
		  'FontUnits', 'points', ...
		  'FontSize', 30, ...
		  'Color', wmc, ...
		  'Interpreter', 'none', ...
		  'HorizontalAlignment', 'center', ...
		  'VerticalAlignment', 'middle', ...
		  'String', watermark, ...
		  'Tag', 'watermark');
  end
  
  y = ((data-dmin)/alimit(2)) + (n-1);
  lh(n) = line('Parent', gh, ...
	       'XData', x, ...
	       'YData', y, ...
	       'Color', defaults.color);
  % 'YData', data-dmin+yOffset);
  
  % label the line
  in = getinstrument(m);
  text('Parent', gh, ...
       'Position', [xlim(end) n-0.5 0], ...
       'Units', 'data', ...
       'Interpreter', 'none', ...
       'FontUnits', 'points', ...
       'FontSize', 9, ...
       'HorizontalAlignment', 'left', ...
       'VerticalAlignment', 'middle', ...
       'String', [' ' eval(defaults.label)]);
  
  
  yOffset = yOffset + alimit(2);
  % ytick(end+1) = yOffset;
  % yticklabel{end + 1} = sprintf('%g', alimit(2));
  [tmp1 tmp2 tmp3 tmp4 tmp5] = ...
      printunits(alimit(2) * power(10, defaults.multiplier), units, ...
		 'multiplier', defaults.multiplier);
  yticklabel{n} = tmp5{1};

end

				     
% timetick(gh, 'X', defaults.starttime, ...
% 	 defaults.endtime, defaults.resolution, 'UT');
timetick2(gh);

if num
  set(gh, ...
      'XLim', xlim, ...
      'YLim', [0, num], ...
      'YTick', 1:num, ...
      'YTickLabel', yticklabel, ...
      'YGrid', 'on', ...
      'Box', 'on');

  xlh = get(gh, 'XLabel');
  ylh = get(gh, 'YLabel');
  
  set(ylh, ...
      'String', datalabel(mia(1), 'parameterindex', ...
			  getparameterindex(mia(1), ...
					    defaults.parameters)), ...
      'FontSize', get(xlh, 'FontSize'), ...
      'FontWeight', 'bold');
end

set(fh, ...
    'Pointer', 'arrow', ...
    'Visible', defaults.visible);

if nargout >= 4
  % need to create imagemap data
  figUnits = get(fh, 'Units');
  set(fh, 'Units', 'pixels');
  figPos = get(fh, 'Position');
  
  axisUnits = get(gh, 'Units');
  set(gh, 'Units', 'pixels');
  axisPos = get(gh, 'Position');

  set(fh, 'Units', figUnits);
  set(gh, 'Units', axisUnits);

  % image map uses top left as origin, so measure downwards
  axisTopEdge = figPos(4) - sum(axisPos([2 4]));
  imap = cell(num, 1);
  for n = 1:num
    % in HTML imagemaps the coords start at 0, not 1
    imap{n} = [ceil(axisPos(1)) ...
	       ceil(axisTopEdge + (axisPos(4)*(num-n)/num)) ...
	       floor(sum(axisPos([1 3]))) ...
	       floor(axisTopEdge + (axisPos(4)*(1+num-n)/num))] - 1;
  end
end


mia_reordered = mia;
