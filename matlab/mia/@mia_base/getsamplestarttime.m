function r = getsamplestarttime(mia, varargin)
%GETSAMPLESTARTTIME  Return start times of data samples.
%
%   r = GETSAMPLESTARTTIME(mia);
%   r = GETSAMPLESTARTTIME(mia, n);
%   r: TIMESTAMP vector, where length(r) == number of samples
%   mia: MIA_BASE object (scalar)
%   n: NUMERIC value (scalar or vector)
%
% GETSAMPLESTARTTIME returns the start time of each sample, or optionally the
% time(s) of sample n (n may be a vector).
%
% See also GETSTARTTIME, GETENDTIME, GETSAMPLETIME, GETSAMPLEENDTIME.

if numel(mia) ~= 1
  error('mia must be scalar');
end

switch length(varargin)
 case 0
  r = mia.sampletime - mia.integrationtime/2;

 case 1
  if strcmp('end', varargin{1})
    r = mia.sampletime(end) - mia.integrationtime(end)/2;
  elseif isnumeric(varargin{1})
    r = mia.sampletime(varargin{1}) - mia.integrationtime(varargin{1})/2;
  else
    error('incorrect type for subscript');
  end
  
 otherwise
  error('incorrect parameters');
end
