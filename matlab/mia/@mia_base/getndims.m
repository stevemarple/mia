function r = getndims(mia)
%GETNDIMS  Return number of dimensions for data array
%
%   r = GETNDIMS(mia)
%   r: scalar
%   mia: MIA_BASE object
%
%   A 'virtual' function which should be overloaded for classes derived from
%   MIA_BASE. GETNDIMS should return the number of dimensions for the data
%   field of MIA_BASE (assuming it ISNUMERIC).
%
%   Do not confuse with the builtin NDIMS(mia), which returns the number of
%   dimensions for the matrix mia.
%
%   See also MIA_BASE.

% not needed for mia_base (it should be a virtual base class), and other
% classes should have own version
if strcmp(class(mia), 'mia_base')
  error('mia_base is a virtual base class. Derive your own class!');
else
  [tmp mf] = fileparts(mfilename);
  error(sprintf('Override %s for class %s'), mf, class(mia));
end
