function r = setcreatetime(mia, t)
%SETCREATETIME Set the creation time.
%
%    r = GETCREATETIME(mia)
%    r: modified MIA_BASE object
%    mia: original MIA_BASE object
%    t: new creation TIMESTAMP
%
%    See also TIMESTAMP, MIA_BASE, GETCREATETIME.

r = mia;
for n = 1:numel(r)
  r(n).createtime = t;
end

