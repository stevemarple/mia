function r = insertmany(varargin)
%INSERTMANY/NONANINSERTMANY  Insert many MIA_BASE objects
%
%   r = INSERT(...)
%   r = NONANINSERT(...)
%
% Insert MIA_BASE objects into the first similar MIA_BASE object in the
% list. If samples coincide then the samples from objects ro the right will
% overwrite the samples from objects on the left (the original input
% parameters are unaffected). When called as NONANINSERTMANY data then
% missing data samples will not overwrite valid samples.
%
% INSERTMANY/NONANINSERTMANY is a much more efficient method of inserting
% many objects together. For n objects INSERTMANY requires a maximum of n *
% ceil(log2(n)) copy operations where as sequential calls to INSERT would
% require triangularnumber(n) - 1 equivalent copy operations.
%
% In Unix systems NONANINSERTMANY is a symbolic link to INSERTMANY.
%
% See also INSERT, NONANINSERT.

want_nonaninsert = ~isempty(findstr(mfilename, 'nonaninsert'));
if want_nonaninsert
  func = 'nonaninsert';
else
  func = 'insert';
end

if nargin == 1
  r = varargin{1};
  
elseif nargin == 2
  r = feval(func, varargin{:});
  
else
  fprintf(sprintf('doing %s', func));
  while numel(varargin) > 1
    % remove any empty elements in varargin
    fprintf('.');
    varargin(cellfun('isempty', varargin)) = [];
    for n = 1:2:(numel(varargin)-1)
      varargin{n} = feval(func, varargin{n}, varargin{n+1});
      varargin{n + 1} = [];
    end
  end
  fprintf('\n');
  
  r = varargin{1};
end
