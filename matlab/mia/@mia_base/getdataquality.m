function r = getdataquality(mia)
%GETDATAQUALITY  Return any data quality warnings for MIA_BASE object.
%
%   w = GETDATAQUALITY(mia)
%   w: CELL array of data quality warnings
%   mia: MIA_BASE object
%
% See also SETDATAQUALITY, ADDDATAQUALITY, MIA_BASE.

if numel(mia) ~= 1
  error('mia must be scalar');
end

r = unique(mia.dataquality);


