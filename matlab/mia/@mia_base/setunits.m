function r = setunits(mia, units)
%SETUNITS  Set the units field of the MIA_BASE object.
%
%   r = SETUNITS(mia, units)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   units: new units (CHAR)
%
%   See also MIA_BASE, GETUNITS.

r = mia;
r.units = units;
