function r = setstarttime(mia, t)
%SETSTARTTIME  Set the starttime of a MIA_BASE object.
%
%   r = SETSTARTTIME(mia, starttime)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object whose start time is to be set
%   t: new start time (TIMESTAMP)
%
%   See also GETSTARTTIME, SETENDTIME.

r = mia;
r.starttime = t;



