function varargout = getresolution(mia)
%GETRESOLUTION  Return the resolution of a MIA_BASE object
%
%   [r mesg] = GETRESOLUTION(mia)
%   r: TIMESPAN object
%   mesg: CHAR error message
%   mia: MIA_BASE object
%
% GETRESOLUTION returns the resolution of the MIA_BASE object. If the
% resolution is not constant (ie if the interval between samples is not
% constant) then an invalid TIMESPAN object is returned and mesg set
% appropriately. If the too few samples are present to calculate the
% resolution (< 2) then mesg is set and a invalid resolution value
% returned.
%
% If only one return value is requested and the error message is not
% empty then ERROR is called with the error message.
%
%
% See also SETRESOLUTION, GUESSRESOLUTION.

r = timespan(nan, 's');
mesg = '';

switch length(mia)
 case 0
  r = repmat(timespan, size(mia));
 case 1
  dif = diff(mia.sampletime);
  if isempty(dif)
    mesg = 'too few samples to calculate resolution';
  elseif length(dif) == 1
    r = dif;
  elseif all(dif(1) == dif(2:end))
    r = dif(1);
  else
    mesg = 'samples not regularly spaced';
  end

  if nargout <= 1 & ~isempty(mesg)
    % error occurred, but user is not testing for errors, so generate one
    % here
    error(mesg);
  end
  
 otherwise
  % error('not a valid operation on non-scalar objects');
  varargout{1} = repmat(timespan, size(mia));
  varargout{2} = cell(size(mia));
  for n = 1:numel(mia)
    [varargout{1:nargout}] = getresolution(mia(n));
  end
  return
end

varargout{1} = r;
varargout{2} = mesg;
