function mialog(mia, varargin)
%MIALOG  Log data usage.
%
%   MIALOG(mia);
%   mia: MIA_BASE object(s)
%
% MIALOG logs details about data usage to a remote webserver, as defined
% by INFO(instrument, 'logurl').
%
% See also MIA_BASE, URL_FETCH, TRY, CATCH, EVAL.

ud = userdetails;

fstr = '%Y%m%dT%H%M%S.%#';
fstr_ts = '%dd %Hh %Mm %S.%#s';

% Not interested in output from the log web page.
if isunix
  file = '/dev/null';
else
  % /dev/null doens't exist and NUL is probably only valid in DOS shells. The
  % easiest and safest option is to use a temprary file and delete it later.
  file = tempname;
end

for n = 1:numel(mia)
  m = mia(n);
  if ~isempty(m.data)
    ins = getinstrument(m);
    logurl = info(ins, 'logurl');
    s = [];
    if ~isempty(logurl)
      s.classname = class(m);
      s.starttime = strftime(getstarttime(m), fstr);
      s.endtime = strftime(getendtime(m), fstr);
      s.createtime = strftime(getcreatetime(m), fstr);
      
      s.instrumenttype = class(ins);
      s.instrumentfacility = getfacility(ins);
      s.instrumentname = getname(ins);
      s.instrumentabbreviation = getabbreviation(ins);
      s.instrumentserialnumber = num2str(getserialnumber(ins));
      
      s.creatorrealname = ud.realname;
      s.creatoremail = ud.email;
      s.creatorinstitute = ud.institute;

      % set any additional parameters that were passed
      for vn = 1:2:numel(varargin)
        s = setfield(s, varargin{vn}, varargin{vn+1});
      end

      % URL-encode parameters
      fn = fieldnames(s);
      list = cell(1, numel(fn));
      for fnn = 1:numel(fn)
        field = fn{fnn};
        list{fnn} = sprintf('%s=%s', field, url_encode(getfield(s, field)));
      end
      % Ignore any errors from url_fetch
      url = sprintf('%s?%s', logurl, join('&', list));
      [status mesg] = url_fetch(url, file, ...
                                'usecache', 0, ...
                                'verbosity', 0);
      if status
        disp(sprintf('could not log usage details to %s', url));
      end
    end
  end
end

if ~isunix & url_exist(file)
  delete(file);
end
