function r = getdatasize(mia, varargin)
%GETDATA  Return the size of the data matrix.
%
%   r = GETDATASIZE(mia)
%   r = GETDATASIZE(mia, dim)
%
%   r: size matrix
%   mia: MIA_BASE object
%   dim: dimension  (NUMERIC) or 'end' to get the size of the last
%   dimension
%
% GETDATASIZE will accept 'end' as the dimension, returning the size of
% the last dimension (the time-variant part).
%
% See also SIZE, mia_base/GETDATA.

r = size(mia.data);
% for single images size matrix is 2D, not the 3D we would like. General
% fix for n dimensions
nd = getndims(mia);
if length(r) < nd
  % r((end+1):nd) = 1;
  % empty matrices should return 0 for 3rd dimension, singles images
  % return 1
  r((end+1):nd) = ~isempty(mia.data);
end

if nargin == 1
  ;
elseif nargin == 2
  if strcmp(varargin{1}, 'end')
    r = r(end);
  else
    r = r(varargin{1});
  end

else
  error('incorrect parameters');
end

