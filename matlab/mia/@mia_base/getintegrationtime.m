function r = getintegrationtime(mia, varargin)
%GETINTEGRATIONTIME  Return times of data integrations.
%
%   r = GETINTEGRATIONTIME(mia);
%   r = GETINTEGRATIONTIME(mia, n);
%   r: TIMESTAMP vector, where length(r) == number of samples
%   mia: MIA_BASE object (scalar)
%   n: NUMERIC value (scalar or vector)
%
% GETINTEGRATIONTIME returns the integration time for each sample, or
% optionally the integration time of sample n (n may be a vector).
%
% See also GETSTARTTIME, GETENDTIME.

if numel(mia) ~= 1
  error('mia must be scalar');
end

switch length(varargin)
 case 0
  r = mia.integrationtime;

 case 1
  if strcmp('end', varargin{1})
    r = mia.integrationtime(end);
  elseif isnumeric(varargin{1})
    r = mia.integrationtime(varargin{1});
  else
    error('incorrect type for subscript');
  end

  
 otherwise
  error('incorrect parameters');
end
