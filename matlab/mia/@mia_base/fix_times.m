function r = fix_times(mia)
%FIX_TIMES  Fix TIMESTAMP/TIMESPAN objects in a MIA_BASE object


r = mia;
if isstruct(r.starttime)
  r.starttime = timestamp(r.starttime);
end
if isstruct(r.endtime)
  r.endtime = timestamp(r.endtime);
end
if isstruct(r.createtime)
  r.createtime = timestamp(r.createtime);
end

if isstruct(r.integrationtime)
  r.integrationtime = timespan(r.integrationtime);
end
if isstruct(r.sampletime)
  r.sampletime = timestamp(r.sampletime);
end
