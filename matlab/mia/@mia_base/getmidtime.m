function r = getmidtime(mia)
%GETMIDTIME  Return the midtime of the MIA_BASE object
%
%   r = GETMIDTIME(mia)
%   r: TIMESTAMP object
%   mia: MIA_BASE object
%
%   See also GETSTARTIME, GETENDTIME, GETDURATION, TIMESTAMP.

t(2) = mia.endtime;
t(1) = mia.starttime;
r = mean(t);
