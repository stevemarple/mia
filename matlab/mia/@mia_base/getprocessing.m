function r = getprocessing(mia)
%GETPROCESSING  Return the processing stages of an MIA_BASE object.
%
%   r = GETPROCESSING(mia)
%   r: CHAR or CELL array
%   mia: MIA_BASE object

r = mia.processing;
% if isempty(r)
%   r = '';
% end
