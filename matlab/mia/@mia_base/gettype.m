function r = gettype(in, varargin)
%GETTYPE  Return instrument type
%
%   r = GETTYPE(in)
%
%   See also MIA_BASE.

r = '';
% no valid function found for the object passed here, if that class can't
% report what type of data it is can this one?
error(sprintf('Derive gettype function for class %s', class(in)));

