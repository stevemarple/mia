function r = getlimits(mia, varargin)
%GETLIMITS  Get the minimum and maximum limits of values in data field
%
%   r = GETLIMITS(mia)
%   r = GETLIMITS(mia, ...)
%
%   r = [1x2 double]
%   mia: MIA_BASE object
%
%   Optional indices into the data array can be given to limit the search.

if nargin == 1
  tmp = mia.data;
else
  s.type = '()';
  s.subs = varargin;
  tmp = subsref(mia.data, s);
end

r = [min(tmp(:)) max(tmp(:))];
