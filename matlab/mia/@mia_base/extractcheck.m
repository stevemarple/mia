function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  Check and process EXTRACT parameters
%
%   [sr, s] = EXTRACTCHECK(mia, ...)
%
%   sr: STRUCT for SUBSREF
%   s: STRUCT with important parameters from MIA_BASE.
%
% EXTRACTCHECK is called by EXTRACT to check and process its
% parameters. It should not be called directly.
%
% Developer notes
%
% If writing an EXTRACT function for a class you may also need to overload
% EXTRACTCHECK. Call the parent classes EXTRACTCHECK so that checking is
% performed on all relevant classes in the object. Do not add new fields to
% s(such as beams), it is not safe to assume that other calls to
% EXTRACTCHECK will not further restrict the set of data returned. Once
% EXTRACTCHECK has indicated which portions of the data are to be
% extracted then calculate whcih beams remain, etc.

st = getstarttime(mia);
et = getendtime(mia);

if matlabversioncmp('<=', '5.1')
  s.subsref = 'subsref2';
  s.subsasgn = 'subsasg2';
else
  s.subsref = 'subsref';
  s.subsasgn = 'subsasgn';
end

s.starttime = st;
s.endtime = et;
s.exclude = [];
s.excludeprocessing = {};

if length(varargin) == 1
  % check parameters are suitable
  if isstruct(varargin{1})
    if ~isfield(varargin{1}, 'type') | ~isfield(varargin{1}, 'subs')
      error('struct must have type and subs fields');
    end
    sr = varargin{1};
    
  elseif iscell(varargin{1})
    sr.type = '()';
    sr.subs = varargin{1};
    if numel(sr.subs) ~= getndims(mia)
      error('incorrect number of subscripts for extract');
    end
    
  else
    error('incorrect parameters for extract');
  end
  
else
  % parameter name/value interface
  defaults.starttime = [];
  defaults.endtime = [];
  defaults.exclude = [];
  
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  % check exclude formatted correctly
  % check exclude is formatted correctly
  if isempty(defaults.exclude)
    ; % ignore
  else
    if isa(mia.sampletime, 'timestamp') ...
	  & ~isa(defaults.exclude, 'timestamp')
      error('exclude must be a timestamp array');
    elseif isa(mia.sampletime, 'timespan') ...
	  & ~isa(defaults.exclude, 'timespan')
      error('exclude must be a timespan array');
    else
      ; % probably wrong class for timestamp but don't generate an error
    end
    
    if ndims(size(defaults.exclude)) > 2 ...
	  | size(defaults.exclude, 2) ~= 2
      error('exclude must be an n by 2 array');
    end
  end
  
  sr.type = '()';
  sr.subs = repmat({':'}, [1 getndims(mia)]);
  
  if ~isempty(defaults.starttime) | ~isempty(defaults.endtime)
    if isempty(defaults.starttime)
      defaults.starttime = st;
      %elseif defaults.starttime < st
      % defaults.starttime = st;
    end
    
    if isempty(defaults.endtime)
      defaults.endtime = et;
      %elseif defaults.endtime > et
      %defaults.endtime = et;
    end
    
    samt = getsampletime(mia);
    if 0
      % extract taking into consideration the integration time
      it_2 = getintegrationtime(mia) ./ 2;
      
      sr.subs{end} = find((samt - it_2 >= defaults.starttime) & ...
			  (samt + it_2 <= defaults.endtime));
    else
      sr.subs{end} = find((samt >= defaults.starttime) & ...
			  (samt < defaults.endtime));
      
    end
    
    s.starttime = defaults.starttime;
    s.endtime = defaults.endtime;
  end
  
  if ~isempty(defaults.exclude)
    if strcmp(sr.subs{end}, ':')
      sr.subs{end} = 1:getdatasize(mia, 'end');
    end
    
    % each row in exclude defines start/end times of unwanted data
    for n = 1:size(defaults.exclude, 1)
      idx = find(mia.sampletime(sr.subs{end}) >= defaults.exclude(n,1) & ...
                 mia.sampletime(sr.subs{end}) < defaults.exclude(n,2));
      if ~isempty(idx)
        sr.subs{end}(idx) = [];
        s.excludeprocessing{end+1} ...
            = sprintf('Excluded data from %s', ...
                      dateprintf(defaults.exclude(n,1), ...
                                 defaults.exclude(n,2)));
      end
    end
    s.exclude = defaults.exclude;
  end
end
