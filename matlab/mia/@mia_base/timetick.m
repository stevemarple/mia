function formatstr = timetick(mia, axishandle, ax, varargin)
%TIMETICK  Add tick marks to an axis, labelled with time.
% 
%   fstr = TIMETICK(mia, ah, ax)
%   fstr = TIMETICK(mia, ah, ax, alflag)
%   fstr: timestamp/STRFTIME format string
%   mia: MIA_BASE object
%   ah: handle to the axis object
%   ax: axis indicator, 'X', 'Y' or 'Z'
%
%   mia_base/TIMETICK is a convenient interface to TIMETICK.
%
%   See also TIMETICK.

formatstr = timetick(axishandle, ax, mia.starttime, mia.endtime, ...
    getresolution(mia), varargin{:});

