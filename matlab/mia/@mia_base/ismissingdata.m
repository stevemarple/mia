function r = ismissingdata(mia, s)
%ISMISSINGDATA  Indicate if a MIA_BASE object's data field is empty.
%
%   r = ISMISSINGDATA(mia, s)
%   r: LOGICAL
%   mia: MIA_BASE object, or one derived from MIA_BASE
%   s: CHAR ('all' or 'any')
%
%   Indicate if all or any data is missing. When mia is non-scalar r will
%   a matrix of the same size.
%
%   See also MIA_BASE, ISEMPTY.

if ~any(strcmp(s, {'all' 'any'}))
  error('incorrect parameters');
end
r = logical(zeros(size(mia)));
for n = 1:numel(mia)
  data = getdata(mia(n));
  r(n) = feval(s, isnan(data(:)));
end

