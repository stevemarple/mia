function r = setdataquality(mia, str)
%SETDATAQUALITY  Set data quality warnings for MIA_BASE object.
%
%   r = SETDATAQUALITY(mia, str)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   str: CHAR or CELL array of data quality warnings
%
% To avoid a proliferation of different warnings the descriptions should
% be chosen from the list below (if possible).
%
%    Preliminary
%    Timing uncertain
%    Uncalibrated
%    Bad data
%
% See also GETDATAQUALITY, ADDDATAQUALITY, MIA_BASE.

if numel(mia) ~= 1
  error('mia must be scalar');
end

r = mia;

if ischar(str)
  str = {str};
elseif ~iscell(str)
  error('incorrect parameters');
end

r.dataquality = unique(str);


