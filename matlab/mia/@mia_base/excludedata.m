function [r, s] = excludedata(mia, t)
%EXCLUDEDATA Exclude certain intervals from a data set.
%
%   [r, s] = EXCLUDEDATA(mia, t)
%   r: MIA_BASE
%   s: CELL of CHARs describing the actual times excluded
%   mia: MIA_BASE
%   t: [n x 2 TIMESTAMP]
%
% Exclude certain intervals from a data set by setting any samples within
% those intervals to NAN. 
%
% See also MIA_BASE, GETNDIMS.

if length(mia) ~= 1
  error('object must be scalar');
end

r = mia;
s = {};

% implement for n-dimensional data
subs = repmat({':'}, 1, getndims(mia));

% get integration time, halved. When not valid assume it is zero
integ_2 = r.integrationtime ./ 2;
badInteg = find(~isvalid(integ_2));
if ~isempty(badInteg)
  integ_2(badInteg) = timespan(0, 's');
end

for n = 1:size(t, 1)
  if t(n, 1) > t(n, 2)
    error(sprintf('exclude times incorrectly ordered (row %d: %s, %s)', ...
		  n, char(t(n, 1)), char(t(n, 2))));
  end

  subs{end} = (r.sampletime-integ_2 >= t(n,1) & ...
	       r.sampletime+integ_2 < t(n, 2));

  r = setdata(r, nan, subs{:});
end
  
  
