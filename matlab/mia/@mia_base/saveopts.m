function r = saveopts(mia)
%SAVEOPTS  Options used when saving MIA data.
%
%   r = SAVEOPTS(mia)
%   r: CELL array of options (can be empty)
%   mia: a MIA_BASE (or derived) object
%
% SAVEOPTS lists the options to be used when saving matlab data. Note
% that the return argument will probably depend on the version of Matlab
% currently in use.
%
% See also MATLABVERSION.

s = dbstack;

if matlabversioncmp('<', '7')
  r = {};
elseif matlabversioncmp('<', '8')
  r = {'-V6'};
elseif matlabversioncmp('<', '9')
  r = {'-V6'};
elseif matlabversioncmp('<', '10')
  r = {'-V6'};
else
  warning(sprintf('unknown Matlab version, please fix or customise %d', ...
		  s(1).name));
  r = {'-V6'};
end

