function r = mia2matlab(mia)

num = numel(mia);
if num == 0
  r = [];
  return;
elseif num > 1
  for n = 1:num
    r(n) = feval(mfilename, mia(n));
  end
  return;
end
  
% mia is singular from this point

r.data = getdata(mia);
r.dataunits = getunits(mia);
r.starttime = datevec(getstarttime(mia));
r.starttime_cdf = getcdfepochvalue(getstarttime(mia));
r.endtime = datevec(getendtime(mia));
r.endtime_cdf = getcdfepochvalue(getendtime(mia));

[res resMesg] = getresolution(mia);
if isempty(resMesg)
  r.resolution = gettotalseconds(res);
end
r.createtime = datevec(getcreatetime(mia));
r.createtime_cdf = getcdfepochvalue(getcreatetime(mia));

instrument = getinstrument(mia);
loc = getlocation(instrument);
r.instrument.type = class(instrument);
[r.instrument.location.name{1} r.instrument.location.name{2}] = ...
    getname(loc);

% ensure any i18n_char object is converted to char
r.instrument.location.name{1} = char(r.instrument.location.name{1});
r.instrument.location.name{2} = char(r.instrument.location.name{2});
 
r.instrument.location.latitude = getgeolat(loc);
r.instrument.location.longitude = getgeolong(loc);

r.processing = getprocessing(mia);

if isa(mia, 'rio_base')
  r.beams = getbeams(mia);
  r.instrument.frequency = getfrequency(instrument);
  
elseif isa(mia, 'mag_data')
  r.components = getcomponents(mia);

end
