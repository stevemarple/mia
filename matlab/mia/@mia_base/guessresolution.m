function varargout = guessresolution(mia)
%GUESSRESOLUTION  Return the probable resolution of a MIA_BASE object
%
%   r = GUESSRESOLUTION(mia)
%   r: TIMESPAN object
%   mia: MIA_BASE object
%
% See also GETRESOLUTION.

r = timespan(nan, 's');
mesg = '';

switch length(mia)
 case 0
  r = repmat(timespan, size(mia));
 case 1
  dif = diff(mia.sampletime);
  if isempty(dif)
    mesg = 'too few samples to calculate resolution';
  elseif length(dif) == 1
    r = dif;
  else
    r = median(dif);
  end

  if nargout <= 1 & ~isempty(mesg)
    % error occurred, but user is not testing for errors, so generate one
    % here
    error(mesg);
  end
  
 otherwise
  % error('not a valid operation on non-scalar objects');
  varargout{1} = repmat(timespan, size(mia));
  varargout{2} = cell(size(mia));
  for n = 1:numel(mia)
    [varargout{1:nargout}] = guessresolution(mia(n));
  end
  return
end

varargout{1} = r;
varargout{2} = mesg;
