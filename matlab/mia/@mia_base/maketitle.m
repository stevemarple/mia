function [plottitle, windowtitle] = maketitle(mia, varargin)
%MAKETITLE  Make plot and figure titles.
%
%   [plottitle, windowtitle] = maketitle(mia)
%   [plottitle, windowtitle] = maketitle(mia, ...)
%
%   Create the titles based on current time/date, data type etc. Various
%   values may be overriden by specifying a parameter name and value (see
%   INTERCEPTPROP). The defaults values are created from the IRISFILEBASE
%   object (or an object derived from IRISFILEBASE). The valid parameters
%   which may be overriden are described below:
%
%     'style', style
%        For MIA_IMAGE_BASE objects the default title style is 'image'. This
%        adds the word 'image' or 'images' to the result of
%        GETNAME(mia). Otherwise the default title style is 'line'.
%
%     'customstring', string
%        Observe chosen style but override the string printed in
%        bold. Useful for Keogram plots and similar.
%
%     'starttime', TIMESTAMP
%
%     'endtime', TIMESTAMP
%
%     'resolution', TIMESPAN
%     'resolution', CHAR
%        If the resolution is given as a TIMESPAN then it is converted to
%        a suitable string. If it is a CHAR array then it is printed
%        without change.   
%
%     'location', LOCATION
%
%     'step', step
%        For image plots using only 1 out of n images, step should be set to
%        n.
%
%     'flipaxes', str
%        The orientation of the images. The string should be one of those
%        returned by MAKEPLOTFIG('flipaxesnames');
%
%     'comment', comment
%        An optional comment, to be printed at the bottom of the title
%
%   See also MIA_IMAGE_BASE, MAKEPLOTFIG, GETNAME, PRINTSERIES.

plottitle = '';
windowtitle = '';

% general format is
% Power/time (beam 1)
% 00:00:00 - 12:00:00 UT 1/1/1999 @ 1 m res.
% Kilpisjarvi, Finland (69.05N, 20.79E)
% [comment]
%
% If step ~= 1 then print that and resolution on separate line to time
% formatStr = '{\\bf %s} %s\n%s%s\n%s%s';
formatStr = { ...
      '{\\bf %s} %s' ...
      '%s%s' ...
      '%s%s'
  };

% entries are:
styleStr = ''; % 'Power/time images', 'Absorption/time', etc
locStr = '';
dateStr = '';
resStr = '' ; % including step size if appropriate

instrument = getinstrument(mia);
loc = getlocation(instrument(1));

% default values
defaults.starttime = getstarttime(mia(1));
defaults.endtime = getendtime(mia(1));
[res resmesg] = getresolution(mia(1));
if isvalid(res)
  defaults.resolution = res;
else
  defaults.resolution = '';
end

defaults.location = loc;
defaults.step = 1;
defaults.style = 'line';
defaults.customstring = '';
defaults.comment = '';
defaults.flipaxes = '';

if nargin > 1
  [defaults unvi] = interceptprop(varargin, defaults);
end

% create normal version of strings
locStr = char(defaults.location);
dateStr = dateprintf(defaults.starttime, defaults.endtime);
if ischar(defaults.resolution)
  resStr = defaults.resolution;
else
  resStr = [' @ ' char(defaults.resolution, 'c') ' res.'];
end

otherStr = '';

% modify standard formmating if not appropriate
if strcmp(defaults.style, 'line')
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    styleStr = gettype(mia, 'c');
  end
  windowtitle = styleStr;
  
elseif strcmp(defaults.style, 'image')
  % handle singular / plural properly
  samt = getsampletime(mia);
  %if (defaults.endtime - defaults.starttime) / ...
  %	(defaults.resolution * defaults.step) > 1
  if 0
  % multiple images plotted
    plural = 's';
  else
    % single image plotted
    plural = '';
  end
   if ~isempty(defaults.customstring)
     styleStr = defaults.customstring;
   else
     styleStr = [getname(mia, 'c') ' image' plural];
   end
  
   otherStr = defaults.flipaxes;
  
  if defaults.step > 1
    % print step information too
    resStr = sprintf('\nresolution %s, plot interval %s', ...
	char(defaults.resolution), ...
	char(defaults.resolution * defaults.step, 'c')); 
  end
  windowtitle = styleStr;
else
  warning('unknown style');
  plottitle = '';
  windowtitle = '';
  return;
end

if ~isempty(defaults.comment)
  % put comment on separate line
  defaults.comment = sprintf('\n%s', defaults.comment);
end

% if ~isempty(defaults.customstring)
%   styleStr = defaults.customstring;
% end

if ~isempty(otherStr)
  otherStr = ['(' otherStr ')'];
end
plottitle = { ...
      sprintf(formatStr{1}, styleStr, otherStr) ...
      sprintf(formatStr{2}, dateStr, resStr)};

if length(mia) == 1
  plottitle{end+1} = sprintf(formatStr{3}, locStr, defaults.comment);
end
