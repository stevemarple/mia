function r = setsampletime(mia, t)
%SETSAMPLETIME  Set the sampletime of a MIA_BASE object.
%
%   r = SETSAMPLETIME(mia, sampletime)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object whose sampletime is to be set
%   t: new sampletime (TIMESTAMP)
%
%   See also GETSAMPLETIME.

r = mia;
r.sampletime = t;



