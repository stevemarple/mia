function r = loadobj(a)
%LOADOBJ  Load filter for MIA_BASE object.
%
if isstruct(a)
  r = mia_base(a);
elseif a.versionnumber ~= 5
  r = mia_base(struct(a));
else
  r = a;
end

if ~isempty(r.dataquality)
  for n = 1:numel(r.dataquality)
    disp(['Data quality warning: ' r.dataquality{n}]);
  end
end
