function mia = mia_base(varargin)
%MIA_BASE  Constructor for MIA_BASE class.
% 
%   MIA_BASE is the base class from which all MIA data classes should be
%   derived. 
%
%   r = MIA_BASE;
%   default constructor
%
%   r = MIA_BASE(mia)
%   copy constructor
%
%   r = MIA_BASE(...)
%   constructor taking parameter name/value pairs
%
% The following parameters are accepted:
%
%   'starttime', [1x1 TIMESTAMP]
%   Start time of the MIA_BASE object.
%
%   'endtime',  [1x1 TIMESTAMP] 
%   End time of the MIA_BASE object.
%
%   'sampletime', [TIMESTAMP]
%   Sample times, one per sample. The time should correspond to the
%   middle of the integration period (or weighted average in the case of
%   post-integrated data).
%
%   'instrument', [1x1 MIA_INSTRUMENT_BASE]
%   The instrument from which data was recorded. See MIA_INSTRUMENT_BASE.
%
%   See also TIMESTAMP, TIMESPAN, MIA_INSTRUMENT_BASE, MIA_IMAGE_BASE.

cls = 'mia_base';
latestversion = 6;

ud = userdetails;
mia.versionnumber = latestversion;
mia.starttime = timestamp({});
mia.endtime = timestamp({});
mia.createtime = timestamp('now');
% mia.resolution = []; removed in version 6

mia.integrationtime = timespan([], 's'); % ?????
mia.instrument = [];
mia.units = '';
mia.creator = ud.realname;
mia.processing = {};
mia.filename = '';
mia.data = []; % version >=2 
% need to indicate if using offset means for lowered resolution data or
% centred means
% mia.timing = ''; % version 3,4,5

% version >= 5

% indication if data quality is not good. The data quality flag is intended
% to work in a similar way to data "tainting" in perl, where once tainted
% data touches any other data that too becomes tainted. dataquality
% is a CELL array of unique CHARs indicating problems with the
% data. Possible problems might be "Preliminary", "Timing errors" etc.
mia.dataquality = {}; 

% version >= 6
% timestamp all data samples. length(sampletime) should be equal to
% getdatasize(mia, getndims(mia)).
mia.sampletime = timestamp({});

if nargin == 0 | (nargin == 1 && isempty(varargin{1}))
  % default constructor (need a default constructor for loading 
  % from file using builtin load
  % disp('# mia default constructor');
  mia = class(mia, cls);

elseif nargin == 1 && strcmp(class(varargin{1}), cls)
  mia = varargin{1};

elseif nargin == 1 && isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = intersect(fieldnames(a), fieldnames(mia));
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls);
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    an = a(n);
    for m = 1:length(fn)
      % tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end
    switch tmp.versionnumber
     case 1
      % possibly no data field
      if ~isfield(tmp, 'data')
	tmp.data = [];
      end
      tmp.timing = '';
      tmp.dataquality = {};
      tmp.resolution = an.resolution;
      tmp = localMakeSampleTime(tmp);
     case 2
      tmp.timing = '';
      tmp.dataquality = {};
      tmp.resolution = an.resolution;
      tmp = localMakeSampleTime(tmp);
     case 3
      tmp.dataquality = {};
      tmp.resolution = an.resolution;
      tmp.timing = an.timing;
      tmp = localMakeSampleTime(tmp);
     case 4
      tmp.dataquality = unique(a.dataquality.warning);
      tmp.resolution = an.resolution;
      tmp.timing = an.timing;
      tmp = localMakeSampleTime(tmp);
     case 5
      tmp.resolution = an.resolution;
      tmp.timing = an.timing;
      tmp = localMakeSampleTime(tmp);
     case 6
      ; % latest
      
     otherwise
      error('unknown version');
    end
    mia(n) = class(tmp, cls);
  end
  
elseif rem(nargin, 2) == 0 && all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [mia unvi] = interceptprop(varargin, mia);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  % horrid hack to enable resolution to be passed on as a loadoption
  resolution_workaround(varargin, unvi);

  mia = class(mia, cls);

else  
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;

% if isempty(mia.integrationtime) && ~isempty(mia.resolution)
%   mia.integrationtime = mia.resolution;
% end

if ischar(mia.processing)
  mia.processing = {mia.processing};
end

if ischar(mia.dataquality)
  mia.dataquality = {mia.dataquality};
elseif ~iscell(mia.dataquality)
  error(sprintf('bad class for data quality (was ''%s'')', ...
		class(mia.dataquality)));
end


% given a struct (or mia_base class) set the sampletime field
% apropriately. This function is intended only for converting old
% versions of this class to the current one.
function r = localMakeSampleTime(s)
disp('converting from non-timestamped data');

r = s;
if s.versionnumber >= 6
  return % sampletime should already be set appropriately
end

if isempty(s.resolution) | isempty(s.starttime) | isempty(s.endtime)
  error('require start and end times, and resolution');
end

if numel(s.resolution) > 1
  % mia_image_base used to use non-scalar values for resolution
  r.sampletime = s.starttime + s.resolution;
else
  if any(strcmp(s.timing, {'centred' 'centered'}))
    offset = timespan(0, 's');
  elseif ~isempty(s.integrationtime)
    offset = s.integrationtime ./ 2;
  else
    offset = s.resolution ./ 2;
  end
  
  % Method below doesn't work for rio_qdc and rio_qdc_mean_sd type object
  % r.sampletime = (s.starttime + offset):s.resolution:...
  %  (s.endtime - s.resolution + offset);

  if length(s.resolution) == 1
    r.sampletime = s.starttime + offset + ...
	s.resolution .* (0:(size(s.data, ndims(s.data))-1));
  else
    r.sampletime = s.starttime + offset + ...
	[timespan(0, 's') s.resolution(1:(end-1))];
  end
end

if isempty(s.integrationtime) && ~isempty(s.resolution)
  r.integrationtime = s.resolution;
end


if length(s.integrationtime) == 1
  r.integrationtime = repmat(r.integrationtime, size(r.sampletime));
end

r = rmfield(r, 'resolution');
r = rmfield(r, 'timing');

