function ascii(mia, filename, varargin)
%ASCII Save MIA_BASE data in ASCII format.
%
%   ASCII(mia, filename, ...)
%   mia: any MIA_BASE object (may be a non-scalar)
%   filename: the filename in which to save the data
%
%

defaultformats.int8   = '% 3d';
defaultformats.uint8  = '% 4d';
defaultformats.int16  = '% 6d';
defaultformats.uint16 = '% 7d';
defaultformats.int32  = '% 11d';
defaultformats.uint32 = '% 12d';
defaultformats.other = '% 15f'; % anything else, same as double

% asciidatefstr = '%Y %m %d %H %M %S'; % for strftime
asciidatefstr = '%04d %02d %02d %02d %02d %06.3f'; % for sprintf/datevec

if strcmp(filename, '-')
  fid = 1; % stdout
else
  [fid message] = fopen(filename, 'w');
  if fid == -1
    error([message '(filename was ' filename ')']);
  end
end

for n = 1:prod(size(mia))
  mia2 = mia(n);
  data = getdata(mia2);

  % default values
  % it is conceivable that the data type could be different in each
  % element of mia, so have to recompute the default format on each
  % itereation of the loop
%   switch class(data)
%    case 'int8'
%     defaults.format = '% 3d';
%    case 'uint8'
%     defaults.format = '% 4d';
%    case 'int16'
%     defaults.format = '% 6d';
%    case 'uint16'
%     defaults.format = '% 7d';
%    case 'int32'
%     defaults.format = '% 11d';
%    case 'uint32'
%     defaults.format = '% 12d';

%    otherwise
%     % assume same as double
%     defaults.format = '% 15f';
%   end

  defaults.format = [];
  
  defaults.timestampformat = '%Y-%m-%d %H:%M:%S';
  [defaults unvi] = interceptprop(varargin, defaults);

  if isempty(defaults.format)
    % lookup which format should be used
    if isfield(defaultformats, class(data))
      defaults.format = getfield(defaultformats, class(data));
    else
      defaults.format = defaultformats.other;
    end
  end
  
  % blank line before data
  ins = getinstrument(mia2);
  loc = getlocation(ins);
  [name country] = getname(loc);
  if isa(name, 'i18n_char');
    name = ascii(name);
  end
  if isa(country, 'i18n_char');
    country = ascii(country);
  end
  loc = setname(loc, name, country);
  mia2_ascii = setinstrument(mia2, setlocation(ins, loc));
  fprintf(fid, '%s\n', char(mia2_ascii, ...
			    'timestampformat', defaults.timestampformat)); 
  
  fstr = [repmat(defaults.format, 1, size(mia2.data,1)) '\n'];

  switch getndims(mia2)
   case 2
    % 2D, write so that time variation (cols) is written as rows
    % fprintf(fid, fstr, mia2.data);
    fstr = ['%s' fstr];
    if isa(getsampletime(mia2), 'timestamp')
      for n = 1:getdatasize(mia2, 2)
	% tstr = strftime(getsampletime(mia2, n), asciidatefstr);
	tstr = sprintf(asciidatefstr, datevec(getsampletime(mia2, n)));
	fprintf(fid, fstr, tstr, mia2.data(:,n));
      end
    elseif isa(getsampletime(mia2), 'timespan')
      for n = 1:getdatasize(mia2, 2)
	tstr = sprintf('%.3f', gettotalseconds(getsampletime(mia2, n)));
	fprintf(fid, fstr, tstr, mia2.data(:,n));
      end      
    else
      error('Unknown sampletime class');
    end
    
   case 3
    fstr = ['%s\n' fstr];
    for n = 1:size(mia2.data, 3)
      % tstr = strftime(getsampletime(mia2, n), asciidatefstr);
      tstr = sprintf(asciidatefstr, datevec(getsampletime(mia2, n)));
      fprintf(fid, fstr, tstr, mia2.data(:,:,n));
      fprintf(fid, '\n');
    end
    
   otherwise
    % don't know what to do
    local_fclose(fid);
    error('Bad value from getndims');
  end

end

if isequal(local_fclose(fid), 0)
  disp(sprintf('Exported as ASCII format to %s', filename));
end


function r = local_fclose(fid)
r = [];
if fid > 2
  r = fclose(fid);
end
