function [r, idx2] = build(mia, a, idx)

if ~strcmp(class(mia), 'mia_base')

  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a BUILD command to use
  % CONSTRUCTFROMFILES. Generate a runtime error with appropriate error
  % message to indicate what needs to be done. Derived classes MUST call
  % this function on their parent field.
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

len = length(a.resolution);
ind = idx:(idx+len-1);
idx2 = idx + len;

r = mia;
r.resolution(ind) = a.resolution;
r.integrationtime(ind) = a.integrationtime;

if idx == 1 & isempty(getunits(mia))
  r = setunits(r, getunits(a));
end

