function r = isdataempty(mia)
%ISDATAEMPTY  Indicate if a MIA_BASE object's data field is empty.
%
%   r = ISDATAEMPTY(mia)
%   r: boolean
%   mia: MIA_BASE object, or one derived from MIA_BASE
%
%   See also MIA_BASE, ISEMPTY.

r = logical(zeros(size(mia)));
for n = 1:prod(size(mia))
  r(n) = isempty(mia(n).data);
end
