function r = calcdefaultsampletime(mia, res)
%CALCDEFAULTSAMPLETIME Calculate default sample times for MIA_BASE object.
%
%   r = CALCDEFAULTSAMPLETIME(mia, res)
%   r: TIMESTAMP objects
%   mia: MIA_BASE object.
%   res: TIMESPAN resolution
%
% CALCDEFAULTSAMPLETIME calculates the default sample times to use when
% changing the resolution of the data.
%
% See also SETRESOLUTION.

r = (mia.starttime+(res./2)):res:mia.endtime;

