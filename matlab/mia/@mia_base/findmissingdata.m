function [r, rt] = findmissingdata(mia, varargin)
%FINDMISSINGDATA  Find periods of missing data.
%
%   [r rt] = FINDMISSINGDATA(mia, ...)
%   r: list of time subscripts for which all samples are NAN.
%   rt: sample times corresponding to time when all samples are NAN.
%   mia: MIA_BASE object.
%
% FINDMISSINGDATA reports times when all samples at a particular sample
% time are NAN. The default behaviour can be modified with the following
% name/value pairs:
%
% 'method', 'all' | 'redundant'
% Indicate the method to use when finding NANs. 'all' finds time when all
% samples are NANs. 'redundant only returns times when all samples are NAN
% and for the previous time all samples were NANs. 
%
% See also MARKMISSINGDATA, REMOVEMISSINGDATA.

r = [];
rt = repmat(timespan, [1 0]);

defaults.method = 'all';
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

data = getdata(mia);
% if data class cannot hold NaNs this function will never find them.
if ~any(strcmp(class(data), {'single' 'double'}))
  error(sprintf('data matrix cannot store NaNS: class is %s', ...
		class(data)));
end
dsz = getdatasize(mia);

if numel(dsz) > 2
  % multi-dimensional data, reshape to 2D matrix
  dsz = [prod(dsz(1:(end-1))) dsz(end)];
  data = reshape(data, dsz);
end


% find where for a given time all samples are nan
missingIdx = find(all(isnan(data), 1));

switch defaults.method
 case 'all'
  r = missingIdx;
  
 case 'redundant'
  % redundant missing data is those samples which are NaN and the
  % previous sample was also NaN
  
  % positions in missingIdx, but need to add one to get the subsequent
  % nan, not the first
  pos = find(diff(missingIdx) == 1); 
  if ~isempty(pos)
    r = missingIdx(pos+1);
  end
  
 otherwise
  error(sprintf('unknown method (was ''%s'')', defaults.method));
end

if nargout > 1
  rt = mia.sampletime(r);
end
