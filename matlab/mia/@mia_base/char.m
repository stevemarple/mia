function r = char(mia, varargin)
%CHAR Convert MIA_BASE object into a CHAR representation.
%
%   s = CHAR(mia)
%   s = CHAR(mia, ...)
%   mia: MIA object
%   s: CHAR representation.
%
%   The following parameter name/value pairs are recognised:
%
%     'timestampformat', CHAR
%     Alternative format for the start/end times
%
%   See also strfun/CHAR.

% NB Make all objects derived from mia_base print a trailing newline
% character 


defaults.timestampformat = '%H:%M:%S %d %B %Y (day %j)';

[defaults unvi] = interceptprop(varargin, defaults);

if length(mia) ~= 1
  r = matrixinfo(mia);
  return;
end


% neatly print all the elements in mia.processing into a temporary string
proc_sz = size(mia.processing);
if proc_sz
  for i = 1:(proc_sz(1)*proc_sz(2))
    if i == 1
      proc_str = sprintf('%s', mia.processing{i});
    else
      proc_str = sprintf('%s\n                    %s', proc_str, ...
	  mia.processing{i});
    end
  end
else
  proc_str = '<none>';
end

if isa(mia.starttime, 'timestamp')
  if numel(mia.starttime) == 1
    start_str = strftime(mia.starttime, defaults.timestampformat);
  else
    start_str = matrixinfo(mia.starttime);
  end
else
  start_str = char(mia.starttime);
end

if isa(mia.endtime, 'timestamp')
  if numel(mia.endtime) == 1
    end_str = strftime(mia.endtime, defaults.timestampformat);
  else
    end_str = matrixinfo(mia.endtime);
  end
else
  end_str = char(mia.endtime);
end

if isa(mia.starttime, 'timestamp') & isa(mia.endtime, 'timestamp')
  dur_str = char(mia.endtime - mia.starttime, 'c');  % want compact form
else
  dur_str = '';
end

[resolution mesg] = getresolution(mia);
if ~isempty(mesg)
  res_str = ['<' mesg '>'];
elseif isa(resolution, 'timespan')
  if length(resolution) > 1 && ...
	all(resolution == resolution(1))
    % all items the same, show as scalar
    res_str = char(resolution(1), 'c');
  else
    res_str = char(resolution, 'c'); % want compact form
  end
else
  % might be empty or something else - use a standard form of char
  res_str = char(resolution);
end

if isa(mia.integrationtime, 'timespan')
  if length(mia.integrationtime) > 1 && ...
	all(mia.integrationtime == mia.integrationtime(1))
    % all items the same, show as scalar
    integ_str = [matrixinfo(mia.integrationtime) ...
		 ' (' char(mia.integrationtime(1), 'c') ')'];
  else
    integ_str = char(mia.integrationtime, 'c'); % want compact form
  end
else
  
  % might be empty or something else - use a standard form of char
  integ_str = char(mia.integrationtime);
end

if isempty(mia.units)
  units_str = '';
elseif iscell(mia.units)
  if prod(size(mia.units)) > 4
    units_str = matrixinfo(mia.units);
  else
    units_str = sprintf('%s', mia.units{1});
    units_str = [units_str sprintf(', %s', mia.units{2:end})];
  end
elseif ischar(mia.units)
  units_str = mia.units;
else
  units_str = '<unknown units format>';
end

if ischar(mia.filename)
  filename_str = mia.filename;
else
  filename_str = matrixinfo(mia.filename);
end

if isempty(mia.instrument)
  instrumentStr = sprintf('instrument        : <none>\n');
else
  instrumentStr = char(mia.instrument);
end

dataquality = getdataquality(mia);
if isempty(dataquality)
  dataqualityStr = '<no data quality issues reported>';
else
  dataqualityStr = ['''' dataquality{1} ''''];
  for n = 2:numel(dataquality)
    dataqualityStr = sprintf('%s, ''%s''', dataqualityStr, dataquality{n});
  end
end

% use compact form for printing resolution
r = sprintf(['class             : %s\n' ...
	     'start time        : %s\n' ...
	     'end time          : %s\n' ...
	     'duration          : %s\n' ...
	     'resolution        : %s\n' ...
	     'sample time       : %s\n' ...
	     'integration time  : %s\n' ...
	     '%s' ...
	     'units             : %s\n' ...
	     'creator           : %s\n' ...
	     'creation time     : %s\n' ...
	     'processing        : %s\n' ...
	     'filename          : %s\n' ...
	     'data              : %s\n' ...
	     'data quality      : %s\n'], ...
	    class(mia), ...
	    start_str, end_str, dur_str, res_str, ...
	    matrixinfo(mia.sampletime), integ_str, ...
	    instrumentStr, ...
	    units_str, char(mia.creator), ...
	    char(round(mia.createtime, timespan(1, 's'))), ...
	    proc_str, filename_str, ...
	    matrixinfo(mia.data), dataqualityStr);  


