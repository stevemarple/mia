function r = preallocate(mia, len)
%PREALLOCATE  Preallocate object fields to allow BUILD
%
%   r = PREALLOCATE(mia)
%   r: MIA_BASE object with preallocated fields
%   mia: MIA_BASE object (derived classes not allowed) 
%   len: length to preallocate to fields
%
%   See also BUILD

if ~strcmp(class(mia), 'mia_base')
  
  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a PREALLOCATE command to use
  % CONSTRUCTFROMFILES or BUILD. Generate a runtime error with appropriate
  % error message to indicate what needs to be done. Derived classes MUST
  % call this function on their parent field.
  
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia; % copy basic things like units, start and end times etc

ts = timespan(repmat(nan, 1, len), 's');
% ts = timespan([1 len]); % initialised to nan
r.resolution = ts;
r.integrationtime = ts;



