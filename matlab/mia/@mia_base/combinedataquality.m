function r = combinedataquality(mia1, mia2)
%COMBINEDATAQUALITY Return combined data quality field for two MIA_BASE objects
%
%   r = JOINDATAQUALITY(mia1, mia2)
%   r: CELL array of combined data quality warnings
%   mia1: MIA_BASE object
%   mia2: MIA_BASE object
%
% Combine data quality warnings from two MIA_BASE objects.
%
% See also GETDATAQUALITY, SETDATAQUALITY, ADDDATAQUALITY.

if ~isa(mia1, 'mia_base') | ~isa(mia2, 'mia_base')
  mia1
  mia2
  class(mia1)
  class(mia2)
  [st i] = dbstack;
  disp('--- stack trace:')
  for n = 1:numel(st)
    fprintf('%s: %d\n', st(n).name, st(n).line);
  end
  disp('--- end of stack trace')
  error('incorrect parameters')
end

r = unique([mia1.dataquality mia2.dataquality]);
