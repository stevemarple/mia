function [r,sr,s] = extract(mia, varargin)
%EXTRACT  Extract a subset of data from an object.
%
% EXTRACT returns a subset of the original data, as an object. To obtain
% numerical values see GETDATA.
%
%
%   [r sr s] = EXTRACT(mia, ...)
%   r: MIA_BASE (or derived class) object(s)
%   sr: STRUCT for SUBSREF
%   s: STRUCT with additional information
%   mia: object dervied from MIA_BASE
%   
% Name/value pairs can be given to control how much data is
% extracted. Thie function accepts the name/value pairs described below,
% other overloaded functions may accept other parameters to further limit
% the data extracted.
%
%   'starttime', TIMESTAMP
%   Limit by the start time of the data.
%
%   'endtime', TIMESTAMP
%   Limit by the end time of the data.
%
%   'exclude', [n x 2] TIMESTAMP
%   Exclude certain periods in the dataset. Periods are defined by a
%   start and end time, with one row per excluded period.
%
% NB EXTRACT will be overloaded for most data types. See also the
% overloaded version to identify other parameter name/value pairs which
% are supported.
%
%   [r sr s] = EXTRACT(mia, subs)
%   r: MIA_BASE (or derived class) object(s)
%   sr: STRUCT for SUBSREF
%   s: STRUCT with additional information
%   mia: object dervied from MIA_BASE
%   subs: CELL matrix of subscripts
%
% An alternatively calling convention allows subscripts to be used to
% identify which subsections of data should be returned. subs must be a CELL
% matrix, with n elements where n = GETNDIMS(mia). The string ':' may be
% used to signify all rows/columns etc.
%
%
% For developers additional return values are given to aid overloading
% EXTRACT for new data types.
%
% See also GETDATA, EXTRACTCHECK, SUBSREF, GETNDIMS.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

[sr s] = extractcheck(mia, varargin{:});
r = mia;
r.starttime = s.starttime;
r.endtime = s.endtime;

if ~strcmp(sr.subs{end}, ':')
  r.sampletime = mia.sampletime(sr.subs{end});
  r.integrationtime = mia.integrationtime(sr.subs{end});
end

if ~isempty(r.data)
  r.data = feval(s.subsref, r.data, sr);
end
r = addprocessing(r, s.excludeprocessing);


