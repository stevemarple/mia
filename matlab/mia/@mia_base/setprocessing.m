function r = setprocessing(mia, varargin)
%SETPROCESSING  Set the processing log of a MIA_BASE object
%
%   r = SETPROCESSING(mia, s)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   s: new processing log (CHAR, or CELL array)
%
%   This function should be used with care, since it will remove all
%   details of all processing steps, ADDPROCESSING is preferred.
%
%   See also ADDPROCESSING.

r = mia;

for n = 1:numel(r)
  r(n).processing = {};
  r(n) = addprocessing(r(n), varargin{:});
end
