function [fh,gh,ph] = plot(mia, varargin)
%PLOT Plot a MIA_BASE (or derived) object with labels, title etc.
%
%   [fh gh ph] = PLOT(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   ph: LINE handle(s)
%
% PLOT plots MIA_BASE data into a new FIGURE (created by MAKEPLOTFIG). The
% following parameter name/value pairs may be used to alter the default
% plotting behaviour.
%
%   'color', COLORSPEC
%   Select the color used for the plotted data. See COLORSPEC for valid
%   methods to specify the color.
%
%   'legend', LOGICAL
%   Indicate if the plots should be given a LEGEND. The legend text is
%   automatically derived from the parameter names.
%
%   'miniplots' [x y]
%   Adjust the number of plot axes used. A [1x2] NUMERIC array is
%   required, specifying the number of plots in the x and y directions.
%   
%   'parameters', NUMERIC or CELL
%   Select the parameters(s) to plot. 
%
%   'plotaxes', AXES handle(s)
%   Specify the handle(s) of the AXES to be used for plotting. If given
%   then a new FIGURE is not created. Useful for overlaying plots.
%
%   'title', CELL or CHAR
%   Overrides the default title for the plot. See MAKETITLE for more
%   information.
%
%   'windowtitle', CHAR
%   Overrides the default window title for the FIGURE. See MAKETITLE for
%   more information.
%
%   'ylim', [1x2 NUMERIC]
%   Adjust the Y limits used when plotting.
%
%   'zdata', NUMERIC
%   Override the default ZData setting (empty matrix) for the LINE
%   object. If non-scalar the value of the first element is used.
%
% Any unknown parameters are passed onto MAKEPLOTFIG.
%
% See also GETBEAMS, GETDATA, MAKEPLOTFIG, INFO.

fh = [];
gh = [];
ph = [];

instrument = getinstrument(mia);

defaults.miniplots = [1 1];
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = '';
defaults.colororder = [];
defaults.marker = 'none';
defaults.ylim = info(instrument, 'limits', mia);
defaults.parameters = getparameters(mia);
defaults.legend = [];
defaults.axistitle = false;

% handle extended periods of missing data gracefully
[res resmesg] = getresolution(mia);
if isempty(resmesg)
  % data is spaced regularly
  defaults.resolution = res;
  defaults.processing = ''; % do not additional processing
else
  [defaults.resolution mesg] = guessresolution(mia);
  defaults.processing = 'markmissingdata';
end
defaults.zdata = [];
[defaults unvi] = interceptprop(varargin, defaults);

switch char(defaults.processing)
 case ''
  ; % do nothing
 case 'spaceregularly'
  mia = spaceregularly(mia);
  
 case 'markmissingdata'
  mia = markmissingdata(mia, 'resolution', defaults.resolution);
end

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  if any(~ishandle(gh))
    error('invalid handle');
  end
  % args = {varargin{unvi} 'plotaxes', defaults.plotaxes};
  args = {varargin{unvi}};
  fh = get(gh, 'Parent');
  if iscell(fh) 
    fh = local_cell2mat(fh);
  end
  fh = unique(fh);
else
  gh = [];
  args = {varargin{unvi}};
end

if isempty(defaults.legend)
  defaults.legend = (numel(defaults.parameters) > 1);
else
  defaults.legend = logical(defaults.legend);
end

[title windowtitle] = maketitle(mia, 'parameters', defaults.parameters);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc
if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', ...
			'logo', info(instrument', 'logo'), ...
			args{:}, ...
			'miniplots', defaults.miniplots, ...
			'pointer', 'watch', ...
			'title', defaults.title);
end

% multiple beams - make subplots for all beams requested
if defaults.miniplots == [1 1] & numel(defaults.parameters) > 1
  n = 1:numel(defaults.parameters); % plot all at once
else
  n = 1;
end

paramindex = getparameterindex(mia, defaults.parameters);

data = getdata(mia);
st = getstarttime(mia);
et = getendtime(mia);
samt = getsampletime(mia);


xdata = getcdfepochvalue(samt);
zdata = defaults.zdata;
% If zdata not empty then use first value
if numel(zdata)
  if iscell(defaults.zdata)
    zdata = zdata{1};
  end
  zdata = repmat(zdata(1), size(xdata));
end
dataquality = getdataquality(mia);

for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    ph2 = [];
    if n <= numel(defaults.parameters)
      keepKeys = {'Tag' 'UserData'};
      keepVal = get(gh(x,y), keepKeys); 

      set(get(gh(x,y), 'Title'), ...
	  'Units', 'normalized', ...
	  'Position', [.05 .05 0], ...
	  'HorizontalAlignment', 'left', ...
	  'VerticalAlignment','baseline');
      
      if paramindex(n) == 0
	% parameter not present, blank axis
	% error('selected parameter not in data');
	disp('selected parameter not in data');
	set(gh(x,y), 'Visible', 'off');
      elseif 1
        % Call plot multiple times and honour ColorOrder
        if ~isempty(defaults.colororder)
          set(gh(x,y), 'ColorOrder', defaults.colororder);
        end
        colororder = get(gh(x,y), 'ColorOrder');

        for m = 1:numel(n)
          if ~isempty(defaults.color)
            col = defaults.color;
          else
            col = colororder(mod(m-1, size(colororder, 1))+1,:);
          end
	  if ~isempty(data)
	    ph2(end+1) = plot(xdata, data(paramindex(n(m)),:), ...
                              'Parent', gh(x,y), ...
                              'ZData', zdata, ...
                              'Color', col, ...
                              'Marker', defaults.marker);
	  end
        end
	
	% Create legend string
	legStr = {};
	for m = 1:numel(n)
	  if isnumeric(defaults.parameters)
	    % paramname = defaults.parameters(paramindex(n(m)));
	    paramname = defaults.parameters(n(m));
	  elseif iscell(defaults.parameters)
	    % paramname = defaults.parameters{paramindex(n(m))};
	    paramname = defaults.parameters{n(m)};
	  else
	    error('unknown parameter type'); % ????
	  end
	  if isnumeric(paramname)
	    legStr{m} = sprintf('%g', paramname);
	  elseif ischar(paramname)
	    legStr{m} = paramname;
	  else
	    legStr{m} = char(paramname); % do whatever seems best!
	  end
	end
	if defaults.legend
	  if matlabversioncmp('ge',[9 5])
		% Fix up legend differences
		legend(ph2, legStr{:});
		legend(gh(x,y), 'boxoff');
	  else
		legend(ph2, legStr{:}, 0);
		legend(gh(x,y), 'boxoff');
	  end
	end
	if defaults.axistitle & numel(legStr) == 1
	  set(get(gh(x,y), 'Title'), ...
	      'String', legStr{1});
	end
	
      elseif 1
	ph2 = plot(xdata, data(paramindex(n),:), ...
		   'Parent', gh(x,y), ...
		   'ZData', zdata, ...
		   'Color', defaults.color, ...
		   'Marker', defaults.marker); 
	ph2 = flipud(ph2); % last line is first
      else
	ph2 = line('Parent', gh(x,y), ...
		   'XData', xdata, ...
		   'YData', data(paramindex(n),:), ...
		   'ZData', zdata, ...
		   'Color', defaults.color, ...
		   'Marker', defaults.marker); 
      end
      set(gh(x,y), keepKeys, keepVal);

      if length(n) == 1
	if iscell(defaults.parameters)
	  par = defaults.parameters{n};
	else
	  par = defaults.parameters(n);
	end
	if isnumeric(par)
	  par_str = sprintf('%g', par);
	else
	  par_str = char(par);
	end
	% set the tag on each line to help identify them later
	set(ph2, 'Tag', par_str);
	set(get(gh(x,y), 'XLabel'), 'String', par_str);
      else
	% NOT WORKING
	% multiple plots in one window
	% for m = 1:prod(size(n))
	  % set(ph2(m), 'Tag', sprintf('beam %d', beams(m)));
	% end
	% tags = cell(prod(size(n), 1));
	% for m = 1:length(tags)
	%   tags{m, 1} = sprintf('beam %d', beams(m));
	% end
	% set(ph2, {'Tag'}, tags);
      end	
      
      ph = [ph; ph2(:)];
      
      % if dataquality issues exist warn about them
      if ~isempty(dataquality)
	makeplotfig('addwatermark', gh(x,y), {dataquality});
      end
      
    else
      % axis blank 
      set(gh(x,y), 'Visible', 'off');
    end
    n = n + 1;
  end
end

xlim = getcdfepochvalue([st et]);
if isempty(defaults.plotaxes)
  % fresh plot, set limits
  set(gh, 'XLim', xlim);
else
  % merge with existing plot limits
  xlim2 = get(gh(1), 'XLim');
  xlim(1) = min(xlim(1), xlim2(1));
  xlim(2) = max(xlim(2), xlim2(2));
  set(gh, 'XLim', xlim);
end


if all(isnan(defaults.ylim))
  ; % do nothing about setting limits
elseif any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  for n = 1: prod(size(gh))
    curLim = get(gh(n), 'YLim');
    curLim(notNans) = defaults.ylim(notNans);
    set(gh(n), 'YLim', curLim);
  end
elseif ~isempty(defaults.ylim)
  set(gh, 'YLim', defaults.ylim);
end

% -------
if isempty(defaults.plotaxes) 
  if exist('heatermenu')
    % make tools menus visible
    toolsMenuHandle = findall(fh, 'Tag', 'figMenuTools', 'Type', 'uimenu');
    if isempty(toolsMenuHandle)
      toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
    end
    
    if ~isempty(toolsMenuHandle)
      set(toolsMenuHandle, 'Visible', 'on');
    end
  end

end
    

if length(gh) == 1 
  if isempty(defaults.plotaxes)
    labelProp = defaultaxeslabelparams;
    labelProp.String = datalabel(mia);
    set(get(gh, 'YLabel'), labelProp);
  end
else
  % turn off tick labels - too crowded
  set(gh, 'XTick', [], 'YTick', []);
  % label, but invisibly because they can make the plot too busy
  set(local_cell2mat(get(gh, 'YLabel')), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14, ...
      'Visible', 'off');
end

timetick2(gh, 'class', class(samt));

if isempty(defaults.plotaxes)
  set(fh, 'Name', defaults.windowtitle, ...
	  'Pointer', 'arrow');
end

varargout{1} = fh;
varargout{2} = gh;
varargout{3} = ph;

return;


function r = local_cell2mat(c)
if isnumeric(c{1})
  r = cell2mat(c);
elseif isobject(c{1})
  r = repmat(c{1}, size(c));
  for n = 1:numel(c)
    r(n) = c{n};
  end
end



