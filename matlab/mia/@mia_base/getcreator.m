function r = getcreator(mia)
%GETCREATOR  Return the creator of the MIA_BASE object.
%
%   r = GETCREATOR(mia)
%   r: creator (CHAR)
%   mia: IRISFILEBASE object

r = mia.creator;
