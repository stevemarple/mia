function r = getdata(mia, varargin)
%GETDATA  Return the data matrix, or part of.
%
%   r = getdata(mia)
%   r = getdata(mia, s);
%
%   r = getdata(mia, row, column);     % for 2D data
%   r = getdata(mia, row, column, t);  % for 3D data
%
%   r: data matrix
%   s: SUBSREF struct (see SUBSREF for details)
%   mia: MIA_BASE object
%   row: row(s)
%   col: column(s)
%
%   Return the data matrix, or part of it. Rows correspond to beams, but
%   use GETBEAMINDEX to map beam numbers into rows. Columns correspond to
%   samples. If row (or col) is zero, or empty, then all rows (or
%   columns) of the data matrix are returned. row and col may be vectors.
%
%   See also MIA_BASE, GETBEAMINDEX, SUBSREF.

if matlabversioncmp('>', '5.1')
  sr = 'subsref';
else
  sr = 'subsref2';
end

if nargin == 1
  r = mia.data;

elseif nargin == 2 & isstruct(varargin{1})
  r = feval(sr, mia, varargin{1});
  
else
  for n = 1:length(varargin)
    if isequal(varargin{n}, 0) | isempty(varargin{n})
      warning(sprintf(['Please use '':'' to signify all elements ' ...
		       'of matching index (parameter #%d)'], n));
      varargin{n} = ':';
    end
  end
  s.type = '()';
  s.subs = varargin;
  r = feval(sr, mia.data, s);
end


