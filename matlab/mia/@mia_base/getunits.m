function r = getunits(mia, varargin)
%GETUNITS  Return the units of the MIA_BASE object.
%
%   r = GETUNITS(mia)
%   r: units (CHAR)
%   mia: MIA_BASE object
%
%   See also MIA_BASE, SETUNITS.

defaults.parameterindex = [];
[defaults unvi] = interceptprop(varargin, defaults);
if length(unvi)
  warning(['The following parameter(s) have been ignored:', ...
	   sprintf(' %s', varargin{unvi(1:2:end)})]);
end

if length(mia) ~= 1 
  error('object must be scalar');
end

if nargin == 1 | isempty(defaults.parameterindex)
  r = mia.units;
else
  units = mia.units;
  if iscell(units)
    r = units{defaults.parameterindex};
  else
    % char array, all the same
    r = repmat({units}, size(defaults.parameterindex));
  end
end
