function r = setendtime(mia, t)
%SETENDTIME  Set the endtime of a MIA_BASE object.
%
%   r = SETENDTIME(mia, t)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object whose endtime is to be set
%   t: new endtime (TIMESTAMP)
%
%   NB This function should be considered 'protected' and used only by
%   member functions of irisfilebase, or classes derived from irisfilebase
%
%   See also GETENDTIME, SETSTARTTIME.

r = mia;
r.endtime = t;




