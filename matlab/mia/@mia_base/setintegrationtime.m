function r = setintegrationtime(mia, ts)
%SETINTEGRATIONTIME  Set the integration time in a MIA_BASE object.
%
%   r = SETINTEGRATIONTIME(mia, ts)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%   ts: new integration time (TIMESPAN)
%
%   See also GETINTEGRATIONTIME.

r = mia;
r.integrationtime = ts;
