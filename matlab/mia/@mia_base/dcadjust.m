function [r, err] = dcadjust(mia, refMia, varargin)
%DCADJUST Align a MIA_BASE object against another.
%
%   r = DCADJUST(mia, refMia, ...)
%   [r, err] = DCADJUST(mia, refMia, ...)
%
%   The following name/value pairs are accepted:
%
%   'parameters'
%
%

disp('fix QDC function so first sample is not low');

% check the two inputs are compatible
if ~strcmp(getunits(mia), getunits(refMia))
  error('Input and reference have different units');
end

defaults.parameters = intersect(getparameters(refMia), getparameters(mia));
[defaults unvi] = interceptprop(varargin, defaults);


res = getresolution(mia);
if getresolution(refMia) ~= res
  refMia = setresolution(refMia, res);
end

st = getstarttime(mia);
et = getendtime(mia);

refSt = getstarttime(refMia);
refEt = getendtime(refMia);

if isa(mia, 'rio_qdc_base') & isa(refMia, 'rio_qdc_base') 
  % don't extracting for different start/end times for QDCs
elseif refSt <= st & refEt >= et
  refMia = extract(refMia, ...
		   'starttime', st, ...
		   'endtime', et);
end

numOfParameters = numel(defaults.parameters);
r = extract(mia, 'parameters', defaults.parameters);
err = zeros(numOfParameters, 1);

for bn = 1:numOfParameters
  b = defaults.parameters(bn); % current beam

  miaBI = getparameterindex(mia, b);
  miaData = getdata(mia, miaBI, ':');
  refMiaData = getdata(refMia, getparameterindex(refMia, b), ':');
  
  [adjData, err(bn)] = dcadjust(miaData, refMiaData, varargin{unvi});
  
  r = setdata(r, adjData, miaBI, ':');
  
end

r = addprocessing(r, 'DC adjustment of levels');

