function r = spaceregularly(mia, varargin)
%SPACEREGULARLY  Space samples at a regular intervals
%
%   r = SPACEREGULARLY(mia, ...)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%
% The following parameter name/value pairs are accepted:
%
% 'markmissingdata', LOGICAL
% Flag to indicate if missing data periods should be marked with the
% insertion of NANs before processing commences. See MARKMISSINGDATA for
% more details. Default is TRUE.
%
% 'method', CHAR
% The method interp1 should use for its interpolation. See INTERP1 for
% details of valid methods.
%
% 'resolution', TIMESPAN (scalar)
% The resolution of the regularly spaced samples. If empty (or not given)
% the result of GUESSRESOLUTION is used.
%
% 'sampletime', TIMESPAN
% The sample times to which data should be aligned to. 
%
% It is not necessary to specify both resolution and sampletime, but if
% both are given they must not conflict.
%
% See also GUESSRESOLUTION, INTERP1.


defaults.markmissingdata = true;
defaults.method = '';
defaults.resolution = [];
defaults.sampletime = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if logical(defaults.markmissingdata)
  % mia = markmissingdata(mia, 'resolution', defaults.resolution);
  mia = markmissingdata(mia);
end

r = mia;

[res resmesg] = getresolution(mia);

if ~isempty(defaults.sampletime)
  % check all samples are spaced equally
  if numel(defaults.sampletime) > 1
    dif = diff(defaults.sampletime);
    if any(dif(1) ~= dif)
      error('sampletime is not spaced regularly');
    end
    if ~isempty(defaults.resolution)
      if defaults.resolution ~= dif(1)
	error(['stated resolution does not match resolution inferred from' ...
	       ' sample time']);
      end
    else
      defaults.resolution = dif(1);
    end
  end
end


if isempty(defaults.resolution)
  if ~isempty(resmesg)
    % samples are not spaced equally, so guess resolution
    defaults.resolution = guessresolution(mia);
  else
    defaults.resolution = res;
    if isempty(defaults.sampletime)
      % data is already spaced regularly, no resolution specified and no
      % sampletime so current object is ok
      return
    end
  end
end

if isempty(defaults.sampletime)
  % calculate samples times for new data
  defaults.sampletime = calcdefaultsampletime(mia, defaults.resolution);
end

r = resampledata(r, defaults.sampletime, ...
		 'resolution', defaults.resolution, ...
		 'method', defaults.method);
