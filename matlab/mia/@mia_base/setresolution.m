function [r, msg] = setresolution(mia, newRes, varargin)
%SETRESOLUTION  Set the resolution of a MIA_BASE object, changing the data.
%
%   [r, msg] = SETRESOLUTION(mia, res)
%   [r, msg] = SETRESOLUTION(mia, res, resmethod)
%
%   r: modified MIA_BASE object
%   msg: processing message (intended for use by LOADDATA)
%   mia: MIA_BASE object
%   res: the new resolution (as a TIMESPAN or in seconds) 
%   resmethod: averaging method to use, suitable methods include 'mean',
%   'nonanmean', 'median' and 'nonanmedian', 'mean50', 'interp1',
%   'interp1e', 'rebin' (see REBIN), 'rms' and 'nonanrms'. FEVAL allows
%   other functions to be called.
%
%   See also MIA_BASE, TIMESPAN.

% update the resolution field AND change the resolution of some data

if length(mia) ~= 1
  sz = size(mia);
  msg = cell(sz);
  for n = prod(sz):-1:1
    [r(n) tmp] = setresolution(mia(n), newRes, varargin{:});
    msg{n} = tmp;
  end
  r = reshape(r, sz);
  return
end

% from this point on mia is scalar 
r = mia;
msg = '';

[resGuess resGuessMesg] = guessresolution(mia);
[oldRes resMesg] = getresolution(mia);
if ~isempty(resMesg)
  % error(sprintf('refusing to change resolution: %s', resMesg));
  oldRes = resGuess;
end

% perform some sanity checking
miadatasz = getdatasize(mia);
if miadatasz(end) == 0
  % cannot setresolution when data matrix is empty, but do not give an
  % error since the resolution might be set as data is loaded, are for
  % this case no data exists
  return
elseif numel(getsampletime(mia)) ~= miadatasz(end)
  error('sample time incorrect size');
elseif numel(getintegrationtime(mia)) ~= miadatasz(end)
  error('integration time incorrect size');
end

if nargin <= 2 
  resmethod = '';  % use empty as a default
else
  resmethod = varargin{1};
end

resmethodComment = '';

if isa(resmethod, 'mia_filter_base')
  % use some fancy method to change the resolution
  r = filter(resmethod, mia);
  msg = getprocessing(resmethod, mia);
  return
  
elseif newRes == oldRes
  % already at the desired resolution
  return

elseif newRes < oldRes | ~isvalid(resGuess)
  % Return data at a higher temporal resolution. Do this by using some
  % form of interpolation. As we are interpolating we choose where the
  % new samples should be.

  % Perform the same processing if it is not possible to guess the
  % resolution (ie 0 or 1 data samples)

  if isempty(resmethod) | strcmp(resmethod, 'interp1')
    resmethod = 'linear';
  end

  r = mia;
  if isvalid(resGuess)
    r = markmissingdata(r, 'resolution', oldRes);
  end
  
  r = resampledata(r, calcdefaultsampletime(mia, newRes), ...
		   'method', resmethod);

  return
  
end


% Return data at a lower temporal resolution. There are many possible
% ways of doing this but it should be kept in mind that the data
% supplied may not be sampled at regular intervals (this is true if
% ~isempty(resMesg)).
%

% Ensure the data we are working from is regularly spaced, with standard
% sample times
mia2 = spaceregularly(mia, 'resolution', oldRes);
% mia2, plot(mia2)
r = mia2;
miadata = getdata(mia2);
miadatasz = getdatasize(mia2);
origmiadatasz = miadatasz;

if length(miadatasz) > 2
  % reshape so that we can pretend multi-dimension data is simple
  % time-series with many rows
  miadatasz = [prod(miadatasz(1:(end-1))) miadatasz(end)];
  miadata = reshape(miadata, miadatasz);
end


% calculate new data size
samt2 = calcdefaultsampletime(mia2, newRes);
it = getintegrationtime(mia2);
it2 = repmat(it(1), 1, numel(samt2));

newdatasz = miadatasz;
newdatasz(2) = numel(samt2);

if floor(newdatasz(end)) ~= newdatasz(end)
  error('new resolution must divide exactly into duration');
end



if length(miadatasz) > 2
  % reshape so that we can pretend multi-dimension data is simple
  % time-series with many rows
  miadatasz = [prod(miadatasz(1:(end-1))) miadatasz(end)];
  miadata = reshape(miadata, miadatasz);
end


% Preallocate for speed. Keep data class the same as it was previously.
newdata = repmat(feval(class(miadata), 0), newdatasz);

if isempty(resmethod)
  resmethod = 'mean';
end


blocksz = newRes ./ oldRes;

switch resmethod
 case 'rebin'
  for i = 1:miadatasz(1)
    newdata(i, :) = rebin(miadata(i, :), newdatasz(2));
  end
  for n = 0:(newdatasz(2)-1)
    idx = ((n*blocksz)+1):((n+1)*blocksz);
    it2(n+1) = nonansum(it(idx));
  end

 otherwise
  if ~any(strcmp(resmethod, {'mean' 'median' 'nonanmean' ...
                    'nonanmedian' 'mean50'}))
    warning(sprintf('unknown method (''%s''), attempting anyway', ...
		    resmethod));
  end

  for n = 0:(length(samt2)-1)
    idx = ((n*blocksz)+1):((n+1)*blocksz);
    newdata(:, n+1) = feval(resmethod, miadata(:, idx), 2);
    it2(n+1) = nonansum(it(idx));
    % samt2(n+1) = mean(samt(idx));
  end
end

r = setsampletime(r, samt2);
r = setintegrationtime(r, it2);


if ~isequal(miadatasz, origmiadatasz)
  % reshape back to original size, except for number of samples in time
  % dimension
  miadatasz = origmiadatasz;
  % number of samples in time dimension is same as for newdata. Can
  % safely use size becauase we reshaped to 2D data, no problem with
  % single images here
  miadatasz(end) = size(newdata, 2);
  newdata = reshape(newdata, miadatasz);
end

r = setdata(r, newdata);

% add a comment to the processing section
if isempty(resmethodComment)
  resmethodComment = resmethod;
end
msg = sprintf('resolution changed to %s (%s)', char(newRes), ...
	      resmethodComment);
r = addprocessing(r, msg); 

r = setreshelper(r, mia2, newRes, oldRes, resmethod);

