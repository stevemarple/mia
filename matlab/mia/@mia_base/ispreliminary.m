function r = ispreliminary(mia)
%ISPRELIMINARY  Indicate if a MIA_BASE object's data is marked as preliminary.
%
%   r = ISPRELIMINARY(mia, s)
%   r: LOGICAL
%   mia: MIA_BASE object, or one derived from MIA_BASE
%
%   Indicate if the data is marked as preliminary. mia maybe be an arrya
%   of objects, in whihch case r is an array the same size as mia.
%
%   See also MIA_BASE, GETDATAQUALITY.

r = logical(zeros(size(mia)));

for n = 1:numel(mia)
  tmp = mia(n);
  r(n) = any(strcmpi(tmp.dataquality, 'preliminary'));
end


