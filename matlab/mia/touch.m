function [status, mesg] = touch(file, varargin)
%TOUCH  Change file timestamps.
%
%   [status, mesg] = TOUCH(file)
%   [status, mesg] = TOUCH(file, t)
%   status: status flag (0 = success, non-zero = failure)
%   mesg: error message
%   file: name of file (CHAR)
%   t: time to set (TIMESTAMP)
%
% TOUCH is a convenient interace to the (unix) SYSTEM command "touch". If
% the time parameter is not supplied TOUCH will use the current time. If no
% return parameters are requested TOUCH will generate an ERROR message if an
% error condition is detected.
%
% See also ERROR, SYSTEM, TIMESTAMP.

if numel(varargin)
  t = varargin{1};
else
  t = timestamp('now');
end

tstr = strftime(t, '%Y-%m-%d %H:%M:%S');
cmd = sprintf('touch -d %s -- %s', systemquote(tstr), systemquote(file));

[status, mesg] = system(cmd);
if status 
  % system returned an error
  if isempty(mesg)
    mesg = '<unknown error>';
  elseif isequal(mesg(end), char(10))
    % trim newline
    mesg(end) = [];
  end
end

if nargout
  % error-checking employed, return whether errors or not
  return
end
if status
  % some error and no error-checking employed by caller so stop here
  error(mesg);
end
