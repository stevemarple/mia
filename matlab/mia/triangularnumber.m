function r = triangularnumber(n)
%TRIANGULARNUMBER  Calculate the triangular number
%
%   r = TRIANGULARNUMBER(n)
%   r: triangular number (matrix same size as n)
%   n: scalar or vector
%
%

r = n .* (n + 1) / 2;
