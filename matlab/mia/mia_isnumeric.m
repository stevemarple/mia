function r = mia_isnumeric(varargin)
%MIA_ISNUMERIC  Modified version of ISNUMERIC, to accept multiple parameters.
%
%   r = MIA_ISNUMERIC(a, ...)
%   a: variable to test
%   r: DOUBLE (zeros and/or ones)
%
%   An extended version of the standard ISNUMERIC function, but accepting
%   multiple input parameters. The LENGTH of the returned matrix is
%   identical to the number of parameters supplied.
%
%   See also ISNUMERIC.

r = zeros(size(varargin));
for n = 1:prod(size(varargin))
  r(n) = isnumeric(varargin{n});
end
