function [pos, len] = runlength(x, varargin)
%RUNLENGTH Indicate position and length of non-zero elements in a vector
%
%   [pos, len] = RUNLENGTH(x)
%   [pos, len] = RUNLENGTH(x, n)
%   
%    pos: position
%    len: length of run
%    x: data vector
%    n: optional minimum length
%

pos = [];
len = [];

num = numel(x);
xx = (x ~= 0);

inRun = 0;
for n = 1:num
  if xx(n)
    if inRun
      len(end) = len(end) + 1;
    else
      inRun = 1;
      pos(end+1) = n;
      len(end+1) = 1;
    end
  else
    inRun = 0;
  end
    
end

if length(varargin)
  n = varargin{1};
  pos(len < varargin{1}) = [];
  len(len < varargin{1}) = [];
end



