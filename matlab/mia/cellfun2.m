function r = cellfun2(fun, c)
%CELLFUN2  Call function on every cell element
%
%   r = CELLFUN2(fun, c)
%   r: CELL array
%   fun: function (CHAR or function handle)
%   c: CELL array
%
% CELLFUN2 calls a function on every cell element. It is similar to
% CELLFUN but can operate with arbitary functions, even selecting the
% appropriate class member function for each CELL element.
%
% See also CELLFUN.

r = cell(size(c));

for n = 1:numel(c)
  r{n} = feval(fun, c{n});
end
