function r = miajet2(varargin)
%MIAJET  Variant of JET COLORMAP.
%
%   MIAJET2(M), a COLORMAP variant of JET(M), but the color ranges from
%   black through blue, green, yellow and orange to bright red. Use
%   COLORMAP(MIAJET2).
%
%   See also MIAJET, JET, COLORMAP, RGBPLOT.

if nargin
  sz = varargin{1};
else
  sz = size(colormap, 1);
end
r = zeros(sz, 3);

skeleton = [0 0 0; % black
	    0 0 1; % blue
	    0 1 1; % cyan
	    1 1 0; % yellow
	    1 0 0]; % red

inc1 = 1/(size(skeleton,1)-1);
inc2 = 1/(sz-1);
for rgb = 1:3
  r(:, rgb) = interp1(0:inc1:1, skeleton(:, rgb), [0:inc2:1]');
end
