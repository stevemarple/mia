function r = atof(str)
%ATOF  Convert a string to an integer like the C function atof.
%
%   r = ATOF(s)
%   r: DOUBLE
%   s: CHAR vector (atring)
%
%   Conversion skips any leading whitespace and stops on the first
%   non-numeric character. String may be signed.
%   Not tested on EBCDIC character set.
%
%   See also ATOI.

deprecated('Please use str2num');

n = 1; % index into string
r = 0; % return value
d = 0; % part due to decimal places
s = 1; % final multiplier to get corect sign
if length(str) == 0
  r = 0;
  return;
end

% skip any leading whitespace
while n <= length(str) & isspace(str(n))
  n = n + 1;
end

if str(n) == '+'
  n = n + 1;  % look at next char
elseif str(n) == '-'
  s = -1;
  n = n + 1;  % look at next char
end


% do the conversion (base 10)
while n <= length(str) & str(n) >= '0' & str(n) <= '9'
  r = (r * 10) + str(n) - '0';
  n = n + 1;
end

if n <= length(str) & str(n) == '.'
  n = n + 1;
  % do decimals
  m = 0.1;
  while n <= length(str) & str(n) >= '0' & str(n) <= '9'
    d = d + (m * (str(n) - '0'));
    m = m / 10;
    n = n + 1;
  end
end

r = s * (r + d);  % include sign information and decimal places



