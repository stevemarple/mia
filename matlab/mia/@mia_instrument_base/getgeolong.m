function r = getgeolong(in)
%GETGEOLONG  Return the geographic longitude of a MIA_INSTRUMENT_BASE object.
%
%   r = GETGEOLONG(in)
%   r: longitude (degrees)
%   in: MIA_INSTRUMENT_BASE object
%
%   See also GETGEOLAT.

r = getgeolong(in.location);

