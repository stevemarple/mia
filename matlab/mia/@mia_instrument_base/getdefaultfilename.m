function [r, df] = getdefaultfilename(mia, t);
%GETDEFAULTFILENAME  Return the default filename for an object
%
%   [r df] = GETDEFAULTFILENAME(mia, t);
%   r: default filename/URL (CHAR)
%   df: STRUCT (see defaultfilename command in mia_instrument_base/INFO)

%   mia: MIA_BASE object of type to be loaded. Some basic information may be
%   required (instrument at the very least).
%   t: TIMESTAMP object
%
% The default filename returned by  the defaultfilename command in
% mia_instrument_base/INFO is typically a STRFTIME format
% string. However, if the string begins with an '@' then it signifies a
% function which must be executed (with FEVAL) to return the
% filename. This function hanldes the necessary logic to convert whatever
% INFO returns to a meaningful filename.
%
% Note that the term filename is used loosely, the result may be a URL.
%
% See also mia_instrument_base/INFO, ISURL.

instrument = getinstrument(mia);
if ~isa(instrument, 'mia_instrument_base');
  error('instrument not valid');
elseif numel(instrument) ~= 1
  error('instrument must be scalar');
end

df = info(instrument, 'defaultfilename', mia);

if strcmp(df.fstr(1), '@')
  % 'filename' is actually a function we should execute
  r = feval(df.fstr(2:end), mia, t);
else
  r = strftime(t, df.fstr);
end

