function display(in)
%DISPLAY  Display an INSTRUMENT object.

disp(' ');
disp(sprintf('%s = \n\n%s', inputname(1), char(in)));
disp(' ');
