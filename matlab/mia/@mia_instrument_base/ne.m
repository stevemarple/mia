function r = ne(a, b)
%NE  Test for inequality on two MIA_INSTRUMENT_BASE objects.
%
%   r = NE(a, b)
%   a: MIA_INSTRUMENT_BASE object
%   b: MIA_INSTRUMENT_BASE object
%
%   NE inverts the result of the EQ operator. For more details see
%   mia_instrument_base/EQ.
%
%   See also MIA_INSTRUMENT_BASE, mia_instrument_base/EQ, 

r = ~(a==b);
