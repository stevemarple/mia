function r = char(in, varargin)
%CHAR  Convert an MIA_INSTRUMENT_BASE object to a CHAR.
%
%   r = CHAR(in)

% NB Make all objects derived from mia_instrument_base print a trailing
% newline character 
if length(in) ~= 1
  r = matrixinfo(in);
  return;
end

if length(varargin)
  flag = varargin{1};
else
  flag = 'f';
end

if strcmp(flag, 'c')
  if isempty(in.name)
    r = sprintf('%s #%d', in.abbreviation, in.serialnumber);
  else
    r = sprintf('%s #%d (%s)', in.abbreviation, in.serialnumber, in.name);
  end
else
  r = sprintf(['instrument        : %s\n' ...
	       'name              : %s\n' ...        
	       'abbreviation      : %s\n' ...        
	       'facility          : %s\n' ...        
	       'location          : %s\n' ...
	       'serial no.        : %d\n'], ...
	      gettype(in), in.name, char(in.abbreviation), ...
	      char(in.facility), char(in.location),  in.serialnumber);
end
