function getimageclassname(in)
%GETIMAGECLASSNAME  Return class name(s) of image types for instrument.
%
%   r = GETIMAGECLASSNAME(in)
%   r: CELL array of class names of associated image classes (empty if
%   none)
%   in: instrument object derived from MIA_INSTRUMENT_BASE.
%
%   See also MIA_INSTRUMENT_BASE.

% this is a virtual function, must overload in derived classes
error(sprintf('derive getimageclassname for class %s', ...
	      class(in)));
