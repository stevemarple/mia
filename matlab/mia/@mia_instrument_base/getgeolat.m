function r = getgeolat(in)
%GETGEOLAT  Return the geographic latitude of a MIA_INSTRUMENT_BASE object.
%
%   r = GETGEOLAT(in)
%   r: latitude (degrees)
%   in: MIA_INSTRUMENT_BASE object
%
%   See also GETGEOLONG.

r = getgeolat(in.location);

