function varargout = instrumenttypeinfo(in, info, varargin)
%INSTRUMENTTYPEINFO  Return information about the instrument type.
%
% Dummy fucntion in case INSTRUMENTTYPEINFO is not overloaded for an
% instrument.

% this is a virtual function, it should be overloaded in the real
% instrument class.
cls = class(in);
if strcmp(cls, 'mia_instrument_base')
  error(['mia_instrument_base intended as a virtual base class, not for' ...
	 ' public use']);
else
  error(sprintf('Please overload instrumenttypeinfo for class %s', cls));
end
