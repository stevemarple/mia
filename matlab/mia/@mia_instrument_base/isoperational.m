function r = isoperational(in, varargin)
%ISOPERATIONAL  Indicate if an instrument is operational
%
%   r = ISOPERATIONAL(in)
%   Indicate if an instrument is operational now.
%   
%   r = ISOPERATIONAL(in, t)
%   Indicate if an instrument was operational at time t
%
%   r = ISOPERATIONAL(in, st, et)
%   Indicate if an instrument was operational at between times st and et
%
%   r: DOUBLE
%   in: object(s) derived from MIA_INSTRUMENT_BASE
%   t, st, et: TIMESPAN
%
% If the instrument start time is unknown (i.e., the time is not valid) then
% the earliest possible date is used (TIMESTAMP('-inf')); similarly is the
% end time is unknown (probably because the instrument is currently
% operating) then the latest possible date is used (TIMESTAMP('+inf')).
%
% See also MIA_INSTRUMENT_BASE.

switch length(varargin)
 case 0
  st = timestamp('now');
  et = st;
 
 case 1
  st = varargin{1};
  et = st;
   
 case 2
  st = varargin{1};
  et = varargin{2};
  
 otherwise
  error('incorrect parameters');
end

r = logical(zeros(size(in)));

for n = 1:numel(in)
  op = info(in(n), 'operatingtimes');

  if ~isvalid(op(1))
    op(1) = timestamp('-inf'); % Unknown so assume earliest possible date
  end
  if ~isvalid(op(2))
    op(2) = timestamp('+inf'); % Unknown so assume latest possible date
  end
  
  r(n) = intersect(st, et, op(1), op(2));
end

