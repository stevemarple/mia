function r = getcode(in, varargin)
%GETCODE  Return MIA_INSTRUMENT_BASE abbreviation and serial number
%
%   r = GETCODE(in)
%   r: instrumnent code made up of abbreviation and serial number
%   in: MIA_INSTRUMENT_BASE object
%
%   r = GETCODE(in, include_type)
%   include_type: flag indicating if instrument type is to be prefixed
%   (LOGICAL)
%
% See also GETABBREVIATION, GETSERIALNUMBER.

p = ''; % no prefix
if numel(varargin) >= 1
  if varargin{1}
    p = [instrumenttypeinfo(in, 'abbreviation') '_'];
  end
end

if length(in) == 1
  r = [p getabbreviation(in) '_' int2str(getserialnumber(in))];
else
  r = cell(size(in));
  for n = 1:prod(size(in))
    r{n} = [p getabbreviation(in(n)) '_' int2str(getserialnumber(in(n)))];
  end
end


