function varargout = calcfov(in, varargin)
%CALCFOV  Calculate the field of view of an instrument.
%
%   [x, y] = CALCFOV(in, ...)
%
%   x: X coordinates
%   y: Y coordinates
%   in: MIA_INSTRUMENT_BASE object
%
%   CALCFOV should be considered a protected function, and called only
%   from within class functions of MIA_INSTRUMENT_BASE (and derived
%   classes).
%
%   See also mia_instrument_base/INFO.

defaults.units = [];
defaults.xpixels = [];
defaults.ypixels = [];
[defaults unvi] = interceptprop(varargin, defaults);


if ~isempty(defaults.xpixels) & ~isempty(defaults.ypixels)
  pixx = defaults.xpixels;
  pixy = defaults.ypixels;
else
  if isempty(defaults.units)
    error('units must be set');
  end
  [pixx pixy] = info(in, 'pixels', 'units', defaults.units, ...
		     varargin{unvi});
end
% x = [min(pixx) max(pixx)];
% y = [min(pixy) max(pixy)];
% varargout{1} = x([1 2 2 1]);
% varargout{2} = y([1 1 2 2]);

if length(pixx) < 2 | length(pixy) < 2
  varargout{1} = [];
  varargout{2} = [];
  return
end

% When real cartesian coordinates are used only the four corners are
% required. However, all points are needed since map projections
% introduce distortions.

% go around matrix (from top left)
varargout{1} = [pixx(1:end) repmat(pixx(end), 1, length(pixy)-2) ...
		pixx(end:-1:1) repmat(pixx(1), 1, length(pixy)-2)];

varargout{2} = [repmat(pixy(1), 1, length(pixx)) pixy(2:(end-1)) ...
		repmat(pixy(end), 1, length(pixx)) pixy((end-1):-1:2)];
  
