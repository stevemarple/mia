function r = char2(in, varargin)

if length(in) ~= 1
  r = cell(size(in));
  for n = 1:numel(in)
    r{n} = feval(mfilename, in, varargin{:});
  end
  return
end

defaults.style = 'compact';
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

nm = getname(in);
abbrev = upper(getabbreviation(in));

loc = getlocation(in);
[locName locCountry] = getname(loc);
if ~isempty(locName)
  locStr = locName;
elseif ~isempty(locCountry)
  locStr = locCountry;
else
  locStr = '?';
end

if isempty(nm)
  nmStr = ' ';
else
  nmStr = ['[' nm '] '];
end

if strcmp(defaults.style, 'long')
  posStr = [', ' getlatlonstr(loc)];
else
  posStr = '';
end
r = sprintf('%s %s(%s #%d%s)', locStr, nmStr, abbrev, ...
	    getserialnumber(in), posStr);
