function r = unique(a)
%UNIQUE  Return a unique vector of instruments
%
%   Unlike the normal UNIQUE function the MIA_INSTRUMENT_BASE version does
%   not return a sorted list. The order is based upon their original order.
%
%   See ops/UNIQUE.

if isempty(a)
  r = a;
  return;
end

nmax = numel(a);


if 1
  % new test method, about 7x faster on test set of 120 non-unique
  % riometer objects
  r = a; % allocate to maximum size required
  left = a;
  ridx = 0;
  
  while ~isempty(left)
    % copy first remaining element
    ridx = ridx + 1;
    r(ridx) = left(1);
    left(left(1) == left) = [];
  end
  
  r = r(1:ridx);
  
else
  % old working method
  r = a(1); % guaranteed unique at this stage
  for n = 2:nmax
    copies = find(a(n) == a);
    if copies(1) == n
      % first copy
      r(end+1) = a(n); 
  end
  end
end
