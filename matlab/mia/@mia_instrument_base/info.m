function varargout = info(in, req, varargin)
%INFO Information function for MIA_INSTRUMENT_BASE objects
%
%
%   [...] = INFO(in, request, ...)
%   in: MIA_INSTRUMENT_BASE object
%   request: information request
%
% INFO returns information requests based upon both the instrument type and
% the actual instrument. INFO documents the information requests for all
% instruments. See <instrument_class>/INFO2 (if it exists) for further
% information specific to the instrument type in question (note that INFO2
% should not be called directly). Also, see
% <instrument_type>_MAG_<facility_name> and/or
% <instrument_type>_MAG_<abbreviation>_<serial_number> for information
% specific to a particular group or individual instrument.
%
% Note that the number of return parameters from INFO are variable and
% determined by the type of information request. INFO Information requests
% which are common to all instruments are described below:
%
%   t = INFO(in, 'bestresolution');
%   t: TIMESTAMP detiling best (highest) temporal resolution of an
%   instrument
%
%   [s mesg] = INFO(in, 'defaultfilename', filetype);
%   filetype: class name (CHAR) or an object of the class
%   s: STRUCT containing the following fields:
%       duration: normal duration of each file (TIMESPAN)
%       failiffilemissing: boolean indicating behaviour if file(s) are
%                          not found
%       format: format of the file (e.g., 'mat', 'ascii', or some other value)
%       fstr: STRFTIME format string
%       loadfunction: name of the function used to load data (if empty
%       CONSTRUCTFROMFILES is used)
%       savefunction: name of the function used to save data
%   mesg: CHAR (or possibly CELL of chars) containing warning messages
%   about unknown archives or unknown data classes.
%
%   If only one return value is requested any warning messages are issued by
%   WARNING. Note that for some instrument classes and filetypes the fstr
%   format specifier is an extension of the STRFTIME format specifiers
%   (e.g., for riometers QDCs may use BEAMSTRFTIME format specifiers.
%
%   r = INFO(in, 'limits', datatype);
%   r: upper and lower plot limits
%   Return limits to be used when plotting data of a certain
%   datatype. NAN may be used to indicate no set limit. Unknown
%   datatypes, or those without any limits return and empty matrix,
%   otherwise r is a 1x2 DOUBLE matrix.
%
%   t = INFO(in, 'starttime');
%   t = INFO(in, 'endtime');
%   t: start or endtime (TIMESTAMP)
%   Return the start/end time of operation. If unknown an invalid
%   TIMESTAMP is returned (see timestamp/ISVALID).
%
%   t = INFO(in, 'operatingtimes');
%   t: [starttime endtime] (1x2 TIMESTAMP)
%   Return the start and end times of operation. If unknown an invalid
%   TIMESTAMP is returned (see timestamp/ISVALID).
%
%   r = INFO(in, 'url');
%   r: uniform resource locator for the instrument (if known)
%
%
% See also INFO2.

if nargout == 0
  nout = 1;
else
  nout = nargout;
end

if length(in) ~= 1
  for n = 1:nout
    varargout{n} = cell(size(in));
  end
  for n = 1:numel(in)
    [tmp{1:nout}] = feval(mfilename, in(n), req, varargin{:});
    for m = 1:nout
      varargout{m}{n} = tmp{m};
    end
  end
  return
end

% from this point on 'in' is singular

insabbrev = instrumenttypeinfo(in, 'abbreviation');
abbrev = lower(getabbreviation(in));
sn = getserialnumber(in);
fac = lower(getfacility(in));

% get instrument data  (as struct)
s = feval(sprintf('info_%s_%s_%d_data', insabbrev, abbrev, sn));

% When calling functions need to know if the request is valid or not so
% that after trying all possible functions any requests which have not
% been marked as valid generate an error. Do this by getting the helper
% functions to set the valid field in the vout struct to a true value
vout.varargout = cell(1, nout); % will become varargout later

% set to 1 here, but zero in the "otherwise" section of the case statement
vout.validrequest = 1; 

% check for requests which are common to all instruments
switch req
 case 'bestresolution'
  vout.varargout{1} = s.resolution;
  
 case 'defaultfilename'
  % If the structure contains the relevant field return that data,
  % otherwise return an empty matrix. 
  cls = varargin{1};
  if ~ischar(varargin{1})
    cls = class(varargin{1});
  end
  vout.varargout{2} = ''; % no warnings initially
  
  defaults.archive = ''; % defer
  [defaults unvi] = interceptprop(varargin(2:end), defaults);
  if ~isempty(defaults.archive)
    if defaults.archive(1) >= '0' & defaults.archive(1) <= '9' 
      defaults.archive = ['Q' defaults.archive];
    end
  end
  
  tmp = getfield(s, req);
  if isfield(tmp, cls)
    tmp =  getfield(tmp, cls);
    if isempty(defaults.archive)
      % find (first) default archive
      fn = fieldnames(tmp);
      for n = 1:numel(fn)
        tmp2 = getfield(tmp, fn{n});
        if tmp2.defaultarchive
          defaults.archive = fn{n};
          break;
        end
      end
    end
    
    if isfield(tmp, defaults.archive)
      vout.varargout{1} = getfield(tmp, defaults.archive);
    
      if length(vout.varargout{1}.fstr) >= 1
        % prepend the directory with the data directory for this
        % instrument. Note that mia_datadir(in) can be overloaded for each
        % instrument class. Don't prepend for URLs.
        if isurl(vout.varargout{1}.fstr(1))
          ;
        elseif vout.varargout{1}.fstr(1) ~= '/' & ...
              vout.varargout{1}.fstr(1) ~= '@' 
          vout.varargout{1}.fstr = fullfile(mia_datadir(in), ...
                                            vout.varargout{1}.fstr);
        end
      end
      % database uses integers to hold size, so unknown is -1, not nan
      vout.varargout{1}.size(vout.varargout{1}.size == -1) = nan;
      if ~isfield(vout.varargout{1}, 'resolution')
        vout.varargout{1}.resolution = timespan('bad');
      elseif isempty(vout.varargout{1}.resolution)
        vout.varargout{1}.resolution = timespan('bad');
      end
    else
      % no such archive
      vout.varargout{1} = [];
      vout.varargout{2} = ...
	  sprintf('no such archive (%s) for this class (%s)', ...
		  defaults.archive, cls);
    end
  else
    % no such data class
    vout.varargout{1} = [];
    vout.varargout{2} = sprintf('no such data class (%s)', cls);
  end
  
  % Issue warnings unless the caller has asked for them to be returned
  if nargout < 2 & ~isempty(vout.varargout{2})
    warning(vout.varargout{2});
  end

 case 'institutions'
  if isfield(s, req)
    vout.varargout{1} = getfield(s, req);
  else
    vout.varargout{1} = {};
  end

 case 'limits'
  cls = varargin{1};
  if ~ischar(varargin{1})
    cls = class(varargin{1});
  end
  tmp = getfield(s, req);
  if isfield(tmp, cls)
    vout.varargout{1} = getfield(tmp, cls);
  else
    vout.varargout{1} = [];
  end
  
 case 'logo'
  if isfield(s, req)
    vout.varargout{1} = getfield(s, req);
  else
    vout.varargout{1} = '';
  end

 case 'logurl'
  vout.varargout{1} = getfield(s, req);;
  
 case {'starttime' 'endtime'}
  vout.varargout{1} = getfield(s, req);
  
 case 'operatingtimes'
  vout.varargout{1} = [s.starttime s.endtime];
 
 case 'url'
  if ~isempty(s.url)
    vout.varargout{1} = s.url;
  else
    vout.varargout{1} = s.facility_url;
  end
  
  % undocumented feature for admin purposes
 case '_struct'
  vout.varargout{1} = s;
  
 otherwise
  vout.validrequest = 0;
end


% give the instrument function a chance to modify any common requests, or
% to check for specific ones
vout = info2(in, vout, s, req, varargin{:});

% do specific functions for this facility and instrument exist? If so give
% then a chance to modify the return value

mFnames = {sprintf('info_%s_%s', insabbrev, fac) ...
	   sprintf('info_%s_%s_%d', insabbrev, abbrev, sn) ...
	  'info_local'};
for n = 1:length(mFnames)
  if ~isempty(which([mFnames{n} '(in)']))
    % call the function to allow it the opportunity to modify the value(s)
    % which are about to be returned. Parameters to the function are:
    
    % instrument (scalar)
    % current return values
    % data structure for instrument
    % info request
    % any additional parameters passed in
    
    % if the function does not understand the request it should return the
    % current return value(s) unchanged
    vout = feval(mFnames{n}, in, vout, s, req, ...
		 varargin{:});
  end
end

if vout.validrequest
  varargout = vout.varargout;
else
  error(sprintf('request not valid (was ''%s'')', req));
end

