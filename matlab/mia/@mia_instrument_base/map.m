function h = map(in, ah, varargin)
%MAP  Plot an instrument location onto a map.
%
%   h = MAP(in, ah, ...)
%   h: LINE and/or TEXT handle(s)
%   in: MIA_INSTRUMENT_BASE object
%   ah: handle of axis to plot into
%
%   MAP will add instrument locations onto a map. It will attempt to
%   identify if the axis contains M_MAP coastlines, and if so will use
%   the M_LINE and/or M_TEXT functions instead of the standard matlab
%   functions.
%
%   The default behaviour can be modified with the following
%   parameter/value pairs:
%
%     'linefunction', CHAR
%     The function to call for adding LINE objects to an AXIS.
%
%     'lineargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the line
%     function. Use to modify the marker type and color.
%
%     'abbreviation', [0 | 1]
%     Boolean to indicate if the instrument code/abbreviation should be
%     printed.
%
%     'textfunction', CHAR
%     The function to call for adding TEXT objects to an AXIS.
%
%     'textargs', CELL
%     A CELL vector of parameter/value pairs to to the text
%     function. Use to modify the color and alignment of the text.
%
%     'formatabbreviation', CHAR
%     A function name used to transform the abbreviations (typically
%     lower case) to the string used by textfunction.
%
%   See also MIA_INSTRUMENT_BASE, AXIS, LINE, TEXT, M_MAP, M_LINE, M_TEXT.

defaults.linefunction = [];  % defer
defaults.lineargs = [];      % defer

defaults.abbreviation = 0;         % don't print abbrev
defaults.textfunction = [];  % defer
defaults.textargs = {'HorizontalAlignment', 'left', ...
		    'VerticalAlignment','bottom'};
defaults.formatabbreviation = '';

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.linefunction)
  % does axis have any children which look they they belong to m_map?
  if ismmap(ah)
    defaults.linefunction = 'm_line';
  else
    defaults.linefunction = 'line';
  end
end
 

if isempty(defaults.lineargs)
  defaults.lineargs = getdefaultlineargs(in);
end
sz = prod(size(in));

h = zeros(sz, 1);
if matlabversioncmp('>', '5.1')
  cb = {'ButtonDownFcn' 'mia_instrument_base_cb;'};
else
  cb = {};
end
for n = 1:sz
  ud = [];
  ud.mia.instrument = in(n);
  loc = getlocation(in(n));
  h(n,1) = feval(defaults.linefunction,  ...
		 getgeolong(loc), getgeolat(loc), ...
		 defaults.lineargs{:}, ...
		 'UserData', ud, ...
		 cb{:});
end

if logical(defaults.abbreviation)
  if isempty(defaults.textfunction)
    % does axis have any children which look they they belong to m_map?
    if ismmap(ah)
      defaults.textfunction = 'm_text';
    else
      defaults.textfunction = 'text';
    end
  end
  for n = 1:sz
    tmp = getabbreviation(in(n));
    if ~isempty(defaults.formatabbreviation)
      tmp = feval(defaults.formatabbreviation, tmp);
    end
    loc = getlocation(in(n));
    h(n,2) = feval(defaults.textfunction, ...
		   getgeolong(loc), getgeolat(loc), ...
		   tmp, defaults.textargs{:});
  end
end

