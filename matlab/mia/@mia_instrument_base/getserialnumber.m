function r = getserialnumber(in)
%GETSERIALNUMBER  Return the MIA_INSTRUMENT_BASE serial number.
%
%   r = GETSERIALNUMBER(in)
%   r: serial number
%   in: MIA_INSTRUMENT_BASE object
%
%   See also MIA_INSTRUMENT_BASE.

r = zeros(size(in));
for n = 1:prod(size(in))
  r(n) = in(n).serialnumber;
end
 
