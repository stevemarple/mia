function r = getfacility(in)
%GETFACILITY  Return MIA_INSTRUMENT_BASE facility code
%
%   r = GETFACILITY(in)
%   r: CHAR facility
%   in: MIA_INSTRUMENT_BASE details
%
%   See also MIA_INSTRUMENT_BASE.

if length(in) == 1
  r = in.facility;
  %   if isempty(r)
  %     disp(char(in))
  %     warning('Facility not set');
  %   end
else
  r = cell(size(in));
  for n = 1:prod(size(in))
    r{n} = getfacility(in(n));
  end
end
