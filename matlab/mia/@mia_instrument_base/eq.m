function r = eq(a, b)
%EQ  Test for equality on two MIA_INSTRUMENT_BASE objects.
%
%   r = EQ(a, b)
%   a: MIA_INSTRUMENT_BASE object
%   b: MIA_INSTRUMENT_BASE object
%
%   Test if two instruments are the same. They must be of the same type,
%   with the same LOCATION and serial number.
%
%   See also MIA_INSTRUMENT_BASE mia_instrument_base/NE, LOCATION, location/EQ.

asz = size(a);
bsz = size(b);
maxsz = max(asz, bsz);
if length(a) > 1 & length(b) > 1 & ~isequal(asz, bsz)
  error('if a and b are not scalar then matrices must be the same size');
end

if ~strcmp(class(a), class(b))
  % all elements in a are same class, so no need to test per element
  r = logical(zeros(maxsz));
  return;
end

if length(a) == 1
  A = repmat(a, bsz);
else
  A = a;
end
if length(b) == 1
  B = repmat(b, asz);
else
  B = b;
end

r = zeros(maxsz);
for n = 1:prod(maxsz)
  r(n) = local_eq(A(n), B(n));
end
r = logical(r);
return

% % ----------------------------------

% asz = size(a);
% bsz = size(b);
% maxsz = max(asz, bsz);

% if length(a) > 1 & length(b) > 1 & ~isequal(asz, bsz)
%   error('if a and b are not scalar then matrices must be the same size');
% end

% if ~strcmp(class(a), class(b))
%   % all elements in a are same class, so no need to test per element
%   r = logical(zeros(maxsz));
%   return;
% end



% r = (getserialnumber(a) == getserialnumber(b)) & ...
%     (getlocation(a) == getlocation(b));
% return;


% -------------------------------
function r = local_eq(a, b)
r = strcmp(getcode(a), getcode(b));
