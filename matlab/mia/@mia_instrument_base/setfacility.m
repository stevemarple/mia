function r = setfacility(in, facility)
%SETFACILITY  Update the INSTRUMENT facility.
%
%   r = SETFACILITY(in, facility)
%   r: modified INSTRUMENT object
%   in: INSTRUMENT
%   facility: CHAR
%
%   See also GETFACILITY, INSTRUMENT.

r = in;

if length(r) ~= 1
  for n = 1:numel(in)
    tmp = r(n);
    tmp.facility = loc;
    r(n) = tmp;
  end
else
  r.facility = facility;
end

