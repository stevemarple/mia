function r = setabbreviation(in, s)
%SETABBREVIATION  Return MIA_INSTRUMENT_BASE abbreviation code
%
%   r = SETABBREVIATION(in, s)
%   r: CHAR abbreviation
%   in: MIA_INSTRUMENT_BASE object
%   s: abbreviation (CHAR)
%
%   See also MIA_INSTRUMENT_BASE.

r = in;
r.abbreviation = s;
