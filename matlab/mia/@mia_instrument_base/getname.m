function r = getname(in)
%GETNAME  Return instrument name.
%
%   r = GETNAME(in)
%
%   See also MIA_INSTRUMENT_BASE.

r = in.name;
