function r = setserialnumber(in, serialnumber)
%SETSERIALNUMBER  Update the INSTRUMENT serial number.
%
%   r = SETSERIALNUMBER(in, serialnumber)
%   r: modified INSTRUMENT object
%   in: INSTRUMENT
%   serialnumber: CHAR
%
%   See also GETSERIALNUMBER, INSTRUMENT.

r = in;

if length(r) ~= 1
  for n = 1:numel(in)
    tmp = r(n);
    tmp.serialnumber = loc;
    r(n) = tmp;
  end
else
  r.serialnumber = serialnumber;
end

