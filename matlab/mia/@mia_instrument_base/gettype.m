function r = gettype(in, varargin)
%GETTYPE  Return instrument type
%
%   r = GETTYPE(in)
%
%   See also MIA_INSTRUMENT_BASE.

error(sprintf('Derive gettype for class %s', class(in)));
r = '';
