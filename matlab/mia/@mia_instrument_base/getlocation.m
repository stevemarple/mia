function r = getlocation(in)
%GETLOCATION  Return INSTRUMENT location.
%
%   r = GETLOCATION(in)
%   r: LOCATION object
%   in: INSTRUMENT
%
%   See also SETLOCATION, INSTRUMENT, LOCATION.

if isempty(in)
  r = repmat(location, size(in));
elseif length(in) > 1
  r = cell(size(in));
  for n = 1:numel(in)
    r{n} = getlocation(in(n));
  end
else
  r = in.location;
end
return

r = repmat(location, size(in));
for n = 1:prod(size(in))
  r(n) = in(n).location;
end
