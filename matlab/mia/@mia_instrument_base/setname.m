function r = setname(in, name)
%SETNAME  Update the INSTRUMENT name.
%
%   r = SETNAME(in, name)
%   r: modified INSTRUMENT object
%   in: INSTRUMENT
%   name: CHAR
%
%   See also GETNAME, INSTRUMENT.

r = in;

if length(r) ~= 1
  for n = 1:numel(in)
    tmp = r(n);
    tmp.name = loc;
    r(n) = tmp;
  end
else
  r.name = name;
end

