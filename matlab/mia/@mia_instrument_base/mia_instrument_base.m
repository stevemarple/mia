function in = mia_instrument_base(varargin)
%MIA_INSTRUMENT_BASE  Constructor for MIA_INSTRUMENT_BASE class.
%
%   MIA_INSTRUMENT_BASE is an abstract class.
%
%   See also RIOMETER

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

in.name = '';
in.abbreviation = '';
in.facility = ''; % name if part of a group of instruments (e.g., EISCAT)
in.location = [];

% significant changes should be reflected by changing the serial
% number. For a riometer a change in operating frequency should use a
% change in serial number
in.serialnumber = 1; % integer


if nargin == 0
  % default constructor
  in = class(in, 'mia_instrument_base');
  
% elseif nargin == 1 & isa(varargin{1}, 'mia_instrument_base')
% in = varargin{1};
elseif nargin == 1 & strcmp(class(varargin{1}), 'mia_instrument_base')
  in = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  if length(unvi)
    warning(['The following parameter(s) have been ignored:', ...
	     sprintf(' %s', varargin{unvi(1:2:end)})]);
  end
  in = class(in, 'mia_instrument_base');
  
else
  error('incorrect parameters');
end

  % Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;

  
