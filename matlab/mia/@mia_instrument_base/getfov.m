function r = getfov(in)
%GETFOV  Return the field of view information
%
%   r = GETFOV(in)
%   r: CELL array of geograpic positions giving the fields of view
%   in: MIA_INSTRUMENT_BASE object(s)
%
%   GETFOV returns information about the field of view for all 'in'
%   instruments. The results are inside a CELL array of size 2 by N,
%   where N is the number of instruments in 'in'.

% MIA_INSTRUMENT_BASE is an abstract type, so no need to do anything.
if strcmp(class(in), 'mia_instrument_base')
  % do nothing for mia_instrument_base, it is supposed to be an abstract
  % class 
  r = cell(2, prod(size(in)));
  return;
end

error(sprintf('Please overload %s for instruments of type %s.', ...
	      mfilename, class(in)));
