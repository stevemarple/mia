function [r, idx] = sort(rio, varargin)
%SORT  Return a sorted list of riometers

if nargin == 1
  order = {'latitude' 'serialnumber', 'locationname'};
else
  order = varargin{1};
end

% lat (desc), SN, loc1,loc2 string
N = prod(size(rio));

tmp.latitude = zeros(N, 1);
tmp.serialnumber = zeros(N, 1);
tmp.locationname = repmat('', N, 1);
longestStr = 0;

for n = 1:N
  loc = getlocation(rio(n));
  sn = getserialnumber(rio(n));
  str = char(loc);
  longestStr = max(longestStr, length(str));
    
  tmp.latitude(n, 1) = -getgeolat(loc); % want highest latitude first
  tmp.serialnumber(n, 1) = sn;
  tmp.locationname(n,1:length(str)) = str;
end

tmp2 = [];

for n = 1:length(order)
  f = double(getfield(tmp, order{n}));
  if strcmp(order{n}, 'locationname') & length(f) < longestStr
    f(longestStr) = 0;
  end
  tmp2 = [tmp2, f];
end
[tmp3 idx] = sortrows(tmp2);
r = rio(idx);
