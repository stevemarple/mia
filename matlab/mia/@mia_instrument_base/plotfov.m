function h = plotfov(in, ah, varargin)
%PLOTFOV  Plot an instrument's field of view on a map.
%
%   h = PLOTFOV(in, ah, ...)
%   h: PATCH handle(s)
%   in: MIA_INSTRUMENT_BASE object
%   ah: handle of axis to plot into
%
%   PLOTFOV will add instrument fields of view (if appropriate) onto a
%   map. It will attempt to identify if the axis contains M_MAP coastlines,
%   and if so will use the M_LINE and/or M_TEXT functions instead of the
%   standard matlab functions.
%
%   The default behaviour can be modified with the following
%   parameter/value pairs:
%
%     'style', ['line' | 'patch' | 'transpatch']
%     Determine if the field of view is to be drawn with a LINE, PATCH or
%     fake transparent patches (see GPC_TRANSPATCH/GPC_M_TRANSPATCH).
%
%     'ismmap', LOGICAL
%     Indicate if the AXES was drawn with M_MAP, and thus whether the M_MAP
%     equivalents of the drawing functions should be used. In normal use
%     PLOTFOV is able to correctly identify this case with ISMMAP.
%
%     'linefunction', CHAR
%     The function to call for adding LINE objects to an AXIS.
%
%     'lineargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the line
%     function. Use to modify the edge and face color, etc.
%
%     'patchfunction', CHAR
%     The function to call for adding PATCH objects to an AXIS.
%
%     'patchargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the patch
%     function. Use to modify the edge and face color, etc.
%
%   See also mia_image_base/PLOTFOV, MIA_INSTRUMENT_BASE, AXIS,
%   PATCH, M_MAP, M_PATCH. 

h = [];
defaults.style = 'patch'; % patch | line
defaults.patchfunction = [];  % defer
defaults.patchargs = {'EdgeColor', 'k', ...
		    'FaceColor', 'none'};

defaults.linefunction = [];  % defer
defaults.lineargs = {'Color', 'k', ...
		    'LineStyle', '-', ...
		    'Marker', 'none'};

defaults.units = 'deg';
defaults.height = [];
defaults.ismmap = ismmap(ah);

if isa(in, 'riometer')
  defaults.antennaazimuth = []; % use default values (see later)
end

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.patchfunction)
  % does axis have any children which look they they belong to m_map?
  if defaults.ismmap
    defaults.patchfunction = 'm_patch';
  else
    defaults.patchfunction = 'patch';
  end
end
if isempty(defaults.linefunction)
  % does axis have any children which look they they belong to m_map?
  if defaults.ismmap
    defaults.linefunction = 'm_line';
  else
    defaults.linefunction = 'line';
  end
end

if isa(in, 'riometer')
  if isempty(defaults.antennaazimuth)
    defaults.antennaazimuth = info(in, 'antennaazimuth'); 
    if iscell(defaults.antennaazimuth)
      defaults.antennaazimuth = [defaults.antennaazimuth{:}];
    end
  end
end

sz = prod(size(in));
if length(defaults.height) == 1 & sz ~= 1
  defaults.height = repmat(defaults.height, size(in));
end

% cannot be sure every camera will have a field of view outline, so
% cannot preallocate matrix
Nout = 0;
for n = 1:sz
  [x y] = info(in(n), 'fov', 'units', defaults.units, varargin{unvi});
  if ~isempty(defaults.height)
    height = defaults.height(n);
  else
    height = info(in(n), 'defaultheight'); % height in metres
  end
  
  coord2degArgs = {getlocation(in(n)), x, y, defaults.units, ...
		   varargin{unvi}, ...
		   'height', height};
  if strcmp(defaults.units, 'm_antenna') | ...
	strcmp(defaults.units, 'km_antenna') 
    coord2degArgs{end+1} = 'antennaazimuth';
    % coord2degArgs{end+1} = info(in(n), 'antennaazimuth');
    coord2degArgs{end+1} = defaults.antennaazimuth(n);
  end
  
  switch defaults.style
   case 'patch'
    if strcmp(defaults.patchfunction, 'm_patch')
      [x y] = coord2deg(coord2degArgs{:});
    end
    if ~isempty(x)
      tmp = feval(defaults.patchfunction,  x, y, 'r', ...
		  defaults.patchargs{:});
    else
      tmp = [];
    end

   case 'transpatch'
    % experimental
    [x y] = coord2deg(coord2degArgs{:});

    if ~isempty(x)
      % both methods below are equivalent
      if 0
	% [x y] = m_ll2xy(x, y, 'clip', 'patch');
	tmp = gpc_m_transpatch(gpc(x, y), ah, 0.8, ...
			       defaults.patchargs{:}, ...
			       'convertcoordinates', 1);
      else
	tmp = gpc_m_transpatch(gpc_m_map(x, y), ah, 0.8, ...
			       defaults.patchargs{:});
      end
    else
      tmp = [];
    end
   

    
   case 'line' 
    if strcmp(defaults.linefunction, 'm_line')
      [x y] = coord2deg(coord2degArgs{:});
    end
    
    % unlike patches lines must be explicitly closed back to the start
    if ~isempty(x)
      x = [x(:); x(1)];
      y = [y(:); y(1)];
    end
    tmp = feval(defaults.linefunction,  x, y, ...
		defaults.lineargs{:});

   otherwise
    error('Unknown style');
  end
    
  % if ~isempty(tmp)
  %  Nout = Nout + 1;
  %  h(Nout,1) = tmp;
  % end
  h = [h; tmp];
end



