function r = getabbreviation(in)
%GETABBREVIATION  Return MIA_INSTRUMENT_BASE abbreviation code
%
%   r = GETABBREVIATION(in)
%   r: CHAR abbreviation
%   in: MIA_INSTRUMENT_BASE details
%
%   See also MIA_INSTRUMENT_BASE.

if length(in) == 1
  r = in.abbreviation;
  %   if isempty(r)
  %     disp(char(in))
  %     warning('Abbreviation not set');
  %   end
else
  r = cell(size(in));
  for n = 1:prod(size(in))
    r{n} = getabbreviation(in(n));
  end
end
