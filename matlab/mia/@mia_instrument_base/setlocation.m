function r = setlocation(in, loc)
%SETLOCATION  Update the INSTRUMENT location.
%
%   r = SETLOCATION(in, loc)
%   r: modified INSTRUMENT object
%   in: INSTRUMENT
%   loc: LOCATION object
%
%   See also GETLOCATION, INSTRUMENT, LOCATION.

r = in;

if length(r) ~= 1
  for n = 1:numel(in)
    tmp = r(n);
    tmp.location = loc;
    r(n) = tmp;
  end
else
  r.location = loc;
end

