function r = nonansum(data, varargin)
%NONANSUM  Similar to SUM, but ignore elements which are NAN.
%
%   r = NONANSUM(x)
%   r = NONANSUM(x, dim)
%
% Take the SUM value ignoring the effect of any NANs. If there are no
% non-NaN elements then 0 is returned.
%
% See also SUM, NONANMEAN, NONANMEDIAN, NONANISEQUAL.

d = data;
d(isnan(data)) = 0;

r = sum(d, varargin{:});

