function r = getancestors(c)
%GETANCESTORS Find parent, grandparent etc.
%
%   r = GETANCESTORS(obj)
%   r: CELL array of ancestors
%   obj: a derived OBJECT
%
% GETANCESTORS returns a CELL array of all ancestors of the OBJECT obj,
% the parent is the first element in the array. obj and its ancestors
% must not be formed from multiple inheritance, otherwise an ERROR will
% result.
%
% See also GETPARENT.

% r = {};
r = {class(c)};
while 1
  tmp = getparentclass(c);
  if iscell(tmp)
    error(' found multiple inheritance');
  elseif isempty(tmp)
    break;
  end
  r{end+1} = tmp;
  cs = struct(c);
  c = getfield(cs, tmp);
end
