function [r, w] = figure2imagefile(h, filename, varargin)
%FIGURE2IMAGEFILE  Print a figure to an image file (PNG, BMP, etc)
%
%   FIGURE2IMAGEFILE(h, filename, ...)
%
%   h: FIGURE or AXIS handle
%   filename: filename (optionally, with path)
%
%   The following parameter name/values pairs are accepted:
%
%     'format', CHAR
%     By default the image format is derived from the file extension.
%
%     'makedirectory', LOGICAL
%     Create the directory to save the image file into (default is 0)
%
%     'screengrab', LOGICAL Get the image information by calling the
%     ImageMagick program import(1), instead of the default method of using
%     GETFRAME and IMWRITE. This method is not recommended as it requires
%     XWindows (and no screen saver to be running when the window is
%     grabbed), but it does allow more esoteric file formats to be
%     supported.
%
%     'size', [1x2 DOUBLE]
%     The size of the image, in pixels. This is achieved by resizing the
%     image, therefore ResizeFcn will be called automatically if set. 
%
%   See also FIGURE, AXES, PRINT.

cf = get(0, 'CurrentFigure');
if ~isempty(cf)
  ca = get(cf, 'CurrentAxes');
end

[Path basename ext] = fileparts(filename);

defaults.makedirectory = 0;
defaults.format = ext(2:end);
defaults.size = [];
defaults.screengrab = 0;

[defaults unvi] = interceptprop(varargin, defaults);

if length(h) > 1
  error('handle must be a scalar');
elseif ~ishandle(h)
  error('not a handle');
else  
  type = get(h, 'Type');
end

screenSize = get(0, 'ScreenSize');

hs = get(h); % get original values as a struct
set(h, 'Units', 'pixels');
hs2 = get(h);

defaults.size = round(defaults.size);

if isoctave & isunix
  terminalProtocol = 'x';
else
  terminalProtocol = get(0, 'TerminalProtocol');
end
switch terminalProtocol
 case 'none'
  % at this point should really print to file, possibly converting to PNG
  % for matlab 5.1
  error('cannot convert figure to an image file without a graphical terminal');
  
 case 'x'
  % ok, everything below assumes X
  
 otherwise
  % will probably fail here for windows. Could probably treat windows the
  % same as X, except for the case of screengrabs.
  error(sprintf('unknown TerminalProtocol (was ''%s'')', terminalProtocol));
end

switch type
 case 'axes'
  fh = hs.Parent;
  if ~isempty(defaults.size)
    warning('resizing of axes not yet supported');
  end
  fhs = get(fh);
  set(fh, 'Units', 'Pixels');
  fhs2 = get(fh);
  pos = [hs2.Position([3 4]) hs2.Position([1 2]) + fhs2.Position([1 2])];
  
 case 'figure'
  fh = h;
  % resize figure, allowing for always-on-top toolbars
  if ~isempty(defaults.size)
    hs2.Position = [10 80 defaults.size];
  else
    hs2.Position = [10 80 hs2.Position(3:4)];
  end
  % check if figure is (probably) visible (can't tell about always-on-top
  % toolbars ignored)
  curpos = get(h, 'Position');
  if all(curpos(1:2) > hs2.Position(1:2)) & ...
	all(curpos(1:2) + curpos(3:4) < screenSize(3:4))
    % probably visible
    pos = curpos;
  else
    disp([mfilename ': resizing figure']);
    set(h, 'Position', hs2.Position); 
    pos = hs2.Position([3 4 1 2]);
  end
  
 otherwise
  error(sprintf('handle is of type %s, which is not supported', type));
end


if logical(defaults.makedirectory)
  mkdirhier(Path);
end

fhs = get(fh);

figure(fh); % bring to front
drawnow; % redraw events MUST be scheduled
[x map] = getframe(h);

if logical(defaults.screengrab)
  if ~strcmp(terminalProtocol, 'x')
    error(sprintf('do not know how to do screen grabs for %s', ...
		  terminalProtocol));
  end
	  
  set(fh, 'Units', 'Pixels');
  fhs2 = get(fh);
  
  windowname = fhs.Name;
  if logical(fhs.NumberTitle)
    if ~isempty(windowname)
      windowname = [': ' windowname ' '];
    end
    if logical(fhs.IntegerHandle)
      windowname = sprintf('Figure No. %d%s ', fh, windowname);
    else
      windowname = sprintf('Figure No. %.6f%s ', fh, windowname);
    end
  end
  
  % pos is the X geometry position vector. In X window system y coordinate
  % is measured DOWNWARDS
  pos(4) = screenSize(4) - sum(pos([2 4]));
  pos = pos + [0 0 -1 1];
  
  disp(sprintf('saving image to %s', filename));
  cmd = sprintf(['import -silent -window root -crop %dx%d+%d+%d %s'], ...
		pos, filename);
  [s, w] = unix(cmd);
  
  if s
    error(sprintf('error from import: %s', w));
  end
  
else
  disp(sprintf('saving image to %s', filename));
  if isempty(map)
    imwrite(x, filename, defaults.format, varargin{unvi});
  else
    imwrite(x, map, filename, defaults.format, varargin{unvi});
  end
end

% set back to original values
set(h, 'Units', hs.Units);
set(fh, 'Units', fhs.Units);

if ~isempty(defaults.size)
  set(h, 'Position', hs.Position);
end
set(fh, 'Position', fhs.Position);

% make previous current figure/axis the current one
if ~isempty(cf)
  figure(cf);
  if ~isempty(ca)
    axes(ca);
  end
end
