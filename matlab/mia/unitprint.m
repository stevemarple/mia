function r = unitprint(d, units, varargin)
%UNITPRINT  Print a value with its units, expressed as an SI multiplier.
%
%   r = UNITPRINT(d, units, ...)
%   r: CHAR representation (or CELL array of CHARs)
%   d: value (DOUBLE)
%   units: CHAR
%
%   Print a value with the units, e.g. 10000m as 10km. If d is not a
%   scalar the return value is a CELL array of the same size as d.
%
%   The default behaviour may be modified with the following name/value
%   pairs:
%
%     'formatstr', CHAR
%     Use an alternative printf format string. Ensure format string only
%     attempts to print one numerical value.
%
%     'multiplier', DOUBLE
%     The LOG10 value of the multiplier to use (e.g., 3 for k(ilo))
%
%     'separator', CHAR
%     The separator character between the value and the units. Default is
%     a space (except when the units is a degree sign in which case the
%     default is no separator.
%
%   See also SPRINTF.

deprecated('Please use printunits');
r = printunits(d, units, varargin{:});
return
% ==================================

defaults.formatstr = '%2.1f';
defaults.multiplier = [];
defaults.separator =  ' ';
[defaults unvi] = interceptprop(varargin, defaults);

% if strcmp(i18n_entity('deg'), units) & nargin == 2
%   % if not setting anything use sensible defaults for degrees
%   defaults.separator = ''; % no separator for degrees
%   defaults.multiplier = 0;
% end

r = cell(size(d));
for n = 1:prod(size(d))
  if isempty(d(n))
    r{n} = '';
  else
    if isempty(defaults.multiplier)
      % multiplier (allow for floating point error)
      mul = floor(eps+log10(d(n)) / 3) * 3; 
    else    
      mul = defaults.multiplier;
    end
    mul_str = '';
    switch mul
     case -18
      mul_str = 'a'; % atto
      
     case -15
      mul_str = 'f'; % femto
     
     case -12
      mul_str = 'p'; % pico
     
     case -9
      mul_str = 'n'; % nano
      
     case -6
      mul_str = 'u'; % micro
      
     case -3
      mul_str = 'm'; % milli
      
      
     case 3
      mul_str = 'k'; % kilo
      
     case 6
      mul_str = 'M'; % mega
      
     case 9
      mul_str = 'G'; % giga
      
     case 12
      mul_str = 'T'; % tera
      
     case 15
      mul_str = 'P'; % peta
      
     case 18
      mul_str = 'E'; % exa
      
    end
    if isempty(mul_str)
      v = d(n);
    else
      v = d(n) / power(10, mul);
    end
    
    
    r{n} = sprintf([defaults.formatstr '%s%s%s'], ...
		   v, defaults.separator, mul_str, units);
    
  end
end

switch length(r)
 case 0
  r = '';
  
 case 1
  r = r{1};
end
