function [data, ts, h, settings] = plw2mat(filename)


if isempty(filename)
  error('filename not specified');
elseif isdir(filename)
  error(sprintf('%s is a directory', filename));
end
[fid, message] = fopen(filename, 'r', 'ieee-le');
if fid == -1
  error(sprintf('Could not open ''%s'': %s', filename, message));
end


h.header_bytes = fread(fid, 1, 'uint16');
h.signature = char(fread(fid, [1 40], 'char'));
h.version = fread(fid, 1, 'uint32');
h.no_of_parameters = fread(fid, 1, 'uint32');

% the documentation states 50, but 250 gives the correct results. Maybe
% the amount varies with the version number
% h.parameters = fread(fid, 50, 'uint16');
h.parameters = fread(fid, 250, 'uint16');

h.samples_no = fread(fid, 1, 'uint32');
h.no_of_samples = fread(fid, 1, 'uint32');
h.max_samples = fread(fid, 1, 'uint32');
h.interval = fread(fid, 1, 'uint32');
h.interval_units = fread(fid, 1, 'uint16');

switch h.interval_units
 case 0
  h.interval_units_str = 'fs';
 case 1
  h.interval_units_str = 'ps';
 case 2
  h.interval_units_str = 'ns';
 case 3
  h.interval_units_str = 'us';
 case 4
  h.interval_units_str = 'ms';
 case 5
  h.interval_units_str = 's';
 case 6
  h.interval_units_str = 'm';
 case 7
  h.interval_units_str = 'h';
 otherwise
  error(sprintf('unknown units (was %d)', h.interval_units));
end

h.trigger_sample = fread(fid, 1, 'uint32');
h.triggered = fread(fid, 1, 'uint16');

h.first_sample = fread(fid, 1, 'uint32');
h.sample_bytes = fread(fid, 1, 'uint32');
h.settings_bytes = fread(fid, 1, 'uint32');
h.start_date = fread(fid, 1, 'uint32'); % days since 1/1/1
h.start_time = fread(fid, 1, 'uint32'); % second of day

if exist('timestamp')
  % have a time class, so use it
  h.start_timestamp = timestamp([1 1 1 0 0 0]) + ...
      timespan(-1, 'd', h.start_date, 'd', h.start_time, 's');
end


h.minimum_time = fread(fid, 1, 'int32');
h.maximum_time = fread(fid, 1, 'int32');

h.notes = char(fread(fid, [1 200], 'char'));
h.current_time = fread(fid, 1, 'int32');
h.spare = fread(fid, 78, 'uint8');

% disp(sprintf('pos = %d   %x H', ftell(fid), ftell(fid)))

% jump past header
fseek(fid, h.header_bytes, -1);

if h.no_of_parameters < 1
  error(sprintf('incorrect number of parameters (was %d)', ...
		h.no_of_parameters));
end

ts = zeros(h.max_samples, 1);
data = zeros(h.max_samples, h.no_of_parameters);


% for n = 1:h.max_samples
for n = 1:h.no_of_samples
  [tmp1 count1] = fread(fid, 1, 'int32');
  [tmp2 count2] = fread(fid, h.no_of_parameters, 'float32');
  
  if count1 == 1 & count2 == h.no_of_parameters  & ~feof(fid)
    ts(n) = tmp1;
    data(n,:) = tmp2;
  else
    % some file error
    n = n - 1;
    ts = ts(1:n);
    data = data(1:n, :);
    break;    
  end
end


settings = 'not yet available';
% settings = char(fread(fid, inf, 'char')');
fclose(fid);


