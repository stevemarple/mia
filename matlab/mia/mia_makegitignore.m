function mia_makegitignore

cwd = pwd;
externals = fullfile(dirname(mia_basedir), 'links.txt');
fid = url_fopen(externals);

while ~feof(fid)
  s = fgetl(fid);
  if isempty(s) 
    continue;
  elseif s(1) == '#'
    continue;
  end

  f = split(char(9), s);
  
  wd = fullfile(mia_basedir, f{1});
  src = f{2};
  dest = f{3};
  
  fid2 = url_fopen(fullfile(wd, '.gitignore'), 'a');
  fprintf(fid2, '/%s\n', dest);
  fclose(fid2);
  
end
  
fclose(fid);
cd(cwd);
