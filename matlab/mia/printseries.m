function [s,r] = printseries(a, varargin)
%PRINTSERIES Print a number series in a more compact form
%   [s, r] = PRINTSERIES(a);
%   [s, r] = PRINTSERIES(a, tol);
%   [s, r] = PRINTSERIES(a, tol, delim);
%   [s, r] = PRINTSERIES(a, tol, delim, colon);
%   s: string
%   r: sequence data (see below)
%   a: matrix containing the series
%   tol: optional tolerance
%   delim: delimiter (default is a space)
%   colonstr: colon string (default is ':')
%
% Given a vector a = [1 2 3 4 5 6 7 10 11] the string '1:7 10:11' is
% returned. Given an empty matrix an empty char matrix is returned. To avoid
% broken series when the series contains non-integer steps the comparisions
% are made with a default tolerance of 1e-12. This default tolerance may be
% changed. A value of zero specifies exact comparisons. An empty tol matrix
% gives default tolerance. The delimiter printed between individual numbers
% and ranges maybe be alter, as may the string indicateing Matlab's COLON
% operator. colonstr may be a single CHAR or a [1x2], to alter how the 3
% argument form of COLON is output.
%
% PRINTSERIES will also return a second return value which is the
% sequence information as a 3 x n DOUBLE matrix. Each column gives
% the start, step and end values for the each component series.

defaultTolerance = 1e-12;
defaultDelim = ' ';
defaultColonstr = ':';

delim = defaultDelim;
colonstr = defaultColonstr;

tol = [];
delim = [];
colonstr = [];

switch nargin
  case 1
   ;
    
  case 2
   tol = varargin{1};
    
  case 3
   tol = varargin{1};
   delim = varargin{2};

 case 4
  tol = varargin{1};
  delim = varargin{2};
  colonstr = varargin{3};
 
 otherwise
    error('incorrect parameters');
end

if isempty(tol)
  tol = defaultTolerance;
end
if isempty(delim)
  delim = defaultDelim;
end
if isempty(colonstr)
  colonstr = defaultColonstr;
end
if length(colonstr) == 1
  colonstr(2) = colonstr(1);
end

delimLen = length(delim);

asz = prod(size(a));
switch asz
  case 0
    s = '';
    r = zeros(3, 0);
    return;
    
  case 1
    s = sprintf('%g', a(1));
    r = [a(1); 1; a(1)];
    return;
    
  case 2
    s = sprintf('%g%s%g', a(1), delim, a(2));
    r = [a(1); a(2)-a(1); a(2)];
    return;
    
  otherwise
    % long enough to be worth trying something smarter
    s = '';
    r = zeros(3, 0);
    first = 1; % first index value in any series
    last = 1;  % last index value in any series
    % if a(2) - a(1) == a(3) - a(2)
    if toleq((a(2) - a(1)), (a(3) - a(2)), tol)
      stepsize = a(2) - a(1);
    else
      stepsize = 1;
    end
    n = 2;
    % ----
    while n <= asz
      % if a(last) + stepsize ~= a(n)
      if ~toleq(a(last) + stepsize, a(n), tol) | toleq(stepsize, 0, tol)
	if first == last
	  s = sprintf('%s%s%g', s, delim, a(last));
	  r(:, end+1) = [a(last); 1; a(last)];
	elseif last - first == 1
	  % not much of a series!
	  s = sprintf('%s%s%g%s%g', s, delim, a(first), delim, a(last));
	  % r(:, end+1) = [a(first); a(last)-a(first); a(last)];
	  r(:, end+1) = [a(first); 1; a(first)];
	  r(:, end+1) = [a(last); 1; a(last)];
	% elseif stepsize == 1
	elseif toleq(stepsize, 1, tol)
	  % end of series - print it
	  s = sprintf('%s%s%g%s%g', s, delim, a(first), colonstr(1), a(last));
	  r(:, end+1) = [a(first); 1; a(last)];
	else
	  s = sprintf('%s%s%g%s%g%s%g', s, delim, ...
		      a(first), colonstr(1), stepsize, colonstr(2), a(last));
	  r(:, end+1) = [a(first); stepsize; a(last)];
	end
	first = n;
	last = n;
	if n < asz - 1
	  stepsize = a(n+1) - a(n);
	else
	  stepsize = 1;
	end
      else
	% series continues
	last = n;
      end
      n = n + 1;
    end
    % ----
    
    if first == last
      % not a series
      s = sprintf('%s%s%g', s, delim, a(n-1));
      r(:, end+1) = [a(n-1); 1; a(n-1)];
    elseif last - first == 1
      % not much of a series!
      s = sprintf('%s%s%g%s%g', s, delim, a(first), delim, a(last));
      % r(:, end+1) = [a(first); a(last)-a(first); a(last)];
      r(:, end+1) = [a(first); 1; a(first)];
      r(:, end+1) = [a(last); 1; a(last)];
	  
    else
      % print series
      % if stepsize == 1
      if toleq(stepsize, 1, tol)
	s = sprintf('%s%s%g%s%g', s, delim, a(first), colonstr(1), a(last));
	r(:, end+1) = [a(first); 1; a(last)];
      else
	s = sprintf('%s%s%g%s%g%s%g', s, delim, ...
		    a(first), colonstr(1), stepsize, colonstr(2), a(last));
      	r(:, end+1) = [a(first); stepsize; a(last)];
      end
    end
end

if strcmp(s(1:delimLen), delim)
  % trim leading whitespace (easy than trying to fix by adding special
  % cases!
  s = s((1+delimLen):length(s));
end
  
return;

% --------------
function r = toleq(a, b, tol)
if a > 0
  if a < (1 - tol) * b
    r = 0;
  elseif a > (1+tol) * b
    r = 0;
  else
    % equality within tolerance
    r = 1;
  end
else
  if a > (1 - tol) * b
    r = 0;
  elseif a < (1+tol) * b
    r = 0;
  else
    % equality within tolerance
    r = 1;
  end
end
return;
