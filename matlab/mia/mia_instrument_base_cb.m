function mia_instrument_base_cb
%MIA_INSTRUMENT_BASE_CB Callback to indicate which instrument when clicked.
%
%   MIA_INSTRUMENT_BASE_CB
%
% MIA_INSTRUMENT_BASE_CB is used when instruments are mapped, when the
% instrument marker is clicked its details are printed to the console.
%
% See also mia_instrument_base/MAP.

cbo = gcbo;
cbf = gcbf;

% if ~strcmp(get(cbf, 'SelectionType'), 'open')
  % Not double-click
  % return;
% end
ud = get(cbo, 'UserData');
disp(getcode(ud.mia.instrument));
