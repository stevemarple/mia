function r = defaultaxeslabelparams(varargin)
%DEFAULTAXESLABELPARAMS  Return default parameters for axes labels
%
%   r = DEFAULTAXESLABELPARAMS
%   r: struct of TEXT parameters (fieldnames all lower case)
%
% Default parameters for TEXT objects exist, but no specific parameters for
% AXES labels exist. This function attempts to provide a common and
% consistent method to obtain such values.
%
% See also AXES, GET, SET.

r.fontweight = 'bold';
r.fontsize = 14;

% add any additional values to the structure
for n = 1:2:length(varargin)
  r = setfield(r, lower(varargin{n}), varargin{n+1});
end

