%COLORSPEC  Color specification
%
%   Description 
%
%   ColorSpec is not a command; it refers to the three ways in which you
%   specify color in MATLAB:
%
%       RGB triple 
%       Short name 
%       Long name 
%
%   The short names and long names are MATLAB strings that specify one of
%   eight predefined colors. The RGB triple is a three-element row vector
%   whose elements specify the intensities of the red, green, and blue
%   components of the color; the intensities must be in the range [0 1]. The
%   following table lists the predefined colors and their RGB equivalents.
%
%     RGB Value  Short Name  Long Name
%     [1 1 0]    y           yellow
%     [1 0 1]    m           magenta 
%     [0 1 1]    c           cyan
%     [1 0 0]    r           red
%     [0 1 0]    g           green
%     [0 0 1]    b           blue
%     [1 1 1]    w           white
%     [0 0 0]    k           black
%
%   Remarks 
%
%   The eight predefined colors and any colors you specify as RGB values are
%   not part of a Figure's colormap, nor are they affected by changes to the
%   Figure's colormap. They are referred to as fixed colors, as opposed to
%   colormap colors.
%
%   Examples 
%
%   To change the background color of a Figure to green, specify the color
%   with a short name, a long name, or an RGB triple. These statements
%   generate equivalent results:
%
%       whitebg('g')
%       whitebg('green')
%       whitebg([0 1 0]);
%
%   You can use ColorSpec anywhere you need to define a color. For example,
%   this statement changes the Figure background color to pink:
%
%       set(gcf,'Color',[1 .4 .6])
%
%   See also BAR, BAR3, COLORDEF, COLORMAP, FILL, FILL3, WHITEBG

% Taken from the MathWorks html help - why isn't this available as a .m
% file for command-line help?
