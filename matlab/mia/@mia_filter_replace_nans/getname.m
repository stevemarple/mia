function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_REPLACE_NANS object
%
%   See also MIA_FILTER_REPLACE_NANS.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'replace NaNs';
  
 case 'u'
  r = 'REPLACE NaNs';
  
 case 'c' 
  r = 'replace NaNs';
 
 case 'C'
  r = 'Replace NaNs';
  
 otherwise
  error('unknown mode');
end

return
