function r = filter(fil, mia)
%FILTER  Replace data with a fixed value.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_FIXED object
%   mia: object derived from MIA_BASE
%
%   See also FILFIXED, FILFUNCBASE, IRISFILEBASE.

data = getdata(mia);
if ~any(isnan(data))
  % No nans, so no filtering needed.
  r = mia;
  return
end

% if using image data then may have a situation with just one image, and
% then the data is 2D, whereas it must be considered as 3D, therefore
% call GETDATASIZE to get the true size
dsz = getdatasize(mia);
sampletime = getsampletime(mia);

% convert the data to a set of independent time series (time as second
% subscript)
data = reshape(data, prod(dsz(1:(end-1))), dsz(end));

% Awkward hack to find out if it is valid to interpolate across the ends
canWrapAround = 1;
day = timespan(1,'d');
if isa(mia, 'rio_qdc_base')
  data = repmat(data, [1 3]);
  x = getcdfepochvalue([sampletime - siderealday, sampletime, ...
                      sampletime + siderealday]);
elseif isa(mia, 'mag_qdc') & getduration(mia) == day
  data = repmat(data, [1 3]);
  x = getcdfepochvalue([sampletime - day, sampletime, sampletime + day]);
else
  canWrapAround = 0;
  x = getcdfepochvalue(sampletime);
end

for n = 1:size(data, 1)
  nans = isnan(data(n,:));
  if any(nans) & ~all(nans)
    data(n, nans) = interp1(x(~nans), data(n, ~nans), x(nans), ...
			    fil.interpmethod);
  end
end

if canWrapAround
  % extract central third
  data = data(:, (dsz(end)+1):(2*dsz(end)));
end




% convert the data back to its original size and insert
data = reshape(data, dsz);
r = setdata(mia, data);

r = addprocessing(r, getprocessing(fil, mia));
