function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_FIXED object
%
%   See also mia_base/ADDPROCESSING

% other classes do need to know data. For consistency, this class should
% insist upon it even though it is not required
error(nargchk(2,2,nargin));

data = getdata(mia);
if ~any(isnan(data))
  r = '';
end

r = sprintf('NaNs replaced using %s interpolation', fil.interpmethod);

