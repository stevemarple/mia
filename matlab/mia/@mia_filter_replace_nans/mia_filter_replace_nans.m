function fil = mia_filter_replace_nans(varargin)
%MIA_FILTER_REPLACE_NANS  Constructor for MIA_FILTER_REPLACE_NANS class.
%
%   r = MIA_FILTER_REPLACE_NANS;
%   default constructor
%
%   r = MIA_FILTER_REPLACE_NANS(m)
%   m: replacement method
%
%   MIA_FILTER_REPLACE_NANS replaces NANs in the data by interpolating
%   across the gaps. The possible interpolation methods are given by
%   mia_filter_replace_nans/INTERPMETHODS.
%
%

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;
fil.interpmethod = '';



if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif nargin == 1 & ischar(varargin{1})
  fil.interpmethod = varargin{1};
  p = feval(parent);
  fil = class(fil, cls, p);

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end


if isempty(fil.interpmethod)
  fil.interpmethod = 'cubic';
end
  

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
