function varargout = interpmethods(ifil)

methods = {'linear', 'spline', 'cubic', 'nearest'};
varargout{1} = methods;

if nargout >= 2
  varargout{2} = find(strcmp(methods, ifil.interpmethod));
end

return;
