function r = miaaspectratio(x, y, units, varargin)
%MIAASPECTRATIO  Return the aspect ratio of the image
%
%   r = MIAASPECTRATIO(x, y, units)
%   r = MIAASPECTRATIO(x, y, units, t)
%   r: aspect ratio [1x2 DOUBLE]
%   x: X coordinates
%   y: Y coordinates
%   units: CHAR ('m', 'm_antenna', 'km', 'km_antenna', 'deg', 'aacgm')
%   t: time (TIMESTAMP)
%
%   The time (as TIMESTAMP) should be given if the units are 'aacgm', so
%   that the correct magnetic field model can be selected.
%
%   See AXES (PloxBoxAspectRatio).

x2 = [min(x(:)) max(x(:))];
y2 = [min(y(:)) max(y(:))];

switch char(units)
 case {'m' 'm_antenna' 'km' 'km_antenna' 'raw' 'beamplan'}
  r = [x2(2)-x2(1), y2(2)-y2(1)];
  
 case 'deg'
  % geographic, use M_MAP toolbox
  r(1) = m_lldist(x2, repmat(mean(y2), [1 2]));
  r(2) = m_lldist(repmat(mean(x2), [1 2]), y2);
  
 case 'aacgm'
  if length(varargin)
    year = getyear(varargin{1});
  else
    year = [];
  end
  [x3 y3] = cgm2geo('longitude', x2, ...
		    'latitude', y2, ...
		    'year', year);
  r(1) = m_lldist(x3, repmat(mean(y3), [1 2]));
  r(2) = m_lldist(repmat(mean(x3), [1 2]), y3);
 
 otherwise
  error('unknown coordinate system');
  
end


