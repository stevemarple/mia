function r = findspikes(fil, mia)
%FINDSPIKES  Return the location of spikes using Dave Milling's algorithm.
%
%   r = FINDSPIKES(fil, mia)
%   r: vector indicating the location of spikes
%   fil: MIA_FILTER_DKM_DESPIKE object
%   mia: object derived from MIA_BASE
%
%  
%
%   See also MIA_FILTER_DKM_DESPIKE, MIA_BASE.

if ~validdata(fil, mia)
  error(['Incompatible data format for ' getname(fil) ' filter']);
end



data = getdata(mia);
% cannot rely on size(data) because single images have size matrix 1x2,
% not 1x3
sz = getdatasize(mia);
ndims = getndims(mia);

t = fil.t;
if numel(t) == 1
  t = repmat(t, sz);
end

if ~isempty(fil.tmin)
  t(t < fil.tmin) = fil.tmin;
end
if ~isempty(fil.tmax)
  t(t > fil.tmax) = fil.tmax;
end


if ndims ~= 2
  % code below is aware of n-dimensional data, but what should we do
  % about correlating for spikes
  error(sprintf('do not know how to handle data with %d dimensions', ...
		ndims));
end

% r = zeros(ndims - 1, 0);
r2 = [];

% to handle a variable number of dimensions must use subsref (or subsref2
% for matlab 5.1 since subsref is just a dummy function)

s.type = '()';
s.subs = repmat({':'}, 1, ndims);
s1 = s;
s2 = s;

if matlabversioncmp('==', '5.1')
  sfunc = 'subsref2';
else
  sfunc = 'subsref';
end

% size of the data at a common time
framesz = sz;
framesz(end) = 1;
frameSamples = prod(framesz);

spikeSubs = cell(1, ndims-1);
% increment over time dimension
for k = 2:(sz(ndims)-2)
  s1.subs{end} = k;
  s2.subs{end} = k - 1;
  d1 = abs(feval(sfunc, data, s1) - feval(sfunc, data, s2));

  if 0 
  s1.subs{end} = k + 1;
  s2.subs{end} = k - 1;
  d2 = abs(feval(sfunc, data, s1) - feval(sfunc, data, s2));

  s1.subs{end} = k + 2;
  s2.subs{end} = k - 1;
  d3 = abs(feval(sfunc, data, s1) - feval(sfunc, data, s2));
  end
  
  % frame = feval(sfunc, data, s1); % all samples at one time
				  
  % Remember where the spikes are. See if first difference exceeds threshold
  % in more than one data point. For imagedata the method must not report
  % spikes twice if they have the same row and column as other spikes.
  s1.subs{end} = k;
  spikeInd = find(d1 > feval(sfunc, t, s1));

  if 0 
      if length(spikeInd) > 1
	[spikeSubs{:}] = ind2sub(framesz, spikeInd);
	tmp = zeros(ndims, length(spikeSubs{1}));
	for n = 1:length(spikeSubs)
	  tmp(n,:) = spikeSubs{n}
	end
	tmp(end, :) = k;
	% append to current location of spikes
	r = [r, tmp];
      end
  end
  if length(spikeInd) > 1
    r2 = [r2; spikeInd(:)+(frameSamples * (k-1))];
  end
end

r = logical(zeros(sz));
r(r2) = 1;
