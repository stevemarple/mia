function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_DKM_DESPIKE object
%
%   See also MIA_FILTER_DKM_DESPIKE, MIA_DESPIKE_BASE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'DKM despike';
  
 case 'u'
  r = 'DKM DESPIKE';
  
 case 'c'
  r = 'DKM despike';
 
 case 'C'
  r = 'DKM Despike';
 
 otherwise
  error('unknown mode');
end

return

