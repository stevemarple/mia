function s = char(fil)
%CHAR Convert MIA_FILTER_DKM_DESPIKE object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_DKM_DESPIKE object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

if isempty(fil.t)
  t = '';
elseif numel(fil.t) == 1
  t = sprintf('%g', fil.t);
else
  t = matrixinfo(fil.t)
end

if isempty(fil.tmin)
  tmin = '';
elseif numel(fil.tmin) == 1
  tmin = sprintf('%g', fil.tmin);
else
  tmin = matrixinfo(fil.tmin)
end

if isempty(fil.tmax)
  tmax = '';
elseif numel(fil.tmax) == 1
  tmax = sprintf('%g', fil.tmax);
else
  tmax = matrixinfo(fil.tmax)
end

s = sprintf(['%st             : %s\n' ...
	     'tmin          : %s\n' ...
	     'tmax          : %s\n'], ...
	    mia_despike_base_char(fil), t, tmin, tmax);

