function r = mia_ischar(varargin)
%MIA_ISCHAR  Modified version of ISCHAR, to accept multiple parameters.
%
%   r = MIA_ISCHAR(a, ...)
%   a: variable to test
%   r: LOGICAL
%
%   An extended version of the standard ISCHAR function, but accepting
%   multiple input parameters. The LENGTH of the returned matrix is
%   identical to the number of parameters supplied.
%
%   See also ISCHAR.

r = logical(zeros(size(varargin)));
for n = 1:prod(size(varargin))
  r(n) = ischar(varargin{n});
end
