function r = nonanisequal(a, b)
%NONANISEQUAL  Modified ISEQUAL, where NANs test equal to each other
%
%   r = NONANISEQUAL(a, b)
%   r: scalar value indicating if the matrices are equal
%   a: any matrix
%   b: any matrix
%
%   The IEEE Standard 754 defines a class of numbers known as NAN. The
%   standard declares that when NANs are tested compared against any number
%   the result is false, even when compared against itself. Thus NAN==NAN
%   is false. It also means that ISEQUAL([NAN], [NAN]) also returns
%   false, which is not always desirable. NONANISEQUAL([NAN], [NAN])
%   returns 1.
%
%   See also ISEQUAL NAN, ISNAN.

r = isequal(a, b) | ...
    (isequal(isnan(a), isnan(b)) & isequal(a(~isnan(a)), b(~isnan(b))));

