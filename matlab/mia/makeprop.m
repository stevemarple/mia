function r = makeprop(s)
%MAKEPROP  Make property list from a structure
%
%   r = MAKEPROP(s)
%   r: CELL vector
%   s: scalar STRUCT (or scalar object)
%
%   MAKEPROP converts a scalar STRUCT to a list of parameter name/value
%   pairs suitable for passing to a function. The number of return values
%   is twice the number of fieldnames in the STRUCT. It is an error for
%   the STRUCT s to be non-scalar. If s is an object it is silently
%   converted to a STRUCT.
%
%   Example:
%
%   args.linestyle = '-';
%   args.color = 'r';
%   args.marker = '+';
%   lineargs = makeprop(s);
%   line(lineargs{:});
%
%   See also INTERCEPTPROP, WARNIGNOREDPARAMETERS, STRUCT.

if length(s) ~= 1
  error('s must be a scalar');
elseif isobject(s)
  s = struct(s);
elseif ~isstruct(s)
  error('s must be a structure')
end

fn = fieldnames(s);
fnl = length(fn);
r = cell(1, 2*fnl);


for n = 1:fnl
  r{2*n -1} = fn{n};
  r{2*n} = getfield(s, fn{n});
end
