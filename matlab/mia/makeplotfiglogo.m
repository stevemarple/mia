function r = makeplotfiglogo(varargin)
%MAKEPLOTFIGLOGO  Default logo for MAKEPLOTFIG.
%
%   r = MAKEPLOTFIGLOGO(h)
%   r: handles of new UI objects
%   h: AXES handle
%
% MAKEPLOTFIGLOGO creates the default logo for MAKEPLOTFIG. The default
% logo may be changed by overloading this function (place a function
% called MAKEPLOTFIGLOGO in the local or MIA/custom directories).
%
%
%   r = MAKEPLOTFIGLOGO(fn)
%   r: old function name (CHAR or FUNCTION_HANDLE)
%   fn: CHAR or FUNCTION_HANDLE
%   
% Set the logo function which will be used by future calls to
% MAKEPLOTFIGLOGO. If fn is an empty string ('') then MAKEPLOTFIGLOGO
% will reset itself to its default logo (see DEFAULTMAKEPLOTFIGLOGO), if
% fn is the string 'none' then no logo will be used for future calls to
% MAKEPLOTFIGLOGO.
%
% See  MAKEPLOTFIG, DEFAULTMAKEPLOTFIGLOGO.

% There should be no need to customise this function. Copy
% defaultmakeplotfiglogo.m into your local or MIA/custom directory and edit
% that to return the name of the logo you wish to use.


persistent logoname;
if isempty(logoname)
  logoname = defaultmakeplotfiglogo;
  % no need to keep this function locked when using the default logo
  munlock;
end

if nargin == 0
  r = logoname;
elseif ischar(varargin{1}) | isa(varargin{1}, 'function_handle')
  r = logoname;
  logoname = varargin{1};
  if ~isempty(logoname)
    mlock;
  end
elseif ishandle(varargin{1})
  if isempty(logoname) | isequal(logoname, 'none')
    % don't add any logo
    r = [];
  else
    r = feval(logoname, varargin{:});
  end
else
  error('incorrect parameters');
end


