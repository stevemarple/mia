function [r, hasSign] = minimisesigndiff(a, b)
%MINIMISESIGNDIFF Helper function for DCADJUST.
%
%   [r, hasSign] = MINIMISESIGNDIFF(a, b);
%   r: error value for iteration routine
%   hasSign: always 1, inidicating the the error value may be + or -
%
%   MINIMISESIGNDIFF is a helper function for DCADJUST, and is called as
%   part of the iteration routine. The error value may be negative, thus
%   the error value indicates the magnitude and direction of the
%   error. Thus DCADJUST is not required to determine the direction in
%   which the iteration should proceed.
%
%   See also DCADJUST, MINIMISESTDDEV.

if prod(size(a)) ~= prod(size(b)) | min(size(a)) ~= 1 | min(size(b)) ~= 1
  error('a and b must be vectors');
end

e = sign(a - b);
r = sum(e(isfinite(e(:))));

hasSign = 1;


