function r = validdata_notimagedata(fil, data)
%VALIDDATA Test if the data can be filtered
%
%   A helper function for MIA_FILTER_BASE classes to indicate that all MIA
%   data types can be filtered except for image data (MIA_IMAGE_BASE). To
%   use this function create a relative symbolic link called
%   "validdata_notimagedata.m" in the class directory pointing to this
%   function.
%
%   See VALIDDATA.

r = ones(size(data)) & isa(data, 'mia_base') & ~isa(data, 'mia_image_base');


