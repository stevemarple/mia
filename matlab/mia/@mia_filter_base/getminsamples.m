function r = getminsamples(ifil)
%GETMINSAMPLES Get the minimum number of samples needed
%
%   r = GETMINSAMPLES(ifil)
%   r: integer value
%   ifil: IFILFUNCBASE object
%
%   See also IFILFUNCBASE.

r = 0;


