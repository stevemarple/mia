function [r1, r2] = getresidue(fil, varargin)
%GETRESIDUE Get the number elements which will not be processed.
%
%   r = GETRESIDUE(fil);
%   r = GETRESIDUE(fil, mia);
%
%   r: [1x2] vector, containing the maximum number of elements which may
%   not be filtered (for left and right ends respectively)
%   fil: MIA_FILTER_BASE object
%   mia: object derived from MIA_BASE
%
%   In certain circumstances the residual data elements which cannot
%   normally be filter may be able to be filtered (e.g. with an IRISQDC
%   object where the data 'wraps around'). If the MIA_BASE oject is passed
%   as an additional parameter then these circumstances can be identified
%   and a more accurate result returned.
%
%   See also MIA_FILTER_BASE.

%   A non-publicised option is to find the residue before wrap-around is
%   applied, this is needed when doing the wrap around
%
%   [r1 r2] = GETRESIDUE(fil, mia);
%
%   Where r1 is the residue after wrapping around, and r2 before

r1 = [0 0];
r2 = [0 0];

