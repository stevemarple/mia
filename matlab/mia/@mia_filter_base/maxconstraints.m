function r = maxconstraints(ifil)
%MAXCONSTRAINTS Get the maximum constraints for IFILFUNCBASE filter object
%
%   r = MAXCONSTRAINTS(ifil)
%   r: integer value
%   ifil: IFILFUNCBASE object
%
%   See also IFILFUNCBASE, MINCONSTRAINTS.

r = 0;


