function r = minconstraints(fil)
%MINCONSTRAINTS Get the minimum constraints for MIA_FILTER_BASE object.
%
%   r = MINCONSTRAINTS(fil)
%   r: integer value
%   fil: MIA_FILTER_BASE object
%
%   See also FILFUNCBASE, MAXCONSTRAINTS.

r = 0;


