function r = char(fil)
%CHAR Convert MIA_FILTER_BASE object to CHAR.
%
%   r = CHAR(fil)
%   r: CHAR representation
%   fil: MIA_FILTER_BASE object to be converted
%
%   See also strfun/CHAR.

% NB Make all objects derived from mia_base print a trailing newline
% character 

if length(fil) ~= 1
  r = matrixinfo(fil)
  return;
end

r = sprintf('name          : %s\n', getname(fil));


