function r = validdata(fil, data)
%VALIDDATA Test if the data can be filtered.
%
%   r = VALIDDATA(fil, mia)
%   r: boolean indicating if the data valid for the filtering operation
%   fil: MIA_FILTER_BASE object (or derived type)
%   mia: object derived from MIA_BASE
%
%   Generally only the data type is tested.
%
%   All MIA_FILTER_BASE classes should override VALIDDATA to indicate
%   valid data types. To accept all data see VALIDDATA_ALL.

[tmp mfname] = fileparts(mfilename);
error(sprintf('overload %s for class %s', mfname, class(fil)));

