function r = getname(fil)
%GETNAME Get the name of an MIA_FILTER_BASE filter object
%
%   r = GETNAME(fil)
%   r: CHAR matrix
%   fil: MIA_FILTER_BASE object
%
%   See also MIA_FILTER_BASE.

r = 'filter base class';

% no valid function found for the object passed here, if that class can't
% report what type of data it is can this one?
if ~strcmp(class(fil), 'mia_filter_base')
  error(sprintf('Derive getname function for class %s', class(fil)));
end
