function r = configure(fil, varargin)
%CONFIGURE  Configure MIA_FILTER_BASE objects (graphically).
%
%   r = CONFIGURE(fil, ...)
%   r: configured object
%   fil: MIA_FILTER_BASE object
% 
%   For some filters additional parameters may be accepted
%
%   NOTES
%
%   Use this function as a standard interface to all configuration
%   functions. Cannot have configuration functions as member functions of
%   class since the despatch argument must be an object, which makes
%   callbacks more tricky and cumbersome to arrange.
%
%   See also MIA_FILTER_BASE.

configname = [class(fil) '_cfg' ];

if exist(configname)
  r = feval(configname, 'init', fil, varargin{:});
else
  r = fil;
  errordlg('There are no configuration options for this filter.', ...
      'Error', 'modal');
end  
