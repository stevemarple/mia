function r = validdata_all(fil, data)
%VALIDDATA_ALL Test if the data can be filtered
%
%   A helper function for MIA_FILTER_BASE classes to indicate that all MIA
%   data types can be filtered. To use this function create a relative
%   symbolic link called "validdata.m" in the class directory pointing to
%   this function.
%
%   See VALIDDATA.

% r = ones(size(data));
r = ones(size(data)) & isa(data, 'mia_base');


