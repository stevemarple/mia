function fil = mia_filter_base(varargin)
%MIA_FILTER_BASE  Constructor for MIA_FILTER_BASE class.
%


[tmp cls] = fileparts(mfilename);
% NB: no parent


% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  fil = class(fil, cls);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
  warnignoredparameters(varargin{unvi(1:2:end)});

  fil = class(fil, cls);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;

