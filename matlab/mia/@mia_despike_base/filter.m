function r = filter(fil, mia, varargin)
%FILTER Filter data with a sliding average.
%
%   r = FILTER(fil, mia)
%   r = FILTER(fil, mia, 'spikes', spikes)
%   r: filtered data, of same type as mia
%   fil: MIA_DESPIKE_BASE object
%   mia: object derived from MIA_BASE
%
%   See also MIA_DESPIKE_BASE, MIA_BASE.

defaults.spikes = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.spikes)
  defaults.spikes = findspikes(fil, mia);
  userDefinedSpikeMap = 0;
  % cannot have MxNx1 matrices, but getdatasize will return such a size
  % if that is correct. Have to use size(getdata(mia)), not
  % getdatasize(mia) here otherwise will incorrectly report errors when
  % using multi-dimensional data and have only one time index
elseif ~isequal(size(defaults.spikes), size(getdata(mia)))
  defSz = size(defaults.spikes)
  dsz = size(getdata(mia))
  error('spike map incorrect size');
else
  defaults.spikes = logical(defaults.spikes);
    userDefinedSpikeMap = 1;
end


r = mia;

N = numel(mia);
if N ~= 1
  for n = 1:N
    [tmp mfname] = fileparts(mfilename);
    r(n) = feval(mfname, fil, mia(n));
  end
  return
end

if getndims(mia) ~= 2
  warning('%s currently only tested on 2D data', getname(fil));
end

blockSize = getblocksize(fil, mia);
if blockSize == 1
  disp(['window too small to filter data at ' char(getresolution(mia)) ...
	' resolution']);
  return;
end
  
% residue with and without possibility of wrap around
[residue1 residue2] = getresidue(fil, mia);
data = getdata(mia);

% WRONG! 
% originalsz = size(data) % single image would be 2D!!!!

% ensure single images have size vector [1x3]
originalsz = getdatasize(mia);

% convert to a 2D array (rows will be independent so should be safe)
datasz = [prod(originalsz(1:(end-1))) originalsz(end)];
data = reshape(data, datasz);

% do the same for the spike map
defaults.spikes = reshape(defaults.spikes, datasz);

if ~isequal(datasz, originalsz)
  % to make inserting the data easier later pretend we had 2D data from the
  % start
  r = setdata(r, data);
end

newdata = data;
interpX = 1:datasz(2);
for n = 1:datasz(1)
  spikeIdx = find(defaults.spikes(n, :));
  if ~isempty(spikeIdx)
    newdata(n, spikeIdx) = ...
      interp1(interpX(~defaults.spikes(n, :)), ...
	      data(n, ~defaults.spikes(n, :)), ...
	      interpX(spikeIdx), ...
	      fil.removalmethod);
    
    % handle spikes longer than desired
    if numel(fil.maximumspikelength) == 1 & isfinite(fil.maximumspikelength)
      [lsPos lsLen] = ...
	  runlength(defaults.spikes(n, :), fil.maximumspikelength + 1);
      for m = 1:length(lsPos)
	newdata(n, lsPos:(lsPos(m) + lsLen(m) - 1)) = nan;
      end
    end
    
  end
end



r = setdata(r, newdata, ':', (residue2(1)+1):(datasz(2)-residue2(2)));



if 0 & isequal(residue1, [0 0])
  % calculate indices for the data to be processed, taking extra taking
  % to be the new residue
  % data: [lastdata_for_new_residue lastdata | firstdata
  % firstdata_for_residue] 
  newdata = [];
  ind = [(datasz(2)-sum(residue2)+1):datasz(2)  1:sum(residue2)];
  newdata = feval(['sliding' fil.method], data(:,ind)', blockSize)';

  ind = [(datasz(2)-residue2(2)+1):datasz(2) 1:residue2(1)];
  r = setdata(r, newdata, ':', ind);
end

processing = getprocessing(fil, mia);
if userDefinedSpikeMap
  processing = [processing '(with user-defined spikemap)'];
end
r = addprocessing(r, processing);
  


if ~isequal(datasz, originalsz)
  % we reshaped the data - better put it back
  r = setdata(r, reshape(getdata(r), originalsz));
end
