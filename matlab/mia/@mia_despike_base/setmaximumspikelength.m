function r = setmaximumspikelength(fil, n)
%SETMAXIMUMSPIKELENGTH Set the maximum acceptable spike length (in sample)
%
%   r = SETMAXIMUMSPIKELENGTH(fil, n)
%   r: modified filter object(s)
%   fil: MIA_DESPIKE_BASE object
%   n: scalar or vector lengths
%
%   If fil is non-scalar then so must n.
%
%   See also mia_filter_base/GETMAXIMUMSPIKELENGTH


if ~isequal(size(fil), size(n))
  error('input sizes differ');
end

r = fil;
for m = 1:numel(fil)
  tmp = r(m);
  tmp.maximumspikelength(m) = n(m);
  r(m) = tmp;
end




