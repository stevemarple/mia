function r = getremovalmethod(fil)
%GETREMOVALMETHOD Get the method used to remove spikes.
%
%   r = GETREMOVALMETHOD(fil)
%   r: CHAR (or CELL of CHARs when fil no scalar)
%   fil: MIA_DESPIKE_BASE object
%
%   See also mia_filter_base/SETMAXIMUMSPIKELENGTH

if numel(fil) == 1
  r = fil.removalmethod;
else
  r = cell(size(fil));
  for n = 1:numel(fil)
    tmp = fil(n);
    r{n} = tmp.maximumspikelength;
  end
end


