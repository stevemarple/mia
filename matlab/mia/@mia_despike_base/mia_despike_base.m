function fil = mia_despike_base(varargin)
%MIA_DESPIKE_BASE  Constructor for MIA_DESPIKE_BASE class.
%
%   r = MIA_DESPIKE_BASE;
%   default constructor
%
%   r = MIA_FILTER_SLIDING_AVERAGE(...)
%   parameter name/value pair interface
%   The only parameter name accepted is 'removalmethod', which may be
%   'nan' to replace the spikes with NANs, or 'linear' for linear
%   interpolation.
%
% MIA_DESPIKE_BASE is a filter object (see MIA_FILTER_BASE) for despiking
% data. MIA_DESPIKE_BASE provides a FILTER method for interpolating over
% spikes (or inserting NANs). Derived classes provides alternativ methos
% to detect spikes.
%
% See also MIA_FILTER_BASE.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.removalmethod = 'linear';
fil.maximumspikelength = 2; % anything longer than this is converted to NaNs


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end


removal_methods = getremovalmethodlist(fil);
if ~any(strcmp(fil.removalmethod, removal_methods))
  error(sprintf('unknown removal method, valid methods are: %s', ...
		commify(removal_methods{:})))
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
