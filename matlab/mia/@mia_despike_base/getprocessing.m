function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_DESPIKE_BASE object
%
%   See also mia_filter_base/ADDPROCESSING

r = sprintf('filter %s applied with %s interpolation', ...
	    class(fil), fil.removalmethod);

