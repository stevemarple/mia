function r = getremovalmethodlist(fil)
%GETREMOVALMETHODLIST  Get a list of valid removal methods
%
%   r = GETREMOVALMETHODLIST(fil)
%   r: CELL array of valid methods
%   fil: MIA_DESPIKE_BASE object(s)
%
% See also GETREMOVALMETHOD.

r = {'nan' 'linear'};

