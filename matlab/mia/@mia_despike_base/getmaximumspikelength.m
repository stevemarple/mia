function r = getmaximumspikelength(fil)
%GETMAXIMUMSPIKELENGTH Get the maximum acceptable spike length (in sample)
%
%   r = GETMAXIMUMSPIKELENGTH(fil)
%   r: DOUBLE
%   fil: MIA_DESPIKE_BASE object
%
%   See also mia_filter_base/SETMAXIMUMSPIKELENGTH

r = zeros(size(fil));
for n = 1:numel(fil)
  tmp = fil(n);
  r(n) = tmp.maximumspikelength;
end



