function varargout = smallest(x, varargin)
%SMALLEST Return smallest value.
%
% Like MIN, but ignores the sign in the comparison, in the output the
% sign is preserved.
%
% See MIN for usage details.

if nargin == 2
  % Get the least value from two matrices. We could use min to do most of
  % the work but since the result matrix is the same size as the input
  % matrices we'd have just as much work to do to correct the sign as
  % doing the comparison here.
  y = varargin{1};

  % Convert scalar to matrix of appropriate size
  if numel(x) == 1 && numel(y) ~= 1
    x = repmat(x, size(y));
  end
  if numel(y) == 1 && numel(x) ~= 1
    y = repmat(y, size(y));
  end
  r = zeros(size(x));
  x_abs = abs(x);
  y_abs = abs(y);
  for n = 1:numel(x)
    if x_abs(n) <= y_abs(n)
      r(n) = x(n);
    else
      r(n) = y(n);
    end
  end
  varargout{1} = r;
  
else
  % Get the least value from one matrix. This can result in an output
  % matrix which is considerably smaller than the input. Try to use min
  % to do most of the work, then check the signs on the output matrix.
  sz = size(x);
  dims = ndims(x);
  
  if nargin == 1
    % compute first non-singleton dimension
    for n = 1:dims
      if sz(n) ~= 1
	dm = n;
	break;
      end
    end
  elseif nargin == 3
    if ~isempty(varargin{1})
      error('second input must be empty');
    end
    dm = varargin{2};
  else
    error('incorrect number of parameters');
  end
  
  [y idx] = min(abs(x), [], dm);
  idxsz = size(idx);
  % For every element in the idx matrix copy the corresponding element to
  % the result matrix
  for n = 1:numel(idx);
    % Get the subscripts for this element
    [subs{1:dims}] = ind2sub(idxsz, n);
    subs{dm} = idx(n);
    y(n) = x(sub2ind(sz, subs{:}));
  end
  varargout{1} = y;
  varargout{2} = idx;
  
end
