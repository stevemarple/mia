function r = matlabversion
%MATLABVERSION  Return the version of matlab as a DOUBLE.
%
%   r = MATLABVERSION
%   r: DOUBLE
%
%   VERSION returns the Matlab version number as a string (in a form
%   similar to "5.3.0.10183 (R11)", which is not suitable for testing the
%   version number. In the above case MATLABVERSION will return 5.3.
%
%   See also VERSION.

if isoctave
  error('Do not use matlabversion in octave!');
end

deprecated('Use matlabversioncmp instead of matlabversion');

persistent mver;
if isempty(mver)
  mver = sscanf(version, '%f',1);
end

r = mver;

