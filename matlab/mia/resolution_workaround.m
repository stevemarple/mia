function resolution_workaround(vararg, unvi)

defaults.resolution = [];
defaults = interceptprop(vararg(unvi), defaults);

global RESOLUTION_WORKAROUND;

if ~isempty(defaults.resolution)
  RESOLUTION_WORKAROUND = defaults.resolution;
  
  disp('==============================================================');
  disp(['resolution must be specified as a load option, ' ...
	'TEMPORARY workaround: setting global variable']);
  disp('==============================================================');
  
end

