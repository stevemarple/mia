function r = mia_despike_filters(varargin)
%MIA_DESPIKE_FILTERS  Return list of all valid filter types for removing spikes
%
%   r = MIA_DESPIKE_FILTERS
%   r: CELL array of class names
%

r = {'mia_filter_as_despike' ...
     'mia_filter_srm_despike' ...
     'mia_filter_dkm_despike' ...
     'mia_filter_chauvenet'};

if nargin >= 1
  % return only the names of filters which are appropriate for this data
  % type 
  tmp = r;
  r = {};
  for n = 1:length(tmp);
    filname = tmp{n};
    fil = feval(filname);
    if validdata(fil, varargin{1}) ~= 0
      r{end+1} = filname;
    end
  end
end
