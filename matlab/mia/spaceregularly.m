function r = spaceregularly(st, et, res, t, data, varargin)
%SPACEREGULARLY  Space timestamped data to regular intervals
%
%   r = SPACEREGULARLY(st, et, res, t, data)
%   r = SPACEREGULARLY(st, et, res, t, data, ...)
%   r: regularly-spaced data
%   st: start time for data (TIMESTAMP)
%   et: end time for data (TIMESTAMP)
%   res: resolution (interval) to use for spacing the data
%   t: sample times of the data
%   data: numeric data. Highest-order dimension used for variation with
%   time
%
% The following parameter name/value pairs can be used to modify the
% behaviour:
%
% 'ndims', NUMERIC
% The number of dimensions of the data. This can normally be calculated
% from the data, but for cases where only one sample is presented it
% cannot be calculated correctly.
%
% 'method', CHAR
% The method interp1 should use for its interpolation. See INTERP1 for
% details of valid methods.
%
% See also INTERP1.

defaults.method = ''; % defer, to allow '' to select default method
defaults.ndims = ndims(data);

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.method)
  defaults.method = 'linear';
end

if defaults.ndims > 2
  % pretend data is only 2D
  origsz = size(data);
  data = reshape(data, prod(sz(1:(end-1)), sz(end)));
end


% check if any samples are spaced by >= 2 * resolution. Insert missing
% samples if true
badspacing = find(diff(t) >= 2 * res);

if isempty(badspacing)
  t2 = t;
  data2 = data;
else
  t2 = repmat(timestamp('bad'), 1, numel(t) + numel(badspacing));
  data2 = repmat(nan, size(data, 1), size(data, 2) + numel(badspacing));
  last = 1;
  for n = 1:numel(badspacing)
    % copy block
    t2([last:badspacing(n)] + (n-1)) = t(last:badspacing(n));
    data2(:, [last:badspacing(n)] + (n-1)) = data(:, last:badspacing(n));
    last = badspacing(n) + 1;
    
    % insert fake missing sample (except after last sample)
    if n ~= numel(t2)
      t2(badspacing(n) + n) = mean(t([0:1] + badspacing(n)));
      % data already nan, no need to insert that
    end
  end
  
  
  % copy from after the last bad sample to the end
  t2((badspacing(n) + n + 1):end) = t((badspacing(n)+1):end);
  data2(:, (badspacing(n) + n + 1):end) = data(:, (badspacing(n)+1):end);

end

% interpolate between samples onto desired spacing
data = zeros(size(data2, 1), (et-st)./res);
for n = size(data, 1)
  data(n, :) = interp1(getcdfepochvalue(t2), data2, ...
		       getcdfepochvalue(st:res:(et-res)), ...
		       defaults.method);
end
  

% extrapolate at the ends, but only if the time difference is less than the
% resolution
if t2(1) < st + res
  for n = size(data, 1)
    data(n, 1) = interp1e(getcdfepochvalue(t2), data2, ...
			  getcdfepochvalue(st), defaults.method);
  end
end

if t2(end) > et - res
  for n = size(data, 1)
    data(n, end) = interp1e(getcdfepochvalue(t2), data2, ...
			    getcdfepochvalue(et), defaults.method);
  end
end


if defaults.ndims > 2
  % return data to original shape
  data = reshape(data, origsz);
end

r = data;
