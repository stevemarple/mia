function [r,sr,s] = extract(mia, varargin)
%EXTRACT Extract a subset of data as another TEMP_DATA object
%
%   See mia_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

[r,sr,s] = mia_base_extract(mia, varargin{:});

if ~isequal(sr.subs{1}, ':')
  comps = getsensors(r);
  r = setsensors(r, comps(sr.subs{1}));
end


