function r = char(mia, varargin)
%CHAR  Convert a MAG_DATA object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

r = sprintf(['%s' ...
	     'sensors           : %s\n'], ...
 	    mia_base_char(mia, varargin{:}), printseries(mia.sensors));


