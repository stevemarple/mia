function [plottitle, windowtitle] = maketitle(rio, varargin)
%MAKETITLE  Make plot and figure titles.
%
%   [plottitle, windowtitle] = maketitle(rio)
%   [plottitle, windowtitle] = maketitle(rio, ...)
%
%   Create the titles based on current time/date, data type etc. Various
%   values may be overriden by specifying a parameter name and value (see
%   INTERCEPTPROP). The defaults values are created from the IRISFILEBASE
%   object (or an object derived from IRISFILEBASE). The valid parameters
%   which may be overriden are described below:
%
%     'style', style
%        For IMAGEDATA objects the default title style is 'image'. This adds
%        the word 'image' or 'images' to the result of
%        GETNAME(rio). Otherwise the default title style is 'line', which
%        instead prints the sensors. 
%
%     'customstring', string
%        Observe chosen style but override the string printed in
%        bold. Useful for Keogram plots and similar.
%
%     'starttime', TIMESTAMP
%
%     'endtime', TIMESTAMP
%
%     'resolution', TIMESPAN
%     'resolution', CHAR
%        If the resolution is given as a TIMESPAN then it is converted to
%        a suitable string. If it is a CHAR array then it is printed
%        without change.   
%
%     'location', LOCATION
%
%     'sensors', [sensors]
%        The default sensors are obtain from the IRISFILEBASE object. The
%        sensors are not printed for the 'image' style, otherwise the sensors
%        are printed in compressed format by PRINTSERIES.
%
%     'step', step
%        For image plots using only 1 out of n images, step should be set to
%        n.
%
%     'flipaxes', str
%        The orientation of the images. The string should be one of those
%        returned by MAKEPLOTFIG('flipaxesnames');
%
%     'comment', comment
%        An optional comment, to be printed at the bottom of the title
%
%   See also IRISFILEBASE, IMAGEDATA, MAKEPLOTFIG, GETNAME, PRINTSERIES.

plottitle = '';
windowtitle = '';

% general format is
% Power/time (sensor 1)
% 00:00:00 - 12:00:00 UT 1/1/1999 @ 1 m res.
% Kilpisjarvi, Finland (69.05N, 20.79E)
% [comment]
%
% If step ~= 1 then print that and resolution on separate line to time
% formatStr = '{\\bf %s} %s\n%s%s\n%s%s';
formatStr = { ...
      '{\\bf %s} %s' ...
      '%s%s' ...
      '%s%s'
  };

% entries are:
styleStr = ''; % 'Power/time images', 'Absorption/time', etc
sensorStr = '';
locStr = '';
dateStr = '';
resStr = '' ; % including step size if appropriate

instrument = getinstrument(rio);
loc = getlocation(instrument(1));

% default values

% alias parameters and sensors
defaults.sensors  = getsensors(rio(1));
defaults.parameters = defaults.sensors;

defaults.starttime = getstarttime(rio(1));
defaults.endtime = getendtime(rio(1));
defaults.resolution = [];
defaults.location = loc;
defaults.step = 1;
defaults.style = 'line';
defaults.customstring = '';
defaults.comment = '';
defaults.flipaxes = '';

if nargin > 1
  [defaults unvi] = interceptprop(varargin, defaults, ...
				  {'sensors' 'parameters'});
end


% create normal version of strings
switch length(defaults.sensors)
 case 0
  sensorStr = '';
 case  1
  sensorStr = ['sensor ' int2str(defaults.sensors)];
 otherwise
  sensorStr = ['sensors ' printseries(defaults.sensors)];
end

locStr = char(defaults.location);
dateStr = dateprintf(defaults.starttime, defaults.endtime);
if isempty(defaults.resolution)
  [res resMesg] = getresolution(rio);
  if isempty(resMesg)
    resStr = [' @ ' char(res, 'c') ' res.'];
  else
    resStr = '';
  end
elseif ischar(defaults.resolution)
  resStr = defaults.resolution;
else
  resStr = [' @ ' char(defaults.resolution, 'c') ' res.'];
end

% modify standard formmating if not appropriate
if strcmp(defaults.style, 'line')
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    styleStr = gettype(rio, 'c');
  end
  windowtitle = [styleStr ': ' sensorStr];
  
elseif strcmp(defaults.style, 'image')
  % handle singular / plural properly
  if (defaults.endtime - defaults.starttime) / ...
	(defaults.resolution * defaults.step) > 1
    % multiple images plotted
    plural = 's';
  else
    % single image plotted
    plural = '';
  end
   if ~isempty(defaults.customstring)
     styleStr = defaults.customstring;
   else
     styleStr = [getname(rio, 'c') ' image' plural];
   end
  
  % % sensor information is not important
  % sensorStr = '';
  % put image flipaxes information into sensorStr, since we have no
  % other use for sensorStr and to print into the same place sensorStr would
  % appear 
  sensorStr = defaults.flipaxes;
  
  if defaults.step > 1
    % print step information too
    resStr = sprintf('\nresolution %s, plot interval %s', ...
	char(defaults.resolution), ...
	char(defaults.resolution * defaults.step, 'c')); 
  end
  windowtitle = styleStr;
else
  warning('unknown style');
  plottitle = '';
  windowtitle = '';
  return;
end

if ~isempty(defaults.comment)
  % put comment on separate line
  defaults.comment = sprintf('\n%s', defaults.comment);
end

if ~isempty(sensorStr)
  sensorStr = ['(' sensorStr ')'];
end

plottitle = { ...
      sprintf(formatStr{1}, styleStr, sensorStr) ...
      sprintf(formatStr{2}, dateStr, resStr)};

if length(rio) == 1
  plottitle{end+1} = sprintf(formatStr{3}, locStr, defaults.comment);
end
