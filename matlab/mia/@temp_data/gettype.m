function r = gettype(mia, varargin)
%GETTYPE  Return temperature data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: TEMP_DATA object
%
%   See also TEMP_DATA.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'temperature';
  
 case 'u'
  r = 'TEMPERATURE';
  
 case {'c' 'C'}
  r = 'Temperature';
 
 otherwise
  error('unknown mode');
end

return

