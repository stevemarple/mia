function r = getsensorindex(mia, sensors)
%GETSENSORINDEX  Get indices of sensors in a TEMP_DATA object data matrix
%
%   r = GETSENSORINDEX(mia, sensors)
%
%   r: the row indices of the sensors in data matrix
%   mia: a TEMP_DATA object
%   sens: a cell array of strings being the names of the sensors
%
%   See also GETSENSORS.


num = numel(sensors);
r = zeros(num,1);
if isempty(mia.sensors)
  return;
end

for n = 1:num
  % where this sensor number occurs in mia.sensors 
  ind = find(mia.sensors == sensors(n));
  switch numel(ind)
   case 0
    % not present
    r(n) = 0; 
   
   case 1
    r(n) = ind;
    
   otherwise
    error(sprintf('sensor id #%d occurs %d times in object', ...
                  sensors(n), numel(ind)));
  end
end


