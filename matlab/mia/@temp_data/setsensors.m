function r = setsensors(mia, sens)
%SETSENSORS  Set the sensors of a TEMP_DATA object
%
%   r = SETSENSORS(mia, sens)
%
%   r: updated TEMP_DATA object
%   mia: TEMP_DATA object
%   sens: cell array of strings being the sensor names

r = mia;
r.sensors = sens;


