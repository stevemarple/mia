function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  Insert TEMP_DATA object into another TEMP_DATA object.
%
%   r = INSERT(a, b)
%

[r aa ar ba br rs] = mia_base_insert(a, b);

r = setsensors(r, rs.sensors);

