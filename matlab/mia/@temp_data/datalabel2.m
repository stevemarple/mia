function [r, m] = datalabel2(mia, varargin)


units = getunits(mia);
% state frequency for absorption

% Don't allow automatic choice of multiplier
m = 0;

if isempty(units)
  r = gettype(mia, 'c');
else
  r = sprintf('%s (%s)', gettype(mia, 'c'), units);
end


