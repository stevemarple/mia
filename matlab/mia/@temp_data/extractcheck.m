function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  EXTRACTCHECK for TEMP_DATA.
%
% See mia_base/EXTRACTCHECK.

if length(varargin) == 1
  % subscript interface
  [sr s] = mia_base_extractcheck(mia, varargin{:});
  return
end  


% parameter name/value interface

% accept sensors and parameters as aliases
defaults.sensors = [];
defaults.parameters = defaults.sensors;

[defaults unvi] = interceptprop(varargin, defaults, ...
				{'sensors', 'parameters'});

[sr s] = mia_base_extractcheck(mia, varargin{unvi});

if ~isempty(defaults.sensors)
  % ensure sensors are unique. Also ensure order of sensors is
  % preserved
  
  uniqsensors = unique(defaults.sensors);
  ci = sort(getsensorindex(mia, uniqsensors));
  if any(ci == 0)
    error(sprintf('sensor(s) %s do not exist in this object', ...
		  printseries(uniqsensors(ci == 0))));
  end
  if strcmp(sr.subs{1}, ':')
    sr.subs{1} = ci;
  else
    sr.subs{1} = intersect(sr.subs{1}, ci);
  end
end

