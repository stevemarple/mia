function nd=getndims(mia)

if ~isa(mia,'temp_data')
  error('Panic! temp_data/getndims called with non-temp_data object!')
end

nd=2;

