function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for TEMP_DATA data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mia_base_insertcheck(a, b);


sensors_a = getsensors(a);
sensors_b = getsensors(b);
% rs.sensors = unique([sensors_a sensors_b]);
rs.sensors = [sensors_a setdiff(sensors_b, sensors_a)];

% now modify the row details for subsasgn to include all different comps
[tmp aa.subs{1}] = ismember(sensors_a, rs.sensors);
[tmp ba.subs{1}] = ismember(sensors_b, rs.sensors);

rs.datasize(1) = numel(rs.sensors);

