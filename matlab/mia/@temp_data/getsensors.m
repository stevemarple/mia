function r = getsensors(mia)
%GETSENSORS  Return the parameters (components) in a TEMP_DATA object
%
%   r = GETSENSORS(mia)
%   r: CELL array of parameters
%   mia: TEMP_DATA object

if length(mia) ~= 1
  error('temp_data object must be scalar');
end

r = mia.sensors;
