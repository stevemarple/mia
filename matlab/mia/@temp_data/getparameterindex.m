function r = getparameterindex(mia, p)
%GETPARAMETERINDEX  Get row indices of parameters.
%
%   See also GETSENSORINDEX.

r = getsensorindex(mia, p);
