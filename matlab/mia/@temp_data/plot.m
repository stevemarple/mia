function [fh,gh,ph] = plot(mia, varargin)

fh = [];
gh = [];
ph = [];

% accept sensors and parameters as aliases
defaults.sensors = [];
defaults.parameters = defaults.sensors;
[defaults unvi] = interceptprop(varargin, defaults, ...
                                {'sensors', 'parameters'});

if isempty(defaults.sensors)
  defaults.sensors = getsensors(mia);
end
% Ensure sensors are unique and sorted
defaults.sensors = unique(defaults.sensors);

[fh gh ph] = ...
    mia_base_plot(mia, ...
                  varargin{unvi}, ...
                  'parameters', defaults.sensors);

return

[fh gh ph] = ...
    mia_base_plot(extract(mia, 'sensors', defaults.sensors), ...
                  varargin{unvi});

