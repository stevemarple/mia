function r = getparametername(mia)
%GETPARAMETERNAME  Return the name of the parameter in the data.
%
%   r = GETPARAMETERNAME(mia)
%   r: [1x2] CELL array, singular and plural names
%  
% For temperature data this is always {'sensor' 'sensors'}
%
% See also GETPARAMETERS, GETPARAMETERINDEX.

r = {'sensor' 'sensors'};
