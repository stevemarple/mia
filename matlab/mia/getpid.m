function r = getpid
%GETPID Return the process ID for the Matlab process
%
%   r = GETPID
%
%   GETPID will probably return incorrect results on non-unix systems. A
%   warning is issued in such cases.
%
%   See also UNIX.

if ~isunix
  warning([mfilename ' will probably be incorrect for non-unix' ...
        ' systems']);
end

[status,result] = unix('echo $PPID');
r = str2num(result);
if status ~= 0 | r == 0
  r = -1;
end
