function varargout = dcadjust(data, refData, varargin)
%DCADJUST  Adjust data up or down to match reference data
%
%   r = DCADJUST(data, refdata, ...)
%   [r, err] = DCADJUST(data, refdata, ...)
%  
%   r: adjusted data
%   err: the adjustment made to the data
%   data: input data
%   reference data
%   The following name/value parameter pairs are also accepted:
%
%   'tolerance', [1x1 DOUBLE]
%   Stop iterating when the step size is within this amount.
%
%   'maximumiterations', [1x1 DOUBLE]
%   The maximum number of iterations to make. If no solution is found
%   after the maximum number of iterations have been made DCADJUST stops
%   with an error message.
%
%   'errorfunction', CHAR
%   The name of the function to use for calculating the error
%   value. Valid functions include MINIMISESTDDEV and
%   MINIMISESIGNDIFF. It is not necessary for the error function to
%   return a signed error. See MINIMISESTDDEV and MINIMISESIGNDIFF for
%   more details about this aspect.
%
%   See also MINIMISESTDDEV, MINIMISESIGNDIFF.

if ~isequal(size(data), size(refData))
  size(data)
  size(refData)
  error('data and ref data not the same size');
end

% variables relating to number of iteration steps
defaults.tolerance = 0.001;  % time to stop iterating
defaults.maximumiterations = 50; % maximum number of iterations
defaults.errorfunction = 'minimisestddev';

% debugging options
defaults.verbose = 0;
defaults.figure = [];
defaults.plotintermediateresults = 0;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

defaults.tolerance = abs(defaults.tolerance); % MUST be +ve

if ~isempty(defaults.figure)
  figure(defaults.figure);
end


% Check for cases which mean no adjustment can be made
if all(isnan(data) | isnan(refData))
  if defaults.verbose
    disp([mfilename ': no common samples which are not NaN']);
  end
  varargout{1} = data;
  varargout{2} = nan;
  return
end

% calculate the initial starting values to iterate between
e = data - refData;

data1 = data - min(e(:)); % upper limit
data2 = data - max(e(:)); % lower limit

if ~isempty(defaults.figure)
  % plot graph showing limits
  plot(data, 'r'); 
  hold on; 
  plot(refData, 'b');
  plot(data1, 'g');
  plot(data2, 'k');
end

firstDataNonNaNIdx = find(~isnan(data));
firstDataNonNaNIdx = firstDataNonNaNIdx(1);

iterations = 0;
while 1
  iterations = iterations + 1;
  [e1 hasSign] = feval(defaults.errorfunction, data1, refData);
  [e2 hasSign] = feval(defaults.errorfunction, data2, refData);
  
  % difference = data1(1) - data2(1)
  difference = data1(firstDataNonNaNIdx) - data2(firstDataNonNaNIdx);
  testData = data1 - 0.5*difference; % go halfway in between
  [testError hasSign] = feval(defaults.errorfunction, testData, refData);
  
  if testError == 0 
    if defaults.verbose
      disp('on target!');
    end
    break;
  elseif abs(difference) < defaults.tolerance
    if defaults.verbose
      disp('close enough');
    end
    % but which do we use
    if abs(e1) < min(abs(e2), abs(testError))
      % data1 is closest
      testData = data1;
      testError = e1;
    elseif abs(e2) < abs(testError)
      % data2 is closest
      testData = data2;
      testError = e2;
    else
      % fTest is closest
    end
    break;
    
  else
    % too high or low
    if ~hasSign
      % compute the direction to move in, indicate this by setting the
      % sign of testError accordingly
      [testErrorPlusSigma tmp] = feval(defaults.errorfunction, ...
	  testData + 0.5*defaults.tolerance, refData); 
      
      if testError > testErrorPlusSigma
	testError = -testError; % too low
      elseif testError < testErrorPlusSigma
	% too high, so leave testError as +ve
      else
	% testError == testErrorPlusSigma
	% probably ought to loop, trying a bigger stepsize, for upto
	% say 10 tries. Too lazy to program it.
	error('Cannot find tell direction to move. Fix me.');
      end
    end
    
    if testError > 0
      % too high, go halfway between testData and data2
      data1 = testData;
    else
      % too low,  go halfway between fTest and data1
      data2 = testData;
    end
    
  end
  
  if ~isempty(defaults.figure) & defaults.plotintermediateresults
    plot(testData, 'y'); % plot intermediate lines
  end

  
  if iterations >= defaults.maximumiterations
    figure
    plot(data);
    error(sprintf('No solution after maximum number of iterations (%d)', ...
	defaults.maximumiterations));
  end
end

if defaults.verbose
  disp(sprintf('number of iterations: %d', iterations));
end
if ~isempty(defaults.figure)
  plot(testData, 'm');
end

varargout{1} = testData;
% Use the first finite value to deduce error
varargout{2} = testData(firstDataNonNaNIdx) - data(firstDataNonNaNIdx);

