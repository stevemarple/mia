function [r,c] = chauvenet(s, varargin)
%CHAUVENET  Use Chauvenet's criterion to remove outliers in a dataset.
%
%   [r,c] = CHAUVENET(s)
%   [r,c] = CHAUVENET(s, n)
%   r: modified version of s with outliers removed
%   c: count of number of outliers removed
%   s: data set (NUMERIC)
%   n: maximum number of deletions to make
%
% CHAUVENET use Chauvenet's criterion to remove outliers from a dataset
% (replacing them with NAN). If n is not specified it defaults to INF. r is
% a matrix the same size as s. Any nans in s are ignored.
%
% See "An Introduction to Error Analysis" by John Taylor (1982) for a
% description of Chauvenet's criterion.
%
% See also STD, MEAN, NANSTD, NANMEAN.

r = s;
c = 0;
r = reshape(s, 1, numel(s));

if length(varargin) == 0
  maxdeletions = inf;
else
  maxdeletions = varargin{1};
end

for n = 1:maxdeletions
  % Calculate standard deviation and mean, ignore nans.
  stddev = nanstd(r);
  mn = nanmean(r);
  
  % Find out which sample is worst, and where it is. Note that the effect of
  % abs is to put worstsample's value above the mean (but same distance from
  % it).
  [distfrommean worstsampleidx] = max(abs(r - mn));
  
  % For a sample n*stddev from the mean how likely is it it could have
  % occurred? (Allow for it occuring on either side of mean, hence 2x in
  % equation below)
  prob = 2 * normcdf(- distfrommean / stddev);
  
  % Account for number of samples remaining in the set
  prob = prob * sum(~isnan(r));
  if prob < 0.5
    % delete the worst sample
    r(worstsampleidx) = nan;
    c = c + 1;
  else
    % no samples deleted, stop processing
    break
  end
end
  
r = reshape(r, size(s));
