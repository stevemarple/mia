function r = mia_filters(varargin)
%MIA_FILTERS  Return list of all valid MIA filter types
%
%   r = MIA_FILTERS
%   r: CELL array of class names
%

r = {'mia_filter_fixed' 'mia_filter_sliding_average' ...
    'mia_filter_interp' 'mia_filter_samnet' 'mia_filter_qdc_mean_sd' ...
     'mia_filter_replace_nans' 'mia_filter_chauvenet' ...
     'mia_filter_sgolay'};

if nargin >= 1
  % return only the names of filters which are appropriate for this data
  % type 
  tmp = r;
  r = {};
  for n = 1:length(tmp);
    filname = tmp{n};
    fil = feval(filname);
    if validdata(fil, varargin{1}) ~= 0
      r{end+1} = filname;
    end
  end
end
