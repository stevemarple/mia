function r = cyclicdiff(x, varargin)
%CYCLICDIFF Difference and appropriate derivative for cyclic values.
%
% CYCLICDIFF(x, n, dim, cyclic)
% cyclic: [1x2 DOUBLE], lower, upper values
%
% When called without the cyclic argument CYCLICDIFF operates exactly
% like DIFF. When the cyclic argument is given CYCLICDIFF ensures that
% the result r wraps around so that lower >= r < upper. For values in
% radians cyclic may be given as either [-pi pi] or [0 pi]. For values in
% degrees appropriate values may be [-180 180] or [0 360].
%
% See also DIFF.

n = 1;

% compute first non-singleton dimension
sz = size(x);
for m = 1:numel(sz)
  if sz(m) ~= 1
    dm = m;
    break;
  end
end


cyclic = [];

if length(varargin) >= 1
  n = varargin{1};
end
if length(varargin) >= 2
  if ~isempty(varargin{2})
    dm = varargin{2};
  end
end
if length(varargin) >= 3
  cyclic = varargin{3};
end

r = diff(x, n, dm);

if ~isempty(cyclic)
  range = (diff(cyclic));
  a = r;
  b = r - range;

  a = rem(a, range);
  b = rem(b, range);
  r = smallest(a, b);
  r = rem(r, range);
  r(r < cyclic(1)) = r(r < cyclic(1)) + range;
  r(r > cyclic(2)) = r(r > cyclic(2)) - range;
end
