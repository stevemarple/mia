function r = defaultmakeplotfiglogo
%DEFAULTMAKEPLOTFIGLOGO The default logo used by MAKEPLOTFIGLOGO.
%
%   r = DEFAULTMAKEPLOTFIGLOGO
%   r: CHAR or FUNCTION_HANDLE
%
% The default is to not have a logo, r is 'none'. Sites should override
% this function by creating an appropriate version in their 'local'
% directory, or users may override this function by creating a copy in
% their 'MIA/custom' directory.
%
% See also MAKEPLOTFIGLOGO, MAKEPLOTFIG.

r = 'none';


