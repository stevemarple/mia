function r = mia_datadir
%MIA_DATADIR  Return the base directory where the MIA data is stored.
%
%   r = MIA_DATADIR;
%
%   See also MIA_BASEDIR, MIA_SUMMARYPLOTDIR.

r = getenv('MIA_DATADIR');
if ~isempty(r)
  return
end

% Indicate possible directories
if isunix
  d = {'/data'};

elseif ispc
  d = {}
  
else
  error('computer system unknown to MIA');
end

% Test the possible directories
for n = 1:length(d)
  r = d{n};
  if exist(r, 'dir')
    return; % directory found
  end
end
error('cannot find MIA data directory');
  
