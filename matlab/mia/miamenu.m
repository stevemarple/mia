function h = miamenu(fh, varargin)
%MIAMENU  Add a standard menu for MIA onto a figure.
%
%   h = MIAMENU(fh)
%   h = MIAMENU(fh, 'help', helpinfo)
%   fh: FIGURE handle
%   helpinfo: n x 3 CELL array. 1st column is help text, 2nd Callback,
%   3rd Tag.
%
%   This function adds standard menus onto a FIGURE. Unused menus are
%   invisible, and act as placeholders for where any subsequent submenus
%   should be. The top-level menus added (and their tags) are 'File'
%   ('file'), 'Edit', ('edit') 'Tools' ('tools'), 'Views' ('viewmenutop'),
%   'Help', ('help').
%
%   See also MIAHELP, FIGURE, UIMENU.

defaults.help = []; % extra item to add onto help menu

% For later. If needed...
% Have to get unvi but we don't need it here
[defaults unvi] = interceptprop(varargin, defaults);

if 0 
  % temporary while testing adding to std matlab menus
  set(findall(fh, 'Type', 'uimenu', 'HandleVisibility', 'on'));
else
  delete(findall(fh, 'Type', 'uimenu'));
end

% Add some menus

% === File ===
fileMenuHandle = uimenu(fh, 'Label', '&File', ...
    'Tag', 'file');

newMenuHandle = uimenu(fileMenuHandle, 'Label', '&New', ...
    'Tag', 'file_new');

% --- new ---
uimenu(newMenuHandle, 'Label', 'Riometer data', ...
       'Callback', 'newriodata init;');

uimenu(newMenuHandle, 'Label', 'ASC data', ...
       'Callback', 'disp(sprintf(''\r\aCamera data not yet available''));');

% -----------
uimenu(fileMenuHandle, 'Label', '&Open...', ...
    'Callback', 'mialoaddlg(''init'');', ...
    'Tag', 'file_open');

uimenu(fileMenuHandle, 'Label', '&Save', ...
    'Visible', 'off', ...
    'Tag', 'file_save');

uimenu(fileMenuHandle, 'Label', '&Export', ...
    'Visible', 'off', ...
    'Tag', 'file_export');

% ......
% uimenu(fileMenuHandle, 'Label', 'Page setup...', ...
%    'Separator', 'on', ...
%    'Visible', 'off', ...
%    'Tag', 'file_page');

uimenu(fileMenuHandle, 'Label', '&Print...', ...
    'Separator', 'on', ...
    'Callback', 'printdlg(gcbf);', ...
    'Visible', 'off', ...
    'Tag', 'file_print');

% ......
uimenu(fileMenuHandle, 'Label', '&Close', ...
    'Callback', 'close(gcbf);', ...
    'Separator', 'on', ...
    'Tag', 'file_close');
    
uimenu(fileMenuHandle, 'Label', '&Exit', ...
    'Callback', 'close(gcbf);', ...
    'Visible', 'off', ...
    'Tag', 'file_exit');

% === Edit ===
% add invisible menu to act as a place holder. Trust(!) other functions
% to search for one before creating another!
uimenu(fh, 'Label', '&Edit', ...
    'Visible', 'off', ...
    'Tag', 'edit')


% === Tools ===
% add invisible menu to act as a place holder. Trust(!) other functions
% to search for one before creating another!
uimenu(fh, 'Label', '&Tools', ...
    'Visible', 'off', ...
    'Tag', 'tools')

% === Options ===
% add invisible menu to act as a place holder. Trust(!) other functions
% to search for one before creating another!
uimenu(fh, 'Label', '&Options', ...
    'Visible', 'off', ...
    'Tag', 'options')


% === Views ===
% add invisible menu to act as a place holder (see VIEWMENU for info).
% DO NOT change this tag - it will break VIEWMENU.

% test if one exists first
vmh = findobj(fh, 'Tag', 'viewmenutop');
if isempty(vmh)
  vmh = uimenu(fh, 'Label', '&Views', ...
      'Visible', 'off', ...
      'Tag', 'viewmenutop');
else
  set(vmh, 'Visible', 'on');
end

% === Help ===
helpMenuHandle = uimenu(fh, 'Label', '&Help', ...
    'Tag', 'help');

for n = 1:size(defaults.help,1)
  uimenu(helpMenuHandle, 'Label', defaults.help{n,1}, ...
      'Callback', defaults.help{n,2}, ...
      'Tag', defaults.help{n,3});
end

if size(defaults.help,1)
  sep = 'on';
else
  sep ='off';
end

uimenu(helpMenuHandle, 'Label', 'Contents', ...
    'Separator', sep, ...
    'Callback', 'miahelp(''helpcontents.html'')', ...
    'Tag', 'help_contents');

% Add link to Matlab help
[status host] = unix('/bin/uname -n');
host = sscanf(host, '%s');  % get rid of whitespace, newlines etc
if strcmp(host, 'mango')
  matlabHelpUrl = 'http://localhost/matlab/';
else
  matlabHelpUrl = 'http://www.dcs.lancs.ac.uk/matlab/';
end
uimenu(helpMenuHandle, 'Label', 'Matlab help', ...
    'Callback', ['web(''' matlabHelpUrl ''');'], ...
    'Tag', 'help_matlabhelp');

uimenu(helpMenuHandle, 'Label', 'About MIA', ...
    'Callback', 'web(''http://www.dcs.lancs.ac.uk/iono/iris/'')', ...
    'Tag', 'help_aboutmia');











