function fil = mia_filter_as_despike(varargin)
%MIA_DESPIKE_BASE  Constructor for MIA_FILTER_AS_DESPIKE class.
%
%   r = MIA_FILTER_AS_DESPIKE;
%   default constructor
%
%   r = MIA_FILTER_AS_DESPIKE(...)
%   parameter name/value pair interface
%   The only parameter name accepted is 'removalmethod', which may be
%   'nan' to replace the spikes with NANs, or 'linear' for linear
%   interpolation.
%
% MIA_FILTER_AS_DESPIKE is a filter object (see MIA_DESPIKE_BASE) for
% despiking data.
%
% See also MIA_DESPIKE_BASE, MIA_FILTER_BASE.

[tmp cls] = fileparts(mfilename);
parent = 'mia_despike_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = 1; % integer

% 1 means no other spikes required
fil.minimumcorrelation = 2;
fil.threshold = 4;
fil.minimumspike = 0.4e-9;
fil.windowsize = 4;         % catches single and double spikes

% window size of the median function used to measure local deviation
fil.medianwindowsize = 61;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
