function r = getblocksize(fil, mia)
%GETBLOCKSIZE Find the block size used by a MIA_FILTER_DKM_DESPIKE filter.
%
%   r = GETBLOCKSIZE(fil, mia);
%   
%   r: scalar
%   fil: MIA_FILTER_DKM_DESPIKE object
%   mia: object derived from MIA_BASE

r = fil.medianwindowsize;

