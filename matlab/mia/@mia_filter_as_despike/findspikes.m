function r = findspikes(fil, mia)
%FINDSPIKES  Return the location of spikes using Andrew Senior's algorithm.
%
%   r = FINDSPIKES(fil, mia)
%   r: vector indicating the location of spikes
%   fil: MIA_FILTER_AS_DESPIKE object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_AS_DESPIKE, MIA_DESPIKE_BASE.

% adapted from code by Andrew Senior

if ~validdata(fil, mia)
  error(['Incompatible data format for ' getname(fil) ' filter']);
end


data = getdata(mia);
% cannot rely on size(data) because single images have size matrix 1x2,
% not 1x3
sz = getdatasize(mia);
ndims = getndims(mia);


if ndims ~= 2
  error(sprintf('do not know how to handle data with %d dimensions', ...
		ndims));
end

s = logical(zeros(sz));

for n = 1:sz(1)
  x = data(n, :);
  
  m = slidingmedianfilt1(x, fil.windowsize);

  % absolute residuals
  residuals = abs(x - m);
  
  % median filter the residuals with a larger window size to estimate
  % the "local variability" (use medianwindowsize)
  d = slidingmedianfilt1(residuals, fil.medianwindowsize);
  
  % test residuals for significance
  t = fil.threshold * d;
  t(t < fil.minimumspike) = fil.minimumspike;

  s(n, :) = (residuals > t);
end

% cannot detect spikes in the first few samples
s(:, 1:(fil.windowsize-1)) = logical(0);

% find spikes which correlate against other spikes
spikes = sum(s) >= fil.minimumcorrelation;

% output only spikes which have the desired number of correlations
% r = find(repmat(spikes, sz(1), 1) & s);
r = logical(repmat(spikes, sz(1), 1) & s);


