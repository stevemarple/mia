function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_AS_DESPIKE object
%
%   See also MIA_FILTER_AS_DESPIKE, MIA_DESPIKE_BASE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'AS despike';
  
 case 'u'
  r = 'AS DESPIKE';
  
 case 'c'
  r = 'AS despike';
 
 case 'C'
  r = 'AS Despike';
 
 otherwise
  error('unknown mode');
end

return

