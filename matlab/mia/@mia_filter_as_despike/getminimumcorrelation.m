function r = getminimumcorrelation(fil)
%GETMINIMUMCORRELATION  Return the minimum number of correlations needed
%
%   r = GETMINIMUMCORRELATION(fil)
%   r: DOUBLE
%   fil: MIA_FILTER_AS_DESPIKE object
%
% If fil is non-scalar then r will be too.
%
% See also MIA_FILTER_AS_DESPIKE.

r = zeros(size(fil));
for n = 1:numel(fil)
  tmp = fil(n);
  r(n) = tmp.minimumcorrelation;
end

