function r = getminimumspike(fil)
%GETMINIMUMSPIKE  Return the minimum spike required.
%
%   r = GETMINIMUMSPIKE(fil)
%   r: DOUBLE
%   fil: MIA_FILTER_AS_DESPIKE object
%
% If fil is non-scalar then r will be too.
%
% See also MIA_FILTER_AS_DESPIKE.

r = zeros(size(fil));
for n = 1:numel(fil)
  tmp = fil(n);
  r(n) = tmp.minimumspike;
end


