function r = round2(a, b, varargin)
%ROUND2  Like ROUND, but round to nearest multiple of b.
%
%   r = ROUND2(a, b)
%
%   r: matrix of same size as a
%   a: matrix to be rounded
%   b: scalar, or matrix the same size as a, defining the boundary to
%   round to.
%
%   See also ROUND, FLOOR2, CEIL2.

if prod(size(b)) == 1 & prod(size(a)) ~= 1
  b = repmat(b, size(a));
elseif ~isequal(size(a), size(b))
  error('b must be a scalar or a matrix the same size as a');
end

if length(varargin) == 0
  r = round(a ./ b) .* b;
else
  c = varargin{1};
  r = (round((a .* c) ./ b) .* b)/c;
end
