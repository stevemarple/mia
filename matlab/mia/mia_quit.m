function mia_quit(r)
%MIA_QUIT Quit MIA and return an exit status to the shell.
%
%   MIA_QUIT(r)
%   r: INTEGER, in range 0-255
%
% MIA_QUIT writes the return value into a file and then calls Matlab's QUIT
% function. The shell script responsible for starting MIA check's the file
% and returns the numeric value found in it to the shell.
%
% See also MIA_EXIT, QUIT.

if ~isnumeric(r)
  warning(sprintf('%s expects a scalar numeric argument, was %s', ...
		  matrixinfo(r)));
  r = 1;
end

if numel(r) ~= 1
    warning(sprintf('%s expects a scalar numeric argument, was %s', ...
		  matrixinfo(r)));
    r = 1;
end

exitfile = fullfile(tempdir, 'mia_exit_code.txt');
[fid mesg] = url_fopen(exitfile, 'w');

if isempty(mesg)
  % Write exit code to the file
  fprintf(fid, '%d\n', floor(abs(r)));
  fclose(fid);
else
  warning(sprintf('Could not write exit code to %s: %s', ...
		  exitfile, mesg));
end		  

quit

