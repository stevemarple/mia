function r = getqdc(fil)
%GETQDC Get the RIO_(RAW)QDC_MEAN_SD object used for filtering.
%
%   r = GETQDC(fil)
%   r: RIO_(RAW)QDC_MEAN_SD object
%   fil: MIA_FILTER_QDC_MEAN_SD object
%

r = fil.qdc;

