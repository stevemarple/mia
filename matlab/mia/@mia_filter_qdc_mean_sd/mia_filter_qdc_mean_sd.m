function fil = mia_filter_qdc_mean_sd(varargin)
%MIA_FILTER_QDC_MEAN_SD  Constructor for MIA_FILTER_QDC_MEAN_SD class.
%
%   r = MIA_FILTER_QDC_MEAN_SD;
%   default constructor
%
%   r = MIA_FILTER_QDC_MEAN_SD(v)
%   v: replacement value
%
%   r = MIA_FILTER_QDC_MEAN_SD(...)
%   parameter name/value pair interface
%
%   The following parameter name/value pairs are accepted:
%
%     'qdc', RIO_QDC_MEAN_SD
%     The quiet day curve mean and standard deviation values, as a
%     RIO_QDC_MEAN_SD object. If not specified (or empty) then the most
%     appropriate object is loaded automatically from the data archive.
%
%     'upperlim', DOUBLE
%     'lowerlim', DOUBLE
%     The upper and lower limits for which data is accepted, defined in
%     terms of the number of standard deviations away from the mean
%     QDC. Note that unless the 'dcadjustmethod' parameter is empty the
%     reference RIO_QDC_MEAN_SD object is first adjusted to the data
%     object (to compensate for changes in sensitivity over time, snow
%     levels, solar-controlled absorption etc.)
%
%
%     'dcadjustmethod', CHAR
%     The name of the function used when 'DC-adjusting' the reference
%     RIO_QDC_MEAN_SD object to the data. See DCADJUST for more details.
%
%   Any unknown parameters are passed onto the constructor for the parent
%   class (MIA_FILTER_BASE).
%
%   MIA_FILTER_QDC_MEAN_SD filters data so that any data points outside
%   the the upperlim and lowerlim points (determined as a number of
%   standard deviations away from the DC-adjusted mean value) are
%   replaced with NAN. Therefor MIA_FILTER_QDC_MEAN_SD provides a method
%   to filter data based on a priori information.
%
%  See also MIA_FILTER_BASE, mia_filter_qdc_mean_sd/FILTER.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = 1; % integer

fil.qdc = [];      % rio_qdc_mean_sd object
fil.upperlim = 2;  % upper limit = mean + ifil.upperlim * std.dev.
fil.lowerlim = -2; % lower limit = mean - ifil.lowerlim * std.dev.
fil.dcadjustmethod = 'minimisestddev';

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end


% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;

if fil.lowerlim >= fil.upperlim
  error('lower limit is not less than upper limit');
end
