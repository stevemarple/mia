function r = getdcadjustmethod(fil)
%GETDCADJUSTMETHOD Return the method used for DC adjustment
%
%   r = GETDCADJUSTMETHOD(fil)
%   r = (fil)
%
%   See also MIA_FILTER_QDC_MEAN_SD, RIO_QDC_MEAN_SD.

if length(fil) == 1
  r = fil.dcadjustmethod;
else
  r = cell(size(fil));
  for n = 1:numel(fil)
    tmp = fil(tmp);
    r{n} = tmp.dcadjustmethod;
  end
end
