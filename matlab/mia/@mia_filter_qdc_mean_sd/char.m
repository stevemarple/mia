function s = char(fil)
%CHAR Convert MIA_FILTER_QDC_MEAN_SD object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_QDC_MEAN_SD object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

if isempty(fil.dcadjustmethod)
  method = '(none)';
else
  method = char(fil.dcadjustmethod);
end

s = sprintf(['%s' ...
	     'lower limit   : %+g std. dev.\n' ...
	     'upper limit   : %+g std. dev.\n' ...
	     'DC adj method : %s\n' ...
	     'QDC sd.       : %s\n'], ...
	    mia_filter_base_char(fil), fil.lowerlim, ...
	    fil.upperlim, method, char(fil.qdc));

