function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_SLIDING_AVERAGE object
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'QDC mean/std. dev.';
  
 case 'u'
  r = 'QDC MEAN/STD. DEV.';
  
 case 'c'
  r = 'QDC mean/std. dev.';
 
 case 'C'
  r = 'QDC Mean/Std. Dev.';
 
 otherwise
  error('unknown mode');
end

return

