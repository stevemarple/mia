function r = filter(fil, mia)
%FILTER filter data to QDC mean +/- n standard deviations
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: FILFIXED object
%   mia: object derived from RIO_BASE
%
%   See also MIA_FILTER_BASE, RIO_BASE.

if ~validdata(fil, mia)
  error(['Incompatible data format for ' getname(fil) ' filter']);
end
  
st = getstarttime(mia);
et = getendtime(mia);
beams = getbeams(mia);

if isempty(fil.qdc)
  % load an appropriate one given the start time and inform this takes
  % time. Best to load one before if possible.
  
  if isa(mia, 'rio_rawqdc') | isa(mia, 'rio_rawpower')
    
    disp('No rio_rawqdc_mean_sd object supplied, loading one. Please wait');
    fil.qdc = rio_rawqdc_mean_sd('time', st, ...
				 'instrument', getinstrument(mia), ...
				 'beams', getbeams(mia));
  elseif isa(mia, 'rio_qdc') | isa(mia, 'rio_power')
    disp('No rio_qdc_mean_sd object supplied, loading one. Please wait');
    fil.qdc = rio_qdc_mean_sd('time', st, ...
			      'instrument', getinstrument(mia), ...
			      'beams', getbeams(mia));
  else
    error(['No QDC object in filter object, and don''t know how to load' ...
	   ' one']);
  end
else
  % for beams use intersection of beams which are present. This allows
  % objects to be only partly filtered, which is faster.
  beams = intersect(beams, getbeams(fil.qdc));  
end

r = mia;

if isempty(beams)
  warning('no common beams found - filtering not performed');
  return;
end

% beam indexes in two data set will differ
miaBi = getparameterindex(mia, beams);
qdcBi = getparameterindex(fil.qdc, beams);


% if data longer than one sidereal day, extend
blocks = ceil((et - st) / siderealday);
startSample = round(stoffset(st, getlocation(getinstrument(mia))) / ...
		    getresolution(mia)) + 1;
endSample = startSample + getdatasize(mia,2) - 1;

% work beam-by-beam to save on memory useage
for n = 1:length(beams)
  % mn = getdata(fil.qdc, qdcBi(n), ':');
  % stddev = getstddev(fil.qdc, qdcBi(n), ':');
  if isa(mia, 'rio_power') | isa(mia, 'rio_rawpower')
    [pwr stddevobj] = align(extract(fil.qdc, 'beams', beams(n)), mia);
    % [fh1 gh1] = plot(mia); plot(pwr, 'plotaxes', gh1, 'color', 'r')
    mn = getdata(pwr);
    stddev = getdata(stddevobj);

    blocks = 1;
    startSample = 1;
    endSample = length(mn);
  else
    qdc2 = extract(fil.qdc, beams(n));
    mn = getdata(qdc2);
    stddev = getstddev(qdc2);
  end
  
  mn2 = repmat(mn, 1, blocks);
  stddev2 = repmat(stddev, 1, blocks);
  % if still too short (might wrap over sidereal midnight)
  if endSample > size(mn2, 2)
    % disp('extending')
    mn2 = [mn2 mn];
    stddev2 = [stddev2 stddev];
  end
  
  data = getdata(mia, miaBi(n), ':');
  
  fh = [];
  if 0
    fh = figure;
    figure(fh);
    drawnow;
    plot(data, 'r'); % RED - data
    hold on;
    plot(mn2(startSample:endSample), 'b'); % BLUE - original mean
  end
  
  % if necessary perform DC adjustment
  if ~isempty(fil.dcadjustmethod) 
    mn2(startSample:endSample) = ...
	dcadjust(mn2(startSample:endSample), data, ...
		 'errorfunction', fil.dcadjustmethod);
    if ~isempty(fh)
      plot(mn2(startSample:endSample), 'g'); % GREEN - adjusted mean
    end
  end

  samples = startSample:endSample;
  mn2 = mn2(samples);
  stddev2 = stddev2(samples);

  % lower limit
  limit = mn2 + fil.lowerlim * stddev2;
  data(find(data < limit)) = nan;
  
  if ~isempty(fh)
    whos
    plot(limit, 'y'); % YELLOW - stddev mean
  end

  % upper limit
  limit = mn2 + fil.upperlim * stddev2;
  data(find(data > limit)) = nan;
  % ===========
  
  if ~isempty(fh)
    plot(limit, 'y'); % YELLOW - stddev mean
  end
  
  r = setdata(r, data, miaBi(n), ':');
end

r = addprocessing(r, getprocessing(fil, mia));
% plotall(mia, r);



