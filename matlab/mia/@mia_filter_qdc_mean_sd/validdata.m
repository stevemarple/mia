function r = validdata(fil, varargin)
%VALIDDATA Test if the data can be filtered
%
%   r = VALIDDATA(fil, data)
%   fil: MIA_FILTER_QDC_MEAN_SD object
%   data: data object
%
% Only RIO_POWER objects can always be filtered. RIO_RAWPOWER objects can
% be  filtered if the QDC object is of type RIO_RAWQDC_MEAN_SD
%
%   See VALIDDATA.

if length(fil) ~= 1
  % could be vectorised, but isn't
  error('filter object must be scalar');
end

r = zeros(size(varargin));

for n = 1:length(varargin)
  if isa(varargin{n}, 'rio_qdc') | isa(varargin{n}, 'rio_power')
    r(n) = 1;
  
  elseif isa(varargin{n}, 'rio_rawqdc') | isa(varargin{n}, 'rio_rawpower') 
    r(n) = isa(fil.qdc, 'rio_rawqdc')
  
  elseif isa(varargin{n}, 'rio_image')
    warning(['filtering of rio_images is not implemented, but probably' ... 
	     ' could be']);
    r(n) = 0
  else
    r(n) = 0;
    
  end
end
  
