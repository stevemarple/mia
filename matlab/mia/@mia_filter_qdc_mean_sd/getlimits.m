function r = getlimits(fil, varargin)
%GETLIMITS Return the upper and/or lower limits from a MIA_FILTER_QDC_MEAN_SD.
%
%   r = GETLIMITS(fil)
%   r = GETLIMITS(fil, n)
%   r: limit(s)
%   fil: MIA_FILTER_QDC_MEAN_SD object
%   n: 1 for lower limit, 2 for upper limit
%
%   If n is not supplied the lower and upper limits, respectively, are
%   returned are returned in a 1x2 vector. If n is 1 only the lower limit
%   is returned, if n is 2 the upper limit is returned. The limits are
%   in terms of the standard deviation, i.e., [1 2] is mean - 1 standard
%   deviation to mean + 2 standard deviations.
%
%   See also MIA_FILTER_QDC_MEAN_SD, RIO_QDC_MEAN_SD.

if nargin == 1
  r = [fil.lowerlim fil.upperlim];

elseif nargin == 2 & varargin{1} == 1
  r = fil.lowerlim;

elseif nargin == 2 & varargin{1} == 2
  r = fil.upperlim;

else
  error('incorrect parameters');
end

