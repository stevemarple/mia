function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil)
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_QDC_MEAN_SD object
%
%   See also mia_base/ADDPROCESSING


% second argument is the data we filtered
if ~isempty(fil.qdc)
  beams = intersect(getbeams(fil.qdc), getbeams(mia));
else
  % would have loaded data for all beams
  beams = getbeams(mia);
end
if length(beams) == 1
  plural = '';
else
  plural = 's';
end
ser = printseries(beams);
dcadj = '';
if ~isempty(fil.dcadjustmethod)
  dcadj = [' with DC adjust method ' fil.dcadjustmethod];
end
r = sprintf('Filtered to QDC %+g/%+g std. dev. for beam%s %s%s', ...
    fil.upperlim, fil.lowerlim, plural, ser, dcadj);

