function r = rebin(a, sz)
%REBIN  Modelled on IDL function of same name

if numel(a) ~= length(a)
  error('must be a vector');
end


len = length(a);
X = 1:len;

if len == sz
  r = a;
  return;
end

invscale = len ./ sz;
scale = sz / len;
if sz > len
  if scale ~= fix(scale)
    error('result dimensions must be integer factor of original dimensions');
  end
  X2 = 1:invscale:len;
  if length(X2) < sz
    X2((end+1):sz) = X2(end);
  end
else
  if invscale ~= fix(invscale)
    error('result dimensions must be integer factor of original dimensions');
  end
  X2 = 1.5:invscale:len;
end

r = interp1(X, a, X2, 'linear');

return

len
X2start = X2(1)
X2end = X2(end)
X2(end-10:end)
invscale
