function r = getparent(obj)
%GETPARENT  Return the parent of an object. 
%
%   r = GETPARENT(obj)
%   r: parent object
%   obj: an OBJECT
%
% If the object has mutliple parent r is a CELL object and each element
% is a parent OBJECT. An ERROR results if obj is not a derived object.
%
% See also GETANCESTORS.


if ~isobject(obj)
  error('not an object');
elseif length(obj) > 1
  % assume all objects have the same parent(s)
  error('object must be scalar');
end

% parent is the last field
s = struct(obj);
fn = fieldnames(s);
r = getfield(s, fn{end});

r = {};

% take advantage of the fact parents are placed last in the structure
for n = length(fn):-1:1
  if isa(obj, fn{n})
    % r{end+1} = fn{n};
    r{end+1} = getfield(s, fn{n});
  else
    break;
  end
end

switch length(r)
 case 0
  error('not a derived object');
  
 case 1
  r = r{1};
end
