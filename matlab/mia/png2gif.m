function png2gif(pngfile, giffile)
%PNG2GIF Convert a PNG format image to GIF format.
%
%   PNG2GIF(pngfile, giffile);
%   pngfile: filename of PNG image
%   giffile: new filename for GIF image
%

switch computer 
 case {'LNX86' 'GLNX86'}
  % unix(['pngtopnm ' pngfile '| ppmtogif > ' giffile]);
  unix(['convert ' pngfile ' gif:' giffile]);
  
 otherwise
  error(['Do not know how to convert PNGs to GIF. Please add for ' ...
	 computer ' to ' mfilename]);
end
