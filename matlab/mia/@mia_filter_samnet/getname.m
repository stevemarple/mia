function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_SAMNET object
%
%   See also MIA_FILTER_FIXED.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case {'l' 'u' 'c' 'C'}
  r = 'SAMNET';
  
 otherwise
  error('unknown mode');
end

return
