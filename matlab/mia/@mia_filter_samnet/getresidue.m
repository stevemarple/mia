function [r1, r2] = getresidue(fil, mia)
%GETRESIDUE Get the number elements which will not be processed.
%
% See mia_filter_base/GETRESIDUE.

res = getresolution(mia);
res_s = gettotalseconds(res);
f_nyquist = 1 ./ (2 * res_s); % in Hz
t_short = gettotalseconds(fil.short);
t_long = gettotalseconds(fil.long);
filter_terms = 3 * f_nyquist * max([t_short t_long]);


r1 = repmat(fix(filter_terms), [1 2]);
r2 = r1;
