function r = getshortperiod(fil)
%GETSHORTPERIOD  Return short period used by a MIA_FILTER_SAMNET filter
%
%   r = GETSHORTPERIOD(fil);
%   r: TIMESPAN object
%   fil: MIA_FILTER_SAMNET object
%
%   See also MIA_FILTER_SAMNET.

if length(fil) == 1
  r = fil.short;
else
  r = timespan(zeros(size(fil)), 's');
  for n = 1:prod(size(fil))
    r(n) = fil(n).short;
  end
end
