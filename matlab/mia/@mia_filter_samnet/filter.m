function r = filter(fil, mia)
%FILTER  Filter using similar algorithm previously used at University of York
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_SAMNET object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_SAMNET

if length(mia) ~= 1
  r = repmat(mia(1), size(mia));
  for n = 1:numel(mia)
    r(n) = feval(mfilename, fil, mia(n));
  end
  return
end

  
if ~validdata(fil, mia)
  error(['Incompatible data format for ' getname(fil) ' filter']);
end

orig_sz = getdatasize(mia);
orig_ndims = getndims(mia);

data = getdata(mia);
if orig_ndims == 2
  sz = orig_sz;
  ndims = orig_ndims;

else
  % reshape to pretend it is 2d data.Time variation is last dimension
  sz =[prod(orig_sz(1:(end-1)))];
  ndims = 2;
  data = reshape(data, sz);
end

% now can pretend data is always 2D.

res = getresolution(mia);
res_s = gettotalseconds(res);
f_nyquist = 1 ./ (2 * res_s); % in Hz
t_short = gettotalseconds(fil.short);
t_long = gettotalseconds(fil.long);

if t_short > 2 * res_s
  f_high = 1 / (f_nyquist * t_short);
else
  f_high = 1;
end

if t_long > 2 * res_s
  f_low = 1 / (f_nyquist * t_long);
else
  f_low = 0;
end

gibbs = 76.8345;

filter_terms = fix(3 * f_nyquist * max([t_short t_long]));

coeffs = idl_digital_filter(f_low, f_high, gibbs, filter_terms);
nc = length(coeffs);

rdata = fliplr(data); % reverse in time
fdata = filter(coeffs, 1, data, [], 2);     % filtered data
frdata = filter(coeffs, 1, rdata, [], 2);   % filtered reversed data

data2 = repmat(feval(class(data), nan), size(data));
% data2 = data; % copy to have same class as data

% copy forward-filtered data
data2(:, 1:(end-filter_terms)) = fdata(:, (filter_terms+1):end);

% insert filtered reversed data at start (reverse back!)
tmp = frdata(:, (end-nc+1):end);
data2(:, 1:nc) = fliplr(tmp);


 data2(:, 1:filter_terms) = nan;
 data2(:, ([1+end-filter_terms]:end)) = nan;

% ah = axes('Parent', figure, 'NextPlot', 'add');
% plot(fdata(1,:), 'r', 'Parent', ah);
% plot(fliplr(frdata(1,:)), 'g', 'Parent', ah);
% plot(data2(1,:), 'b', 'Parent', ah);
% set(ah, 'ylim', [min(data2(1,:)) max(data2(1,:))])

if orig_ndims ~= 2
  % reshape data matrix back to original size
  data2 = reshape(data2, orig_sz);
end

r = setdata(mia, data2);
r = addprocessing(r, getprocessing(fil, mia));
