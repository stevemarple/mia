function fil = mia_filter_samnet(varargin)
%MIA_FILTER_SAMNET  Constructor for MIA_FILTER_SAMNET class.
%
%   r = MIA_FILTER_SAMNET;
%   default constructor
%
%   r = MIA_FILTER_SAMNET(...)
%   parameter name/value pair interface
%   The only parameter names accepted are 'short' and 'long'.
%
%   MIA_FILTER_SAMNET filters data in a similar method to the original
%   IDL code used at the University of York. Thus MIA_FILTER_SAMNET is
%   intended to be a compatible replacement for MIA.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

% allow [] to be default values, convert to timespans later
fil.short = [];
fil.long = [];


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

if isempty(fil.short)
  fil.short = timespan(0, 's');
end
if isempty(fil.long)
  fil.long = timespan(0, 's');
end
  

% check value exists and is scalar
if length(fil.short) ~= 1
  error('short period not scalar');
elseif ~isa(fil.short, 'timespan')
  error('short period not defined by a timespan object');
elseif length(fil.long) ~= 1
  error('long period not scalar');
elseif ~isa(fil.long, 'timespan')
  error('long period not defined by a timespan object');
end
  
% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;

