function r = getlongperiod(fil)
%GETLONGPERIOD  Return long period used by a MIA_FILTER_SAMNET filter
%
%   r = GETLONGPERIOD(fil);
%   r: TIMESPAN object
%   fil: MIA_FILTER_SAMNET object
%
%   See also MIA_FILTER_SAMNET.

if length(fil) == 1
  r = fil.long;
else
  r = timespan(zeros(size(fil)), 's');
  for n = 1:prod(size(fil))
    r(n) = fil(n).long;
  end
end
