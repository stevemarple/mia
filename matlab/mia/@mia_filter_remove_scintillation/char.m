function r = char(fil)
%CHAR  Convert MIA_FILTER_REMOVE_SCINTILLATION object to CHAR.
%
%   r = CHAR(fil)
%   r: CHAR representation
%   fil: MIA_FILTER_REMOVE_SCINTILLATION object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

newline = char(10);

% if isa(fil.filter, 'mia_filter_base')
% filchar = sprintf(' ...\n%s', char(fil.filter));

tmp = char(fil.filter);

if isempty(tmp)
  filchar = newline;
else
  % remove any trailing newline
  if strcmp(tmp(end), newline)
    tmp(end) = [];
  end
  % replace any other newlines with an indent first, and add trailing
  % newline
  tmp = sprintf('\n%s', tmp);
  filchar = sprintf('%s\n', strrep(tmp, newline, sprintf('\n     ')));
end
  
if isempty(fil.threshold)
  thresh = '<empty>';
else
  thresh = sprintf('%f', fil.threshold);
end

if isempty(fil.method)
  method = '<empty>';
else
  method = fil.method;
end

if isempty(fil.invertmask)
  invert = '<empty>';
elseif fil.invertmask
  invert = 'true';
else
  invert = 'false';
end

r = sprintf(['%s' ...
	     'sources       : %s\n' ...
	     'method        : %s\n' ...
	     'threshold     : %s\n' ...
	     'invertmask    : %s\n' ...
	     'replacement filter:%s\n'], ...
	    mia_filter_base_char(fil), ...
	    commify(fil.sources{:}) ,...
	    method, ...
	    thresh, ...
	    invert, ...
	    filchar);




