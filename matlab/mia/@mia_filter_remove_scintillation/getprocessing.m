function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_REMOVE_SCINTILLATION object
%
%   See also mia_base/ADDPROCESSING

% other classes do need to know data. For consistency, this class should
% insist upon it even though it is not required
nargchk(2,2,nargin); 

r = sprintf('Scintillation removed with %s method using filter ''%s''', ...
	    fil.method, getname(fil.filter));

