function [r, mask, val, t, stardata] = filter(fil, mia)
%FILTER  Replace scintillation with filtered values.
%
%   [r, mask] = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   mask: LOGICAL values indicating if data probably affected by scintillation
%   fil: MIA_FILTER_REMOVE_SCINTILLATION object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_REMOVE_SCINTILLATION.

rio = getinstrument(mia);
beams = getbeams(mia);
samt = getsampletime(mia);

% Allow filter to choose method based on riometer. For now always choose
% angulardistance
if isempty(fil.method)
  fil.method = 'angulardistance';
end

if strcmp(fil.method, 'directivity')
  warning('directivity method is now called antennagain');
  fil.method = 'antennagain';
end

if isempty(fil.threshold);
  switch fil.method
   case 'angulardistance'
    fil.threshold = info(rio, 'beamwidth', 'beams', beams) ./ 2;
    
   case {'antennagain' 'antennagainmainlobe'}
    fil.threshold = -3;
  end
end

[mask, val, t, stardata] = ...
    scintillation_calculator(rio, ...
			     'sources', fil.sources, ...
			     'time', samt, ...
			     'beams', beams, ...
			     'method', fil.method, ...
			     'threshold', fil.threshold);
if fil.invertmask
  % Invert the mask
  mask = ~mask;
end

mia2 = filter(fil.filter, mia);

data = getdata(mia);
data2 = getdata(mia2);

data(mask) = data2(mask);

r = setdata(mia, data);
r = addprocessing(r, getprocessing(fil, mia));

