function fil = mia_filter_remove_scintillation(varargin)
%MIA_FILTER_REMOVE_SCINTILLATION  Constructor for MIA_FILTER_REMOVE_SCINTILLATION
%
%   r = MIA_FILTER_REMOVE_SCINTILLATION;
%   default constructor
%
%   r = MIA_FILTER_REPLACE_NANS(...)
%   parameter name/value interface
%
%

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';
validmethods = scintillation_calculator(riometer, 'listmethods');

% Make it easy to change the class definition at a later date
latestversion = 3;
fil.versionnumber = latestversion;
fil.sources = {'cassiopeia_a', 'cygnus_a', 'sagittarius_astar'};

% determine default method later when riometer is known
% fil.mode = 'angulardistance'; % renamed from mode to method in version 3
fil.threshold = [];

% Added in version 2
% If true invert the mask (ie keep scintillation, discard clean data
fil.invertmask = false; 

% Version 3
fil.method = 'angulardistance';


% By default replace scintillation values with NaNs
fil.filter = mia_filter_fixed(nan);

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif nargin == 1 & isstruct(varargin{1})
  % Construct from struct. Useful for failed load commands when the class
  % layout has changed.
  p = feval(parent);
  a = varargin{1};
  
  for n = 1:numel(a)
    an = a(n);
    
    fn = intersect(fieldnames(a), fieldnames(fil));
    tmp = fil;
    fil = repmat(fil, size(a));
    fil = class(fil, cls, p);
    
    for m = 1:length(fn)
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end
    
    if ~isfield(an, 'versionnumber');
      an.versionnumber = latestversion;
    end

    switch tmp.versionnumber
     case {1 2}
      if isfield(an, 'mode');
	tmp.method = an.mode;
      end
     case 3;
      ;
     otherwise
      error(sprintf('unknown version (was %d)', an.versionnumber));
    end
    fil(n) = class(tmp, cls, p);
  end
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end


for n = 1:numel(fil)
  if ischar(fil(n).sources)
    fil(n).sources = {fil(n).sources};
  end
  
  % Ensure fil(n).invertmask is logical
  fil(n).invertmask = isequal(fil(n).invertmask, true);
  
  % Ensure that the returned object is marked with the latest version
  % number
  fil(n).versionnumber = latestversion;
  
  % Check method
  if ~isempty(fil(n).method) & ~any(strcmp(fil(n).method, validmethods))
    error(sprintf('unknown method (was ''%s''), valid methods are %s', ...
		  fil(n).method, commify(validmethods{:})));
  end
end

