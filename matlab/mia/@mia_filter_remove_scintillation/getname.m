function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_REMOVE_SCINTILLATION object
%
%   See also MIA_FILTER_REMOVE_SCINTILLATION.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'remove scintillation';
  
 case 'u'
  r = 'REMOVE SCINTILLATION';
  
 case 'c' 
  r = 'Remove scintillation';
 
 case 'C'
  r = 'Remove Scintillation';
  
 otherwise
  error('unknown mode');
end

return
