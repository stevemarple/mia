function r = validdata(fil, data)
%VALIDDATA  Test if the data can be filtered
%
%   See VALIDDATA.

r = ones(size(data)) & isa(data, 'rio_base') & ~isa(data, 'rio_qdc_base');


