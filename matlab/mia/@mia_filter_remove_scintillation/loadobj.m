function r = loadobj(a)
%LOADOBJ  Load filter for MIA_FILTER_REMOVE_SCINTILLATION object
%
if isstruct(a)
  r = mia_filter_remove_scintillation(a);
elseif a.versionnumber ~= 3
  r = mia_filter_remove_scintillation(struct(a));
else
  r = a;
end



