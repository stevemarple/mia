function r = cdaweb2mia(filename)

r = [];
datainfo = cdfinfo(filename)

datainfo.VariableAttributes.FIELDNAM(:, 1)
Epoch_idx = find(strcmp(datainfo.VariableAttributes.FIELDNAM(:, 1), 'Epoch'));
if isempty(Epoch_idx)
  error('Cannot find Epoch details');
end

epoch = cdfread(filename, 'Variables','Epoch'); % read CDF epoch values
epoch = struct([epoch{:}]); % convert from cell to matrix
epoch = timestamp('cdfepoch', [epoch.date]);
epoch(1)
epoch(2)
epoch(3)
epoch(end)
day = timespan(1, 'd');
starttime = floor(epoch(1), day)
endtime = ceil(epoch(end), day)
resolution = min(diff(epoch))

