function r = preferredinstrument(cls)
%PREFERREDINSTRUMENT  Select default instrument of a given type
%
%   in = PREFERREDINSTRUMENT(cls)
%   in: MIA_INSTRUMENT_BASE object
%   cls: class of object to return
%
% See MIA_INSTRUMENT_BASE.

if isa(cls, 'mia_instrument_base')
  cls = class(cls);
end

switch cls
 case 'riometer'
  r = rio_kil_1;
  
 case 'camera'
  r = cam_dasi_1;
  
 case 'energymap'
  r = ene_dasi_kil;
  
 otherwise
  error(sprintf('no known preferred instrument for %s', class(in)));
end
