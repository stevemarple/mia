function r = gettimes(fil)
%GETVALUE Return the times of day for the MIA_FILTER_TIME_OF_DAY filter.
%
%   r = GETTIMES(fil);
%   r: times for which blanking is applied
%   fil: MIA_FILTER_TIME_OF_DAY object
%
%   See also MIA_FILTER_TIME_OF_DAY.

r = fil.times;

