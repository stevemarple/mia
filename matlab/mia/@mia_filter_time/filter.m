function r = filter(fil, mia)
%FILTER  Replace data with a fixed value.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_TIME object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_TIME.


d = getdata(mia);
samt = getsampletime(mia);
t = fil.time;

tr = size(t, 1); % number of rows in time 
idx = cell(1, tr);

if isa(samt, 'timestamp')
  if isa(t, 'timestamp');
    ; % use samt and t as they are
  elseif isa(t, 'timespan')
   % use time of day for sample times
   samt = gettimeofday(samt);
  else
    error(sprintf('unsupported class type for filter time (was ''%s'')', ...
		  class(t)));
  end
elseif isa(samt, 'timespan')
  if isa(t, 'timestamp');
    error('cannot filter based of timestamp when sampletime is timespan');
  elseif isa(t, 'timespan')
    ; % use samt and t as they are
  else
    error(sprintf('unsupported class type for filter time (was ''%s'')', ...
		  class(t)));
  end
else
  error(sprintf('unsupported class type for sample time (was ''%s'')', ...
		class(samt)));
end

for n = 1:tr
  if isa(t, 'timespan') & t(n,1) > t(n,2)
    % timespan wrapping across midnight
    idx{n} = find(samt >= t(n, 1) || samt < t(n,2));
  else
    idx{n} = find(samt >= t(n,1) & samt < t(n,2));
  end
end

% Convert each set of time indices to a unique sorted list.
idx = unique([idx{:}]);

% Use extract to extract the peiods we want left
proc = getprocessing(mia);
s.type = '()';
nd = getndims(mia);
s.subs = repmat({':'}, 1, nd); % keep all samples

% now extract only the time indices we wish to keep
s.subs{end} = setdiff(1:getdatasize(mia, nd), idx);
r = extract(mia, s);

% Restore the original processing details, we are going to add our own!
r = setprocessing(r, proc); 

r = addprocessing(r, getprocessing(fil, mia));

