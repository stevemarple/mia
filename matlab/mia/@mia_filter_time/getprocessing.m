function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_TIME object
%
%   See also mia_base/ADDPROCESSING

% other classes do need to know data. For consistency, this class should
% insist upon it even though it is not required
nargchk(2,2,nargin); 

if isempty(fil.time)
  r = '';
  return 
end

plural = '';
if size(fil.time, 1) ~= 1
  plural ='s';
end

p = cell(1, size(fil.time, 1));
for n = 1:size(fil.time, 1)
  p{n} = dateprintf(fil.time(n, 1), fil.time(n, 2));
end

r = sprintf('Data deleted for %d time period%s: %s', ...
	    numel(p), plural, join('; ', p));

