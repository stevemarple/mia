function fil = mia_filter_time(varargin)


[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.time = repmat(timespan, 0, 2);

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

if ~isa(fil.time, 'timestamp') & ~isa(fil.time, 'timespan')
  error('time must be a timestamp or timespan matrix');
end

for n = 1:numel(fil)
  tmp = fil(n);
  
  % check size of times exists and is correct size
  if size(tmp.time, 2) ~= 2
    error('times matrix must have two columns');
  elseif ndims(tmp.time) ~= 2
    error('times matrix must be 2 dimensional');
  end
  
  % Ensure that the returned object is marked with the latest version
  % number
  tmp.versionnumber = latestversion;
  
  fil(n) = tmp;
end
  
