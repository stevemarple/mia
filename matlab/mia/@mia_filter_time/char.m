function r = char(fil)
%CHAR  Convert MIA_FILTER_TIME object to CHAR.
%
%   r = CHAR(fil)
%   r: CHAR representation
%   fil: MIA_FILTER_TIME object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end


r = sprintf(['%stime          : %s\n'], ...
	    mia_filter_base_char(fil), ...
	    matrixinfo(fil.time));





