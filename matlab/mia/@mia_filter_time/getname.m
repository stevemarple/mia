function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_TIME object
%
%   See also MIA_FILTER_TIME.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'time';
  
 case 'u'
  r = 'TIME';
  
 case 'c' 
  r = 'Time';
 
 case 'C'
  r = 'Time';
  
 otherwise
  error('unknown mode');
end

return
