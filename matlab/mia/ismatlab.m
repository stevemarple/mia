function r = ismatlab
%ISMATLAB  Test if running under Matlab interpreter.
%
% r = ISMATLAB
% r: LOGICAL
%
% ISMATLAB returns TRUE if running under MATLAB, false otherwise. The
% results is based upon the existence of the matlabrc function.

if exist('matlabrc')
  r = true;
else
  r = false;
end
