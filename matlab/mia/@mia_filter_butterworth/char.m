function s = char(fil)
%CHAR Convert MIA_FILTER_BUTTERWORTH object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_BUTTERWORTH object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

[freqStr periodStr] = getfreqstr(fil);
 
s = sprintf(['%stype          : %s\n' ...
	     'order         : %d\n' ...
	     'frequency     : %s\n' ...
	     'period        : %s\n'], ...
            mia_filter_base_char(fil), fil.type, fil.order, ...
	    freqStr, periodStr);

