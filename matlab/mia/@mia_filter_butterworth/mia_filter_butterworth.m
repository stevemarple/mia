function fil = mia_filter_butterworth(varargin)


[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% fields
latestversion = 1;
fil.versionnumber = 1; % integer
fil.order = 1;
fil.frequency = 1;
fil.type = 'low';

if nargin == 0
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);

else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;


