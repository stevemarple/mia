function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_SLIDING_AVERAGE object
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case {'l' 'c' 'C'}
  r = 'Butterworth';
  
 case 'u'
  r = 'SLIDING AVERAGE';
   
 otherwise
  error('unknown mode');
end

return

