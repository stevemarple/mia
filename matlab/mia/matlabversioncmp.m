function r = matlabversioncmp(cmp, v)
%MATLABVERSIONCMP Compare Matlab version with numeric/string value.
%
%   r = MATLABVERSIONCMP(cmp, v)
%   r: LOGICAL result
%   cmp: comparison operator [ 'ge', 'gt', 'le', 'lt', 'eq', 'ne' ] or
%   symbolic equivalents
%   v: version to compare, CHAR or NUMERIC array
%
% MATLABVERSIONCMP Compares the Matlab version obtained from VERSION with
% the specified version, using the given comparison operator. A string
% representation such '7.12' is assumed to mean major version 7, minor
% version 12, and is thus larger than '7.3', which is a major version of 7
% and a minor version of 3. String representations of the version number are
% converted to a NUMERIC array; either may be supplied. The matlab
% version string uses four version levels, but the given version string
% can contain fewer levels, in which case less tests are made.
%
% See also VERSION.

if isoctave
  error('Do not use matlabversioncmp in octave!');
end

switch cmp
 case '>'
  cmp = 'gt';
 case '>='
  cmp = 'ge';
 case '<'
  cmp = 'lt';
 case '<='
  cmp = 'le';
 case '=='
  cmp = 'eq';
 case '~='
  cmp = 'ne';
end

if ischar(v)
  v = local_vstring_to_array(v);
elseif isnumeric(v)
  if ~isequal(v, fix(v))
    error('All numeric parts must be integers');
  end
end

if isempty(v)
  error('version must not be empty');
end

mv = local_vstring_to_array(version);


len = min(numel(mv), numel(v));

% Trim arrays
mv = mv(1:len);
v = v(1:len);

switch cmp
 case 'eq'
  r = all(mv == v);
  
 case 'ne'
  r = ~all(mv == v);
  
 case {'gt' 'ge'}
  for n = 1:len
    if mv(n) > v(n)
      r = true;
      return
    elseif mv(n) < v(n)
      r = false;
      return
    end
  end
  % must be equal, return true for ge, false for gt
  r = strcmp(cmp, 'ge');
  
 case {'lt' 'le'}
  for n = 1:len
    if mv(n) < v(n)
      r = true;
      return
    elseif mv(n) > v(n)
      r = false;
      return
    end
  end
  % must be equal, return true for le, false for lt
  r = strcmp(cmp, 'le');

 otherwise
  error(sprintf('unknown comparison operator (was ''%s'')', cmp));
end

return

function r = local_vstring_to_array(s)
% Delete from first space to end of string
spci = find(s == ' ');
if ~isempty(spci)
  s(spci(1):end) = [];
end

if any(s == '.')
  a = split('.', s);
else
  a = {s};
end

r = zeros(1, numel(a));
for n = 1:numel(a)
  r(n) = str2num(a{n});
end
