function tri = splittriangle(n)
%SPLITTRIANGLE Split a triangle, and return the vertices of the sub-triangles
%
%   r = SPLITTRIANGLE(n)
%   r: n x 3 vector of vertices
%   n: number of levels to split into
%
%   The total number of vertices is given by TRIANGULARNUMBER(n+1), while
%   the number of discrete triangles is given by n^2.
%
% Vertices are given in order a,b,c, where the triangle appears as:
%
%     c
%    / \
%   /   \
%  a-----b
%
% and as a,d,b, where the triangle appears as
%
%  a-----b
%   \   /
%    \ /
%     d
%


%
% Consider splitting a triangle into 4 levels. The number of component
% triangles is 4^2 = 16 and the number of vertices is givern by
% TRIANGULARNUMBER(4) = 10.
%
% Let the triangle be split as:
%
%             1
%
%          2     3 
%
%      4      5      6
%
%    7     8     9     10
%
% 11    12    13    14    15

trinums = triangularnumber(1:n);
tri = zeros(n*n, 3);

t = 0; % triangle under consideration
for r = 1:n
  % vertex = trinums(r+1) - 1; % initial vertex
  
  for m = 1:r
    t = t + 1;
    a = trinums(r) + m;
    % do upward pointing triangles
    tri(t,:) = [a a+1 a-r];
    
    if r ~= n
      % do downward pointing triangles (not for last row)
      t = t + 1;
      tri(t,:) = [a a+r+2 a+1];
    end
  end
  
end
