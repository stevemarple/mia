function r = horzcat_struct(varargin)
%HORZCAT_STRUCT Horizontal concatenation on all fields in a struct.
%
%   r = HORZCAT_STRUCT(s1, s2, ...)
%   r: STRUCT
%   s1, s2, ...: STRUCTS with identically-named fields
%
% HORZCAT_STRUCT performs a HORZCAT on each field of the structures. For
% example,
% s1.a = [1 2 3]
% s1.b = {'a' 'b' 'c'}
% s2.a = [4 5 6]
% s2.b = {'d' 'e' 'f'}
%
% s = HORZCAT_STRUCT(s1, s2)
% would give
% s.a = [1 2 3 4 5 6]
% s.b = {'a' 'b' 'c' 'd' 'e' 'f'}
%
% See also HORZCAT.

if nargin == 0
  r = [];
elseif nargin == 1
  r = varargin{1};
end
  
fn = fieldnames(varargin{1});
fn_sort = sort(fn);
for n = 2:numel(varargin)
  if ~isequal(fn_sort, sort(fieldnames(varargin{n})))
    error(sprintf('fieldnames in argument %d differ', n));
  end
end

while numel(varargin) > 1
  % remove any empty elements in varargin
  varargin(cellfun('isempty', varargin)) = [];
   for n = 1:2:(numel(varargin)-1)
      varargin{n} = local_horzcat(fn, varargin{n}, varargin{n+1});
      varargin{n + 1} = [];
   end
end
r = varargin{1};


function r = local_horzcat(fn, a, b)
r = a;
for n = 1:numel(fn)
  a_f = getfield(a, fn{n});
  b_f = getfield(b, fn{n});
  r = setfield(r, fn{n}, [a_f b_f]);
end

