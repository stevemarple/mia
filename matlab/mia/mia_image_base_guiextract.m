function varargout = mia_image_base_guiextract(action, varargin)


switch action
 case 'init'
  
 case 'list'
  % first return value of a cell array of methods. Each method is a cell
  % array suitable for passing to feval as "feval(cellarray{:}, mia);"
  % where mia is the data to be used
  varargout{1} = {{mfilename 'timeinit' 'data'}, ...
		  {mfilename 'pixelsinit' 'data'}}; % options
  % second return value is a cell array of names to be used on menus
  if nargout >= 2
    varargout{2} = {'Start/end time' 'X/Y pixel location'};
  end
  
 otherwise
  error(sprintf('unknown action (was %s)', action));
end

  
