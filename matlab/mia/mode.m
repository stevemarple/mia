function [r,rn] = mode(a)
%MODE Compute the modal average.
%
% [r, rn] = MODE(a)
% r: the mode (DOUBLE)
% rn: number of times the mode occurs (DOUBLE)
% a: input set of values (DOUBLE)
%
% MODE computes the most frequently occuring value in the set 'a'. If a is
% empty MODE returns NAN. For some data sets the mode is not unqiue, in
% which case r will be a vector containing all modes in ascending
% order. Any NANs in the input set are ignored.

switch numel(a)
 case 0
  r = nan;
  rn = 0;
 case 1
  r = a;
  rn = 1;
  
 otherwise
  as = sort(a(:));
  
  % Actual mode and number of times it occurs. r may be a vector, meaning
  % mutliple modes.
  r = nan;
  rn = 0;
  
  lastval = as(1);
  count = 0;
  
  for n = 1:numel(as)
    if as(n) == lastval
      count = count + 1;
      if count > rn
	% current value occurs most often so far, set mode
	r = as(n);
	rn = count;
      
      elseif count == rn
	% a draw, remember this value in addition to previous values
	r(end+1) = as(n);
      end
    else
      lastval = as(n);
      count = 1;
    end
  end

end


