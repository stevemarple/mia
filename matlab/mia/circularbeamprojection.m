function [xIono, yIono] = circularbeamprojection(varargin)
%CIRCULARBEAMPROJECTION Caclulate the projection of a beam on the ionosphere.
%
% [x, y] = CIRCULARBEAMPROJECTION(...)


% plot the projection of the beams at a height h. If h is not given then a 
% default height of 90km is used.

%
% $Log$

xIono = [];
yIono = [];
pi_2 = pi/2; 



defaults.points = 80;  % number of points used on cone of beam
defaults.height = [];
defaults.azimuth = [];
defaults.zenith = [];
defaults.beamwidth = [];
defaults.units = 'm'; % units of x and y
defaults.location = [];
defaults.earthradius = earthradius; % radius of the Earth (approx 6378000m)


[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.height)
  error('height not specified');
elseif numel(defaults.height) ~= 1
  error('height must be a scalar');
end

if isempty(defaults.azimuth)
  error('azimuth not specified');
elseif size(defaults.azimuth, 2) ~= 1
  error('azimuth must be a column matrix');
else
  numOfBeams = numel(defaults.azimuth);
end

if isempty(defaults.zenith)
  error('zenith not specified');
elseif size(defaults.zenith, 2) ~= 1
  error('zenith must be a column matrix');
elseif numel(defaults.zenith) ~= numOfBeams
  error('number of zenith angles must match number of azimuth angles');
end
if isempty(defaults.beamwidth)
  error('beamwidth not specified');
elseif size(defaults.beamwidth, 2) ~= 1
  error('beamwidth must be a column matrix');
elseif numel(defaults.beamwidth) ~= numOfBeams
  error('number of beamwidth angles must match number of azimuth angles');
end

if isempty(defaults.location)
  if strcmp(defaults.units', 'deg')
    error('location must be specified for units=deg');
  end
elseif ~isa(defaults.location, 'location')
  error('location must be a location object');
end

if ~any(strcmp(defaults.units, {'m', 'km', 'deg'}))
  error(sprintf('unknown units (was ''%s'')', defaults.units));
end

% convert azimuth from compass degrees to graph degrees
defaults.azimuth = rem(-defaults.azimuth + 90, 360);

defaults.beamwidth
%  convert to radians
zenithR = defaults.zenith * 2 * pi / 360;
azimuthR = defaults.azimuth * 2 * pi / 360;
beamwidthR = defaults.beamwidth * 2 * pi / 360;

xIono = zeros(defaults.points, numOfBeams);
yIono = zeros(defaults.points, numOfBeams);

% circumscribe a circle around the beam centre (at unit distance from 
% origin) and convert it to new azimuth and zenith angles. 

% r(1,:) = tan(beamwidthR(1,:)/2); % beam radius
r = tan(beamwidthR(1,:)/2); % beam radius

for bn = 1:numOfBeams

  [x,y,z] = sph2cart(azimuthR(bn), (pi_2-zenithR(1,bn)), 1);
  for i = 1:defaults.points+1
    alpha = ((2 * pi * (i-1)) / defaults.points);

    deltax = -r*sin(alpha)*sin(azimuthR(bn)) - ...
	     r*cos(alpha)*cos(zenithR(1,bn))*cos(azimuthR(bn));
    deltay = r*sin(alpha)*cos(azimuthR(bn)) - ...
	     r*cos(alpha)*cos(zenithR(1,bn))*sin(azimuthR(bn));
    deltaz = r * sin(zenithR(1,bn)) * cos(alpha);
    
    [beamEdgeAzimuthR, beamEdgeElevation] = ...
	cart2sph(x+deltax, y+deltay, z+deltaz);
    
    for jn =  1:numOfBeams
      % j = defaults.beams(jn);
      d(1,jn) = defaults.earthradius * ...
		polarangleofray(pi_2 - beamEdgeElevation(1,jn), ...
				defaults.height); 
    end
    xIono(i,bn) = d(1,bn) * cos(beamEdgeAzimuthR(bn));
    yIono(i,bn) = d(1,bn) * sin(beamEdgeAzimuthR(bn));
  end
end


switch defaults.units
  case 'km'
    % already in km
    
  case 'deg'
    % check whether xy2longlat wants distances at height h=0 or h=height
    [xIono yIono] = xy2longlat(xIono, yIono, ...
			       defaults.height, defaults.location);
    
  otherwise
    error(['unknown unit (was ' defaults.units ')']);
end




