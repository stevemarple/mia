function r = slidingmean(data, blockSize)
%SLIDINGMEAN Perform a sliding mean.

dim = 1;

if ~isequal(size(blockSize), [1 1]) | blockSize < 1
  error('block size must be a single, positive integer')
end

if prod(size(data)) == length(data) & size(data, 1) == 1
  % any vectors acceptable but internally deal with column vectors only
  data = reshape(data, length(data), 1);
  reshaped = 1;
else
  reshaped = 0;
end

dataSize = size(data);
r = zeros(dataSize - [blockSize-1 0]);
if isempty(r)
  error('data size must be larger than blocksize');
end

% [runningSum divisor] = nonansum(data(1:(blockSize-1), :), dim);
runningSum = nonansum(data(1:(blockSize-1), :), dim);
divisor = sum(~isnan(data(1:(blockSize-1), :)), dim);

for n = 1:size(r,1)
  % add lastest sample
  notNans = find(~isnan(data(n + blockSize - 1, :)));
  if ~isempty(notNans)
    runningSum(notNans) = runningSum(notNans) + ...
	data(n + blockSize - 1, notNans);
    divisor(notNans) = divisor(notNans) + 1;
  end
  
  % avoid divide by zero errors by setting zeros in the divisor to nan
  tmpDivisor = divisor;
  divZero = find(divisor == 0);
  if ~isempty(divZero)
    tmpDivisor(divZero) = nan;
  end
  r(n, :) = runningSum ./ tmpDivisor;
  
  % remove oldest sample
  notNans = find(~isnan(data(n, :)));
  if ~isempty(notNans)
    runningSum(notNans) = runningSum(notNans) - data(n, notNans);
    divisor(notNans) = divisor(notNans) - 1;
  end
end


if reshaped
  r = r';
end
