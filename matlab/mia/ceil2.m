function r = ceil2(a, b)
%CEIL2  Like CEIL, but ceil to nearest multiple of b.
%
%   r = CEIL2(a, b)
%
%   r: matrix of same size as a
%   a: matrix to be ceiled
%   b: scalar, or matrix the same size as a, defining the boundary to
%   round up to.
%
%   See also CEIL, FLOOR2, ROUND2.

if prod(size(b)) == 1 & prod(size(a)) ~= 1
  b = repmat(b, size(a));
elseif ~isequal(size(a), size(b))
  error('b must be a scalar or a matrix the same size as a');
end

r = ceil(a ./ b) .* b;

