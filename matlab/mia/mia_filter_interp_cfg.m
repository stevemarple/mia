function r = mia_filter_interp_cfg(action, varargin)

r = [];
switch action
  case 'init'
    % mfilename('init', ifil)
    % disp(action);
    
    spacing = [10 10]; % x,y
    frameSpacing = [10 10]; % figure to frame spacing
    lineSpacing = 8;  % vertical spacing between lines of text
    fh = figure('WindowStyle', 'modal', ...
	'NumberTitle', 'off', ...
	'Visible', 'off', ...
	'Name', 'Configure filter');
    
     
    % no menus (modal dialog box)
    set(fh, 'MenuBar', 'none');
    
    bgColour = get(fh, 'Color');
    frameColour = get(fh, 'Color');

    % in case anything else uses UserData, or window is later created in
    % a different way load UserData
    ud = get(fh, 'UserData');
    ud.fh = fh;
    ud.originalIfil = varargin{1};
    ud.ifil = ud.originalIfil;

    % need to find suitable width and height for all the text objects in
    % the LH column. Do this by creating a test object and finding out
    % what its position is after uiresize. The text used should be the
    % longest. Strictly speaking that might depend on font, kerning etc
    % but we will ignore that. Add an extra space if you have problems :)
    textH = uicontrol(fh, 'Style', 'text', ...
	'Position', [0 0 1 1], ...
	'String', 'Interpolation:  ');
    textPos = uiresize(textH, 'l', 'b');
    delete(textH);
     
    genH     = zeros(4,1);   % handles to uicontrols in general frame
    genPos   = zeros(4,4);   % positions  "                    "

    frameWidth = 3.9 * textPos(3);
    
    genPos(1,:) = [frameSpacing frameWidth 0] + ...
	[0 50 0 2*spacing(2)+3*textPos(4)+2*lineSpacing]; 

    figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
    figPos = get(fh, 'Position');
    screenSize = get(0, 'ScreenSize');
    set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
    
        % -----------------------------
    % General frame
    % frame  = 1
    genH(1) = uicontrol(fh, 'Style', 'frame', ...
	'BackgroundColor', frameColour, ...
	'Position', genPos(1,:), ...
	'Tag', 'generalframe');
 
    % 'Configure ...' = 2
    genPos(2,:) = [genPos(1,1)+spacing(1), ...
	  genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
	  genPos(1,3)-2*spacing(2), textPos(4)];
    genH(2) = uicontrol(fh, 'Style', 'text', ...
	'String', ...
	sprintf('Configure %s filter', lower(getname(ud.ifil))) , ...
	'FontWeight', 'bold', ...
	'HorizontalAlignment', 'center', ...
	'Position', genPos(2,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');
    
    % 'Interpolation' = 3
    genPos(3,:) = [genPos(1,1)+spacing(1), ...
	  genPos(2,2)-lineSpacing- textPos(4), ...
	  textPos(3), textPos(4)];
    genH(3) = uicontrol(fh, 'Style', 'text', ...
	'String', 'Interpolation: ', ...
	'HorizontalAlignment', 'right', ...
	'Position', genPos(3,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');

    rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;

    % interpolation menu = 4
    [interpNames interpVal] = interpmethods(ud.ifil);

    genPos(4,:) = [rhCol genPos(3,2) 1 1];
    genH(4) = uicontrol(fh, 'Style', 'popupmenu', ...
	'String', interpNames, ...
	'Value', interpVal, ...
	'HorizontalAlignment', 'center', ...
	'Position', genPos(4,:), ...
	'Callback', [mfilename '(''interptype'');'], ...
	'Tag', 'interpmenu');
    % 	'Callback', [mfilename '(''filtertype'');'], ...
    
    genPos([2 4],:) = uiresize(genH([2 4]), 'l', 'b', {''; ''});
    
    if matlabversioncmp('>=', '5.2')
      set(genH(4), 'TooltipString', 'select interpolation method');
    end

    
    bb = boundingbox(genPos(2,:), genPos(4,:));
    genPos(1,:) = [genPos(1, 1:2) (bb(1:2)+bb(3:4) - genPos(1,1:2) + ...
	  frameSpacing)];
    set(genH(1), 'Position', genPos(1,:));
    
    fhPos = get(fh, 'Position');
    fhPos(3:4) = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
    set(fh, 'Position', fhPos);
    
    [ud.oh ud.ch] = okcancel('init', fh, [0 20 1 1], ...
	[mfilename '(''ok'');'], [mfilename '(''cancel'');']);
    
    set(fh, 'UserData', ud, ...
	'Visible', 'on', ...
	'Resize', 'off');
    waitfor(ud.oh); % wait for the ok button to be deleted
    
    % resumed, other callbacks will have modified UserData by now (or may
    % have) so reload
    ud = get(fh, 'UserData');
    close(fh);
    r = ud.ifil;
    return;
    
    % --------------------------------------------------------------
  case 'ok'
    % mfilename('ok')
    ud = get(gcbf, 'UserData');
    % everything ok, just activate the previous waitfor by deleting ok
    % handle
    delete(ud.oh);
        
    
  case 'cancel'
    % mfilename('cancel')
    ud = get(gcbf, 'UserData');
    % put original data back
    ud.ifil = ud.originalIfil;
    set(ud.fh, 'UserData', ud);
    
    % activate the previous waitfor by deleting ok handle (if it exists)
    if ishandle(ud.oh)
      delete(ud.oh);
    else
      % (l)user pressed CTRL-C
      delete(ud.fh);
    end
    
    %   case 'config'
    %     % mfilename('config')
    %     ud = get(gcbf, 'UserData');
    %     ifil2 = getfilter(ud.ifil);
    %     ifil2 = configure(ifil2);
    %     ud.ifil = ifilremscint(ifil2);
    %     set(ud.fh, 'UserData', ud);

    %   case 'filtertype'
    %     % mfilename('filtertype')
    %     ud = get(gcbf, 'UserData');
    %     menuNum = get(findobj(ud.fh, 'Type', 'uicontrol', 'Tag', ...
    % 	'filtermenu'), 'Value');
    %     ud.ifil = ifilremscint(ud.ifilters{menuNum});
    %     set(ud.fh, 'UserData', ud);
	
  case 'interptype'
    % mfilename('interptype')
    ud = get(gcbf, 'UserData');
    ud.ifil = ...
	mia_filter_interp(popupstr(findobj(ud.fh, 'Type', 'uicontrol', ...
					   'Tag', 'interpmenu')));
    set(ud.fh, 'UserData', ud);
    
  otherwise
    error('unknown action');
end

