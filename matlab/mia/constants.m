function varargout = constants(varargin)
%CONSTANTS  Return the values of physical and other important constants.
%
%   constants(...)
%
%   Without any return parameters CONSTANTS prints information about the
%   constants. If not constants are named CONSTANTS prints details on all
%   the constants it knows about.
%
%
%   [value units units2 uncertainty name symbol] = constants(...)
%
%   value: value of constant (DOUBLE)
%   units: units the of the value and uncertainty is measured in (CHAR/CELL)
%   units2: units the of the value and uncertainty is measured in (CELL)
%   uncertainty: uncertainty associated with the value. For some
%                constants this is zero, as the constant is defined to be
%                that value (DOUBLE)
%   name: the name of the constant (CHAR/CELL)
%   symbol: the symbol used to access the physical constant data
%
% With one or more return parameters CONSTANTS returns data associated with
% each constant. If no input parameters were supplied CONSTANTS behaves as
% if all known constants were listed. 'units' is a textual representation of
% the units, in a free format. 'units2' describes the units as a CELL
% matrix, one element for every SI base unit, possibly with '-1' appended to
% indicate division.
%
% Details are taken from http://physics.nist.gov/cuu/index.html


c.c.name = 'speed of light in vacuum';
c.c.value = 299792458;
c.c.uncertainty = 0; % exact!
c.c.units = 'm/s';
c.c.units2 = {'m' 's-1'};
	     
c.e.name = 'elementary charge (charge on electron)';
c.e.value = 1.602176462e-19;
c.e.uncertainty = 0.000000063e-19;
c.e.units = 'C';
c.e.units2 = {'C'};

c.h.name = 'Planck constant';
c.h.value = 6.62606876e-34;
c.h.uncertainty = 0.00000052e-34;
c.h.units = 'Js';
c.h.units2 = {'J' 's'};

c.Re.name = 'polar radius of Earth';
c.Re.value = earthradius;
c.Re.uncertainty = nan;
c.Re.units = 'm';
c.Re.units2 = {'m'};

if nargin == 0
  names = sort(fieldnames(c));
else
  names = varargin;
end


if nargout == 0
  disp(' ');
  for n = 1:length(names)
    if ~isfield(c, names{n});
      error(sprintf('do not know about a constant with symbol ''%s''', ...
		    names{n}));
    end
    tmp = getfield(c, names{n});
    disp(sprintf(['     symbol: %s\n' ...
		  'description: %s\n' ...
		  '      value: %g %s\n' ...
		  'uncertainty: %g %s\n'], ...
		 names{n}, ...
		 tmp.name, ...
		 tmp.value, tmp.units, ...
		 tmp.uncertainty, tmp.units));
  end
else
  % return values: [value, units, units2, uncertainty, name, fieldname]
  % value and uncertainty are doubles, rest are cells

  for n = 1:min(nargout, 6)
      % preallocate matrices
      if n == 1 | n == 4
	varargout{n} = zeros(1, nargin);
      else
	varargout{n} = cell(1, nargin);
      end
  end
  
  for n = 1:length(names)
    if ~isfield(c, names{n});
      error(sprintf('do not know about a constant with symbol ''%s''', ...
		    names{n}));
    end
    tmp = getfield(c, names{n});
    varargout{1}(n) = tmp.value;
    if nargout >= 2
      varargout{2}{n} = tmp.units;
      if nargout >= 3
	varargout{3}{n} = tmp.units2;
	if nargout >= 4
	  varargout{4}(n) = tmp.uncertainty;
	  if nargout >= 5
	    varargout{5}{n} = tmp.name;
	    if nargout >= 6
	      varargout{6}{n} = names{n};
	    end
	  end
	end
      end
    end
  end

  % If single input extract single element from CELL matrices (except units2)
  if nargin == 1
    for n = 1:length(names)
      if iscell(varargout{n}) & n ~= 3
	varargout{n} = varargout{n}{1};
      end
    end
  end
end
