function r = mia_filter(mia, fil)
%MIA_FILTER Apply filters/transformations to data.
%
%   r = MIA_FILTER(mia, fil)
%   r: MIA_BASE or derived object
%   mia: MIA_BASE or derived object
%   fil: CELL, CHAR, FUNCTION_HANDLE or object derived from MIA_FILTER_BASE
%
% MIA_FILTER is used internally by MIA to apply filtering to data; the term
% 'filtering' is used in a generic way and can be any tranformation which
% affects part of the object. The key point is that the data returned is the
% same type as the input.
%
% If the fil parameter is empty no filtering is applied - the returned value
% is the same as the input. The fil parameter may be any type listed below:
%
%   CHAR:
%   It is assumed that the string is a function name; that function is
%   called with the data object as the sole parameter.
%
%   FUNCTION_HANDLE:
%   The function is called with the data object as the sole parameter.
%
%   MIA_FILTER_BASE (or derived types):
%   The FILTER function will be called with fil as the first parameter
%   and mia as the second parameter, in the standard way defined by
%   MIA_FILTER_BASE.
%
%   CELL:
%   The CELL array argument will be recursively inspected and filters
%   corresponding to the above types will be applied in the order in which
%   they are found.
%
% Note that it is not possible to pass parameters to functions defined as
% CHAR or FUNCTION_HANDLEs; either define a dedicated function or create a
% class derived from MIA_FILTER_BASE and use an object of that class to
% store the parameters (ie a 'functor').
%
% See also MIA_FILTERS, MIA_FILTER_BASE, FUNCTION_HANDLE, FEVAL.

if numel(mia) ~= 1
  r = mia;
  for n = 1:numel(mia)
    r(n) = feval(basename(mfilename), mia(n), fil);
  end
  return
end

% From this point on mia is scalar

if isempty(fil)
  r = mia;
elseif iscell(fil)
  r = mia;
  for n = 1:numel(fil)
    r = feval(basename(mfilename), r, fil{n});
  end
elseif isa(fil, 'mia_filter_base')
  % MIA filter objects have a filter function
  r = filter(fil, mia);
elseif ischar(fil) | isa(fil, 'function_handle')
  r = feval(fil, mia);
else
  error(sprintf('do not know what to do with filter argument (was %s)', ...
                matrixinfo(fil)));
end
