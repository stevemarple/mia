function varargout = interptriangle(n, data, varargin)

trinums = triangularnumber(1:(n+1));
tri = splittriangle(n);

data2 = zeros(1, trinums(n+1));
doCoords = (length(varargin) == 2);
if doCoords
  lon = varargin{1};
  lat = varargin{2};
  
  x = zeros(size(data2));
  y = zeros(size(data2));
  z = zeros(size(data2));
  % lat2 = zeros(size(data2));
  % lon2 = zeros(size(data2));
end

% copy initial vertices to new vertices
oldVertices = [1 trinums(n)+1 trinums(n+1)];
% data2(1) = data(1);
% data2(trinums(n)+1) = data(2);
% data2(end) = data(3);
data2(oldVertices) = data;

% interpolate along edges
for r = 1:(n-1)
  data2(trinums(r)+1) = ((n-r)/n * data(1)) + (r/n * data(2)); % LHS
  data2(trinums(r+1)) = ((n-r)/n * data(1)) + (r/n * data(3)); % RHS
  data2(trinums(n)+1+r) = ((n-r)/n * data(2)) + (r/n * data(3)); % bottom
end

if doCoords
  d2r = pi/180;
  r2d = 180/pi;
  % vertices
  % [x(oldVertices) y(oldVertices) z(oldVertices)] = sph2cart(lon, lat, 1);
  [x(oldVertices) y(oldVertices) z(oldVertices)] = ...
      sph2cart(lon * d2r, lat * d2r, 1);
  
  % edges
  for r = 1:(n-1)
    x(trinums(r)+1) = ((n-r)/n * x(oldVertices(1))) + ...
	(r/n * x(oldVertices(2))); % LHS
    x(trinums(r+1)) = ((n-r)/n * x(oldVertices(1))) + ...
	(r/n * x(oldVertices(3))); % RHS
    x(trinums(n)+1+r) = ((n-r)/n * x(oldVertices(2))) + ...
	(r/n * x(oldVertices(3))); % bottom

    y(trinums(r)+1) = ((n-r)/n * y(oldVertices(1))) + ...
	(r/n * y(oldVertices(2))); % LHS
    y(trinums(r+1)) = ((n-r)/n * y(oldVertices(1))) + ...
	(r/n * y(oldVertices(3))); % RHS
    y(trinums(n)+1+r) = ((n-r)/n * y(oldVertices(2))) + ...
	(r/n * y(oldVertices(3))); % bottom

    z(trinums(r)+1) = ((n-r)/n * z(oldVertices(1))) + ...
	(r/n * z(oldVertices(2))); % LHS
    z(trinums(r+1)) = ((n-r)/n * z(oldVertices(1))) + ...
	(r/n * z(oldVertices(3))); % RHS
    z(trinums(n)+1+r) = ((n-r)/n * z(oldVertices(2))) + ...
	(r/n * z(oldVertices(3))); % bottom

  end
end


% interpolate inside original triangle, along rows
for r = 2:(n-1)
  edge1 = trinums(r)+1;
  edge2 = trinums(r+1);
  % disp(sprintf('e1=%d\ne2=%d', edge1, edge2));
  for c = 1:(r-1)
    data2(edge1+c) = ((r-c)/r * data2(edge1)) + (c/r * data2(edge2));

    if doCoords
      x(edge1+c) = ((r-c)/r * x(edge1)) + (c/r * x(edge2));
      y(edge1+c) = ((r-c)/r * y(edge1)) + (c/r * y(edge2));
      z(edge1+c) = ((r-c)/r * z(edge1)) + (c/r * z(edge2));
    end
  end
end


varargout{1} = data2;
varargout{2} = tri;
if doCoords
  % [varargout{2} varargout{3} rad] = cart2sph(x, y, z);
  [th phi rad] = cart2sph(x, y, z);
  varargout{3} = th * r2d;
  varargout{4} = phi * r2d;
  if any(rad <= eps)
    error('radius is zero');
  end
end


% debug code
if 0
  disp('---');
  for m = 1:(trinums(n+1))
    disp(sprintf('%d  %g', m, data2(m)));
  end
  
  fh = figure;
  ah = axes('Parent', fh, ...
	    'Box', 'on', ...
	    'XGrid', 'on', ...
	    'XGrid', 'on', ...
	    'YGrid', 'on');
  rotate3d on;
  h = zeros(size(tri,1), 1);
  for m = 1:length(h)
    h(m) = patch('Parent', ah, ...
		 'XData', x(tri(m,:)), ...
		 'YData', y(tri(m,:)), ...
		 'ZData', z(tri(m,:)), ...
		 'EdgeColor', 'k', ...
		 'CData', data2(tri(m,:)));
  end

  shading interp;
  set(h, 'EdgeColor', 'k');
end
