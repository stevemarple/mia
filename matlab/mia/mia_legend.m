function r = mia_legend(h_in)
r = [];

h = [];
s = {};
h_in = h_in(:);
n = 1;
while n <= length(h_in)
  if ishandle(h_in(n))
    p = get(h_in(n));
    switch p.Type
     case {'figure' 'root'}
      ; % ignore 
     case 'axes'
      % append handles to the list to check
      h_in = [h_in; get(h_in(n), 'Children')];
     
     otherwise
      if ~isempty(p.Tag)
	h(end+1) = h_in(n);
	s{end+1} = p.Tag;
      end
    end
  end
  n = n + 1;
end 

if ~isempty(h)
  r = legend(h, s{:});
end
