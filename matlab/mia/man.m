function man(varargin)
%MAN  Online help.
%
%   MAN name
%
%   Read the documentatio for "name".
%
%
%   MAN -k <name> [<name2> ...] 
%
%   Look for help about "name" (calls LOOKFOR <name>).
%
%   See also HELP, LOOKFOR.

if nargin > 1 & strcmp(varargin{1}, '-k')
  % extend LOOKFOR and search multiple times
  for n = 2:nargin
    lookfor(varargin{n});
  end
else
  help(varargin{:});
end

