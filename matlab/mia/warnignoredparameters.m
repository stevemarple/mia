function warnignoredparameters(varargin)
%WARNIGNOREPARAMETERS  Warn about parameters which have been ignored
%
%   WARNIGNOREPARAMETERS(name, ...)
%
% Given a list of parameter names issue a WARNING that they have been
% ignored.
%
% See also WARNING, INTERCEPTPROP.

switch length(varargin)
 case 0
  return;
  
 case 1
  s = ['The following parameter was ignored: ' varargin{1}];
  
 otherwise
  s = ['The following parameters were ignored: ' commify(varargin{:})];
end

% If warning was called here then the first line of the warning would be in
% this file. If backtrace is off then that action is
% veryunhelpful. Therefore call the warning in the caller's workspace. This
% means the try string must hold the warning string as a string literal, not
% a variable.
w = ['warning(''' s ''');'];
evalin('caller', w);

