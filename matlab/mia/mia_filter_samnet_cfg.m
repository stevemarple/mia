function r = mia_filter_samnet_cfg(action, varargin)
r = [];


switch action
  % ------------------------------------------------
 case 'init'
  % mfilename('init', fil)
  % disp(action);
  
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text
  fh = figure('WindowStyle', 'modal', ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'Name', 'Configure filter');
    
  bgColour = get(fh, 'Color');
  frameColour = get(fh, 'Color');

  % in case anything else uses UserData, or window is later created in
  % a different way load UserData
  ud = get(fh, 'UserData');
  ud.fh = fh;
  ud.originalFil = varargin{1};
  ud.fil = ud.originalFil;

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'Short period:  ');
  textPos = uiresize(textH, 'l', 'b');
  delete(textH);
  
  genH     = zeros(5,1);   % handles to uicontrols in general frame
  genPos   = zeros(5,4);   % positions  "                    "

  frameWidth = 3.9 * textPos(3);
  
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+3*textPos(4)+2*lineSpacing]; 

  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
  
  % -----------------------------
  % General frame
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', frameColour, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'Configure ...' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', sprintf('Configure %s filter', ...
					lower(getname(ud.fil))) , ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');

  genPos(2,:) = uiresize(genH(2), 'l', 'b');

  % 'Short period' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Short period: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');

  rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;

  % short period timespanbox = 4
  genPos(4,:) = [rhCol genPos(3,2) 1 1];
  genH(4) = timespanbox(fh, genPos(4,:), 1);
  ud.shortH = genH(4);
  settimespan(gettimespanbox(genH(4)), getshortperiod(ud.originalFil));

  
  % 'Long period' = 5
  genPos(5,:) = [genPos(1,1)+spacing(1), ...
		 (genPos(3,2) -lineSpacing - textPos(4)), ...
		 textPos(3:4)]; 
  genH(5) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Long period: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(5,:), ...
		      'BackgroundColor', frameColour, ...
		      'Callback', [mfilename '(''config'');'], ...
		      'Tag', 'configbutton');

  % long period timespanbox = 4
  genPos(6,:) = [rhCol genPos(5,2) 1 1];
  genH(6) = timespanbox(fh, genPos(6,:), 1);

  ud.longH = genH(6);
  settimespan(gettimespanbox(genH(6)), getlongperiod(ud.originalFil));
  
  bb = boundingbox(genPos(2,:), ...
		   genPos(4,:), ...
		   gethandles(gettimespanbox(genH(4))), ...
		   genPos(5,:), ...
		   gethandles(gettimespanbox(genH(6))));
  genPos(1,:) = [genPos(1, 1:2) (bb(1:2)+bb(3:4) - genPos(1,1:2) + ...
				 frameSpacing)];
  set(genH(1), 'Position', genPos(1,:));
  
  fhPos = get(fh, 'Position');
  fhPos(3:4) = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  set(fh, 'Position', fhPos);
  
  [ud.oh ud.ch] = okcancel('init', fh, [0 20 1 1], ...
			   [mfilename '(''ok'');'], ...
			   [mfilename '(''cancel'');']);
  
  set(fh, 'UserData', ud, ...
	  'Visible', 'on', ...
	  'Resize', 'off');
  waitfor(ud.oh); % wait for the ok button to be deleted
  
  % resumed, other callbacks will have modified UserData by now (or may
  % have) so reload
  ud = get(fh, 'UserData');
  close(fh);
  r = ud.fil;
  return;

  % ------------------------------------------------
 case 'ok'
  % mfilename('ok')
  ud = get(gcbf, 'UserData');
  shortperiod = gettimespan(gettimespanbox(ud.shortH));
  longperiod = gettimespan(gettimespanbox(ud.longH));
  ud.fil = ...
      mia_filter_samnet('short', shortperiod, ...
			'long', longperiod);
  set(ud.fh, 'UserData', ud);
  
  % everything ok, just activate the previous waitfor by deleting ok
  % handle
  delete(ud.oh);
  
  % ------------------------------------------------
 case 'cancel'
  % mfilename('cancel')
  ud = get(gcbf, 'UserData');
  % put original data back
  ud.fil = ud.originalFil;
  set(ud.fh, 'UserData', ud);
  
  % activate the previous waitfor by deleting ok handle (if it exists)
  if ishandle(ud.oh)
    delete(ud.oh);
  else
    % (l)user pressed CTRL-C
    delete(ud.fh);
  end
  
  % ------------------------------------------------
 otherwise
  error('unknown action');
end

