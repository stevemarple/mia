function r = filter(fil, mia)
%FILTER Replace data with data interpolated between user-defined constraints.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_INTERP object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_INTERP, MIA_BASE.

% convert all times to sample numbers
% mia
st = getstarttime(mia);
% res = getresolution(mia);
sampletime = getsampletime(mia);
sampletime_cdf = getcdfepochvalue(sampletime);

% FIL_CONSTRAINTS_Y = fil.constraints.y

% for n = 1:len
%   tmp = fil.constraints.x{n}
%   if isa(tmp, 'timespan') & isa(sampletime, 'timestamp')
%     tmp = st + tmp;
%   end
%   % tablex(n) = ((tmp - st) / res) + 1;
%   tablex(n) = getcdfepochvalue(tmp);
%   tabley(n) = fil.constraints.y(n);
% end

for n = 1:numel(fil.constraints.x)
  disp(sprintf('%s: %f', char(fil.constraints.x(n)), fil.constraints.y(n)));
end

dateprintf(min(sampletime), max(sampletime))
dateprintf(min(fil.constraints.x), max(fil.constraints.x))

if isa(fil.constraints.x, 'timespan') & isa(sampletime, 'timestamp')
  tablex = getcdfepochvalue(fil.constraints.x + st);
else
  tablex = getcdfepochvalue(fil.constraints.x);
end
% tablex = getcdfepochvalue(fil.constraints.x);

tabley = fil.constraints.y;

% now sort tablex into ascending order
[tablex index] = sort(tablex);
tabley = tabley(index); % rearrange in same way

data = getdata(mia);
dsz = getdatasize(mia);
fildata = interp1(tablex, tabley, sampletime_cdf, fil.interpmethod);
% repeat for all rows (handle n-dimensional cases)
fildata2 = repmat(fildata, prod(dsz(1:(end-1))), 1);
fildata2 = reshape(fildata2, dsz);


if 0
figure;
xdata = getcdfepochvalue(sampletime);
ydata = getdata(mia);
plot(xdata, ydata(1,:), 'b');
hold on;

plot(tablex, tabley, 'r-+');

plot(xdata, fildata2(1,:), 'g');
end


if ~fil.extrapolate
  % replace only data between constraints; merge existing data into the
  % filtered data
  
  % tidx = find(sampletime_cdf >= tablex(1) & sampletime_cdf <= tablex(end));
  s.type = '()';
  s.subs = repmat({':'}, [1 numel(dsz)]);
  s.subs{end} = find(~(sampletime_cdf >= tablex(1) & ...
                       sampletime_cdf <= tablex(end)));
  
  if matlabversioncmp('>', '5.1')
    fildata2 = subsasgn(fildata2, s, subsref(data, s));
  else
    fildata2 = subsasg2(fildata2, s, subsref2(data, s));
  end
end

r = setdata(mia, fildata2);
r = addprocessing(r, getprocessing(fil, mia));
