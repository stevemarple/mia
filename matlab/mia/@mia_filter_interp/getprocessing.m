function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil)
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_INTERP object
%
%   See also mia_base/ADDPROCESSING


r = sprintf('Filter %s applied: method: ''%s''; #constraints: %d', ...
	    class(fil), char(fil.interpmethod), length(fil.constraints.y)); 



p = getparameters(mia);
pn = getparametername(mia);
pn = pn{1 + length(p) > 1}; % take plural version if many

if isnumeric(p)
  r = sprintf('%s; %s: %s', r, pn, printseries(p));
elseif localCellOfChars(p) & length(p) >= 1
  r = [sprintf('%s; %s: %s', r, pn, p{1}) ...
       sprintf(', %s', p{2:end})];
end


function r = localCellOfChars(a)
if iscell(a)
  r = 1;
  for n = 1:prod(size(a))
    if ~ischar(a{n})
      r = 0;
      break;
    end
  end
else
  r = 0;
end
