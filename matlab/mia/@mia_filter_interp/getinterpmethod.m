function r = getinterpmethod(fil)
%GETINTERPMETHOD Return the interpolation method
%
%   r = GETINTERPMETHOD(fil);
%   r: interp method (CHAR)
%   fil: MIA_FILTER_INTERP object

r = fil.interpmethod;
