function r = char(fil)
%CHAR Convert FILINTERP object to char.
%
%   r = CHAR(fil)
%   r: CHAR representation
%   fil: FILINTERP object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

if 0
  cons = length(fil.constraints.y);
else
  cons = {};
  for n = 1:numel(fil.constraints.x)
    cons{n} = sprintf('%s, %g', char(fil.constraints.x(n)), ...
                      fil.constraints.y(n));
  end
  cons = join(sprintf('\n                '), cons);
end

r = sprintf(['%s' ...
	     'method        : %s\n' ...
	     'constraints   : %s\n'], ...
	    mia_filter_base_char(fil), fil.interpmethod, cons);

