function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_FIXED object
%
%   See also MIA_FILTER_FIXED.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'interpolation';
  
 case 'u'
  r = 'INTERPOLATION';
  
 case {'c' 'C'}
  r = 'Interpolation';
 
 otherwise
  error('unknown mode');
end

return
