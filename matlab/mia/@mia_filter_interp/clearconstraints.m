function r = clearconstraints(fil)
%CLEARCONSTRAINTS Remove all existing contraints from a filter
%
%   r = CLEARCONSTRAINTS(fil)
%   r: modified MIA_FILTER_INTERP object
%   fil: MIA_FILTER_INTERP object
%
% Remove the constraining points from a MIA_FILTER_INTERP object.
%
% See also MIA_FILTER_INTERP, ADDCONSTRAINTS, MAXCONSTRAINTS

r = fil;
r.constraints.x = {};
r.constraints.y = [];
