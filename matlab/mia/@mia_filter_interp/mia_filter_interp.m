function fil = mia_filter_interp(varargin)


[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% fields
latestversion = 2;
fil.versionnumber = 1; % integer
fil.interpmethod = 'spline';
fil.constraints.x = [];
fil.constraints.y = [];
% version 2 and above
fil.extrapolate = false;

if nargin == 0
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif nargin == 1 & ischar(varargin{1})
  % check that the methods is allowed
  if ~any(strcmp(varargin{1}, interpmethods(feval(cls))))
    error(sprintf('%s is not a valid interpolation method for %s', ...
		  varargin{1}, cls));
  end
  fil.interpmethod = varargin{1};
  p = feval(parent);
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;


