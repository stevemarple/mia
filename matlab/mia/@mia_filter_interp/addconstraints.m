function r = addconstraints(fil, x, y)
%ADDCONSTRAINTS  Add interpolation constraints to MIA_FILTER_INTERP filter.
%
%   r = ADDCONSTRAINTS(fil, x, y);
%   r: modified MIA_FILTER_INTERP object
%   fil: MIA_FILTER_INTERP
%   x: x (mantissa) value(s)
%   y: y (ordinate) value(s)
%
% The coordinate vectors, x and y, must be the same size, and be row
% vectors.
%
% See also MIA_FILTER_INTERP, CLEARCONSTRAINTS, MAXCONSTRAINTS.

r = fil;
if ~isequal(size(x), size(y))
  error('x and y constraints must be the same size')
elseif size(x, 1) ~= 1 
  error('x must be a row vector')
elseif size(y, 1) ~= 1
  error('y must be a row vector')
end


if iscell(x)
  %   r.constraints.x = {r.constraints.x{:} x{:}};
  error('cannot be cell');
  if isempty(r.constraints.x)
    r.constraints.x = repmat(x{1}, 0, 0);
  end

  r.constraints.x = [r.constraints.x(:)' x{:}];
else
  if isempty(r.constraints.x)
    r.constraints.x = repmat(x, 0, 0);
  end

  % r.constraints.x = {r.constraints.x{:} x(:)};
  r.constraints.x = [r.constraints.x(:)' x(:)'];
end

r.constraints.y = [r.constraints.y(:)' y(:)'];

