function r = maxconstraints(fil)
%MAXCONSTRAINTS Get the maximum constraints for MIA_FILTER_INTERP object
%
%   r = MAXCONSTRAINTS(fil)
%   r: integer value
%   fil: MIA_FILTER_INTERP object
%
%   See also MIA_FILTER_INTERP, MINCONSTRAINTS.

r = inf;


