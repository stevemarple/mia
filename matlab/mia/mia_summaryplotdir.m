function r = mia_summaryplotdir
%MIA_DATADIR  Return the base directory where the MIA summary plots are stored.
%
%   r = MIA_DATADIR;
%
%   See also MIA_BASEDIR, MIA_DATADIR

r = getenv('MIA_SUMMARYPLOTDIR');
if ~isempty(r)
  return
end

% Indicate possible directories
if isunix
  d = {'/summary' '/data/summary'};

elseif ispc
  d = {}
  
else
  error('computer system unknown to MIA');
end

% Test the possible directories
for n = 1:length(d)
  r = d{n};
  if exist(r, 'dir')
    return; % directory found
  end
end
error('cannot find MIA summary data directory');
  
