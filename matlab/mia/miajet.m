function r = miajet(varargin)
%MIAJET  Variant of JET COLORMAP.
%
%   MIAJET(M), a COLORMAP variant of JET(M), but the color ranges from black
%   through blue, green, yellow and orange to bright red then dark red. Use
%   COLORMAP(MIAJET).
%
%   See also JET, COLORMAP, RGBPLOT.

if nargin
  sz = varargin{1};
else
  sz = size(colormap, 1);
end
r = zeros(sz, 3);
black2blue = 0:0.125:0.5;
bbl = length(black2blue);
r(1:bbl, 3) = black2blue';
r((bbl+1):sz,:) = jet(sz-bbl);

