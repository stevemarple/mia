function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_WEIGHTED_AVERAGE object
%
%   See also MIA_FILTER_WEIGHTED_AVERAGE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'weighted average';
  
 case 'u'
  r = 'WEIGHTED AVERAGE';
  
 case 'c'
  r = 'Weighted average';
 
 case 'C'
  r = 'Weighted Average';
 
 otherwise
  error('unknown mode');
end

return

