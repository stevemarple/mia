function r = filter(fil, mia)
%FILTER Filter data with a sliding average.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_WEIGHTED_AVERAGE object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_WEIGHTED_AVERAGE, MIABASE.

r = mia;

N = prod(size(mia));
if N ~= 1
  for n = 1:N
    [tmp mfname] = fileparts(mfilename);
    r(n) = feval(mfname, fil, mia(n));
  end
  return
end

if getndims(mia) ~= 2
  error('%s currently only tested on 2D data', getname(fil));
end

origRes = getresolution(mia);
blockSize = getblocksize(fil, mia);
cb_2 = ceil(blockSize/2);
fb_2 = floor(blockSize/2);

if blockSize == 1
  disp(sprintf('window too small to filter data at %s resolution', ...
	       char(origRes)));
  return;
elseif isnan(blockSize)
  error('resolution must decrease by an integer factor');
end


residue = getresidue(fil, mia);

data = getdata(mia);

% create the coefficients
coeffs = localGetCoeffs(blockSize)
% plot(coeffs);


% WRONG! 
% originalsz = size(data) % single image would be 2D!!!!

% ensure single images have size vector [1x3]
originalsz = getdatasize(mia);
numOfBlocks = ceil((originalsz(end) - sum(residue)) / cb_2);

% convert to a 2D array (rows will be independent so should be safe)
datasz = [prod(originalsz(1:(end-1))) originalsz(end)];
data = reshape(data, datasz);

if ~isequal(datasz, originalsz)
  % to make inserting the data easier later pretend we had 2D data from the
  % start
  r = setdata(r, data);
end

coeffs2 = repmat(coeffs, datasz(1), 1);
newdata = zeros(datasz(1), numOfBlocks);
m = 0;
ii = -fb_2:fb_2;
for n = cb_2:cb_2:(datasz(2)-fb_2)
  m = m + 1;
  newdata(:, m) = sum(data(:, ii+n) .* coeffs2, 2);
end

r = setdata(r, newdata);
r = addprocessing(r, getprocessing(fil, mia));

if ~isequal(datasz, originalsz)
  % we reshaped the data - better put it back
  r = setdata(r, reshape(getdata(r), originalsz));
end

st = getstarttime(r);
st = st + residue(1) * origRes;
r = setstarttime(r, st);
r = setendtime(r, m*fil.resolution + st);
r = setresolutionfield(r, fil.resolution);
r = setintegrationtime(r, getintegrationtime(r) * (fil.resolution/origRes));

function r = localGetCoeffs(n)
warning('fix the coefficients to be correct');
tmp = hanning(n)';
r = tmp./sum(tmp);

return;

c = fix(n/2)+1 % mean 
x = 1:n;
% constant to convert desired bandwidth to standard deviation
C = 1 / sqrt(2*log(2));
C = sqrt(log(2)/2) / 2;

sigma = c * C;
% gaussian membership function
r = exp(-(x - c).^2/(2*sigma^2));
r = r / sum(r); % normalise so that the weights sum to 1
plot(r);grid on
