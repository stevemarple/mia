function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_WEIGHTED_AVERAGE object
%
%   See also MIA_FILTER_WEIGHTED_AVERAGE.

% calculate real window size used
r = sprintf(['Filter %s applied; resolution: %s, ' ...
	     'window function: %s, window size: %d'], ...
	    getname(fil), char(fil.resolution), ...
	    char(fil.window), getblocksize(fil, mia));

