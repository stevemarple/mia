function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_SRM_DESPIKE object
%
%   See also MIA_FILTER_SRM_DESPIKE, MIA_FILTER_AS_DESPIKE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'SRM despike';
  
 case 'u'
  r = 'SRM DESPIKE';
  
 case 'c'
  r = 'SRM despike';
 
 case 'C'
  r = 'SRM Despike';
 
 otherwise
  error('unknown mode');
end

return

