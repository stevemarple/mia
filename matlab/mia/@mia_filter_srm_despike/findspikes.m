function r = findspikes(fil, mia)
%FINDSPIKES  Return the location of spikes using Steve Marple's algorithm.
%
%   r = FINDSPIKES(fil, mia)
%   r: vector indicating the location of spikes
%   fil: MIA_FILTER_SRM_DESPIKE object
%   mia: object derived from MIA_BASE
%
%  This algorithm is shamelessly borrowed by MIA_FILTER_AS_DESPIKE, with
%  a tiny modification to check adjacent points
%
%   See also MIA_FILTER_SRM_DESPIKE, MIA_FILTER_AS_DESPIKE.

r = mia_filter_as_despike_findspikes(fil, mia);

sz = size(r);
if length(sz) > 2
  error('only know how to deal with 2D data matrices');
end

data = getdata(mia);
idx = find(sum(r, 1)); % positions of spikes

minspike = getminimumspike(fil);

for n = idx
  if n > 1 & n < sz(2)
    for m = 1:sz(1)
      interpdata = interp1([-1 +1], data(m, n+[-1 +1]), 0);
      r(m, n) = abs(data(m, n) - interpdata) > minspike;
    end
  end
end

% ensure the minimum number of correlations criteria is met
r = r & repmat(sum(r) >= getminimumcorrelation(fil), sz(1), 1);
