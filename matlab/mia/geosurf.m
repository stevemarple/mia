function r = geosurf(varargin)

defaults.axes = [];
defaults.latitude = [];
defaults.longitude = [];
defaults.data = [];
defaults.maxlength = 1000e3; % max length (metres) to interpolate over
defaults.maxtrianglelength = 400e3;

[defaults unvi] = interceptprop(varargin, defaults);

len = prod(size(defaults.latitude));
if ~isequal(len, prod(size(defaults.longitude))) | ...
      ~isequal(len, prod(size(defaults.data)))
  error('number of data points incorrect');
end

if isempty(defaults.axes)
  defaults.axes = gca;
end

% find appropriate method to use to place patch on axis
if ismmap(defaults.axes)
  defaults.patchfunction = 'm_patch';
else
  defaults.patchfunction = 'patch';
end

tri = sphdelaunay('latitude', defaults.latitude, ...
		  'longitude', defaults.longitude);

% display triangles
p = local_displaytri(defaults, tri, ...
		     defaults.latitude, defaults.longitude, ...
		     defaults.data);




function r = local_trilengths(tri, lat, lon)
r = zeros(1, 3);
r(1) = m_lldist(lon(tri([2 3])), lat(tri([2 3]))); % not 1
r(2) = m_lldist(lon(tri([1 3])), lat(tri([1 3]))); % not 2
r(3) = m_lldist(lon(tri([1 2])), lat(tri([1 2]))); % not 3


function r = local_displaytri(defaults, tri, lat, lon, data, varargin)
if ~length(varargin)
  depth = 1;
else
  depth = varargin{1};
end

r = [];

for n = 1:size(tri, 1)
  tri2 = tri(n, :);
  trilen = local_trilengths(tri2, lat, lon);
  if max(trilen) < defaults.maxlength & depth < get(0, 'RecursionLimit') -50
    % disp(sprintf('(%d) min trilen: %g', depth, min(trilen)));
    % if min(trilen) > defaults.maxtrianglelength
    if max(trilen) > defaults.maxtrianglelength
      % split up and display in parts
      [newtri newlat newlon newdata] = ...
	  local_split_triangle(defaults, lat(tri2), lon(tri2), data(tri2));
      tmp = local_displaytri(defaults, newtri, newlat, newlon, newdata, ...
			     depth+1);
      r = [r tmp];
    else
      % display
      if depth == 1
	col = 'k';
      else
	col = 'w';
      end
      r(end+1) = feval(defaults.patchfunction, lon(tri2), lat(tri2), ...
		     'w', 'ZData', data(tri2), ...
		     'EdgeColor', col, 'FaceColor', 'none');
      % 'EdgeColor', 'none', 'FaceColor', 'interp');
    
    end
  else
    % too big
    feval(defaults.patchfunction, lon(tri2), lat(tri2), ...
	  'w', 'ZData', data(tri2), 'EdgeColor', 'r', 'FaceColor', 'none');
  end
end


function [tri, lat2, lon2, data2] = local_split_triangle(defaults, lat, ...
						  lon, data)
% Consider triangle:
%
%           V1
%           /\ 
%          /  \ 
%         /    \
%        /      \
%       /        \
%      /          \
%     /            \
% V2 /______________\ V3
%
% Break into:
%
%           V1
%           /\ 
%          /  \ 
%         / a  \
%     V4 /______\ V6
%       /\      /\
%      /  \ d  /  \
%     / b  \  / c  \
% V2 /______\/______\ V3
%
%           V5
%
% V4, V5 and V6 are just the midpoints (ie mean values) of (V1,V2), (V2,V3)
% and (V1,V3). They must be normalized back onto a sphere of unit radius.
%

V = cell(6, 1);

[x y z] = sph2cart(lat, lon, 1);

x(6) = mean(x([1 3]));
y(6) = mean(y([1 3]));
z(6) = mean(z([1 3]));
data(6) = mean(data([1 3]));

x(5) = mean(x([2 3]));
y(5) = mean(y([2 3]));
z(5) = mean(z([2 3]));
data(5) = mean(data([2 3]));

x(4) = mean(x([1 2]));
y(4) = mean(y([1 2]));
z(4) = mean(z([1 2]));
data(4) = mean(data([1 2]));
	  
[lon(4:6) lat(4:6)] = cart2sph(x(4:6), y(4:6), z(4:6));

lat2 = lat;
lon2 = lon;
data2 = data;

% start triangles at an original point, and continue CW
tri = [1 4 6;
       2 4 5;
       3 5 6;
       4 6 5;];


