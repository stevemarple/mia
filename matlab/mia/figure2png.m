function r = figure2png(h, filename, sz, varargin)
%FIGURE2PNG  Print a figure to a PNG file
%
%   FIGURE2PNG(h, filename, sz, ...)
%
%   h: FIGURE handle
%   filename: filename (optionally, with path)
%   sz: vector of output size
%
%   The following parameter name/values pairs are accepted:
%
%   'makedirectory', [0 | 1]
%
%   See also FIGURE, PRINT.

defaults.makedirectory = 0;
[defaults unvi] = interceptprop(varargin, defaults);

prop = get(h);
printresolution = 80; % dpi;

s.ResizeFcn = '';
s.Resize = 'off';
s.PaperOrientation = 'portrait';
s.PaperPositionMode = 'auto';
s.Units = 'inch';
s.Position = [0 0 sz ./ printresolution];

set(h, s);

disp(['printing to ' filename]);

if logical(defaults.makedirectory)
  mkdirhier(fileparts(filename));
end

print(h, '-dpng',  sprintf('-r%g', printresolution), filename);

% restore figure to previous state
fn = fieldnames(s);
for n = 1:length(fn)
  set(h, fn{n}, getfield(prop, fn{n}));
end
