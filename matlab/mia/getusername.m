function username = getusername
%GETUSERNAME  Return the unix (or other) username
%
%   username = GETUSERNAME
%   username: CHAR
%
%   Return the username of the user executing the command. Inspects the
%   USERNAME and USER environment variables, else defaults to
%   system-specific methods.
%
%   See also GETENV, UNIX.

username = getenv('USERNAME');
if isempty(username)
  username = getenv('USER');
end
if isempty(username)
  username = getenv('LOGNAME');
end

if isempty(username) 
  disp('Empty username')
  switch computer
    case 'LNX86'
      % last ditch attempt for linux
      [status username] = unix('id -u -n');
      if status
	% something went wrong
	username = ['unknown ' computer ' user'];
	return;
      end
      
    otherwise 
      username = ['unknown ' computer ' user'];
  end
end


l = length(username);
% remove trailing newline/carriage return
while l > 0 & (username(l) == 10 | username(l) == 13)
  username = username(1:(l-1));
  l = l - 1;
end
return;

