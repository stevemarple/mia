function r = join(a, b)
%JOIN  Join strings together (like Perl's join function).
%
%   r = JOIN(a, b)
%   r: joined string (CHAR)
%   a: string inserted between strings (CHAR)
%   b: CELL array of CHAR
%
% See also SPLIT, COMMIFY.

if ~iscell(b)
  error('invalid parameters');
end

num = numel(b);
switch num
 case 0
  r = '';

 case 1
  r = b{1};
  
 otherwise
  r = b{1};
  for n = 2:num
    r = [r a b{n}];
  end

end


