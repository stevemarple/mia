function r = floor2(a, b)
%FLOOR2  Like FLOOR, but floor to nearest multiple of b.
%
%   r = FLOOR2(a, b)
%
%   r: matrix of same size as a
%   a: matrix to be floored
%   b: scalar, or matrix the same size as a, defining the boundary to
%   round down to.
%
%   See also FLOOR, CEIL2, ROUND2.

if prod(size(b)) == 1 & prod(size(a)) ~= 1
  b = repmat(b, size(a));
elseif ~isequal(size(a), size(b))
  error('b must be a scalar or a matrix the same size as a');
end

r = floor(a ./ b) .* b;

