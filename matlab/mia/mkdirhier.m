function [status, msg] = mkdirhier(dirname)
%MKDIRHIER Make a directory structure by calling the system's mkdir command
%
%   [status, msg] = MKDIRHIER(dirname)
%   status: status from mkdirhier
%   msg: text message returned by mkdirhier 
%   dirname: directory structure to make
%
%   If status is non-zero then the call to mkdirhier failed, msg contains
%   the error message return from mkdirhier. On Unix commands the -p
%   option is passed to mkdir. The directory name is quoted using
%   url_tools/SYSTEMQUOTE.
%
%   See also SYSTEMQUOTE.

d = systemquote(dirname); % from url_tools

if isunix
  cmd = sprintf('mkdir -p %s', d);
else
  cmd = sprintf('mkdir %s', d);
end

[status, msg] = system(cmd);

if nargout == 0 & status ~= 0
  % error condition but return argument not checked
  error(['mkdirhier: ' msg]);
end

