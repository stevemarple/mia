function r = mia_version
%MIA_VERSION  Return the version of MIA in use.
%
%   r = MIA_VERSION
%   r: CHAR
%
% MIA_VERSION returns the version of MIA in use. MIA_VERSION attempts to
% use the svnversion command (if it exists) to ascertain the subversion
% revision number. If the svnversion command does not exist or returns
% "exported" then the contents of the VERSION.txt file (created
% automatically by update_mia_snapshot) is read to deteremine the current
% version.
%
% If the version still cannot be determined MIA_VERSION returns
% 'unknown'.
%
% Example return values are given below (see svnversion --help for the
% full list of styles it my return):
%
% '400':
%     MIA version 400, either from a snapshot or an unmodified subversion
%     repository.
%
% '400M': MIA version 400 with local modifications (from a MIA
%     repository).
%
% 'unknown':
%     Either svnversion does not exist (or not in the path) or the
%     MIA_BASEDIR is exported but missing the VERSION.txt file.
%
% See also MIA_BASEDIR.

cmd = sprintf('svnversion %s', systemquote(mia_basedir));
[s w] = system(cmd);
w = deblank(w); % remove trailing newline etc

if s ~= 0 || strcmp(w, 'exported')
  % Exported or svnversion not present, look for VERSION.txt file
  fn = fullfile(mia_basedir, 'VERSION.txt');
  [fid mesg] = fopen(fn);
  if fid == -1
    r = 'unknown';
  else
    r = fgetl(fid);
    fclose(fid);
  end
else
  r = w;
end
  
