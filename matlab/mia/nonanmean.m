function r = nonanmean(data, varargin)
%NONANMEAN  Similar to MEAN, but ignore elements which are NAN.
%
%   r = NONANMEAN(x)
%   r = NONANMEAN(x, dim)
%
% Take the MEAN value ignoring the effect of any NANs. If there are no
% non-NaN elements then NAN is returned.
%
% See also MEAN, NONANSUM, NONANMEDIAN, NONANISEQUAL.

% sum, excluding the effects of nans
s = nonansum(data, varargin{:});

% count the number of non-NaN elements
div = sum(~isnan(data), varargin{:});

% if there are no non-NaN elements then ensure NaN is returned for the mean
% value. (This would actually happen since both s and div are 0, however
% avoid the divide by zero error by converting zero elements in div to
% NaNs).
div(div == 0) = nan;

% return the mean
r = s./div;

