function r = getparentclass(c)
%GETPARENTCLASS  Return the class name of the parent object
%
%   r = GETPARENTCLASS(d)
%   r: class name (CHAR)
%   d: data object
%
% GETPARENTCLASS returns an empty string if the input data object is not a
% class or does not have any parent objects. 
%
% See also GETANCESTORS.

if ~isobject(c)
  r = '';
  return
end
 
cs = struct(c);
fn = fieldnames(cs);

r = {};

% take advantage of the fact parents are placed last in the structure
for n = length(fn):-1:1
  if isa(c, fn{n})
    r{end+1} = fn{n};
  else
    break;
  end
end

switch length(r)
 case 0
  r = '';
  
 case 1
  r = r{1};
end
