function r = preallocate(mia, t)
%PREALLOCATE  Preallocate object fields to allow BUILD
%
%   r = PREALLOCATE(mia, tl)
%   r: MIA_IMAGE_BASE object with preallocated fields
%   mia: MIA_IMAGE_BASE object (derived classes not allowed) 
%   tl: size in time dimension
%
%   See also BUILD

if ~strcmp(class(mia), 'mia_image_base')
  
  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a PREALLOCATE command to use
  % CONSTRUCTFROMFILES or BUILD. Generate a runtime error with appropriate
  % error message to indicate what needs to be done. Derived classes MUST
  % call this function on their parent field.
  
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia; % copy basic things like units, start and end times etc
r.mia_base = preallocate(r.mia_base, t);

r = setdata(r,  ...
	    repmat(nan, [length(r.xpixelpos), length(r.ypixelpos), t]));

% r.imagedata = repmat(nan, [length(r.xpixelpos), length(r.ypixelpos),
% t]);

r.imagetime = timestamp(cell(1, t));
r.imagemidtime = r.imagetime; % need same size
 
