function r = char(mia, varargin)
%CHAR Convert MIA_IMAGE_BASE object into a CHAR representation.
%
%   s = CHAR(mia)
%   mia: MIA object
%   s: CHAR representation.
%
%   See also MIA_BASE/CHAR.

% NB Make all objects derived from mia_base print a trailing newline
% character 

if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

xpixelpos_str = printseries(mia.xpixelpos);
ypixelpos_str = printseries(mia.ypixelpos);
if ~isempty(mia.pixelunits)
  pixelunits_str = char(mia.pixelunits);
else
  pixelunits_str = '';
end

height_str = printunits(mia.height, 'm');

r = sprintf(['%s' ...
	     'X pixel positions : %s\n' ...
	     'Y pixel positions : %s\n' ...
	     'pixel pos. units  : %s\n' ...
	     'height            : %s\n'], ...
 	    mia_base_char(mia, varargin{:}), ...
	    xpixelpos_str, ypixelpos_str, ...
	    pixelunits_str, height_str); 

	    % char(mia.mia_base, varargin{:}), ...

