function [r, varargout] = getimagelabel(mia, defaults, varargin)
%GETIMAGELABEL  Return imagelabel information.
%
%   GETIMAGELABEL(mia, 'list')
%   mia: object derived from MIA_IMAGE_BASE 
%   Print to the console window the valid IMAGELABEL style options for
%   this type of data.
%
%   r = GETIMAGELABEL(mia, defaults, ...)
%   mia: MIA_IMAGE_BASE object
%   r: image label (CHAR)
%   defaults: defaults STRUCT from mia_imagebase/IMAGEPLOT.
%   
%   Optional parameters are parameter name/value pairs unknown to
%   IMAGEPLOT. The exact form of the returned string is dependent upon
%   the chosen style (stored in defaults.style).
%
%   [r1 r2 = GETIMAGELABEL(mia, 'list')
%   mia: MIA_IMAGE_BASE object
%   r1: valid style options, as CELL array of CHARs
%   r2: formatted names for style options, as CELL array of CHARs (for
%   use in menus, etc)
%   
%   Return a list of the valid style options, intended to GUI functions
%   which need to create an appropriate menu structure.

if isstruct(defaults)
    samt = getsampletime(mia);
  if numel(samt) ~= 1
    error('mia should contain one image');
  end  

  r = '';
  it = getintegrationtime(mia,1);
  switch defaults.style
   case 'integration'
    r = char(it);
    % case 'resolution'
    % r = char(getresolution(mia,1));
   case 'starttime'
    r = strftime(getsampletime(mia,1) - it./2, defaults.timeformat);
   case {'sampletime' 'midtime'}
    r = strftime(getsampletime(mia,1), defaults.timeformat);
    
   otherwise
    val = getimagelabel(mia, 'list');
    s2 = [val{1} sprintf(', %s', val{2:end})]; % insert commas
    warning(sprintf('unknown label style (was %s). Valid styles are: %s', ...
		    defaults.style, s2));
  end
elseif any(strcmp(defaults, {'list' 'help'}))
  % list known styles. NB none and number are supported in the imagelabel
  % function. This function cannot identify number, but knows it is an
  % option imagelabel supports, so we list it here
  
  tmp1 = {'starttime' 'sampletime' 'integration'};
  tmp2 = {'Image start time' ...
	  'Sample time' ...
	  'Integration duration'}; 
  if strcmp(defaults, 'list')
    r = tmp1;
    if nargout >= 2
      varargout{1} = tmp2;
    end
    return
  end
  
  % print help. Don't forget about the two styles handled by
  % getimagelabel, or any additional ones passed up from lower
  % getimagelabel functions
  if length(varargin) < 1
    varargin{1} = {};
  end
  if length(varargin) < 2
    varargin{2} = {};
  end
  tmp1 = {'none' 'number', tmp1{:} varargin{1}{:}};
  tmp2 = {'No label' 'Sequential image number' tmp2{:} varargin{2}{:}};
  
  fprintf(1, 'The following imagelabel styles are available for %s:\n\n', ...
	  class(mia));
  for n = 1:length(tmp1)
    disp(sprintf('%20s: %s', tmp1{n}, tmp2{n}));
  end
  return

end


