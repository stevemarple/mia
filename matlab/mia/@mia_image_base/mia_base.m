function r = mia_base(mia)
%MIA_BASE  Converter to base class for MIA_IMAGE_BASE class.
%
%   r = MIA_BASE(mia)
%   r: MIA_BASE object(s)
%   mia: MIA_IMAGE_BASE object(s)
%
%   See also MIA_IMAGE_BASE.

r = mia.mia_base;
