function [xi, yi, ti, w, defaults, unvi] = getpixelindices(mia, varargin)
%GETPIXELINDEXES  Return the indices for addressing pixels.
%
%   [xi, yi, ti, w, def, unvi] = GETPIXELINDEXES(mia, ...)
%
%   xi: indices of pixel elements in X dimension
%   yi: indices of pixel elements in Y dimension
%   ti: indices of elements in time dimension
%   w: CELL array of warnings
%
% The function accepts the following parameter name/value pairs:
%
%   'xpixelpos', DOUBLE
%   Restrict the spatial extent in the X dimension. The parameter value
%   is a vector of pixel locations. If any locations are not present then
%   a warning is generated.
%
%   'ypixelpos', DOUBLE
%   Restrict the spatial extent in the Y dimension. The parameter value
%   is a vector of pixel locations. If any locations are not present then
%   a warning is generated.
%
%   'starttime', TIMESTAMP
%   Restrict the data used based on time.
%
%   'endtime', TIMESTAMP
%   Restrict the data used based on time.
%
%   'tolerance', DOUBLE
%   Adjust the default tolerance value used when searching for pixel
%   positions.
%
% If any of the values are empty then that parameter is ignored.
%
% See also EXTRACT.

defaults.xpixelpos = [];
defaults.ypixelpos = [];
defaults.starttime = [];
defaults.endtime = [];
defaults.tolerance = [];

% Don't warn about unknown parameters, filter out only those which are
% needed, then it is acceptable for a function to pass all of its parameters
% to this function, and let this one sort out the indices, but do return the
% unknown ones so that the calling function can warn if it decides it is
% necessary.

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.tolerance)
  defaults.tolerance = 256 * eps;
end

sz = getdatasize(mia);
if length(sz) == 2
  sz(3) = 1;
end

w = {};

if isempty(defaults.xpixelpos)
  xi = 1:sz(1);
else
  [xi wtmp] = localFindIndices('X', defaults.xpixelpos, mia.xpixelpos, ...
			     defaults.tolerance);
  w = {w{:} wtmp{:}};
end

if isempty(defaults.ypixelpos)
  yi = 1:sz(2);
else
  [yi wtmp] = localFindIndices('Y', defaults.ypixelpos, mia.ypixelpos, ...
			     defaults.tolerance);
  w = {w{:} wtmp{:}};
end


if isempty(defaults.starttime) & isempty(defaults.endtime)
  ti = 1:sz(3);
else
  % one or both specified
  if isempty(defaults.starttime)
    defaults.starttime = getstarttime(mia);
  end
  if isempty(defaults.endtime)
    defaults.endtime = getendtime(mia);
  end
  samt = getsampletime(mia);
  if isempty(samt)
    ti = [];
  else
    it_2 = getintegrationtime(mia) ./ 2;
    ti = find(samt - it_2 >= defaults.starttime & ...
	      samt + it_2 <= defaults.endtime);
  end
end


return;

function [pi, w] = localFindIndices(dim, pixels, pixelsPresent, tolerance)
w = {};
pi = [];
for n = 1:prod(size(pixels))
  tmp = abs(pixelsPresent - pixels(n));
  idx = find(tmp <= tolerance);
  if isempty(idx)
    w{end+1} = sprintf('%s pixel value %g not found', dim, pixels(n));
  elseif length(idx) > 1
    w{end+1} = sprintf('Multiple matches for %s pixel value %g', dim, ...
		       pixels(n));
  else
    pi(end+1) = idx;
  end
end
