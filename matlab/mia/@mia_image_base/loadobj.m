function r = loadobj(a)
%LOADOBJ  Load filter for MIA_IMAGE_BASE object.
%

if isstruct(a)
  r = mia_image_base(a);
else
  r = a;
end

