function r = setpixelunits(mia, units)
%SETPIXELUNITS  Return the X and Y pixel units of a MIA_IMAGE_BASE object.
%
%   r = SETPIXELUNITS(mia, units)
%
%   r: modified MIA_IMAGE_BASE object
%   mia: MIA_IMAGE_BASE object
%   units: new units (CHAR)
%
%   See also MIA_IMAGE_BASE, mia_image_base/GETPIXELS.

r = mia;
r.pixelunits = units;
