function [data, xi, yi, ti] = keodata(mia, varargin)
%KEODATA  Produce data for a keogram
%
%   r = keodata(mia)
%   r = keodata(mia, ...)
%   r: data
%   mia: MIA_IMAGE_BASE object
%
%   KEODATA uses GETPIXELINDICES and so accepts its parameter name/value
%   pairs to restrict the spatial or temporal extent.
%
%   See also GETPIXELINDICES, KEOGRAM.

[xi yi ti w defaults unvi] = getpixelindices(mia, varargin{:});
for n = 1:length(w)
  % print the warnings
  warning(w{n});
end

if ~isempty(unvi)
  warning(['The following parameter names were not recognised: ' ...
           sprintf('%s ', varargin{unvi(1:2:end)})]);
end

if length(xi) > 1 & length(yi) > 1
  error('keogram coordinates must be constant over X or Y axes');
elseif length(xi) == 1 & length(yi) == 1
  % no point in a single-point keogram
  error('cannot construct a keogram for a single point');
end

if isdataempty(mia)
  data = zeros(numel(xi), numel(yi), numel(ti));
else
  data = getdata(mia, xi, yi, ti);
end

if length(xi) == 1
  datasz = [length(yi) length(ti)];
elseif length(yi) == 1
  datasz = [length(xi) length(ti)];
else
  error('unknown error'); % oops
end

data = reshape(data, datasz);
