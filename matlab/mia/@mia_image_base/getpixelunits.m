function r = getpixelunits(mia)
%GETPIXELUNITS  Return the X and Y pixel units of a MIA_IMAGE_BASE object.
%
%   r = GETPIXELUNITS(mia)
%
%   r: pixel units
%   mia: MIA_IMAGE_BASE object
%
%   See also MIA_IMAGE_BASE, mia_image_base/GETPIXELS.

r = mia.pixelunits;
