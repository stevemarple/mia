function r = setypixelpos(mia, ypix)
%SETYPIXELPOS  Set x pixel positions.
%
%   r = SETYPIXELPOS(mia, ypix)
%   r: modified MIA_IMAGE_BASE object
%   ypix: new Y pixel positions
%
%   See also SETYPIXELPOS, GETPIXELS.

r = mia;
r.ypixelpos = ypix;

