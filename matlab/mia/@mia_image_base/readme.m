function readme(varargin)
%README file for MIA_IMAGE_BASE class
%
% The MIA_IMAGE_BASE class is a "virtual base class", that is it is not
% intended to be instantiated itself, only classes derived from it should be
% instantiated.
%
% The important fields contained in the class are:
%
% imagedata: 3D (X, Y, t) matrix of images
% imagetime: 1D matrix of TIMESTAMPs for the start of each image
% resolution: 1D matrix of TIMESPANs, one per image
% integrationtime: 1D matrix of TIMESPANs, one per image
% midpointtime: 1D matrix of TIMESPANs, one per image
%
% NOTE
% Integrationtime is the time the image is exposed for
% Resolution is the time taken to complete the image taking process. 
% For the n-th image, the subsequent image (if one exists - it may not)
% should begin at imagetime(n) + resolution(n). Explanation why a
% midpoint time is needed follows below.
%
% Instruments which produce images may not do so at regular intervals
% (e.g. radars) or the periods of operation may be interrupted by bad
% fitting of the data (radars), clouds or changes in optical filters
% (all-sky cameras). For this reason all images are TIMESTAMPed, where the
% imagetime refers to the time the image started. The resolution indicates
% how often the images should be expected. For SCASI used with alternate OH
% filter/no filter the integration times vary, therefore the resolution
% (time of next image) also varies. A scenario as below is likely:
%
%   X: no filter
%   O: OH filter
%   .: readout
%
%   XXXX.OOOOOOOOOOOOO.XXXX.OOOOOOOOOOOOO.XXXX.OOOOOOOOOOOOO.XXXX.
%
%   If each character represents 30s then the integration time for no-filter
%   is 2 minutes, readout time 30s, while the integration time for the OH
%   filtered data is 6 minutes 30s, readout time 30s. On their own OH would
%   have a resolution of 2m 30s, and OH 7m. The total cycle time is 9m 30s.
%
% It does not make sense to post-integrate images exposed through different
% filters, so after separating the two sets of data the OH data would look
% like:
%
%   -: no data
%
%   -----OOOOOOOOOOOOO.-----OOOOOOOOOOOOO.-----OOOOOOOOOOOOO.-----
%
% and the no-filter data would look like:
%
%   XXXX.--------------XXXX.--------------XXXX.--------------XXXX.
%
%
% Post-integrating either dataset is now sensible. However, a question
% remains about how best to display post-integrated data which originates
% from discontinous time periods. From the discussion above it seems
% sensible to say that after post-integrating images in pairs the both data
% sets have a resolution of 2 * 9m 30s = 19 minutes. The OH data has an
% integration time of 13 minutes, and the no-filter data an integration time
% of 4 minutes. Suppose we wish to make a line plot of the intensity of a
% certain pixel value against time. The marker should be positioned on the X
% axis at the midpoint of the integration period. For a single integration
% period the midpoint is easily calculated. For post-integrated data with
% (essentially) continuous integration periods it is also trivial. However,
% given the information
%
%   resolution 19 minutes
%   integration time 13 minutes
%
% we have no way to tell the difference between a single 13 minute
% integration (in a 19 minute period) and post-integration of 2 images as
% shown above. The importance behind knowing that information is that
% without it we cannot properly place the line marker at the correct
% location on the X axis. This is critical for any study which relies on
% time information (i.e. any study which combines data from two or more
% instruments).
%
% One solution is to remember how many images were used in the
% post-integration. This approach does not work if the images to be
% post-integrated had different integration times. An alternative method is
% when post-integrating to calculate (and remember) the time at which half
% the integration time has passed. If this problem is applied recursively,
% so that we have post-integration of post-integrated images with different
% post-integrated integration times the midpoint approach would still work
% (assuming we use a weighted sum).
%
%
% COORDINATE SYSTEMS
%
% A number of coordinate systems are in use:
% 'deg'   
%   degrees longitude/latitude
%
% 'aacgm' 
%   altidude-adjusted corrected geomagnetic degrees longitude/latitude
%
% 'm'
%   metres from instrument location, Y axis aligned with true north
%
% 'm_antenna'
%   metres from instrument location, Y axis aligned with antenna
%   pointing direction
%
% 'km', 'km_antenna'
%   km representations, now deprecated since the toolkit should use SI
%   units throughout
%
% See also MIA_IMAGE_BASE, TIMESTAMP, TIMESPAN.

% if called as a function show this file as if "help readme" was typed.
[st idx] = dbstack;
help(st(1).name);
