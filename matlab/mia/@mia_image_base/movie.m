function movie(mia, varargin)
%MOVIE  Save as an animated GIF movie.
%
%   MOVIE(mia, ...)
%
%   mia: MIA_IMAGE_BASE object
%
%   The following parameter name value pairs are recognised:
%
%     'filename', CHAR
%     The filename to save as (default is 'untitled.gif')
%
%     'makedirectory', LOGICAL
%     Flag to indicate if the directory part of the name should be
%     created if needed.
%
%     'starttime', TIMESTAMP
%     The time the movie should start.
%
%     'endtime', TIMESTAMP
%     The time the movie should end.
%     
%     'delay', TIMESPAN
%     Delay between frames, must be multiples of 1/100th of a second (see
%     gifsicle(1)
%
%     'scale', [1x2 DOUBLE]
%     Scaling factor used by gifsicle(1).
%
%     'loopcount', DOUBLE
%     Number of times the movie will be shown, default is 0 (continous
%     playback).
%
%     'colormap', DOUBLE
%     Colormap used for the movie (default is JET(256)).
%
%     'optimize', LOGICAL
%     Specify --optimize when calling gifsicle(1).
%
%     'clim', [1x2 DOUBLE]
%     Minimum and maximum values used when producing the movie.
%
%     'timebar', LOGICAL
%     Flag to control if a timebar should be included.
%
%     'timebarcolor', [1x3 DOUBLE]
%     Color of the timebar, numbers should be in range 0->1
%
%     'timebargap', [DOUBLE]
%     The height of the gap between the colorbar and the bottom edge of
%     the image, in pixels (and before scaling effects by gifsicle(1)).
%
%     'timebarheight', [DOUBLE]
%     The height of the color bar, in pixels (and before scaling effects by
%     gifsicle(1)).
%
%     'preliminary', LOGICAL
%     Flag to indicate that the movie is preliminary.
%
%     'infofile', CHAR
%     The name of the information file containing information about the
%     movie. An empty filename means the file will not be produced
%     (the default). The fields are:
%        lower limit: DOUBLE
%        upper limit: DOUBLE
%        preliminary flag: [0|1]
%        units: CHAR
%        height: DOUBLE
%        pixel units: CHAR
%        X pixels min: DOUBLE
%        X pixels max: DOUBLE
%        Y pixels min: DOUBLE
%        Y pixels max: DOUBLE
%
%     'htmlfile', CHAR
%     The name of a file containing some HTML used to show the movie. It
%     is assumed that the movie file will be in the same directory as
%     this file.  An empty filename means the file will not be produced
%     (the default).
%
%

[Path filename ext] = fileparts(getfilename(mia));
if ~isempty(filename)
  % replace any extension with '.gif'
  defaults.filename = fullfile(Path, [filename '.gif']);
else
  defaults.filename = 'untitled.gif';
end
defaults.makedirectory = 0;
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
defaults.delay = timespan(100,'ms');
defaults.scale = [1 1];
defaults.loopcount = 0;
defaults.colormap = jet(256);
defaults.optimize = 0;

defaults.clim = getclim(mia);
defaults.heaterevent = [];

defaults.timebar = 1;
defaults.timebarcolor = [1 0 0];  % R G B
defaults.timebargap = 2;
defaults.timebarheight = 3;

defaults.preliminary = 0;
defaults.preliminarytext = 'Preliminary';

defaults.infofile = ''; % filename of info file
defaults.htmlfile = ''; % filename of HTML file

defaults.method = '';

[defaults unvi] = interceptprop(varargin, defaults);

if ~isequal(floor(defaults.scale), defaults.scale)
  error('scaling must be integer value(s)');
end
if length(defaults.scale) == 1
  defaults.scale(2) = defaults.scale(1);
end


defaults.preliminary = logical(defaults.preliminary);

if defaults.optimize
  optimizeStr = sprintf(' -O%d ', fix(max(min(defaults.optimize, 2), 0)));
else 
  optimizeStr = '';
end


m = 1;
% index to valid images
samt = getsampletime(mia);
idx = find(samt >= defaults.starttime & samt < defaults.endtime);

% imdata = mia.imagedata(:,:,idx);
dsz = getdatasize(mia);
if isdataempty(mia)
  imdata = repmat(nan, [dsz(1:2) 1]);
else
  imdata = getdata(mia,':',':',idx);
end

if isnan(defaults.clim(2))
  defaults.clim(1) = min(imdata(:));
end
if isnan(defaults.clim(2))
  defaults.clim(2) = max(imdata(:));
end


clear imdata; % reclaim memory

imgdir = tempname2('', '.d');
[imgParentDir imgDirName imgDirExt] = fileparts(imgdir);
[success mesg] = mkdir(imgParentDir, [imgDirName imgDirExt]);
if ~success
  error(sprintf('could not create directory %s: %s', imgdir, mesg));
end

imgfiles = {};
if matlabversioncmp('<', '5.2')
  imgfilesfmt = 'gif';
else
  imgfilesfmt = 'png';
end

% number images with leading zeros. Amount of leading zeroes required
% depends on largest number. Calculate width required.
imgfilenamewidth = ceil(log10(max(idx)+1));
if isempty(imgfilenamewidth)
  imgfilenamewidth = 1;
end

if isempty(samt)
  data = repmat(nan, [dsz(1:2) 1]);
  samt = defaults.starttime;
  idx = 1;
else
  data = getdata(mia);
end

for n = idx
  t = samt(n);
  
  if t >= defaults.starttime & t < defaults.endtime

    % imgfiles{m} = tempname;
    % number images with suitable amount of leading zeros
    imgfiles{m} = sprintf('%0*d.%s', imgfilenamewidth, m, imgfilesfmt);
    imgfiles{m} = fullfile(imgdir, imgfiles{m});
    
    cmapSz = size(defaults.colormap, 1);
    % image as integer
    %     im = flipud(1 + round((cmapSz - 1) * ...
    % 			  (getdata(mia,':',':',n)' - defaults.clim(1)) ...
    % 			  ./  (defaults.clim(2) - defaults.clim(1))));
    
    im = flipud(1 + round((cmapSz - 1) * ...
			  (data(:,:,n)' - defaults.clim(1)) ...
			  ./  (defaults.clim(2) - defaults.clim(1))));

    % convert anything outside of limits to the limiting values
    im(find(im > cmapSz)) = cmapSz;
    im(find(im < 1)) = 1;
    
    % convert image to true colour
    [nansr nansc] = find(isnan(im));  % remember what was nan
    if ~isempty(nansr) % needed for matlab 5.1 compatibility
      im(nansr, nansc) = 1;
    end
    imc = reshape(defaults.colormap(im,:), size(im,1), size(im,2), 3);
    
    imc(nansr, nansc, :) = nan; % convert back
    
    if ~isempty(defaults.heaterevent)
      resolution = getresolution(mia);
      imc = onofftimes(defaults.heaterevent, [], ...
		       varargin{unvi}, ...
		       'style', 'imagepatch', ...
		       'image', imc, ...
		       'plotstarttime', samt(n), ...
		       'plotendtime', samt(n) + resolution);
    end
    
    if defaults.timebar
      imcsz = size(imc);
      % add blank pixels
      imc(imcsz(1) + (1:(defaults.timebargap+defaults.timebarheight)), ...
	  :, :) = nan;
      % % size(imc)
      % % imcsz(1) + 1:(defaults.timebargap+defaults.timebarheight)
      % fill timebar
      timebarX = round(imcsz(2) * ...
		       ((samt(n) - defaults.starttime) / ...
			(defaults.endtime - defaults.starttime)));
      for c = 1:3
	imc(imcsz(1)+defaults.timebargap+(1:defaults.timebarheight), ...
	    1:timebarX, c) = defaults.timebarcolor(c);
      end
      
    end

    switch imgfilesfmt
     case 'gif'
      imwrite(imc, imgfiles{m}, 'gif');
      
      case 'png'
      imwrite(imc, imgfiles{m}, 'png', 'Alpha', double(~isnan(imc(:,:,1))));
     
     otherwise
      error('unknown format');
    end
    
    m = m + 1;	
  end
  
end

% animate the gifs
switch computer 
  case {'LNX86' 'GLNX86', 'GLNXA64'}
   switch char(defaults.method)
     case 'gifsicle'
      if defaults.loopcount < 1
	loop = 'forever';
      else
	loop = int2str(defaults.loopcount);
      end
      commandStr = sprintf(['gifsicle --delay %d --scale %dx%d ' ...
		    '--loopcount=%s %s %s > %s'], ... 
			   round(defaults.delay/timespan(100,'ms')), ...
			   defaults.scale([1 2]), ...
			   loop, ...
			   optimizeStr, ...
			   sprintf(' %s', imgfiles{:}), defaults.filename);
      
     case {'' 'convert' 'imagemagick'}
      if ~isequal(defaults.scale, [1 1])
	scaleStr = sprintf(' -scale %dx%d ', ...
			     defaults.scale([1 2]) .*  dsz([1 2]));
      else
	scaleStr = '';
      end
      
      commandStr = ...
	  sprintf(['convert -delay %d' ...
		   '%s' ...
		   ' -loop %d %s %s'], ... 
		  round(defaults.delay/timespan(100,'ms')), ...
		  scaleStr, ...
		  defaults.loopcount, ...
		  sprintf(' %s', imgfiles{:}), defaults.filename);
    end
    if logical(defaults.makedirectory)
      dir_to_make = fileparts(defaults.filename);
      if ~isempty(dir_to_make)
	mkdirhier(dir_to_make);
      end
    end
    
    cmd_file = fullfile(imgdir, 'command.sh');
    [cmd_fid msg] = fopen(cmd_file, 'w');
    if cmd_fid == -1
      error(sprintf('could not open %s: %s', cmd_file, msg));
    end
    fprintf(cmd_fid, '#!/bin/sh\n');
    fprintf(cmd_fid, '%s\n', commandStr);
    fclose(cmd_fid);
    if system(['chmod a+rx ' cmd_file]) ~= 0
      error(sprintf('could not chmod command file %s', cmd_file));
    end
        
    % [status, msg] = system(commandStr)
    [status, msg] = system(cmd_file);
    
    if status ~= 0
      % commandStr
      commandStr(1:min(end, 80))
      
      if ~isempty(msg)
	if msg(end) == char(10)
	  msg(end) = []; % remove trailing newline
	end
      end
      length(commandStr)
      error(sprintf('cannot system cmd: %s', msg));
    end
        
  otherwise
    error(['Do not know how to animate GIFs. Please add for ' ...
	  computer ' to ' mfilename]);
end


if ~isempty(defaults.infofile)
  localsavemovieinfo(mia, defaults);
end
if ~isempty(defaults.htmlfile)
  localsavehtml(mia, defaults);
end

delete(imgfiles{:});
rmdir(imgdir, 's');


% ----------------------------------------------
function localsavemovieinfo(mia, defaults)
%SAVEMOVIEINFO Save MOVIE information for the summary plots pages
%
%   SAVEMOVIEINFO(mia, defaults)
%
%   mia: MIA_IMAGE_BASE object
%   filename: filename to save to
%   prelim: flag indicating if preliminary data
%
%   This function intended for use by SUMMARYPLOT functions only.
%
%   See SUMMARYPLOT.

if defaults.makedirectory
  mkdirhier(fileparts(defaults.infofile));
end
[fid msg] = fopen(defaults.infofile, 'w');
if fid == -1
  error(sprintf('Cannot open %s: %s', defaults.infofile, msg));
end

[xp yp pixunits] = getpixels(mia);
units = getunits(mia);
if isempty(units)
  units = '?';
end
h = getheight(mia);
if isempty(h)
  h = nan;
end

fprintf(fid, '%g %g %d %s %g %s %g %g %g %g\n', ...
	getlimits(mia), defaults.preliminary, ...
	units, h, pixunits, min(xp), max(xp), ...
	min(yp), max(yp));
fclose(fid);


% ----------------------------------------------
function localsavehtml(mia, defaults)

if isempty(defaults.htmlfile)
  return 
end

if defaults.makedirectory
  dir_to_make = fileparts(defaults.htmlfile);
  if ~isempty(dir_to_make)
    mkdirhier(dir_to_make);
  end
end
[fid msg] = fopen(defaults.htmlfile, 'w');
if fid == -1
  error(sprintf('Cannot open %s: %s', defaults.htmlfile, msg));
end

[tmp imgfile ext] = fileparts(defaults.filename);
dsz = getdatasize(mia);
width = dsz(1) * defaults.scale(1);
height = (dsz(2) + defaults.timebargap + defaults.timebarheight) ...
	 * defaults.scale(2);

fprintf(fid, '<html>\n<head>\n<title>%s movie</title>\n</head>\n<body>\n', ...
	gettype(mia, 'c'));

% JavaScript -----------------
fprintf(fid, '<script>\n');
fprintf(fid, '<!-- comment to hide code from JavaScript impaired browsers\n');
fprintf(fid, '// t: this reference for movie scaling control\n');
fprintf(fid, 'function movieScalingChange(t, origWidth, origHeight, \n');
fprintf(fid, '            minColourBarHeight, colourBarHeightFactor) {\n');
fprintf(fid, '  var scaling = parseInt(t.value, 10);\n');
fprintf(fid, '  for(var n = 0; n < document.images.length; ++n) {\n');
fprintf(fid, '    if(document.images[n].className == "movie") {\n');
fprintf(fid, '      // do multiply, then divide in case height/width\n');
fprintf(fid, '      // are implemented as ints\n');
fprintf(fid, '      document.images[n].width = origWidth * scaling;\n');
fprintf(fid, '      document.images[n].height = origHeight * scaling;\n');
fprintf(fid, '    }\n');
fprintf(fid, '    if(document.images[n].className == "colourbar") {\n');
fprintf(fid, '      document.images[n].width = origWidth * scaling;\n');
fprintf(fid, '      var h = (colourBarHeightFactor * scaling);\n');
fprintf(fid, '      h = (h < minColourBarHeight ? minColourBarHeight : h);\n');
fprintf(fid, '      document.images[n].height = h;\n');
fprintf(fid, '    }\n');
fprintf(fid, '  } \n');
fprintf(fid, '} \n');
fprintf(fid, '  function writeForm() {\n');
fprintf(fid, '  document.writeln(''<p><form>'');\n');
fprintf(fid, '  document.writeln(''Movie scaling:&nbsp;<select name="scale"'');\n');
fprintf(fid, '  document.writeln(''onchange="movieScalingChange(this, ');
fprintf(fid, '%d, %d, 4, 2)">'');', width, height);

for n = [1:10 12:2:20] 
  fprintf(fid, ...
	  '  document.writeln(''<option value="%d">%d</option>'');\n', ...
	  n, n);
end
fprintf(fid, '  document.writeln(''</select> <br>'');\n');
fprintf(fid, '  document.writeln(''</form></p>'');\n');
fprintf(fid, '} \n');
fprintf(fid, '  writeForm();\n');
fprintf(fid, '//  done hiding code-->\n');
fprintf(fid, '</script>\n');
% JavaScript -----------------

fprintf(fid, '<p align="center">\n');
fprintf(fid, '<img alt="%s movie" src="%s" width="%d" height="%d" ', ...
	gettype(mia, 'c'), [imgfile ext], width, height);
fprintf(fid, ' class="movie"><br>\n');

[a b c d] = datalabel(mia, 'values', getlimits(mia));
fprintf(fid, '%s - %s<br>\n', d{1}, b{2});
if defaults.preliminary
  fprintf(fid, '%s<br>\n', defaults.preliminarytext);
end

[xp yp pixunits] = getpixels(mia);
xp = unique(xp([1 end]));
yp = unique(yp([1 end]));
if any(strcmp(pixunits, {'deg', 'cgm', 'aacgm'}))
  for n = 1:length(xp)
    if xp(n) >= 0
      xs{n} = sprintf('%.2f &deg;E', xp(n));
    else
      xs{n} = sprintf('%.2f &deg;W', -xp(n));
    end
  end
  for n = 1:length(yp)
    if yp(n) >= 0
      ys{n} = sprintf('%.2f &deg;N', yp(n));
    else
      ys{n} = sprintf('%.2f &deg;S', -yp(n));
    end
  end

elseif any(strcmp(pixunits, {'km', 'm'}))
  for n = 1:length(xp)
    xs{n} = sprintf('%g', xp(n));
  end
  for n = 1:length(yp)
    ys{n} = sprintf('%g', yp(n));
  end
  
else
  error(sprintf('unknown coordindate system (was ''%s'')', pixunits));
end

if length(xs) > 1
  fprintf(fid, 'X: %s to %s<br>\n', xs{1:2});
else
  fprintf(fid, 'X: %s<br>\n', xs{1});
end
if length(ys) > 1
  fprintf(fid, 'Y: %s to %s<br>\n', ys{1:2});
else
  fprintf(fid, 'Y: %s<br>\n', ys{1});
end
switch pixunits
 case 'deg'
  coord = 'geographic';
 case {'aacgm' 'cgm'}
  coord = sprintf('geomagnetic (%s)', upper(pixunits));
 otherwise
  coord = '';
end
if ~isempty(coord)
  fprintf(fid, 'Coordinate system: %s<br>\n', coord);
end

fprintf(fid, '</p>\n</body>\n</html>');
fclose(fid);
