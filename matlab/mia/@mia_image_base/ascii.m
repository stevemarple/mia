function ascii(mia, filename, varargin)
%ASCII  Save MIA_IMAGE_BASE data as ASCII data
%
%   ASCII(mia, filename, ...);
%   mia: MIA_IMAGE_BASE object
%
% If the mia object is non-scalar then multiple data sets are saved to
% the same file.
%
% The file format is:
% 1: textual header describing the data
% 2: blank line

% 3: Image data: firstly the sample time of the first image, then the
% integration time for the image, followed by the image itself. Each image
% row in the file corresponds to one row (constant Y) from the image. First
% row is the smallest Y value. First column is the smallest X
% value. Succesive images are described in the same way, with blank lines
% between the images.
%
%


[fid message] = fopen(filename, 'w');
if fid == -1
  error([message '(filename was ' filename ')']);
end

for n = 1:prod(size(mia))
  data = getdata(mia(n));

  % default values
  % it is conceivable that the data type could be different in each
  % element of mia, so have to recompute the default format on each
  % itereation of the loop
  switch class(data)
   case 'int8'
    defaults.format = '% 3d';
   case 'uint8'
    defaults.format = '% 4d';
   case 'int16'
    defaults.format = '% 6d';
   case 'uint16'
    defaults.format = '% 7d';
   case 'int32'
    defaults.format = '% 11d';
   case 'uint32'
    defaults.format = '% 12d';

   otherwise
    % assume same as double
    defaults.format = '% 15f';
  end

  defaults.timestampformat = '%Y-%m-%d %H:%M:%S.%#';
  [defaults unvi] = interceptprop(varargin, defaults);


  fprintf(fid, '%s', char(mia(n), ...
			  'timestampformat', defaults.timestampformat)); 
  dsz = getdatasize(mia(n));
  fstr = [defaults.format repmat([9 defaults.format], 1, dsz(1)-1) 10];

  for m = 1:dsz(3)
    fprintf(fid, '\n');
    fprintf(fid, '%s\n', strftime(getsampletime(mia(n), m), ...
				  defaults.timestampformat));
    fprintf(fid, '%s\n', char(getintegrationtime(mia(n), m)));
    for k = 1:dsz(2)
      fprintf(fid, fstr, data(:,k,m));
    end

  end

end

fclose(fid);
disp(sprintf('Exported as ASCII format to %s', filename));

