function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for MIA_IMAGE_BASE data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mia_base_insertcheck(a, b);

if ~strcmp(getpixelunits(a), getpixelunits(b))
  error('different coordinate schemes');
end

[apixx apixy rs.pixelunits] = getpixels(a);
[bpixx bpixy] = getpixels(b);

% X pixels
rs.xpixelpos = unique([apixx bpixx]);
[tmp aa.subs{1}] = ismember(apixx, rs.xpixelpos);
ar.subs{1} = ':'; % copy all

[tmp ba.subs{1}] = ismember(bpixx, rs.xpixelpos);
br.subs{1} = ':'; % copy all

% Y pixels
rs.ypixelpos = unique([apixy bpixy]);
[tmp aa.subs{2}] = ismember(apixy, rs.ypixelpos);
ar.subs{2} = ':'; % copy all

[tmp ba.subs{2}] = ismember(bpixy, rs.ypixelpos);
br.subs{2} = ':'; % copy all

