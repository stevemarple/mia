function r = getaspectratio(mia)
%GETASPECTRATIO  Return the aspect ratio of the image
%
%   r = GETASPECTRATIO(mia)
%   r: aspect ratio [1x2 DOUBLE]
%   mia: MIA_IMAGE_BASE object
%
%   See also MIA_IMAGE_BASE.

units = getpixelunits(mia);
[x y] = getpixels(mia);

r = miaaspectratio(x,  y, units, getmidtime(mia));
