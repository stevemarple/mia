function [r, idx2] = build(mia, a, idx)

if ~strcmp(class(mia), 'mia_image_base')

  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a BUILD command to use
  % CONSTRUCTFROMFILES. Generate a runtime error with appropriate error
  % message to indicate what needs to be done. Derived classes MUST call
  % this function on their parent field.
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia;
[r.mia_base idx2] = build(mia.mia_base, a.mia_base, idx);

ind = idx:(idx2-1);

% r.imagedata(:,:,ind) = a.imagedata;
s.type = '()';
s.subs = {':', ':', ind};
r = setdata(r, getdata(a), s);

r.imagetime(ind) = a.imagetime;
r.imagemidtime(ind) = a.imagemidtime;


