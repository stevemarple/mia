function r = setxpixelpos(mia, xpix)
%SETXPIXELPOS  Set x pixel positions.
%
%   r = SETXPIXELPOS(mia, xpix)
%   r: modified MIA_IMAGE_BASE object
%   xpix: new X pixel positions
%
%   See also SETYPIXELPOS, GETPIXELS.

r = mia;
r.xpixelpos = xpix;

