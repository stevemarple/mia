function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for MIA_IMAGE_BASE data.
%
% See mia_base/INSERT.


[r aa ar ba br rs] = mia_base_insert(a, b);

r.xpixelpos = rs.xpixelpos;
r.ypixelpos = rs.ypixelpos;
r.pixelunits = rs.pixelunits;

