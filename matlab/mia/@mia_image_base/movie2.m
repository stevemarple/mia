function movie2(mia, varargin)
%MOVIE  Make an animated GIF movie.
%
% MOVIE2: an improved version of MOVIE which allows multiple datasets (of
% different image types) to be animated.


[Path filename ext] = fileparts(getfilename(mia(1)));
if ~isempty(filename)
  % replace any extension with '.gif'
  defaults.filename = fullfile(Path, [filename '.gif']);
else
  defaults.filename = 'untitled.gif';
end
defaults.makedirectory = 0;
% defaults.numberwidth = fix(log10(length(mia.imagetime))) + 1;
defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];
defaults.delay = timespan(100,'ms');
defaults.scale = [1 1];
defaults.loopcount = 0;
defaults.colormap = jet(256);
defaults.missingdatacolor = [0.6 0.6 0.6];
defaults.clim = [];
defaults.optimize = 0;
defaults.heaterevent = [];

defaults.timebar = 1;
defaults.timebarcolor = [1 0 0];  % R G B
defaults.timebargap = 2;
defaults.timebarheight = 3;

defaults.xpixels = [];
defaults.ypixels = [];

% allow multiple datasets to be combined. Cannot always use an array for
% mia since it is desirable to make movies of combined riometer/all-sky
% camera data, which are different types and cannot be put into the same
% array
defaults.otherdata = {};
defaults.rows = []; 
defaults.columns = [];
defaults.separatorpixels = 1;
defaults.separatorcolor = 'none';

[defaults unvi] = interceptprop(varargin, defaults);

% accumulate all data
% [data{1:prod(size(mia))}] = deal(mia);
data = cell(1, prod(size(mia)));
for n = 1:length(data)
  data{n} = mia(n);
end

if iscell(defaults.otherdata)
  for n = 1:prod(size(defaults.otherdata))
    data{end+1} = defaults.otherdata{n};
  end
else
  data{end+1} = defaults.otherdata;
end

if isempty(data)
  error('no data present');
end

% calculate sensible defaults
STmin = getstarttime(data{1});
ETmax = getendtime(data{1});
clim = cell(size(data));
clim{1} = getclim(data{1});
resolution = min(getresolution(data{1},1))
[xpixels ypixels pixelunits] = getpixels(data{1}(1));

for n = 2:length(data)
  STmin = min(STmin, getstarttime(data{n}));
  ETmax = max(ETmax, getendtime(data{n}));
  clim{n} = getclim(data{n});
  resolution = min(resolution, min(getresolution(data{n})));
  [xpixtmp ypixtmp pixunitstmp] = getpixels(data{n});
  if ~strcmp(pixunitstmp, pixelunits)
    error(sprintf('pixel units differ (%s and %s)', pixunitstmp, pixelunits));
  end
  % have a problem with double values not being entirely equal, even
  % though they supposedly are the same values, so convert to single
  % precision and back to double as a quick fix to working within a given
  % tolerance
  xpixels = unique(double([single(xpixels) single(xpixtmp)]));
  ypixels = unique(double([single(ypixels) single(ypixtmp)]));
end


% set defaults to sensible values unless already set
if isempty(defaults.starttime)
  defaults.starttime = STmin;
end
if isempty(defaults.endtime)
  defaults.endtime = ETmax;
end
if isempty(defaults.resolution)
  defaults.resolution = resolution;
end
if isempty(defaults.clim)
  for n = 1:length(data)
    defaults.clim = clim;
  end
elseif iscell(defaults.clim)
  ;
elseif length(data) == 1
  defaults.clim = {defaults.clim};
else
  error('For multiple datasets clim must be a cell matrix');
end

if isempty(defaults.columns)
  defaults.columns = ceil(sqrt(length(data)));
end
if isempty(defaults.rows)
  defaults.rows = ceil(length(data) / defaults.columns);
end

if defaults.rows .* defaults.columns < length(data)
  error(sprintf(['Insufficient number of rows (%d) and columns (%d) ' ...
		 'for the number of data sets (%d)'], defaults.rows, ...
		defaults.columns, length(data)));
end

if isempty(defaults.xpixels)
  defaults.xpixels = xpixels;
end
if isempty(defaults.ypixels)
  defaults.ypixels = ypixels;
end

cmapSz = size(defaults.colormap, 1);

% convert colors to RGB values
defaults.timebarcolor = text2rgb(defaults.timebarcolor);
defaults.missingdatacolor = text2rgb(defaults.missingdatacolor);
if strcmpi(defaults.separatorcolor, 'none') 
  defaults.separatorcolor = [nan nan nan];
elseif isequal(isnan(defaults.separatorcolor), [1 1 1])
  ;
else
  % use 'none' or nan for no color (transparent)
  defaults.separatorcolor = text2rgb(defaults.separatorcolor);
end

if defaults.loopcount < 1
  defaults.loopcount = 'forever';
else
  defaults.loopcount = int2str(defaults.loopcount);
end


if defaults.optimize
  optimizeStr = sprintf(' -O%d ', fix(max(min(defaults.optimize, 2), 0)));
else 
  optimizeStr = '';
end

defaults.endtime
defaults.starttime
defaults.resolution
giffile = cell((defaults.endtime - defaults.starttime)/...
	       defaults.resolution, 1);
imgnum = 0;

datainfo = [];
blank = repmat(nan, [length(defaults.ypixels)*defaults.rows + ...
		    defaults.separatorpixels*(defaults.rows-1), ...
		    length(defaults.xpixels)*defaults.columns + ...
		    defaults.separatorpixels*(defaults.columns-1), ...
		    3]);
% blank(:,:,[1 3]) = 1; % magenta

% set the color of the separator pixels
% TO DO
for n = 1:3
  blank(:,:,n) = defaults.separatorcolor(n);
end

% need to make two passes. On the first pass calculate the CLim values,
% based on the actual data to be used. On the second pass output the
% values


for pass = 1:2
  t = defaults.starttime
  while t <= defaults.endtime
    if pass == 2
      IMC = blank;
    end
    for d = 1:length(data)
      if imgnum == 0 & pass == 1
	[row col] = ind2sub([defaults.rows defaults.columns], d);
	
	% find out indices for pixels
	[xp yp] = getpixels(data{d});
	% pixelidx is the index into the data, to decide which pixels to
	% use, imgidx is the index into the image, for where to put them
	[tmp datainfo(d).xpixelidx datainfo(d).ximgidx] = ...
	    intersect(double(single(xp)), defaults.xpixels);
	[tmp datainfo(d).ypixelidx datainfo(d).yimgidx] = ...
	    intersect(double(single(yp)), defaults.ypixels);
	% adjust where images should go to allow for tiling the image
	datainfo(d).ximgidx = datainfo(d).ximgidx + ...
	    (col-1) * (length(defaults.xpixels) + defaults.separatorpixels);
	% datainfo(d).yimgidx = datainfo(d).yimgidx + ...
	%   (col-1) * (length(defaults.ypixels) + defaults.separatorpixels);
	datainfo(d).yimgidx = (length(defaults.ypixels) * defaults.rows) ...
	    + 1 - (datainfo(d).yimgidx + ...
	     (row-1) * (length(defaults.ypixels) + defaults.separatorpixels));

	
	% remember the original clim values, to detect if CLim should be
        % computed based on actual data (or take given values)
	datainfo(d).originalclim = defaults.clim{d};
      end
      
      
      % find image(s) corresponding to current time, and insert into
      % correct place
      ti = getimagetime(data{d});
      n = find(t >= ti & t < ti + getresolution(data{d}));
      im = getdata(data{d}, datainfo(d).xpixelidx , ...
			    datainfo(d).ypixelidx, n);
      switch length(n)
       case 0
	disp('NO DATA')
	im = repmat(nan, length(datainfo(d).xpixelidx), ...
		    length(datainfo(d).ypixelidx));
       case 1
	% disp('ONE IMAGE')
	;
       otherwise
	disp('MULTIPLE IMAGES')
	im = mean(getdata(data{d}, datainfo(d).xpixelidx , ...
				   datainfo(d).ypixelidx, n), 3);
      end
      
      if pass == 1
	% recompute CLim values (if necessary (nan))
	if isnan(datainfo(d).originalclim(1))
	  defaults.clim{d}(1) = min(defaults.clim{d}(1), min(im(:)));
	end
	if isnan(datainfo(d).originalclim(2))
	  defaults.clim{d}(2) = max(defaults.clim{d}(2), max(im(:)));
	end
      else
	% image as integer
	im = 1 + round((cmapSz - 1) * ...
		       (im - defaults.clim{d}(1)) ...
		       ./  (defaults.clim{d}(2) - defaults.clim{d}(1)));
	
	% convert anything outside of limits to the limiting values
	im(im > cmapSz) = cmapSz;
	im(im < 1) = 1;

	im = im';
	
	% convert image to true colour
	if 0 
	  [nansr nansc] = find(isnan(im));  % remember what was nan
	  if ~isempty(nansr) % needed for matlab 5.1 compatibility
	    im(nansr, nansc) = 1;
	  end
	  imc = reshape(defaults.colormap(im,:), size(im,1), size(im,2), 3);
	  imc(nansr, nansc, :) = nan; % convert back
	else
	  nans = find(isnan(im));  % remember what was nan
	  if ~isempty(nans) % needed for matlab 5.1 compatibility
	    im(nans) = 1;
	  end
	  imc = reshape(defaults.colormap(im,:), size(im,1), size(im,2), 3);
	  imsz = size(im);
	  for m = 1:length(nans)
	    [nansr nansc] = ind2sub(imsz, nans(m));
	    % imc(nansr, nansc, :) = nan; % convert back
	    imc(nansr, nansc, :) = defaults.missingdatacolor; % convert back
	  end
	end
	
	% IMC(datainfo(d).ximgidx, datainfo(d).yimgidx, :) = imc;
	% datainfo(d).ximgidx
	IMC(datainfo(d).yimgidx, datainfo(d).ximgidx, :) = imc;
      end
    end

    if pass > 1
      
      if defaults.timebar
	IMCsz = size(IMC);
	% add blank pixels
	IMC(IMCsz(1) + (1:(defaults.timebargap+defaults.timebarheight)), ...
	    :, :) = nan;
	% fill timebar
	timebarX = round(IMCsz(2) * ...
			 ((t - defaults.starttime) / ...
			  (defaults.endtime - defaults.starttime)));
	for c = 1:3
	  IMC(IMCsz(1)+defaults.timebargap+(1:defaults.timebarheight), ...
	      1:timebarX, c) = defaults.timebarcolor(c);
	end
	
      end
      
      imgnum = imgnum + 1;
      giffile{imgnum} = tempname;
      pngfile = tempname;

      imwrite(IMC, pngfile, 'png', 'Alpha', ~isnan(IMC(:,:,1)));
      
      png2gif(pngfile, giffile{imgnum});
      delete(pngfile);
      
      % figure
      % image(IMC, 'CDataMapping','scaled')

    end
    t = t + defaults.resolution;
  end
end

% animate the gifs
switch computer 
 case {'LNX86' 'GLNX86'}
  
  commandStr = sprintf(['gifsicle --delay %d --scale %dx%d ' ...
		    '--loopcount=%s %s %s > %s'], ... 
		       round(defaults.delay/timespan(100,'ms')), ...
		       defaults.scale([1 2]), ...
		       defaults.loopcount, ...
		       optimizeStr, ...
		       sprintf(' %s', giffile{:}), defaults.filename);

  % disp(commandStr);
  if logical(defaults.makedirectory)
    mkdirhier(fileparts(defaults.filename));
  end
  unix(commandStr);
  
 otherwise
  error(['Do not know how to animate GIFs. Please add for ' ...
	 computer ' to ' mfilename]);
end

delete(giffile{:});
return



m = 1;
% index to valid images
idx = find(mia.imagetime >= defaults.starttime & ...
	   mia.imagetime < defaults.endtime);

% imdata = mia.imagedata(:,:,idx);
imdata = getdata(mia,':',':',idx);
if isnan(defaults.clim(2))
  defaults.clim(1) = min(imdata(:));
end
if isnan(defaults.clim(2))
  defaults.clim(2) = max(imdata(:));
end

clear imdata; % reclaim memory

for n = idx
  t = mia.imagetime(n);
  
  if t >= defaults.starttime & t < defaults.endtime
    % disp(char(t));
    giffile{m} = tempname;
    pngfile = tempname;
    
    cmapSz = size(defaults.colormap, 1);
    % image as integer
    im = flipud(1 + round((cmapSz - 1) * ...
			  (getdata(mia,':',':',n)' - defaults.clim(1)) ...
			  ./  (defaults.clim(2) - defaults.clim(1))));
    % convert anything outside of limits to the limiting values
    im(find(im > cmapSz)) = cmapSz;
    im(find(im < 1)) = 1;
    
    % convert image to true colour
    [nansr nansc] = find(isnan(im));  % remember what was nan
    if ~isempty(nansr) % needed for matlab 5.1 compatibility
      im(nansr, nansc) = 1;
    end
    imc = reshape(defaults.colormap(im,:), size(im,1), size(im,2), 3);
    
    imc(nansr, nansc, :) = nan; % convert back
    
    if ~isempty(defaults.heaterevent)
      imc = onofftimes(defaults.heaterevent, [], ...
		       varargin{unvi}, ...
		       'style', 'imagepatch', ...
		       'image', imc, ...
		       'plotstarttime', mia.imagetime(n), ...
		       'plotendtime', mia.imagetime(n) + defaults.resolution);
    end
    
    if defaults.timebar
      imcsz = size(imc);
      % add blank pixels
      imc(imcsz(1) + (1:(defaults.timebargap+defaults.timebarheight)), ...
	  :, :) = nan;
      % % size(imc)
      % % imcsz(1) + 1:(defaults.timebargap+defaults.timebarheight)
      % fill timebar
      timebarX = round(imcsz(2) * ...
		       ((mia.imagetime(n) - defaults.starttime) / ...
			(defaults.endtime - defaults.starttime)));
      for c = 1:3
	imc(imcsz(1)+defaults.timebargap+(1:defaults.timebarheight), ...
	    1:timebarX, c) = defaults.timebarcolor(c);
      end
      
    end
    % imwrite(imc, pngfile, 'png');
    imwrite(imc, pngfile, 'png', 'Alpha', ~isnan(imc(:,:,1)));
    
    png2gif(pngfile, giffile{m});
    delete(pngfile);
    m = m + 1;	
  end
  
end

% animate the gifs
switch computer 
 case {'LNX86' 'GLNX86'}
  
  commandStr = sprintf(['gifsicle --delay %d --scale %dx%d ' ...
		    '--loopcount=%s %s %s > %s'], ... 
		       round(defaults.delay/timespan(100,'ms')), ...
		       defaults.scale([1 2]), ...
		       defaults.loopcount, ...
		       optimizeStr, ...
		       sprintf(' %s', giffile{:}), defaults.filename);

  % disp(commandStr);
  if logical(defaults.makedirectory)
    mkdirhier(fileparts(defaults.filename));
  end
  unix(commandStr);
  
 otherwise
  error(['Do not know how to animate GIFs. Please add for ' ...
	 computer ' to ' mfilename]);
end



% remove gif files
% for n = 1:length(giffile)
%   delete(giffile{n});
% end
delete(giffile{:});

