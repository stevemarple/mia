function varargout = guiextractdata(mia)
%GUIEXTRACT  Return a list of methods for EXTRACT using a gui.
%

guiFunc = 'mia_image_base_guiextract';
% options
varargout{1} = {{guiFunc 'timeinit' 'data'}, ...
		{guiFunc 'pixelsinit' 'data'}}; 

% second return value is a cell array of names to be used on menus
if nargout >= 2
  varargout{2} = {'Start/end time' 'X/Y pixel location'};
end
