function ph = patch2(mia, varargin)
%PATCH  Add patch representing MIA_IMAGE_BASE object onto axes.
%
%   h = PATCH(mia, ...)
%   h: handle of UI PATCH object
%   mia: MIA_IMAGE_BASE object
%
%   The following parameter name/value pairs are recognised.
%
% 

ph = [];

height = getheight(mia);
in = getinstrument(mia);
loc = getlocation(in);
if length(loc) ~= 1 & ~strcmp(mia.pixelunits, 'deg')
  error(sprintf(['Cannot convert from %s grid to lat/lon grid for' ...
		 ' %d locations'], mia.pixelunits, length(loc)));
end

defaults.axes = [];
defaults.colormap = [];
defaults.edgecolor = 'none';
defaults.time = getimagetime(mia, 1);

defaults.extentx = [nan nan];
defaults.extenty = [nan nan];
defaults.shading = 'flat'; 
defaults.clim = getclim(mia);
defaults.patchfunction = ''; % use 'm_patch' for mapping with m_map package
[defaults unvi] = interceptprop(varargin, defaults);

% gca can create axes so only call if needed
if isempty(defaults.axes)
  defaults.axes = gca;
end

if isempty(defaults.colormap)
  defaults.colormap = get(get(defaults.axes,'Parent'),'Colormap');
end

if isempty(defaults.patchfunction)
  if ismmap(defaults.axes)
    defaults.patchfunction = 'm_patch';
  else
    defaults.patchfunction = 'patch';
  end
end

xn = length(mia.xpixelpos);
yn = length(mia.ypixelpos);



% left
if isnan(defaults.extentx(1))
  if xn == 1
    error('X extent must be set if xpixelpos is a scalar');
  end
  defaults.extentx(1) = mia.xpixelpos(1) - ...
      0.5 * (mia.xpixelpos(2)-mia.xpixelpos(1));
end
% right
if isnan(defaults.extentx(2))
  if xn == 1
    error('X extent must be set if xpixelpos is a scalar');
  end
  defaults.extentx(2) = mia.xpixelpos(xn) + ...
      0.5 * (mia.xpixelpos(xn)-mia.xpixelpos(xn-1));
end
% bottom
if isnan(defaults.extenty(1))
  if yn == 1
    error('Y extent must be set if ypixelpos is a scalar');
  end
  defaults.extenty(1) = mia.ypixelpos(1) - ...
      0.5 * (mia.ypixelpos(2)-mia.ypixelpos(1));
end
% top
if isnan(defaults.extenty(2))
  if yn == 1
    error('Y extent must be set if ypixelpos is a scalar');
  end
  defaults.extenty(2) = mia.ypixelpos(yn) + ...
      0.5 * (mia.ypixelpos(yn)-mia.ypixelpos(yn-1));
end


im = [];

if isa(defaults.time, 'timestamp')
  % convert to sample number
  n = find(defaults.time == mia.imagetime);
  if isempty(n)
    error(sprintf('Cannot find image corresponding to %s', ...
		  char(defaults.time)));
  end
  defaults.time = n;
end

% im = mia.imagedata(:,:,defaults.time);
im = getdata(mia, ':', ':', defaults.time);

% set color limits, unless overriden in parameter list
if isnan(defaults.clim(1))
  defaults.clim(1) = min(im(:));
end
if isnan(defaults.clim(2))
  defaults.clim(2) = max(im(:));
end

% keep it simple for m_patch. Use only one face per patch
switch defaults.shading
 case 'flat'
  % one colour per face, one face per value in image
  % centre of face on a grid intersection
  xVertices = [defaults.extentx(1) ...
	       (mia.xpixelpos(1:(xn-1)) + ...
		(mia.xpixelpos(2:xn) - mia.xpixelpos(1:(xn-1))) ./ 2) ...
	       defaults.extentx(2)];
  yVertices = [defaults.extenty(1) ...
	       (mia.ypixelpos(1:(yn-1)) + ...
		(mia.ypixelpos(2:yn) - mia.ypixelpos(1:(yn-1))) ./ 2) ...
	       defaults.extenty(2)];
  
  % have to enlarge image. Only using bottom-left values for shading
  % anyhow
  im2 = repmat(nan, size(im,1)+1, size(im,2)+1);
  im2(1:xn,1:yn) = im;
  % im2(1:xn,1:yn) = im';
  % im = im2;

  
 case 'interp'
  % one colour per vertex, one face between four vertices
  % vertex on a grid intersection, centre of face between grid
  % intersections 
  xVertices = mia.xpixelpos;
  yVertices = mia.ypixelpos;
  error('not implemented');
  
 otherwise
  error('unknown shading');
  
end

xvlen = length(xVertices);
yvlen = length(yVertices);
% xx = zeros(xvlen*yvlen, 4);
% yy = zeros(xvlen*yvlen, 4);
% zz = zeros(xvlen*yvlen, 4);
ph = zeros((xvlen-1)*(yvlen-1),1);
% for x = 1:(xvlen-1)
for x = 1:xn
  % for y = 1:(yvlen-1)
  for y = 1:yn
    xx = [xVertices(x) xVertices(x+1)  xVertices(x+1) xVertices(x)];
    yy = [yVertices(y) yVertices(y) yVertices(y+1) yVertices(y+1)];
    zz = im2(x,y);

    cdatatmp = round((im(x,y) - defaults.clim(1)) ./ ...
		     (defaults.clim(2)-defaults.clim(1)) .* ...
		     size(defaults.colormap,1));
    cdatatmp = min(size(defaults.colormap,1), max(cdatatmp, 1));
    cdata = defaults.colormap(cdatatmp, :);

    
    %     % if necessary map to longitude/latitude
    %     switch mia.pixelunits
    %      case 'deg'
    %       % no need to map
    
    %      case 'km'
    %       [xx yy] = xy2longlat(xx*1e3, yy*1e3, height, loc);
    
    %      case 'm'
    %       [xx yy] = xy2longlat(xx, yy, height, loc);
    
    %      case 'aacgm'
    %       tmp = geocgm('direction', -1, ...
    % 		   'latitude', yy, ...
    % 		   'longitude', xx, ...
    % 		   'height', height, ...
    % 		   'year', getyear(getmidtime(mia)));
    %       xx = tmp.geocentric_longitude;
    %       yy = tmp.geocentric_latitude;
    
    %      case {'km_antenna' 'm_antenna'}
    %       % antenna direction is compass angle, different sense to trig angle
    %       theta = -info(in, 'antennaazimuth') * pi/180;
    %       tmpxx = xx*cos(theta) - yy*sin(theta);
    %       tmpyy = yy*cos(theta) + xx*sin(theta);
    %       if strcmp(mia.pixelunits, 'km_antenna')
    % 	tmpxx = tmpxx * 1e3;
    % 	tmpyy = tmpyy * 1e3;
    %       end
    %       [xx yy] = xy2longlat(tmpxx, tmpyy, height, loc);
    
    %      otherwise
    %       error('unknown units - cannot map to longitude/latitude');
    %     end
    
    % convert any coordinate system we know about into degrees
    % coord2degArgs = {xx, yy, mia.pixelunits, ...
    coord2degArgs = {loc, xx, yy, mia.pixelunits, ...
		     'height', height};
    if isa(in, 'riometer')
      % riometer have extra coordinates schemes
      coord2degArgs{end+1} = 'antennaazimuth';
      coord2degArgs{end+1} = info(in, 'antennaazimuth');
    end
    
    [xx yy] = coord2deg(coord2degArgs{:});
        
    % need a color, even if we override it later!
    ph(localXy2n(x, y, xvlen-1)) = ...
	feval(defaults.patchfunction, ...
	      xx, yy, 'r', ...
	      'FaceColor', cdata, ...
	      'EdgeColor', defaults.edgecolor, ...
	      'Parent', defaults.axes, ...
	      'CDataMapping', 'direct');
  end
end



function r = localXy2n(x, y, maxX)
% map (x,y) to a vertex num
r = y + (x-1)*maxX;

