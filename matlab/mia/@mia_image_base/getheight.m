function r = getheight(mia)
%GETHEIGHT  Return the height of a MIA_IMAGE_BASE object.
%
%   r = GETHEIGHT(mia)
%
%   r: height (in metres)
%   mia: MIA_IMAGE_BASE object   
% 
%   The exact meaning of height is dependent upon the instrument. For
%   most ground-based instruments it is the projection height.
%
%   See also MIA_IMAGE_BASE.

r = mia.height;
