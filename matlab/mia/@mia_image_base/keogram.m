function [fh, gh, cbh] = keogram(mia, varargin)
%KEOGRAM  Display a MIA_IMAGE_BASE object as a keogram.
%
%   [fh, gh, cbh] = KEOGRAM(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle

fh = [];
gh = [];
cbh = [];

instrument = getinstrument(mia);

defaults.xpixelpos = [];
defaults.ypixelpos = [];
defaults.miniplots = [1 1];
defaults.starttime = getstarttime(mia);
defaults.endtime = []; % defer (miniPlots might be changed)
defaults.title = ''; % defer
defaults.windowtitle = ''; % defer
defaults.logo = info(instrument, 'logo');
defaults.plotaxes = [];
defaults.seriesstyle = 'printseries';
defaults.shading = 'flat';
defaults.visible = 'on'; % final state for figure

defaults.clim = [nan nan]; % autoscale

defaults.method = 'surface';

[defaults unvi] = interceptprop(varargin, defaults);

% some local copies
st = getstarttime(mia);
et = getendtime(mia);
[res resmesg] = getresolution(mia);

integ = getintegrationtime(mia);

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  fh = [];
else
  gh = [];
end

% check and/or calculate starttime
if (defaults.starttime >= et | ...
    defaults.starttime < st) & 0
  warning('Incorrect start time');
  defaults.starttime = st;
end

% check and/or calculate endtime
if isempty(defaults.endtime)
  defaults.endtime = et;
elseif defaults.endtime <= defaults.starttime | ...
      defaults.endtime > et
  warning('Incorrect end time');
  defaults.endtime = et;
end

if isempty(defaults.xpixelpos) & isempty(defaults.ypixelpos)
  % assume N-S keogram across image centre
  defaults.xpixelpos = mia.xpixelpos(ceil(length(mia.xpixelpos)/2));
  defaults.ypixelpos = mia.ypixelpos;
end


xPixLen = length(defaults.xpixelpos);
yPixLen = length(defaults.ypixelpos);


% [plottitle windowtitle] = maketitle(mia, defaults.starttime, ...
%    defaults.endtime, defaults.step);
windowtitle = 'Keogram';
[pixelStr ylab invertSign] = localIdLoc2str(mia, defaults);


[plottitle windowtitle] = maketitle(mia, ...
				    'style', 'keogram', ...
				    'starttime', defaults.starttime, ...
 				    'endtime', defaults.endtime, ...
				    'xpixelpos', defaults.xpixelpos, ...
				    'ypixelpos', defaults.ypixelpos);

if isempty(defaults.title)
  defaults.title = plottitle;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end


if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', varargin{unvi}, ...
			'logo', defaults.logo, ...
			'miniplots', defaults.miniplots, ...
			'title', defaults.title, ...
			'visible', 'off', ...
			'footer', 'on');
  set(fh, 'Name', defaults.windowtitle);   % Write name into figures title bar 
  makeplotfig('addshading', fh);  % Add pixel shading options to menus
end

[data xidx yidx tidx] = keodata(mia, varargin{unvi}, ...
				'xpixelpos', defaults.xpixelpos, ...
				'ypixelpos', defaults.ypixelpos, ...
				'starttime', defaults.starttime, ...
				'endtime', defaults.endtime);
[xpix ypix] = getpixels(mia);
defaults.xpixelpos = xpix(xidx);
defaults.ypixelpos = ypix(yidx);

surfXData = getcdfepochvalue(getsampletime(mia, tidx));
xlim = getcdfepochvalue([defaults.starttime defaults.endtime]);

% Bugs in surface and patch cause problems with large X values. Subtract
% start time from all values.
surfXData = surfXData - xlim(1);
xlim = xlim - xlim(1);

if xPixLen == 1
  % N-S keogram
  ylim = [min(defaults.ypixelpos) max(defaults.ypixelpos)];
  surfYData = defaults.ypixelpos;
elseif yPixLen == 1
  % E-W keogram
  ylim = [min(defaults.xpixelpos) max(defaults.xpixelpos)];
  surfYData = defaults.xpixelpos
else
  % may NOT have to be
  error('Keogram must be aligned with grid');
end
% images may not have been taken at constant intervals,  ensure XData
% reflects this fact

switch defaults.method
  case 'surface'
   sh = surface('Parent', gh(1), ... 
                'XData', surfXData, ...
                'YData', surfYData, ...
                'ZData', data, ...
                'CData', data, ...
                'EdgeColor', 'none');
   view(gh(1), 2);
   
 case 'surfaceimage'
  [surfXData2  surfYData2] = meshgrid(surfXData, surfYData);
  sh = surfaceimage(surfXData2, ...
                    surfYData2, ...
                    data, ...
                    data, ...
                    'Parent', gh(1), ... 
                    'EdgeColor', 'none');
  
 otherwise
  error(sprintf('unknown method (was ''%s'')', defaults.method));
end

set(gh(1), 'XLim', xlim, ...
	   'YLim', ylim, ...
	   'TickDirMode', 'manual', ...
	   'TickDir', 'out', ...
	   'Layer', 'top', ...
	   'Box', 'on');

timetick(gh(1), 'X', defaults.starttime, defaults.endtime, ...
 	 timespan(1, 'ms'), 1);
% timetick2(gh(1), 'class', class(getsampletime(mia)));

if length(gh) > 1
  % later when plotting into multiple axes this will break
  error(['Please fix ' mfilename ' so that all axes have same colour' ...
	 ' limits']);
end


clim = get(gh(1), 'CLim');
defaults.clim(isnan(defaults.clim)) = clim(isnan(defaults.clim));

if ~isempty(ylab)
  axes(gh(1));
  ylabel(sprintf('\\bf %s', char(ylab)), 'FontSize', ...
	 get(get(gh(1), 'XLabel'), 'FontSize'));
  if invertSign
    % all negative, so make + and label as deg south
    ytick = get(gh(1), 'YTick');
    yticklabel = cell(length(ytick), 1);
    for n = 1:length(ytick)
      yticklabel{n} = sprintf('%g', -ytick(n));
    end
  end
end

set(sh, 'FaceColor', defaults.shading');

if ~isempty(fh)
  makeplotfig('pixelshading',defaults.shading,fh);
  cbh = makeplotfig('addcolorbar', fh, ...
		    'shading', defaults.shading, ...
		    'clim', defaults.clim, ...
		    'xlim', defaults.clim, ...
		    'title', datalabel(mia));
  set(fh, ...
      'Pointer', 'arrow', ...
      'Visible', defaults.visible);

end

set(gh, 'CLim', defaults.clim);

dataquality = getdataquality(mia);
if ~isempty(dataquality)
  wmh = makeplotfig('addwatermark', gh, {dataquality});
  pos = get(wmh, 'Position');
  set(wmh, 'Position', [pos(1:2) 1]);
end
      
  

return;


% ========================
function [s, ylab, invertSign] = localIdLoc2str(id, defaults)
s = '';
ylab = '';
invertSign = 0;

pixunits = getpixelunits(id);
xpixelpos = defaults.xpixelpos;
ypixelpos = defaults.ypixelpos;


if all(xpixelpos > 0)
  xpixunits = 'E';
elseif all(xpixelpos < 0)
  xpixunits = 'W';
  xpixelpos = abs(xpixelpos);
  invertSign = 1;
else
  xpixunits = 'E/W';
end
if all(xpixelpos > 0)
  ypixunits = 'N';
elseif all(xpixelpos < 0)
  ypixunits = 'S';
  ypixelpos = abs(ypixelpos);
  invertSign = 1;
else
  ypixunits = 'N/S';
end

if length(xpixelpos) == 1 & length(ypixelpos) > 1
  ylab = ['Latitude (%s' ypixunits ')'];
elseif length(xpixelpos) > 1 & length(xpixelpos) == 1
  ylab = ['Longitude (%s' xpixunits ')'];
end

if strcmp(pixunits,'deg')
  % pixunits = i18n_entity('deg');
  pixunits = ' deg';
elseif strcmp(pixunits, 'aacgm')
  % pixunits = [i18n_entity('deg') ' (AACGM)'];
  pixunits = ' deg (AACGM)';
else
  % pixunits = [' ' pixunits];
  pixunits = [pixunits ' '];
end

ylab = sprintf(ylab, pixunits);

switch char(defaults.seriesstyle)
  case {'' 'printseries'}
   xpix = printseries(xpixelpos);
   ypix = printseries(ypixelpos);
 
 case 'keogram'
  xpix = localKeogramSeries(xpixelpos);
  ypix = localKeogramSeries(ypixelpos);
  
 otherwise
  % user-supplied method
  if exists(defaults.seriesstyle)
    xpix = feval(defaults.seriesstyle, xpixelpos);
    ypix = feval(defaults.seriesstyle, ypixelpos);    
  else
    error(sprintf(['Unknown series style and no method ' ...
		   'of the same name (was %s)'], defaults.seriesstyle));
  end
end

[n c] = getname(getlocation(getinstrument(id)));
s = sprintf('%s, %s, %s%s %s %s%s %s', n, c, ...
    xpix, pixunits, xpixunits, ...
    ypix, pixunits, ypixunits);
return

% ========================
function r = localKeogramSeries(a)
asz = prod(size(a));
switch asz
 case 0
  r = '';
 
 case 1
  r = sprintf('%g', a);
  
 otherwise
  r = sprintf('%g - %g (in %g)', a(1), a(asz), (a(2)-a(1)));
end
return

