function r = setpixels(mia, xpix, ypix, varargin)
%SETPIXELS  Set pixel information.
%
%   r = SETPIXELS(mia, x, y)
%   r = SETPIXELS(mia, x, y, units)
%   r: modified MIA_IMAGE_BASE object
%   x: new X pixel positions
%   y: new Y pixel positions
%   units: new units
%
%   See also SETXPIXELS, SETYPIXELPOS, GETPIXELS.

r = mia;
r.xpixelpos = xpix;
r.ypixelpos = ypix;

if length(varargin)
  r.pixelunits = varargin{1};
end

