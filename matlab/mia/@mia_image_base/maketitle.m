function [plottitle, windowtitle] = maketitle(mia, varargin)
%MAKETITLE  Make plot and figure titles.
%
%   [plottitle, windowtitle] = maketitle(mia)
%   [plottitle, windowtitle] = maketitle(mia, ...)
%
%   Create the titles based on current time/date, data type etc. Vamiaus
%   values may be overriden by specifying a parameter name and value (see
%   INTERCEPTPROP). The defaults values are created from the IRISFILEBASE
%   object (or an object derived from IRISFILEBASE). The valid parameters
%   which may be overriden are described below:
%
%     'style', style
%        For IMAGEDATA objects the default title style is 'image'. This adds
%        the word 'image' or 'images' to the result of
%        GETNAME(mia). Otherwise the default title style is 'line', which
%        instead prints the beams. 
%
%     'customstring', string
%        Observe chosen style but override the string printed in
%        bold. Useful for Keogram plots and similar.
%
%     'starttime', TIMESTAMP
%
%     'endtime', TIMESTAMP
%
%     'resolution', TIMESPAN
%     'resolution', CHAR
%        If the resolution is given as a TIMESPAN then it is converted to
%        a suitable string. If it is a CHAR array then it is printed
%        without change.   
%
%     'location', IRISLOCATION
%
%     'beams', [beams]
%        The default beams are obtain from the IRISFILEBASE object. The
%        beams are not printed for the 'image' style, otherwise the beams
%        are printed in compressed format by PRINTSERIES.
%
%     'step', step
%        For image plots using only 1 out of n images, step should be set to
%        n.
%
%     'flipaxes', str
%        The orientation of the images. The string should be one of those
%        returned by MAKEPLOTFIG('flipaxesnames');
%
%     'comment', comment
%        An optional comment, to be printed at the bottom of the title
%
%   See also IRISFILEBASE, IMAGEDATA, MAKEPLOTFIG, GETNAME, PRINTSERIES.

plottitle = '';
windowtitle = '';

% general format is
% Power/time (beam 1)
% 00:00:00 - 12:00:00 UT 1/1/1999 @ 1 m res.
% Kilpisjarvi, Finland (69.05N, 20.79E)
% [comment]
%
% If step ~= 1 then print that and resolution on separate line to time
% formatStr = '{\\bf %s} %s\n%s%s\n%s%s';
formatStr = { ...
      '{\\bf %s} %s' ...
      '%s%s' ...
      '%s%s'
  };

% entries are:
styleStr = ''; % 'Power/time images', 'Absorption/time', etc
beamStr = '';
locStr = '';
dateStr = '';
resStr = '' ; % including step size if appropriate

instrument = getinstrument(mia);
loc = getlocation(instrument);
% [imagingBeams wideBeams] = info(instrument, 'beams');

% default values
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
[defaults.resolution resmesg] = getresolution(mia);

defaults.location = loc;
defaults.step = 1;
defaults.style = 'image';
defaults.customstring = '';
defaults.comment = '';
defaults.flipaxes = '';

defaults.xpixelpos = [];
defaults.ypixelpos = [];

if nargin > 1
  [defaults unvi] = interceptprop(varargin, defaults);
end

if strcmp(defaults.style, 'keogram')
  if isempty(defaults.xpixelpos)
    error('style=keogram requires that xpixelpos be set');
  elseif isempty(defaults.ypixelpos)
    error('style=keogram requires that ypixelpos be set');
  end
end

beamStr = '';

locStr = char(defaults.location);
dateStr = dateprintf(defaults.starttime, defaults.endtime);
if ischar(defaults.resolution)
  resStr = defaults.resolution;
elseif isvalid(defaults.resolution)
  resStr = [' @ ' char(defaults.resolution(1), 'c') ' res.'];
else
  resStr = '';
end

name = getname(instrument);
if isempty(name)
  name = upper(getabbreviation(instrument));
end
if isempty(name)
  nameType = gettype(mia, 'c');
else
  nameType = [name ' ' gettype(mia, 'l')];
end

% modify standard formmating if not appropriate
if strcmp(defaults.style, 'line')
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    % styleStr = gettype(instrument, 'c');
    styleStr = [gettype(instrument, 'c') ' ' gettype(mia, 'l')];
  end
  windowtitle = [styleStr ': ' beamStr];
  
elseif strcmp(defaults.style, 'image')
  % handle singular / plural properly
  [xi yi ti] = getpixelindices(mia, 'starttime', defaults.starttime, ...
				    'endtime', defaults.endtime);
  if length(ti) > 1
    % multiple images plotted
    plural = 's';
  else
    % single image plotted
    plural = '';
  end
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    styleStr = [nameType ' image' plural];
  end
  
  % % beam information is not important
  % beamStr = '';
  % put image flipaxes information into beamStr, since we have no
  % other use for beamStr and to print into the same place beamStr would
  % appear 
  beamStr = defaults.flipaxes;
  
  if defaults.step > 1 & ~isnan(defaults.resolution)
    % print step information too
    resStr = sprintf('\nresolution %s, plot interval %s', ...
		     char(defaults.resolution), ...
		     char(defaults.resolution * defaults.step, 'c')); 
  end
  windowtitle = styleStr;
  
elseif strcmp(defaults.style, 'keogram')
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    styleStr = [nameType ' keogram'];
  end
  
else
  warning('unknown style');
  plottitle = '';
  windowtitle = '';
  return;
end

if ~isempty(defaults.comment)
  % put comment on separate line
  defaults.comment = sprintf('\n%s', defaults.comment);
end

% if ~isempty(defaults.customstring)
%   styleStr = defaults.customstring;
% end

if ~isempty(beamStr)
  beamStr = ['(' beamStr ')'];
end
% plottitle = sprintf(formatStr, styleStr, beamStr, dateStr, resStr, ...
%    locStr, defaults.comment);
plottitle = { ...
      sprintf(formatStr{1}, styleStr, beamStr) ...
      sprintf(formatStr{2}, dateStr, resStr), ...
      sprintf(formatStr{3}, locStr, defaults.comment) ...
      };

if strcmp(defaults.style, 'keogram') & isa(mia, 'mia_image_base')
  pixunits = getpixelunits(mia);
  xstr = 'X=';
  ystr = 'Y=';
  switch pixunits
   case 'deg'
    unitsfstr = '%g deg';
    xstr = 'Longitude ';
    ystr = 'Latitude ';
   case 'aacgm'
    unitsfstr = '%g deg (AACGM)';
    xstr = 'Longitude ';
    ystr = 'Latitude ';
   case 'km_antenna'
    unitsfstr = '%g km (Antenna-aligned)';
   case 'm_antenna'
    unitsfstr = '%g m (Antenna-aligned)';
   otherwise
    unitsfstr = ['%g ' pixunits];
  end
  
  xpixlen = numel(defaults.xpixelpos);
  ypixlen = numel(defaults.ypixelpos);
  if xpixlen == 1
    plottitle{end+1} = [xstr sprintf(unitsfstr, defaults.xpixelpos)];
  elseif ypixlen == 1
    plottitle{end+1} = [ystr sprintf(unitsfstr, defaults.ypixelpos)];
  else
    error('Keogram must be aligned with grid');
  end
end
