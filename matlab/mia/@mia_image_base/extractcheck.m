function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  Check if can extract subset of data from MIA_IMAGE_BASE object.
%
% See mia_base/EXTRACTCHECK.


if length(varargin) == 1
  % subscript interface
  [sr s] = mia_base_extractcheck(mia, varargin{:});
  return
end


% parameter name/value interface
defaults.xpixelpos = [];
defaults.ypixelpos = [];
defaults.tolerance = [];
[defaults unvi] = interceptprop(varargin, defaults);

[sr s] = mia_base_extractcheck(mia, varargin{unvi});

[xi yi] = getpixelindices(mia, ...
			  'xpixelpos', defaults.xpixelpos, ...
			  'ypixelpos', defaults.ypixelpos, ...
			  'tolerance', defaults.tolerance);

if strcmp(sr.subs{1}, ':')
  sr.subs{1} = xi;
else
  sr.subs{1} = intersect(sr.subs{1}, xi);
end

if strcmp(sr.subs{2}, ':')
  sr.subs{2} = yi;
else
  sr.subs{2} = intersect(sr.subs{2}, yi);
end
