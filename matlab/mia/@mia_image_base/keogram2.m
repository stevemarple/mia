function [fh, gh, cbh] = keogram2(mia, varargin)
%KEOGRAM  Display a MIA_IMAGE_BASE object as a keogram.
%
%   [fh, gh, cbh] = KEOGRAM(mia, ...)
%

fh = [];
gh = [];
cbh = [];

defaults.xpixelpos = [];
defaults.ypixelpos = [];
defaults.miniplots = [1 1];
defaults.starttime = getstarttime(mia);
defaults.endtime = []; % defer (miniPlots might be changed)
defaults.title = ''; % defer
defaults.windowtitle = ''; % defer
defaults.surface2args = {};
% defaults.step = 1;   % number of frames to advance each time
defaults.plotaxes = [];
defaults.seriesstyle = 'printseries';

defaults.visible = 'on'; % final state for figure

[defaults unvi] = interceptprop(varargin, defaults);

% some local copies
st = getstarttime(mia);
et = getendtime(mia);
res = getresolution(mia);
integ = getintegrationtime(mia);

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  fh = [];
else
  gh = [];
end

% check and/or calculate starttime
if (defaults.starttime >= et | ...
    defaults.starttime < st) & 0
  warning('Incorrect start time');
  defaults.starttime = st;
end

% check and/or calculate endtime
if defaults.endtime <= defaults.starttime | ...
      defaults.endtime > et
  warning('Incorrect end time');
  defaults.endtime = et;
elseif isempty(defaults.endtime)
  defaults.endtime = et;
end

if isempty(defaults.xpixelpos) & isempty(defaults.ypixelpos)
  % assume N-S keogram across image centre
  defaults.xpixelpos = mia.xpixelpos(ceil(length(mia.xpixelpos)/2));
  defaults.ypixelpos = mia.ypixelpos;
end

xPixLen = length(defaults.xpixelpos);
yPixLen = length(defaults.ypixelpos);


% [plottitle windowtitle] = maketitle(mia, defaults.starttime, ...
%    defaults.endtime, defaults.step);
windowtitle = 'Keogram';
[pixelStr ylab invertSign] = localIdLoc2str(mia, defaults);

plottitle = sprintf('{\\bf %s keogram}\n%s\n%s @ %s res.', ...
		    gettype(mia, 'c'), pixelStr, ...
		    dateprintf(st, et), char(res,'c')); 
if isempty(defaults.title)
  defaults.title = plottitle;
end


if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', varargin{unvi}, ...
			'miniplots', defaults.miniplots, ...
			'title', defaults.title, ...
			'visible', 'off', ...
			'footer', 'on');
  set(fh, 'Name', windowtitle);   % Write name into figures title bar 
  makeplotfig('addshading', fh);  % Add pixel shading options to menus
end

[data xidx yidx tidx] = keodata(mia, varargin{unvi}, ...
				'xpixelpos', defaults.xpixelpos, ...
				'ypixelpos', defaults.ypixelpos, ...
				'starttime', defaults.starttime, ...
				'endtime', defaults.endtime);


if xPixLen == 1
  % N-S keogram
  ylim = [min(defaults.ypixelpos) max(defaults.ypixelpos)];
  surfYData = defaults.ypixelpos;
elseif yPixLen == 1
  % E-W keogram
  ylim = [min(defaults.xpixelpos) max(defaults.xpixelpos)];
  surfYData = defaults.xpixelpos
else
  % may NOT have to be
  error('Keogram must be aligned with grid');
end
% images may not have been taken at constant intervals,  ensure XData
% relfects this fact
surfXData = (mia.imagetime(tidx) - defaults.starttime) ./ integ;
[X Y] = meshgrid(surfXData, surfYData);
% allow our choice of parameters for surface2 to be modified, but supply
% good defaults for a keogram
surface2(X, Y, data, data, 'parent', gh(1), ...
	 'edgecolor', 'none', ...
	 'method', 'own', ...
	 'subpixel', 'triangle8', ...
	 'debug', 0, ...
	 defaults.surface2args{:});
view(2);

set(gh(1), 'XLim', [0 (defaults.endtime - defaults.starttime) / res], ...
	   'YLim', ylim, ...
	   'TickDirMode', 'manual', ...
	   'TickDir', 'out', ...
	   'Box', 'on');

timetick(gh(1), 'X', mia.imagetime(tidx(1)), ...
	 mia.imagetime(tidx(end)) + integ, integ, 1);

if length(gh) > 1
  % later when plotting into multiple axes this will break
  error(['Please fix ' mfilename ' so that all axes have same colour' ...
	 ' limits']);
end

clim = get(gh(1), 'CLim');

if ~isempty(ylab)
  axes(gh(1));
  ylabel(sprintf('\\bf %s', ylab), 'FontSize', ...
	 get(get(gh(1), 'XLabel'), 'FontSize'));
  if invertSign
    % all negative, so make + and label as deg south
    ytick = get(gh(1), 'YTick');
    yticklabel = cell(length(ytick), 1);
    for n = 1:length(ytick)
      yticklabel{n} = sprintf('%g', -ytick(n));
    end
  end
end

if ~isempty(fh)
  makeplotfig('pixelshading','flat',fh);
  cbh = makeplotfig('addcolorbar', fh, ...
		    'shading', 'flat', ...
		    'clim', clim, ...
		    'xlim', clim, ...
		    'title', datalabel(mia));
  set(fh, 'Pointer', 'arrow', ...
	  'Visible', defaults.visible);

end

return;


% ========================
function [s, ylab, invertSign] = localIdLoc2str(id, defaults)
s = '';
ylab = '';
invertSign = 0;

units = getunits(id);
xpixelpos = defaults.xpixelpos;
ypixelpos = defaults.ypixelpos;


if all(xpixelpos > 0)
  xunits = 'E';
elseif all(xpixelpos < 0)
  xunits = 'W';
  xpixelpos = abs(xpixelpos);
  invertSign = 1;
else
  xunits = 'E/W';
end
if all(xpixelpos > 0)
  yunits = 'N';
elseif all(xpixelpos < 0)
  yunits = 'S';
  ypixelpos = abs(ypixelpos);
  invertSign = 1;
else
  yunits = 'N/S';
end

if length(xpixelpos) == 1 & length(ypixelpos) > 1
  ylab = ['Latitude (%s' yunits ')'];
elseif length(xpixelpos) > 1 & length(xpixelpos) == 1
  ylab = ['Longitude (%s' xunits ')'];
end

if strcmp(units,'deg')
  % units = i18n_entity('deg');
  units = ' deg';
else
  % units = [' ' units];
  units = [units ' '];
end

ylab = sprintf(ylab, units);

switch char(defaults.seriesstyle)
  case {'' 'printseries'}
   xpix = printseries(xpixelpos);
   ypix = printseries(ypixelpos);
 
 case 'keogram'
  xpix = localKeogramSeries(xpixelpos);
  ypix = localKeogramSeries(ypixelpos);
  
 otherwise
  % user-supplied method
  if exists(defaults.seriesstyle)
    xpix = feval(defaults.seriesstyle, xpixelpos);
    ypix = feval(defaults.seriesstyle, ypixelpos);    
  else
    error(sprintf(['Unknown series style and no method ' ...
		   'of the same name (was %s)'], defaults.seriesstyle));
  end
end

[n c] = getname(getlocation(getinstrument(id)));
s = sprintf('%s, %s, %s%s %s %s%s %s', n, c, ...
    xpix, units, xunits, ...
    ypix, units, yunits);
return

% ========================
function r = localKeogramSeries(a)
asz = prod(size(a))
switch asz
 case 0
  r = '';
 
 case 1
  r = sprintf('%g', a);
  
 otherwise
  r = sprintf('%g - %g (in %g)', a(1), a(asz), (a(2)-a(1)));
end
return

