function verify(mia)
%VERIFY  Verify the object is valid
%
%   VERIFY(mia)
%   mia: MIA_IMAGE_BASE object
%
%   VERIFY attempts to verify that the MIA_IMAGE_BASE object is correct,
%   namely that the sizes of some internal fields are correct. It tests if
%   imagedata field is in agreement with the lengths of xpixelpos, ypixelpos
%   and imagetime. It also checks that imagemidtime, resolution and
%   integrationtime fields are the same length as the number of images.

if 0
  stack = dbstack;
  if length(stack) < 2
    caller = '?';
  else
    caller = stack(2).name; % should be the name of who called the
			    % deprecated file
  end
  mesg = sprintf('VERIFY called from %s\n%s\n', caller);
  
  warningStatus = warning; % remember status
  warning('on');
  warning(mesg);
  warning(warningStatus); % return to previous warning status
end

% idsz = size(mia.imagedata);
idsz = getdatasize(mia);
xlen = length(mia.xpixelpos);
ylen = length(mia.ypixelpos);
tlen = length(mia.imagetime);

if ~isequal(idsz, [xlen ylen tlen])
  if isequal(idsz, [ylen xlen tlen])
    error('imagedata wrong size, probably has X and Y coordinates swapped');
  end
  error('imagedata size does not agree with other pixel positions/image times');
end

if length(mia.imagemidtime) ~= tlen
  error('imagemidtime incorrect length');
end

if length(getresolution(mia)) ~= tlen
  error('resolution incorrect length');
end

if length(getintegrationtime(mia)) ~= tlen
  error('integrationtime incorrect length');
end
