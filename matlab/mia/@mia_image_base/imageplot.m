function [fh, gh] = imageplot(mia, varargin)

% some local copies
% starttime = getstarttime(mia);
% endtime = getendtime(mia);
% resolution = getresolution(mia);

% our default values
defaults.miniplots = [8 8];
defaults.starttime = []; % starttime;
defaults.endtime = []; % defer (miniPlots might be changed)
defaults.xpixelpos = [];
defaults.ypixelpos = [];
defaults.title = []; % defer
defaults.windowtitle = []; % defer
defaults.step = 1;   % number of frames to advance each time
defaults.plotaxes = [];
defaults.addcolorbar = [];
defaults.shading = 'flat';
defaults.clim = [nan nan]; % not set for either limit
defaults.method = '';
defaults.logo = info(getinstrument(mia), 'logo');

% use transparency if available. Printing with transparency may cause
% problems.
defaults.usetransparency = 0; 

defaults.heaterevent = [];

% get flipaxes names from makeplotfig, then if the printed name changes this
% will still work properly
fanames = makeplotfig('flipaxesnames');
defaults.flipaxes = fanames{1};

% allow automatic plotting/printing
defaults.autoprint = 0;
defaults.autoprintoverlap = 6;
defaults.autoprinttries = 1;
defaults.scale = 'automatic';
defaults.autoprintfilename = ''; % generate based on start/end times
defaults.autoprintfilenameextension = '';
defaults.autoprintpath = '';
defaults.autoprintdevice = '-dpsc2';
defaults.autoprintappend = 0;

% plot labelling defaults
% Valid styles are 'number', 'timestamp'
% defaults.imagelabelstyle = 'timestamp'; 
% defaults.imagelabelposition = [0.05 0.05];
% defaults.imagelabeltimeformat = '%H:%M:%S';
% defaults.imagelabelnumberformat = '%d';
% defaults.imagelabelcolor = 'w';
% defaults.imagelabelfontsize = 10;
% defaults.imagelabelverticalalignment = 'bottom';
% defaults.imagelabelhorizontalalignment = 'left';
% defaults.imagelabelvisible = 'off'; % inital condition, change via menu

defaults.tolerance = 1024*eps; % tolerance used for comparing pixels

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.xpixelpos)
  defaults.xpixelpos = mia.xpixelpos;
end
if isempty(defaults.ypixelpos)
  defaults.ypixelpos = mia.ypixelpos;
end

if isempty(defaults.starttime)
  defaults.starttime = getstarttime(mia);
elseif numel(defaults.starttime) > 1
  error('start time must be scalar');
elseif isa(defaults.starttime, 'timestamp')
  ; % ok, do nothing
else
  error('bad value for start time');
end

% cut mia down to size
mia = extract(mia, ...
	      'starttime', defaults.starttime, ...
	      'endtime', defaults.endtime, ...
	      'xpixelpos', defaults.xpixelpos, ...
	      'ypixelpos', defaults.ypixelpos, ...
	      'tolerance', defaults.tolerance);
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
% resolution = getresolution(mia);


if isempty(defaults.method) | strcmp(defaults.method, 'automatic')
  % choose most appropriate method. Take the simplest one we can get away
  % with
  if length(mia.xpixelpos) < 2
    xDiffSame = 1;
  else
    xDiffSame = all(abs(diff(mia.xpixelpos) - diff(mia.xpixelpos(1:2))) <= ...
		    defaults.tolerance);
  end
  if length(mia.ypixelpos) < 2
    yDiffSame = 1
  else
    yDiffSame = all(abs(diff(mia.ypixelpos) - diff(mia.ypixelpos(1:2))) <= ...
		    defaults.tolerance);
  end
  if strcmp(lower(defaults.shading), 'flat') & xDiffSame & yDiffSame
    defaults.method = 'image';
  elseif length(mia.xpixelpos) > 100 & length(mia.ypixelpos) > 100 
    defaults.method = 'surface'; 
  else
    defaults.method = 'surfaceimage'; % visually the best, but more work
  end
  disp(sprintf('\rUsing %s to display images', defaults.method));
end


% ----- autoprint -----
if strcmp(defaults.autoprint, 'on') ...
      | (isnumeric(defaults.autoprint) & defaults.autoprint > 0)
  % [fh gh] = localAutoPrint(mia, beams, defaults, varargin{unvi});
  [fh gh] = localAutoPrint(mia, defaults, varargin{unvi});
  return;
end
% ----- autoprint (end)


if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  fh = [];
else
  gh = [];
end

% by default add color only when creating a figure
if isempty(defaults.addcolorbar)
  defaults.addcolorbar = isempty(defaults.plotaxes);
end

% find first image
sampleStart = 1;

dsz = getdatasize(mia);
sampleEnd = min(prod(defaults.miniplots).* defaults.step - ...
		  (defaults.step-1), dsz(end));

samt = getsampletime(mia);
it = getintegrationtime(mia);
title_st = samt(sampleStart) - it(sampleStart)./2;
title_et = samt(sampleEnd) + it(sampleEnd)./2;
% calculate title now that beams, starttime etc are known
[title windowtitle] = maketitle(mia, ...
				'starttime', title_st, ...
				'endtime', title_et, ...
				'step', defaults.step, ...
				'flipaxes', defaults.flipaxes);
% 'endtime', defaults.endtime, ...
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end


% Calculate the spatial limits
xlim = [mia.xpixelpos(1) mia.xpixelpos(end)];
ylim = [mia.ypixelpos(1) mia.ypixelpos(end)];


if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to overrmiae
  [fh gh] = makeplotfig('init', varargin{unvi}, ...
			'miniplots', defaults.miniplots, ...
			'title', defaults.title, ...
			'logo', defaults.logo, ...
			'footer', 'on');
  % add shading options
  makeplotfig('addshading', fh, 'shading', defaults.shading);
  % add option to reverse images
  makeplotfig('addreverseimages', fh);
end

% Write name into figures title bar, preserve image label color, but
% don't have a background color in the figure (ought to be blank)
fh_color = 'w';
if matlabversioncmp('<=', '5.1')
  fh_color = get(0, 'DefaultFigureColor');
end
set(fh, 'Name', windowtitle, ...
	'InvertHardCopy', 'off', ...
	'Color', fh_color);

% plot axes should contain basic object information, but not the full
% data. Processing information is unnecessary too. call extract() on mia2
% later so that only information pertaining to image n is stored.
mia2 = setdata(mia, []);
mia2 = setprocessing(mia, {});


% turn off axes visibility on all plot windows since it leaves a black
% line on the bottom and left of the image(s). This does not affect
% children of the axes.
set(gh, 'Visible', 'off');

ghUsed = zeros(size(gh)); % flags to indicate which axes contain real data
m = 1;
n = sampleStart;
[xMesh yMesh] = meshgrid(mia.xpixelpos, mia.ypixelpos);
% imZData = zeros(length(mia.xpixelpos), length(mia.ypixelpos));
imZData = zeros(length(mia.ypixelpos), length(mia.xpixelpos));
for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    axes(gh(x,y));

    if n <= sampleEnd
      % im = mia.imagedata(:,:,n)';
      im = getdata(mia, ':', ':', n)';
      
      % check if any image is present
      if ~isnan(im(1))
	imageOk = 1;
	% Matlab doesn't do short-circuit evaulation
      % elseif isempty(find(~isnan(im)))
      elseif all(isnan(im))
	% all Nans
	imageOk = 0;
      else
	imageOk = 1;
      end

      if imageOk
	% remember tag because the 'high-level' graphics functions lose it 
	keepKeys = {'Tag' 'UserData'};
	keepVal = get(gh(x,y), keepKeys); 
	% ud = get(gh(x,y), 'UserData');  % remember UserData too
	% surf(xMesh, yMesh, im);
	switch defaults.method
	 case 'surface'
	  surface('Parent', gh(x,y), ...
		  'XData', xMesh, ...
		  'YData', yMesh, ...
		  'ZData', imZData, ...
		  'CData', double(im));
	  % 'ZData', double(im), ...
	 case 'surfaceimage'
	  surfaceimage(xMesh, yMesh, imZData, double(im), ...
                       'parent', gh(x,y));
	  
	 case 'image'
	  % image makes the axis visible. grrrr.
	  imh = image(im, 'Parent', gh(x,y), ...
		      'XData', mia.xpixelpos, ...
		      'YData', mia.ypixelpos, ...
		      'CDataMapping', 'scaled');
	  set(gh(x, y), 'Visible', 'off', 'YDir', 'normal');
	  % set(gh(x, y), 'Visible', 'off');
	  
	  % not sure if it works properly with 6.1. Limit to 6.5 and
          % above
	  if any(isnan(im(:))) & matlabversioncmp('>=', '6.5') ...
		& logical(defaults.usetransparency)
	    ad = double(~isnan(im));
	    im(isnan(im)) = min(im(:)); % don't alter limits!

	    % set(imh, 'CData', im, 'AlphaData', ad);
	    set(imh, ...
		'CData', im, ...
		'AlphaDataMapping', 'none', ...
		'AlphaData', ad);
	  end
	  
	 otherwise
	  error(sprintf('unknown method (was %s)', defaults.method));
	end
	
	% shading interp;
	shading(defaults.shading)
	view(2);
	set(gh(x,y), keepKeys, keepVal);
	ghUsed(x,y) = 1;
      
      else
	% blank axes ?
	disp('warning: blank image');
	set(gh(x,y), 'Visible', 'on', ...
		     'Color', 'white', ...
		     'XColor', 'white', ...
		     'YColor', 'white', ...
		     'Box', 'on', ...
		     'Layer', 'top', ...
		     'XTick', [], ...
		     'YTick', []);
	lims = get(gh(x,y), {'XLim', 'YLim'});
	text('Parent', gh(x,y), ...
	     'String', sprintf('Missing\ndata'), ...
	     'HorizontalAlignment', 'center', ...
	     'VerticalAlignment', 'middle', ...
	     'Units', 'data', ...
	     'Position', [mean(xlim), mean(ylim)]);
	% 'Position', [(lims{1}(2) - lims{1}(1))/2, ...
	% 	  (lims{2}(2) - lims{2}(1))/2]);
	
	% axes(gh(x,y));
	% cla
      end
      % set(gh(x,y), 'UserData', ...
      %	   {mia.imagetime(n), mia.imagetime(n)+resolution, n});
      % % {mia.imagetime(n), mia.imagetime(n)+resolution});
      % ud.imagenumber = n;
      ud = [];
      ud.imagenumber = n;
      ud.mia = setdata(extract(mia2, {':', ':', n}), []);
      set(gh(x,y), 'UserData', ud);

    else
      % out of samples, blank (axis should already be invisible)
      % set(gh(x,y), 'Visible', 'off');
    end
    m = m + 1;             % advance to next axes
    n = n + defaults.step; % advance to next sample
  end
end

set(gh,'XLimMode', 'manual', ...
       'YLimMode', 'manual', ...
       'XLim', xlim, ...
       'yLim', ylim);

% set colour limits to all be same
% climTmp = get(gh, 'CLim');
climTmp = get(gh(find(ghUsed)), 'CLim');

if iscell(climTmp)
  climTmp = [climTmp{:}];
end
% for backward compatibility where an empty matrix used for default
if isempty(defaults.clim)
  defaults.clim = [nan nan];
end

if any(ghUsed(:))
  if isnan(defaults.clim(1))
    defaults.clim(1) = min(climTmp(:));
  end
  if isnan(defaults.clim(2))
    defaults.clim(2) = max(climTmp(:));
  end
else
  % set to defaults
  defaults.clim = get(gcf,'DefaultAxesCLim');
end

aspectRatio = getaspectratio(mia);
aspectRatio(3) = 1;
       
set(gh, 'PlotBoxAspectRatio', aspectRatio);

if defaults.clim(2) <= defaults.clim(1)
  dataTmp = getdata(mia, ':', ':', sampleStart:defaults.step:sampleEnd);
  defaults.clim = [min(dataTmp(:)) max(dataTmp(:))];
  clear dataTmp;
end

set(gh,'CLimMode', 'manual', ...
       'CLim', defaults.clim);

if logical(defaults.addcolorbar)
  if 1
    % this method scales the colorbar data, allowing the number of
    % tickmarks to be adjusted.
    [label tmp2 tmp3 tmp4 mul] = datalabel(mia, 'values', defaults.clim(2));

    mul = power(10, -mul); % convert from log value to the fudge factor
    % add colourbar
    cbh = makeplotfig('addcolorbar', get(gh(1), 'Parent'), ...
		      'shading', defaults.shading, ...
		      'clim', defaults.clim * mul, ...
		      'xlim', defaults.clim * mul, ...
		      'title', label);
  else
    % this method keeps the colorbar data the same but changes the
    % ticklabels. This means the number of values of the tick marks must
    % not change, otherwise their labels are incorrect
    
    % add colourbar
    cbh = makeplotfig('addcolorbar', get(gh(1), 'Parent'), ...
		      'shading', defaults.shading, ...
		      'clim', defaults.clim, ...
		      'xlim', defaults.clim, ...
		      'title', '');
    % 'title', datalabel(mia));

    % fix colorbar to use sensible units
    ticks = get(cbh, 'XTick');
    [label tmp2 tmp3 ticklabels] = datalabel(mia, 'values', ticks);
    % set(cbh, 'XTickLabel', ticklabels);
    set(cbh, 'XTickLabel', ticklabels, 'XTickLabelMode', 'manual');
    set(get(cbh,'XLabel'), 'String', label);
  
  end
end

% a hack to get the paper size/aspect ratio correct for 8x8 images
if ~isempty(fh)
  set(fh, 'PaperType', 'a4', ...
	  'PaperPositionMode', 'manual', ...
	  'PaperUnits', 'centimeters');
  paperPos = [0 0 18 24];
  paperPos(1:2) = (get(fh, 'PaperSize') - paperPos(3:4)) * 0.5;
  set(fh ,'PaperPosition', paperPos, ...
	  'Pointer', 'arrow');
end

% -------
if isempty(defaults.plotaxes) 
  if exist('heatermenu')
    % make tools menus visible
    toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
    if ~isempty(toolsMenuHandle)
      set(toolsMenuHandle, 'Visible', 'on');
      % load the heater menu with the same on/off times used for creating
      % the image plot(s)
      heatermenu('init', toolsMenuHandle, ...
		 'starttime', defaults.starttime, ...
		 'endtime', defaults.endtime, ...
		 'onofftimes', 'on', ...
		 'heaterevent', defaults.heaterevent);
    end
  end
   
end


% -------
% add image label stuff to the menus
imagelabel(varargin{unvi}, 'action', 'init', 'image', mia, 'figure', fh);

% if any imagelabels were added then make the menu item visible, and set
% the Checked property to the same state as the image label visibility
ilh = findobj(fh, 'Type', 'Text', 'Tag', 'imagelabel');
% if ~isempty(ilh)
%   set(findobj(fh, 'Type', 'uimenu', 'Tag', 'showimagelabels'), ...
%       'Visible', 'on', ...
%       'Checked', defaults.imagelabelvisible);
% end

return;



% ===============================================================
% function [fh, gh] = localAutoPrint(mia, beams, defaults, varargin)
function [fh, gh] = localAutoPrint(mia, defaults, varargin)
fh = [];
gh = [];

defaults.autoprint = 0; % turn it off now to avomia recursive calls
if isempty(defaults.endtime)
  defaults.endtime = getendtime(mia);
end
% defaults.beams = beams;
if isempty(defaults.autoprintfilename)
  fstr = '%Y%m%d%H%M%S'
  defaults.autoprintfilename = [strftime(defaults.starttime, fstr) '-' ...
		    strftime(defaults.endtime, fstr)];
end

switch defaults.scale
 case 'fixed'
  % data = getdata(mia, beams, ':');
  mi = nan; % initialise running min and max with nan
  ma = nan;
  for n = 1:getimagesize(mia, 2)
    data = getimage(mia, n);
    mi = min(mi, min(data(:)));
    ma = max(ma, max(data(:)));
  end
  % choose min and max - not the cleverest method but will do for now
  % defaults.clim = [min(data(:)), max(data(:))];
  defaults.clim = [mi ma];
  
 case 'automatic'
  % no need to do anything
  
 otherwise
  error(['Unknown scale type was (' defaults.scale ')']);
end

% convert all the default name/values to a cell array
fn = fieldnames(defaults);
args = cell(1, 2*length(fn));
for n = 0:(length(fn)-1)
  args{(2*n)+1} = fn{n+1};
  args{(2*n)+2} = getfield(defaults, fn{n+1});
end
% if isempty(defaults.endtime)
%   defaults.endtime = getendtime(mia);
% end
if defaults.autoprintdevice(1) ~= '-';
  defaults.autoprintdevice = ['-' defaults.autoprintdevice];
end
if isempty(defaults.autoprintfilenameextension)
  switch defaults.autoprintdevice
   case {'-dps' '-dpsc' '-dps2' '-dpsc2'}
    ext = 'ps';
   case {'-deps' '-depsc' '-deps2' '-depsc2'}
    ext = 'eps';
   case {'-dpdfwrite' '-dpdf'}
    ext = 'pdf';
   case '-dhpgl'
    ext = 'hpgl';
   case '-dtiff' 
    ext = 'tiff';
   case {'-dpcxmono' '-dpcx16' '-dpcx256' '-dpcx24b'}
    ext = 'pcx';
   case {'-dpbm' '-dpbmraw'}
    ext = 'pbm';
   case {'-dpgm' '-dpgmraw'}
    ext = 'pgm';
   case {'-dppm' '-dppmraw'}
    ext = 'ppm';
   otherwise
    ext = 'prn';
  end
  defaults.autoprintfilenameextension = ext;
end

res = getresolution(mia);
sameRes = all(res(1) == res)
imageStepTS = defaults.step * res(1); % timespan between images
plotDuration = prod(defaults.miniplots) * imageStepTS;

st = defaults.starttime;
et = defaults.endtime;
t1 = st;
failedtries = 0; % the number of times aligning failed
page = 1;

while t1 < et
  align = 0;
  if sameRes ...
	& defaults.autoprintoverlap > 0 ...
	& failedtries <= defaults.autoprinttries ...
	& isequal(defaults.miniplots, [8 8]) ...
	& rem(defaults.starttime, imageStepTS) == timespan
    % attempt to be smart, find the best alignment within the allowed
    % number of overlaps.
    tAlign = []; % best alignment time
    tAlignStep = []; % best alignment step (biggest)
    for n = 0:1:defaults.autoprintoverlap
      tTest = max(t1 - n*imageStepTS, st);
      if rem(tTest, 60*imageStepTS) == timespan
	% align to 60*imageStep
	align = 1;
	tTestStep = 60*imageStepTS;
      elseif rem(tTest, timespan(1, 'm')) == timespan
	% align to 1 minute boundary
	align = 1;
	tTestStep = timespan(1, 'm');
      end
      % if align
      % 	t1 = tTest;
      % 	break;
      %     end
      if align
	if isempty(tAlign)
	  tAlign = tTest;
	  tAlignStep = tTestStep;
	elseif tTestStep > tAlignStep
	  tAlign = tTest;
	  tAlignStep = tTestStep;
	end
      end
    end
    if align
      t1 = tAlign;
    else
      failedtries = failedtries + 1;
    end
  end
  t2 = min(t1 + plotDuration, et);
  [fh gh] = imageplot(mia, args{:}, varargin{:}, ...
		      'starttime', t1, ...
		      'endtime', t2);

  varargin{:}
  disp(sprintf('VISIBLE2: %s', get(fh, 'visible')));
  
  if defaults.autoprintappend
    fname = sprintf('%s.%s', defaults.autoprintfilename, ...
		    defaults.autoprintfilenameextension);
    if ~isempty(defaults.autoprintpath)
      [tmp fname ext] = fileparts(fname);
      fname = fullfile(defaults.autoprintpath, [fname ext]);
    end
    if page > 1
      app = {'-append'};
    else
      app = {};
    end
    % print(fh, defaults.autoprintdevice, '-append', fname);
    print(fh, defaults.autoprintdevice, app{:}, fname);
  else
    fname = sprintf('%s_p%d.%s', defaults.autoprintfilename, page, ...
		    defaults.autoprintfilenameextension);
    if ~isempty(defaults.autoprintpath)
      [tmp fname ext] = fileparts(fname);
      fname = fullfile(defaults.autoprintpath, [fname ext]);
    end
    print(fh, defaults.autoprintdevice, fname);
  end
  close(fh);
  fh = [];
  t1 = t2;
  page = page + 1;
end

% have to return fh and gh since calling functions expect them, however they
% are too late to do anything useful with them. Since we have already called
% the derived version imageplot with autoprint=off for each page, the
% original imageplot() should have already done everything necessary


return; 

