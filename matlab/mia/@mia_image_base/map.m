function h = map(mia, ah, varargin)
%MAP  Map MIA_IMAGE_BASE object(s) onto a map.
%
%   h = MAP(mia, ah, ...)
%
% Plot an image on a map. The following parameter name/value pairs are
% accepted:
%
% 'time', TIMESTAMP or numeric data
% The time of the image (or sample number) to be added to the map.
%
% See also MIA_IMAGE_BASE.

ah = [];
defaults.time = 1;

[defaults unvi] = interceptprop(varargin, defaults);


if isa(defaults.time, 'timestamp')
  % convert to sample number
  n = find(defaults.time == mia.imagetime);
  if isempty(n)
    error(sprintf('Cannot find image corresponding to %s', ...
		  char(defaults.time)));
  end
  defaults.time = n;
end

h = patch(mia, 'time', defaults.time, 'axes', ah, varargin{unvi});
