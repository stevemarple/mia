function [fh, gh, ph] = plot(mia, varargin)

fh = [];
gh = [];
ph = [];

% st = getstarttime(mia);
res = getresolution(mia);
[xpix ypix pixunits] = getpixels(mia);
instrument = getinstrument(mia);

% look for starttime, endtime, xpixelpos, ypixelpos and 
[xi yi ti w defaults unvi1] = getpixelindices(mia, varargin{:});
for n = 1:length(w)
  % issue any warnings
  warning(w{n});
end

defaults.miniplots = [1 1];
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.ylim = info(instrument, 'limits', mia);

[defaults unvi2] = interceptprop(varargin, defaults);
unvi = union(unvi1, unvi2);


if isempty(defaults.xpixelpos)
  xi = ceil(length(xpix) / 2);
  defaults.xpixelpos = xpix(xi);
end
if isempty(defaults.ypixelpos)
  yi = ceil(length(ypix) / 2);
  defaults.ypixelpos = ypix(yi);
end
if ~isequal(size(defaults.xpixelpos), size(defaults.ypixelpos))
  error('xpixelpos and ypixelpos must be the same size');
end
numPixelsToPlot = numel(defaults.xpixelpos);

if isempty(defaults.starttime)
  defaults.starttime = getstarttime(mia);
end
if isempty(defaults.endtime)
  defaults.endtime = getendtime(mia);
end

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.xpixelpos); % override setting
  gh = defaults.plotaxes;
  if any(~ishandle(gh))
    error('invalid handle');
  end
  args = varargin(unvi);
  fh = get(gh, 'Parent');
  if iscell(fh)
    fh = cell2mat(fh);
  end
  fh = unique(fh);
else
  gh = [];
  args = {varargin{unvi}};
end

if numPixelsToPlot == 1
  titleComment = sprintf('Pixel location: %s', ...
			 char(location('', '', defaults.ypixelpos, ...
				       defaults.xpixelpos)));
else
  titleComment = '';
end

[title windowtitle] = maketitle(mia, 'comment', titleComment);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc
if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', args{:}, ...
			'logo', info(instrument', 'logo'), ...
			'miniplots', defaults.miniplots, ...
			'pointer', 'watch', ...
			'title', defaults.title);
end


% multiple beams - make subplots for all beams requested
if defaults.miniplots == [1 1] & numPixelsToPlot > 1
  n = 1:numPixelsToPlot; % plot all at once
else
  n = 1;
end


data = getdata(mia);
samt = getsampletime(mia);
xdata = getcdfepochvalue(samt);

tiLen = length(ti);
for yn = 1:length(yi)
  for xn = 1:length(xi)
    if n <= numPixelsToPlot
      keepKeys = {'Tag' 'UserData'};
      keepVal = get(gh(xn, yn), keepKeys); 

      ph2 = plot(xdata, reshape(data(xi(xn), yi(yn), ti), [1 tiLen]), ...
		 'Parent', gh(xn, yn), ...
		 'Color', defaults.color); 
      ph2 = flipud(ph2); % last line is first
      
      ph = [ph; ph2];
      
    else
      % axis blank 
      set(gh(xn, yn), 'Visible', 'off');
      
    end
    n = n + 1;
    
  end
end


if length(gh) == 1
  set(get(gh, 'YLabel'), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14);
else
  % turn off tick labels - too crowded
  set(gh, 'XTick', [], 'YTick', []);
  % label, but invisibly because they can make the plot too busy
  set(cell2mat(get(gh, 'YLabel')), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14, ...
      'Visible', 'off');
end

set(gh, ...
    'Tag', 'plot', ...
    'XLim', ...
    [0 getcdfepochvalue(defaults.endtime - defaults.starttime)]);
    
timetick2(gh, 'label', true);


% ---------------------------------------------
if isempty(defaults.plotaxes)
  set(fh, 'Name', defaults.windowtitle, ...
	  'Pointer', 'arrow');
end
