function varargout = getpixels(mia)
%GETPIXELS  Return the X and Y pixel details from a MIA_IMAGE_BASE object.
%
%   [x, y, units] = GETPIXELS(mia)
%
%   x: X pixel positions
%   y: Y pixel positions
%   units: pixel units
%   mia: MIA_IMAGE_BASE object
%
%   See also MIA_IMAGE_BASE.

varargout{1} = mia.xpixelpos;
varargout{2} = mia.ypixelpos;
varargout{3} = mia.pixelunits;
