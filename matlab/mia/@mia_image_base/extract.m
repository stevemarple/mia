function [r,sr,s] = extract(mia, varargin)
%EXTRACT  Extract a subset of data from a MIA_IMAGE_BASE object.
%
% See mia_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end


[r sr s] = mia_base_extract(mia, varargin{:});

if ~strcmp(sr.subs{1}, ':')
  r.xpixelpos = r.xpixelpos(sr.subs{1});
end

if ~strcmp(sr.subs{2}, ':')
  r.ypixelpos = r.ypixelpos(sr.subs{2});
end
