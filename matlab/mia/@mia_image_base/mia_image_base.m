function mia = mia_image_base(varargin)
%MIA_IMAGE_BASE  Constructor for MIA_IMAGE_BASE class.
%
%   MIA_IMAGE_BASE is an abstract class from which concrete types should
%   be derived.
%
%   Note that the imagedata field is a three-dimensional matrix, which
%   should be addressed as (x, y, t).
%
%   See also MIA_BASE, RIO_IMAGE.

%
% Log$

cls = 'mia_image_base';
parent = 'mia_base';

% version 4 born at the same time as sampletime added to mia_base,
% therefore no need to imagemidtime or imagetime
latestversion = 4;
mia.versionnumber = latestversion;
mia.xpixelpos = [];
mia.ypixelpos = [];
mia.pixelunits = '';
% mia.imagedata = []; % only in versions 1,2
% mia.imagedata = zeros(0, 0, 0); % empty 3D matrix

% mia.imagetime = []; removed in version 4
% mia.imagemidtime = []; % only in versions 2 and 3

% metres (exact meaning depends on instrument, for imaging riometers this is
% a projection height)
mia.height = []; 


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  mia = class(mia, cls, feval(parent));
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};

elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {parent, 'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {parent});
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent));

  % copy common fields (not base class)
  for n = 1:prod(size(a))
    an = a(n);
    for m = 1:length(fn)
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end

    base = getfield(an, parent); % parent class should already be current
    switch tmp.versionnumber
     case 1
      % % calculate imagemidtime       
      % tmp.imagemidtime = getresolution(base)/2 + tmp.imagetime;
      base = setdata(base, an.imagedata);
     case 2
      % used imagedata field, not mia_base.data
      % tmp = setfield(tmp, parent, setdata(getfield(tmp, parent), ...
      % 					  tmp.imagedata));
      if isfield(an, 'imagedata')
	base = setdata(base, an.imagedata);
      end
      base = setsampletime(base, an.imagemidtime);
     case 3
      base = setsampletime(base, an.imagemidtime);
     case 4
      ; % latest
     otherwise
      error('unknown version');
    end
    tmp = class(tmp, cls, base);
    mia(n) = tmp;
  end
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [mia unvi] = interceptprop(varargin, mia);
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave parent to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);

else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

mia.versionnumber = latestversion;

n = [1 getdatasize(mia, 3)];

