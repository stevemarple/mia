function h = plotfov(mia, ah, varargin)
%PLOTFOV  Plot an instrument's field of view on a map.
%
%   h = PLOTFOV(mia, ah, ...)
%   h: PATCH handle(s)
%   mia: MIA_IMAGE_BASE object
%   ah: handle of axis to plot into
%
%   PLOTFOV will add the actual field of view (if appropriate) onto a
%   map. It will attempt to identify if the axis contains M_MAP coastlines,
%   and if so will use M_PATCH/M_LINE instead of PATCH/LINE.
%
%   The default behaviour can be modified with the following
%   parameter/value pairs:
%
%     'style', 'line' | 'patch'
%     Select whether to lines or patchs are used. This option does not
%     select between M_LINE and LINE, (use linefunction) or M_PATCH and
%     PATCH (use patchfunction).
%  
%     'linefunction', CHAR
%     The function to call for adding LINE objects to an AXIS.
%
%     'lineargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the line
%     function. Use to modify the edge and face color, etc.
%
%     'patchfunction', CHAR
%     The function to call for adding PATCH objects to an AXIS.
%
%     'patchargs', CELL
%     A CELL vector of parameter/value pairs to be passed to the patch
%     function. Use to modify the edge and face color, etc.
%
%   See also mia_instrument_base/PLOTFOV, MIA_IMAGE_BASE, AXIS, PATCH,
%   M_MAP, M_PATCH. 

h = [];
defaults.style = 'patch'; % patch | line
defaults.patchfunction = [];  % defer
defaults.patchargs = {'EdgeColor', 'k', ...
		    'FaceColor', 'none'};

if matlabversioncmp('>=', '6')
  defaults.patchargs{end} = 'k';
  defaults.patchargs{end+1} = 'FaceAlpha';
  defaults.patchargs{end+1} = 0.2;
end

defaults.linefunction = [];  % defer
defaults.lineargs = {'Color', 'k', ...
		    'LineStyle', '-', ...
		    'Marker', 'none'};

defaults.units = 'deg'; % output units

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.patchfunction)
  % does axis have any children which look they they belong to m_map?
  if ismmap(ah)
    defaults.patchfunction = 'm_patch';
  else
    defaults.patchfunction = 'patch';
  end
end
if isempty(defaults.linefunction)
  % does axis have any children which look they they belong to m_map?
  if ismmap(ah)
    defaults.linefunction = 'm_line';
  else
    defaults.linefunction = 'line';
  end
end
 
sz = prod(size(mia));

Nout = 0;
for n = 1:sz
  [x y units] = getpixels(mia(n));
  if length(x) < 2 | length(y) < 2;
    warning('refusing to plot field of view for singular point');
  else    
    x = sort(x(:)');
    y = sort(y(:)');
    xlen = length(x);
    ylen = length(y);
    xl = x(1) - 0.5*diff(x([1 2]));         % outside left edge
    xr = x(end) + 0.5*diff(x([end-1 end])); % outide right edge
    
    yb = y(1) - 0.5*diff(y([1 2]));         % outside bottom edge
    yt = y(end) + 0.5*diff(y([end-1 end])); % outside top edge
    
    xx = [xl x repmat(xr, 1, ylen+2) fliplr(x)  repmat(xl, 1, ylen+1)];
    yy = [repmat(yb, 1, xlen+2) y repmat(yt, 1, xlen+2) fliplr(y)];
    
    x = xx;
    y = yy;
    coord2degArgs = {getlocation(getinstrument(mia(n))), x, y, ...
		     defaults.units};

    if strcmp(defaults.units, 'm_antenna') | ...
	  strcmp(defaults.units, 'km_antenna') 
      coord2degArgs{end+1} = 'antennaazimuth';
      coord2degArgs{end+1} = info(getinstrument(mia(n)), 'antennaazimuth');
    end
  
    switch defaults.style
     case 'patch'
      if strcmp(defaults.patchfunction, 'm_patch')
	[x y] = coord2deg(coord2degArgs{:});
      end
      tmp = feval(defaults.patchfunction,  x, y, 'r', ...
		  defaults.patchargs{:});
      
     case 'line' 
      if strcmp(defaults.linefunction, 'm_line')
	[x y] = coord2deg(coord2degArgs{:});
      end
      
      % unlike patches lines must be explicitly closed back to the start
      if ~isempty(x)
	x = [x(:); x(1)];
	y = [y(:); y(1)];
      end
      tmp = feval(defaults.linefunction,  x, y, ...
		  defaults.lineargs{:});
      
     otherwise
      error('Unknown style');
    end
    
    h = [h; tmp];
  end
end



