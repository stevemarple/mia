function [r, hasSign] = minimisestddev(a, b)
%MINIMISESTDDEV Helper function for DCADJUST.
%
%   [r, hasSign] = MINIMISESTDDEV(a, b);
%   r: error value for iteration routine
%   hasSign: always 0, inidicating the the error value is not signed
%
%   MINIMISESTDDEV is a helper function for DCADJUST, and is called as
%   part of the iteration routine. The error value is never negative, so
%   no cannot indicate in which direction the iteration routine should
%   proceed. hasSign is always 0, so that DCADJUST should move by a
%   small amount to determine the correct direction itself.
%
%   See also DCADJUST, MINIMISESIGNDIFF.

tmp = power(b - a, 2);
r = sum(tmp(isfinite(tmp(:))));
hasSign = 0;
