function r = mean50(data, varargin)
%MEAN50  Similar to NONANMEAN, but require that >= 50% of values are not NAN.
%
%   r = MEAN50(x)
%   r = MEAN50(x, dim)
%
% Take the NONANMEAN value ignoring the effect of any NANs. If more than
% 50% of the values are NAN then return NAN.
%
% See also MEAN, NONANSUM, NONANMEDIAN, NONANISEQUAL.

% Sum, excluding the effects of nans
s = nonansum(data, varargin{:});

% Count the number of non-NaN elements
div = sum(~isnan(data), varargin{:});

% Count the number of elements
count = sum(ones(size(data)), varargin{:});

% If there are no non-NaN elements then ensure NaN is returned for the mean
% value. (This would actually happen since both s and div are 0, however
% avoid the divide by zero error by converting zero elements in div to
% NaNs).
div(div == 0) = nan;

% If the divisor is < 50% of the count then set to nan
div(div ./ count < .5) = nan;

% return the mean
r = s./div;

