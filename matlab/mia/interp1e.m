function yi = interp1e(varargin)
%INTERP1E 1-D interpolation (INTERP1) with extrapolation beyond table ends.
%
%   yi = INTERP1E(x, y, xi)
%
%   Interpolation with INTERP1, but data beyond ends of table is
%   extrapolated, whereas INTERP1 inserts NAN.
%
%   See also INTERP1.

method = [];

switch nargin 
  case 2
    y = varargin{1};
    xi = varargin{2};
    ysz = size(y);
    ylen = length(y); 
    x = 1:prod(ysz);
    
  case 3
    if ischar(varargin{3})
      method = varargin{3};
      y = varargin{1};
      xi = varargin{2};
      ysz = size(y);
      ylen = length(y); 
      if ysz(1) == 1
	x = 1:ysz(2);
      else
	x = 1:ysz(1);
      end
    else
      x = varargin{1};
      y = varargin{2};
      ysz = size(y);
      ylen = length(y); 
      xi = varargin{3};
      
    end
    
  case 4
    x = varargin{1};
    y = varargin{2};
    xi = varargin{3};
    method = varargin{4};
    ysz = size(y);
    ylen = length(y); 
end

% sanity checks
if prod(size(x)) ~= length(x)
  error('x must be a vector');
end
if prod(size(xi)) ~= length(xi)
  % error('xi must be a vector');
end

if isempty(method)
  method = 'linear'; % default method
end

% vector or matrix 
if prod(ysz) == ylen
  % vector
  % reshape y so we can assume the interpolation is always performed
  % columnwise. The shape of the returned matrix is dependent upon xi,
  % not yi
  ysz = [ylen 1];
  y = reshape(y, ysz);
end


yi = interp1(x, y, xi, method);


% find row/column of every xi which is outside the table. x must be
% monotonic so only need to look at first and last values
xmin = min(x(1), x(ysz(1)));
xmax = max(x(1), x(ysz(1)));
% colMin = min(x(1,:), x(ysz(1)));
% colMin = max(x(1,:), x(ysz(1)));

% columns are independent tables

% extrapolation:
%
% two points are known precisely, (x1, y1) and (x2, y2). We want to know
% y3 for point (x3,y3), assuming all 3 points lie on a straight line.
% m = (y1-y2)/(x1-x2) and m = (y1-y3)/(x1-x3). Therefore
%
% y3 = y1 - (x1-x3)(y1-y2) / (x1-x2)

[r c] = find(xi < xmin | xi > xmax);

if x(2) > x(1)
  xIncreasing = 1;
else
  xIncreasing = 0;
end

for n = 1:length(r)
  x3 = xi(r(n), c(n));

  % if (xIncreasing & x3 > xmax) | (~xIncreasing & x3 < xmin)
  if x3 > x(1)
    % use RH end
    x1 = x(ysz(1)-1);
    y1 = y(ysz(1)-1, :);
    x2 = x(ysz(1));
    y2 = y(ysz(1), :);
  else
    % use LH end
    x1 = x(1);
    y1 = y(1, :);
    x2 = x(2);
    y2 = y(2, :);
  end

  y3 = y1 - ((x1-x3).*(y1-y2) ./ (x1-x2));
  yi(r(n)*c(n), :) = y3;
end


