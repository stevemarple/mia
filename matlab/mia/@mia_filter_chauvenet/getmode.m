function r = getmode(fil)
%GETMODE Return mode used by a MIA_FILTER_CHAUVENET filter.
%
%   r = GETMODE(fil);
%   r: string (eg 'block', 'sliding')
%   fil: MIA_FILTER_CHAUVENET object
%
%   See also MIA_FILTER_CHAUVENET.

if length(fil) == 1
  r = fil.mode;
else
  r = cell(size(fil));
  for n = 1:prod(size(fil))
    r{n} = fil(n).mode;
  end
end
