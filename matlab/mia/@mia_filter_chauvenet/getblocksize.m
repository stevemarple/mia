function r = getblocksize(fil, mia)
%GETBLOCKSIZE Find the block size used by a MIA_FILTER_CHAUVENET filter.
%
%   r = GETBLOCKSIZE(fil, mia);
%   
%   r: scalar
%   fil: MIA_FILTER_CHAUVENET object
%   mia: object derived from MIA_BASE

r = round(fil.windowsize ./ getresolution(mia));
