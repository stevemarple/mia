function r = getthreshold(fil)
%GETTHRESHOLD Return threshold used by a MIA_FILTER_CHAUVENET filter.
%
%   r = GETTHRESHOLD(fil);
%   r: threshold
%   fil: MIA_FILTER_CHAUVENET object
%
%   See also MIA_FILTER_CHAUVENET.

r = zeros(size(fil));
for n = 1:prod(size(fil))
  tmp = fil(n);
  r(n) = tmp.threshold;
end

