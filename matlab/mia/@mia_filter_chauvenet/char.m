function s = char(fil)
%CHAR Convert MIA_FILTER_CHAUVENET object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_CHAUVENET object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_despike_base_char(fil);
  return;
end

s = sprintf(['%s' ...
	     'mode          : %s\n' ...
	     'threshold     : %g\n' ...
	     'window size   : %s'], ...
	    mia_despike_base_char(fil), ...
	    char(fil.mode), fil.threshold, char(fil.windowsize));

