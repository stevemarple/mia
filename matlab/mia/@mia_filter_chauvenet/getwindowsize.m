function r = getwindowsize(fil)
%GETRESOLUTION  Return window size used by a MIA_FILTER_CHAUVENET filter
%
%   r = GETRESOLUTION(fil);
%   r: TIMESPAN object
%   fil: MIA_FILTER_CHAUVENET object
%
%   See also MIA_FILTER_CHAUVENET.

if length(fil) == 1
  r = fil.windowsize;
else
  r = timespan(zeros(size(fil)), 's');
  for n = 1:prod(size(fil))
    r(n) = fil(n).windowsize;
  end
end
