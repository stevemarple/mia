function fil = mia_filter_chauvenet(varargin)
%MIA_FILTER_CHAUVENET  Constructor for MIA_FILTER_CHAUVENET class.
%
%   r = MIA_FILTER_CHAUVENET;
%   definition constructor
%
%   r = MIA_FILTER_CHAUVENET(...)
%   parameter name/value pair interface
%   The only parameter names accepted are 'threshold' and 'windowsize'.
%
% MIA_FILTER_CHAUVENET removes outliers from a sliding window (of 'windowsize')


[tmp cls] = fileparts(mfilename);
parent = 'mia_despike_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;
fil.mode = '';
fil.threshold = [];
fil.windowsize = timespan(10, 'm');


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

if isempty(fil.mode)
  % set default
  fil.mode = 'block';
end

if isempty(fil.threshold)
  % set threshold to default
  fil.threshold = 4;
elseif numel(fil.threshold) ~= 1
  error('threshold must be scalar');
elseif isnan(fil.threshold) 
  error('threshold cannot be NaN');
elseif isinf(fil.threshold) 
  error('threshold must be finite');
end
  
% check windowsize exists and is scalar
if length(fil.windowsize) ~= 1
  error('window size not scalar');
elseif ~isa(fil.windowsize, 'timespan')
  error('windowsize must be a timespan object');
elseif sign(fil.windowsize) < 0
  % timespan must be > 0!
  error('windowsize must have a positive duration');
end
  
% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;

