function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_CHAUVENET object
%
%   See also MIA_FILTER_CHAUVENET.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'Chauvenet criterion';
  
 case 'u'
  r = 'CHAUVENET CRITERION';
  
 case 'c'
  r = 'Chauvenet criterion';
 
 case 'C'
  r = 'Chauvenet Criterion';
 
 otherwise
  error('unknown mode');
end

return

