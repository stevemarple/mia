function r = findspikes(fil, mia)
%FINDSPIKES  Return the location of spikes using Chauvenet criterion
%
%   r = FINDSPIKES(fil, mia)
%   r: vector indicating the location of spikes
%   fil: MIA_FILTER_CHAUVENET object
%   mia: object derived from MIA_BASE
%
%  This algorithm is an implementation of the Chauvenet criterion, with a
%  sliding window. Instead of using probabilities the threshold for
%  eliminating samples (ie classifying the spikes) is determined by n
%  standard deviations, where n is specified as the threshold.
%
%   See also MIA_FILTER_CHAUVENET.

if ~validdata(fil, mia)
  error(['Incompatible data format for ' getname(fil) ' filter']);
end

orig_sz = getdatasize(mia);
orig_ndims = getndims(mia);

data = getdata(mia);
if orig_ndims == 2
  sz = orig_sz;
  ndims = orig_ndims;

else
  % reshape to pretend it is 2d data.Time variation is last dimension
  sz =[prod(orig_sz(1:(end-1)))];
  ndims = 2;
  data = reshape(data, sz);
end

r = logical(zeros(sz));

bs = getblocksize(fil, mia);

  


% now can pretend data is always 2D. Loop over each row.
for rn = 1:sz(1)
  % process each row until there are no more spikes (outliers) found


  switch fil.mode
   case {'block' 'sliding'}

    % in these modes each block is completely modified before moving onto
    % the next block
    
    switch fil.mode
     case 'block'
      tnstep = bs;
     case 'sliding'
      tnstep = 1;
     otherwise
      error(sprintf('unknown mode parameter (was ''%s'')', fil.mode));
    end

    
    % loop over every time value, but use a while loop as the number of
    % samples may be large, and that saves on having a large pre-computed loop
    % matrix
    tn = 1;
    while tn <= sz(2)
      idx = tn:(tn+bs-1);
      if idx(end) > sz(2)
	if strcmp(fil.mode, 'sliding')
	  break;
	else
	  % move block back a bit (overlap with previous block)
	  tn = sz(2) - bs + 1;
	  idx = tn:(tn+bs-1);
	end
      end
      
      new_spikes = 1;
      while new_spikes
	tmpdata = data(rn, idx);
	m = nanmean(tmpdata);
	
	if ~isnan(m)
	  % valid samples exist
	  stddev = nanstd(tmpdata);
	  
	  % spikes are more than threshold standard deviations from the
	  % mean. Keep previous spike identifications (logical OR)
	  tmpspikes = (abs(tmpdata - m) > (fil.threshold .* stddev));
	  r(rn, idx) = tmpspikes | r(rn, idx);
	  
	  % delete any spikes just found from the set of valid data
	  tmpspikes_pos = find(tmpspikes);
	  if ~isempty(tmpspikes_pos)
	    data(rn, (tmpspikes_pos + tn - 1)) = nan;
	  else
	    new_spikes = 0;
	  end
	end
	
      end
      
      tn = tn + tnstep;
    end
    
    
   case 'sliding2'

    new_spikes = 1;
    while new_spikes
      % number of spikes last time round
      last_num_of_spikes = length(find(r(rn,:))); 
      
      % loop over every time value, but use a while loop as the number of
      % samples may be large, and that saves on having a large pre-computed
      % loop matrix
      tn = 1;
      while (tn + bs - 1) <= sz(2)
	idx = tn:(tn+bs-1);
	
	tmpdata = data(rn, idx);
	m = nanmean(tmpdata);
	
	if ~isnan(m)
	  % valid samples exist
	  stddev = nanstd(tmpdata);
	  
	  % spikes are more than threshold standard deviations from the
	  % mean. Keep previous spike identifications (logical OR)
	  tmpspikes = (abs(tmpdata - m) > (fil.threshold .* stddev));
	  r(rn, idx) = tmpspikes | r(rn, idx);
	  
	  % delete any spikes just found from the set of valid data
	  tmpspikes_pos = find(tmpspikes);
	  if ~isempty(tmpspikes_pos)
	    data(rn, (tmpspikes_pos + tn - 1)) = nan;
	  end
	end
	
	tn = tn + 1;
      end
      % number of new spikes found
      new_spikes = length(find(r(rn,:))) - last_num_of_spikes
      find(r(rn, :))
    end
  
      
   otherwise
    error(sprintf('unknown mode parameter (was ''%s'')', fil.mode));
  end

  

    
end




if orig_ndims ~= 2
  % reshape spike matrix back to size of original data
  r = reshape(r, orig_sz);
end


% ensure result is logical type
r = logical(r);
