function [r, s] = commify(varargin)
%COMMIFY Join a set of CHARs with commas and "and".
%
%   [r, plural] = commify(...)
%   r: CHAR
%   plural: 's' if multiple input parameters, '' otherwise
%

varargin_char = cellfun2('char', varargin);
switch nargin
 case 0
  r = '';
  s = '';
  
 case 1
  r = varargin_char{1};
  s = '';
  
 case 2
  r = [varargin_char{1} ' and ' varargin_char{2}];
  s = 's';
  
 otherwise
  r = [sprintf('%s, ', varargin_char{1:(end-1)}) 'and ' varargin_char{end}];
  s = 's';
end




