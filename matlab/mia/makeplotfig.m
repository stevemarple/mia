function varargout = makeplotfig(action, varargin)
%MAKEPLOTFIG Make a figure for plotting into, c/w menus, axes etc.
%
%   [fh gh wmh] = MAKEPLOTFIG('init', 'PropertyName',PropertyValue,...)
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   wmh: TEXT handle(s) of the watermarks (if any)
%
%   Make a standard plot window, and add standard features such as code for
%   printing the FIGURE, displaying the header/title, GRID etc. The cursor
%   over the window is set to 'watch', and should be set to an appropriate
%   cursor after the code to draw into the AXES has terminated. The
%   following property name/value pairs are recognised.
%
%   'miniplots' (default [1 1]): 1 x 2 vector of the x,y numbers of plots,
%   e.g. [8 8] for abs. images.
%
%   'spacing': 1 x 4 (or 6) vector ([l r t b]) defining the spacing used
%   around the plot window, measured as the fractional width or height of
%   a plot window. If multiple plot windows are specified a 1 x 6 vector 
%   ([l r t b x y]) may be used to give different spacings for the 
%   horizontal and vertical gaps between plots.
%
%   'title'
%   Title given to the figure
%
%   'footer': ['yes' | 'no']
%   Add a footer to the window
%
%   'pointer': Select the pointer to be attached to the FIGURE. Default
%   is 'watch'. See FIGURE for more details.
%
%   'receivedraganddrop': [0 | 1 | nan]
%   Control whether axes can be the target for drag-and-drop
%   requests. Use 0 to disable, 1 to enable and any other value to prompt
%   the user when drag-and-drop occurs.
%
%   'providedraganddrop': [0 | 1]
%   Control whether the axes in the figure can be providers in
%   drag-and-drop actions. Use 0 to disable, 1 to enable.
%
%   'watermark': CHAR or CELL
%
%   Add a watermark on each axes containing the corresponding
%   string. Watermarks can be disabled entirely if the watermark value is
%   numeric. The default value is '[]', hence by default watermarks are
%   disabled. To add multiple watermarks on each axis/axes use a CELL
%   array, with size corresponding to the number of plotaxes. In each
%   element store a CELL array of strings.
%
%   'watermarkcolor', COLORSPEC
%   The color to use for the WATERMARK.
%
%   'logo', CHAR
%   The name of the function which should be used to generate the
%   logo. The function is called with the axis handle as the first and
%   only parameter, the function should return the handles of any objects
%   it creates. The default function name is MAKEPLOTFIGLOGO. The default
%   logo can be overridden by placing a function called MAKEPLOTFIGLOGO
%   in the local or MIA/custom directories.
%
%   Any other parameters, which are also valid FIGURE properties, are
%   applied to the FIGURE immediately after it has been created..
%
%   MAKEPLOTFIG('addshading', fh);
%
%   Add an option for changing the shading type. (Applicable to SURF /
%   SURFACE plots only). This function call should later be followed by
%   MAKEPLOTFIG('shading', type, fh), where type is one of 'faceted',
%   'flat', or 'interp'. This will set all figures to have the correct
%   shading and to set the menu checkbuttons.
%
%
%   MAKEPLOTFIG('addcolorbar', fh, options);
%
%   Add a color bar to the footer, in a pre-determined position. The
%   colorbar axes is given a tag value 'colorbar'. Optional parameter
%   name/value pairs may be passed. The following are recognised:
%
%     'xlim', [1x2]
%       Limits for the x axis
%     
%     'clim', [1x2]
%       Limits for the y axis
%    
%     'title', string
%       Title to use for labelling the axes
%
%     'shading', string
%       Shading to use on the color map, see SHADING.
%
%     'position', [left bottom width height] or 'stack'
%       Use the given numeric position instead of the
%       'DefaultFigurePosition' root property. If the position value is
%       'stack' then the figure is placed slightly left and down from the
%       last (visible) figure created (or at the default position if no
%       visible figures exist).
%
%
%   MAKEPLOTFIG('logo', fh);
%   MAKEPLOTFIG('logo', fh, 'on');
%   MAKEPLOTFIG('logo', fh, 'off');
%
%   Toggle or change displaying of the logo. 
%
%   See also FIGURE, AXES, GRID, graph2d/PLOT.

% Uses American (mis)spelling of colour.

hitTestOff = {};
if matlabversioncmp('>=', '5.2')
  hitTestOff = {'HitTest', 'off'};
end

defaultwatermarkcolor = [1 .6 .6];

switch action
  % --------------------------------------------------------------
 case 'init'
  
  % DEFAULT VALUES
  % different defaults are used depending whether multiple plot windows
  % are created 
  defaults.spacing = []; 
  defaults.miniplots = [1 1];
  defaults.title = 'untitled';
  defaults.footer = 0; % 'no';
  defaults.graphpos = [];
  defaults.name = '';
  defaults.receivedraganddrop = 0;
  defaults.providedraganddrop = 0;
  defaults.linkedzoom = 0;
  
  defaults.integerhandle = 'on';
  defaults.visible = 'on';
  % defaults.color = get(0, 'DefaultFigureColor');
  defaults.color = [1 1 1];
  defaults.units = get(0, 'DefaultFigureUnits');
  defaults.position = [];
  defaults.pointer = 'arrow';
  defaults.toolbar = 'figure'; % > matlab 5.3 only
  defaults.resize = get(0, 'DefaultFigureResize'); 
  defaults.resizefcn = get(0, 'DefaultFigureResizeFcn');
  defaults.windowstyle = 'normal';
  defaults.nextplot = 'add';
  
  defaults.grid = 'off';
  defaults.watermark = []; % no watermark at all if numeric (can be cell)
  defaults.watermarkcolor = []; % (can be cell)

  % allow user to change the logo. A default logo can be set
  defaults.logo = 'makeplotfiglogo';

  
  
  [defaults unvi] = interceptprop(varargin, defaults);
  % defaults.footer = ascii2bool(defaults.footer);
  
  if isempty(defaults.position)
    defaults.position = get(0, 'DefaultFigurePosition'); 
  elseif strcmp(defaults.position, 'stack')
    % stack figures, put figure slightly right and down from last figure
    % tmph = findall(0, 'Type', 'figure', 'Visible','on');
    tmph = findall(0, 'Type', 'figure');
    if isempty(tmph)
      defaults.position = get(0, 'DefaultFigurePosition'); 
    else
      defaults.position = get(tmph(1), 'Position') + [20 -20 0 0];
    end
    clear tmph;
  elseif strcmp(defaults.position, 'fullscreen')
    % do nearly full screen, since most window managers have toolbars
    defaults.position = get(0, 'ScreenSize') + [20 80 -40 -200];
    
  elseif ~(isnumeric(defaults.position) ...
	   & isequal(size(defaults.position), [1 4]))
    error('Illegal value for position vector');
  end
  
  defaults.footer = logical(defaults.footer);
  if size(defaults.miniplots) ~= [1 2]
    error('bad value for miniplots');
  end
  
  if isempty(defaults.receivedraganddrop)
    defaults.receivedraganddrop = 0;
  end
  if prod(size(defaults.receivedraganddrop)) == 1
    defaults.receivedraganddrop = 0;
  end
  if prod(size(defaults.receivedraganddrop)) == 1
    defaults.receivedraganddrop = repmat(defaults.receivedraganddrop, ...
					 defaults.miniplots);
  end
  if isempty(defaults.providedraganddrop)
    defaults.providedraganddrop = 1;
  end
  if prod(size(defaults.providedraganddrop)) == 1
    defaults.providedraganddrop = repmat(defaults.providedraganddrop, ...
					 defaults.miniplots);
  end
  
  if isempty(defaults.watermarkcolor)
    defaults.watermarkcolor = defaultwatermarkcolor;
  end
  
  if matlabversioncmp('>=', '5.3')
    figProps = {'MenuBar', 'figure', ...
	       'ToolBar', defaults.toolbar};
  else
    figProps = {'MenuBar', 'none'};
  end
  figProps = {figProps{:}, ...
	      'InvertHardCopy', 'off'};
  
  fh = figure('Name', defaults.name, ...
	      'NumberTitle', 'off', ...
	      'MenuBar', 'figure', ...
	      'IntegerHandle', defaults.integerhandle, ...
	      'Visible', defaults.visible, ...
	      'Color', defaults.color, ...
	      'Units', defaults.units, ...
	      'Pointer', defaults.pointer, ...
	      'Position', defaults.position, ...
	      'Resize', defaults.resize, ...
	      'ResizeFcn', defaults.resizefcn, ...
	      'PaperUnits', 'centimeters', ...
	      'WindowStyle', defaults.windowstyle, ...
	      'DefaultAxesBox', 'on', ...
	      figProps{:});

  % are any of the unknown name/value pairs valid figure properties?
%   figureProperties = fieldnames(get(fh));
%   unknownProps = {};
%   for n = 1:2:numel(unvi)
%     if any(strcmpi(varargin{unvi(n)}, figureProperties))
%       set(fh, varargin{unvi([n n+1])});
%     else
%       unknownProps{end+1} = varargin{unvi(n)};
%     end
%   end
%   warnignoredparameters(unknownProps{:});
  
  unknownProps = {};
  for n = 1:2:numel(unvi)
    found = false;
    eval(sprintf('get(fh, ''%s'');found = true;', varargin{unvi(n)}), ...
	 'found = false');
    if found
      set(fh, varargin{unvi([n n+1])});
    else
      unknownProps{end+1} = varargin{unvi(n)};
    end
  end
  warnignoredparameters(unknownProps{:});
  
  
  
  if defaults.providedraganddrop
    draganddropcb('init', fh);
  end
  
  % work in cm (paper units set above)
  % set(fh, 'PaperUnits', 'cm');
  % fhProp = get(fh);
  paperOrientation = get(fh, 'PaperOrientation');
  paperPosition = get(fh, 'PaperPosition');
  paperSize = get(fh, 'PaperSize');
  printSize = [18 24];
  if strcmp(paperOrientation, 'portrait')
    paperPosition(3:4) = printSize;
  else
    paperPosition([4 3]) = printSize;
  end
  paperPosition(1:2) = ...
      0.5 .* (paperSize - paperPosition(3:4));
  set(fh, 'PaperPosition', paperPosition);
  
  % area reserved for the graphs (including their borders)
  if isempty(defaults.graphpos)
    graphPos = [0 0.0 1 0.85];
    if defaults.footer
      graphPos = [0 0.1 1 0.75];
      footerPos = [0 0 1 0.1];
    end
  else
    graphPos = defaults.graphpos;
    footerPos = [0 0 1 graphPos(2)];
  end
  
  headerPos = [0 graphPos(2)+graphPos(4) 1 1-(graphPos(2)+graphPos(4))];

  % logoPos = [0 headerPos(2) 0.1 headerPos(4)];
  logoPos = [0 headerPos(2) 0.2 headerPos(4)];
  logoPos(1) = headerPos * [1; 0; 1; 0] - logoPos(3);
  % make width and height smaller to allow for a margin
  logoPos = logoPos .* [1 1 0.95 0.95];

  % offset header position to allow for logo axes
  % headerPos(3) = headerPos(3) - logoPos(3);

  % The UserData is reserved for the scaleFactor applied to the plot
  % window(s) when the header is made invisible
  headerHandle = axes('Parent', fh, ...
		      'Units', 'normal', ...
		      'Position', headerPos, ...
		      'Color', 'none', ...
		      'XTickLabel', {}, ...
		      'YTickLabel', {}, ...
		      'Visible' ,'off', ...
		      'HandleVisibility', 'off', ...
		      'Tag', 'header');


  axesColor = get(headerHandle, 'Color');

  % add logo

  % logoImageHandle = addlogo(fh, logoPos);
  logoAxesHandle = axes('Parent', fh, ...
			'Position', logoPos,...
			'Visible', 'off', ...
			'HandleVisibility', 'off', ...
			'Tag', 'logo');
  if ~isempty(defaults.logo)
    logoHandles = feval(defaults.logo, logoAxesHandle);
  else
    logoHandles = [];
  end

  % add a title
  titleHandle = get(headerHandle, 'Title');

  titlePos = [(headerPos(1)+headerPos(3))/2 (headerPos(2)+headerPos(4))/2];
  set(titleHandle, 'Position', [0.5 0.5 0], ...
		   'String', defaults.title, ...
		   'HorizontalAlignment', 'center', ...
		   'VerticalAlignment', 'middle', ...
		   'HandleVisibility', 'on', ...
		   'FontSize', 12, ...
		   'FontWeight', 'normal', ...
		   'Visible', 'on', ...
		   'Tag', 'title');

  % --------------------------------------------------------------
  % Do the main graph window(s)
  % --------------------------------------------------------------
  if defaults.miniplots == [1 1]
    % add margins around the graph, allow for different left/right,
    % top/bottom margins.
    defaultSpacing = [0.15 0.15 0.05 0.05];
    if isempty(defaults.spacing)
      % fractional height/width of plot at bottom,left and top, right
      defaults.spacing = defaultSpacing;
    end
    if length(defaults.spacing) == 2
      defaults.spacing = [defaults.spacing defaults.spacing];
    end
    defaults.spacing(isnan(defaults.spacing)) = ...
	defaultSpacing(isnan(defaults.spacing));
    
    % width =  graphPos(3) / (1 + defaults.spacing(1) + defaults.spacing(3));
    % height = graphPos(4) / (1 + defaults.spacing(2) + defaults.spacing(4));
    width =  graphPos(3) * (1 - defaults.spacing(1) - defaults.spacing(3));
    height = graphPos(4) * (1 - defaults.spacing(2) - defaults.spacing(4));
    defaults.spacing = defaults.spacing .* [width height width height];
    graphPos = [graphPos(1:2) + defaults.spacing(1:2) width height];

    % create one big plot window
    graphHandle = axes('Parent', fh, ...
		       'Units', 'normal', ...
		       'Position', graphPos, ...
		       'NextPlot', defaults.nextplot, ...
		       'Tag', 'plot');

    ud = get(graphHandle, 'UserData');
    ud.receivedraganddrop = defaults.receivedraganddrop(1);
    ud.providedraganddrop = defaults.providedraganddrop(1);
    set(graphHandle, 'UserData', ud);
  else
    % create many plot windows
    graphHandle = zeros(defaults.miniplots);
    % 1 2 3 4 5 6 7 8
    % 9 10 ...
    
    % defaultSpacing = [0.1 0.1];
    % spacing now determined by [left bottom right top horizGap vertGap]
    defaultSpacing = repmat(0.1, [1 6]);
    if isempty(defaults.spacing)
      defaults.spacing = defaultSpacing; % fractional height/width of plot
    elseif length(defaults.spacing) == 2
      % spacing was a [1x2] matrix which set both margins and gaps
      % between axes, keep backwards compatibility
      defaults.spacing = defaults.spacing([1 2 1 2 1 2]);
    elseif length(defaults.spacing) == 4
      defaults.spacing([5 6]) = defaults.spacing([1 2]);
    end
    defaults.spacing(isnan(defaults.spacing)) = ...
	defaultSpacing(isnan(defaults.spacing));
    
    % total w/h given by margins + gaps + plotaxes
    totalWidth  = (sum(defaults.spacing([1 3])) + ...
		   (defaults.miniplots(1) - 1) * defaults.spacing(5) ...
		   + defaults.miniplots(1));
    totalHeight = (sum(defaults.spacing([2 4])) +  ...
		   (defaults.miniplots(2) - 1) * defaults.spacing(6) ...
		   + defaults.miniplots(2));
    % normal units from terms of miniplot window size to 'normal' units
    width = graphPos(3) / totalWidth;
    height = graphPos(4) / totalHeight;
    defaults.spacing = defaults.spacing .* ...
	[width height width height width height];
    miniplotpos = [0 0 width height];
    miniPlotOffset = [graphPos(1:2)+ defaults.spacing(1:2) 0 0];

    
    for r = 1:defaults.miniplots(2) % row - so plot one drawn first :)
      y = 1+defaults.miniplots(2)-r;
      for x = 1:defaults.miniplots(1)
	graphHandle(x,r) = ...
	    axes('Parent', fh, ...
		 'Units', 'normal', 'Position', ...
		 [ (miniplotpos(3) + defaults.spacing(5)) * (x-1), ...
		   (miniplotpos(4) + defaults.spacing(6)) * (y-1) , ...
		   miniplotpos(3)  miniplotpos(4)] + miniPlotOffset, ...
		 'NextPlot', defaults.nextplot, ...
		 'Tag', 'plot');
	ud = get(graphHandle(x,r), 'UserData');
	ud.receivedraganddrop = defaults.receivedraganddrop(x,r);
	ud.providedraganddrop = defaults.providedraganddrop(x,r);
	set(graphHandle(x,r), 'UserData', ud);
	% 'XColor', axesColor, ...
	% 'YColor', axesColor, ...
	% 'ZColor', axesColor, ...
	
	% This might be useful for debugging ...
	% 'Tag', 'plot', ...
	% 'UserData', [x r]);
      end
    end
  end

  set(graphHandle, ...
      'XGrid', defaults.grid, ...
      'YGrid', defaults.grid, ...
      'ZGrid', defaults.grid);

  graphProp = get(graphHandle);
  
  % create an axes as big as all plotaxes
  % take a shortcut and assume first and last plotaxes are diagonally
  % oppposite, this means we only need calculate the bounding box
  % based on those two
  axes('Parent', fh, ...
       'Position', boundingbox(reshape(graphHandle([1 ...
		    prod(defaults.miniplots)]), 2, 1)), ...
       'Visible', 'off', ...
       hitTestOff{:}, ...
       'HandleVisibility', 'off', ...
       'Tag', 'plotbb');
  % axes(graphHandle(1));
  % set(fh, 'CurrentAxes', graphHandle(1)); % moved to just before return

  % --------------------------------------------------------------
  if defaults.footer
    % recalculate position now that spacing in terms of figure width
    % (not plot width) is known
    if length(defaults.spacing) == 2
      footerPos = footerPos + [defaults.spacing(1), 0, -2*defaults.spacing(1), 0];
    else
      footerPos = footerPos + [defaults.spacing(1), 0, ...
		    - defaults.spacing(1) - defaults.spacing(3), 0];
    end
    footerAxesHandle = axes('Parent', fh, ...
			    'Position', footerPos, ...
			    'Color', 'w', ...
			    'XTickLabel', {}, ...
			    'YTickLabel', {}, ...
			    'Visible', 'off', ...
			    'HandleVisibility', 'callback', ...
			    hitTestOff{:}, ...
			    'Tag', 'footer');
  end
  
  
  % --------------------------------------------------------------
  % Add standard menus

  % -------
  miamenu(fh);
  % make print menu visible
  set(findobj(fh, 'Tag', 'file_print'), 'Visible', defaults.visible);

  % make export menu visible
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'file_export'), ...
      'Label', '&Export...', ...
      'Visible', 'on', ...
      'Callback', 'filemenufcn(gcbf,''FileExport'');');
  
  
  % make options menu visible
  optionsMenuHandle = findobj(fh, 'Tag', 'options');
  set(optionsMenuHandle, 'Visible', 'on');
  
  uimenu(optionsMenuHandle, 'Label', 'Show title', ...
	 'Tag', 'showtitle', ...
	 'Callback', [mfilename '(''title'');'], ...
	 'Checked', get(titleHandle, 'Visible'));

  if defaults.footer
    uimenu(optionsMenuHandle, 'Label', 'Show footer', ...
	   'Tag', 'showfooter', ...
	   'Callback', [mfilename '(''footer'');']);
    % 'Checked', get(titleHandle, 'Visible'));
  end
  
  if ~isempty(defaults.logo) & length(logoHandles) ...
	& strcmp(get(logoHandles(1), 'Visible'), 'on')
    logoChecked = 'on';
  else
    logoChecked = 'off';
  end
    %else
    %   logoChecked = 'off';
    % end
  
  uimenu(optionsMenuHandle, 'Label', 'Show logo', ...
	 'Tag', 'showlogo', ...
	 'Callback', [mfilename '(''logo'');'], ...
	 'Checked', logoChecked, ...
	 'Enable', ...
	 logical2onoff(~isempty(defaults.logo) & length(logoHandles)));
  %    'Checked', get(logoImageHandle, 'Visible'));
  % 'Checked', get(logoHandles(1), 'Visible'), ...

  % logical2onoff(logoChecked), ...
  
  uimenu(optionsMenuHandle, 'Label', 'Show grid', ...
	 'Tag', 'showgrid', ...
	 'Callback', [mfilename '(''grid'');'], ...
	 'Checked', get(graphHandle(1), 'XGrid'));

  uimenu(optionsMenuHandle, 'Label', 'Image labels', ...
	 'Visible', 'off', ...
	 'Tag', 'imagelabels', ...
	 'Checked', 'off');

  
  zoomMenuHandle = uimenu(optionsMenuHandle, 'Label', 'Zoom', ...
			  'Tag', 'options_zoom');

  if logical(defaults.linkedzoom)
    zoomAction = 'tazoom';
  else
    zoomAction = 'zoom';
  end
  za = {'On', 'Off', 'Out'};
  for zan = 1:length(za)
    uimenu(zoomMenuHandle, 'Label', za{zan}, ...
	   'Tag', ['zoom_' lower(za{zan})], ...
	   'Callback', sprintf('%s(''%s'',''%s'');', ...
			       mfilename, zoomAction, za{zan}));
  end

 
  % -------
  % Context-sensitive menu
  if matlabversioncmp('>=', '5.2')
    cmh = uicontextmenu('Parent', fh, ...
			'Callback', [mfilename '(''options'');']);

    uimenu(cmh, 'Label', 'Show title', ...
	   'Tag', 'cmshowtitle', ...
	   'Callback', [mfilename '(''title'');'], ...
	   'Checked', get(titleHandle, 'Visible'));

    uimenu(cmh, 'Label', 'Show logo', ...
	   'Tag', 'cmshowlogo', ...
	   'Callback', [mfilename '(''logo'');'], ...
	    'Checked', logoChecked);
    % 'Checked', get(logoHandles(1), 'Visible'));
    % 'Checked', get(logoImageHandle, 'Visible'));

    % if length(graphHandle) == 1
    uimenu(cmh, 'Label', 'Show grid', ...
	   'Tag', 'cmshowgrid', ...
	   'Callback', [mfilename '(''grid'');'], ...
	   'Checked', get(graphHandle(1), 'XGrid'));
    % end

    uimenu(cmh, 'Label', 'Show box', ...
	   'Tag', 'cmshowbox', ...
	   'Callback', [mfilename '(''box'');'], ...
	   'Checked', get(graphHandle(1), 'XGrid'));

    zoomCmHandle = uimenu(cmh, 'Label', 'Zoom', ...
			  'Tag', 'options_zoom');

%     uimenu(zoomCmHandle, 'Label', 'On', ...
% 	   'Tag', 'zoom_on', ...
% 	   'Callback', [mfilename '(''zoom'',''on'',gca);']);

%     uimenu(zoomCmHandle, 'Label', 'Off', ...
% 	   'Tag', 'zoom_off', ...
% 	   'Callback', [mfilename '(''zoom'',''off'',gca);']);

%     uimenu(zoomCmHandle, 'Label', 'Out', ...
% 	   'Tag', 'zoom_out', ...
% 	   'Callback', [mfilename '(''zoom'',''out'',gca);']);


    for zan = 1:length(za)
      uimenu(zoomCmHandle, 'Label', za{zan}, ...
	     'Tag', ['zoom_' lower(za{zan})], ...
	     'Callback', sprintf('%s(''%s'',''%s'');', ...
				 mfilename, zoomAction, za{zan}));
    end

    % associate with all axes
    set(graphHandle, 'UIContextMenu', cmh);

  end
  
  
  % add standard toolbar
  miatoolbar('init', ...
	     'parent', fh);

  
  % if the MathWorks toolbar functions are present, and linkedzoom is
  % on then ensure they activate tazoom, not MathWorks own crappy zoom
  if logical(defaults.linkedzoom)
    set(findall(fh, 'Type','uitoggletool','Tag', 'figToolZoomIn'), ...
	'ClickedCallback', '', ...
	'OnCallback', [mfilename '(''tazoom'',''on'');'], ...
	'OffCallback', [mfilename '(''tazoom'',''off'');']);
    set(findall(fh, 'Type','uitoggletool','Tag', 'figToolZoomOut'), ...
	'Visible', 'off');
  end
  
  
  % add watermarks
  wmh = feval(mfilename, 'addwatermark', graphHandle, ...
	      defaults.watermark, defaults.watermarkcolor);
  
  set(fh, 'CurrentAxes', graphHandle(1));
  varargout{1} = fh;
  varargout{2} = graphHandle;
  varargout{3} = wmh;
  
  % -- end of 'init' --
  
  % --------------------------------------------------------------
 case 'addshading'
  % create the menu items for adding shading, etc.
  fh = varargin{1};
  
  % default values
  defaults.grid = 'off';
  % since the user will have to spell it I guess being consistently
  % wrong about spelling is best.
  defaults.gridcolor = 'b'; % can be any COLORSPEC
  defaults.shading = 'interp'; % flat or interp
  
  % find out if calling function overrode any of our settings
  if nargin > 2
    defaults = interceptprop({varargin{2:nargin-1}}, defaults);
  end
  
  % find the options top-level menu
  omh = findobj(fh, 'Type', 'uimenu', 'Tag', 'options');
  smh = uimenu(omh, 'Label', 'Shading', ...
	       'Separator', 'on', ...
	       'Tag', 'shading');
  % create submenus for each type (currently 3 in matlab5)
  if strcmp(lower(defaults.shading), 'flat')
    flatShading = 'on';
    interpShading = 'off';
  else
    flatShading = 'off';
    interpShading = 'on';
  end
  uimenu(smh, 'Label', 'Flat', ...
	 'Callback', [mfilename '(''pixelshading'',''flat'', gcbf);'], ...
	 'Checked', flatShading, ...
	 'Tag', 'flatshading');
  uimenu(smh, 'Label', 'Interpolated', ...
	 'Callback', [mfilename '(''pixelshading'',''interp'', gcbf);'], ...
	 'Checked', interpShading, ...
	 'Tag', 'interpshading');
  
  uimenu(smh, 'Label', 'Grid', ...
	 'Callback', [mfilename '(''pixelgrid'', ''toggle'', gcbf);'], ...
	 'Checked', defaults.grid, ...	
	 'Separator', 'on', ...
	 'Tag', 'pixelgrid');
  
  % store the last chosen color in its UserData area
  uimenu(smh, 'Label', 'Grid color...', ...
	 'Callback', [mfilename '(''pixelgridcolor'', [], gcbf);'], ...
	 'UserData', [0 0 0], ...
	 'Tag', 'pixelgridcolor');

  return;
  
  % --------------------------------------------------------------
 case 'options'
  % options top-level menu object selected
  
  % find out if the first plot window has XTick set, if so assume all
  % windows do, and that a grid will be shown. Otherwise disable the
  % 'Show grid' option
  fh = gcbf;
  mh = gcbo;
  if strcmp(get(mh, 'Type'), 'uimenu')
    % current object is a normal menu
    showgridh = findobj(mh, 'Type', 'uimenu', 'Tag', 'showgrid');
    % do for all axes based on first
    gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  else
    % must be uicontextmenu, do only for current axes
    showgridh = findobj(mh, 'Type', 'uimenu', 'Tag', 'cmshowgrid');
    % do only for parent
    gh = gca;
  end
  if isempty(get(gh(1), 'XTick')) & isempty(get(gh(1), 'YTick'))
    set(showgridh, 'Enable', 'off')
  else
    set(showgridh, 'Enable', 'on')    
  end
  
  % --------------------------------------------------------------
 case 'logo'
  % mfilename('logo');
  % mfilename('logo',fh);
  % mfilename('logo',fh,'on');
  % mfilename('logo',fh,'off');
  
  if nargin == 1
    fh = gcbf;
    % mh = gcbo;
  else
    fh = varargin{1};
    % mh = findobj(fh, 'Type', 'uimenu', 'Tag', 'showlogo');
  end
  mh = [findobj(fh, 'Type', 'uimenu', 'Tag', 'showlogo') ...
	findobj(fh, 'Type', 'uimenu', 'Tag', 'cmshowlogo')];
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  hiddenHandlesState = get(0, 'ShowHiddenHandles');
  set(0,'ShowHiddenHandles', 'on');
  ah = findobj(fh, 'Type', 'axes', 'Tag', 'logo');
  set(0,'ShowHiddenHandles', hiddenHandlesState);
  ih = get(ah, 'Children');

  if nargin < 3
    % toggle logo visible/invisible
    oppStatus = invertonoff(get(mh, 'Checked'));
  else
    oppStatus = varargin{2};
  end
  
  set(ih, 'Visible', oppStatus);
  set(mh, 'Checked', oppStatus);
  % make sure the draw events are completed before setting the pointer
  % back
  drawnow;
  set(fh, 'Pointer', pointer);     
  
  % --------------------------------------------------------------
 case 'title'
  % toggle title
  % h = gcbo;
  if nargin == 1
    fh = gcbf;
  else
    fh = varargin{1};
  end
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  hiddenHandlesState = get(0, 'ShowHiddenHandles');
  set(0,'ShowHiddenHandles', 'on');
  tah = findobj(fh, 'Type', 'axes', 'Tag', 'header'); % title axes
  tth = findobj(tah, 'Type', 'text', 'Tag', 'title');
  lah = findobj(fh, 'Type', 'axes', 'Tag', 'logo');
  set(0,'ShowHiddenHandles', hiddenHandlesState);
  lih = get(lah, 'Children');
  stmh = findobj(fh, 'Type', 'uimenu', 'Tag', 'showtitle');
  % stmh = gcbo;
  slmh = findobj(fh, 'Type', 'uimenu', 'Tag', 'showlogo');
  
  % if a single plot window exists change its size
  pah = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  tahpos = get(tah, 'Position');
  pahpos = get(pah, 'Position');
  
  if isempty(pah)
    disp(sprintf('\rCannot find plot axes to hide/show title'));
    return;
  elseif length(pah) == 1
    bbpos =  pahpos;  % no need for a bounding box
  else
    bbpos = boundingbox(pahpos);
  end
  
  if strcmp(get(tth, 'Visible'), 'on')
    % enlarge plot(s)
    scaleFactor = 1 + tahpos(4)/bbpos(4);
    set(tah, 'UserData', scaleFactor);
    oppStatus = 'off';
    
    set(lih, 'Visible', 'off');      
  else
    oppStatus = 'on';
    scaleFactor = 1 / get(tah, 'UserData');
    set(lih, 'Visible', get(slmh, 'Checked'));  % look at saved state :)
  end
  
  if length(pah) == 1
    % single plot window
    pahpos = bbpos .* [1 1 1 scaleFactor];
  else
    for n = 1:prod(size(pahpos))
      % scale Y axis, then translate upwards
      pahpos{n} = pahpos{n} .* [1 0 1 scaleFactor] + ...
	  [0 ((pahpos{n}(2)-bbpos(2)) * scaleFactor) + bbpos(2) ...
	   0 0]; 
    end
  end
  
  
  set(tth, 'Visible', oppStatus);
  % set(lah, 'Visible', oppStatus);

  set(stmh, 'Checked', oppStatus);
  set(slmh, 'Enable', oppStatus);
  if iscell(pahpos)
    for n = 1:prod(size(pah))
      set(pah(n), 'Position', pahpos{n});
    end
  else
    set(pah(:), 'Position', pahpos(:));
  end
  
  % plotbbH = findobj(fh, 'Type', 'axes', 'Tag', 'plotbb')
  plotbbH = findall(fh, 'Type', 'axes', 'Tag', 'plotbb');
  plotbbPos = get(plotbbH, 'Position');
  plotbbPos = plotbbPos .* [1 0 1 scaleFactor] + ...
      [0 ((plotbbPos(2)-bbpos(2)) * scaleFactor) + bbpos(2) 0 0]; 
  set(plotbbH, 'Position', plotbbPos);
  
  % Update any legends which are displayed.
  % legend('ResizeLegend');
  
  % make sure the draw events are completed before setting the pointer
  % back
  drawnow;
  set(fh, 'Pointer', pointer); 
  
  % --------------------------------------------------------------
 case 'footer'
  return;
  
  
  % --------------------------------------------------------------
 case 'grid'
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  % gh = gca;

  % status = get(gh(1), 'XGrid');
  % if strcmp(status, 'on')
  %       oppStatus = 'off';
  %     else
  %       oppStatus = 'on';
  %     end
  oppStatus = invertonoff( get(gh(1), 'XGrid'));
  set(gh, 'XGrid', oppStatus, 'YGrid', oppStatus);
  drawnow;
  set(fh, 'Pointer', pointer); 

  % --------------------------------------------------------------
  % case 'shading'
  %     shadingType = varargin{1};
  %     fh = varargin{2};
  %     smh = findobj('Type', 'uimenu', 'Tag', 'shading');
  %     % children of smh should be all the shading type uimenus
  %     smhKids = get(smh, 'Children');
  %     % set all off, turn the chosen type back on
  %     set(smhKids, 'Checked', 'off');
  %     set(findobj(smhKids, 'flat', 'Tag', [shadingType 'shading']), ...
  % 	 'Checked', 'on');
  %     % set the shading to the type we were told
  %     figure(fh);
  %     shadingType
  %     myshading(shadingType);

  % --------------------------------------------------------------
 case 'pixelshading'
  shadingType = lower(varargin{1});
  fh = varargin{2};
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  
  flatH = findobj('Type', 'uimenu', 'Tag', 'flatshading');
  interpH = findobj('Type', 'uimenu', 'Tag', 'interpshading');
  
  switch shadingType
   case 'flat'
    if strcmp(get(flatH, 'Checked'), 'on')
      set(fh, 'Pointer', pointer);    
      return; % already set
    end
    set(flatH, 'Checked', 'on');
    set(interpH, 'Checked', 'off');
    
   case 'interp'
    if strcmp(get(interpH, 'Checked'), 'on')
      set(fh, 'Pointer', pointer);    
      return; % already set
    end
    set(flatH, 'Checked', 'off');
    set(interpH, 'Checked', 'on');
    
   otherwise
    warning(['shading type ' shadingType ' not valid (ignored)']);
    return;
  end
  % valid types (don't allow a COLORSPEC - makes no sense here find all
  % surfaces in the figure. Might want to restrict it to children of
  % plot or colorbar axes, but won't now for speed. Tags to look for on
  % the axes will be 'plot' and 'colorbar'.
  % sh = findobj(fh, 'Type', 'surface');
  sh = findplotsurfaces(fh);
  sh = [sh(:)' findobj(fh, 'Type', 'surface', 'Tag', 'colorbar')];
  set(sh, 'FaceColor', shadingType);
  % make sure the draw events are completed before setting the pointer
  % back
  drawnow;
  set(fh, 'Pointer', pointer);    
  
  % --------------------------------------------------------------
 case 'pixelgrid'
  newStatus = lower(varargin{1});
  fh = varargin{2};

  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  mh = gcbo;   % assume menu called this function
  if isempty(mh)
    % assumed wrong - not called by a callback
    mh = findobj('Type', 'uimenu', 'Tag', 'pixelgrid');
  end
  
  % to speed up search only look at sibling menus
  % (grid color menu handle)
  gcmh = findobj(get(mh, 'Parent'), 'Tag', 'pixelgridcolor');
  % sh = findobj(fh, 'Type', 'surface');
  sh = findplotsurfaces(fh);
  if strcmp(newStatus, 'toggle')
    newStatus = invertonoff(get(mh, 'Checked'));
  end
  switch newStatus
   case 'on'
    col = get(gcmh, 'UserData');
    if size(col) ~= [1 3]
      col = [1 1 1]; % black
    end
    set(sh, 'EdgeColor', col);
    
   case 'off'
    set(sh, 'EdgeColor', 'none');
    
   otherwise
    warning('grid state unknown (ignored)');
    return;
  end
  set(mh, 'Checked', newStatus);
  set(gcmh, 'Enable', newStatus);
  % make sure the draw events are completed before setting the pointer
  % back
  drawnow;
  set(fh, 'Pointer', pointer);     
  
  % --------------------------------------------------------------
 case 'pixelgridcolor'
  % have to hope that using uisetcolor won't break things when its not
  % modal. In Matlab 5.1 no way to make it modal. :( (Even changing
  % DefaultWindowMode in root object doesn't work
  newcolor = varargin{1};
  fh = varargin{2};
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch'); 
  gcmh = gcbo;
  if isempty(gcmh)
    % not in a callback
    gcmh = findobj('Type', 'uimenu', 'Tag', 'pixelgridcolor');
  end
  
  if isempty(newcolor) | size(newcolor) ~= [1 3]
    % no color or bad color, ask user to supply color, try to
    % initialise uisetcolor with old color 
    newcolor = get(gcmh, 'UserData');
    if isempty(newcolor) | size(newcolor) ~= [1 3]
      % give up - use black
      newcolor = [0 0 0];
    end
    % ask user to set color
    newcolor = uisetcolor(newcolor, 'Select grid color');
  end
  if newcolor == 0
    % something screwed up
    newcolor = [0 0 0] % black
  end
  % sh = findobj(fh, 'Type', 'surface');
  sh = findplotsurfaces(fh);
  set(gcmh, 'UserData', newcolor);
  set(sh, 'EdgeColor', newcolor);
  % make sure the draw events are completed before setting the pointer
  % back
  drawnow;
  set(fh, 'Pointer', pointer);     

  % --------------------------------------------------------------
 case 'box'
  % makefig('box');
  h = gca;
  set(h, 'Box', invertonoff(get(h, 'Box')));
  
  
  % --------------------------------------------------------------
 case 'zoom'
  % makeplotfig('zoom','zoom_action');
  % makeplotfig('zoom','zoom_action', axes);
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  if nargin == 2
    gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
    ca = gca;
  else
    gh = varargin{2};
    ca = gh;
  end
  
  for ax = gh'
    set(fh, 'CurrentAxes', ax);
    zoom(fh, varargin{1});
  end
  set(fh, 'CurrentAxes', ca); % put current axes back
  drawnow;
  set(fh, 'Pointer', pointer);

  % --------------------------------------------------------------
 case 'tazoom'
  % makeplotfig('tazoom','zoom_action');
  % makeplotfig('tazoom','zoom_action', axes);
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  if nargin == 2
    gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
    ca = get(fh, 'CurrentAxes');
  else
    gh = varargin{2};
    ca = gh;
  end

  tazoom(gh, varargin{1});
  
  % if the MathWorks toolbar is present then signal zoom state on
  % that. BUT, set the callbacks to empty first, otherwise they get
  % called when the state is changed! Only do this if the callback object
  % is NOT a uitoggletool.
  if ~strcmp(get(gcbo, 'Type'), 'uitoggletool')
    zb = findall(fh, 'Type', 'uitoggletool', 'Tag', 'figToolZoomIn');
    zbp = get(zb);
    for n = 1:length(zb)
      set(zb(n), 'ClickedCallback', '', 'OnCallback', '', 'OffCallback', '');
      switch lower(varargin{1})
       case {'on' 'xon'}
	set(zb(n), 'State', 'on');
       case {'off' 'restore'}
	set(zb(n), 'State', 'off');
      end
      set(zb(n), ...
	  'ClickedCallback', zbp(n).ClickedCallback, ...
	  'OnCallback', zbp(n).OnCallback', ...
	  'OffCallback', zbp(n).OffCallback);
    end
  end
  
  set(fh, 'CurrentAxes', ca); % put current axes back
  drawnow;
  set(fh, 'Pointer', pointer);

  % --------------------------------------------------------------
 case 'addcolorbar'
  % makeplotfig('addcolorbar', fh, varargin)
  cf = get(0, 'CurrentFigure');
  ca = get([], 'CurrentAxes');

  fh = varargin{1};
  footerH = findall(fh, 'Tag', 'footer', 'Type', 'axes');
  if isempty(footerH)
    warning('Could not find footer to add color bar to, aborting');
    return;
  end

  existingCBars = findall(fh, 'Type', 'axes', 'Tag', 'colorbar');
  % some defaults for this action
  defaults.xlim = [];
  defaults.zlim = [];
  defaults.clim = [];
  defaults.title = []; % not set
  defaults.shading = 'flat';
  defaults.axes = [];
  if nargin > 2
    [defaults unvi] = interceptprop(varargin(2:nargin-1), defaults);
  end
  
  if isempty(defaults.axes)
    switch length(existingCBars)
     case 1
      % put 2nd on far right
      position = get(footerH, 'Position') * ...
	  [ 1 0   0    0; ...
	    0 1   0    0; ...
	    0.75 0   0.25 0; ...
	    0 0.75 0    0.25];
      
     case 2
      % put 3rd in middle
      position = get(footerH, 'Position') * ...
	  [ 1 0   0    0; ...
	    0 1   0    0; ...
	    0.375 0   0.25 0; ...
	    0 0.75 0    0.25];

      % put 2nd in the middle, 3rd goes on the right
      position = get(footerH, 'Position') * ...
	  [ 1 0   0    0; ...
	    0 1   0    0; ...
	    0.375 0   0.25 0; ...
	    0 0.75 0    0.25];
      oldPos = cell2mat(get(existingCBars, 'Position'));
      [tmp right] = max(oldPos(:,1));
      set(existingCBars(right), 'Position', position);
      position = oldPos(right, :);

     otherwise
      % none, or too many for us to compute a sensible position
      position = get(footerH, 'Position') * ...
	  [ 1 0   0    0; ...
	    0 1   0    0; ...
	    0 0   0.25 0; ...
	    0 0.75 0    0.25];
    end

    defaults.axes = axes('Parent', fh, ...
			 'Position', position, ...
			 'HandleVisibility', 'callback', ...
			 hitTestOff{:});
  end
  set(defaults.axes, ...
      'Box', 'on', ...
      'Layer', 'top', ...
      'TickDirMode', 'manual', ...
      'TickDir', 'out', ...
      'Visible','off', ...
      'YTick', [], ...
      'YTickLabel', {}, ...
      'Tag', 'colorbar');

  if ~isempty(defaults.xlim)
    set(defaults.axes, 'XLimMode', 'manual', ...
		      'XLim', defaults.xlim);
  else
    defaults.xlim = get(defaults.axes, 'XLim');
  end
  
  if ~isempty(defaults.clim)
    set(defaults.axes, 'CLimMode', 'manual', ...
		      'CLim', defaults.clim);
  end

  if ~isempty(defaults.zlim)
    set(defaults.axes, 'ZLimMode', 'manual', ...
		      'ZLim', defaults.clim);
  end

  xData = [defaults.xlim(1):((defaults.xlim(2)-defaults.xlim(1))/40):...
	   defaults.xlim(2)];
  yData = [1 2];
  zData = [xData;xData];
  surfH = surface('Parent', defaults.axes, ...
		  'XData', xData, ...
		  'YData', yData, ...
		  'ZData', zData, ...
		  'CData', zData, ...
		  'Tag', 'colorbar');
  
  shading(defaults.axes, defaults.shading);
  view(2);
  if ~isempty(defaults.title)
    set(get(defaults.axes, 'XLabel'), 'String', defaults.title);
  end

  % ensure Tag is set after surf function, and axes is visible
  set(defaults.axes, 'Tag', 'colorbar', ...
		    'Visible', 'on'); 
  varargout{1} = defaults.axes;
  % set current axes/figure back
  set(0, 'CurrentFigure', cf);
  set(cf, 'CurrentAxes', ca);

  
  % --------------------------------------------------------------
 case 'addreverseimages'
  % mfilename('addreverseimages', fh);
  fh = varargin{1};
  % find the options top-level menu
  omh = findobj(fh, 'Type', 'uimenu', 'Tag', 'options');
  rih = uimenu(omh, 'Label', 'Flip axes', ...
	       'Tag', 'flipaxes', ...
	       'Callback', [mfilename '(''reverseimagesmenu'');']);
  % create submenus for each possible flip
  uimenu(rih, 'Label', 'Plan view', ...
	 'Callback', [mfilename '(''reverseimages'',''plan'');'], ...
	 'Tag', 'reverseimages_plan');
  uimenu(rih, 'Label', 'N-S reversed', ...
	 'Callback', [mfilename '(''reverseimages'',''ns'');'], ...
	 'Tag', 'reverseimages_ns');
  uimenu(rih, 'Label', 'E-W reversed', ...
	 'Callback', [mfilename '(''reverseimages'',''ew'');'], ...
	 'Tag', 'reverseimages_ew');
  
  % --------------------------------------------------------------
 case 'reverseimagesmenu'
  % mfilename('addreverseimages');
  fh = gcbf;
  cbo = gcbo;
  % find all plot axes
  gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  % get the direction of the first one, assume other are the same(!)
  xdir = get(gh(1), 'XDir');
  ydir = get(gh(1), 'YDir');
  planh = findobj(cbo, 'Tag', 'reverseimages_plan');
  nsh = findobj(cbo, 'Tag', 'reverseimages_ns');
  ewh = findobj(cbo, 'Tag', 'reverseimages_ew');    
  plan = 1; % assume plan view
  if strcmp(xdir, 'normal')
    set(ewh, 'Checked', 'off');
  else
    set(ewh, 'Checked', 'on');
    plan = 0; % cannot be plan view
  end
  if strcmp(ydir, 'normal')
    set(nsh, 'Checked', 'off');
  else
    set(nsh, 'Checked', 'on');
    plan = 0; % cannot be plan view
  end

  if plan
    set(planh, 'Checked', 'on');
  else
    set(planh, 'Checked', 'off');
  end
  
  % --------------------------------------------------------------
 case 'reverseimages'
  % mfilename('reverseimages', fh)
  fh = gcbf;
  flipaxes = varargin{1};
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  
  tih = findall(fh, 'Type', 'text', 'Tag', 'title');
  ti = get(tih, 'String');

  onames = feval(mfilename, 'flipaxesnames');
  
  % find all plot axes
  gh = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  dir = {'normal', 'reverse'};
  if strcmp(get(gh(1), 'XDir'), 'normal');
    xDirReversed = 0;
  else
    xDirReversed = 1;
  end
  if strcmp(get(gh(1), 'YDir'), 'normal');
    yDirReversed = 0;
  else
    yDirReversed = 1;
  end    
  switch flipaxes
   case 'plan'
    % set (absolutely) to normal
    xDirReversed = 0;
    yDirReversed = 0;
    set(gh, 'XDir', dir{xDirReversed + 1}, ...
	    'YDir', dir{yDirReversed + 1});
   case 'ns'
    % toggle YDir
    yDirReversed = ~yDirReversed;
    set(gh, 'YDir', dir{yDirReversed + 1});
   case 'ew'
    % toggle XDir
    xDirReversed = ~xDirReversed;
    set(gh, 'XDir', dir{xDirReversed + 1});
   otherwise
    error(['unknown flip (was ' flipaxes ')']);
  end
  % update title
  onamesNum = 1 + (yDirReversed + 2*xDirReversed);
  for n = length(onames):-1:1
    ti = strrep(ti, onames{n}, onames{onamesNum});
  end
  set(tih, 'String', ti);
  drawnow;
  set(fh, 'Pointer', pointer);

  % --------------------------------------------------------------
 case 'flipaxesnames'
  varargout{1} = {'plan', 'N-S reversed', 'E-W reversed', ...
		  'N-S & E-W reversed'};
  return;
  
  % --------------------------------------------------------------
 case 'gettitle'
  if numel(varargin) ~= 1
    error('figure must be specified and be scalar')
  elseif ~ishandle(varargin{1})
    error('not a valid handle')
  elseif ~strcmp(get(varargin{1}, 'Type'), 'figure')
    error('not a figure handle');
  end
  varargout{1} = findall(varargin{1}, 'Type', 'text', 'Tag', 'title');

  % --------------------------------------------------------------
 case 'addwatermark'
  % mfilename('addwatermark', gh, watermark);
  % mfilename('addwatermark', gh, watermark, color);
  switch numel(varargin)
   case 2
    gh = varargin{1};
    watermark = varargin{2};
    watermarkcolor = defaultwatermarkcolor;
    
   case 3
    gh = varargin{1};
    watermark = varargin{2};
    watermarkcolor = varargin{3};
    
   otherwise
    error('incorrect parameters');
  end
  
  if isnumeric(watermark)
    wmh = []; % don't do
  else
    wmh = zeros(size(gh));
    
    for n = 1:prod(numel(gh))
      if iscell(watermarkcolor)
	wmc = watermarkcolor{n};
      else
	wmc = watermarkcolor;
      end
      if ischar(watermark)
	wm = watermark;
      elseif iscell(watermark)
	wm = watermark{n};
      end
      wmh(n) = text('Parent', gh(n), ...
		    'Clipping', 'off', ...
		    'Units', 'normalized', ...
		    'Position', [0.5 0.5 -1], ...
		    'FontUnits', 'points', ...
		    'FontSize', 30, ...
		    'Color', wmc, ...
		    'Interpreter', 'none', ...
		    'HorizontalAlignment', 'center', ...
		    'VerticalAlignment', 'middle', ...
		    'String', wm, ...
		    'Tag', 'watermark');
    end
  end
  
  varargout{1} = wmh;
  
  % don't allow it to be selected as current object
  if matlabversioncmp('>', '5.1')
    set(wmh, 'HitTest', 'off'); % 5.1 does not have HitTest property
  end

  % --------------------------------------------------------------
 case 'legend'
  % Run an action on all legends, given figure or axes handle(s)
  h = varargin{1}; % figure and/or axes handle(s)
  lh = [];
  for n = 1:numel(h)
    switch get(h(n), 'Type')
     case 'figure'
      lh = vertcat(lh, findall(h(n), 'Type', 'axes', 'Tag', 'legend'));
     case 'axes'
      lh = vertcat(lh, legend(h(n))); % this tends to turn boxon, Matlab bug?
     otherwise
      warning(sprintf('no action for handle type %s', ...
		      get(h(n), 'Type')));
    end
  end
  varargout{1} = lh;
  if nargin > 2
    for n = 1:numel(lh)
      legend(lh(n), varargin{2:end});
    end
  end
  
  % --------------------------------------------------------------
 case {'xlim' 'ylim'}
  % mfilename('xlim', h, xlim);
  % mfilename('ylim', h, ylim);
  % h is figure or axes handle
  gh = findall(varargin{1}, ...
	       'Type', 'axes', ...
	       'Tag', 'plot');
  % set(gh, action, gh, varargin{2});
  % call xlim or ylim, as this is overloaded for timestamp and timespan
  for n = 1:numel(gh)
    feval(action, gh(n), varargin{2});
  end
  if nargout
    varargout{1} = gh;
  end
  
  % --------------------------------------------------------------
  % mfilename('timetick2', h, ...);
  % h is figure or axes handle
 case 'timetick2'
  gh = findall(varargin{1}, ...
	       'Type', 'axes', ...
	       'Tag', 'plot');
  varargout{1} = timetick2(gh, varargin{2:end});
  
  % --------------------------------------------------------------
 otherwise
  error(['unknown action (was ''' action ''')']);

end


% --------------------------------------------------------------
function bool = ascii2bool(str)
% convert a string from ascii to a Boolean value. 'on', and 'true'
% evaluate to 1, everything else evalutes to zero. Only second character
% is tested (for speed).

if length(str) < 2
  error('Invalid property value');
end

bool = 0;
switch str(2)
  % on, ON, true, TRUE
 case {'n', 'N', 'r', 'R'}
  bool = 1;

end
return;

% --------------------------------------------------------------
function r = logical2onoff(a)
if logical(a)
  r = 'on';
else
  r = 'off';
end
  
% --------------------------------------------------------------
function s = invertonoff(str)
if strcmp(lower(str), 'on')
  s = 'off';
else
  s = 'on';
end
return;

% --------------------------------------------------------------
function h = findplotsurfaces(fh)
% Find handles only to surfaces who are children of plot axes
%
% h: array of handles to surface objects
% fh: figure handle (or root handle if to look in all figures)

ah = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
h = [findobj(ah, 'Type', 'surface');
     findobj(ah, 'Type', 'patch')];

