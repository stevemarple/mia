function r = split(p, s)
%SPLIT  Split a string into substrings (like Perl's split function).
%
%   r = SPLIT(d, s)
%   r: CELL array of CHARs
%   d: delimiting string (CHAR)
%   s: string to be split (CHAR)
%
% SPLIT separates a string into substrings using a given delimiting string.
%
% See also JOIN, COMMIFY.

r = {};
idx = [];
if length(p) >= length(s)
  return
end

idx = findstr(p, s);

if ~isempty(idx)
  idx = [1-numel(p) idx length(s)+1];
  r = cell(1, numel(idx) - 1); 
  for n = 1:length(r)
    r{n} = s((idx(n) + numel(p)):(idx(n+1) - 1));
  end
end
