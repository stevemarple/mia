function [xIono, yIono, xIonoC, yIonoC] = ellipticalbeamprojection(varargin)
%ELLIPTICALBEAMPROJECTION Caclulate the projection of a beam on the ionosphere.
%
%   [x, y, xc, yc] = CIRCULARBEAMPROJECTION(...)
%   x, y: coordinates of beam outline(s) mapped to specified height.
%   xc, yc: coordinates of the beam centre(s), mapped to specified height.
%
% The following parameter name/value pairs are required:
%
% 'height', DOUBLE
% The projection height above ground, in metres
%
% 'azimuth', DOUBLE
% 'zenith', DOUBLE
% Azimuth and zenith angle(s) of the beam(s). Azimuth is measured in
% 'compass' degrees, ie clockwise from North.
%
% 'beamwidth', DOUBLE
% 'beamwidthminor', DOUBLE
% 'beamwidthmajor', DOUBLE
% The beam width of the beams, in degrees. For the case of circular beams
% only beamwidth need be specified; for elliptical beams both
% beamwidthminor and beamwidthmajor are required.
%
% 'ellipseazimuth', DOBULE
% The azimuth angle of the ellipse major axis, in 'compass' degrees, ie
% clockwise from North.
%
% 'units', CHAR
% Units of the output coordinates. Valid values are 'm' (metres), 'km'
% (kilometres) and 'deg' (geographic degrees. 'deg' requires location to
% be set.
%
% Optional parameters are:
%
% 'location', LOCATION
% The ground location from which the beams are projected.
%
% 'earthradius', DOUBLE
% The radius of the Earth used in the calculation. Default value is the
% value returned by EARTHRADIUS.
%
% See also EARTHRADIUS, LOCATION, POLARANGLEOFRAY.

xIono = [];
yIono = [];
pi_2 = pi/2; 

d2r = pi / 180;
r2d = 180 / pi;

defaults.points = 80;  % number of points used on cone of beam
defaults.height = [];
defaults.azimuth = [];
defaults.zenith = [];
defaults.beamwidth = [];
defaults.beamwidthminor = [];
defaults.beamwidthmajor = [];
defaults.ellipseazimuth = [];

defaults.units = ''; % units of x and y
defaults.location = [];
defaults.earthradius = earthradius; % radius of the Earth (approx 6378000m)

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.units)
  error('units must be specified');
end

if isempty(defaults.height)
  error('height not specified');
elseif numel(defaults.height) ~= 1
  error('height must be a scalar');
end

if isempty(defaults.azimuth)
  error('azimuth not specified');
elseif size(defaults.azimuth, 2) ~= 1
  error('azimuth must be a column matrix');
else
  numOfBeams = numel(defaults.azimuth);
end
azSz = size(defaults.azimuth);

if isempty(defaults.zenith)
  error('zenith not specified');
elseif size(defaults.zenith, 2) ~= 1
  error('zenith must be a column matrix');
elseif numel(defaults.zenith) ~= numOfBeams
  error('number of zenith angles must match number of azimuth angles');
end

if isempty(defaults.ellipseazimuth)
  error('ellipseazimuth not specified');
elseif size(defaults.ellipseazimuth, 2) ~= 1
  error('ellipseazimuth must be a column matrix');
elseif numel(defaults.ellipseazimuth) ~= numOfBeams
  error('number of ellipseazimuth angles must match number of azimuth angles');
end

if isempty(defaults.beamwidth)
  if isempty(defaults.beamwidthmajor) | isempty(defaults.beamwidthminor)
    error('beamwidth not fully specified');
  elseif ~isequal(size(defaults.beamwidthmajor), azSz)
    error('beamwidthmajor must be same size as azimuth');
  elseif ~isequal(size(defaults.beamwidthminor), azSz)
    error('beamwidthminor must be same size as azimuth');
  end
  
elseif size(defaults.beamwidth, 2) ~= 1
  error('beamwidth must be a column matrix');
elseif numel(defaults.beamwidth) ~= numOfBeams
  error('number of beamwidth angles must match number of azimuth angles');
else
  defaults.beamwidthmajor = defaults.beamwidth;
  defaults.beamwidthminor = defaults.beamwidth;
end

if isempty(defaults.location)
  if strcmp(defaults.units', 'deg')
    error('location must be specified for units=deg');
  end
elseif ~isa(defaults.location, 'location')
  error('location must be a location object');
end

if ~any(strcmp(defaults.units, {'m', 'km', 'deg'}))
  error(sprintf('unknown units (was ''%s'')', defaults.units));
end

% convert azimuths from compass degrees to graph degrees
defaults.azimuth = rem(-defaults.azimuth + 90, 360);

%  convert to radians
zenithR = defaults.zenith .* d2r;

% convert from compass degrees to graph degrees
azimuthR = -rem(-defaults.azimuth + 90, 360) .* d2r;
bwMajorR = defaults.beamwidthmajor .* d2r;
bwMinorR = defaults.beamwidthminor .* d2r;




% Assume the beam is vertical, the beam outline is elliptical and its
% major axis is aligned N-S
% alpha = ((2 * pi * (i-1)) / 0defaults.points);
alpha = (0:defaults.points) .* (2*pi / defaults.points);

% work at unit height
x = (bwMinorR ./ 2) * cos(alpha);
y = (bwMajorR ./ 2) * sin(alpha);
z = ones(size(x));


% for useful information about rotation vectors
% http://prt.fernuni-hagen.de/lehre/KURSE/PRT001/course_main/node9.html

% xIono = zeros(defaults.points+1, numOfBeams);
% yIono = zeros(defaults.points+1, numOfBeams);

X = zeros(defaults.points+1, numOfBeams);
Y = X;
Z = X;

Xc = zeros(1, numOfBeams);
Yc = Xc;
Zc = Xc;

for m = 1:numOfBeams
  
  % precompute some rotation vectors
  a = -defaults.ellipseazimuth(m) .* d2r;
  rot_vector_ellipse = ...
      [cos(a) -sin(a) 0;
       sin(a)  cos(a) 0;
       0       0      1];
  
  a = -zenithR(m);
  rot_vector_zenith = ...
      [1 0      0;
       0 cos(a) -sin(a);
       0 sin(a) cos(a)];

  a = azimuthR(m);
  rot_vector_azimuth = ...
      [cos(a) -sin(a) 0;
       sin(a)  cos(a) 0;
       0       0      1];

  for n = 1:numel(x)
    % rotate, x and y to include effect of major axis not being N-S
    % alpha = alpha + (defaults.ellipseazimuth .* d2r);
    p = [x(n); y(n); z(n)];
    p = rot_vector_ellipse * p;
    
    % now tilt coordinate system northwards to account for non-zero zenith
    % angle
    p = rot_vector_zenith * p;

    % now rotate coordinate system to account for non-zero azimuth
    p = rot_vector_azimuth * p;

    x(n) = p(1);
    y(n) = p(2);
    z(n) = p(3);
    
    
  end
  
  % do the same for the centre
  pc = [0; 0; z(1)];
  pc = rot_vector_ellipse * pc;
  pc = rot_vector_zenith * pc;
  pc = rot_vector_azimuth * pc;
  
  X(:, m) = x';
  Y(:, m) = y';
  Z(:, m) = z';
  
  Xc(m) = pc(1);
  Yc(m) = pc(2);
  Zc(m) = pc(3);
end


[az elev] = cart2sph(X, Y, Z);

dist = defaults.earthradius .* ...
       polarangleofray(pi/2 - elev, ...
		       repmat(defaults.height', defaults.points+1, 1));

xIono = dist .* cos(az);
yIono = dist .* sin(az);

[azc elevc] = cart2sph(Xc, Yc, Zc);

distc = defaults.earthradius .* ...
	polarangleofray(pi/2 - elevc, ...
			repmat(defaults.height', defaults.points+1, 1));

xIonoC = distc .* cos(azc);
yIonoC = distc .* sin(azc);


switch defaults.units
 case 'm'
  % already in m
  ;
  
 case 'km'
  xIono = xIono * 1e-3;
  yIono = yIono * 1e-3;
  xIonoC = xIonoC * 1e-3;
  yIonoC = yIonoC * 1e-3;
  
 case 'deg'
  % check whether xy2longlat wants distances at height h=0 or h=height
  [xIono yIono] = xy2longlat(xIono, yIono, ...
			     defaults.height, defaults.location);
  
  [xIonoC yIonoC] = xy2longlat(xIonoC, yIonoC, ...
			       defaults.height, defaults.location);
  
 otherwise
  error(['unknown unit (was ' defaults.units ')']);
end

