function fil = mia_filter_gaussian_average(varargin)
%MIA_FILTER_GAUSSIAN_AVERAGE  Constructor for MIA_FILTER_GAUSSIAN_AVERAGE.
%
%   r = MIA_FILTER_GAUSSIAN_AVERAGE;
%   default constructor
%
%   r = MIA_FILTER_GAUSSIAN_AVERAGE('resolution', res)
%   parameter name/value pair interface
%
%   MIA_FILTER_GAUSSIAN_AVERAGE reduces the resolution of data by applying a
%   mean weighted by the gaussian function. 
%
%  See also MIA_FILTER_SLIDING_AVERAGE.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.resolution = timespan(5,'s');


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% check resolution is set correctly
if ~isa(fil.resolution, 'timespan')
  error('resolution must be a timespan');
elseif fil.resolution <= timespan(0, 's')
  % timespan must be > 0!
  error('resolution must have a positive duration');
end

% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
