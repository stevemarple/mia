function s = char(fil)
%CHAR Convert MIA_FILTER_GAUSSIAN_AVERAGE object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_GAUSSIAN_AVERAGE object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

s = sprintf('%sresolution    : %s\n', ...
	    mia_filter_base_char(fil), char(fil.resolution));

