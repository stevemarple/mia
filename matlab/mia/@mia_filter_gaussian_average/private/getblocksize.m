function r = getblocksize(fil, mia)
%GETBLOCKSIZE Find the block size used by a MIA_FILTER_GAUSSIAN_AVERAGE filter.
%
%   r = GETBLOCKSIZE(fil, mia);
%   
%   r: scalar
%   fil: MIA_FILTER_GAUSSIAN_AVERAGE object
%   mia: object derived from MIA_BASE

res = getresolution(mia);
if length(res) > 1
  % handle mia_image_data classes
  if all(res(1) == res)
    res = res(1);
  else
    error('resolutions must all be the same');
  end
  
  % other criteria
  if isa(mia, 'mia_image_base')
    dt = diff(getimagetime(mia))
    if ~all(dt(1) == dt)
      error('images must be at equal intervals');
    end
  end
  
elseif length(res) == 0
  error('resolution not set');
end


r = fil.resolution / res;

if r <= 1
  r = 1; % do not filter
elseif r ~= fix(r)
  r = nan; % error
else
  r = 2 * r - 1;
end
