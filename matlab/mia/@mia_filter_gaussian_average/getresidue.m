function r = getresidue(fil, varargin)
%GETRESIDUE Get the number elements which will not be processed.
%
%   r = GETRESIDUE(fil, mia);
%
%   r: [1x2] vector, containing the maximum number of elements which may
%   not be filtered (for left and right end respectively)
%   fil: FILSLIDINGAV object
%   mia: object derived from MIA_BASE
%
%   In certain circumstances the residual data elements which cannot
%   normally be filter may be able to be filtered (e.g. with a RIO_QDC_BASE
%   object where the data 'wraps around'). If the MIA_BASE oject is passed
%   as an additional parameter then these circumstances can be identified
%   and a more accurate result returned.
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

% worst case
if nargin == 1
  % cannot calculate without data
  r = [nan nan];
  return;
else
  blockSize = getblocksize(fil, varargin{1});
end
r = [(blockSize - 1)/2, (blockSize - 1)/2];
