function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_GAUSSIAN_AVERAGE object
%
%   See also MIA_FILTER_GAUSSIAN_AVERAGE.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'gaussian average';
  
 case 'u'
  r = 'GAUSSIAN AVERAGE';
  
 case 'c'
  r = 'Gaussian average';
 
 case 'C'
  r = 'Gaussian Average';
 
 otherwise
  error('unknown mode');
end

return

