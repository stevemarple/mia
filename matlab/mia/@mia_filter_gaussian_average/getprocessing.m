function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_GAUSSIAN_AVERAGE object
%
%   See also MIA_FILTER_GAUSSIAN_AVERAGE.

% calculate real window size used
r = sprintf('%s filter applied with resolution %s', ...
	    getname(fil), char(fil.resolution));

