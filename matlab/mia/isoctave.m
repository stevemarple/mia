function r = isoctave
%ISOCTAVE  Test if running under octave interpreter.
%
% r = ISOCTAVE
% r: LOGICAL
%
% ISOCTAVE returns TRUE if running under OCTAVE, false otherwise. The
% results is based upon the existence of the built-in OCTAVE_VERSION
% function.

if exist('OCTAVE_VERSION')
  r = true;
else
  r = false;
end
