function r = filter(fil, mia)
%FILTER  Replace data with a fixed value.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_FIXED object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_FIXED.

r = setdata(mia, repmat(fil.value, getdatasize(mia)));
r = addprocessing(r, getprocessing(fil, mia));

