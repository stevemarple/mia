function r = char(fil)
%CHAR  Convert MIA_FILTER_FIXED object to CHAR.
%
%   r = CHAR(fil)
%   r: CHAR representation
%   fil: MIA_FILTER_FIXED object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

if length(fil.value) == 1
  val = sprintf('%g', fil.value);
else
  val = matrixinfo(val);
end

r = sprintf('%svalue         : %s\n', mia_filter_base_char(fil), val);





