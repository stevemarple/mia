function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_FIXED object
%
%   See also MIA_FILTER_FIXED.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case 'l'
  r = 'fixed value';
  
 case 'u'
  r = 'FIXED VALUE';
  
 case 'c' 
  r = 'Fixed value';
 
 case 'C'
  r = 'Fixed Value';
  
 otherwise
  error('unknown mode');
end

return
