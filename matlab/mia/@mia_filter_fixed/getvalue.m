function r = getvalue(fil)
%GETVALUE Return the replacement value for the MIA_FILTER_FIXED filter.
%
%   r = GETVALUE(fil);
%   r: replacement value
%   fil: MIA_FILTER_FIXED object
%
%   See also MIA_FILTER_FIXED.

r = fil.value;

