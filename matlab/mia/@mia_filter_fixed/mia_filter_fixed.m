function fil = mia_filter_fixed(varargin)
%MIA_FILTER_FIXED  Constructor for MIA_FILTER_FIXED class.
%
%   r = MIA_FILTER_FIXED;
%   default constructor
%
%   r = MIA_FILTER_FIXED(v)
%   v: replacement value
%
%   r = MIA_FILTER_FIXED(...)
%   parameter name/value pair interface
%   The only parameter name accepted is 'value'.
%
%   MIA_FILTER_FIXED replaces data with a fixed value. The default
%   replacement value is NAN. Although apparently not very useful
%   MIA_FILTER_FIXED is used as a sub-filter by other filter classes. It
%   is most useful when combined with INSERT and EXTRACT functions of
%   MIA_BASE objects.
%

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.value = nan; % replacement value


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif nargin == 1 & isnumeric(varargin{1})
  fil.value = varargin{1};
  p = feval(parent);
  fil = class(fil, cls, p);

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% check value exists and is scalar
if length(fil.value) ~= 1
  error('replacement value not scalar');
end
  
% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
