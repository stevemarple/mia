function [r, val, mul, mul_str, val_str] = printunits(d, units, varargin)
%PRINTUNITS  Print a value with its units, expressed as an SI multiplier.
%
%   r = PRINTUNITS(d, units, ...)
%   r: CHAR representation (or CELL array of CHARs)
%   d: value (DOUBLE)
%   units: CHAR
%
%   Print a value with the units, e.g. 10000m as 10km. If d is not a
%   scalar the return value is a CELL array of the same size as d. If
%   units is 'degrees' then the degree sign is used in the output string,
%   which is then of type I18N_CHAR.
%
%   The default behaviour may be modified with the following name/value
%   pairs:
%
%     'formatstr', CHAR
%     Use an alternative printf format string. Ensure format string only
%     attempts to print one numerical value.
%
%     'multiplier', DOUBLE
%     The LOG10 value of the multiplier to use (e.g., 3 for k(ilo))
%
%     'separator', CHAR
%     The separator character between the value and the units. Default is a
%     space (except when the units is a degree sign in which case the
%     default is no separator.
%
%   See also SPRINTF.

% deg_sign = i18n_entity('deg');
deg_sign = ' deg';

% defaults.formatstr = '%2.2f';
defaults.formatstr = '%g';
defaults.multiplier = []; % automatically select a multiplier
defaults.separator =  ' ';
defaults.degdir = '';
[defaults unvi] = interceptprop(varargin, defaults);

if strcmp(units, deg_sign)
  units = 'degrees'; % internally use 'degrees', but print using deg sign
end
  

if strcmp(units, 'degrees')
  unitsStr = deg_sign; % what to print
  % if not setting these then use sensible defaults for degrees
  if ~any(strcmp(varargin, 'separator')) & isempty(defaults.degdir)
    defaults.separator = ''; % no separator for degrees
  end
  if ~any(strcmp(varargin, 'multiplier'))
    defaults.multiplier = 0;
  end
else
  unitsStr = units;
end



r = cell(size(d));
val = zeros(size(d));
mul = zeros(size(d));
mul_str = cell(size(d));
val_str = cell(size(d));
for n = 1:prod(size(d))
  if isempty(d(n))
    r{n} = '';
  else
    if isempty(defaults.multiplier)
      if isfinite(d(n)) & d(n) ~= 0
	% multiplier (allow for floating point error)
	mul(n) = floor(eps+log10(d(n)) / 3) * 3; 
      else
	mul(n) = 0;
      end
    else    
      mul(n) = defaults.multiplier;
    end
    mul_str{n} = '';
    switch mul(n)
     case -18
      mul_str{n} = 'a'; % atto
      
     case -15
      mul_str{n} = 'f'; % femto
     
     case -12
      mul_str{n} = 'p'; % pico
     
     case -9
      mul_str{n} = 'n'; % nano
      
     case -6
      mul_str{n} = 'u'; % micro
      
     case -3
      mul_str{n} = 'm'; % milli
      
     case 0
      ;
      
     case 3
      mul_str{n} = 'k'; % kilo
      
     case 6
      mul_str{n} = 'M'; % mega
      
     case 9
      mul_str{n} = 'G'; % giga
      
     case 12
      mul_str{n} = 'T'; % tera
      
     case 15
      mul_str{n} = 'P'; % peta
      
     case 18
      mul_str{n} = 'E'; % exa
      
    end
    if isempty(mul_str{n})
      val(n) = d(n);
    else
      val(n) = d(n) / power(10, mul(n));
    end
    

    if strcmp(units,'degrees') & ~isempty(defaults.degdir)
      switch lower(defaults.degdir)
       case 'n/s'
	direction = {'N' 'S'};
       case 'e/w'
	direction = {'E' 'W'};
       otherwise
	error('unknown direction');
      end
      
      units2 = [unitsStr direction{1 + (d(n) < 0)}]; % append N/S etc
    else
      units2 = unitsStr;
    end

    val_str{n} = sprintf(defaults.formatstr, val(n));
    % r{n} = sprintf([defaults.formatstr '%s%s%s'], ...
    % 	   val(n), defaults.separator, mul_str{n}, units2);
    r{n} = [val_str{n} defaults.separator, mul_str{n}, units2];

  end
end

switch length(r)
 case 0
  r = '';
  
 case 1
  r = r{1};
  mul_str = mul_str{1};
end



