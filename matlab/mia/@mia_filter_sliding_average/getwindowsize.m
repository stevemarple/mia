function r = getwindowsize(fil)
%GETWINDOWSIZE  Return window size used by a MIA_FILTER_SLIDING_AVERAGE filter
%
%   r = GETWINDOWSIZE(fil);
%   r: TIMESPAN object
%   fil: MIA_FILTER_SLIDING_AVERAGE object
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

if length(fil) == 1
  r = fil.windowsize;
else
  r = timespan(zeros(size(fil)), 's');
  for n = 1:prod(size(fil))
    r(n) = fil(n).windowsize;
  end
end
