function blockSize = getblocksize(fil, mia)
%GETBLOCKSIZE Find the block size used by a MIA_FILTER_SLIDING_AVERAGE filter.
%
%   r = GETBLOCKSIZE(fil, mia);
%   
%   r: scalar
%   fil: MIA_FILTER_SLIDING_AVERAGE object
%   mia: object derived from MIA_BASE

blockSize = fil.windowsize / getresolution(mia);
% round to nearest odd number
blockSizeR = rem(blockSize, 2);
if  blockSizeR > 1
  blockSize = blockSize + 1 - blockSizeR;
else
  blockSize = blockSize + 1 - blockSizeR;
end
% remove loss of precision errors and ensure it really is an integer
blockSize = round(blockSize);  

if blockSize < 1
  blockSize = 1;
end
