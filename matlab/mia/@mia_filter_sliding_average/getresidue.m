function varargout = getresidue(fil, varargin)
%GETRESIDUE Get the number elements which will not be processed.
%
%   r = GETRESIDUE(fil, mia);
%
%   r: [1x2] vector, containing the maximum number of elements which may
%   not be filtered (for left and right end respectively)
%   fil: FILSLIDINGAV object
%   mia: object derived from MIA_BASE
%
%   In certain circumstances the residual data elements which cannot
%   normally be filter may be able to be filtered (e.g. with a RIO_QDC_BASE
%   object where the data 'wraps around'). If the MIA_BASE oject is passed
%   as an additional parameter then these circumstances can be identified
%   and a more accurate result returned.
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

%   A non-publicised option is to find the residue before wrap-around is
%   applied, this is needed when doing the wrap around
%
%   [r1 r2] = GETRESIDUE(fil, mia);
%
%   Where r1 is the residue after wrapping around, and r2 before

r1 = [];
r2 = [];

% worst case
if nargin == 1
  % cannot calculate without data
  r1 = [nan nan];
  return;
else
  blockSize = getblocksize(fil, varargin{1});
end
  
r1 = [(blockSize - 1)/2, (blockSize - 1)/2];
r2 = r1;

if nargin == 2 & isa(varargin{1}, 'rio_qdc_base')
  % can take special action on certain quiet day curves

  % work to nearest second
  sd = round(siderealday, timespan(1, 's'));
  tmp = sd / getresolution(varargin{1});
  if fix(tmp) == tmp
    % divides exactly into sidereal day, can wrap
    r1 = [0 0];
  end
end

varargout{1} = r1;
if nargout == 2
  varargout{2} = r2;
end

