function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_SLIDING_AVERAGE object
%
%   See also mia_filter_base/ADDPROCESSING

% calculate real window size used
rws = getblocksize(fil, mia) * getresolution(mia);

r = sprintf('Filter %s applied: sliding%s with window size %s', ...
    class(fil), fil.method, char(rws));

