function r = getmethod(fil)
%GETMETHOD Return method used by a MIA_FILTER_SLIDING_AVERAGE filter.
%
%   r = GETMETHOD(fil);
%   r: string (eg 'mean', 'median')
%   fil: MIA_FILTER_SLIDING_AVERAGE object
%
%   See also MIA_FILTER_SLIDING_AVERAGE.

if length(fil) == 1
  r = fil.method;
else
  r = cell(size(fil));
  for n = 1:prod(size(fil))
    r{n} = fil(n).method;
  end
end
