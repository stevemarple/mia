function r = filter(fil, mia)
%FILTER Filter data with a sliding average.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_SLIDING_AVERAGE object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_SLIDING_AVERAGE, MIA_BASE.

r = mia;

N = numel(mia);
if N ~= 1
  for n = 1:N
    [tmp mfname] = fileparts(mfilename);
    r(n) = feval(mfname, fil, mia(n));
  end
  return
end

if getndims(mia) ~= 2
  error('%s currently only tested on 2D data', getname(fil));
end

blockSize = getblocksize(fil, mia);
if blockSize == 1
  disp(['window too small to filter data at ' char(getresolution(mia)) ...
	' resolution']);
  return;
end
  
% residue with and without possibility of wrap around
[residue1 residue2] = getresidue(fil, mia);
data = getdata(mia);

% WRONG! 
% originalsz = size(data) % single image would be 2D!!!!

% ensure single images have size vector [1x3]
originalsz = getdatasize(mia);

% convert to a 2D array (rows will be independent so should be safe)
datasz = [prod(originalsz(1:(end-1))) originalsz(end)];
data = reshape(data, datasz);

if ~isequal(datasz, originalsz)
  % to make inserting the data easier later pretend we had 2D data from the
  % start
  r = setdata(r, data);
end


newdata = feval(['sliding' fil.method], data', blockSize )';


r = setdata(r, newdata, ':', (residue2(1)+1):(datasz(2)-residue2(2)));



if isequal(residue1, [0 0])
  % calculate indices for the data to be processed, taking extra taking
  % to be the new residue
  % data: [lastdata_for_new_residue lastdata | firstdata
  % firstdata_for_residue] 
  newdata = [];
  ind = [(datasz(2)-sum(residue2)+1):datasz(2)  1:sum(residue2)];
  newdata = feval(['sliding' fil.method], data(:,ind)', blockSize)';

  ind = [(datasz(2)-residue2(2)+1):datasz(2) 1:residue2(1)];
  r = setdata(r, newdata, ':', ind);
end

r = addprocessing(r, getprocessing(fil, mia));


if ~isequal(datasz, originalsz)
  % we reshaped the data - better put it back
  r = setdata(r, reshape(getdata(r), originalsz));
end
