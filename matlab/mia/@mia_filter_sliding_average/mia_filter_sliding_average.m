function fil = mia_filter_sliding_average(varargin)
%MIA_FILTER_SLIDING_AVERAGE  Constructor for MIA_FILTER_SLIDING_AVERAGE class.
%
%   r = MIA_FILTER_SLIDING_AVERAGE;
%   default constructor
%
%   r = MIA_FILTER_SLIDING_AVERAGE(v)
%   v: replacement value
%
%   r = MIA_FILTER_SLIDING_AVERAGE(...)
%   parameter name/value pair interface
%   The only parameter names accepted are 'method' and 'windowsize'.
%
% MIA_FILTER_SLIDING_AVERAGE smooths data with a sliding average
% function. The function name is given by 'method'. The window size it
% operates over is given by 'windowsize'. Note that window sizes must be
% defined as a scalar TIMESPAN object. This ensures a similar effect
% regardless of the temporal resolution of the data being filter.
%
% See also MIA_FILTER_SLIDING_AVERAGE.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.method = 'mean';
fil.windowsize = timespan(5,'s');


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% check average method is set
if isempty(fil.method)
  error('method not set');
end
  
zt = timespan(0, 's');
% check windowsize exists and is scalar
if length(fil.windowsize) ~= 1
  error('window size not scalar');
elseif ~isa(fil.windowsize, 'timespan')
  error('windowsize must be a timespan object');
elseif fil.windowsize <= zt
  % timespan must be > 0!
  error('windowsize must have a positive duration');
end
  
% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
