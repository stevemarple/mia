function mia_makelinks(varargin)


cwd = pwd;
externals = fullfile(dirname(mia_basedir), 'links.txt');
fid = url_fopen(externals);

while ~feof(fid)
  s = fgetl(fid);
  if isempty(s) 
    continue;
  elseif s(1) == '#'
    continue;
  end

  f = split(char(9), s);
  if ~strcmp('/', filesep)
    % Map to correct path separators
    f = strrep(f, '/', filesep);
  end
  
  wd = fullfile(mia_basedir, f{1});
  src = f{2};
  dest = f{3};
  
  if ~url_exist(wd)
    mkdir(wd);
  end
  cd(wd);
  
  if ispc
    % Copy files/directories. Symbolic links and junction points need
    % adminstrator access.
    if isdir(dest)
      [success, mesg] = rmdir(dest, 's');
    elseif url_exist(dest)
      delete(dest)
    end
    
    if isdir(src)
      % Need /i to suppress question about whether destination if file or
      % directory      
      [status, mesg] = system(sprintf('xcopy "%s" "%s" /R /E /Y /i', ...
				      src, dest));
    else
      [success, mesg] = copyfile(src, dest, 'f');
      if ~success
	src
	dest
	wd
	error(sprintf('Could not copy "%s" to "%s": %s', ...
		      src, dest, mesg));
      end
    end
  else
    % For unix systems use symbolic links
	if url_exist(dest)
	  delete(dest)
	end
    system(sprintf('ln -s "%s" "%s"', src, dest));
  end
  
end
  
fclose(fid);
cd(cwd);

disp('Links made successfully');

if length(varargin) >= 1 & varargin{1}
  disp('Exit matlab');
  exit
end
