function r = mia_image_instruments
%MIA_IMAGE_INSTRUMENTS  Return a list of supported image instruments.
%
%   r = MIA_IMAGE_INSTRUMENTS
%   r: class names of instruments supported in MIA
%
%   See also MIA_INSTRUMENT_BASE, CAMERA, RIOMETER etc.

r = {'camera', 'energymap'};
