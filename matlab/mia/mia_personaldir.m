function r = mia_personaldir
%MIA_PERSONALDIR  Get the name of the user's personal MIA directory


r = getenv('MIA_PERSONALDIR');
if ~isempty(r)
  return
end

if ispc
  % Windows
  home = getenv('USERPROFILE');
else
  home = getenv('HOME');
end

r = fullfile(home, 'MIA');
