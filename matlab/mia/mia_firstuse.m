function varargout = mia_firstuse(varargin)
%MIA_FIRSTUSE  Initialise MIA on first use
%
%   mesg = MIA_FIRSTUSE
%   mesg CELL array of messages relating to actions taken (empty if none)
%
% MIA_FIRSTUSE initialises MIA for new users. It is called at STARTUP and
% normally will take no action, though if the user has deleted his/her
% personal or custom directories then they and any necessary files will
% be recreated.

if nargin
  remake = logical(varargin{1});
else
  remake = true;
end

mesg = {};
personal_dir = mia_personaldir;
if ~isdir(personal_dir)
  disp(sprintf('creating MIA personal directory ''%s''', personal_dir));
  url_mkdir(personal_dir);
  mesg{end+1} = ...
      sprintf('created MIA personal directory ''%s''', personal_dir);
end
addpath(personal_dir);

custom_dir = fullfile(personal_dir, 'custom');
if ~isdir(custom_dir)
  disp(sprintf('creating user''s custom directory ''%s''', custom_dir));
  url_mkdir(custom_dir);
  mesg{end+1} = ...
      sprintf('created user''s custom directory ''%s''', custom_dir);
end
addpath(custom_dir);

% Try to get userdetails
eval('ud = userdetails;', 'ud = [];');

if remake == true
  ud = []; % force details to be remade
end

udfilename = fullfile(custom_dir, 'userdetails.m');

if isempty(ud)
  % Cannot get user details, so insist that user enters them
  modalmessage('MIA requires your user details.', 'Details needed');
  ud.realname = modalquery('Please enter your name', 'Name?', ...
                        'Your name is required');
  ud.email = modalquery('Please enter your email address', 'Email address?', ...
                     'Your email address is required');
  ud.institute = modalquery('Please enter your institute', 'Institute?', ...
                         'Your institute is required');
  makeuserdetails(ud, udfilename);
  mesg{end+1} = ...
      sprintf('created %s', udfilename);
end

if remake ~= false
  disp(sprintf(['You can reset your user details at anytime by ' ...
		'editting %s or by running "mia_firstuse(true)" on ' ...
		'the Matlab command line'], udfilename));
end

if nargout
  varargout{1} = mesg;
end

% Write user's details to userdetails.m in the user's custom directory
function filename = makeuserdetails(ud, filename)

fid = url_fopen(filename, 'w');

fprintf(fid, 'function r = userdetails\n');
fprintf(fid, '%%USERDETAILS  Details about the current user.\n');
fprintf(fid, '%%\n');
fprintf(fid, '%%   r = USERDETAILS\n');
fprintf(fid, '%%   r: struct containing the following fields\n');
fprintf(fid, '%%      realname: real name of current user\n');
fprintf(fid, '%%      email: email address of current user\n');
fprintf(fid, '%%      institute: institute of current user\n');
fprintf(fid, '\n');
fprintf(fid, 'r.realname = ''%s'';\n', ud.realname);
fprintf(fid, 'r.email = ''%s'';\n', ud.email);
fprintf(fid, 'r.institute = ''%s'';\n', ud.institute);
fclose(fid);

