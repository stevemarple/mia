function r2 = atoi(varargin)
%ATOI  Convert a string to an integer like the C function atoi.
%
%   r = ATOI(s)
%   r = ATOI(s, ...)
%   r: integer value (DOUBLE)
%   s: CHAR array (string)
%
%   
%   Conversion skips any leading whitespace and stops on the first
%   non-numeric character. String may be signed. Not tested on EBCDIC
%   character set. Multiple arguments may be specified, r is a 1xN
%   vector where N is the number of input arguments.
%
%   See also ATOF.

deprecated('Please use fix(str2num)');

r2 = repmat(nan, 1, nargin);
for a = 1:nargin
  str = varargin{a};
  n = 1; % index into string
  r = 0; % return value
  s = 1; % final multiplier to get corect sign
  if length(str) == 0
    r = 0;
    return;
  end

  % skip any leading whitespace
  while n <= length(str) & isspace(str(n))
    n = n + 1;
  end

  if str(n) == '+'
    n = n+1;  % look at next char
  elseif str(n) == '-'
    s = -1;
    n = n+1;  % look at next char
  end


  % do the conversion (base 10)
  while n <= length(str) & str(n) >= '0' & str(n) <= '9'
    r = (r * 10) + str(n) - '0';
    n = n + 1;
  end

  r = s * r;  % include sign information

  r2(a) = r;
end
