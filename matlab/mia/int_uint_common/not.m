function r = not(a)
%NOT  NOT for (U)INT 8, 16, 32 data types.
%
%   See ops/NOT.


r = feval(class(a), not(double(a)));
