function r = ne(a, b)
%NE  NE for (U)INT 8, 16, 32 data types.
%
%   See ops/NE.


r = logical(feval(promote(a, b), ne(double(a), double(b))));
