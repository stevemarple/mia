function r = lt(a, b)
%LT  LT for (U)INT 8, 16, 32 data types.
%
%   See ops/LT.


r = logical(feval(promote(a, b), lt(double(a), double(b))));
