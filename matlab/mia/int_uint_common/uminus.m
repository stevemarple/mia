function r = uminus(a)
%UMINUS  UMINUS for (U)INT 8, 16, 32 data types.
%
%   See ops/UMINUS.


r = feval(class(a), uminus(double(a)));
