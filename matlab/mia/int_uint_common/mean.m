function r = mean(a, varargin)
%MEAN   Average or mean value.
%
%   See datafun/MEAN.

% overload mean to avoid overflow

v = cell(size(varargin));
for n = 1:length(varargin)
  v{n} = double(varargin{n});
end

% r = feval(promote(a, b), mean(double(a), v{:}));
r = mean(double(a), v{:});

