function r = subsindex(a)
%SUBSINDEX  SUBSINDEX for (U)INT 8, 16, 32 data types.
%
%   See ops/SUBSINDEX.

r = double(a) - 1;
