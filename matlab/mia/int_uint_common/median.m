function r = median(a, varargin)
%MEDIAN   Average or median value.
%
%   See datafun/MEDIAN.

% overload median to avoid overflow

v = cell(size(varargin));
for n = 1:length(varargin)
  v{n} = double(varargin{n});
end

r = feval(promote(a, varargin{:}), median(double(a), v{:}));
