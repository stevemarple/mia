function r = max(a, varargin)
%MAX  MAX for (U)INT 8, 16, 32 data types.
%
%   See datafun/MAX.

for n = 1:length(varargin)
  varargin{n} = double(varargin{n});
end

r = feval(promote(a, varargin{:}), max(double(a), varargin{:}));
