function r = uplus(a)
%UPLUS  UPLUS for (U)INT 8, 16, 32 data types.
%
%   See ops/UPLUS.


r = feval(class(a), uplus(double(a)));
