function r = ldivide(a, b)
%LDIVIDE  LDIVIDE for (U)INT 8, 16, 32 data types.
%
%   See ops/LDIVIDE.


r = feval(promote(a, b), ldivide(double(a), double(b)));
