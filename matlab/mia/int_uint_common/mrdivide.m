function r = mrdivide(a, b)
%MRDIVIDE  MRDIVIDE for (U)INT 8, 16, 32 data types.
%
%   See ops/MRDIVIDE.


r = feval(promote(a, b), mrdivide(double(a), double(b)));
