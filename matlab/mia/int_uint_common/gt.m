function r = gt(a, b)
%GT  GT for (U)INT 8, 16, 32 data types.
%
%   See ops/GT.


r = logical(feval(promote(a, b), gt(double(a), double(b))));
