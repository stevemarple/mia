function r = min(a, varargin)
%MIN  MIN for (U)INT 8, 16, 32 data types.
%
%   See datafun/MIN.

for n = 1:length(varargin)
  varargin{n} = double(varargin{n});
end

r = feval(promote(a, varargin{:}), min(double(a), varargin{:}));
