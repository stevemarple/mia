function r = rdivide(a, b)
%RDIVIDE  RDIVIDE for (U)INT 8, 16, 32 data types.
%
%   See ops/RDIVIDE.


r = feval(promote(a, b), rdivide(double(a), double(b)));
