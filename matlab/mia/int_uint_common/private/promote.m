function r = promote(a, b, varargin)
%PROMOTE  Return the class to which values should be promoted
%
%   r = PROMOTE(a, b)
%   r: CHAR describing class to be used
%   a: object of a numerical class
%   b: object of a numerical class
%

ac = class(a);
if nargin == 1
  r = ac;
  return;
end

bc = class(b);

s.logical = 1;
s.int8 = 8;
s.uint8 = 8.5;
s.int16 = 16;
s.uint16 = 16.5;
s.int32 = 32;
s.uint32 = 32.5;
s.double = 64;
s.sparse = 64.5; % sparse array of doubles, and double * sparse = sparse

% not ideal using non-integer values, but 0.5 can be represented properly as
% a float.



switch max(getfield(s, ac), getfield(s, bc))
 case 1
  r = 'logical';
 
 case 8
  r = 'int8';
 case 8.5
  r = 'uint8';
 case 16
  r = 'int16';
 case 16.5
  r = 'uint16';
 case 32
  r = 'int32';
 case 32.5
  r = 'uint32';
 
 case 64
  r = 'double';
 case 64.5
  r = 'sparse';
 
 otherwise
  error('unknown class to promote to');
end


if length(varargin)
  r = promote(feval(r, 0), varargin{:});
end
