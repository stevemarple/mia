function r = colon(a, varargin)
%COLON  COLON for (U)INT 8, 16, 32 data types.
%
%   See ops/COLON.

cls = promote(a, varargin{:});
a = double(a)
for n = 1:(length(varargin))
  varargin{n} =  double(varargin{n});
end

r = feval(cls, builtin('colon', a, varargin{:}));
