function r = isnan(a)
%ISNAN   Test if not-a-number
%
%   See elmat/ISNAN.

r = repmat(isnan(1), size(a));
