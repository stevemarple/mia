function r = and(a, b)
%AND  AND for (U)INT 8, 16, 32 data types.
%
%   See ops/AND.


r = feval(promote(a,b), and(double(a), double(b)));
