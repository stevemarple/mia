function r = mldivide(a, b)
%MLDIVIDE  MLDIVIDE for (U)INT 8, 16, 32 data types.
%
%   See ops/MLDIVIDE.


r = feval(promote(a, b), mldivide(double(a), double(b)));
