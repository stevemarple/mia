function r = sum(a, varargin)
%SUM  Sum for (U)INT 8, 16, 32 data types.
%
%   See datafun/SUM.

r = feval(promote(a, varargin{:}), sum(double(a), varargin{:}));
