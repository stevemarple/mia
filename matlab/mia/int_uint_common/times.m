function r = times(a, b)
%TIMES  TIMES for (U)INT 8, 16, 32 data types.
%
%   See ops/TIMES.


r = feval(promote(a,b), times(double(a), double(b)));
