function r = le(a, b)
%LE  LE for (U)INT 8, 16, 32 data types.
%
%   See ops/LE.


r = logical(feval(promote(a, b), le(double(a), double(b))));
