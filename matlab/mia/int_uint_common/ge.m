function r = ge(a, b)
%GE  GE for (U)INT 8, 16, 32 data types.
%
%   See ops/GE.


r = logical(feval(promote(a,b), ge(double(a), double(b))));
