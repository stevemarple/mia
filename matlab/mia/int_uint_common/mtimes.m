function r = mtimes(a, b)
%MTIMES  MTIMES for (U)INT 8, 16, 32 data types.
%
%   See ops/MTIMES.


r = feval(promote(a,b), mtimes(double(a), double(b)));
