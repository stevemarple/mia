function r = plus(a, b)
%PLUS  PLUS for (U)INT 8, 16, 32 data types.
%
%   See ops/PLUS.

r = feval(promote(a, b), plus(double(a), double(b)));
