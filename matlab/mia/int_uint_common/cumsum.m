function r = cumsum(a)
%CUMSUM  CUMSUM for (U)INT 8, 16, 32 data types.
%
%   See datafun/CUMSUM.

% r = feval(class(a), cumsum(double(a)));

% don't return as anything other than double for fear of overflow
r = cumsum(double(a)); 
