function r = mpower(a, b)
%MPOWER  MPOWER for (U)INT 8, 16, 32 data types.
%
%   See ops/MPOWER.


r = feval(promote(a, b), mpower(double(a), double(b)));
