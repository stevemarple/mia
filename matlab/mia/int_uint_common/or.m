function r = or(a, b)
%OR  OR for (U)INT 8, 16, 32 data types.
%
%   See ops/OR.


r = feval(promote(a, b), or(double(a), double(b)));
