function r = power(a, b)
%POWER  POWER for (U)INT 8, 16, 32 data types.
%
%   See ops/POWER.


r = feval(promote(a, b), power(double(a), double(b)));
