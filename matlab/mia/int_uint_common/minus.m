function r = minus(a, b)
%MINUS  MINUS for (U)INT 8, 16, 32 data types.
%
%   See ops/MINUS.


r = feval(promote(a,b), minus(double(a), double(b)));
