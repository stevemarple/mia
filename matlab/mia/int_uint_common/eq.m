function r = eq(a, b)
%EQ  EQ for (U)INT 8, 16, 32 data types.
%
%   See ops/EQ.


r = feval(promote(a,b), eq(double(a), double(b)));
