function [r, idx] = sort(a, varargin)
%SORT  SORT for (U)INT 8, 16, 32 data types.
%
%   See datafun/SORT.

[tmp idx] = sort(double(a), varargin{:});
r = feval(class(a), tmp);
