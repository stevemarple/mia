function [C,x]=matfourierfit(y,n,w)
% MATFOURIERFIT  Fit Fourier series to vector with weighting and ignoring NaNs
%
% [C,X]=MATFOURIERFIT(Y,N,W)
%
% Y is the vector containing the function to be fitted with a
% truncated Fourier series of order N (i.e. the highest order terms
% will be cos(N*t) and sin(N*t) where t runs from 0 to 2*PI over
% Y.) W is a weighting function for the fit and should be the same
% size as Y. Usually W=ONES(SIZE(Y)) should be used.
%
% If Y contains NaNs, the weighting function is automatically set
% to zero at these points so that they are ignored in the fitting
% process.
%
% C is the vector of coefficients. C(1) is the zero-frequency term,
% C(2:2+N-1) are the cosine coefficients and C(2+N:end) are the
% sine coefficients. X is fitted Fourier series. SIZE(X)==SIZE(Y).

% 2004-02-12 senior: initial version

y=y(:);
w=w(:);

% auto-nan-avoidance
w(isnan(y))=0;
y(isnan(y))=0; % because even nan*zero=nan!

% independent variable
t=linspace(0,2*pi,length(y))';

% basis functions
fprintf('matfourierfit: evaluating basis functions\n');
drawnow;
B=repmat(nan,length(y),2*n+1);
B(:,1)=ones(size(y));
T=repmat(t,1,n).*repmat(1:n,length(y),1);
B(:,2:2+n-1)=cos(T);
B(:,2+n:end)=sin(T);

% weighted integrals of the function with the basis functions
fprintf('matfourierfit: evaluating RHS\n');
drawnow;
Y=repmat(y,1,2*n+1);
W=repmat(w,1,2*n+1);
RHS=trapz(t,W.*Y.*B);

% weighted integrals of products of the basis functions
fprintf('matfourierfit: evaluating matrix\n');
drawnow;
A=zeros(2*n+1);
% form each row of the matrix
for row=1:2*n+1
  P=repmat(B(:,row),1,2*n+2-row);
  Q=B(:,row:end);
  W=repmat(w,1,2*n+2-row);
  I=trapz(t,W.*P.*Q);
  A(row,row:end)=I;
end
% complete the matrix using symmetry
A=A+(A.'-diag(diag(A)));

% and now the easy bit: find the coefficients!
fprintf('matfourierfit: solving matrix equation\n');
drawnow;
C=A\RHS';

% evaluate the solution
fprintf('matfourierfit: evaluating solution\n');
drawnow;
x=zeros(size(y));
for k=1:2*n+1
  x=x+C(k)*B(:,k);
end

