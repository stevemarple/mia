function readme(varargin)
%README file for Multi-Instrument Analysis
%
%   This README file attempts to document some of the design decisions
%   undertaken in producing MIA, and why it is the way it is.
% 
%
%   CLASS CONSTRUCTORS
%
%   Class constructors MUST be able to convert valid STRUCTs back into
%   class objects. This is needed to matlab 5.1 compatibility, which does
%   not automatically call LOADOBJ. If the class stored in the file
%   differs from the current version (deined by fielsnames and order) it
%   will fail to load properly, and it converted to struct.

% if called as a function show this file as if "help readme" was typed.
[st idx] = dbstack;
help(st(1).name);
