function r = nonanrms(d, varargin)
%NONANRMS  Return the root mean square value, ignoring NANs
%
%   r = RMS(x)
%   r = RMS(x, dim)
%   r: root (no-nan) mean square of x
%   x: NUMERIC vector or matrix
%   dim: optional dimension (default is 1)
%
% RMS is the root mean square value of the matrix x, along dimension,
% ignoring NANs (ie the MEAN function is replaced by NONANMEAN).
%
% See also RMS, NONANMEAN.

error(nargchk(1, 2, nargin));

switch numel(varargin)
 case 0
  dim = 1;
  
 case 1
  dim = varargin{1};
end

r = sqrt(nonanmean(power(d, 2), dim));


