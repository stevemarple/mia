function [r, t] = earthradius(varargin)
%EARTHRADIUS  Radius of Earth in metres
%
%   r = EARTHRADIUS
%   r = EARTHRADIUS(...)
%
% The default behaviour can bw modified with following parameter
% name/value pairs:
%
%   'type', CHAR
%   Select which radius to use. A list of valid types can see seen with
%   "earthradius('list')". In addition the default polar radius can be
%   specified with 'polar' and the default equatorial radius with
%   'equatorial'.

defaults.type = '';
defaults.list = 0;

if nargin == 1 & strcmp(varargin{1}, 'list')
  defaults.list = 1;
else
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
end

s.polar_weast1981 = 6357e3;

% There are from Astronomical Almanac (1994)
s.equatorial_iau1964 = 6378160;
s.polar_iau1964 = 6356774.7;

s.equatorial_iau1976 = 6378140;
s.polar_iau1976 = s.equatorial_iau1976 * (1-0.00335281);

s.polar_iugg = 6378137;

% From http://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
s.mean_volumetric_nasa = 6371.0e3;

% From http://www.gfy.ku.dk/~iag/HB2000/part4/grs80_corr.htm
s.mean_iugg = 6371008.7714;

% Mean radius, mass, and inertia for reference Earth models Frederic
% Chambat, Bernard Valette. Physics of the Earth and Planetary Interiors 124
% (2001) 237 253
s.mean_geoidal_chambat_2001 = 6370994.4; % (+/- 3.0)


default_type = 'equatorial_iau1976';
default_equatorial = 'equatorial_iau1976';
default_polar = 'polar_iau1976';
default_mean = 'mean_volumetric_nasa';

if isempty(defaults.type)
  defaults.type = default_type;
end
if strcmp(defaults.type, 'polar')
  defaults.type = default_polar;
elseif strcmp(defaults.type, 'equatorial')
  defaults.type = default_equatorial;
elseif strcmp(defaults.type, 'mean')
  defaults.type = default_mean;
end

if logical(defaults.list)
  if nargout
    r = s;
  else
    disp('The following radius types may be selected:');
    fn = fieldnames(s);
    for n = 1:length(fn)
      disp(sprintf('%30s: %f m', fn{n}, getfield(s, fn{n})));
    end
    disp(' ');
    disp(['Default radius: ' default_type]);
    disp(['Default equatorial radius: ' default_equatorial]);
    disp(['Default polar radius: ' default_polar]);
    disp(['Default mean radius: ' default_mean]);
  end
  return
end

t = defaults.type;
if isfield(s, defaults.type)
  r = getfield(s, defaults.type);
else
  error(sprintf('unknown type for radius (was ''%s'')', defaults.type));
end

return

switch defaults.type
 case 'polar'
  r = s.polar_iau1976;


end


return

switch defaults.type
 case {'polar_weast1981' 'polar'}
  r = 6357e3;
  
 case 'polar_iau1964'
  r = 6356774.7; % ref: Astronomical Almanac (1994)
  
 case 'equatorial_iau1964'
  r = 6378160; % ref: Astronomical Almanac (1994)
  
 case 'polar_iau1976'
  r = 6378140; % ref: Astronomical Almanac (1994)
  
 case 'equatorial_iau1976'
  r = 6378140 * (1-0.00335281); % ref: Astronomical Almanac (1994)
  
  case 'polar_iugg'
  r = 6378137; % ref: Astronomical Almanac (1994)
 
  
 otherwise
  error(sprintf('unknown type for radius (was ''%s'')', defaults.type));
end

t = defaults.type;
