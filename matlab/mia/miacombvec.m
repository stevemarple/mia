function r = miacombvec(varargin)
%MIACOMBVEC Create all combinations of vectors.
%
%   r = MIACOMBVEC(a, b, ...)
%   r: Matix of all possible column vectors from a, b etc.
%
% MIACOMBVEC is compatible with the COMBVEC found in Matlab's Neural
% Network toolbox but is a different implementation. Unlike COMBVEC
% MIACOMBVEC will not only accept 1 and 2 dimensional matrices; an ERROR
% results if NDIMS returns > 2 for any input.
%


switch nargin
 case 0
  error('no parameters provided');
 case 1
  r = varargin{1};
 case 2
  a = varargin{1};
  b = varargin{2};
  if ndims(a) > 2 || ndims(b) > 2
    error('found matrix with dimension > 2');
  end
  sza = size(a);
  szb = size(b);

  % r = zeros(sza(1) + szb(1), sza(2) * szb(2));

  % Do top section which is a repeated szb(2) times
  ra = repmat(a, [1 szb(2)]);
  
  % Do bottom section which has each column of b repeated sza(2) times
  rb = zeros(szb(1), szb(2) * sza(2));
  for n = 1:szb(2)
    rb(:, ((n-1)*sza(2))+[1:sza(2)]) = repmat(b(:, n), [1 sza(2)]);
  end

  r = [ra;rb];
 otherwise
  r = miacombvec(miacombvec(varargin{1}, varargin{2}), varargin{3:end});
end
