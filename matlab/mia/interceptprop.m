function [newprop, unvi] = interceptprop(list, property, varargin)
%INTERCEPTPROP Manipulate a property name/value list.
%
%   [newprop unvi] = INTERCEPTPROP(list, property)
%   [newprop unvi] = INTERCEPTPROP(list, property, alias1, ...)
%   newprop: property name/value structure
%   unvi: unknown name/value indices
%   list: property name/value list (CELL vector)
%   property: structure, where field names are the property names, and
%   the default values are the field values.
%   alias1: CELL array.
%
% property is a STRUCT whose field names are the property names to
% intercept. If the property name is found in the list its value is stored
% in the structure. The changed structure is returned as newprop. If
% propertyName occurs multiple times in the list, its last value is
% taken. Indices of names/values not listed in property are given in unvi
% (unknown name/value index). This can be used to pass unknown parameters
% onto other functions which might not know to deal with them, while the
% intercepted name/value pairs are not forwarded on. If the unknown
% name/value pairs are not forwarded onto other functions it is good
% practice to warn the user of any unknown parameter names,
% WARNIGNOREDPARAMETERS provides a simple method to do this.
%
% Optionally, parameter names can be aliased. Groups of aliases should
% be given as CELL arrays. Aliased names must appear as fields in the
% property STRUCT.
%
% See also MAKEPROP, WARNIGNOREDPARAMETERS.

newprop = property;
unvi = [];
propNames = fieldnames(property);
propNamesLower = lower(propNames);

if mod(numel(list), 2) ~= 0
  list
  error(sprintf('list must contain an even number of items (was %d)', ...
      numel(list)));
end

for m = 1:2:numel(list)
  if ~ischar(list{m})
    error('parameter name must be a CHAR');
  end
  li = lower(list{m});
  found = 0;
  for n = 1:length(propNames)
    if strcmp(propNamesLower{n}, li)
      newprop = setfield(newprop, propNames{n}, list{m+1});
      found = 1;
      break;
    end
  end
  if ~found
    % unvi = [unvi m m+1];
    unvi(end + [1:2]) = [m m+1];
  end
end
  
% varargin is a list of aliases
for n = 1:numel(varargin)
  aliases = varargin{n};
  if ~iscell(aliases)
    error('aliases must be cell arrays');
  end
  % check aliases are known fields
  found = logical(zeros(1, numel(list)/2));
  for m = 1:numel(aliases)
    if ~isfield(newprop, aliases{m})
      error(sprintf('%s is given as an alias but is not a known name', ...
		    aliases{m}));
    end
    found = found | strcmp(aliases{m}, list(1:2:end));
  end
  if any(found)
    % get value associated with the last alias found in the list
    valIdx = max(find(found)) * 2;
    val = list{valIdx};
    
    % now apply to all aliases fields
    for m = 1:numel(aliases)
      newprop = setfield(newprop, aliases{m}, val);
    end
  end
end


