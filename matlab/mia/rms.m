function r = rms(d, varargin)
%RMS  Return the root mean square value
%
%   r = RMS(x)
%   r = RMS(x, dim)
%   r: root mean square of x
%   x: NUMERIC vector or matrix
%   dim: optional dimension (default is 1)
%
% RMS is the root mean square value of the matrix x, along dimension
% d.
%
% See also NONANRMS.

error(nargchk(1, 2, nargin));

switch numel(varargin)
 case 0
  dim = 1;
  
 case 1
  dim = varargin{1};
end

r = sqrt(mean(power(d, 2), dim));


