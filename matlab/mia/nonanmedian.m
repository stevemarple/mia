function varargout = nonanmedian(data, varargin)
%NONANMEDIAN Median of elements, ignoring any NaNs.
%

varargout = cell(1, nargout);

nonansatall = ~any(isnan(data(:)));

if nonansatall
  varargout{1} = median(data, varargin{:});
  if nargout == 1
    return
  end
end

% hack so this works with both matlab 5.1 and newer versions
if matlabversioncmp('>=', '5.2')
  SUBSREF = 'subsref';
  SUBSASGN = 'subsasgn';
else
  SUBSREF = 'subsref2';
  SUBSASGN = 'subsasg2';
end

if nargin == 1
  dim = min(find(size(data) ~= 1));
  if isempty(dim)
    dim = 1;
  end
else
  dim = varargin{1};
end


sz = size(data);
nd = ndims(data);

rsz = sz;
rsz(dim) = 1;
rmedian = zeros(rsz);
rnum = zeros(rsz);

if nonansatall
  rnum(:) = sz(dim);
  varargout{2} = rnum;
  return;
end

cvin = cell(1, nd);
for n = 1:nd
  cvin{n} = 1:rsz(n);
end
cv = miacombvec(cvin{:}); % all combinations which must be tried


s.type = '()';
s.subs = cell(1, nd);
s.subs{dim} = ':';

for n = 1:size(cv, 2)
  for m = 1:size(cv,1)
    if m ~= dim
      s.subs{m} = cv(m,n);
    end
  end
  data2 = feval(SUBSREF, data, s);
  nonans = find(~isnan(data2));
  data2median = median(data2(nonans));
  data2num = length(nonans);
  rnum = feval(SUBSASGN, rnum, s, data2num);
  if data2num
    rmedian = feval(SUBSASGN, rmedian, s,data2median);
  else
    % all are nan
    rmedian = feval(SUBSASGN, rmedian, s, nan);
  end
end

varargout{1} = rmedian;
if nargout >= 2
  varargout{2} = rnum;
end
	  
