function setup_curl(remake)
%SETUP_CURL Set up file to use Curl
%
% SETUP_CURL(remake)
% remake: if set and true delete any existing files.
%
% SETUP_CURL creates a .curlrc file (_curlrc on Windows) and a .netrc
% (_netrc on Windows) in the user's home directory. The netrc file
% contains your STP data password, edit the file manually if you change
% your password.
%

home = getenv('HOME');
if isempty(home)
  error('HOME environment variable is not set, cannot continue');
end


if ispc
  prefix = '_';
else
  prefix - '.';
end

curlrc = fullfile(home, [prefix 'curlrc']);
netrc = fullfile(home, [prefix 'netrc']);

if nargin > 0 && remake
  if url_exist(curlrc)
    delete(curlrc);
  end
  if url_exist(netrc)
    delete(netrc);
  end
end


curlrcTxt = '--netrc-optional';
if url_exist(curlrc)
  disp(sprintf('%s already exists, check it contains "%s"', ...
	       curlrc, curlrcTxt));
else
  fid = url_fopen(curlrc, 'w');
  fprintf(fid, '%s\n', curlrcTxt);
  fclose(fid);
end

password = modalquery('Enter your STP data access password', ...
		      'STP password?', 'Your password is required');

ud = userdetails;
netrcTxt = sprintf('machine www.dcs.lancs.ac.uk login %s password %s', ...
		   ud.email, password);
if url_exist(netrc)
  disp(sprintf('%s already exists, check it contains "%s"', ...
	       netrc, netrcTxt));
else
  fid = url_fopen(netrc, 'w');
  fprintf(fid, '%s\n', netrcTxt);
  fclose(fid);
end
  
disp(['You can reset your STP data access details by running' ...
      ' "setup_curl(true)"']);
