function r = getorder(fil)
%GETORDER Return order used by a MIA_FILTER_SGOLAY filter.
%
%   r = GETORDER(fil);
%   r: order (DOUBLE)
%   fil: MIA_FILTER_SGOLAY object
%
%   See also MIA_FILTER_SGOLAY.

if length(fil) == 1
  r = fil.order;
else
  r = zeros(size(fil));
  for n = 1:prod(size(fil))
    tmp = fil(n);
    r(n) = tmp.order;
  end
end

