function r = getwindowsize(fil)
%GETRESOLUTION  Return window size used by a MIA_FILTER_SGOLAY filter
%
%   r = GETRESOLUTION(fil);
%   r: TIMESPAN object
%   fil: MIA_FILTER_SGOLAY object
%
%   See also MIA_FILTER_SGOLAY.

if length(fil) == 1
  r = fil.windowsize;
else
  r = timespan(zeros(size(fil)), 's');
  for n = 1:prod(size(fil))
    tmp = r(n);
    r(n) = tmp.windowsize;
  end
end
