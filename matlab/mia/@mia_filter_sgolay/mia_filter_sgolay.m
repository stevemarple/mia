function fil = mia_filter_sgolay(varargin)
%MIA_FILTER_SGOLAY  Constructor for MIA_FILTER_SGOLAY class.
%
%   r = MIA_FILTER_SGOLAY;
%   default constructor
%
%   r = MIA_FILTER_SGOLAY(...)
%   parameter name/value pair interface. The following parameters are
%   accepted:
%
%     'order', SCALAR
%     Order of the Savitzky-Golay filter.
%
%     'windowsize', TIMESPAN
%     Desired window size.
%
% See also mia_filter_base/FILTER.

[tmp cls] = fileparts(mfilename);
parent = 'mia_filter_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
fil.versionnumber = latestversion;

fil.order = 3;
fil.windowsize = timespan(5,'s');


if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  ; % nothing needs to be done
  p = feval(parent);
  fil = class(fil, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  fil = varargin{1}

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [fil unvi] = interceptprop(varargin, fil);
 
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_filter_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  fil = class(fil, cls, p);
  
else
  error('incorrect parameters');
end

% check order is set
if isempty(fil.order)
  error('order not set');
end
  
zt = timespan(0, 's');
% check windowsize exists and is scalar
if length(fil.windowsize) ~= 1
  error('window size not scalar');
elseif ~isa(fil.windowsize, 'timespan')
  error('windowsize must be a timespan object');
elseif fil.windowsize <= zt
  % timespan must be > 0!
  error('windowsize must have a positive duration');
end
  
% Ensure that the returned object is marked with the latest version
% number
fil.versionnumber = latestversion;
