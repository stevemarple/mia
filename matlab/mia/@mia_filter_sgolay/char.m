function s = char(fil)
%CHAR Convert MIA_FILTER_SGOLAY object to char.
%
%   s = CHAR(fil)
%   s: CHAR representation
%   fil: MIA_FILTER_SGOLAY object to be converted
%
%   See also strfun/CHAR.

if length(fil) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_filter_base_char(fil);
  return;
end

s = sprintf('%sorder         : %d\nwindow size   : %s', ...
            mia_filter_base_char(fil), fil.order, char(fil.windowsize));
