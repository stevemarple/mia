function r = getprocessing(fil, mia)
%GETPROCESSING Get a comment describing the processing carried out
%
%   r = GETPROCESSING(fil, mia)
%   r: CHAR array
%   fil: MIA_FILTER_SGOLAY object
%
%   See also mia_filter_base/ADDPROCESSING

% calculate real window size used
rws = getblocksize(fil, mia) * getresolution(mia);

r = sprintf('%s filter applied: order %d with window size %s', ...
            getname(fil), fil.order, char(rws));

