function r = filter(fil, mia)
%FILTER Filter data with a sliding average.
%
%   r = FILTER(fil, mia)
%   r: filtered data, of same type as mia
%   fil: MIA_FILTER_SGOLAY object
%   mia: object derived from MIA_BASE
%
%   See also MIA_FILTER_SGOLAY, MIA_BASE.

r = mia;

N = numel(mia);
if N ~= 1
  for n = 1:N
    [tmp mfname] = fileparts(mfilename);
    r(n) = feval(mfname, fil, mia(n));
  end
  return
end

blockSize = getblocksize(fil, mia);
if blockSize <= fil.order
  disp(sprintf(['window too small to filter data at %s resolution ' ...
                'and order %s'], char(getresolution(mia), fil.order)));
  
  return;
end
 
data = getdata(mia);

% WRONG! 
% originalsz = size(data) % single image would be 2D!!!!

% ensure single images have size vector [1x3]
originalsz = getdatasize(mia);

% convert to a 2D array (rows will be independent so should be safe)
datasz = [prod(originalsz(1:(end-1))) originalsz(end)];
data = reshape(data, datasz);

% sgolayfilt works on columns, our data is in rows
newdata = sgolayfilt(data', fil.order, blockSize)';

% return data to original size
data = reshape(newdata, originalsz);

% put back into object
r = setdata(r, newdata);

r = addprocessing(r, getprocessing(fil, mia));
