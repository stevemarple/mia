function r = getname(fil, varargin)
%GETTYPE  Return filter name.
%
%   r = GETNAME(fil)
%   r: CHAR
%   mia: MIA_FILTER_SGOLAY object
%
%   See also MIA_FILTER_SGOLAY.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch mode
 case {'l' 'c' 'C'}
  % proper names, so should be capitalized
  r = 'Savitzky-Golay';
  
 case 'u'
  r = 'SAVITZKY-GOLAY';
 
 otherwise
  error('unknown mode');
end

return

