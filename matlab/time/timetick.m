function formatStr = timetick(axisHandle, ax, startTime, endTime, resolution, varargin)
%TIMETICK Set ticks on an axis to be marked with time.
%
%   fstr = TIMETICK(ah, ax, st, et, res)
%   fstr = TIMETICK(ah, ax, st, et, res, alflag)
%
%   fstr: format string use for labelling ticks
%   ah: AXES handle
%   ax: axis ('X', 'Y' or 'Z')
%   st: start time (TIMESTAMP or TIMESPAN)
%   et: end time (TIMESTAMP or TIMESPAN)
%   alflag: autolabel flag (0 = don't label axis), if char, the string is
%   used instead of 'time'.
%   
%   A high-level function for labelling axes with time. The start and end
%   times may be given as TIMESTAMP or TIMESPAN objects.
%
%   See also TIMESTAMP, TIMESPAN, AXES.

formatStr = '';
if isempty(axisHandle)
  return
end

ax = upper(ax(1));

% inherit fontsize, fontweight etc from defaultaxeslabelparams;
defaults = defaultaxeslabelparams;

% an approx number of tickmarks wanted work in seconds to avoid unnecessary
% calls to time class rawIntervalSec = gettotalseconds((endTime - startTime)
% ...  / defaults.tickmarksnum);

tmpfields1 = fieldnames(defaults);
defaults.tickmarksnum = 8;  
defaults.autolabel = 1;

tmpfields2 = fieldnames(defaults);
nontextparamfields = setdiff(tmpfields2, tmpfields1);

if nargin < 5
  error('incorrect parameters');
elseif nargin == 5
  defaults.autolabel = 0;

elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % varargin is parameter name/value list
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});

elseif nargin == 6
  defaults.autolabel = varargin{1};

elseif nargin == 7
  % seems to be undocumented...
  defaults.autolabel = varargin{1};
  defaults.tickmarksnum = varargin{2};

else
  error('incorrect parameters');
end

if isa(startTime, 'timestamp') & isa(endTime, 'timespan')
  % convert the duration to an end time
  endTime = startTime + endTime; 
end


% calculate a sensible interval between (visible) tickmarks


lim = get(axisHandle(1), [ax 'Lim']);
plotSt = lim(1) * resolution + startTime;
plotEt = lim(2) * resolution + startTime;
plotDuration = (lim(2) - lim(1)) * resolution;
rawIntervalSec = gettotalseconds(plotDuration) / ...
    defaults.tickmarksnum;

alignWithDates = [];
% duration = endTime - startTime;

if plotDuration > timespan(3650, 'd')
  % very long time, use even years
  alignWithDates.years = (2*floor(getyear(plotSt)/2)):2:getyear(plotEt);
  alignWithDates.months = 1;
  alignWithDates.days = 1;
  formatStr = '%Y';
elseif plotDuration > timespan(800, 'd')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1;
  alignWithDates.days = 1;
  formatStr = '%Y';
elseif plotDuration > timespan(200, 'd')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1:3:12;
  alignWithDates.days = 1;
  formatStr = '%d/%m/%Y';
elseif plotDuration > timespan(60, 'd')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1:12;
  alignWithDates.days = [1];
  formatStr = '%d/%m/%Y';
elseif rawIntervalSec > 10*86400
  intervalSec = rawIntervalSec - rem(rawIntervalSec, 10*86400); 
  formatStr = '%d/%m/%Y';
elseif rawIntervalSec > 2*86400;
  intervalSec = rawIntervalSec - rem(rawIntervalSec, 2*86400); 
  formatStr = '%d/%m';
elseif rawIntervalSec > 24*3600
  intervalSec = 2*86400;
  formatStr = '%d/%m';
elseif rawIntervalSec > 12*3600
  intervalSec = 24*3600;
  formatStr = '%d/%m';  
% elseif rawIntervalSec > 8*3600
  % intervalSec = 12*3600;
  % formatStr = '%H';
elseif rawIntervalSec > 7*3600
  intervalSec = 24*3600;
  formatStr = '%d/%m';  
elseif rawIntervalSec > 6*3600
  intervalSec = 8*3600;
  formatStr = '%H';
elseif rawIntervalSec > 4*3600
  intervalSec = 6*3600;
  formatStr = '%H';
elseif rawIntervalSec > 2*3600
  intervalSec = 4*3600;
  formatStr = '%H';
elseif rawIntervalSec > 60*60
  intervalSec = 2*3600;
  formatStr = '%H';
elseif rawIntervalSec > 30*60
  intervalSec = 60*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 20*60
  intervalSec = 30*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 15*60
  intervalSec = 20*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 10*60
  intervalSec = 15*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 5*60
  intervalSec = 10*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 4*60
  intervalSec = 5*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 2*60
  intervalSec = 4*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 60
  intervalSec = 2*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 30
  intervalSec = 60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 20
  intervalSec = 30;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 15
  intervalSec = 20;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 10
  intervalSec = 15;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 8
  intervalSec = 10;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 4
  intervalSec = 5;
  formatStr = '%H:%M:%S';
else
  intervalSec = 1;
  formatStr = '%H:%M:%S';
end

timeStr = 'time';
switch formatStr
 case '%Y'
  timeUnits = 'year';
  timeStr = 'date';
 case '%d/%m/%Y'
  if isa(startTime, 'timespan')
    % cannot sensibly print the month
    formatStr = '%d';
    timeUnits = 'd';
  else
    timeUnits = 'day/month/year';
  end
  timeStr = 'date';
 case '%d/%m'
  if isa(startTime, 'timespan')
    % cannot sensibly print the month
    formatStr = '%d';
    timeUnits = 'd';
  else
    timeUnits = 'day/month';
  end
  timeStr = 'date';
 case '%H'
  timeUnits = 'h';
 case '%H:%M'
  timeUnits = 'h:m';
 case '%H:%M:%S'
  timeUnits = 'h:m:s';
 case '%M:%S'
  timeUnits = 'm:s';
 case '%S'
  timeUnits = 's';
 otherwise
  timeUnits = [];
end

if ischar(defaults.autolabel)
  if isempty(timeUnits)
    labelStr = ['\bf{' defaults.autolabel '}'];
  else
    labelStr = ['\bf{' defaults.autolabel ' (' timeUnits ')}'];
  end
elseif isempty(timeUnits)
  % in case we missed any
  labelStr = ['\bf{' timeStr '}'];
else
  labelStr = ['\bf{' timeStr ' (' timeUnits ')}'];
end

if ~isempty(alignWithDates)
  % special case for long durations
  tickStr = {};
  tickPos = [];
  n = 1;
  for y = alignWithDates.years
    for m = alignWithDates.months
      for d = alignWithDates.days
	t = timestamp([y m d 0 0 0]);
	tickStr{n} = strftime(t, formatStr);
	tickPos(n) = (t - startTime) / resolution;
	n = n + 1;
      end
    end
  end
  
else 
  interval = timespan(intervalSec, 's');

  % calculate sensible locations for the tickmarks to be
  % disp([mfilename ': make timeticks only for visible part!']);
  % tickStartTime = floor(startTime, interval);
  % tickEndTime = ceil(endTime, interval);

  % for first elem at 1
  % tickStartTime = floor(startTime + (lim(1) - 1)*resolution, interval);
  % tickEndTime =    ceil(startTime + (lim(2) - 1)*resolution, interval);

  % for first elem at 0
  tickStartTime = floor(startTime + (lim(1))*resolution, interval);
  tickEndTime =    ceil(startTime + (lim(2))*resolution, interval);

  sz = fix((tickEndTime - tickStartTime)/interval) + 1;
  tickStr = cell(1, sz);  % matrix to hold the tick label strings 
  tickPos = zeros(1, sz); % matrix to hold the tick positions

  t = tickStartTime;
  n = 1;
  while(t <= tickEndTime)
    if isa(t, 'timespan')
      if t < timespan(0,'s')
	tickStr{n} = ['-' strftime(t, formatStr)];
      elseif t > timespan
	tickStr{n} = strftime(t, formatStr);
      else
	% work around some odd matlab formatting
	tickStr{n} = ['  ' strftime(t, formatStr)];
      end
      
    else
      % for TIME objects
      tickStr{n} = strftime(t, formatStr);
    end
    
    % tickPos(n) = ((t - startTime) / resolution) + 1; % first elem is 1
    tickPos(n) = ((t - startTime) / resolution) + 0; % first elem is 0
    t = t + interval;
    n = n + 1;
  end

end

set(axisHandle, [ax, 'Tick'], tickPos, ...
		[ax, 'TickLabel'], tickStr);

% hopefully later versions of matlab will be able to supply handles to tick
% label text. If so test if tick labels overlap, can then reduce number
% of tickmarks
tickmarksNum2 = length(tickPos); % current number, (not requested number!)
if 0 & ...
      length(axisHandle) == 1 & tickmarksNum2 > 3 & ...
      tickmarksNum2 <= defaults.tickmarksnum
  % don't attempt for multiple axes
  ahData = get(axisHandle);
  tmpH = text('Parent', axisHandle, ...
	      'String', tickStr{1}, ...
	      'Units', ahData.Units);
  ahData.Position
  textExtent = get(tmpH, 'Extent')
  delete(tmpH);
  % worst case is the ticks are positioned so that we almost have
  % defaults.tickmarksnum+1 tickmarks
  if textExtent(3)*(tickmarksNum2+1) > 0.9*ahData.Position(3)
    disp('tickmarks too close, reducing the number of tickmarks...');
    formatStr = feval(mfilename, axisHandle, ax, startTime, endTime,  ...
		      resolution, defaults.autolabel, tickmarksNum2-1);
    return;
  end
end


ud = {startTime endTime resolution defaults.autolabel};
labelH = get(axisHandle, [ax 'Label']);
if length(labelH) > 1
  labelH = [labelH{:}]; % convert from cell array
end
set(labelH, 'UserData', ud);

if defaults.autolabel ~= 0
  set(labelH, rmfield(defaults, nontextparamfields), 'String', labelStr);
end



