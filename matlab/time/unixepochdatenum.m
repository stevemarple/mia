function r = unixepochdatenum

r = datenum(1970, 1, 1); % unix epoch datenum
