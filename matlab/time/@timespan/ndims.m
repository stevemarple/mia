function r = ndims(ts)
%NDIMS NDIMS overloaded for TIMESPAN class.
%
% See NDIMS.

r = ndims(ts.ms);

