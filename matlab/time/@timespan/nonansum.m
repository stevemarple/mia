function r = nonansum(a)
%NONANSUM SUM of TIMESPAN objects, ignoring NANs.
%
% See NONANSUM.

r_ms = nonansum(a.ms);
r = timespan('cdfepoch', r_ms);
