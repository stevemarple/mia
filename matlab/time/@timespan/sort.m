function [y,i] = sort(t, varargin)
%SORT  Sort TIMESPANs in ascending order.
%
% See datafun/SORT.

[y_ms,i] = sort(t.ms, varargin{:});
y = timespan('cdfepoch', y_ms);
