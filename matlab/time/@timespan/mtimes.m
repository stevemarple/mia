function r = mtimes(a, b)
%MTIMES  Matrix multiply operator for TIMESPAN objects.
%
% r = MTIMES(a, b)
% r: TIMESPAN object
% a: TIMESPAN object
% b: numeric data
%
% r = MTIMES(a, b)
% r: TIMESPAN object
% a: numeric data
% b: TIMESPAN object
%
% a and b may be scalars or matrices.
%
% See TIMESPAN.

if isa(a, 'timespan') & isnumeric(b)
  r = timespan(mtimes(a.ms, b), 'ms');
elseif isnumeric(a) & isa(b, 'timespan')
  r = timespan(mtimes(a, b.ms), 'ms');
else
  error('invalid parameters');
end
