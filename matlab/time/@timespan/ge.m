function r = ge(a, b)
%GE  Greater than or equal operator overloaded for TIMESPAN class.
%
% See GE.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/ge must be timespan objects');
end

r = ge(a.ms, b.ms);


