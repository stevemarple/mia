function r = sum(a)
%SUM SUM of TIMESPAN objects, ignoring NANs.
%
% See datafun/SUM.

r_ms = sum(a.ms);
r = timespan('cdfepoch', r_ms);

