function r = numel(t)
%NUMEL NUMEL overloaded for TIMESPAN class.
%
% See NUMEL.

r = numel(t.ms);
