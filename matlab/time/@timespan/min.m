function varargout = min(a, varargin)
%MIN Min function overloaded for TIMESPAN.
%
% See datafun/MIN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/min must be a timespan object');
elseif nargin == 2 
  if isa(varargin{1}, 'timespan')
    % need the millisecond values
    varargin{1} = getcdfepochvalue(varargin{1});
  else
    error('second parameter to timespan/min must be a timespan object');
  end
end

n = nargout;
if n == 0
  n = 1;
end

[varargout{1:n}] = min(a.ms, varargin{:});
varargout{1} = timespan('cdfepoch', varargout{1});

