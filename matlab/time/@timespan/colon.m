function r = colon(a, varargin)
%COLON  Colon operator (:) for TIMESPAN class.
%
%   See COLON.

if nargin ~= 3
  error('timespan/colon requires 3 parameters');
end

b = varargin{1};
c = varargin{2};

if numel(a) ~= 1 | numel(b) ~= 1 | numel(c) ~= 1 
  error('all parameters must be scalar')
elseif ~isa(a, 'timespan') | ~isa(b, 'timespan') | ~isa(c, 'timespan')
  error('all 3 parameters must be timespan objects');
end

r = timespan(a.ms:b.ms:c.ms, 'ms');
