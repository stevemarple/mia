function [d, h, m, s] = getdhms(ts)
%GETDHMS Return the number of days, hours, minutes and seconds
%
%   [d h m s] = GETDHMS(ts)
%   d: days (DOUBLE)
%   h: hours (DOBULE)
%   m: minutes (DOUBLE)
%   s: seconds (DOUBLE)
%   ts: TIMESPAN object
%
%   See also TIMESPAN.


if nargout < 2 
  error('getdhms expects four return parameters');
end

[d h m s] = s2dhms(gettotalseconds(ts));
