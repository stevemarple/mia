function ts = timespan(varargin)
%TIMESPAN  Constructor for TIMESPAN class.
%
%   ts = TIMESPAN
%   ts: TIMESPAN object
%   Default constructor, duration is zero.
%
%   ts = TIMESPAN(<num>,<unit>, ...)
%   ts: TIMESPAN object
%   Create a TIMESPAN object from a set of value/unit pairs where num is the
%   numerical value and unit is one of the following quantities:
%     'd': days
%     'h': hours
%     'm': minutes
%     's': seconds
%     'ms': milliseconds
%     
% TIMESPAN(1,'h', 2,'m', 3,'s') creates a timespan of duration 1 hour, 2
% minutes and 3 seconds. Internally the timespan is stored as
% milliseconds.
%
%   See also TIMESTAMP.

cls = 'timespan';

% inferiorto('timestamp');

ts.ms = 0; % milliseconds

ts = class(ts, cls);

if nargin == 0
  % default constructor
  ;
  
elseif nargin == 1 & isa(varargin{1}, cls)
  % copy constructor
  ts = varargin{1};
  
elseif nargin == 1 & ischar(varargin{1}) & strcmp(varargin{1}, 'bad')
  ts.ms = nan;
elseif nargin == 1 & isstruct(varargin{1})
  if isequal(fieldnames(varargin{1}), {'ms'})
    ms = reshape([varargin{1}.ms], size(varargin{1}));
    ts = setcdfepochvalue(ts, ms);
    
  elseif isequal(fieldnames(varargin{1}), {'days'; 'seconds'; 'negative'})
    % old method
    days = [varargin{1}.days];
    seconds = [varargin{1}.seconds];
    % matlab 5.1 complains about index being zero. neg is logical but was
    % "uint8 (logical)" (as shown by whos function)
    neg = logical(double([varargin{1}.negative]));

    ms = (86400 * days + seconds) * 1e3;
    ms(neg) = -ms(neg);
    ms = reshape(ms, size(varargin{1}));
    ts = setcdfepochvalue(ts, ms);
    
  else
    error('incorrect parameters');
  end

elseif nargin == 2 & strcmp(varargin{1}, 'cdfepoch') ...
      & isnumeric(varargin{2})
  ts = setcdfepochvalue(ts, double(varargin{2}));
  
elseif mod(nargin, 2) == 0
  % in value/units pairs

  ms = 0;
  for n = 1:2:nargin
    if ~isa(varargin{n+1}, 'char')
      n
      varargin{n}
      error('incorrect parameters');
    end
    
    switch lower(varargin{n+1})
     case 'd'
      ms = ms + (86400e3 * varargin{n});
     case 'h'
      ms = ms + (3600e3 * varargin{n});
     case 'm'
      ms = ms + (60e3 * varargin{n});
     case 's'
      ms = ms + (1e3 * varargin{n});
     case 'ms'
      ms = ms + double(varargin{n});
     otherwise
      error('Unknown unit of time');
    end
  end
  
  ts = setcdfepochvalue(ts, ms);
  
else
  disp('---');
  nargin
  varargin{1:nargin}
  error('constructor called with incorrect arguments');
end

