function r = size(t, varargin)
%SIZE SIZE overloaded for TIMESPAN class.
%
% See elmat/SIZE.

r = size(t.ms, varargin{:});
