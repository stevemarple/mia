function r = fix(a, b)
%FIX  Round TIMESPAN objects down to TIMESPAN boundary closest to zero.
%
%    r = FIX(a, b)
%    r: TIMESPAN, rounded to ts boundary closest to TIMESPAN(0, 's')
%    a: TIMESPAN object to be rounded
%    b: TIMESPAN object
%
%    See also TIMESPAN, CEIL, FLOOR, ROUND.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

r_ms = fix(a.ms ./ b.ms) .* b.ms;
r = timespan('cdfepoch', r_ms);
