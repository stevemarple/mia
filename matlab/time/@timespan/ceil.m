function r = ceil(a, b)
%CEIL  Round TIMESPAN objects upwards to nearest TIMESPAN boundary.
%
%    r = CEIL(a, b)
%    r: TIMESPAN, rounded up to nearest ts boundary
%    a: TIMESPAN object to be rounded
%    b: TIMESPAN object
%
%    See also TIMESPAN, FIX, FLOOR, ROUND.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

r_ms = ceil(a.ms ./ b.ms) .* b.ms;
r = timespan('cdfepoch', r_ms);
