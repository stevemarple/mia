function r = minus(a, b)
%MINUS  - Minus operator for TIMESPAN objects.
%
%   r = MINUS(a, b)
%   r: TIMESPAN object
%   a: TIMESPAN object
%   b: TIMESPAN object
%
% a and b may be scalars or matrices, but if both are matrices they should
% be the same size.
%
% See TIMESPAN.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('arguments to timespan/minus must be timespan objects');
end

r = a;
r.ms = minus(a.ms, b.ms);

