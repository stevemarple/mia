function r = subsref(a, s)
%SUBSREF Subscripted reference overloaded for TIMESPAN class.
%
% See ops/SUBSASGN.

r = a;
r.ms = subsref(r.ms, s);
