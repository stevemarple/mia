function s = dateprintf(st, et)
%DATEPRINTF print start and end times in a neat, concise manner
%
%   s = DATEPRINTF(st, et)
%   st: starttime (e.g. when plotting only a portion of the data)
%   et: endtime    "        "

s = [strftime(st, '%H:%M:%S - ') strftime(et, '%H:%M:%S UT')];









