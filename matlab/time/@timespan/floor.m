function r = floor(a, b)
%FLOOR  Round TIMESPAN objects down to nearest TIMESPAN boundary.
%
%    r = FLOOR(a, b)
%    r: TIMESPAN, rounded down to smallest ts boundary
%    a: TIMESPAN object to be rounded
%    b: TIMESPAN object
%
%    See also TIMESPAN, CEIL, FIX, ROUND.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

r_ms = floor(a.ms ./ b.ms) .* b.ms;
r = timespan('cdfepoch', r_ms);
