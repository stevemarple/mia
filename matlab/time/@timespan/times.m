function r = times(a, b)
%TIMES  Array multiply operator for TIMESPAN objects.
%
% r = TIMES(a, b)
% r: TIMESPAN object
% a: TIMESPAN object
% b: numeric data
%
% r = TIMES(a, b)
% r: TIMESPAN object
% a: numeric data
% b: TIMESPAN object
%
% a and b may be scalars or matrices, but if both are matrics they should be
% the same size.
%
% See TIMESPAN.

if isa(a, 'timespan') & isnumeric(b)
  r = timespan(times(a.ms, b), 'ms');
elseif isnumeric(a) & isa(b, 'timespan')
  r = timespan(times(a, b.ms), 'ms');
else
  error('invalid parameters');
end
