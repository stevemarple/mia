function r = ldivide(a, b)
%LDIVIDE  Left array divide for TIMESPAN objects.
%
%   r = LDIVIDE(a, b)
%   r: numeric data or TIMESPAN object
%   a: TIMESPAN object
%   b: TIMESPAN object or numeric data
%
% a and b may be scalar or matrices, but if both are matrices they should be
% the same size.
%
% See TIMESPAN, LDIVIDE.

if ~isa(a, 'timespan') 
  error('invalid type passed to timespan/ldivide');
end

if isa(b, 'timespan')
  % result is double
  r = ldivide(a.ms, b.ms);
  
elseif isnumeric(b)
  % result is timespan
  r = timespan(ldivide(a.ms, b), 'ms');
  
else
  error('invalid type passed to timespan/ldivide');
end
