function r = any(a)
%ANY  True if any TIMESPAN elements are non-zero.
%
% See ops/ANY.

r = any(a.ms);
