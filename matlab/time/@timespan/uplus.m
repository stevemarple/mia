function r = uplus(a)
%UPLUS  + Unary plus operator for TIMESPAN objects.
%
%   r = UPLUS(a)
%   r: TIMESPAN object
%   a: TIMESPAN object
%
% See TIMESPAN, ops/UPLUS.

r = a;
r.ms = uplus(a.ms);

