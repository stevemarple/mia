function r = gettotalseconds(ts)
%GETTOTALSECONDS  Return the TIMESPAN in seconds.
%
%   r = GETTOTALSECONDS(ts)
%   r: total number of seconds (DOUBLE)
%   ts: TIMESPAN object(s)
%
%   GETTOTALSECONDS returns the values of the TIMESPAN in seconds, ie
%   including any contribution from days.
%
%   See also time/GETSECONDS.

r = ts.ms ./ 1e3;
