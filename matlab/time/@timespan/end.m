function r = end(t, k, n);
%END Last index in an indexing expression of TIMESPAN object array.
%
%    END(A,K,N) is called for indexing expressions involving the 
%    object A when END is part of the K-th index out of N indices.
%

if n == 1
  % e.g., t(2) return the number of elements in the object
  r = numel(t.ms);
else
  sz = size(t.ms);
  if k > length(sz)
    % if the index is greater than the object's dimensions
    r = 1;
  else 
    % return last element in that dimension
    r = sz(k);
  end
end
