function r = rdivide(a, b)
%RDIVIDE  Left array divide for TIMESPAN objects.
%
%   r = RDIVIDE(a, b)
%   r: numeric data or TIMESPAN object
%   a: TIMESPAN object
%   b: TIMESPAN object or numeric data
%
% a and b may be scalar or matrices, but if both are matrices they should be
% the same size.
%
% See TIMESPAN, RDIVIDE.

if ~isa(a, 'timespan') 
  error('invalid type passed to timespan/rdivide');
end

if isa(b, 'timespan')
  % result is double
  r = rdivide(a.ms, b.ms);
  
elseif isnumeric(b)
  % result is timespan
  r = timespan(rdivide(a.ms, b), 'ms');
  
else
  error('invalid type passed to timespan/rdivide');
end
