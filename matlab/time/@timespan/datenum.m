function r = datenum(t)
%DATENUM  Return TIMESPAN object as a DATENUM scalar.
%
%   r = DATENUM(t)
%   r: DOUBLE (same size as t)
%   t: TIMESPAN object(s)
% 
%   See also DATENUM.

r = (t.ms ./ 86400e3) + 1;
