function r = rem(a, b)
% r = REM(t, ts)
% Return remainder of x/y
%
% x: timespan
% y: timespan
% returns: remainder of x/y
%
% See also TIMESPAN, REM

[tmp op] = fileparts(mfilename);

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error(sprintf('parameters to timespan/%s must be timespan objects'));
end


r = a - b * fix(a ./ b);


