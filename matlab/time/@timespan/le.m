function r = le(a, b)
%LE  Less than or equal operator overloaded for TIMESPAN class.
%
% See LE.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/le must be timespan objects');
end

r = le(a.ms, b.ms);


