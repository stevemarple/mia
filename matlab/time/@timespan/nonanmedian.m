function r = nonanmedian(a, varargin)
%NONANMEDIAN MEDIAN value for TIMESPAN objects, ignoring NANs.
%
% See NONANMEDIAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/nonanmedian must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/nonanmedian must be numeric');
end

r_ms = nonanmedian(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

