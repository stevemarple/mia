function r = nanmean(a, varargin)
%NANMEAN MEAN value for TIMESPAN objects, ignoring NANs.
%
% See stats/NANMEAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/nanmean must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/nanmean must be numeric');
end

r_ms = nanmean(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

