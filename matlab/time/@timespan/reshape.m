function r = reshape(t, varargin)
%RESHAPE RESHAPE overloaded for TIMESPAN class.
%
% See RESHAPE.

r = t;
r.ms = reshape(r.ms, varargin{:});
