function r = permute(t, order)
%PERMUTE  Permute array dimensions of TIMESPAN objects.
%
% See elmat/PERMUTE.

r = t;
r.ms = permute(r.ms, order);
