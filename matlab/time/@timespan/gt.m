function r = gt(a, b)
%GT  Greater than operator overloaded for TIMESPAN class.
%
% See GT.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/gt must be timespan objects');
end

r = gt(a.ms, b.ms);


