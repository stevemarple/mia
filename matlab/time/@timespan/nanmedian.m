function r = nanmedian(a, varargin)
%NANMEDIAN MEDIAN value for TIMESPAN objects, ignoring NANs.
%
% See stats/NANMEDIAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/nanmedian must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/nanmedian must be numeric');
end

r_ms = nanmedian(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

