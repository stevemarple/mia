function [s,rep] = strftime(ts, formatstr, varargin)
%STRFTIME  Write a TIMESPAN with control over format.
%
%   [r,rep] = STRFTIME(ts, fstr, ...)
%   r: CHAR (or CELL matrix of CHARs if ts is not scalar)
%   rep: CELL, list of replacements made
%   ts: TIMESPAN object
%   fstr: see unix manual page for strftime
%
% STRFTIME converts the time into a CHAR using a user-defined format
% string. The standard unix format specifiers are implemented (see list
% below). In addition some extra format specifiers are included. The
% optional rep return parameter provides a list of the format specifiers
% which were replaced.
%
%   For compatibility with the timestamp/STRFTIME function the following are
%   recognised, though not all can be sensibly implemented; those marked
%   'ignored' will be replace by an empty string and will cause a WARNING
%   to be emitted.
%
%   %%: %
%   %A: Full day of the week (ignored)
%   %b: Abbreviated month name (ignored)
%   %B: Full month name (January-December) (ignored)
%   %d: day of month (1-31) (number of whole days)
%   %H: hour (00-23) (number of whole hours)
%   %j: day of year (001-366) (ignored)
%   %m: month (01-12) (ignored)
%   %M: minutes (00-59) (number of whole minutes)
%   %N: sign (+ or -)
%   %q: quarter (of year) (ignored)
%   %s: total number of seconds (differs from timestamp/STRFTIME)
%   %S: seconds (00-59) (number of whole seconds)
%   %t: 'st', 'nd', 'rd' or 'th' dependent upon day number in month (ignored)
%   %y: year (yy) (ignored)
%   %Y: year (YYYY) (ignored)
%   %#: fractional seconds (eg 1ms -> '001')
%
%   Additional parameter name/value pairs may be given to modify the
%   standard behaviour:
%
%     additionalreplacements, STRUCT
%       Intended for functions which extend strftime and wish to use their
%       own format specifiers. The structure fieldnames should be single
%       characters correpsonding to the new format specifiers, and the value
%       associated with the field is the replacement text, which should be a
%       CHAR. Tests for these additional replacements are made before
%       standard ones.
%
%   See also TIMESPAN, timestamp/STRFTIME.

if length(ts) ~= 1
  s = cell(size(ts));
  rep = cell(size(ts));
  for n = 1:numel(ts)
    % tsn = ts(n);
    
    % the above command doesn't call timespan/subsref in some matlab
    % versions! Do so manually.
    sr.type = '()';
    sr.subs = {n};
    tsn = subsref(ts, sr);
    [s{n} rep{n}] = feval(mfilename, tsn, formatstr);
  end
  return
end

% from this point on ts is scalar
rep = {};

defaults.additionalreplacements = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:end)});

spos = 1; % position of next write in output string

if ts < timespan(0,'s')
  negFlag = 1;
else
  negFlag = 0;
end

% dhms = abs(getdhms(ts));
[days hours mins secs] = getdhms(abs(ts));

i = 1;
s = '';
while i <= length(formatstr)
  switch formatstr(i)
   case '%'

    % look to next char
    i = i + 1;
    
    replacedchar = formatstr(i);
    if isfield(defaults.additionalreplacements, formatstr(i))
      tmp = getfield(defaults.additionalreplacements, formatstr(i));
      s = [s tmp];
      spos = spos + length(tmp);
    else

      switch formatstr(i)
	% percent
       case '%'
	s(spos) = '%';
	spos = spos + 1;

	% Full day name
       case 'A'
	warning('Not sensible format specifier (%A) for timespan class');
	
	% Abbreviated month name
       case 'b'
	warning('Not sensible format specifier (%b) for timespan class');
	
	% Full month name
       case 'B'
	warning('Not sensible format specifier (%B) for timespan class');
	
	% day dd
       case 'd'
	tmp = sprintf('%g', days);
	s = [s tmp];
	spos = spos + length(tmp);
	
	% hour HH
       case 'H'
	s = sprintf('%s%02d', s, hours);
	spos = spos + 2;
	
	% day of year
       case 'j'
	warning('Not sensible format specifier (%j) for timespan class');
	
	% month mm
       case 'm'
	warning('Not sensible format specifier (%m) for timespan class');
	
	% minute MM
       case 'M'
	s = sprintf('%s%02d', s, mins);
	spos = spos + 2;
	
	% negative sign
	% This is an extension to any other strftime
       case 'N'
	if negFlag
	  s = [s '-'];
	else
	  s = [s '+'];
	end
	spos = spos + 1;

       case 'q'
	warning('Not sensible format specifier (%q) for timespan class');

       case 's'
	% total seconds
	tmp = sprintf('%g', gettotalseconds(ts));
	s = [s tmp];
	spos = spos + length(tmp);
	
	% second SS
       case 'S'
	s = sprintf('%s%02d', s, fix(secs));
	spos = spos + 2;

	% 'st', 'nd', 'rd' or 'th' dependent upon mday number
       case 't'
	warning('Not sensible format specifier (%t) for timespan class');
	
	% year yy
       case 'y'
	warning('Not sensible format specifier (%y) for timespan class');
	
	% year YYYY
       case 'Y'
	warning('Not sensible format specifier (%Y) for timespan class');
	
       case '#'
	% fractional seconds (as milliseconds)
	s = sprintf('%s%03d', s, round(abs(secs-fix(secs))*1e3));
	spos = spos + 3;
       
        otherwise
	warning(sprintf(['unknown format specifier - ' ...
			 'ignoring (was %%%s)'], formatstr(i)));
        replacedchar = '';
      end
    end
    if ~isempty(replacedchar)
      % remember which characters were replaced
      rep{end+1} = replacedchar;
    end
    
   otherwise
    s(spos) = formatstr(i);
    spos = spos + 1;
  end
  i = i + 1; % next char in format string
end

if nargout > 1
  rep = unique(rep);
end
