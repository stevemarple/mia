function r = isvalid(ts)
%ISVALID  Indicate if a TIMESPAN is valid.
%
%   r = ISVALID(ts)
%   r: DOUBLE
%   ts: TIMESPAN
%
%   If ts is a matrix r is a matrix of the same size.
%
%   See also TIMESPAN.

r = isfinite(ts.ms);

