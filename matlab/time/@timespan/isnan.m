function r = isnan(ts)
%ISNAN  Indicate if a TIMESPAN is a NAN.
%
%   r = ISNAN(ts)
%   r: DOUBLE
%   ts: TIMESPAN
%
%   If ts is a matrix r is a matrix of the same size.
%
%   See also TIMESPAN.

r = isnan(ts.ms);
