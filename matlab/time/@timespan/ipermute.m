function r = ipermute(t, order)
%IPERMUTE  Ipermute array dimensions of TIMESPAN objects.
%
% See elmat/IPERMUTE.

r = t;
r.ms = ipermute(r.ms, order);
