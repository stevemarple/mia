function r = loadobj(t)
%LOADOBJ  Load filter for TIMESPAN class.

if isstruct(t)
  % old timespan class
  r = timespan(t);
else
  r = t;
end  
