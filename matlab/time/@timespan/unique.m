function [r, ri, rj] = unique(a, varargin)
%UNIQUE  Unique set overloaded for TIMESPAN.
%
% See ops/UNIQUE.

[r_ms, ri, rj] = unique(a.ms, varargin{:});

r = timespan(r_ms, 'ms');
