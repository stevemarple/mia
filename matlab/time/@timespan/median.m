function r = median(a, varargin)
%MEDIAN Average or median value for TIMESPAN objects.
%
% See datafun/MEDIAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/median must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/median must be numeric');
end

r_ms = median(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

