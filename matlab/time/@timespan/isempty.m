function r = isempty(t)
%ISEMPTY ISEMPTY overloaded for TIMESPAN class.
%
% See elmat/ISEMPTY.

r = isempty(t.ms);

