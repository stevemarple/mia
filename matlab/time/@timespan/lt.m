function r = lt(a, b)
%LT  Less than operator overloaded for TIMESPAN class.
%
% See LT.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/lt must be timespan objects');
end

r = lt(a.ms, b.ms);


