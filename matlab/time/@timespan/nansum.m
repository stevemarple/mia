function r = nansum(a)
%NANSUM SUM of TIMESPAN objects, ignoring NANs.
%
% See stats/NANSUM.

r_ms = nansum(a.ms);
r = timespan('cdfepoch', r_ms);

