function r = ne(a, b)
%NE  Non-equality operator overloaded for TIMESPAN class.
%
% See NE.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/ne must be timespan objects');
end

r = ne(a.ms, b.ms);


