function r = mrdivide(a, b)
%MRDIVIDE  Left array divide for TIMESPAN objects.
%
%   r = MRDIVIDE(a, b)
%   r: numeric data or TIMESPAN object
%   a: TIMESPAN object
%   b: TIMESPAN object or numeric data
%
% a and b may be scalar or matrices, but if both are matrices they should be
% the same size.
%
% See TIMESPAN, MRDIVIDE.

if ~isa(a, 'timespan') 
  error('invalid type passed to timespan/mrdivide');
end

if isa(b, 'timespan')
  % result is double
  r = mrdivide(a.ms, b.ms);
  
elseif isnumeric(b)
  % result is timespan
  r = timespan(mrdivide(a.ms, b), 'ms');
  
else
  error('invalid type passed to timespan/mrdivide');
end
