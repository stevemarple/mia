function vargout = xlim(varargin)
%XLIM XLIM overloaded for TIMESPAN
%
% TIMESPAN values are automatically converted to millisceonds since CDF
% epoch and the X limits set appropriately.
%
% See XLIM.

for n = 1:nargin
  if isa(varargin{n}, 'timespan')
    varargin{n} = getcdfepochvalue(varargin{n});
  end
end

[varargout{1:nargout}] = xlim(varargin{:});

