function r = vertcat(varargin)
%VERTCAT  Vertical concatenation for TIMESPAN objects
%
% See ops/VERTCAT.

ms = cell(size(varargin));
for n = 1:nargin
  if ~isa(varargin{n}, 'timespan')
    error('all elements must be timespans');
  end
  ms{n} = varargin{n}.ms;
end

r = varargin{1};
r.ms = vertcat(ms{:});

