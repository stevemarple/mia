function r = horzcat(varargin)
%HORZCAT  Horizontal concatenation for TIMESPAN objects
%
% See ops/HORZCAT.

ms = cell(size(varargin));
for n = 1:nargin
  if ~isa(varargin{n}, 'timespan')
    varargin{n}
    error(sprintf('all elements must be timespans (#%d was a %s)', ...
		  n, class(varargin{n})));
  end
  ms{n} = varargin{n}.ms;
end

r = varargin{1};
r.ms = horzcat(ms{:});

