function r = getcdfepochvalue(ts)
%GETCDFEPOCHVALUE Return the value of a TIMESPAN in CDF epoch units (ms).
%
%   r = GETCDFEPOCHVALUE(t)
%   r: duration in milliseconds (DOUBLE)
%   t: TIMESPAN

r = ts.ms;
