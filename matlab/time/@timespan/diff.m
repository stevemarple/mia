function r = diff(t, varargin)
%DIFF Difference between successive TIMESPAN values.
%
% See datafun/DIFF.

r = timespan(diff(t.ms, varargin{:}), 'ms');
