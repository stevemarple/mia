function r = isotime(ts)
%ISOTIME Convert TIMESPAN to ISO-style time string
%
%   r = ISOTIME(ts)
%   r: CHAR (or CELL is ts is not scalar)
%   ts: TIMESPAN object
%
% ISOTIME converts TIMESPAN objects to strings of the form HH:MM:SS. If
% the timespan object is greater than 1d then days are also included.
%
% See also timestamp/ISOTIME, timestamp/ISODATE.

if numel(ts) ~= 1
  r = cell(size(ts));
  for n = 1:numel(ts)
    % tsn = ts(n);
    % the above command doesn't call timestamp/subsref in some matlab
    % versions! Do so manually.    
    sr.type = '()';
    sr.subs = {n};
    tsn = subsref(ts, sr);
    r{n} = feval(mfilename, tsn);
  end
  return
end

day = timespan(1, 'd');
% ts is now scalar
if isvalid(ts)
  if ts < timespan(0, 's')
    % prefix with negative sign
    r = '-';
    ts = -ts;
  else
    r = '';
  end
  days = floor(ts ./ day);
  ts = rem(ts, day);
  if days
    r = sprintf('%s%dd ', r, days);
  end
  r = strftime(ts, [r '%H:%M:%S']);
else
  r = char(ts);
end

    
