function r = transpose(t)
%TRANSPOSE TRANSPOSE overloaded for TIMESPAN class.
%
% See TRANSPOSE.

r = t;
r.ms = transpose(t.ms);

