function r = eq(a, b)
%EQ  Equality operator overloaded for TIMESPAN class.
%
% See EQ.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('parameters to timespan/eq must be timespan objects');
end

r = eq(a.ms, b.ms);
