function r = nonanmean(a, varargin)
%NONANMEAN MEAN value for TIMESPAN objects, ignoring NANs.
%
% See stats/NONANMEAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/nonanmean must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/nonanmean must be numeric');
end

r_ms = nonanmean(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

