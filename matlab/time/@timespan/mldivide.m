function r = mldivide(a, b)
%MLDIVIDE  Left array divide for TIMESPAN objects.
%
%   r = MLDIVIDE(a, b)
%   r: numeric data or TIMESPAN object
%   a: TIMESPAN object
%   b: TIMESPAN object or numeric data
%
% a and b may be scalar or matrices, but if both are matrices they should be
% the same size.
%
% See TIMESPAN, MLDIVIDE.

if ~isa(a, 'timespan') 
  error('invalid type passed to timespan/mldivide');
end

if isa(b, 'timespan')
  % result is double
  r = mldivide(a.ms, b.ms);
  
elseif isnumeric(b)
  % result is timespan
  r = timespan(mldivide(a.ms, b), 'ms');
  
else
  error('invalid type passed to timespan/mldivide');
end
