function r = abs(t)
%ABS  Overloaded ABS function for TIMESPAN class.
%
%   r = ABS(t)
%   r: TIMESPAN object (same size as t)
%   t: TIMESPAN object
%
%   ABS(t) is the absolute value of the TIMESPAN object t.
%
%   See also elfun/ABS.

r = timespan(abs(t.ms), 'ms');


