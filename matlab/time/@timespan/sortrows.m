function [r,i] = sortrows(a, varargin)
%SORTROWS  Sort rows in ascending order for TIMESPAN arrays.
%
% See datafun/SORTROWS.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/sortrows must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/sortrows must be numeric');
end

[r_ms i] = sortrows(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

