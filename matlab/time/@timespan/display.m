function display(ts)
%DISPLAY  Display function for TIMESPAN class.
%
%   DISPLAY(ts)
%   ts: TIMESPAN object to be printed.
%
%   See also timespan/CHAR.

disp(' ');
disp([inputname(1),' = ' char(ts)]);
disp(' ');
