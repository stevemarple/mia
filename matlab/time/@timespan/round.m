function r = round(a, b)
%ROUND  Round TIMESPAN objects to nearest TIMESPAN boundary.
%
%    r = ROUND(a, b)
%    r: TIMESPAN, rounded to nearest ts boundary
%    a: TIMESPAN object to be rounded
%    b: TIMESPAN object
%
%    See also TIMESPAN, CEIL, FIX, FLOOR.

if ~isa(a, 'timespan') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

r_ms = round(a.ms ./ b.ms) .* b.ms;
r = timespan('cdfepoch', r_ms);
