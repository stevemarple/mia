function r = ctranspose(t)
%CTRANSPOSE CTRANSPOSE overloaded for TIMESPAN class.
%
% See CTRANSPOSE.

r = t;
r.ms = ctranspose(t.ms);

