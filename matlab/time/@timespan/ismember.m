function varargout = ismember(a, s, varargin)
%ISMEMBER Overloaded ISMEMBER function for TIMESTAMP class
%
% See ISMEMBER.

n = nargout;
if n == 0
  n = 1;
end

[varargout{1:n}] = ismember(a.ms, s.ms, varargin{:});
