function r = setcdfepochvalue(t, ms)
%SETCDFEPOCHVALUE  Set a TIMESPAN object to given duration in CDF epoch units.
%
%   r = SETCDFEPOCHVALUE(t, ms)
%   r: TIMESPAN
%   t: TIMESPAN
%   ms: duration in milliseconds
%
% See also TIMESPAN.

r = t;
r.ms = ms;
