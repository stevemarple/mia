function r = mean(a, varargin)
%MEAN Average or mean value for TIMESPAN objects.
%
% See datafun/MEAN.

if ~isa(a, 'timespan') 
  error('first parameter to timespan/mean must be a timespan object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timespan/mean must be numeric');
end

r_ms = mean(a.ms, varargin{:});
r = timespan('cdfepoch', r_ms);

