function r = all(a)
%ALL  True if all TIMESPAN elements are non-zero.
%
% See ops/ALL.

r = all(a.ms);
