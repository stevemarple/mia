function r = char(ts, varargin)
%CHAR convert timespan object to a string
%   s = CHAR(ts)
%   s = CHAR(ts, cf)
%   s: string representation
%   ts: TIMESPAN object
%   cf: compact flag
%
%   If the flag for compact is set then units whose value are zreo are
%   not printed.


% x = ts.seconds;
% hours = fix(ts.seconds / 3600); % whole hours
% x = x - (hours*3600); % in seconds, excluding hours component
% minutes = fix(x / 60); % whole minutes
% seconds = rem(x,60); % in seconds, excluding hours and minutes

if numel(ts) ~= 1
  r = matrixinfo(ts);
  return;
end

% ts is scalar from this point on

if ~isvalid(ts)
  if ts.ms == inf
    r = '<bad timespan (Inf)>';
  elseif ts.ms == -inf
    r = '<bad timespan (-Inf)>';
  else
    r = '<bad timespan>';
  end
  return;
end

compact = 1;
if nargin > 1
  if varargin{1}(1) == 'c'
    compact = 1;
  elseif varargin{1}(1) == 'v'
    % verbose mode
    compact = 0;
  else
    warning('warning unknown flag');
    compact = 1;
  end
end

[d h m s] = s2dhms(getcdfepochvalue(ts) / 1000);
if any([d h m s] < 0) 
  ns = '-';
  d = abs(d);
  h = abs(h);
  m = abs(m);
  s = abs(s);
else
  ns = '';
end

if ~compact
  if d > 0
    r = sprintf('%s%i d %i h %i m %g s', ns, d, h, m, s);
    return;
  elseif h > 0
    r = sprintf('%s%i h %i m %g s', ns, h, m, s);
    return;
  elseif m > 0
    r = sprintf('%s%i m %g s', ns, m, s);
    return;
  else
    r = sprintf('%s%g s', ns, s);
  end

else
  r = ns;
  padding = '';
  if d > 0
    r = sprintf('%s%d d', r, d);
    padding = ' ';
  end
  if h > 0
    r = sprintf('%s%s%d h', r, padding, h);
    padding = ' ';
  end
  if m > 0
    r = sprintf('%s%s%d m', r, padding, m);
    padding = ' ';
  end
  if s > 0
    r = sprintf('%s%s%g s', r, padding, s);
    padding = ' ';
  end  
  if isempty(r)
    r = '0 s';
  end
end


