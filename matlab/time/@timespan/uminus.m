function r = uminus(a)
%UMINUS  - Unary minus operator for TIMESPAN objects.
%
%   r = UMINUS(a)
%   r: TIMESPAN object
%   a: TIMESPAN object
%
% See TIMESPAN, ops/UMINUS.

r = a;
r.ms = uminus(a.ms);

