function r = subsasgn(a, s, b)
%SUBSASGN Subscripted assignment overloaded for TIMESPAN class.
%
% See ops/SUBSASGN.

if ~isa(a, 'timespan') & ~isempty(a)
  % Only override subsasgn when the target object is a timespan
  r = builtin('subsasgn', a, s, b);
  return
end

if isa(b, 'timespan')
  b_ms = b.ms;
elseif isempty(b)
  b_ms = [];
else
  b_ms = getcdfepochvalue(timespan(b));
end

r = a;
r.ms = subsasgn(r.ms, s, b_ms);
