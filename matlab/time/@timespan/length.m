function r = length(t)
%LENGTH LENGTH overloaded for TIMESPAN class.
%
% See LENGTH.

r = length(t.ms);

