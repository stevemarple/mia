function r = sign(ts)
%SIGN  Sign function overloaded for TIMESPAN objects.
%
% See elfun/SIGN.

r = sign(ts.ms);
