function r = gcd(a, b)
%GCD  Greatest common divisor
%
% g = GCD(a,b)
%
% g: greatest common divisor
% b, c: see spencfun/GCD.
% a: TIMESPAN
% a: TIMESPAN
%

r = timespan(gcd(a.ms, b.ms), 'ms');
