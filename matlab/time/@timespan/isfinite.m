function r = isfinite(t)
%ISFINITE  Indicate if a TIMESPAN is finite.
%
%   r = ISFINITE(t)
%   r: DOUBLE
%   t: TIMESPAN
%
%   If t is a matrix r is a matrix of the same size.
%
%   See also TIMESPAN.

r = isfinite(t.ms);
