function r = getcdfepochvalue(t)
%GETCDFEPOCHVALUE Return the value of a CDFEPOCH in ms from CDF epoch.
%
%   r = GETCDFEPOCHVALUE(t)
%   r: time in milliseconds since CDF epoch (0000-01-01 00:00:00)
%   t: CDFEPOCH

r = reshape([t.date], size(t));
