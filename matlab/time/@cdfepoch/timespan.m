function r = timespan(c)
%TIMESPAN Convert CDFEPOCH object(s) to TIMESPAN object(s).
%
%   t = TIMESPAN(c)
%   t: TIMESPAN object(s)
%   c: CDFEPOCH object(s)
%
% See also cdfepoch/CDFEPOCH, timespan/TIMESPAN.

r = timespan('cdfepoch', reshape([c.date], size(c)));
