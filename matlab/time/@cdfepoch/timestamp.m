function r = timestamp(c)
%TIMESTAMP Convert CDFEPOCH object(s) to TIMESTAMP object(s).
%
%   t = TIMESTAMP(c)
%   t: TIMESTAMP object(s)
%   c: CDFEPOCH object(s)
%
% See also cdfepoch/CDFEPOCH, timestamp/TIMESTAMP.

r = timestamp('cdfepoch', reshape([c.date], size(c)));
