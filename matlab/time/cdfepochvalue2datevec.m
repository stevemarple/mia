function r = cdfepochvalue2datevec(ms)
%CDFEPOCHVALUE2DATEVEC  Convert CDF epoch values to Matlab's DATEVEC format.
%
%   r = CDFEPOCHVALUE2DATEVEC(ms)
%   r: [nx6] DOUBLE, as [year month mday hour minute second]
%
% CDFEPOCHVALUE2DATEVEC Converts the date format used by CDF to same format
% that DATEVEC returns. Note that very large or very small values of ms
% cannot be converted. In such cases CDFEPOCHVALUE2DATEVEC treats the input
% and +INF or -INF, and returns a row filled will +INF or -INF.
%
% See also TIMESTAMP.

% use Matlab's datenum to get the number of whole days. Note that datenum
% gives the days since 0000-01-00, whereas the CDF epoch is
% 0000-01-01. Then add the number of seconds. Finally multiply everything
% by 1000 to convert to milliseconds

ms = reshape(ms, numel(ms), 1); % turn into column matrix

% Matlab's datevec can only handle numbers in the range -1.1e19 ms to 1.1e19
% ms, map anything outside of those value to +/- Inf.
ms(ms > 1.1e19) = inf;
ms(ms < -1.1e19) = -inf;

% find nans and inf values, map to 0 for now so that datevec call will succeed
nans = find(isnan(ms));
plus_infs = find(ms == inf);
neg_infs = find(ms == -inf);

ms(nans) = 0;
ms(plus_infs) = 0;
ms(neg_infs) = 0;

% if ms == inf | ms > 1.1e19
%   r = repmat(inf, [1 6]);
%   return
% elseif ms == -inf | ms < -1.1e19
%   r = repmat(-inf, [1 6]);
%   return
% elseif isnan(ms)
%   r = repmat(nan, [1 6]);
%   return
% end

% need to round towards -inf, so use floor
dse = floor(ms ./ 86400e3); % days since epoch
ms_partday = ms - (dse .* 86400e3); 
[tmp h m s] = s2dhms(ms ./ 1e3); % remainder always positive

% use Matlab's datevec to convert an integer day number. Don't ask it to
% handle the fraction of a day since it loses precision.
r = datevec(cdfepochdatenum + dse);
r(:, 4:6) = [h m s];

r(nans, :) = nan;
r(plus_infs, :) = +inf;
r(neg_infs, :) = -inf;
