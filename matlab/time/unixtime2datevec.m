function r = unixtime2datevec(t)
%UNIXTIME2DATEVEC Helper function for TIMESTAMP class. 
%
% See timestamp/DATEVEC.

if numel(t) ~= 1
  error('s must be scalar');
end

% need to round towards -inf, so use floor
dse = floor(t ./ 86400); % days since epoch
[tmp h m s] = s2dhms(t); % remainder always positive

% use Matlab's datevec to convert an integer day number. Don't ask it to
% handle the fraction of a day since it loses precision.
r = datevec(unixepochdatenum + dse);
r(4:6) = [h m s];
