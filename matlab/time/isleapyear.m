function r = isleapyear(year)
%ISLEAPYEAR  Calculate if year is a leap year or not.

r = logical(zeros(size(year)));

% map non-finite years to 1999 (ie not a leap year)
year(~isfinite(year)) = 1999;

year_mod4 = mod(year, 4);
year_mod100 = mod(year, 100);
year_mod400 = mod(year, 400);

% leap years are years which are divisible by 4 and not by 100, or are
% divisible by 400
r = (((year_mod4 == 0) & (year_mod100 ~= 0)) | (year_mod400 == 0));




