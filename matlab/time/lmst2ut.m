function [ut] = lmst2ut(lmst, longitude, d)
%LMST2UT  Convert local mean sidereal time to time in UT.
%
%   ut = LMST2UT(lmst, longitude, d)
%   ut: time in UT (TIMESTAMP)
%   lmst: local mean sidereal time (TIMESPAN)
%   longitude: longitude of the location, or a LOCATION object
%   d: date (TIMESTAMP)
%
%   The time portion of d is ignored.
%
%   See also UT2LMST, GMST, SIDEREALDAY.

% From "The Astronomical Almanac 1996, p B6, B7"


if strcmp(class(longitude), 'location')
  longitude = getgeolong(longitude);
end
if longitude > 180
  longitude = longitude -180
end

% Add west longitude (subtract east longitude)
% NB west longitude is negative!

t = lmst - (timespan(1, 'd') * longitude / 360);
% if t > 24h then subtract a day, if < 0h then add a day
oneDay = timespan(1, 'd');
if t > oneDay
  t = t - oneDay;
elseif t < timespan(0, 'h')
  t = t + oneDay;
end

% subtract Greenwich mean sidereal time at 0h UT
t = t - gmst(d);
if t < timespan(0, 's');
  t = t + oneDay;
end

% convert to the equivalent UT interval
meanSiderealDayToMeanSolarDay = 1.00273790935;

ut = t / meanSiderealDayToMeanSolarDay;

% convert to a time object
ut = ut + getdate(d);



