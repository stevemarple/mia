function r = horzcat(varargin)

ms = cell(size(varargin));
for n = 1:nargin
  if ~isa(varargin{n}, 'timestamp')
    error('all elements must be timestamps');
  end
  ms{n} = varargin{n}.ms;
end

r = varargin{1};
r.ms = horzcat(ms{:});

