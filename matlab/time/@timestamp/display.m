function display(t)
%DISPLAY  Display a TIMESTAMP object.

disp(' ');
disp([inputname(1),' = ' char(t)]);
disp(' ');
