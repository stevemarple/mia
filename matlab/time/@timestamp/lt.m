function r = lt(a, b)
%LT  Less than operator overloaded for TIMESTAMP class.
%
% See LT.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/lt must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = lt(a_ms, b_ms);


