function r = getdayofweek(t)
%GETDAYOFWEEK  Return the day of week for a TIMESTAMP object
%
%   r = GETDAYOFWEEK(t)
%   r: day of week (CHAR or CELL)
%   t: TIMESTAMP object(s)
%
%   If t is a matrix then r is a CELL matrix of the same size.
%
%   See also TIMESTAMP, DATEVEC.

if numel(t) == 1
  r = strftime(t, '%A');
else
  r = cell(size(t));
  for n = 1:prod(size(t))
    r{n} = strftime(t(n), '%A');
  end
end
