function r = unixtime(t)
%UNIXTIME  Convert  a TIMESTAMP object to number of seconds since 1970
%
%   r = UNIXTIME(t)
%   r: number of seconds since 00:00:00 1/1/1970
%   t: TIMESTAMP object


ue_ms =  getcdfepochvalue(timestamp([1970 1 1 0 0 0])); % unix epoch
t_ms = getcdfepochvalue(t);
r = (t_ms - ue_ms) ./ 1000;

