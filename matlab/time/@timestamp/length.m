function r = length(t)
%LENGTH LENGTH overloaded for TIMESTAMP class.
%
% See LENGTH.

r = length(t.ms);

