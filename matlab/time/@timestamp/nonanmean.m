function r = nonanmean(a, varargin)
%NONANMEAN MEAN value for TIMESTAMP objects, ignoring NANs.
%
% See stats/NONANMEAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/nonanmean must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/nonanmean must be numeric');
end

r_ms = nonanmean(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

