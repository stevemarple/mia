function r = isnan(t)
%ISNAN  Indicate if a TIMESTAMP is a NAN.
%
%   r = ISNAN(t)
%   r: DOUBLE
%   t: TIMESTAMP
%
%   If t is a matrix r is a matrix of the same size.
%
%   See also TIMESTAMP.

r = isnan(getcdfepochvalue(t));
