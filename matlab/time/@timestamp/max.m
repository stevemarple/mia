function varargout = max(a, varargin)
%MAX Max function overloaded for TIMESTAMP.
%
% See datafun/MAX.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/max must be a timestamp object');
elseif nargin == 2 
  if isa(varargin{1}, 'timestamp')
    % need the millisecond values
    varargin{1} = getcdfepochvalue(varargin{1});
  else
    error('second parameter to timestamp/max must be a timestamp object');
  end
end

n = nargout;
if n == 0
  n = 1;
end

[varargout{1:n}] = max(getcdfepochvalue(a), varargin{:});
varargout{1} = timestamp('cdfepoch', varargout{1});

