function [y,i] = sort(t, varargin)
%SORT  Sort TIMESTAMPs in ascending order.
%
% See datafun/SORT.

[y_ms,i] = sort(t.ms, varargin{:});
y = timestamp('cdfepoch', y_ms);
