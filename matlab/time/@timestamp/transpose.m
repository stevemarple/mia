function r = transpose(t)
%TRANSPOSE TRANSPOSE overloaded for TIMESTAMP class.
%
% See TRANSPOSE.

r = t;
r.ms = transpose(t.ms);

