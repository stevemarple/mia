function r = minus(a, b)
%MINUS  Addition operator (+) for TIMESTAMP class.
%
%   r = MINUS(a,b)
%   r = a+b (TIMESTAMP)
%   a: TIMESTAMP object(s)
%   b: TIMESPAN object(s)
%
%   See also TIMESTAMP, TIMESPAN.

if isa(a, 'timestamp') & isa(b, 'timespan')
  r = timestamp('cdfepoch', getcdfepochvalue(a) - getcdfepochvalue(b));

elseif isa(a, 'timestamp') & isa(b, 'timestamp')
  r = timespan(getcdfepochvalue(a) - getcdfepochvalue(b), 'ms');

else
  errormsg = sprintf('incorrect parameters (were %s, %s)', ...
    class(a), class(b));
  error(errormsg);
end

