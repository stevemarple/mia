function r = floor(a, b)
%FLOOR  Round TIMESTAMP objects down to nearest TIMESPAN boundary.
%
%    r = FLOOR(a, b)
%    r: TIMESTAMP, rounded down to smallest ts boundary
%    a: TIMESTAMP object to be rounded
%    b: TIMESTAMP object
%
%    See also TIMESTAMP, CEIL, FIX, ROUND.

if ~isa(a, 'timestamp') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);
r_ms = floor(a_ms ./ b_ms) .* b_ms;
r = timestamp('cdfepoch', r_ms);
