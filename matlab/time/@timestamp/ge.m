function r = ge(a, b)
%GE  Greater than or equal operator overloaded for TIMESTAMP class.
%
% See GE.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/ge must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = ge(a_ms, b_ms);


