function r = nonanmedian(a, varargin)
%NONANMEDIAN MEDIAN value for TIMESTAMP objects, ignoring NANs.
%
% See NONANMEDIAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/nonanmedian must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/nonanmedian must be numeric');
end

r_ms = nonanmedian(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

