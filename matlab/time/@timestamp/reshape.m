function r = reshape(t, varargin)
%RESHAPE RESHAPE overloaded for TIMESTAMP class.
%
% See RESHAPE.

r = t;
r.ms = reshape(r.ms, varargin{:});
