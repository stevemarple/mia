function [d, leap] = daysinmonth(t)
% DAYSINMONTH Return the number of days in a month. Indicate if leap year
%
%   [d leap] = DAYSINMONTH(t);
%   d: days in month
%   leap: LOGICAL inidcating if a leap year
%   t: TIMESTAMP object(s)
%
% See also TIMESTAMP

% d = zeros(size(t));
d = repmat(nan, size(t));
leap = logical(zeros(size(t)));

for n = 1:numel(t)
  if isvalid(t(n))
    [d(n) leap(n)] = daysinmonth(getyear(t(n)), getmonth(t(n)));
  end
end
