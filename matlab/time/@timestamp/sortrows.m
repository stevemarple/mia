function [r,i] = sortrows(a, varargin)
%SORTROWS  Sort rows in ascending order for TIMESTAMP arrays.
%
% See datafun/SORTROWS.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/sortrows must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/sortrows must be numeric');
end

[r_ms i] = sortrows(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

