function r = plus(a, b)
%PLUS  Addition operator (+) for TIMESTAMP class.
%
%   r = PLUS(a,b)
%   r = a+b (TIMESTAMP)
%   a: TIMESTAMP object(s)
%   b: TIMESPAN object(s)
%
%   See also TIMESTAMP, TIMESPAN.

if (isa(a, 'timestamp') & isa(b, 'timespan')) | ...
      (isa(a, 'timespan') & isa(b, 'timestamp'))
  r = timestamp('cdfepoch', getcdfepochvalue(a) + getcdfepochvalue(b));
else
  errormsg = sprintf('incorrect parameters (were %s, %s)', ...
    class(a), class(b));
  error(errormsg);
end

