function [s,rep] = strftime(t, formatstr, varargin)
%STRFTIME  Write a TIMESTAMP with control over format.
%
%   [r,rep] = STRFTIME(t, fstr, ...)
%   r: CHAR (or CELL matrix of CHARs if t is not scalar)
%   rep: CELL, list of replacements made
%   t: TIMESTAMP object
%   fstr: format string, see also unix manual page for strftime
%
% STRFTIME converts the date and time into a CHAR using a user-defined
% format string. The standard unix format specifiers are implemented (see
% list below). In addition some extra format specifiers are included. The
% optional rep return parameter provides a list of the format specifiers
% which were replaced.
%
% Implemented format specifiers: 
%   %%: %
%   %a: Abbreviated day of the week
%   %A: Full day of the week
%   %b: Abbreviated month name
%   %B: Full month name (January-December)
%   %d: day of month (1-31)
%   %H: hour (00-23)
%   %j: day of year (001-366)
%   %m: month (01-12)
%   %M: minutes (00-59)
%   %q: quarter (of year) (*)
%   %s: seconds since the unix epoch (1970-01-01 00:00:00)
%   %S: seconds (00-59)
%   %t: 'st', 'nd', 'rd' or 'th' dependent upon day number in month (*)
%   %y: year (yy)
%   %Y: year (YYYY)
%   %#: fractional seconds (eg 1ms -> '001') (*)
%
%   (*) Extension to unix strftime format specifier
%
% Additional parameter name/value pairs may be given to modify the standard
% behaviour:
%
%     additionalreplacements, STRUCT
%       Intended for functions which extend strftime and wish to use their
%       own format specifiers. The structure fieldnames should be single
%       characters corresponding to the new format specifiers, and the value
%       associated with the field is the replacement text, which should be a
%       CHAR. Tests for these additional replacements are made before
%       standard ones.
%
%   See also TIMESTAMP, timespan/STRFTIME.

if length(t) ~= 1
  s = cell(size(t));
  rep = cell(size(t));
  for n = 1:numel(t)
    % tn = t(n);
    % the above command doesn't call timestamp/subsref in some matlab
    % versions! Do so manually.    
    sr.type = '()';
    sr.subs = {n};
    tn = subsref(t, sr);
    [s{n} rep{n}] = feval(mfilename, tn, formatstr);
  end
  return
end

% form this point on t is scalar
rep = {};

defaults.additionalreplacements = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:end)});


unixEpoch = timestamp([1970 1 1 0 0 0]);

spos = 1; % position of next write in output string

dayName = {'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', ...
	   'Friday', 'Saturday'};

dName = {'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'};

monthName = {'January', 'February', 'March', 'April', 'May', 'June', ...
	     'July', 'August', 'September', 'October', 'November', 'December'};

mName = {'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', ...
	 'Oct', 'Nov', 'Dec'};
i = 1;
s = '';
if ~isvalid(t)
  % s = char(t);
  if t.ms == inf
      s = '<bad timestamp (Inf)>';
  elseif t.ms == -inf
    s = '<bad timestamp (-Inf)>';
  else
    s = '<bad timestamp>';
  end
  return;
end

% datevec can fail if the value is too large or too small, even though it is
% not +/inf.
dv = datevec(t);
if any(dv == inf)
  s = '<timestamp too large>';
  return
elseif any(dv == -inf)
  s = '<timestamp too small>';
  return
end

while i <= length(formatstr)
  switch formatstr(i)
   case '%'
    
    % look to next char
    i = i + 1;
    
    replacedchar = formatstr(i);
    if isfield(defaults.additionalreplacements, formatstr(i))
      tmp = getfield(defaults.additionalreplacements, formatstr(i));
      s = [s tmp];
      spos = spos + length(tmp);
    else

      switch formatstr(i)
	% percent
       case '%'
	s(spos) = '%';
	spos = spos + 1;
	
       case 'a'
	% Abbreviated day name
	dayNumber = 1 + mod(fix(datenum(t)) -2, 7);
	s = sprintf('%s%s', s, dName{dayNumber});
	spos = spos + length(dName{dayNumber});	  
	
	% Full day name
       case 'A'
	dayNumber = 1 + mod(fix(datenum(t)) -2, 7);
	s = sprintf('%s%s', s, dayName{dayNumber});
	spos = spos + length(dayName{dayNumber});
	
	% Abbreviated month name
       case 'b'
	s = sprintf('%s%s', s, mName{dv(2)});
	spos = spos + length(mName{dv(2)});
	
	% Full month name
       case 'B'
	s = sprintf('%s%s', s, monthName{dv(2)});
	spos = spos + length(monthName{dv(2)});
	
	% day dd
       case 'd'
	s = sprintf('%s%02d', s, dv(3));
	spos = spos + 2;
	
	% hour HH
       case 'H'
	s = sprintf('%s%02d', s, dv(4));
	spos = spos + 2;
	
	% day of year
       case 'j'
	s = sprintf('%s%03d', s, fix(getdayofyear(t)));
	spos = spos + 3;
	
	% month mm
       case 'm'
	s = sprintf('%s%02d', s, dv(2));
	spos = spos + 2;
	
	% minute MM
       case 'M'
	s = sprintf('%s%02d', s, dv(5));
	spos = spos + 2;
	
	% quarter (of year) q
       case 'q'
	s = sprintf('%s%01d', s, fix(dv(2)/4)+1);
	spos = spos + 1;
	
	% seconds since unix epoch
       case 's'
	tmp = int2str((t - unixEpoch)/timespan(1,'s'));
	s = [s tmp];
	spos = spos + length(tmp);
	
	% second SS
       case 'S'
	s = sprintf('%s%02d', s, fix(dv(6)));
	spos = spos + 2;

	% 'st', 'nd', 'rd' or 'th' dependent upon mday number
       case 't'
	switch dv(3)
	 case {1, 21, 31}
	  nth = 'st';
	 case {2, 22}
	  nth = 'nd';
	 case {3, 23};
	  nth = 'rd';
	 otherwise
	  nth = 'th';
	end
	s = [s nth];
	spos = spos + 2;
	
	% year yy
       case 'y'
	s = sprintf('%s%02d', s, rem(dv(1), 100));
	spos = spos + 2;
	
	% year YYYY
       case 'Y'
	s = sprintf('%s%04d', s, dv(1));
	spos = spos + 4;
	
       case '#'
	% fractional seconds (as milliseconds)
	s = sprintf('%s%03d', s, round(rem(t.ms, 1000)));
	spos = spos + 3;

       otherwise
	warning(sprintf(['unknown format specifier - ' ...
			 'ignoring (was %%%s)'], formatstr(i)));
        replacedchar = '';
      end
      
    end
    if ~isempty(replacedchar)
      % remember which characters were replaced
      rep{end+1} = replacedchar;
    end
    
   otherwise
    s(spos) = formatstr(i);
    spos = spos + 1;
  end
  i = i + 1; % next char in format string
end

if nargout > 1
  rep = unique(rep);
end
