function r = subsasgn(a, s, b)
%SUBSASGN Subscripted assignment overloaded for TIMESTAMP class.
%
% See ops/SUBSASGN.
if ~isa(a, 'timestamp') & ~isempty(a)
  % Only override subsasgn when the target object is a timestamp
  r = builtin('subsasgn', a, s, b);
  return
end
  
if isa(a, 'timestamp')
  r = a;
elseif isempty(a)
  % this case happens when a variable does not exist and it is assigned
  % to with a subscript
  r = timestamp;
else
  class(a)
  a
  b
  s
  % must have go here because b is a timestamp, even though a
  % isn't. Should all elements in a be converted to timestamp? Not sure
  % right now. Wait until this case happens.
  error(['assigning timestamp to non-timestamp array, do not know what to' ...
	 ' do']);
end

if isa(b, 'timestamp')
  b_ms = getcdfepochvalue(b);
elseif isempty(b)
  % delete element(s)
  a_ms = getcdfepochvalue(a);
  b_ms = [];

  r_ms = subsasgn(a_ms, s, b_ms);
  
  r.ms = r_ms;
  return
else
  b_ms = getcdfepochvalue(timestamp(b));
end



% attempt to resize the ms matrix such that newly created times have the
% value NaN (ie are not valid)
if numel(s) == 1 & strcmp(s(1).type, '()') & ~isempty(a)
  a_ms_size = size(a.ms);

  % compute the minimum size we know the new object will have
  if length(s.subs) == 1
    % single subscript
    if length(a_ms_size) > 2
      error('Attempt to grow array along ambiguous dimension.');
    end
    
    if s(1).subs{1} <= numel(a.ms)
      % resize not needed
      newminsz = a_ms_size;
    elseif a_ms_size(1) > 1 && a_ms_size(2) <= 1
      newminsz = [s(1).subs{1} 1];
    elseif a_ms_size(1) <= 1 && a_ms_size(2) >= 1
      newminsz = [1 s(1).subs{1}];
    else
      % can only extend with a single subscript if a is a vector (not
      % matrix)
      error('In an assignment  A(I) = B, a matrix A cannot be resized.');
    end
  else
    % multi-dimensional subscripting
    newminsz = zeros(1, numel(s.subs));
    for n = 1:length(newminsz)
      if strcmp(s.subs{n}, ':')
        newminsz(n) = max(size(a.ms, n), size(b.ms, n));
      else
        newminsz(n) = max(s.subs{n});
      end
    end
  end
  
  % current size
  sz = size(r.ms);
  sz((end+1):length(newminsz)) = 1; % include any extra dimensioning needed

  % new size
  newsz = max(newminsz, sz);
  
  if any(newsz > sz)
    % result is in a bigger matrix so create a blank matrix
    % r_ms = repmat(nan, newsz);
    
    % now copy the existing matrix into the blank one
    s2.type = '()';
    s2.subs = cell(1, length(sz));
    for n = 1:length(sz)
      s2.subs{n} = 1:sz(n);
    end
    
    r.ms = subsasgn(repmat(nan, newsz), s2, r.ms);
    
  end
end

r.ms = subsasgn(r.ms, s, b_ms);

