function r = ipermute(t, order)
%IPERMUTE  Ipermute array dimensions of TIMESTAMP objects.
%
% See elmat/IPERMUTE.

r = t;
r.ms = ipermute(r.ms, order);
