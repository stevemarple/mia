function r = gt(a, b)
%GT  Greater than operator overloaded for TIMESTAMP class.
%
% See GT.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/gt must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = gt(a_ms, b_ms);


