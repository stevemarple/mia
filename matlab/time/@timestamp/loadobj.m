function r = loadobj(t)
%LOADOBJ  Load filter for TIMESTAMP class.

if isstruct(t)
  % old timestamp class
  r = timestamp(t);
else
  r = t;
end  
