function r = isvalid(t)
%ISVALID  Indicate if a TIMESTAMP is valid.
%
%   r = ISVALID(t)
%   r: DOUBLE
%   t: TIMESTAMP
%
%   If t is a matrix r is a matrix of the same size.
%
%   See also TIMESTAMP.

ms = getcdfepochvalue(t);
r = isfinite(ms) & ms >= 0;


