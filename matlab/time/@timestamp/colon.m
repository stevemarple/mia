function r = colon(a, varargin)
%COLON  Colon operator (:) for TIMESTAMP class.
%
%   See COLON.

if nargin ~= 3
  error('timestamp/colon requires 3 parameters');
end

b = varargin{1};
c = varargin{2};

if numel(a) ~= 1 | numel(b) ~= 1 | numel(c) ~= 1 
  error('all parameters must be scalar')
elseif ~isa(a, 'timestamp')
  error('first parameter must be a timestamp');
elseif ~isa(b, 'timespan')
  error('second parameter must be a timespan');
elseif ~isa(c, 'timestamp')
  error('third parameter must be a timestamp');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);
c_ms = getcdfepochvalue(c);

r = timestamp('cdfepoch', a_ms:b_ms:c_ms);
