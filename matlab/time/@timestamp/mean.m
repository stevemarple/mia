function r = mean(a, varargin)
%MEAN Average or mean value for TIMESTAMP objects.
%
% See datafun/MEAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/mean must be a timestamp object');
elseif nargin == 2 && ~isnumeric(varargin{1})
  error('second parameter to timestamp/mean must be numeric');
end

r_ms = mean(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

