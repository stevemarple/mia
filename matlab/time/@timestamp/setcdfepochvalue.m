function r = setcdfepochvalue(t, ms)
%SETCDFEPOCHVALUE  Set a TIMESTAMP object to date as CDF epoch units.
%
%   r = SETCDFEPOCHVALUE(t, ms)
%   r: TIMESTAMP
%   t: TIMESTAMP
%   ms: CDF epoch value (duration since 0000-01-01 00:00:00 in milliseconds)
%
% See also TIMESTAMP.

r = t;
r.ms = ms;
