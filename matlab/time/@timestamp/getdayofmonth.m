function r = getdayofmonth(t)
%GETDAYOFMONTH  Return the day of month for a TIMESTAMP object
%
%   r = GETDAYOFMONTH(t)
%   r: day of month (DOUBLE) (1 <= dom <= 31)
%   t: TIMESTAMP object(s)
%
%   If t is a matrix then r is a matrix of the same size.
%
%   See also TIMESTAMP, DATEVEC.

r = datevec(t, 3);
