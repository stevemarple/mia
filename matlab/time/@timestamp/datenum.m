function r = datenum(t)
%DATENUM  Return TIMESTAMP object as a DATENUM scalar.
%
%   r = DATENUM(t)
%   r: DOUBLE (same size as t)
%   t: TIMESTAMP object(s)
% 
%   See also DATENUM.


ms = getcdfepochvalue(t);
r = (ms ./ 86400e3) + cdfepochdatenum;
