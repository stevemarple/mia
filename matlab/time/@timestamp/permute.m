function r = permute(t, order)
%PERMUTE  Permute array dimensions of TIMESTAMP objects.
%
% See elmat/PERMUTE.

r = t;
r.ms = permute(r.ms, order);
