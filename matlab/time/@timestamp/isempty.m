function r = isempty(t)
%ISEMPTY ISEMPTY overloaded for TIMESTAMP class.
%
% See elmat/ISEMPTY.

r = isempty(t.ms);

