function r = ceil(a, b)
%CEIL  Round TIMESTAMP objects upwards to nearest TIMESPAN boundary.
%
%    r = CEIL(a, b)
%    r: TIMESTAMP, rounded up to nearest ts boundary
%    a: TIMESTAMP object to be rounded
%    b: TIMESTAMP object
%
%    See also TIMESTAMP, FIX, FLOOR, ROUND.

if ~isa(a, 'timestamp') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);
r_ms = ceil(a_ms ./ b_ms) .* b_ms;
r = timestamp('cdfepoch', r_ms);
