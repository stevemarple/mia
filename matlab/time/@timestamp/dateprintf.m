function s = dateprintf(st, et)
%DATEPRINTF  Print start and end times in a neat, concise manner.
%
%   s = DATEPRINTF(st, et)
%   s: formatted string
%   st: start time (TIMESTAMP)
%   et: end time (TIMESTAMP)
%
%   See also STRFTIME.

ut = ' UT';

if nargin == 1 & length(st) == 1
  s = strftime(st, ['%Y-%m-%d %H:%M:%S' ut]);
  return
end

if numel(st) ~= 1 & numel(et) ~= 1
  error('st and et must be scalars');
end

if getdate(st) == getdate(et)
  % same day
  s = [strftime(st, '%Y-%m-%d %H:%M:%S - ') strftime(et, ['%H:%M:%S' ut])];

else
  s = [strftime(st, ['%Y-%m-%d %H:%M:%S' ut ' - ']) ...
       strftime(et, ['%Y-%m-%d %H:%M:%S' ut])];
end	








