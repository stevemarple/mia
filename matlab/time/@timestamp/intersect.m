function varargout = intersect(varargin)
%INTERSECT  Test if TIMESTAMPS intersect
%
%   [r ia ib] = INTERSECT(a, b)
%
%   Standard INTERSECT, overloaded for TIMESTAMP, see ops/INTERSECT.
%
%
%   r = INTERSECT(a1, a2, b1, b2);
%   r: LOGICAL scalar or matrix
%   a1: start time (inclusive) of range 1 (TIMESTAMP)
%   a2: end time (exclusive) of range 1 (TIMESTAMP)
%   b1: start time (inclusive) of range 2 (TIMESTAMP)
%   b2: end time (exclusive) of range 2 (TIMESTAMP)
%
%   Test if the ranges defined by a1 to a2 and b1 to b2 intersect.


if nargin == 2 | nargin == 3
  % normal intersect function
  if ~isa(varargin{1}, 'timestamp') | ~isa(varargin{2}, 'timestamp')
    error('a and b must be timestamps');
  end

  % convert timestamps to scalars
  varargin{1} = getcdfepochvalue(varargin{1});
  varargin{2} = getcdfepochvalue(varargin{2});
  [r varargout{2} varargout{3}] = intersect(varargin{:});

  % convert r back to timestamp
  varargout{1} = timestamp('cdfepoch', r);
  
elseif nargin == 4
  varargout{1} = intersect_ranges(varargin{:});

else
  error('incorrect parameters');
end

function r = intersect_ranges(a1, a2, b1, b2)
if ~isa(a1, 'timestamp') | ~isa(a2, 'timestamp') | ...
      ~isa(b1, 'timestamp') | ~isa(b2, 'timestamp')
  error('All objects must be of class timestamp');
end

a1sz = size(a1);
if ~isequal(a1sz, size(a2)) | ~isequal(a1sz, size(b1)) | ...
      ~isequal(a1sz, size(b2))
  error('All objects must be the same size');
end


% r = (b1 >= a1 & b1 < a2) | (b2 > a1 & b2 <= a2) | ...
%    (a1 >= b1 & a2 <= b2) | (b1 >= a1 & b2 <= a2);

a1_ms = getcdfepochvalue(a1);
a2_ms = getcdfepochvalue(a2);
b1_ms = getcdfepochvalue(b1);
b2_ms = getcdfepochvalue(b2);

r = (b1_ms >= a1_ms & b1_ms < a2_ms) | (b2_ms > a1_ms & b2_ms <= a2_ms) | ...
    (a1_ms >= b1_ms & a2_ms <= b2_ms) | (b1_ms >= a1_ms & b2_ms <= a2_ms);

