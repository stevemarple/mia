function r = getmonth(t)
%GETMONTH  Return the month of a TIMESTAMP object.
%
%   r = GETMONTH(t)
%   r: month
%   t: TIMESTAMP object(s)
%
%
%   r is the same size as t.
%
%   See also TIMESTAMP.

r = datevec(t, 2);
