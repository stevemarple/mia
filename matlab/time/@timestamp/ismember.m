function varargout = ismember(a, s, varargin)
%ISMEMBER Overloaded ISMEMBER function for TIMESTAMP class
%
% See ISMEMBER.

a_ms = getcdfepochvalue(a);
s_ms = getcdfepochvalue(s);
n = nargout;
if n == 0
  n = 1;
end

[varargout{1:n}] = ismember(a_ms, s_ms, varargin{:});
