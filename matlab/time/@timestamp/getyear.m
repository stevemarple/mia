function r = getyear(t)
%GETYEAR  Return the year of a TIMESTAMP object.
%
%   r = GETYEAR(t)
%   r: year
%   t: TIMESTAMP object(s)
%
%
%   r is the same size as t.
%
%   See also TIMESTAMP.

r = datevec(t, 1);
