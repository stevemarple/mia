function r = fix(a, b)
%FIX  Round TIMESTAMP objects down to TIMESPAN boundary closest to zero.
%
%    r = FIX(a, b)
%    r: TIMESTAMP, rounded to ts boundary closest to TIMESPAN(0, 's')
%    a: TIMESTAMP object to be rounded
%    b: TIMESTAMP object
%
%    See also TIMESTAMP, CEIL, FLOOR, ROUND.

if ~isa(a, 'timestamp') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);
r_ms = fix(a_ms ./ b_ms) .* b_ms;
r = timestamp('cdfepoch', r_ms);
