function t = timestamp(varargin)
%TIMESTAMP  Constructor for TIMESTAMP class.
%
%   t = TIMESTAMP
%   Default constructor, returning a null timestamp. Use TIMESTAMP('now')
%   to get current date/time.
%
%   t = TIMESTAMP(a)
%   Copy constructor
%
%   t = TIMESTAMP('yesterday')  % midnight yesterday
%   t = TIMESTAMP('today')      % midnight today
%   t = TIMESTAMP('now')        % current time
%   t = TIMESTAMP('tomorrow')   % midnight tomorrow
%   t = TIMESTAMP('bad')        % invalid/null timestamp
%
%   t = TIMESTAMP([])
%   Create a null TIMESTAMP
%
%   t = TIMESTAMP(c)
%   t: TIMESTAMP same size as c
%   c: CELL array, each cell element is used to assign a value to the
%   corresponding output element in t. Use TIMESTAMP(CELL(m,n,...)) to
%   create a matrix of null TIMESTAMPs
%
%   t = TIMESTAMP([yyyy mm dd hh mm ss])
%   t = TIMESTAMP([n x 6 NUMERIC)
%   construct from a DATEVEC-style timestamp matrix. If the matrix has
%   multiple rows then mutliple TIMESTAMPs are created.
%
%   t = timestamp(year, dayofyear, hh, mm, ss)
%   construct from year and day of year (1->366)
%
% Invalid TIMESTAMPs can be tested with timestamp/ISVALID.
%
% See also DATEVEC, timestamp/CHAR, timestamp/DATENUM, timestamp/DATEVEC,
% timestamp/DISPLAY, timestamp/EQ, timestamp/GE, timestamp/GETDATE,
%  timestamp/GETDAYOFYEAR, timestamp/GT, timestamp/JULIANDATE,
%  timestamp/LE, TIMESPAN.

% When a function call includes both timestamps and timespans have it call
% the timestamp version. This means, for example timestamp + timespan
% addition needs to be handled in only one place.
superiorto('timespan');

% milliseconds since CDF epoch (0000-01-01)
t.ms = nan;

cls = 'timestamp';
t = class(t, cls);


if nargin == 0
  % default constructor
  ; % leave as bad timestamp
  
elseif nargin == 1 & isa(varargin{1}, cls)
  % copy constructor
  t.ms = varargin{1}.ms;
  
elseif nargin == 1 & isstruct(varargin{1}) 
  if isequal(fieldnames(varargin{1}), {'ms'})
    t_ms = reshape([varargin{1}(:).ms], size(varargin{1}));
    t = setcdfepochvalue(t, t_ms);

  elseif isequal(fieldnames(varargin{1}), {'dv'})
    t_ms = datevec2cdfepochvalue(varargin{1});
    t = setcdfepochvalue(t, t_ms);

  else
    error('incorrect struct');
  end

elseif nargin == 1 & iscell(varargin{1})
  t_ms = datevec2cdfepochvalue(varargin{1});
  t = setcdfepochvalue(t, t_ms);
  
elseif nargin == 1 & isempty(varargin{1})
  ; % null timestamp
  
elseif nargin == 1 & isnumeric(varargin{1}) & size(varargin{1}, 2) == 6
  t.ms = zeros(size(varargin{1}, 1), 1);
  for n = 1:numel(t.ms)
    t.ms(n) = datevec2cdfepochvalue(varargin{1}(n, :));
  end
  
elseif nargin == 1 & ischar(varargin{1})
  str = varargin{1};
  if strcmp(str, 'now')
    t.ms = round((now - 1) * 86400e3);
  elseif strcmp(str, 'today')
    t.ms = (fix(now) - 1) * 86400e3;
  elseif strcmp(str, 'yesterday')
    t.ms = (fix(now) - 2) * 86400e3;
  elseif strcmp(str, 'tomorrow')
    t.ms = (fix(now)) * 86400e3;
  elseif strcmp(str, 'bad')
    ; % null timestamp
  elseif any(strcmp(str, {'inf' '+inf'}))
    t.ms = inf;
  elseif strcmp(str, '-inf')
    t.ms = -inf;
    
  elseif strcmp(str([3 6 9]), ':: ')
    
    % try format for '00:00:00 [day_of_month] 21 Oct[ober] 1997...'
    % where [] used to denote optional parts
    
    % NB reading without a field width means we lose one char when the
    % end char is a space
    err = 0;
    len = length(str);
    [hms count message ind] = sscanf(str(1:8), '%2d:%2d:%2d'); % H M S
    err = ~isempty(message);

    % scan through day of week
    while ~(str(ind) >= '0' & str(ind) <= '9')
      ind = ind + 1;
    end
    
    [d count message ind2] = sscanf(str(ind:len), '%d'); % day of month
    ind = ind + ind2 - 1; 
    err = err | (count ~= 1);

    [m count message ind2] = sscanf(str(ind:len), '%s', 1); % month
    ind = ind + ind2; 
    err = err | (count ~= 1);

    [y count message ind] = sscanf(str(ind:len), '%4d'); % year
    err = err | (count ~= 1);

    switch lower(m(1:3))
      case 'jan'
	m = 1;
      case 'feb';
	m = 2;
      case 'mar';
	m = 3;
      case 'apr';
	m = 4;
      case 'may';
	m = 5;
      case 'jun';
	m = 6;
      case 'jul';
	m = 7;
      case 'aug';
	m = 8;
      case 'sep';
	m = 9;
      case 'oct';
	m = 10;
      case 'nov';
	m = 11;
      case 'dec';
	m = 12;
      otherwise
	err = 1;
    end

    if ~err
      t.ms = datevec2cdfepochvalue([y m d hms(:)']);
    end

  elseif length(str) == 10 & strcmp(str([5 8]), '--') & ...
	all(str([1:4 6:7 9:10]) >= '0' & ...
	    str([1:4 6:7 9:10]) <= '9')
    % match ISO string
    % 'YYYY-mm-dd' (format)
    %  1234567890 (char positions)
    t.ms = datevec2cdfepochvalue([str2num(str(1:4)) str2num(str(6:7)) ...
		    str2num(str(9:10)) 0 0 0]);

  elseif length(str) == 19 & ...
	strcmp(str([5 8 14 17]), '--::') & ...
	(str(11) == ' ' || str(11) == 'T') & ...
	all(str([1:4 6:7 9:10 12:13 15:16 18:19]) >= '0' & ...
	    str([1:4 6:7 9:10 12:13 15:16 18:19]) <= '9')
    % match ISO string
    % 'YYYY-mm-dd HH:MM:SS' (format)
    %  12345678901234567890 (char positions)
    t.ms = datevec2cdfepochvalue([str2num(str(1:4)) str2num(str(6:7)) ...
		    str2num(str(9:10)) str2num(str(12:13)) ...
		    str2num(str(15:16)) str2num(str(18:19))]);
    
  elseif length(str) == 22 & ...
	strcmp(str([5 8 14 17 20:22]), '--::+00') & ...
	(str(11) == ' ' || str(11) == 'T') & ...
	all(str([1:4 6:7 9:10 12:13 15:16 18:19]) >= '0' & ...
	    str([1:4 6:7 9:10 12:13 15:16 18:19]) <= '9')
    % match ISO string
    % 'YYYY-mm-dd HH:MM:SS' (format)
    %  12345678901234567890 (char positions)
    t.ms = datevec2cdfepochvalue([str2num(str(1:4)) str2num(str(6:7)) ...
		    str2num(str(9:10)) str2num(str(12:13)) ...
		    str2num(str(15:16)) str2num(str(18:19))]);
    
    % could try other formats
  else
    ; % unknown format, leave as null timestamp
  end
  
elseif nargin == 2 & strcmp(varargin{1}, 'cdfepoch')
  t = setcdfepochvalue(t, varargin{2});

elseif nargin == 2 & strcmp(varargin{1}, 'unixtime')
  % unixtime is seconds since 1970
  t = timestamp([1970 1 1 0 0 0]) + timespan(varargin{2}, 's');
  % t = setcdfepochvalue(t, varargin{2});

elseif nargin == 5
  % year, dayofyear, hour, minute, second
  
  year = varargin{1};
  [month dom] = dayofyear2dayofmonth(year, varargin{2});
  if numel(year) == 1
    t.ms = datevec2cdfepochvalue([year month dom varargin{3:5}]);
  else
    % vectorised version
    c = repmat({repmat(nan, [1 6])}, size(year));
    for n = 1:numel(c)
      c{n}(1) = year(n);
      c{n}(2) = month(n);
      c{n}(3) = dom(n);
      c{n}(4) = varargin{3}(n);
      c{n}(5) = varargin{4}(n);
      c{n}(6) = varargin{5}(n);
    end
   
    t.ms = datevec2cdfepochvalue(c);
  end
  
else
  varargin{:}
  error('incorrect parameters');
end


  





