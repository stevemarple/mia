function r = ne(a, b)
%NE  Non-equality operator overloaded for TIMESTAMP class.
%
% See NE.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/ne must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = ne(a_ms, b_ms);


