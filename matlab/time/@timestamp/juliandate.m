function jd = juliandate(t)
%JULIANDATE  Convert TIMESTAMP object(s) to the Julian date.
%
%   r = JULIANDATE(t)
%   r: Julian date
%   t: TIMESTAMP object(s) [timezone assumed to be UT]
%
%   See also TIMESTAMP.

% this is the offset between Matlab's serial date number (where 1
% corresponds to 1-Jan-0000) and the Julian date.
% Julian date starts 12:00:00 1/1/4713 BC and is Julian day 0.
% A useful calculator between UTC and julian date can be found at 
% http://www.fourmilab.ch/cgi-bin/uncgi/Earth

offset = 1721058.5;
jd = datenum(t) + offset;

