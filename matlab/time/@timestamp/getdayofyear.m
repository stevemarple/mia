function r = getdayofyear(t)
%GETDAYOFYEAR  Return the day of year for a TIMESTAMP object
%
%   r = GETDAYOFYEAR(t)
%   r: day of year (DOUBLE) (1 <= doy <= 366)
%   t: TIMESTAMP object(s)
%
%   If t is a matrix then r is a matrix of the same size.
%
%   See also TIMESTAMP.

% use Matlab's datenum to return a day number for day in question and
% reference that from the datenum for the start of the year

% add 1 because 1/1/year is day 1
r = floor(datenum(t) - datenum(getyear(t), 1, 1) + 1);

