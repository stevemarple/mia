function r = median(a, varargin)
%MEDIAN Average or median value for TIMESTAMP objects.
%
% See datafun/MEDIAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/median must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/median must be numeric');
end

r_ms = median(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

