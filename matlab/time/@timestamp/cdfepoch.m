function r = cdfepoch(t)
%CDFEPOCH  Convert TIMESTAMP to CDFEPOCH class.
%
%   r = CDFEPOCH(t)
%   r: CDFEPOCH object(s)
%   t: TIMESTAMP object(s)
%
% See also cdfepoch/CDFEPOCH.



r = cdfepoch(datenum(t));
