function r = numel(t)
%NUMEL NUMEL overloaded for TIMESTAMP class.
%
% See NUMEL.

r = numel(t.ms);
