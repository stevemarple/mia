function r = diff(t, varargin)

r = timespan(diff(getcdfepochvalue(t), varargin{:}), 'ms');
