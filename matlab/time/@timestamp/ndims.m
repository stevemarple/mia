function r = ndims(t)
%NDIMS NDIMS overloaded for TIMESTAMP class.
%
% See NDIMS.

r = ndims(t.ms);

