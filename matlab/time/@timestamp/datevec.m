function r = datevec(t, varargin)
%DATEVEC  Return TIMESTAMP as a DATEVEC format matrix.
%
%   r = DATEVEC(t)
%   r: DOUBLE vector ([years, months, days, hours, minutes, seconds]) 
%   t: TIMESTAMP object
%
%   If t is non-scalar then the nubmer of rows in r will match the number
%   of SIZE of t.
%
%   To return only some columns in r
%   a = DATEVEC(t, n)
%   a: a = r(n) (i.e. n==1, a == years etc)
%   n: matrix with all elements in range 1:6
%   t: TIMESTAMP object
%
%   When returning part of the datevec r is RESHAPEd to the same size as t
%   if n is scalar.
%
% See also timefun/DATEVEC.


r = cdfepochvalue2datevec(getcdfepochvalue(t));
switch nargin
 case 1
  ; % do nothing
 
 case 2
  n = varargin{1};
  % check that n is in 1:6
  if any(~ismember(varargin{1}, 1:6))
    error('n must be in range 1:6');
  end
  r = r(:, n);
  
  if numel(n) == 1
    % Only want one part of the datevec, make it the same shape as
    % t. This preserves previous behaviour
    r = reshape(r ,size(t));
  end
  
 otherwise
  error('incorrect number of parameters');
end
  
return

cdfe_dn = cdfepochdatenum;

if nargin == 1 
  % if numel(t) ~= 1
  %   error('Complete datevec only available when t is scalar');
  % end
  

  % r = unixtime2datevec(unixtime(t));
  r = cdfepochvalue2datevec(getcdfepochvalue(t));

elseif nargin == 2
  if numel(t) == 1
    % tmp = unixtime2datevec(unixtime(t));
    tmp = cdfepochvalue2datevec(getcdfepochvalue(t));
    r = tmp(varargin{1});
        
  elseif numel(varargin{1}) == 1
    r = zeros(size(t));
    for n = 1:numel(t)
      % tmp = unixtime2datevec(unixtime(t));
      tmp = cdfepochvalue2datevec(getcdfepochvalue(t(n)));
      r(n) = tmp(varargin{1});
    end
  else
    error('t or n must be scalar');
  end
end
