function r = round(a, b)
%ROUND  Round TIMESTAMP objects to nearest TIMESPAN boundary.
%
%    r = ROUND(a, b)
%    r: TIMESTAMP, rounded to nearest ts boundary
%    a: TIMESTAMP object to be rounded
%    b: TIMESTAMP object
%
%    See also TIMESTAMP, CEIL, FIX, FLOOR.

if ~isa(a, 'timestamp') | ~isa(b, 'timespan')
  error('incorrect parameters');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);
r_ms = round(a_ms ./ b_ms) .* b_ms;
r = timestamp('cdfepoch', r_ms);
