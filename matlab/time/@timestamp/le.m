function r = le(a, b)
%LE  Less than or equal operator overloaded for TIMESTAMP class.
%
% See LE.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/le must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = le(a_ms, b_ms);


