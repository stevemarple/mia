function r = eq(a, b)
%EQ  Equality operator overloaded for TIMESTAMP class.
%
% See EQ.

if ~isa(a, 'timestamp') | ~isa(b, 'timestamp')
  error('parameters to timestamp/eq must be timestamp objects');
end

a_ms = getcdfepochvalue(a);
b_ms = getcdfepochvalue(b);

r = eq(a_ms, b_ms);


