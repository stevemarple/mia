function r = ctranspose(t)
%CTRANSPOSE CTRANSPOSE overloaded for TIMESTAMP class.
%
% See CTRANSPOSE.

r = t;
r.ms = ctranspose(t.ms);

