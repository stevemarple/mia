function [r, nm] = getstartofmonth(t)
%GETSTARTOFMONTH Return the date of the start of the month
%
%   [r nm] = GETSTARTOFMONTH(t)
%   r: start of the month (TIMESTAMP)
%   nm: start of the next month
%   t: a date within the month (TIMESTAMP)
%
% GETSTARTOFMONTH returns the start of the month, and optionally the
% start of the next month. If t is non-scalar than r and nm are the same
% size as t.
%
% See also timestamp/DAYSINMONTH.

% r = repmat(timestamp('bad'), size(t));

r = t;

if nargout > 1
  nm = r;
end

if isempty(t)
  return
end

t_n = t;
tmp = t;
tmp.ms = 0; % make scalar

for n = 1:numel(t)
  t_n.ms = t.ms(n);
  dv = datevec(t_n);
  r.ms(n) = datevec2cdfepochvalue([dv(1:2) 1 0 0 0]);
  
  if nargout > 1
    tmp.ms = r.ms(n);
    q = tmp + timespan(daysinmonth(dv(1), dv(2)), 'd');
    nm.ms(n) = q.ms;    
  end
end

