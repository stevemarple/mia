function r = isfinite(t)
%ISFINITE  Indicate if a TIMESTAMP is finite.
%
%   r = ISFINITE(t)
%   r: DOUBLE
%   t: TIMESTAMP
%
%   If t is a matrix r is a matrix of the same size.
%
%   See also TIMESTAMP.

r = isfinite(getcdfepochvalue(t));
