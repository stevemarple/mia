function r = times(a, b)
%TIMES  Array multliplication (.*) operator for TIMESTAMP class.
%
%   TIMESTAMP objects cannot be multiplied, it is not a sensible
%   operation. Any attempt to do so will result in an error.
%
%   See also TIMESTAMP.

error('".*" is not a sensible operation for timestamp');

