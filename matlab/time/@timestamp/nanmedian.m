function r = nanmedian(a, varargin)
%NANMEDIAN MEDIAN value for TIMESTAMP objects, ignoring NANs.
%
% See stats/NANMEDIAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/nanmedian must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/nanmedian must be numeric');
end

r_ms = nanmedian(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

