function r = rem(t, ts)
%REM  Return the remainder part of a TIMESTAMP.
%
%   r = REM(t, ts)
%   r: difference between a t and FLOOR(t, ts) (TIMESPAN)
%   t: TIMESTAMP
%   ts: TIMESPAN
%
% See also TIMESTAMP, FLOOR.

if(ts < timespan(0, 's'))
  error('Please use a postive timespan');
end

r = t - floor(t,ts);

