function s = char(t)
%CHAR  Convert TIMESTAMP object to char representation.
%
%   s = CHAR(t)
%   s: string
%   t: TIMESTAMP object
%
%   See also TIMESTAMP.

if numel(t) ~= 1
  s = matrixinfo(t);
  return;
end

% from here on t is scalar
if rem(getcdfepochvalue(t), 1000) ~= 0
  % has milliseconds, so print them
  s = strftime(t, '%H:%M:%S.%# %d %B %Y');
else
  % whole seconds, so don't bother with milliseconds
  s = strftime(t, '%H:%M:%S %d %B %Y');
end
