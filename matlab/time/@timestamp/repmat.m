function r = repmat(t, varargin)
%REPMAT REPMAT overloaded for TIMESTAMP class.
%
% See REPMAT.

r = t;
r.ms = repmat(t.ms, varargin{:});
