function h = plot(varargin)
%PLOT Overloaded plot function for TIMESTAMP
%
% TIMESTAMP values are automatically converted to millisceonds since CDF
% epoch. If the X axis label is an empty string then TIMETICK2 is called to
% label it appropriately.
%
% See PLOT.

for n = 1:numel(varargin)
  if isa(varargin{n}, 'timestamp')
    varargin{n} = getcdfepochvalue(varargin{n});
  end
end

% Now call plot again, hopefully not recursively!
h = plot(varargin{:});

if ~isempty(h)
  % plotted something
  ah = get(h(1), 'Parent');
  xh = get(ah, 'XLabel');
  
  if strcmp(get(xh, 'String'), '')
    % no label on X axis
    timetick2(ah, 'class', 'timestamp');
  end
end
