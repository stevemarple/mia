function r = size(t, varargin)
%SIZE SIZE overloaded for TIMESTAMP class.
%
% See elmat/SIZE.
r = size(t.ms, varargin{:});

