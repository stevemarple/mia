function r = nanmean(a, varargin)
%NANMEAN MEAN value for TIMESTAMP objects, ignoring NANs.
%
% See stats/NANMEAN.

if ~isa(a, 'timestamp') 
  error('first parameter to timestamp/nanmean must be a timestamp object');
elseif nargin == 2 & ~isnumeric(varargin{1})
  error('second parameter to timestamp/nanmean must be numeric');
end

r_ms = nanmean(a.ms, varargin{:});
r = timestamp('cdfepoch', r_ms);

