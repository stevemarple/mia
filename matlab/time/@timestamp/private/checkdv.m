function r = checkdv(dv)

r = logical(0);

if ~isnumeric(dv)
  % wrong type
  return;
elseif ~isa(dv, 'double')
  dv = double(dv);
end

if numel(dv) ~= 6
  % wrong length
  return
elseif ~isequal(floor(dv(1:5)), dv(1:5))
  % non-integer where integer expected
  return
elseif dv(1) < 0
  % bad year
  return
elseif dv(2) < 1 | dv(2) > 12
  % bad month
  return
elseif dv(3) < 1 | dv(3) > daysinmonth(dv(1))
  % bad day of month
  return
elseif dv(4) < 0 | dv(4) > 23
  % bad hour
  return
elseif dv(5) < 0 | dv(5) > 59
  % bad minute
  return
elseif dv(6) < 0 | dv(6) >= 60
  return
end

r = logical(1);
