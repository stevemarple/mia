function r = isotime(t)
%ISOTIME Convert TIMESTAMP to ISO-style date and time string
%
%   r = ISOTIME(t)
%   r: CHAR (or CELL is t is not scalar)
%   t: TIMESTAMP object
%
% ISOTIME converts TIMESTAMP objects to strings of the form 
% YY-MM-DD HH:MM:SS. 
%
% See also timestamp/ISODATE, timespan/ISOTIME.

if numel(t) ~= 1
  r = cell(size(t));
  for n = 1:numel(t)
    % tn = t(n);
    % the above command doesn't call timestamp/subsref in some matlab
    % versions! Do so manually.    
    sr.type = '()';
    sr.subs = {n};
    tn = subsref(t, sr);
    r{n} = feval(mfilename, tn);
  end
  return
end

% t is now scalar
if isvalid(t)
  r = strftime(t, '%Y-%m-%d %H:%M:%S');
else
  r = char(t);
end

    
