function r = gettimeofday(t)
%GETTIMEOFDAY  Return the time since midnight.
%
%   r = GETTIMEOFDAY(t)
%   r: TIMESTAMP object(s)
%   t: TIMESPAN object(s)
%
%   See also TIMESTAMP, timestamp/GETDATE, TIMESPAN.

ms = getcdfepochvalue(t);
r = timespan(rem(ms, 86400e3), 'ms');
