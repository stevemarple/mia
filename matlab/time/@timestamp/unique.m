function [r, ri, rj] = unique(a, varargin)
%UNIQUE  Unique set overloaded for TIMESTAMP.
%
% See ops/UNIQUE.

[r_ms, ri, rj] = unique(a.ms, varargin{:});

r = timestamp('cdfepoch', r_ms);
