function r = getdate(t)

r = floor(t, timespan(1, 'd'));
