function readme(varargin)
%README file for TIMESTAMP and TIMESPAN classes
%
%   Time is a difficult quantity to use. Working with just hours, minutes
%   and seconds is normally straightforward. Sooner or later some reference
%   must be made to a date, and that is where the problems begin.
%   Encapsulating the intricacies of calculating leap years, month and year
%   boundaries etc. within a class simplifies calculations which reference
%   time.
%
%   The TIMESTAMP and TIMESPAN classes make a distinction between a date (a
%   specific point in time for which a year must be specified) and a
%   duration. This is important because durations (TIMESPANs) may be summed,
%   but dates (TIMESTAMPs) cannot. Similarly, TIMESTAMPs cannot be
%   multiplied or divided by real numbers, but a TIMESPAN can be.
%
%   The TIMESTAMP and TIMESPAN classes know which operations are sensible,
%   and will generate errors for any combinations which aren't.
%
%   Both classes work correctly for matrices of TIMESTAMP and TIMESPAN
%   objects.
%
%   See also TIMESTAMP, TIMESPAN.

% if called as a function show this file as if "help readme" was typed.
[st idx] = dbstack;
help(st(1).name);
