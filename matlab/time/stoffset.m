function [r,sd] = stoffset(t, loc, varargin)
%STOFFSET  Calculate the offset from the start of the sidereal day.
%
%   r = STOFFSET(t, loc)
%   [r n] = STOFFSET(t, loc, st)
%   r: offset from LMST midnight (TIMESPAN)
%   n: sidereal day number (DOUBLE)
%   t: time (TIMESPAN)
%   loc: location (LOCATION)
%   st: start time (TIMESPAN)
%
% Return the sidereal time offset (as a TIMESTAMP) since sidereal
% midnight. Optionally also return a sidereal day number, where st
% represents a time in day zero.
%
%   See also UT2LMST, LMST2UT, SIDEREALDAY.

% On this day find out what time is LMST midnight
midnight = lmst2ut(timespan(0,'d'), loc, t);

% if midnight is after time 't' then use the sidereal midnight from the
% previous day
% if midnight > t
%   midnight = lmst2ut(timespan(0,'d'), loc, t-timespan(1,'d'));
% end
idx = midnight > t;
if any(idx)
  midnight(idx) = lmst2ut(timespan(0,'d'), loc, t(idx)-timespan(1,'d'));
end

r = t - midnight;

% if r > siderealday
%  r = r - siderealday;
% end

idx = r > siderealday;
if any(idx)
  r(idx) = r(idx) - siderealday;
end

if nargout > 1
  % calculate in which sidereal day each sample falls; let the first day
  % be zero
  if nargin < 3
    error('reference time needed for sidereal day number');
  end
  
  st = varargin{1};
  sd = floor((t - st + stoffset(st, loc)) ./ siderealday);
end
