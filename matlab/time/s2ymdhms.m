function ymdhms = s2ymdhms(seconds)
% S2YMDHMS  seconds to hours, minutes and seconds
% s: number of seconds
% returns: hours, minutes and seconds as datevec matrix,
% [0, 0, 0, h, m, s]

if seconds < 0
  negative = 1;
else 
  negative = 0;
end
x = abs(seconds);
h = fix(x / 3600); % whole hours
x = x - (h*3600);    % in seconds, excl. hours component
m = fix(x / 60);         % whole minutes
s = rem(x,60);           % in seconds, excl. hours and minutes

if negative
  ymdhms = [0, 0, 0, -h, -m, -s];
else
  ymdhms = [0, 0, 0, h, m, s];
end
