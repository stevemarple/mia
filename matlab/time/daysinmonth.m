function [r, leap] = daysinmonth(year, varargin)
%DAYSINMONTH  Return the number of days in a month.
%
%   [r leap] = DAYSINMONTH(year)
%   leap: LOGICAL scalar indicating if year is leap
%   r: [1x12] DOUBLE containing the number of days in each month, with a
%   correction for leap years
%   year: NUMERIC scalar
%
% Return the number of days in each month for the year indicated.
%
%
%   [r leap] = DAYSINMONTH(year, month)
%   r: DOUBLE containing the number of days in a given month,
%   leap: LOGICAL value indicating if year is leap
%   year: NUMERIC scalar or matrix
%   month: NUMERIC scalar or matrix
%
%   Return the number of days in a given month. year and month can be
%   scalar or matrices. If both are matrices they must have the same size.
%
% See also ISLEAPYEAR, TIMESTAMP.

%                J  F  M  A  M  J  J  A  S  O  N  D
days_in_month = [31 28 31 30 31 30 31 31 30 31 30 31];

switch nargin
 case 1
  if numel(year) ~= 1
    error('year must be scalar');
  end
  r = days_in_month;
  leap = isleapyear(year);
  if leap
    r(2) = 29;
  end
  return
  
 case 2
  month = varargin{1};
  
 otherwise
  error('incorrect parameters');
end
  


switch numel(year)
 case 0
  error('year cannot be empty');
  
 case 1
  % year scalar
  
  switch numel(month)
   case 0
      error('month cannot be empty');
      
   case 1
    ; % both scalar, ok
    
   otherwise
    year = repmat(year, size(month));
  end
  
 otherwise
  % year non-scalar
  
  switch numel(month)
   case 0
    error('month cannot be empty');
      
   case 1
    % month scalar
    month = repmat(month, size(year));
    
   otherwise
    if ~isequal(size(year), size(month))
      error(['when year and month are not scalar they must have the same' ...
	     ' size']);
    end
  end
end
    
  
leap = isleapyear(year);
r = zeros(size(year));
% r = repmat(nan, size(year));


% remember which months are bad
bad_months = (~isfinite(month) | month < 1 | month > 12);

% for table-lookup ensure months are in range 1 to 12
month(bad_months) = 1; 


r = days_in_month(month);

% bad months should return a bad number of days
r(bad_months) = nan;

% bad years should return a bad number of days
r(~isfinite(year)) = nan;

% correct for leap years having 29 days in February
r(leap & month == 2) = r(leap & month == 2) + 1;

