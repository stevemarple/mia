function ts = gmst(t)
%GMST  Return the Greenwich Mean Sidereal Time at 00:00 UT
%
%   r = GMST(t)
%   r: Greenwich mean sidereal time (TIMESPAN)
%   t: time (TIMESTAMP)
%
%   See also LMST2UT, UT2LMST, SIDEREALDAY.

% From "The Astronomical Almanac 1996, p B6, B7"

meanSiderealDayToMeanSolarDay = 1.00273790935;
% Tu = interval of time, measured in Julian centuries of 36525 days of 
% universal time (mean solar days), elapsed since the epoch 2000 
% January 1d 12h UT
% calculate Tu based at time at start of day

% % Tu = (juliandate(time([1,1,1,0,0,0].*datevec(t))) - 2451545)/36525;
% T u = (juliandate(getday(t)) - 2451545)/36525;

Tu = (juliandate(getdate(t)) - 2451545)/36525;

if 0
  % original, non-vectorised version
ts = 24110.54841 + 8640184.812866*Tu + ...
    (0.093104*Tu*Tu) - ...
    (6.2e-6 *Tu*Tu*Tu)
else
  % vectorised version
  ts = 24110.54841 + 8640184.812866*Tu + ...
       (0.093104 * power(Tu, 2)) - ...
    (6.2e-6 * power(Tu, 3));
end

ts = timespan(mod(ts, 86400), 's');  % convert to +ve values if before 2000



