function ts = ut2lmst(t, longitude)
%UT2LMST  Convert a UT time to the mean local sidereal time.
%
%   lmst = UT2LMST(t, longitude)
%   lmst: local mean sidereal time (TIMESPAN)
%   t: TIMESTAMP object containing time in UT
%   longitude: longitude of the location, or a LOCATION object
%
%   See also LMST2UT, GMST, LOCATION.

% From "The Astronomical Almanac 1996, p B6, B7"

meanSiderealDayToMeanSolarDay = 1.00273790935;

% GMST at 00:00 UT
ts = gmst(t);
% add the equivalent mean sidereal time from 0h to the time
% secs = datevec(t) * [0;0;0;3600;60;1] * meanSiderealDayToMeanSolarDay
secs = gettotalseconds(gettimeofday(t)) * meanSiderealDayToMeanSolarDay;
ts = gettotalseconds(ts) + secs;
ts = mod(ts, 86400); % remove any extra days



% gmst = Greenwich Mean Sidereal Time
gmst2 = timespan(ts,'s');


% local mean sidereal time
if isa(longitude, 'location')
  longitude = getgeolong(longitude);
end
if longitude > 180
  longitude = longitude -180
end
timediff = timespan(86400*longitude/360, 's');
ts = gmst2 + timediff;

if ts < timespan 
  ts = ts + timespan(1,'d');
end



