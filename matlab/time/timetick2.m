function formatStr = timetick2(varargin)
%TIMETICK2 Set ticks on an axis to be marked with time.
%
%   fstr = TIMETICK2(ah, ...)
%
%   fstr: format string use for labelling ticks
%   ah: AXES handle
%
% A high-level function for labelling axes with time. This function
% supercedes TIMETICK, which is retained for compatibility reasons. The
% following name/value pairs may be given to modify the default behaviour:
%
%   'axis', CHAR
%   Which axis to label, default is 'X'.
%
%   'label', LOGICAL or CHAR
%   If the label is a LOGICAL it acts as a flag to indicate if the axis
%   label is to be set, default is TRUE. If label is a CHAR then
%   labelling is enabled and the string is used as the label.
%
%   'tickmarksnum', NUMERIC
%   The (approximate) number of tickmarks which should be used when
%   calculating the tick labels.
%
%   'fstr', CHAR
%   Override the default choice of format string which is passed to
%   STRFTIME.
%
%   'interval', TIMESPAN
%   Override the default choice of interval used for the tick marks.
%
%   'mininterval', TIMESPAN
%   Override the default choice of interval used for the tick marks,
%   interval used will be not less than mininterval.
%
%   'class', 'timestamp' | 'timespan'
%   The class to assume for labelling the axis. Default is 'timestamp',
%   meaning that the axis is represents TIMESTAMP values. 'timespan'
%   indicates that the axis represents TIMESPAN values.
%
% See also TIMESTAMP, TIMESPAN, AXES.

formatStr = '';

if nargin == 0
  ah = [];
  varargin = {};
else
  ah = varargin{1};
  varargin(1) = []; % delete axis handle from list
end

if isempty(ah)
  ah = gca;
end

% Handle callback case, ah is 'cba' so fetch the actual (callback) axis.
if strcmp(ah, 'cba')
  % use callback objects parent axis
  ah = local_gcba(gcbo);
elseif strcmp(ah, 'zoomcb')
  ah = local_findtimetickaxes;
  % ensure time callback only updates
  varargin{end+1} = 'updateonly';
  varargin{end+1} = true;
end


if numel(ah) ~= 1
  formatStr = cell(size(ah));
  for n = 1:numel(ah)
    formatStr{n} = timetick2(ah(n), varargin{:});
  end
  return
end


% inherit fontsize, fontweight etc from defaultaxeslabelparams;
defaults = defaultaxeslabelparams;

defaults.axis = 'X';
defaults.label = 1;
defaults.tickmarksnum = 8;  
defaults.fstr = '';
defaults.interval = [];
defaults.mininterval = [];
defaults.class = 'timestamp';

defaults.updateonly = false;

% an approx number of tickmarks wanted work in seconds to avoid unnecessary
% calls to time class rawIntervalSec = gettotalseconds((endTime - startTime)
% ...  / defaults.tickmarksnum);

tmpfields1 = fieldnames(defaults);


[defaults unvi] = interceptprop(varargin, defaults);
% warnignoredparameters(varargin{unvi(1:2:end)});

if defaults.updateonly
  % load defaults from the first valid label and return
  labelprefixes = {'X' 'Y' 'Z'};
  for n = 1:3
    xyz = labelprefixes{n};
    ud = get(get(ah, [xyz 'Label']), 'UserData');
    if isstruct(ud) && isfield(ud, 'timetick2')
      defaults = ud.timetick2;
      defaults.updateonly = false;
      prop = makeprop(defaults);
      feval(mfilename, ah, prop{:});
    end
  end
  return
end

tmpfields2 = fieldnames(defaults);
nontextparamfields = setdiff(tmpfields2, tmpfields1);

ax = upper(defaults.axis);
axLim = [ax 'Lim'];
% calculate a sensible interval between (visible) tickmarks


lim = get(ah(1), axLim);
plotSt = feval(defaults.class, 'cdfepoch', lim(1));
plotEt = feval(defaults.class, 'cdfepoch', lim(2));
plotDuration = plotEt - plotSt;
rawIntervalSec = gettotalseconds(plotDuration) ./ ...
    defaults.tickmarksnum;

alignWithDates = [];
% duration = endTime - startTime;

if plotDuration > timespan(3650, 'd') & isa(plotSt, 'timestamp')
  % very long time, use even years
  alignWithDates.years = (2*floor(getyear(plotSt)/2)):2:getyear(plotEt);
  alignWithDates.months = 1;
  alignWithDates.days = 1;
  formatStr = '%Y';
elseif plotDuration > timespan(800, 'd') & isa(plotSt, 'timestamp')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1;
  alignWithDates.days = 1;
  formatStr = '%Y';
elseif plotDuration > timespan(200, 'd') & isa(plotSt, 'timestamp')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1:3:12;
  alignWithDates.days = 1;
  formatStr = '%d/%m/%Y';
elseif plotDuration > timespan(60, 'd') & isa(plotSt, 'timestamp')
  alignWithDates.years = getyear(plotSt):getyear(plotEt);
  alignWithDates.months = 1:12;
  alignWithDates.days = [1];
  formatStr = '%d/%m/%Y';
elseif rawIntervalSec > 10*86400
  intervalSec = rawIntervalSec - rem(rawIntervalSec, 10*86400); 
  formatStr = '%d/%m/%Y';
elseif rawIntervalSec > 2*86400;
  intervalSec = rawIntervalSec - rem(rawIntervalSec, 2*86400); 
  formatStr = '%d/%m';
elseif rawIntervalSec > 24*3600
  intervalSec = 2*86400;
  formatStr = '%d/%m';
elseif rawIntervalSec > 12*3600
  intervalSec = 24*3600;
  formatStr = '%d/%m';  
% elseif rawIntervalSec > 8*3600
  % intervalSec = 12*3600;
  % formatStr = '%H';
elseif rawIntervalSec > 7*3600
  intervalSec = 24*3600;
  formatStr = '%d/%m';  
elseif rawIntervalSec > 6*3600
  intervalSec = 8*3600;
  formatStr = '%H';
elseif rawIntervalSec > 4*3600
  intervalSec = 6*3600;
  formatStr = '%H';
elseif rawIntervalSec > 2*3600
  intervalSec = 4*3600;
  formatStr = '%H';
elseif rawIntervalSec > 60*60
  intervalSec = 2*3600;
  formatStr = '%H';
elseif rawIntervalSec > 30*60
  intervalSec = 60*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 20*60
  intervalSec = 30*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 15*60
  intervalSec = 20*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 10*60
  intervalSec = 15*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 5*60
  intervalSec = 10*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 4*60
  intervalSec = 5*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 2*60
  intervalSec = 4*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 60
  intervalSec = 2*60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 30
  intervalSec = 60;
  formatStr = '%H:%M';
elseif rawIntervalSec > 20
  intervalSec = 30;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 15
  intervalSec = 20;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 10
  intervalSec = 15;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 8
  intervalSec = 10;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 4
  intervalSec = 5;
  formatStr = '%H:%M:%S';
elseif rawIntervalSec > 2
  intervalSec = 2;
  formatStr = '%H:%M:%S';
else
  intervalSec = 1;
  formatStr = '%H:%M:%S';
end

% Allow user to override default format string
if ~isempty(defaults.fstr)
  formatStr = defaults.fstr;
end

% Allow user to override the interval
if ~isempty(defaults.mininterval)
  % interval should be no less than mininterval
  intervalSec = max(gettotalseconds(defaults.mininterval), intervalSec);
end
if ~isempty(defaults.interval)
  intervalSec = gettotalseconds(defaults.interval);
end

timeStr = 'time';
switch formatStr
 case '%Y'
  timeUnits = 'year';
  timeStr = 'date';
 case '%d/%m/%Y'
  if isa(plotSt, 'timespan')
    % cannot sensibly print the month
    formatStr = '%d';
    timeUnits = 'd';
  else
    timeUnits = 'day/month/year';
    timeStr = 'date';
  end
 case '%d/%m'
  if isa(plotSt, 'timespan')
    % cannot sensibly print the month
    formatStr = '%d';
    timeUnits = 'd';
  else
    timeUnits = 'day/month';
    timeStr = 'date';
  end
 case '%H'
  timeUnits = 'h';
 case '%H:%M'
  timeUnits = 'h:m';
 case '%H:%M:%S'
  timeUnits = 'h:m:s';
 case '%M:%S'
  timeUnits = 'm:s';
 case '%S'
  timeUnits = 's';
 otherwise
  timeUnits = [];
end

if ischar(defaults.label)
  if isempty(timeUnits)
    labelStr = defaults.label;
  else
    labelStr = [defaults.label ' (' timeUnits ')'];
  end
elseif isempty(timeUnits)
  % in case we missed any
  labelStr = timeStr;
else
  labelStr = [timeStr ' (' timeUnits ')'];
end

if ~isempty(alignWithDates)
  % special case for long durations
  tickStr = {};
  tickPos = [];
  n = 1;
  for y = alignWithDates.years
    for m = alignWithDates.months
      for d = alignWithDates.days
	t = timestamp([y m d 0 0 0]);
	tickStr{n} = strftime(t, formatStr);
	% tickPos(n) = (t - startTime) / resolution;
	tickPos(n) = getcdfepochvalue(t);
	n = n + 1;
      end
    end
  end
  
else 
  interval = timespan(intervalSec, 's');

  % calculate sensible locations for the tickmarks to be
  % disp([mfilename ': make timeticks only for visible part!']);

  % for first elem at 0
  % tickStartTime = floor(startTime + (lim(1))*resolution, interval);
  % tickEndTime =    ceil(startTime + (lim(2))*resolution, interval);

  tickStartTime = floor(plotSt, interval);
  tickEndTime = ceil(plotEt, interval);
  
  sz = fix((tickEndTime - tickStartTime)/interval) + 1;
  tickStr = cell(1, sz);  % matrix to hold the tick label strings 
  tickPos = zeros(1, sz); % matrix to hold the tick positions

  t = tickStartTime;
  n = 1;
  while t <= tickEndTime
    if isa(t, 'timespan')
      if t < timespan(0,'s')
	tickStr{n} = ['-' strftime(t, formatStr)];
      elseif t > timespan
	tickStr{n} = strftime(t, formatStr);
      else
	% work around some odd matlab formatting
	tickStr{n} = ['  ' strftime(t, formatStr)];
      end
      
    else
      % for TIME objects
      tickStr{n} = strftime(t, formatStr);
    end
    
    % tickPos(n) = ((t - startTime) / resolution) + 1; % first elem is 1
    % tickPos(n) = ((t - startTime) / resolution) + 0; % first elem is 0
    tickPos(n) = getcdfepochvalue(t);
    t = t + interval;
    n = n + 1;
  end

end

set(ah, [ax, 'Tick'], tickPos, ...
		[ax, 'TickLabel'], tickStr);

% hopefully later versions of matlab will be able to supply handles to tick
% label text. If so test if tick labels overlap, can then reduce number
% of tickmarks
tickmarksNum2 = length(tickPos); % current number, (not requested number!)


% ud = {plotSt plotEt defaults.label};
labelH = get(ah, [ax 'Label']);
%if length(labelH) > 1
%  labelH = [labelH{:}]; % convert from cell array
%end
% set(labelH, 'UserData', ud, 'Tag', 'timetick2label');


p = get(labelH);


% set(labelH, rmfield(defaults, nontextparamfields), 'String', labelStr);

textprop = rmfield(defaults, setdiff(lower(fieldnames(defaults)), ...
                                     lower(fieldnames(p))));

if ~isempty(textprop)
  textpropcell = makeprop(textprop);
  set(labelH, textpropcell{:});
end

if ischar(defaults.label) || logical(defaults.label)
  visible = 'on';
else
  visible = 'off';
end

% Create callback string
cb = sprintf(['%s(''cba'',''axis'',''%s'',''label'',%d,''tickmarksnum'',%d,' ...
              '''fstr'',''%s'',''class'',''%s'');'], ...
             basename(mfilename), defaults.axis, defaults.label, ...
             defaults.tickmarksnum, defaults.fstr, defaults.class);
set(labelH, 'String', labelStr, ...
	    'FontWeight', 'bold', ...
            'Visible', visible);

if ~defaults.updateonly
  if matlabversioncmp('<', '7.3')
    % no easy way to add callbacks
    set(labelH, 'ButtonDownFcn', cb);
  else
    % Zoom callbacks are supported!
    z = zoom(ah);
    set(z, 'ActionPostCallback', sprintf('%s(''zoomcb'');', mfilename));
  end

  % Update user data
  ud = get(labelH, 'UserData');
  if isempty(ud) | isstruct(ud)
    ud.timetick2 = defaults;
    set(labelH, 'UserData', ud);
  end
  
  % Set tag
  set(labelH, 'Tag', 'timetick2label');
end
    
if numel(unvi)
  set(labelH, varargin{unvi});
end


% % set axes to be tight on selected axis only
% axProp = get(ah);
% axis(ah, 'tight');
% axProp2.XLim = axProp.XLim;
% axProp2.YLim = axProp.YLim;
% axProp2.ZLim = axProp.ZLim;
% axProp2 = setfield(axProp2, axLim, get(ah, axLim));
% set(ah, axProp2);


% get callback axis
function r = local_gcba(h)

r = h;
p = get(h);
while ~strcmp(p.Type, 'axes') & ~isempty(r)
  r = p.Parent;
  p = get(r);
end
return


switch p.Type
 case {'figure', 'root'}
  r = [];
  return
 case 'axes'
  r = obj;
  return;
 otherwise
  r = local_gcba(obj.Parent);
end

function ah = local_findtimetickaxes

fh = gcbf;
if isempty(fh)
  fh = gcf;
end
lh = findall(fh, 'Type', 'text', 'Tag', 'timetick2label');
if iscell(lh)
  lh = [lh{:}];
end
ah = get(lh, 'Parent');
if iscell(ah)
  ah = [ah{:}];
end
