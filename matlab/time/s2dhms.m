function [d, h, m, s] = s2dhms(x)
% S2DHMS  Convert seconds to number of days, hours, minutes and seconds
%
% [d h m s] = S2DHMS(secs);
% d, h, m, s: days, hours minutes and seconds
% secs: total number of seconds
%
% The values of d, h and m are always integers, s may be a floating-point
% value. d, h, m and s are the same size as secs.

d = fix(x ./ 86400);
x = rem(x, 86400); % x = x - (d .* 86400)


h = fix(x ./ 3600);
x = rem(x, 3600);

m = fix(x ./ 60);
x = rem(x, 60);

s = x;
