function r = datevec2unixtime(dv)
%DATEVEC2UNIXTIME Helper function for TIMESTAMP class.
%
% See TIMESTAMP.

% Matlab's datenum for the unix epoch
ue_dn = unixepochdatenum;

% the matric to multiply hours, minutes and seconds by to get the total
% number of seconds
hms2s = [3600; 60; 1];


if isnumeric(dv)
  % [1x6] numeric array
  
  % if ~isequal(size(dv), [1 6])
  %   error('dv must be a 1x6 matrix');
  % end
  if checkdatevecok(dv)
    dn = datenum(dv(1), dv(2), dv(3)); % do whole day
    secs = dv(4:6) * hms2s;
  else
    dn = nan;
    secs = nan;
  end
  
elseif iscell(dv)
  % cell array of [1x6] double matrices
  dn = zeros(size(dv));
  secs = dn;
  for n = 1:numel(dv)
    if checkdatevecok(dv{n})
      dn(n) = datenum(dv{n}(1), dv{n}(2), dv{n}(3));
      secs(n) = dv{n}(4:6) * hms2s;
    else
      dn(n) = nan;
      secs(n) = nan;
    end
  end
  
elseif isstruct(dv)
  % array of structs, each having a dv field which is a [1x6] matrix
  dn = zeros(size(dv));
  secs = dn;
  for n = 1:numel(dv)
    if checkdatevecok(dv(n))
      dn(n) = datenum(dv(n).dv(1), dv(n).dv(2), dv(n).dv(3));
      secs(n) = dv(n).dv(4:6) * hms2s;
    else
      dn(n) = nan;
      secs(n) = nan;
    end
  end
 
else
  error('incorrect parameters');
end

r = ((dn - ue_dn) * 86400) + secs;
