function r = siderealday
%SIDEREALDAY  Return the duration of a sidereal day as a TIMESPAN.
%
%   r = SIDEREALDAY
%   r: TIMESPAN (approx 86164 seconds)
%
%   See also TIMESPAN.

r = timespan(86164,'s');
% r = timespan(1 / 1.00273790935, 'd');

