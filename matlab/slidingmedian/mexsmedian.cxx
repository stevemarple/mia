// mexsmedian.cpp

#include "slidingmedian.h"
#include "mex.h"
#include <stdlib.h>

/*
 * $Log$
 */

/*
 * 
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if(nrhs != 2)
    mexErrMsgTxt("Only two input parameters allowed");

  if(nlhs != 1)
    mexErrMsgTxt("Only one output parameter allowed");

  if(!mxIsNumeric(prhs[0]) || mxIsComplex(prhs[0]) || mxIsSparse(prhs[0])
     || !mxIsDouble(prhs[0]) || !(mxGetM(prhs[0]) == 1 && mxGetN(prhs[0]))) 
    mexErrMsgTxt("blocksize must be a scalar");
  
  int blockSize = int(mxGetPr(prhs[0])[0]);
  if(blockSize < 1)
    mexErrMsgTxt("blocksize must be a positive integer");
  
  int rows = mxGetM(prhs[1]);
  int cols = mxGetN(prhs[1]);
  if(!mxIsNumeric(prhs[0]) || mxIsComplex(prhs[0]) || mxIsSparse(prhs[0])
     || !mxIsDouble(prhs[0]) || !(mxGetM(prhs[0]) == 1 && mxGetN(prhs[0]))) 
    mexErrMsgTxt("data must be of type double");

  int datasets;
  int datasize;
  if(rows == 1 && cols >= blockSize) {
    // row vectors are acceptable
    datasets = 1;
    datasize = cols;
    plhs[0] = mxCreateDoubleMatrix(rows, cols + 1 - blockSize, mxREAL);
  }
  else {
    if(cols == 1 && rows >= blockSize) {
      // column vectors are acceptable
      datasets = 1;
      datasize = rows;
      plhs[0] = mxCreateDoubleMatrix(rows + 1 - blockSize, cols, mxREAL);
    }
    else {
      if(rows < blockSize && cols != 1)
	mexErrMsgTxt("matrix data must have at least blockSize rows");
      else {
	if(rows == 1 || cols == 1)
	  mexErrMsgTxt("length of vector data must be at least blockSize");
	else {
	  // matrix input
	  datasets = cols;
	  datasize = rows;
	  plhs[0] = mxCreateDoubleMatrix(rows + 1 - blockSize, cols, mxREAL);
	}
      }
    }
  }

  double *data = mxGetPr(prhs[1]);
  double *r = mxGetPr(plhs[0]);

  int n = 0;
  for(int j = 0; j < datasets; ++j) {
    slidingMedian<double> sm(blockSize, &data[j*datasize]);
    for(int i = (blockSize - 1)+(datasize*j); i < (j+1)*datasize; ++i) {
      r[n++] = sm.median(data[i]);
    }
  }
  
  return;
}







