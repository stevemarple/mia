// slidingmedian.h

#include <list>
#include <math.h>
#include <iostream>

using namespace std;

/*
 * $Log: slidingmedian.h,v $
 * Revision 1.2  2000/06/15 16:10:31  marple
 * Mostly working version.
 *
 * Revision 1.1  2000/02/10 18:07:39  marple
 * Initial revision
 */

//typedef float myList;


template <class T>
class slidingMedian
{
public:
  typedef typename list<T>::iterator my_list_iterator;

  slidingMedian(int blockSize, const T *data) throw ();
  ~slidingMedian() throw ();

  T median(const T &d) throw ();
  T static mean(const T &a, const T &b) throw ();
  ostream& print(ostream &sout, const char* marker = "--------------")
    throw ();  
    
private:
  int _blockSize;
  int _medListSize;
  
  // typename list<T>::iterator _median;
  my_list_iterator _median;
  
  list<T> _medList;      // for normal numbers
  list<T> _nanList;      // nans are kept out of normal list
  list<T> _tmpList;  // temporary place for the remove item

  //typename list<list<T>::iterator> _history;
  //typename list<list<T>::iterator> _tmpHistory;  
  list<my_list_iterator> _history;
  list<my_list_iterator> _tmpHistory;
    
};


template<class T>
slidingMedian<T>::slidingMedian(int blockSize, const T* data) throw ()
{
  _blockSize = blockSize;
    
  _medListSize = 0;
  
  //list<T>::iterator medTail = _medList.end();
  //list<T>::iterator nanTail = _nanList.end();
  my_list_iterator medTail = _medList.end();
  my_list_iterator nanTail = _nanList.end();
    
  for(int i = 0; i < blockSize - 1; ++i) {
    if(isnan(data[i])) {
      _nanList.push_back(data[i]);
      nanTail++;
      _history.push_back(nanTail);
    }
    else {
      ++_medListSize;
      _medList.push_back(data[i]);
      ++medTail;
      _history.push_back(medTail);
    }
  }
  _medList.sort();
  
  _median = _medList.begin();
  // cout << "INIT " << *_median << endl;
  // ### DEBUG ### had to subtract 1 for mango
  int iterations = (_medListSize / 2) - !(_medListSize % 2);
  for(int i = 0; i < iterations; ++i)
    ++_median;

  _tmpList.push_back(666.666);
  // _tmpHistory.push_back(list<T>::iterator());
  _tmpHistory.push_back(typename list<T>::iterator());
  
  // print(cout);
}

template <class T>
slidingMedian<T>::~slidingMedian() throw ()
{
  ;
}


template <class T>
T slidingMedian<T>::median(const T& d) throw ()
{
  // cout << "new data: " << d << endl;
  T r;
  // list<T>::iterator it;
  my_list_iterator it;
    
  // check if data is valid
  if(isnan(d)) {
    // not valid
    _nanList.push_back(d);
    it = _nanList.end();
    // add to history list    
    // _history.push_back(--it);
    *(_tmpHistory.begin()) = --it;
    _history.splice(_history.end(), _tmpHistory, _tmpHistory.begin());
  }
  else {
    if(_medListSize == 0) {
      ++_medListSize;
      it = _medList.end();
      *(_tmpList.begin()) = d;
      _medList.splice(it, _tmpList, _tmpList.begin());
      // add to history list
      //_history.push_back(--it);
      *(_tmpHistory.begin()) = --it;
      _history.splice(_history.end(), _tmpHistory, _tmpHistory.begin());
      _median = it;
    }
    else {
    
      it = _median;
      // find insertion point, if the new item has the same value as any
      // existing items it is always placed to the right of them. When
      // removing items the oldest will always be on the left.
      if(d < *_median) {
	// insertation point left of median
	// look left
	if(d < *_medList.begin()) {
	  // cout << "new item is smallest" << endl;
	  it = _medList.begin();
	}
	else {
	  // int i = 0;
	  while(d < *it) {
	    // cout << "i is " << i++ << endl;
	    --it;
	  }
	  ++it;
	}
	++_medListSize;

	*(_tmpList.begin()) = d;
	//_medList.splice(++it, _tmpList, _tmpList.begin());
	_medList.splice(it, _tmpList, _tmpList.begin());

	// add to history list
	*(_tmpHistory.begin()) = --it;
	_history.splice(_history.end(), _tmpHistory, _tmpHistory.begin());

	if(_medListSize % 2 == 0)
	  --_median;
      }
      else {
	// insertation point right of median

	// if(d == *_median) {
	//   ++it;  // ensure it is inserted to the right of the median value
	// }
	// else {

	// look right, and insert after any items of the same size
	while(*it <= d && it != _medList.end()) {
	  ++it;
	}
	// }
      
	++_medListSize;
	// _medList.insert(it, d);
	*(_tmpList.begin()) = d;
	_medList.splice(it, _tmpList, _tmpList.begin());
	// add to history list
	// _history.push_back(--it);
	*(_tmpHistory.begin()) = --it;
	_history.splice(_history.end(), _tmpHistory, _tmpHistory.begin());

	if(_medListSize % 2 == 1)
	  ++_median;
      
      }
    }
  }
  // print(cout, "----------- in median()");
  
  if(_medListSize % 2 == 1)
    r = *_median;
  else {
    if(_medListSize == 0) {
      r = _nanList.front();       
    }
    else {
      it = _median;
      r = mean(*_median, *++it);
    }
  }

  // find the list containing the oldest element
  list<T>* oldestElemList;
  if(isnan(*(_history.front())))
    oldestElemList = &_nanList;
  else
    oldestElemList = &_medList;
  
  // remove oldest element
  if(_history.front() == _median) {
    // oldest is also median
    if(_medListSize % 2 == 0)
      ++_median;
    else
      --_median;
    // _medList.erase(_history.front());
    // _tmpList.splice(_tmpList.begin(), _medList, _history.front());
    _tmpList.splice(_tmpList.begin(), *oldestElemList, _history.front());
    --_medListSize;
  }
  else {
    if(isnan(*_history.front())) {
      // oldest is NaN
      // _nanList.erase(_history.front());
      // _tmpList.splice(_tmpList.begin(), _medList, _history.front());
      _tmpList.splice(_tmpList.begin(), *oldestElemList, _history.front());
    }
    else {
      // _medList.erase(_history.front());
      // _tmpList.splice(_tmpList.begin(), _medList, _history.front());
      _tmpList.splice(_tmpList.begin(), *oldestElemList, _history.front());
      --_medListSize;
      // cout << "*_history.front(): " << *_history.front() << endl;
      // cout << "*_median: " << *_median << endl;
      if(*_history.front() <= *_median) {
	// cout << "oldest left of median" << endl;
	// oldest left of median
	if(_medListSize % 2 == 1)
	  ++_median;
      }
      else {
	// cout << "oldest right of median" << endl;
	// oldest right of median
	if(_medListSize % 2 == 0)
	  --_median;
      }
    }
  }
  // _history.pop_front();
  _tmpHistory.splice(_tmpHistory.end(), _history, _history.begin());
  // _tmpList.front() = 666.666; // ### DEBUG ###
  return r;
}

// overload if necessary for types which cannot be divided by two
// (e.g. Galois fields)
template <class T>
T slidingMedian<T>::mean(const T &a, const T &b) throw ()
{
  // cout << "MEAN " << a << "  " << b << "  " << (a + b)/2 << endl;
  return (a + b)/2;
}

template <class T>
ostream& slidingMedian<T>::print(ostream &sout,
				 const char* marker) 
  throw ()  
{
  sout << marker << endl
       << "med list: size (" << _medList.size() << ", "
       << _medListSize << ") : ";
  //list<T>::iterator it = _medList.begin();
  //list<T>::iterator en = _medList.end();
  my_list_iterator it = _medList.begin();
  my_list_iterator en = _medList.end();
  
  while(it != en) {
    sout << *it << "  ";
    it++;
  }
  sout << endl;
  
  sout << "NaN list: (" << _nanList.size() << ")         : ";
  it = _nanList.begin();
  en = _nanList.end();

  while(it != en) {
    sout << *it << "  ";
    it++;
  }
  sout << endl;
    

  sout << "history (" << _history.size() << ")           : ";
  //list<list<T>::iterator>::iterator ith = _history.begin();
  //list<list<T>::iterator>::iterator enh = _history.end();
  typename list<my_list_iterator>::iterator ith = _history.begin();
  typename list<my_list_iterator>::iterator enh = _history.end();

  while(ith != enh) {
    sout << *(*ith) << "  ";
    ith++;
  }
  sout << endl;

  // find real median point
  it = _medList.begin();
  cout << "P " << *it << endl;
  for(int i = 0; _medList.size() && i < ((1+_medList.size())/2) -1; ++i)
    it++;
  cout << "approx median pt        : " << *_median << endl;
  cout << "recalc median pt        : " << *it
       << (it == _median ? " match" : " DIFFERS") << endl;
  
  sout << marker << endl;
  return sout;
}


