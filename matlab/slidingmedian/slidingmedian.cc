// slidingmedian.cpp

//
// $Log: slidingmedian.cpp,v $
// Revision 1.1  2000/06/15 15:51:40  marple
// Initial revision
//

#include "slidingmedian.h"

#include <iostream>
#include <stdio.h>

#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef double T;

int main(int argc, char **argv)
{
 
  if(argc != 4) {
    cout << argv[0] << ":\n\t"
	 << argv[0] << " blocksize infile outfile" << endl;
    exit(EINVAL);
  }

  int blocksize = atoi(argv[1]);
  if(blocksize <= 0) {
    cout << "blocksize must be a positive integer" << endl;
    exit(EINVAL);
  }
  
  FILE *fin = fopen(argv[2], "rb");
  FILE *fout = fopen(argv[3], "wb");

  if(fin == NULL) {
    perror(argv[2]);
    exit(errno);
  }

  /*
  fseek(fin, 0, SEEK_END);
  int nelem = ftell(fin) / sizeof(T);
  T* tmp = new T[nelem];
  rewind(fin);
  cout << "nelem: " << nelem << endl;
  int nelem2 = fread(tmp, sizeof(T), nelem2, fin);
  cout << "nelem2: " << nelem2 << endl;
  for(int i = 0; i < nelem2; ++i)
    cout << tmp[i] << " ";
  cout << endl;
  rewind(fin);
  */
  
  if(fout == NULL) {
    perror(argv[3]);
    exit(errno);
  }
  
  T *data = new T[blocksize-1];

  
  int items = fread(data, sizeof(T), blocksize - 1, fin);
  if(items != blocksize - 1) {
    cout << argv[2] << " too short for given block size" << endl;
    exit(EINVAL);
  }

  slidingMedian<T> sm(blocksize, data);
  delete[] data;
    T d1;
  T d2;
  int i = 0;
  while(!feof(fin) && fread(&d1, sizeof(T), 1, fin) == 1) {
    // cout << i++ << " read " << d1 << flush;
    d2 = sm.median(d1);
    fwrite(&d2, sizeof(T), 1, fout);
    // cout << " wrote " << d2 << endl;

  }

  /* 
  FILE *fout2 = fopen("test", "wb");
  double test[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
  fwrite(test, sizeof(T), 16, fout2);
  */      
  
  
  return 0;
}


