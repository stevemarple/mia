// mexsmedian.cpp

#include "slidingmedian.h"

#include "mex.h"

#include <iostream>

#include <stdlib.h>
#include <time.h>


typedef double T;



int main(int argc, char **argv)
{
  const int blockSize = 59;
   // const int blockSize = 5;
#define RND_DATA 1
  
#if RND_DATA
  
  // const int dataSize = 3658;
  const int dataSize = (3600*10) + 58;
  T *data = new T[dataSize];
  srand(0);
  for(int i = 0; i < dataSize; ++i) {
    int n = rand() % 64;
    if(n % 22 == 0)
      data[i] = 0.0/0.0;
    else
      data[i] = n;
  }

  for(int i = 300; i < 400; ++i) 
    data[i] = 0.0/0.0;
  
#else

  const int dataSize = 59;
  //T data2[] = {7.2, 2.2, 0.0/0.0, 3, 9, 8, 3.2, 10, 8.5};
  // T data2[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  T data2[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
  T *data = data2;
#endif
  
  slidingMedian<double> sm(blockSize, data);  
  T med;

  time_t t1 = time(NULL);
  
  cout << "======================" << endl;
  int n = 0;
  for(int i = blockSize - 1; i < dataSize; ++i) {
    med = sm.median(data[i]);
    // sm.print(cout);
    cout << "n: " << ++n << endl;
    cout << "MEDIAN " << med << endl;
    cout << "======================" << endl;
  }

  time_t t2 = time(NULL);
  cerr << "Time taken: " << t2-t1 << " seconds" << endl;
  
#if RND_DATA
  cout << "data : " << endl;
  for(int i = 0; i < dataSize; ++i)
    cout << "DATA " << data[i] << endl;
#endif
  return 0;
}



void mexsmedian(double r[], double blocksize[], double data[]);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if(nrhs != 2)
    mexErrMsgTxt("Only two input parameters allowed");

  if(nlhs != 1)
    mexErrMsgTxt("Only one output parameter allowed");

  /*
    if(!mxIsNumeric(prhs[0]) || mxIsComplex(prhs[0]) || mxIsSparse(prhs[0])
    || !mxIsDouble(prhs[0]) || !(mxGetM(prhs[0]) == 1 && mxGetN(prhs[0]))) 
    mexErrMsgTxt("blocksize must be a scalar");
  */
  
  int blockSize = *mxGetPr(prhs[0]);
  
  int r = mxGetM(prhs[1]);
  int c = mxGetN(prhs[1]);
  if(!mxIsNumeric(prhs[0]) || mxIsComplex(prhs[0]) || mxIsSparse(prhs[0])
     || !mxIsDouble(prhs[0]) || !(mxGetM(prhs[0]) == 1 && mxGetN(prhs[0]))) 
    mexErrMsgTxt("data must be of type double");
  
  if(r < 1 || c < blockSize)
    mexErrMsgTxt("data must have at least 1 row and at least blockSize columns");

  
      
}







