function r = slidingmedian(datain, blockSize)
%SLIDINGMEDIAN  Perform a sliding median
%
%   r = SLIDINGMEDIAN(x, blocksize);


callCprog = 1;

if ~isequal(size(blockSize), [1 1]) | blockSize < 1
  error('block size must be a single, positive integer')
end

if prod(size(datain)) == length(datain) & size(datain, 1) == 1
  % any vectors acceptable but internally deal with column vectors only
  datain = reshape(datain, length(datain), 1);
  reshaped = 1;
else
  reshaped = 0;
end

dataSize = size(datain);
r = zeros(dataSize - [blockSize-1 0]);

if callCprog
  % write to file, call external program and read back result
  for c = 1:size(datain, 2)
    data = datain(:,c);
    
    f1 = tempname;
    [fid1 message] = fopen(f1, 'w');
    if fid1 == -1
      disp('##1');
      error(message);
    end
    
    % need platform dependent data format
    fwrite(fid1, data, 'double');
    
    f2 = tempname;
    
    slidingmedian_exec = fullfile(mia_basedir, 'bin', ...
				  ['slidingmedian_' lower(computer)]);
    
    cmd = sprintf('%s %d %s %s', systemquote(slidingmedian_exec), ...
		  blockSize, f1, f2);
    if isempty(dir(slidingmedian_exec))
      error(sprintf('sliding median program ''%s'' does not exist', ...
		    slidingmedian_exec));
    end
    
    [status message] = system(cmd);
    if status ~= 0
      fclose(fid1);
      delete(f1);
      if exist(f2)
	delete(f2);
      end
      disp('##2');
      error(sprintf(['could not execute sliding median program\n', ...
		     'cmd was: ''%s''\n' ...
		     'error message was: ''%s''\n' ...
		     'return value was : %d'], ...
		    cmd, message, status)); 
    end
    
    [fid2 message] = fopen(f2, 'r');
    if fid2 == -1
      f2
      fclose(fid1);
      delete(f1);
      % disp('##3');
      % error(message);
      if exist(f2)
	delete(f2);
      end
      disp('##3');
      error(message);
    end
    
    [tmp count] = fread(fid2, inf, 'double');
    if count ~= size(data,1) - blockSize + 1
      fclose(fid1);
      delete(f1);
      fclose(fid2);
      delete(f2);
      count
      size(tmp)
      disp('##4');
      error('internal error (returned data too small)');
    end
    r(:,c) = tmp;
    
    fclose(fid1);
    delete(f1);
    fclose(fid2);
    delete(f2);
  end  

else
  % do matlab only version
  % r = zeros(dataSize(1), dataSize(2) + 1 - blockSize);

  blockIndices = 1:blockSize;
  for n = 1:size(r,1)
    for c = 1:dataSize(2)
      r(n, c) = nonanmedian(datain(blockIndices, c), 1);
      % r(:, n) = nanmedian(datain(:, blockIndices)');
      % r(:, n) = median(datain(:, blockIndices), 2);
    end
    blockIndices = blockIndices + 1;
  end

end

if reshaped
  r = r';
end



