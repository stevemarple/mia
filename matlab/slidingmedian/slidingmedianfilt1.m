function r = slidingmedianfilt1(x, n)
%SLIDINGMEDIANFILT1  Perform a sliding median filter.
%
%   r = SLIDINGMEDIANFILT1(x, blocksize);

if numel(n) ~= 1
  error('blocksize must be scalar');
elseif n < 1
  error('blocksize must be a postive integer');
elseif n > numel(x)
  error('block size too large for data');
end

if numel(x) ~= length(x)
  error('data must be a vector');
end

if rem(n, 2) == 0
  padding = zeros(n/2, 1);
else
  padding = zeros((n-1)/2, 1);
end

x2 = [padding; x(:); padding];
tmp = slidingmedian(x2, n);

if rem(n, 2) == 0
  % when odd must delete last element
  tmp(end) = [];
end
r = reshape(tmp, size(x));
