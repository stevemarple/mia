function r = url_usecache(varargin)
%URL_USECACHE  Set or get the URL_TOOLS cache status.
%
%   r = URL_USECACHE
%   r: LOGICAL value indicating if cache is to be used
%
%   r = URL_USECACHE(a)
%   r: LOGICAL value indicating old value cache status
%   a: LOGICAL new value for cache status
%
% URL_USECACHE gets and sets whether URL_TOOLS should use a local file cache
% when transferring URLs. If the environment variable URLTOOLS_CACHEDIR is
% set then the default is to use a cache, otherwise not. The cache status
% may be altered at anytime by calling URL_USECACHE with the new cache
% status.
%
% See also URL_TOOLS, URL_CACHEDIR.

persistent status;
if isequal(status, [])
  % status has not been initialised
  status = ~isempty(getenv('URLTOOLS_CACHEDIR'));
end

r = status;

if nargin
  status = logical(varargin{1});
  mlock; % prevent M-file from being unloaded and its state changed
end
