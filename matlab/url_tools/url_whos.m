function varargout = url_whos(varargin)
%URL_WHOS Similar to WHOS but accepts URLs in addition to filenames.
%
% See also URL_TOOLS, URL_WHO, general/WHOS.

% if using the -file sntax then fetch file

file = '';
if nargin >= 2
  if strcmp(varargin{1}, '-file') & isurl(varargin{2})
    originalurl = varargin{2};
    url = url_mangle(originalurl, basename(mfilename));
    
    % get temporary filename with the .mat extension
    file = tempname2('', '.mat');
    url_fetch(url, file, ...
              'originalurl', originalurl); % fetch remote file
    varargin{2} = file; % swap URL for its local filename
  end
end

% quote all parameters
varg = varargin;
for n = 1:numel(varargin)
  varg{n} = sprintf('''%s''', varargin{n});
end

% create the argument list for whos
arglist = join(',', varg);
if isempty(arglist)
  cmd = 'whos';
else
  cmd = ['whos(' arglist ')'];
end

[varargout{1:nargout}] = evalin('caller', cmd);

if ~isempty(file)
  delete(file); % removing the temporary file
end

