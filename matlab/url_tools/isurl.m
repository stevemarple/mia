function [r,p] = isurl(s, varargin)
%ISURL  Check if a string is a URL
%
%   [r p] = ISURL(s)
%   r: flag to indicate if URL (LOGICAL)
%   p: protocol (eg file, https)
%   s: string, possibly a URL (CHAR)
%   prot: optional protocols to test for (CHAR or CELL of CHARs)
%
% ISURL tests if a string represents a known URL with a known
% protocol. It does not test if the format of the URL is valid, nor if it
% points to a valid resource (see EXIST_URL for that).
%
% See also URL_TOOLS, URL_PARTS, URL_EXIST, URL_FOPEN, URL_LOAD, URL_SAVE,
% URL_FETCH, URL_PUT.

if ~ischar(s) | length(varargin) > 1
  error('incorrect parameters')
end

% protocol part is meant to be case-insensitive
s = lower(s);

% the '//' is used to denote host part, and is not part of the delimiter
% between protocol name and remainder of URL.
protocols.file = 'file:';

protocols.ftp = 'ftp://';
protocols.jar = 'jar:';
protocols.http = 'http://';
protocols.https = 'https://';
protocols.scp = 'scp://';
protocols.gzip = 'gzip:';

if length(varargin) == 0
  prot = fieldnames(protocols);
elseif isempty(varargin{1})
  prot = fieldnames(protocols);
else
  prot = varargin{1};

  if iscell(prot)
    ; % ok, do nothing
  elseif ischar(prot)
    prot = {prot};
  else
    error('incorrect parameters');
  end

  % check protocol(s)
  for n = 1:numel(prot)
    if ~isfield(protocols, prot{n});
      error(sprintf('unknown protocol (was ''%s'')', prot{n}));
    end
  end
end



r = logical(0);
p = '';

for n = 1:numel(prot)
  m = findstr(getfield(protocols, prot{n}), s);
  if numel(m) >= 1
    if m(1) == 1
      r = logical(1);
      p = prot{n};
      return
      
    end
  end
end
