function r = url_cachedir
%URL_CACHEDIR  Return the name of the cache directory used by URL_TOOLS.
%
%   r = URL_CACHEDIR
%   r: Directory used by URL_TOOLS for caching files (CHAR)
%
% URL_CACHEDIR examines the environment variable "URLTOOLS_CACHEDIR", and if
% set and is non-empty it is taken to be the cache directory to use. If
% the environment variable is missing or empty then the cache directory
% defaults to "urltools_cache" in the TEMPDIR directory. 
%
% See also URL_TOOLS, URL_USECACHE.

r = getenv('URLTOOLS_CACHEDIR');
if ~isempty(r)
  return
end

r = fullfile(tempdir, 'urltools_cache');
