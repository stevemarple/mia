function [status,mesg] = url_post(url, file, postparams, varargin)
%URL_POST  Send data with a HTTP(S) POST command.
%
%   [status, mesg] = URL_POST(url, file, postparams, ...)
%   status: 0 if success
%   mesg: error message, empty if success
%   url: URL, http or https only (CHAR)
%   file: local file (CHAR)
%   postparams: CELL array of name value pairs. 
%
% If URL_POST is called without any output arguments then an ERROR message
% is automatically generated if the details cannot be posted. When called
% with output arguments URL_POST silently ignores errors and sets status and
% mesg. The following name/value pairs maybe be given to alter the default
% behaviour:
%
%   'verbosity', NUMERIC
%   Adjust the information printed by URL_POST. If verbosity is 0 then no
%   information(except errors) is printed. If verbosity is 1 then the URL
%   being fetched is displayed. Higher verbosity levels may produce more
%   output.
%
% Note that URL_POST nevers uses the cache.
%
% See also URL_TOOLS, URL_PUT, URL_EXIST, URL_FETCH, URL_FOPEN, URL_LOAD, 
% URL_SAVE, URL_PARTS.

defaults.verbosity = 1;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

[exec opt] = url_executable;
exec_name = basename(exec);

% allow the URL to be altered
url = url_mangle(url, basename(mfilename));

if ~isurl(url, {'http' 'https'})
  error('URL must be for HTTP or HTTPS protocol');
end
if rem(numel(postparams), 2)
  error('post parameters must contain an even number of elements');
end

p = url_parts(url);

% encode the post parameters
postparams_enc = url_encode(postparams, 1);

if defaults.verbosity
  disp(['posting to ' url]);
end

switch exec_name
 case 'curl'
  % use curl to do posting
  
  switch defaults.verbosity
   case {0 1}
    verbflag = '--silent';
    
   case 2
    verbflag = '';
      
   otherwise
    verbflag = '--verbose';
  end
  
  % create data options
  dataopts = cell(1, numel(postparams_enc) / 2);
  for n = 1:length(dataopts)
    tmp = sprintf('%s=%s', postparams_enc{[n*2 - 1, n*2]});
    dataopts{n} = sprintf('-d %s', systemquote(tmp));
  end
  data = join(' ', dataopts);
  cmd = sprintf('%s %s %s %s --fail -o %s %s', ...
		systemquote(exec), opt, verbflag, data, systemquote(file), ...
		systemquote(url));
  
  [status mesg] = system(cmd);
  mesg = url_getcurlerror(status);
  
 otherwise
  error('unknown post method');

end

if nargout
  % error-checking employed, return whether errors or not
  return
end

if status
  % some error and no error-checking employed by caller so stop here
  error(mesg);
end

