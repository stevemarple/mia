function [status,mesg] = url_fetch(url, file, varargin)
%URL_FETCH  Fetch a file from a URL and save to a local file.
%
%   [status, mesg] = URL_FETCH(url, file, ...)
%   status: 0 if success
%   mesg: error message, empty if success
%   url: URL, http/https/ftp (CHAR)
%   file: local file (CHAR)
%
% If URL_FETCH is called without any output arguments then an ERROR message
% is automatically generated if the URL cannot be fetched. When called with
% output arguments URL_FETCH silently ignores errors and sets status and
% mesg. The following name/value pairs maybe be given to alter the
% default behaviour:
%
%   'usecache', LOGICAL
%   Determines whether or not the file is cached. Default value depends on
%   URL_USECACHE.
%
%   'verbosity', NUMERIC
%   Adjust the information printed by URL_FETCH. If verbosity is 0 then no
%   information(except errors) is printed. If verbosity is 1 then the URL
%   being fetched is displayed. Higher verbosity levels may produce more
%   output.
%
%   'originalurl', CHAR
%   If set the cache filename is ased on the original unmangled
%   URL. Intended for use only by other URL_TOOLS functions.
%
% See also URL_TOOLS, URL_PUT, URL_EXIST, URL_FOPEN, URL_LOAD, URL_SAVE, 
% URL_PARTS, URL_POST.

defaults.usecache = url_usecache;
defaults.originalurl = url;
defaults.verbosity = 1;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if any(url == '?')
  % never, ever cache anything with query-parameters
  defaults.usecache = 0;
end

% allow the URL to be altered
url = url_mangle(url, basename(mfilename));

status = 0;
mesg = '';

if ~logical(defaults.usecache)
  % don't cache, fetch and return
  [status,mesg,cmd] = realurl_fetch(url, file, defaults);
  error_check(nargout, status, mesg, cmd);
  return
end



% Map url to a cache file name
if isempty(defaults.originalurl)
  cachefile = url_makecachename(url);
else
  cachefile = url_makecachename(defaults.originalurl);
end

if isempty(dir(cachefile))
  % cache file does not exist
  
  % ensure target directory exists
  targetdir = fileparts(cachefile);
  
  if ~isdir(targetdir)
    % use our friendly version of mkdir, but the directory must be local,
    % not a URL.
    [success mesg] = url_mkdir(targetdir); 
    if ~success
      status = 1;
      mesg = sprintf(['target directory (''%s'') missing and cannot ' ...
                      'create: %s'], targetdir, mesg);
      return
    end
  end
  
  % download to the cache
  [status, mesg,cmd] = realurl_fetch(url, cachefile, defaults);

  if status
    % Maybe the cache filesystem is full, try again, downloading to actual
    % file. Remove the cachefile as that may not be complete.
    
    if ~isempty(dir(cachefile))
      delete(cachefile);
    end
    [status, mesg,cmd] = realurl_fetch(url, file, defaults);
  end

  error_check(nargout, status, mesg, cmd);
end

[status mesg] = url_copyfile(cachefile, file);
error_check(nargout, status, mesg, cmd);


% ----------------------------------------------------------------------

function [status,mesg,cmd] = realurl_fetch(url, file, defaults)
%REALURL_FETCH Do the actual downloading. Arguments and return values as
%URL_FETCH.
cmd = '(command not set)';
[isu protocol] = isurl(url);
if isu
  p = url_parts(url);
end

[exec opt] = url_executable;
exec_name = basename(exec);
switch protocol
 case {'file' 'ftp' 'http' 'https'}
  if defaults.verbosity
    disp(['fetching ' url]);
  end
  
  switch exec_name
   case 'wget'
    cmd = sprintf('%s %s -q -O %s %s', systemquote(exec), opt, ...
		  systemquote(file), systemquote(url));
    [status mesg] = system(cmd);
    
    if status 
      % wget returned an error
      if isempty(mesg)
        mesg = '<unknown error>';
      elseif isequal(mesg(end), char(10))
        % trim newline
        mesg(end) = [];
      end
    end
  
   case 'curl'
    switch defaults.verbosity
     case {0 1}
      verbflag = '--silent';
      
     case 2
      verbflag = '';
      
     otherwise
      verbflag = '--verbose';
    end
    
    cmd = sprintf('%s %s %s --fail -o %s %s', ...
		  systemquote(exec), opt, verbflag, systemquote(file), ...
		  systemquote(url));
    
    [status mesg] = system(cmd);
    mesg = url_getcurlerror(status);
   otherwise
    error('unknown fetch method');
  end

  if defaults.verbosity >= 2
    disp(sprintf('url_fetch command: %s', cmd));
  end
  if status == 0 & isempty(dir(file))
    % for some reason the file doesn't exist
    status = 1;
    mesg = sprintf('could not fetch %s', url);
  end
  
 case 'scp'
  port = '';
  if isfield(p, 'port')
    % not certain if and how ports should be specified
    if ~isempty(p.port)
      cmd = [cmd ' -p ' p.port];
    end
  end
  if isempty(p.username)
    username_hostname = p.hostname;
  else
    username_hostname = [p.username '@' p.hostname];
  end

  cmd = sprintf('scp %s %s %s', port, ...
		systemquote([username_hostname ':' p.pathandfile]), ...
		systemquote(file));
  [status w] = system(cmd);
  if status ~= 0
    mesg = sprintf('Could not downloaded %s to %s', url, file);
    if ~isempty(w)
      mesg = [mesg ':' w];
    end
  else
    mesg = '';
  end
 
 case 'jar'
  p = url_parts(url);
  % fetch the archive
  tmpfile = tempname2;
  [status mesg] = url_fetch(p.jarfile, tmpfile, ...
			    'verbosity', 0);
  if ~status
    cmd = sprintf('unzip -p %s %s > %s', systemquote(tmpfile), ...
		  systemquote(p.entry), systemquote(file));
    [status mesg] = system(cmd);
  end
  
  if url_exist(tmpfile)
    delete(tmpfile);
  end
  
 case 'gzip'
  p = url_parts(url);
  if isurl(p.pathandfile)
    % fetch gzip file
    tmpfile = tempname2;
    [status mesg] = url_fetch(p.pathandfile, tmpfile, ...
			      'verbosity', 0);
    if ~status
      cmd = sprintf('zcat %s > %s', systemquote(tmpfile), ...
		    systemquote(file));
      [status mesg] = system(cmd);
    end
    if url_exist(tmpfile)
      delete(tmpfile);
    end
  else
    if url_exist(p.pathandfile)
      cmd = sprintf('zcat %s > %s', systemquote(p.pathandfile), ...
		    systemquote(file));
      [status mesg] = system(cmd);
    else
      status = 1;
      mesg = 'No such file or directory';
    end
  end
  
 case ''
  % invalid protocol
  status = 1;
  mesg = sprintf('%s does not appear to be a valid URL', url);

 otherwise
  status = 1;
  mesg = sprintf('download not implemented for protocol %s', protocol);
end
 

function error_check(narg, status, mesg, cmd)
if narg
  % error-checking employed, return whether errors or not
  return
end

if status
  % An error occurred but no error-checking employed by caller so stop
  if isempty(mesg)
    mesg = 'Unknown error';
  end
  mesg = sprintf('%s\nCommand was %s', mesg, cmd)
  error(mesg);
end

