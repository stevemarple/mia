function url_save(varargin)
%URL_SAVE  Save workspace variables, possible via a URL.
%
% URL_SAVE is designed to be a plug-in replacement for SAVE, which is able to
% save to local files or remote files as desginated by a URL. See SAVE for
% details.
%
% See also URL_TOOLS, SAVE, URL_LOAD, LOAD, URL_FETCH, URL_PUT.


if nargin == 0
  % let matlab save everything in caller's workspace to matlab.mat
  evalin('caller', 'save'); 
  return
end

if ~isurl(varargin{1})
  s = sprintf('save %s', join(' ', varargin));
  evalin('caller', s);
  return
end

originalurl = varargin{1};

% allow the URL to be altered
url = url_mangle(originalurl, basename(mfilename));


varargin{1} = tempname2('', '.mat');
s = sprintf('save %s', join(' ', varargin));
evalin('caller', s);
url_put(varargin{1}, url, ...
        'originalurl', originalurl);

