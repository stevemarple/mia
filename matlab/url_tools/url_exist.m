function r = url_exist(url)
%URL_EXIST  Check a URL is valid
%
%   r = URL_EXIST(url)
%   r: LOGICAL value, true if URL exists
%   url: Any supported URL (see ISURL), or a plain filename
%
% URL_EXIST tests if files, directories and URLs exist. There are some
% important differences from the standard EXIST function:
%
% 1. URL_EXIST looks only for files, directories and URLs which match. It
% does not look for variables or Java classes which match. 
%
% 2. The filename or URL passed to URL_EXIST must be an exact match to an
% existing file or URL; URL_EXIST('test') will not find test.m or test.mat.
%
% 3. The return parameter is a logical value. (EXIST returns a a numeric
% value, identifying the type of match found).
%
% See also URL_TOOLS, EXIST, URL_FOPEN, URL_FETCH.

if exist('url_mangle.m')
  % allow the URL to be altered
  url = url_mangle(url, basename(mfilename));
end

[isu protocol] = isurl(url);

[exec opt] = url_executable;
exec_name = basename(exec);

switch protocol
 case ''
  % plain file
  r = ~isempty(dir(url));
  
 case 'file'
  r = ~isempty(dir(url(6:end)));

 case {'ftp' 'http' 'https'}
  switch exec_name
   case 'wget'
    cmd = sprintf('%s %s -q --spider %s', systemquote(exec), opt, ...
		  systemquote(url));
    [status mesg] = system(cmd);
    r = (status == 0);
   case 'curl'
    cmd = sprintf('%s %s -i --silent --fail --head %s', ...
		  systemquote(exec), opt, systemquote(url));
    [status mesg] = system(cmd);
    r = (status == 0);
   otherwise
    error('no way to check if URL exists');
  end
  
 case 'scp'
  % Try SSH'ing into host and then do an ls. This assumes a POSIX
  % compatible remote host
  p = url_parts(url);
  if isempty(p.pathandfile) | strcmp(p.pathandfile, '/')
    r = logical(0);
    return;
  end
  
  cmd = 'ssh ';
  if isfield(p, 'port')
    % not certain if and how ports should be specified
    if ~isempty(p.port)
      cmd = [cmd ' -p ' p.port ' '];
    end
  end
  if ~isempty(p.username)
    cmd = [cmd p.username '@'];
  end
  cmd = sprintf('%s%s /bin/ls -d %s', ...
		cmd, p.hostname, systemquote(p.pathandfile));
  [status w] = system(cmd);
  r = (status == 0);
  
 case 'jar'
  p = url_parts(url);
  % fetch the archive
  file = tempname2;
  [status mesg] = url_fetch(p.jarfile, file, ...
			    'verbosity', 0);
  if status
    % archive does not exist
    r = false;
    return
  end

  cmd = sprintf('unzip -t %s %s', ...
		systemquote(file), systemquote(p.entry));
  
  [status w] = system(cmd);
  r = (status == 0);
  
 case 'gzip'
  p = url_parts(url);
  % Take the path and file part and see if that exists. 
  r = url_exist(p.pathandfile);
  
 otherwise 
  error(sprintf('unknown protocol (was ''%s'')', protocol));
end

