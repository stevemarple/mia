function r = tempname2(varargin)
%TEMPNAME2  An improved method to get a temporary filename.
%
%   r = TEMPNAME2;
%   r = temporary file name in TEMPDIR (CHAR)
%
%   r = TEMPNAME2(dir);
%   r = temporary file name in dir (CHAR)
%   dir: alternative directory to use, which must exist (CHAR)
%
%   r = TEMPNAME2(dir, ext);
%   r = temporary file name in dir (CHAR)
%   dir: alternative directory to use, which must exist (CHAR)
%   ext: filename extension (including any leading '.', if required)
%
% TEMPNAME2 is an improved version of TEMPNAME, and can directly replace
% TEMPNAME.
%
% If dir is empty then TEMPDIR is used. Use '.' to specify the
% current directory. TEMPDIR tests if the filename it produces
% exists, if so new names are produced until one is found which does not
% exist on the selected directory.
%
% See also URL_TOOLS, TEMPNAME.

d = '';
ext = '';

switch length(varargin)
 case 0
  ;
  
 case 1
  d = varargin{1};
  
 case 2
  d = varargin{1};
  ext = varargin{2};
  
 otherwise
  error('incorrect parameters');
end

if isempty(d)
  d = tempdir;
end
if ~isdir(d)
  error(sprintf('%s is not a directory'));
end

while 1
  t0 = clock;
  t0(6) = fix(t0(6) * 100);
  while 1
    % loop until next hundredth of a second
    t = clock;
    t(6) = fix(t(6) * 100);
    if t0(6) ~= t(6)
      break;
    end
  end
  
  r = fullfile(d, [sprintf('%04d%02d%02d%02d%02d%04d', t) ext]);
  % check that the file does not exist
  if isempty(dir(r))
    break;
  end
end
