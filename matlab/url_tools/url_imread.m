function [a,status,mesg] = url_imread(url, varargin)
%URL_IMREAD  Read an image from a file or URL.
%
%   [a, status, mesg] = URL_IMREAD(url, ...)
%   a: image
%   status: 0 if success, 1 if failure
%   mesg: error mesg (CHAR), empty is success
%
% URL_IMREAD Is a URL-enabled version of IMREAD, with the option of reading
% from remote files via a URL. It also returns error status. If there are
% less than two return parameters it calls ERROR on errors, otherwise status
% is 1 and an appropriate error message is given.
%
% See also URL_TOOLS, IMREAD, URL_IMWRITE.

a = [];
status = 0;
mesg = '';

if isurl(url)
  originalurl = url;
  url = url_mangle(originalurl, basename(mfilename));

  % download to a temporary file with the same extension as the URL
  up = url_parts(url);
  [p f e] = fileparts(up.pathandfile);
  file = tempname2('', e);
  [status mesg] = url_fetch(url, file, ...
                            'originalurl', originalurl);
  
  if status
    if nargout > 1
      % caller is checking for errors
      return
    end
    error(mesg);
  end
  
  a = imread(file, varargin{:});
  delete(file);
else
  a = imread(url, varargin{:});
end

