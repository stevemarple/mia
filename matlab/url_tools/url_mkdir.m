function [success, mesg] = url_mkdir(dname)
%URL_MKDIR Make a directory structure.
%
%   [success, mesg] = URL_MKDIR(dname)
%   sucess: 1 if successful, 0 otherwise
%   mesg: text message returned by url_mkdir 
%   dname: directory structure to make
%
% URL_MKDIR is similar to the Matlab MKDIR command, but handles the case
% when dname is actually a URL. Unlike the matlab command there is no need
% to work out what the parent directory is. 
%
% If URL_MKDIR is called without any output arguments then an ERROR message
% is automatically generated if the directory cannot be made. When called
% with output arguments URL_MKDIR silently ignores errors and sets success
% and mesg.
%
% See also URL_TOOLS, MKDIR.

success = logical(1);
mesg = '';

if isempty(dname)
  % easy, no directory to make!
  return 
end

if exist('url_mangle.m')
  % allow the URL to be altered
  dname = url_mangle(dname, basename(mfilename));
end


[isu protocol] = isurl(dname);
if isu
  p = url_parts(dname);
end

switch protocol
 case {'' 'file'}
  if strcmp(protocol, 'file')
    dname = p.pathandfile;
  end
  % avoid unwanted warning messages
  if isdir(dname)
    return
  end
  % % parent directory for mkdir doesn't need to be an existing dir, just
  % % the parent of the last part (odd - why not just accept the name of
  % % the directory the user wants to create)?
  % [pa fi ex] = fileparts(dname)
  % [success mesg] = mkdir(pa, [fi ex])

  % The only reliable way to do this in both Matlab and Octave is to make
  % a system call
  if ispc
    cmd = sprintf('mkdir %s', systemquote(dname));
  else
    cmd = sprintf('mkdir -p %s', systemquote(dname));
  end
  [status mesg] = system(cmd);
  success = (status == 0);
  
 case {'ftp' 'http' 'https'}
  success = logical(0);
  mesg = sprintf('Cannot create directories for %s protocol', protocol);
 
 case 'scp'
  cmd = 'ssh ';
  if isfield(p, 'port')
    % not certain if and how ports should be specified
    if ~isempty(p.port)
      cmd = [cmd ' -p ' p.port ' '];
    end
  end
  if ~isempty(p.username)
    cmd = [cmd p.username '@'];
  end
  cmd = sprintf('%s%s mkdir -p ''%s''', cmd, p.hostname, p.pathandfile);
  [status w] = system(cmd);
  if status 
    % ssh returned an error
    if isempty(mesg)
      mesg = '<unknown error>';
    elseif isequal(mesg(end), char(10))
      % trim newline
      mesg(end) = [];
    end
  end
  success = (status == 0);
end


if nargout == 0 & ~success
  % error condition but return argument not checked
  error(['url_mkdir: ' mesg]);
end
