function r = url_makecachename(url)
%URL_MAKECACHENAME  Convert a URL to a local filename for the cache.
%
%   r = URL_MAKECACHENAME(url)
%   r: local filename (CHAR)
%   url: URL (CHAR)
%
% URL_MAKECACHENAME converts a URL to the name of a file on the local file
% system. If the URL has not been cached the file will not exist. The cache
% directory is given by URL_CACHEDIR.
%
% See also URL_TOOLS, URL_FETCH.


% Map url to a cache file name. It is very easy to get a URL which is too
% long to be a filename, hopefully by breaking it up into directories the
% problem can be reduced. Some characters are not permitted and others have
% a special meaning to the shell, so remove anything which is harmful. Work
% by breaking up URL into component parts, at '/', '?', '&' or ';' then
% URL-encode.
url2 = url;
url2(url2 == '?') = '/';
url2(url2 == '&') = '/';
url2(url2 == ';') = '/';

% URL-encode the string. Use safemode so that characters which are
% undesirable in system commands are encoded.
up = url_encode(split('/', url2), 1);
r = fullfile(url_cachedir, up{:});
