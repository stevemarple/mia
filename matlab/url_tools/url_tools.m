%URL_TOOLS  Tools to URL-enable matlab functions.
%
% URL_TOOLS is a set of functions which mimic or extend standard Matlab
% functions in order to handle URLs and plain files. In most cases they are
% direct replacements for the corresponding Matlab function. Certain
% functions (eg URL_FOPEN) perform better than the standard Matlab function,
% such as by checking for error conditions and calling ERROR if the calling
% function has not requested sufficient return parameters to perform error
% checking itself.
%
% Remote resources are downloaded by URL_FETCH, which can optionally cache
% the files locally. Files are uploaded by URL_PUT (and the cached file
% updated, if it exists); note that most web servers are not configured to
% accept PUT commands. However, the function URL_MANGLE can be used so that
% matching URLs can be 'mangled' from one protocol (eg http) to another (eg
% scp) in order that the file can be transferred. Users should define their
% own URL_MANGLE function to perform any URL mangling. Note that in some
% cases if the mangled URL is mapped to a file then the cached file may
% not be updated.
%
% The cache directory can be configured by setting the environment variable
% "URLTOOLS_CACHEDIR", or by modifying URL_CACHEDIR. The cache can be
% enabled and disabled with the command URL_USECACHE. URL_FETCH and URL_PUT
% can also be told to use/not use the cache for specific requests. Note that
% URL_POST never uses the cache. Files are copied to/from the cache using
% the URL_COPYFILE command. If users are concerned about file permissions on
% the copied files then users may overload URL_COPYFILE with a version which
% sets permissions to their liking.
%
% See also URL_ENCODE, URL_EXECUTABLE, URL_EXIST, URL_FETCH, URL_FOPEN,
% URL_IMWRITE, URL_LOAD, URL_MKDIR, URL_PARTS, URL_PUT, URL_READ, URL_SAVE,
% URL_WHO, URL_WHOS.
