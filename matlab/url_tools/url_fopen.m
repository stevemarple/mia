function [fid, mesg] = url_fopen(varargin)
%URL_FOPEN  Convenient replacement for FOPEN to handle URLs.
%
%   [fid mesg] = URL_FOPEN(...)
%
% Equivalent to URL_FOPEN but accept URLs as valid filenames, transparently
% transferring remote files to a temporary file which is DELETEd on calling
% FCLOSE. Plain filenames and URLs which use the 'file:' protocol may be
% opened in any valid mode, all other URLs must be opened in read mode ('r'
% or 'rt').
%
% Opening files is essentially the same as for FOPEN, but errors are checked
% automatically. If an error opening the file occurs then ERROR is called
% with an appropriate error message.
%
% When URLs are opened (except 'file:' URLs) fid is an object of type
% URL_HANDLE. All valid file operations are overloaded. In addition,
% comparison (EQ and NE) with numeric values is also allowed, in order to
% test if URL_FOPEN failed (fid tests equal to -1).
%
% See also URL_TOOLS, FOPEN, ISURL.

switch nargin
 case 0
  error('filename is required');
  
 case 1
  % mode defaults to read, but we need a permission to check against
  varargin{2} = 'r'; 
  
end

mesg = '';

% allow the URL to be altered
originalurl = varargin{1};
url = url_mangle(originalurl, basename(mfilename));


[isu protocol] = isurl(url);

switch protocol
 case ''
  % not a URL
  ;
 case 'file'
  url = url(6:end);
  
 otherwise
  % URL, but not 'file:'
  [fid mesg] = url_handle({url originalurl}, varargin{2:end});
  if fid == -1 
    if strcmp(originalurl, url)
      mesg = sprintf('Could not open %s: %s', originalurl, mesg); 
    else
      mesg = sprintf('Could not open %s (mapped to %s): %s', ...
                     originalurl, url, mesg); 
    end
    if nargout < 2
      error(mesg);
    end
  end
  return
  
end

[fid mesg] = fopen(url, varargin{2:end});
if fid == -1 
  if strcmp(originalurl, url)
    mesg = sprintf('Could not open %s: %s', originalurl, mesg); 
  else
    mesg = sprintf('Could not open %s (mapped to %s): %s', ...
                   originalurl, url, mesg); 
  end

  if nargout < 2
    error(mesg);
  end
end
