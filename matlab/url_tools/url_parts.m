function r = url_parts(s)
%URL_PARTS  Split a URL into component parts
%
%   r = URL_PARTS(s)
%   r: STRUCT, whose FIELDNAMES are dependent upon the protocol
%   s: CHAR
%
% See also URL_TOOLS, ISURL.

% find first ':'
noturl = 'Not a valid URL';

[r.protocol rest] = local_split_once(':', s);
if isempty(s)
  error(noturl);
end

r.protocol = lower(r.protocol);
switch r.protocol
 case 'file'
  r.pathandfile = rest;
  [r.path, r.name, r.ext] = fileparts(rest);
  r.ver = []
  
 case {'ftp' 'http' 'https'}
  % check the '//' exists
  if ~strcmp(rest(1:2), '//')
    error(sprintf('%s protocol requires // before hostname', r.protocol));
  end
  rest(1:2) = [];
  
  [user_host_port filepart] = local_split_once('/', rest);
  r = local_split_user_host_port(r, user_host_port);
  if ~isempty(filepart)
    filepart = ['/' filepart];
  end
  r.pathandfile = filepart;

 case 'scp'
  % Format is scp://user@hostname:path/and/file
  % check the '//' exists
  if ~strcmp(rest(1:2), '//')
    error(sprintf('%s protocol requires // before hostname', r.protocol));
  end
  rest(1:2) = [];
  
  [user_host_port filepart] = local_split_once(':', rest);
  r = local_split_user_host_port(r, user_host_port);
  r = rmfield(r, {'password' 'port'});
  r.pathandfile = filepart;
  
 case 'jar'
  % Java archive - a ZIP file
  [r.jarfile r.entry] = local_split_once('!/', rest);

 case 'gzip'
  r.pathandfile = rest;
  
 otherwise
  error(sprintf('unknown protocol (was ''%s'')', r.protocol));
end

% Given a char to use, split a string into two on the first occurence
% of c. If c does not occur a==s and b == ''
function [a, b] = local_split_once(c, s)
if isempty(s)
  a = '';
  b = '';
else
  len = numel(c);

  n = findstr(c, s);
  
  if isempty(n)
    a = s;
    b = '';
  else
    a = s(1:(n(1)-1));
    b = s((n(1)+len):end);
    if isempty(b)
      b = ''; % character we checked for is at end of the string
    end
  end
    
end


% Given a struct and a string split the sting into user, host and port
% parts. Set corresponding fields in the struct and return modified struct
function r = local_split_user_host_port(stru, s)

r = stru;

[a b] = local_split_once('@', s);
if isempty(b)
  username = '';
  rest = a;
else
  username = a;
  rest = b;
end

% username part could contain a password
[r.username r.password] = local_split_once(':', username);

[r.hostname r.port] = local_split_once(':', rest);


