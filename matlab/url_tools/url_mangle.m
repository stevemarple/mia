function r = url_mangle(url, func)
%URL_MANGLE Mangle URLs when opening/saving or fetching/putting files.
%
%   r = URL_MANGLE(url, func)
%   r: mangled URL (CHAR)
%   url: original URL (CHAR)
%   func: function name (url_load, url_save, url_exist, url_fopen, fclose
%   or url_mkdir)
%
% URL_MANGLE is not intended to be called directly. The urltools functions
% which open, save, fetch or put files should automatically call
% URL_MANGLE.
%
% This version of URL_MANGLE does not mangle URLs, but users may copy and
% modify it to mangle URLs.
%
% See also URL_TOOLS, URL_LOAD, URL_SAVE, URL_FOPEN, URL_EXIST, FCLOSE.

r = url;
return


% Below is some example code

switch func
 case {'url_load' 'url_fopen' 'url_exist'}
  % don't mangle when opening files or testing existence
  
 case {'url_save' 'fclose' 'url_mkdir' 'url_put' 'url_imwrite'}
  % cannot use a HTTP PUT to write to webserver, so use SCP instead
  r = strrep(r, ...
             'http://www.example.com/data/', ...
             'scp://www.example.com/webserver/public_html/data/');

 otherwise
  warning(sprintf('do not know about URL mangling for function %s', func));
  
end
