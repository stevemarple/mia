function varargout = systemquote(varargin)
%SYSTEMQUOTE Quote arguments for a SYSTEM call.
%
%   [r, ...] = SYSTEMQUOTE(s, ...)
%   r: quoted string
%   s: input string
%
% If n input parameters are given then n output parameters are returned

for n = 1:nargin
  s = varargin{n};
  if ispc
    % Use double quotes for Windows
    varargout{n} = sprintf('"%s"', s);
  else
    % Try to use single quotes if possible, otherwise fall back to using
    % double quotes
    if any(s == '''')
      % String contains single quotes
      
      % Quote with double quotes instead, remembering to escape any
      % double quotes which might exist
      % s = strrep(s, '"', '\"');
      % varargout{n} = sprintf('"%s"', s);
      
      % Escape single quotes from shell, but do need to end and restart
      % the single quotes 
      s = strrep(s, '''', '''\'''''); % ' -> '\''
      varargout{n} = sprintf('''%s''', s);
         
    else
      varargout{n} = sprintf('''%s''', s);
    end
  end
end
