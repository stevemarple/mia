function [a,status,mesg] = url_dlmread(url, varargin)
%URL_DLMREAD  Read ASCII delimited data from a file or URL.
%
%   [a, status, mesg] = URL_DLMREAD(url, ...)
%   a: image
%   status: 0 if success, 1 if failure
%   mesg: error mesg (CHAR), empty is success
%
% URL_DLMREAD Is a URL-enabled version of DLMREAD, with the option of
% reading from remote files via a URL. It also returns error status. If
% there are less than two return parameters it calls ERROR on errors,
% otherwise status is non-zero and an appropriate error message is given.
%
% See also URL_TOOLS, DLMREAD, URL_IMWRITE.

a = [];
status = 0;
mesg = '';

if isurl(url)
  originalurl = url;
  url = url_mangle(originalurl, basename(mfilename));

  % download to a temporary file
  file = tempname2;
  [status mesg] = url_fetch(url, file, ...
                            'originalurl', originalurl);
  
  if status
    if nargout > 1
      % caller is checking for errors
      return
    end
    error(mesg);
  end
  
  a = dlmread(file, varargin{:});
  delete(file);
else
  a = dlmread(url, varargin{:});
end

