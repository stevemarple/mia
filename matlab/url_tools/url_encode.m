function r = url_encode(s, varargin)
%URL_ENCODE  URL-encode a string.
%
%   r = URL_ENCODE(s)
%   r = URL_ENCODE(s, safemode)
%   r: URL-encoded string (CHAR or CELL)
%   s: string to encode (CHAR or CELL)
%   safemode: make string safe for use in SYSTEM commands (LOGICAL)
%
% URL_ENCODE takes a string and encodes it into a string suitable for
% passing to URL_FETCH. s may be a CELL array of CHARs, in which case r is
% also a CELL array of the same size. 
%
% Normally URL_ENCODE will encode all non-alphanumeric characters except for
% ";", "$", "-", "_", ".", "+", "!", "*", "'", "(", ")" and ",". In safe
% mode and characters which may be dangerous in SYSTEM commands (for either
% UNIX os DOS) are also hex-encoded, the only non-alphanumeric characters
% not encoded are "-", "_", ".", "+" and ",".
%
% See also URL_TOOLS, URL_FETCH.

if iscell(s)
  r = cell(size(s));
  for n = 1:numel(s)
    r{n} = feval(mfilename, s{n}); % make recursive function call
  end
  return
end

% pre-allocate some space, the final string will be at least as long as s
r = s;

safemode = 0;
if length(varargin)
  safemode = logical(varargin{1});
end
if safemode
  safechars = ['0123456789' ...
               'abcdefghijklmnopqrstuvwxyz' ...
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ...
               '-_.+,'];
else
  safechars = ['0123456789' ...
               'abcdefghijklmnopqrstuvwxyz' ...
               'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ...
               ';$-_.+!*''(),'];
end

rn = 1;
for sn = 1:numel(s)
  cin = s(sn);
  
  if any(cin == safechars)
    % character is allowed unencoded
    cout = cin;
  else
    % must encode character
    cout = ['%' lower(dec2hex(cin))];
  end
  coutlen = length(cout);
  r(rn:(rn+coutlen-1)) = cout;
  rn = rn + coutlen;
end
