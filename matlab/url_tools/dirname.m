function r = dirname(filename)
%DIRNAME  Return the dirname of a file
%
%   r = DIRNAME(s)
%   r: CHAR, dirname of file with path and extension removed
%   s: CHAR, filename
%
% See also URL_TOOLS, FILEPARTS.

r = fileparts(filename);

