function [status,mesg] = url_imwrite(a, url, varargin)
%URL_IMWRITE  Write an image to a file or URL.
%
%   [status mesg] = url_imwrite(a, url, ...)
%   status: 0 if success, 1 if failure
%   mesg: error mesg (CHAR), empty is success
%
% URL_IMWRITE Is a URL-enabled version of IMWRITE, with the option of
% saving to remote files via a URL. It also returns error status. If
% there are no return parameters it calls ERROR on errors, otherwise
% status is 1 and an appropriate error message is given.
%
% See also URL_TOOLS, IMWRITE, URL_IMREAD.

status = 0;
mesg = '';

if isurl(url)
  originalurl = url;
  url = url_mangle(url, basename(mfilename));
  
  % url_mkdir(fileparts(url));
  
  % create a temporary file with the same extension as the URL
  up = url_parts(url);
  [p f e] = fileparts(up.pathandfile);
  file = tempname2('', e);
  
  imwrite(a, file, varargin{:});
  [status mesg] = url_put(file, url, ...
                          'originalurl', originalurl);
  delete(file);
else
  % url_mkdir(fileparts(url));
  imwrite(a, url, varargin{:});
end


if nargout
  % checking for errors
  return
end

if status
  error(mesg);
end
