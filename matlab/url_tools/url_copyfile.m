function [status,mesg] = url_copyfile(src, dest)
%URL_COPYFILE  URL_TOOLS File copy command.
%
%   [status, mesg] = URL_COPYFILE(src, dest)
%   status: 0 for success, 1 if error
%   mesg: error mesg if copy failed
%   src: source filename (CHAR)
%   dest: destination filename (CHAR)
%
% URL_COPYFILE copies a file from src to dest, functioning like the standard
% Matlab COPYFILE command. A separate function is used so that users
% concerned about file permissions on the files created in the URL_TOOLS
% cache directory (see URL_CACHEDIR) can overload URL_COPYFILE with their
% own version which sets the appropriate permissions.
%
% If URL_COPYFILE is called without any output arguments then ERROR is
% automatically called if the file cannot be copied. When URL_COPYFILE is
% called with output arguments ERROR is not called, status and mesg are set
% appropriately.
%
% See also URL_TOOLS.

if ispc
  cmd = sprintf('copy %s %s', systemquote(src), systemquote(dest));
else
  cmd = sprintf('cp %s %s', systemquote(src), systemquote(dest));
end
[status mesg] = system(cmd);
if status
  if status 
    % system returned an error
    if isempty(mesg)
      mesg = '<unknown error>';
    elseif isequal(mesg(end), char(10))
      % trim newline
      mesg(end) = [];
    end
  end
end
