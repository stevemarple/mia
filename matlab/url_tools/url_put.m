function [status,mesg] = url_put(file, url, varargin)
%URL_PUT Upload a file.
%
%   [status mesg] = URL_PUT(file, url)
%   status: 0 if success
%   mesg: error message, empty is success
%   file: local file (CHAR)
%   url: URL (CHAR)
%
% If URL_PUT is called without any output arguments then an ERROR message is
% automatically generated if the file cannot be uploaded. When called with
% output arguments URL_PUT silently ignores errors and sets status and
% mesg. The following name/value pairs maybe be given to alter the
% default behaviour:
%
%   'usecache', LOGICAL
%   If set and the corresponding cache file exists then the cached file is
%   updated. Default value depends on URL_USECACHE.
%
%   'originalurl', CHAR
%   If set the cache filename is ased on the original unmangled
%   URL. Intended for use only by other URL_TOOLS functions.
%
% See also URL_TOOLS, URL_FETCH.

defaults.usecache = url_usecache;
defaults.originalurl = url;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

mesg = '';

[exec opt] = url_executable;
exec_name = basename(exec);

% allow the URL to be altered
url = url_mangle(url, basename(mfilename));


[isu protocol] = isurl(url);
if isu
  p = url_parts(url);
end

switch protocol
 case 'file'
  % check file is not a directory
  d = dir(file);
  if isempty(d)
    status = 1;
    mesg = sprintf('%s does not exist', file);
  elseif numel(d) ~= 1
    % directory or wildcard in filename
    status = 1;
    mesg = sprintf('%s is not a file', file); 
  elseif d.isdir
    % could have adirectory with no files in it
    status = 1;
    mesg = sprintf('%s is a directory', file); 
  else
    [success mesg] = copyfile(file, p.pathandfile);
    status = ~success;
  end
  
 case {'http' 'https' 'ftp'}
  % not tested over HTTP/HTTPS due to not having a server which accepts
  % PUT. FTP works.
  cmd = sprintf('%s %s --silent --fail --upload-file %s %s', ...
		systemquote(exec), opt, systemquote(file), systemquote(url));
  [status mesg] = system(cmd);
  mesg = url_getcurlerror(status);
  
 case 'scp'
  port = '';
  if isfield(p, 'port')
    % not certain if and how ports should be specified
    if ~isempty(p.port)
      cmd = [cmd ' -p ' p.port];
    end
  end
  if isempty(p.username)
    username_hostname = p.hostname;
  else
    username_hostname = [p.username '@' p.hostname];
  end
  
  cmd = sprintf('scp %s %s %s', port, systemquote(file), ...
		systemquote([username_hostname ':' p.pathandfile]));
  [status w] = system(cmd);
  if status ~= 0
    mesg = sprintf('Could not uploaded %s to %s', file, url);
    if ~isempty(w)
      mesg = [mesg ':' w];
    end
  end
 
 case ''
  % invalid protocol
  status = 1;
  mesg = sprintf('%s does not appear to be a valid URL', url);

 otherwise
  status = 1;
  mesg = sprintf('upload not implemented for protocol %s', protocol);
end

% Update the cache, but only if told to AND the target file exists
if logical(defaults.usecache)
  % Map url to a cache file name
  if isempty(defaults.originalurl)
    cachefile = url_makecachename(url);
  else
    cachefile = url_makecachename(defaults.originalurl);
  end
  if url_exist(cachefile)
    url_copyfile(file, cachefile);
  end
end


if nargout
  % error-checking employed, return whether errors or not
  return
end

if status
  % some error and no error-checking employed by caller so stop here
  error(mesg);
end
