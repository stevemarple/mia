function [s,status,mesg] = url_read(url)
%URL_READ  Alternative to Matlab's URLREAD which does not require a JVM.
%
% [s status, mesg] = URL_READ(url);
%
% See also URL_TOOLS, URLREAD.

s = '';
status = 1;

[fid mesg] = url_fopen(url, 'r');
if fid == -1
  status = 0;
  if nargout < 2
    error(mesg);
  end
  return
end

s = char(fread(fid, inf, 'uchar'))';
fclose(fid);



