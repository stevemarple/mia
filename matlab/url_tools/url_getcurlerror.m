function r = url_getcurlerror(n)
%URL_GETCURLERROR  Convert curl status code to a readable error message.
%
%   r = URL_GETCURLERROR(s)
%   r: error message (CHAR)
%   s: status code
%
% URL_GETCURLERROR is intended for use by functions which call curl directly.
%
% See URL_TOOLS, URL_FETCH, URL_GET.

s = {
    ['Unsupported protocol. This build  of  curl  has  no support for ' ...
     'this protocol.']; 
    'Failed to initialize.';
    'URL malformat. The syntax was not correct.';
    ['URL  user  malformatted.  The  user-part of the URL syntax was not ' ...
     'correct.'];
    ['Couldn''t resolve proxy. The given proxy host  could not be ' ...
     'resolved.'];
    ['Couldn''t  resolve  host.  The given remote host was  not ' ...
     'resolved.'];
    'Failed to connect to host.';
    'FTP weird server reply. The server sent  data  curl couldn''t parse.';
    'FTP access denied. The server denied login.';
    ' FTP  user/password  incorrect.  Either  one or both were not accepted by the server.';
    ['FTP weird PASS reply. Curl couldn''t parse the reply  sent to the ' ...
     'PASS request.']; 
    ['FTP weird USER reply. Curl couldn''t parse the reply sent to the ' ...
     'USER request.'];
    ['FTP weird PASV reply, Curl couldn''t parse the reply sent to the ' ...
     'PASV request.'];
    

    'FTP  weird 227 format.  Curl  couldn''t  parse the the server sent.';
    ['FTP can''t get host. Couldn''t resolve the host IP we got in the ' ...
     '227-line.']; 
    ['FTP  can''t  reconnect. Couldn''t connect to the host we got in the ' ...
     '227-line.']; 
    ['FTP couldn''t set binary. Couldn''t  change  transfer method to ' ...
     'binary.']; 
    'Partial  file.  Only  a part of the file was transfered.';
    ['FTP couldn''t download/access the  given  file,  the RETR ' ...
     '(or similar) command failed.']; 
    'FTP  write  error. The transfer was reported bad by the server.';
    ['FTP quote error. A  quote  command  returned  error from the ' ...
     'server.'];
    'HTTP  not  found. The requested page was not found.';
    ['Write error. Curl couldn''t write data  to a local filesystem or ' ...
     'similar.']; 
    'Malformat user. User name badly specified.';
    'FTP  couldn''t STOR file. The server denied the STOR operation.';
    'Read error. Various reading problems.';
    'Out of memory. A memory allocation request  failed.';
    ['Operation  timeout.  The  specified time-out period was reached ' ...
     'according to the conditions.'];
    'FTP couldn''t set  ASCII.  The  server  returned  an unknown reply.';
    'FTP PORT failed. The PORT command failed.';
    'FTP couldn''t use REST. The REST command failed.';

    ['FTP couldn''t use SIZE. The SIZE command failed. The command is an ' ...
     'extension to the  original  FTP  spec RFC 959.'];
    
    'HTTP range error. The range "command" didn''t work.';

    'HTTP post error. Internal  post-request  generation error.';
    'SSL connect error. The SSL handshaking failed.'

    ['FTP bad download resume. Couldn''t continue an earlier aborted ' ...
     'download.'];

    'FILE couldn''t read file. Failed to open  the  file Permissions?';
    
    'LDAP cannot bind. LDAP bind operation failed.';

    'LDAP search failed.';

    'Library  not found. The LDAP library was not found.';

    'Function not found. A required  LDAP  function was not found.'

    ['Aborted  by  callback.  An application told curl to abort the ' ...
     'operation.']; 
    
    'Internal error. A function was called  with  a  bad parameter.'; 

    'Internal  error.  A  function  was  called in a bad order.';
    
    ['Interface error.  A  specified  outgoing  interface could not be ' ...
     'used.'];     
    
    ['Bad  password  entered.  An error was signaled when the password ' ...
     'was entered.'];  

    ['Too many redirects. When following redirects,  curl hit the maximum ' ...
     'amount.'];   
    
    'Unknown TELNET option specified.';
    'Malformed telnet option.';
    
    'Error value unknown';
    
    'The remote peer''s SSL certificate wasn''t ok';
    ['The  server  didn''t  reply  anything, which here is considered an ' ...
     'error.'];
    
    'SSL crypto engine not found';
    
    'Cannot set SSL crypto engine as default';
    'Failed sending network data';
    'Failure in receiving network data';
    'Share is in use (internal error)';
    'Problem with the local certificate';
    'Couldn''t use specified SSL cipher';
    'Problem with the CA cert (path? permission?)';
    'Unrecognized transfer encoding';
    'Invalid LDAP URL';
    'Maximum file size exceeded';
    'The remote server denied curl to login';
};

if n == 0
  r = '';
elseif n > numel(s)
  r = 'Unknown error';
else
  r = s{n};
end
