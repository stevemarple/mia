function fewind(h, varargin)
%FREWIND  FREWIND overloaded for a URL_HANDLE.
%
% See iofun/FREWIND, FOPEN_URL.

frewind(h.fid, varargin{:});
