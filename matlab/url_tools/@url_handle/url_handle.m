function [h,mesg] = url_handle(varargin)
%URL_HANDLE  Constructor for URL_HANDLE class.
%
% URL_HANDLE is a class to encapsulate access to files via
% URLs. URL_HANDLE should not be called directly, see URL_FOPEN.
%
% See also URL_TOOLS, URL_FOPEN.


%   h = URL_HANDLE
%   default constructor
%
%   h = URL_HANDLE(url, ...)
%
% Open a URL. For some protocols (http, https) access may be limited to
% read-only. If the file cannot be transferred (see URL_FETCH) when it is
% needed then -1 is returned, otherwise a URL_HANDLE is returned. When
% FCLOSE is called the temporary file is closed and then deleted.

superiorto('double');

% don't bother storing a version number to allow backward compatibility
% between saved objects - file handles don't have that kind of
% persistence.
% h.versionnumber = 1;
h.url = '';
h.filename = '';
h.mode = '';
h.fid = -1;
h.usecache = url_usecache;
h.originalurl = '';
mesg = '';

cls = 'url_handle';
h = class(h, cls);

if nargin == 0
  % default constructor
  ; % leave as is
  
elseif nargin >= 1 & (ischar(varargin{1}) |  iscell(varargin{1}))
  if ischar(varargin{1})
    % first parameter is URL
    h.url = varargin{1};
    h.originalurl = varargin{1};
  else
    % first parameter contains both URL and original URL before
    % URL_MANGLE was called
    h.url = varargin{1}{1};
    h.originalurl = varargin{1}{2};
  end
    
  h.filename = tempname2;

  if nargin < 2
    % if read mode wasn't explicitly selected then set it ourselves
    varargin{2} = 'r';
  end
  h.mode = varargin{2};

  must_be_present_modes = {'r' 'rt'};
  may_be_present_modes = {'a' 'at' 'a+' 'at+' 'A' 'At'};
  fetch_before_open_modes = {must_be_present_modes{:} may_be_present_modes{:}};
                      
  % uploads are not supported for all protocols, so refuse to open files
  % from those protocols in anything other than read-only mode
  [isu protocol] = isurl(h.url);
  if ~any(strcmp(protocol, {'scp' 'ftp'})) & ~any(strcmp(h.mode, {'r' 'rt'}))
    mesg = sprintf(['upload not supported for %s protocol so %s mode ' ...
                    'not permitted'], protocol, h.mode);
    h = -1;
    if nargout < 2
      error(mesg);
    end
    return
  end
  
  switch h.mode
   case fetch_before_open_modes
    % try fetching before opening file
    [status mesg] = url_fetch(h.url, h.filename, ...
                              'usecache', h.usecache, ...
                              'originalurl', h.originalurl);
    if status & any(strcmp(h.mode, must_be_present_modes))
      % some error, no need to return a url_handle for this, -1 will do
      h = -1;
      if nargout < 2
        error(mesg);
      end
      return
    end
    
   case {'w' 'wt' 'w+' 'wt+' 'W' 'Wt'}
    % no need to fetch before opening local file
    ;
    
   case {'r+' 'rt+'}
    % ensure file NOT present, before opening a local file
    if url_exist(h.url)
      mesg = sprintf('%s already exists', h.url);
      h = -1;
      if nargout < 2
        error(mesg);
      end
      return
    end
    
   otherwise
    error(sprintf('unknown mode (was ''%s'')', h.mode'));
  end

  
  [h.fid mesg] = url_fopen(h.filename, varargin{2:end});
  if h.fid == -1
    % some error, no need to return a url_handle for this, -1 will do
    h = -1;
    if nargout < 2
      error(mesg);
    end
  end
   
else
  error('incorrect parameters');
end


  
