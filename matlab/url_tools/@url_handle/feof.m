function eof = feof(h, varargin)
%FEOF  FEOF overloaded for a URL_HANDLE.
%
% See iofun/FEOF, FOPEN_URL.

eof = feof(h.fid, varargin{:});
