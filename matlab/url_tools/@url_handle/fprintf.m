function count = fprintf(h, varargin)
%FPRINTF  FPRINTF overloaded for a URL_HANDLE.
%
% See iofun/FPRINTF, FOPEN_URL.

count = fprintf(h.fid, varargin{:});
