function [mesg,errnum] = ferror(h, varargin)
%FERROR  FERROR overloaded for a URL_HANDLE.
%
% See iofun/FERROR, FOPEN_URL.

[mesg errnum] = ferror(h.fid, varargin{:});
