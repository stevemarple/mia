function pos = ftell(h, varargin)
%FTELL  FTELL overloaded for a URL_HANDLE.
%
% See iofun/FTELL, FOPEN_URL.

pos = ftell(h.fid, varargin{:});
