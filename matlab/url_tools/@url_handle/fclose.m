function r = fclose(h)
%FCLOSE  Overloaded version of FCLOSE for URL_HANDLE objects.
%
%
% See FCLOSE.

mesg = '';
r = fclose(h.fid);
if r ~= 0
  mesg = sprintf('could not close file %s (local file=%s, fid=%d)', ...
                  h.url, h.filename, h.fid);
  if ~any(strcmp(h.mode, {'r' 'rt'}))
    % no need to comment about this for files not in write mode
    mesg = [mesg ', file NOT uploaded'];
  end
else
  % if necessary transfer the file back
  if ~any(strcmp(h.mode, {'r' 'rt'}))
    % allow the URL to be altered
    url = url_mangle(url, basename(mfilename));
    
    
    [r mesg] = url_put(h.filename, h.url, ...
                       'usecache', h.usecache, ...
                       'originalurl', h.originalurl);
  end
  
  if r == 0
    % Success, delete the temporary file. It's temporary, so don't
    % complain if for some reason it can't be deleted.
    delete(h.filename);
  else
    mesg = sprintf(['Could not upload to %s, not deleting local copy ' ...
                    '(''%s''). Upload error was: %s'], ...
                   h.url, h.filename, mesg);
  end
end

if ~isempty(mesg)
  if nargout == 0
    % status not checked and there are errors so flag them up. This is
    % important because it might mean local files could not transferred.
  else
    % caller is checking for error status but standard fclose does not
    % have an error message return parameter, so generate a warning.
    warn(mesg);
  end
end

