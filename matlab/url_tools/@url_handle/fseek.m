function status = fseek(h, varargin)
%FSEEK  FSEEK overloaded for a URL_HANDLE.
%
% See iofun/FSEEK, FOPEN_URL.

status = fseek(h.fid, varargin{:});
