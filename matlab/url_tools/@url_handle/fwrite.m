function count = fwrite(h, varargin)
%FWRITE  FWRITE overloaded for a URL_HANDLE.
%
% See iofun/FWRITE, FOPEN_URL.

count = fwrite(h.fid, varargin{:});
