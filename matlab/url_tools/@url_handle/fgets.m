function r = fgets(h, varargin)
%FGETS  FGETS overloaded for a URL_HANDLE.
%
% See iofun/FGETS, FOPEN_URL.

r = fgets(h.fid, varargin{:});
