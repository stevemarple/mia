function [a,count] = fscanf(h, varargin)
%FSCANF  FSCANF overloaded for a URL_HANDLE.
%
% See iofun/FSCANF, FOPEN_URL.

[a count] = fscanf(h.fid, varargin{:});
