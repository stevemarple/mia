function r = fgetl(h, varargin)
%FGETL  FGETL overloaded for a URL_HANDLE.
%
% See iofun/FGETL, FOPEN_URL.

r = fgetl(h.fid, varargin{:});
