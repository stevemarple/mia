function [a,count] = fread(h, varargin)
%FREAD  FREAD overloaded for a URL_HANDLE.
%
% See iofun/FREAD, FOPEN_URL.

[a count] = fread(h.fid, varargin{:});
