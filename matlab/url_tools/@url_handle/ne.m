function r = ne(h, d)

if isa(h, 'url_handle')
  r = (h.fid ~= d);
else
  r = (d.fid ~= h);
end




