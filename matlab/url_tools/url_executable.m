function [exec,opt] = url_executable
%URL_EXECUTABLE  Return name of executable to be used for URL_TOOLS.
%
% [exec, opt] = URL_EXECUTABLE
% exec: The curl executable: 'curl', 'c:\windows\curl.exe' or similar.
% opt: Default options for curl, e.g., ''--netrc-optional'
%
% The current version of URL_TOOLS uses curl to access URLs. The default
% string returned by URL_EXECUTABLE is 'curl' but this can be customised to
% the location of your curl executable (required unless the executable is in
% your system path). The default options can also be overridden in the
% same way. Be sure to quote the quote the options if necessary (see
% SYSTEMQUOTE).
%
% See URL_TOOLS.

exec = 'curl';
opt = '--netrc-optional';
