function varargout = url_load(url, varargin)
%URL_LOAD  Load a file, possibly via a URL.
%
% URL_LOAD(url)
% URL_LOAD(url, var1, ...)
% Load data, either from a local file or an URL, optionally loading on
% the named variables
%
% s = URL_LOAD(url)
% Load the variables into a structure.
%
% URL_LOAD is designed to be a plug-in replacement for LOAD, which is able
% to load local files or remote files from a URL. See LOAD for details.
%
% See also URL_TOOLS, URL_SAVE, URL_FETCH, URL_PUT.

originalurl = url;

% allow the URL to be altered
url = url_mangle(url, basename(mfilename));


outputfile = '';
[isu protocol] = isurl(url);
switch protocol
 case ''
  % plain file
  outputfile = url;
  isu = 0;
  
 case 'file'
  outputfile = url(6:end);
  isu = 0;
    
 otherwise
  % get extension of remote file, first remove any query parameters
  qmark = find(url == '?');
  if isempty(qmark)
    url_noquery = url;
  else
    url_noquery = url(1:(qmark(1)-1));
  end

  [urldir urlfile urlext] = fileparts(url_noquery);
  outputfile = tempname2('', urlext);
  url_fetch(url, outputfile, ...
            'originalurl', originalurl);
end

% need to clear as many variables as possible, so store everything we
% need into a struct called 'unlikelyvariablename'. Hope no user ever
% creates a var of that name. Then load up new vars.
unlikelyvariablename.varargin = varargin;
unlikelyvariablename.outputfile = outputfile;
unlikelyvariablename.isurl = isu;
unlikelyvariablename.who = who;
clear(unlikelyvariablename.who{~strcmp(unlikelyvariablename.who, ...
				       'unlikelyvariablename')});

try
  [unlikelyvariablename.varargout{1:nargout}] = ...
      builtin('load', ...
	      unlikelyvariablename.outputfile, ...
	      unlikelyvariablename.varargin{:});
  
  % if any new variables have appeared assign them into the caller's
  % workspace
  unlikelyvariablename.who = who;
  for unlikelyvariablename_n = 1:numel(unlikelyvariablename.who)
    if ~strcmp(unlikelyvariablename.who{unlikelyvariablename_n}, ...
	       'unlikelyvariablename')
      assignin('caller', ...
	       unlikelyvariablename.who{unlikelyvariablename_n}, ...
	       eval(unlikelyvariablename.who{unlikelyvariablename_n}));
    end
  end
  
  varargout = unlikelyvariablename.varargout;
  unlikelyvariablename.lasterror = '';
catch
  unlikelyvariablename.lasterror = lasterror;
end

% delete any temporary files
if unlikelyvariablename.isurl
  if ~isempty(unlikelyvariablename.outputfile)
    if ~isempty(dir(unlikelyvariablename.outputfile))
      delete(unlikelyvariablename.outputfile);
    end
  end
end

if ~isempty(unlikelyvariablename.lasterror)
  rethrow(unlikelyvariablename.lasterror);
end
