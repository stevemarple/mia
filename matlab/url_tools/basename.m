function r = basename(filename, varargin)
%BASENAME  Return the basename of a file
%
%   r = BASENAME(s)
%   r: CHAR, basename of file with path and extension removed
%   s: CHAR, filename
%
%   r = BASENAME(s, 'full')
%   r: CHAR, basename of file with path removed but extension intact
%   s: CHAR, filename

%
% See also URL_TOOLS, FILEPARTS.


[tmp r ext] = fileparts(filename);

switch length(varargin)
 case 0
  ;
 case 1
  if strcmp(varargin{1}, 'full')
    r = [r ext];
  else
    error('unknown option');
  end
  
 otherwise
  error('unknown option');
end
