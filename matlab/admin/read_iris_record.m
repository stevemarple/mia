function [r,mesg] = read_iris_record(fid, delScan0)
%READ_IRIS_RECORD  Read an IRIS data record from a file
%
%   r = READ_IRIS_RECORD(fid, delScan0)
%   r: record (STRUCT)
%   delScan0: (LOGICAL)
%
%
% NOTES
%
% INTERCEPTPROP is not used for performance reasons.
%
% See also PROCESS_IRIS_DATA_FILE.


mesg = '';
% error messages, with no error inserted at start of list. Taken from
% pciris.dat
error_message = {''
		 'System clock has FAILED                     '
		 'No response from antenna CPU                '
		 'Write timeout on COM1 serial port           '
		 'Read timeout on COM2 serial port            '
		 'Write timeout on COM2 serial port           '
		 'Noise generator is ON                       '
		 'Riometer n is NOT working                   '
		 'System is in READ mode                      '
		 'System is in STOP mode                      '
		 'Keyboard is LOCKED                          '
		 'Keyboard is UNLOCKED                        '
		 'Operating system error from MS-DOS          '
		 'Input buffer OVERFLOW on COM1               '
		 'Input buffer OVERFLOW on COM2               '
		 '1/8th second marker was MISSED              '
		 'ILLEGAL keyboard scan code received         '
		 'Input buffer OVERFLOW from the keyboard     '
		 'Error on reading disk                       '
		 'Error on writing disk                       '
		 'UNKNOWN error number                        '
		 'Error reading parameter file                '
		 'Error opening directory: D:\A_00_000        '
		 'Error opening directory: D:\A_00_000        '
		 'Error opening file: F_00_000.000            '
		 'Error opening file: F_00_000.000            '
		 'Error writing file: F_00_000.000            '
		 'Error writing file: F_00_000.000            '
		 'GPS/IRIG-B clock board not responding!      '};
		

r.offset = ftell(fid);

[headerBytes count] = fread(fid, 40, 'uint8');
[progVersion count] = fread(fid, 24, 'char');

year = [10 1] * headerBytes(1:2);
if year > 89
  year = year + 1900;
else
  year = year + 2000;
end

doy = [100 10 1] * headerBytes(3:5);
hour = [10 1] * headerBytes(6:7);
minute = [10 1] * headerBytes(8:9);
second = [10 1] * headerBytes(10:11);
% ignore milliseconds (can it really be milliseconds? (not enough range)

% assuming temp is recorded in range -25 -> +75 (true for us)
r.temperature = (headerBytes(13) / 2.55) - 25;


rxStep = headerBytes(17:24);
if ~isequal(rxStep, [48;49;50;51;52;53;54;55]) % '01234567'
  disp(sprintf('---\nFile offset: %d', r.offset));
  disp('receiver step not in normal order:');
  disp([rxStep']);
  % disp(['receiver step: ' char(rxStep')]);
  % disp(char(progVersion'))
  fseek(fid, 4032, 0);
  mesg = 'receiver step not in normal order';
  if nargout > 1
    return
  else
    error(mesg);
  end
  
  rxStep = [48;49;50;51;52;53;54;55];
end

stationId = headerBytes(25);
stepsPerSecond = headerBytes(26);

spareRX = headerBytes(27);
r.spareRX = headerBytes(27);
% if spareRX ~= 64
  % fseek(fid, 4032, 0);
  % error('Spare receiver in use');
  % warning('Spare receiver in use');
% end

r.system_error_number(1:4) = headerBytes(29:32);
% r.system_error_message{1} = error_message{headerBytes(29) + 1};
% r.system_error_message{2} = error_message{headerBytes(30) + 1};
% r.system_error_message{3} = error_message{headerBytes(31) + 1};
% r.system_error_message{4} = error_message{headerBytes(32) + 1};
for n = 1:4
  if r.system_error_number(n) < 0 ...
	| r.system_error_number(n) >= numel(error_message)
    r.system_error_message{n} = 'Illegal error number';
  else
    r.system_error_message{n} = error_message{r.system_error_number(n) + 1};
  end
end

r.noisegenerator = headerBytes(28);
% if r.noisegenerator
%  fseek(fid, 4032, 0);
%  error('Noise generator switched on');
%end

if headerBytes(33) == 63 % '?'
  r.gpsquality = -1;
else
  r.gpsquality = headerBytes(33) - 48; % 48 is ASCII for '0'
end

r.adcresolution = headerBytes(34);
% if r.adcresolution ~= 12
%   r.adcresolution
%   disp(char(progVersion'))
%   % fseek(fid, 4032, 0);
%   disp('not 12 bit recording');
% end

tmp = headerBytes(35);

if (tmp == r.adcresolution)
  % probably an error in recording software
  disp(sprintf('ADC error?? %d %d', r.adcresolution, tmp));
  step0Rec = 0;
  logicalRecordSize = headerBytes(37);
elseif isequal(headerBytes([35:40])', [8 255 56 0 0 0])
  % Different probable error identified 2012-11-08
  disp('another err??')
  r.adcresolution = 8;
  step0Rec = 255;
  logicalRecordSize = 56;
else
  step0Rec = headerBytes(35);
  logicalRecordSize = headerBytes(36);
end

if step0Rec & step0Rec ~= 255
  rows = 8;
else
  rows = 7;
end

% headerBytes([34,35,36,37])'
% headerBytes'
% logicalRecordSize

% [progVersion count] = fread(fid, 24, 'char');
r.programversion = char(progVersion)';

r.starttime = timestamp(year, doy, hour, minute, second);
r.endtime = r.starttime + timespan(4032/logicalRecordSize, 's');
r.resolution = timespan(1, 's');
switch r.adcresolution
 case 8
  [r.data count] = fread(fid, [8*rows, 4032/logicalRecordSize], 'uint8');
  
 case 12
  [r.data count] = fread(fid, [8*rows, 4032/logicalRecordSize], 'ubit12');
  
 otherwise
  fprintf('unknown data resolution (was %d)\n', r.adcresolution);
  fseek(fid, 4032, 0);
  r.data = [];
  count = 0;
end

% in some cases the buffer is corrupted
switch r.programversion
 case {'Last Revision 09/03/94  ', ...
       'Last Revision 08/13/94  '}

  % First 12 data bytes are corrupted. Bug only affects the first sample for
  % the first 8 beams/riometers (beams >= 12) solve by using using 2nd
  % sample for both first and 2nd samples
  r.data(1:8, 1) = r.data(1:8, 2);
  
 case {'Y2K Revision 8/31/1999  '}
  % as seen in Halley riometer data
  % Assume bug affecting the first 12 data bytes is fixed
  
 otherwise
  fseek(fid, 4032, 0);
  mesg = sprintf(['do not know details about version ''%s'', ' ...
                  'please update %s'], r.programversion, mfilename);
  if nargout > 1
    return
  else
    error(mesg);
  end
  
end
  

% if step0Rec & delScan0
if (step0Rec & step0Rec ~= 255) & delScan0
  % delete scan 0
  r.data = r.data(2:end, :);
end

% swap spare receiver
% '@' = 64 = deselect spare receiver
% 'A' = 65 = select spare receiver into column 1
% 'B' = 66 = select spare receiver into column 2
% 'C' = 67 = select spare receiver into column 3
% 'D' = 68 = select spare receiver into column 4
% 'E' = 69 = select spare receiver into column 5
% 'F' = 70 = select spare receiver into column 6
% 'G' = 71 = select spare receiver into column 7
if spareRX ~= 64 & 0
  disp(sprintf('---\nFile offset: %d', r.offset));
  disp('Spare receiver in use');
  spareIdx = 8:8:size(r.data,1); % index positions for spare riometer
  badRioIdx = (spareRX-64):8:size(r.data,1); % index positions for bad rio
  
  spareData = r.data(spareIdx, :);
  badRioData = r.data(badRioIdx, :);
  % now swap around
  r.data(spareIdx, :) = badRioData;
  r.data(badRioIdx, :) = spareData;
end

return

% -------------------


[fh gh] = makeplotfig('init','miniplots',[8 8], ...
		      'title', char(r.starttime), ...
		      'pointer', 'arrow');
for n = 1:64
  axes(gh(n))
  plot(r.data(n,:))
  set(gh, 'YLim', [0 4096]);
end
set(gh,'YLim',[0, 4096])


