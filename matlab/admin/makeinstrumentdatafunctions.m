function varargout = makeinstrumentdatafunctions(varargin)
%MAKEINSTRUMENTDATAFUNCTIONS (Re)make instrument definitions.
%
% MAKEINSTRUMENTDATAFUNCTIONS(...)
%
% Make or update the definitions for instruments. Optionally update the
% function responbile for creating the instrument definition.
%
% The default behaviour can be modified with the following parameter
% name/value pairs:
%
%   'instrument', MIA_INSTRUMENT_BASE.
%   Instrument(s) whose defintions are to be updated.
%
%   'instrumenttypes', CELL
%   CELL array of classes of instrument which should be updated. When empty
%   default to updating all possible instrument types (currently riometer,
%   magnetometer and pulsation_magnetometer).
%
%   'makeinstrumentfunction', LOGICAL
%   Flag to indicate if instrument function files (not just the data
%   defintion functions) should be updated. Default is 0. Instrument
%   function files are always created if they do not exist.
%
% Note the the data definition functions are subject to change and should
% not be accessed directly. Use the INFO function to query instrument
% data.
%
% See also INFO.

defaults.instrument = [];
defaults.instrumenttypes = {};
defaults.makeinstrumentfunction = 0;
defaults.url = 'http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% details of all instruments to be processed, one row per instrument,
% with columns for instrument class, abbreviation (lower case) and serial
% number
instrumentdetails = {};


if ~isempty(defaults.instrument) 
  if ~isempty(defaults.instrumenttypes)
    error(['cannot specify instrument and instrument types' ...
	   ' simultaneously']);
  end
  defaults.instrumenttypes = class(defaults.instrument);
  for n = numel(defaults.instrument):-1:1
    instrumentdetails(n,:) = {class(defaults.instrument(n)), ...
		    getabbreviation(defaults.instrument(n)), ...
		    getserialnumber(defaults.instrument(n))};
  end
end



if isempty(defaults.instrumenttypes)
  % fetch the instrument types whch can be processed
  fid = url_fopen([defaults.url '?list_instrument_types=1'], 'r');
  while ~feof(fid)
    s = fgetl(fid);
    disp(s);
    defaults.instrumenttypes{end+1} = s;
  end
  fclose(fid);
end

allOfType = 0;

if isempty(instrumentdetails)
  allOfType = 1;
  for n = 1:numel(defaults.instrumenttypes)
    % fetch all known instruments of this type
    fid = url_fopen([defaults.url '?list_instruments=' ...
                     defaults.instrumenttypes{n}], 'r');
    while ~feof(fid)
      abbrev = fscanf(fid, '%s', 1);
      sn = fscanf(fid, '%d', 1);
      if feof(fid)
	break
      end
      instrumentdetails(end+1, 1:3) ...
	  = {defaults.instrumenttypes{n}, abbrev, sn};
    end
    fclose(fid);   
    
    clsabbrev = instrumenttypeinfo(feval(defaults.instrumenttypes{n}), ...
				   'abbreviation');
    
    % fetch the instrument type information function
    itfile = fullfile(mia_basedir, defaults.instrumenttypes{n}, ...
		      sprintf('%s_instrumenttypeinfo_data.m', clsabbrev));
    url_fetch([defaults.url '?instrument_type_information=' ...
	       defaults.instrumenttypes{n}], itfile);
  end
end


for n = 1:size(instrumentdetails, 1)
  cls = instrumentdetails{n, 1};
  abbrev = instrumentdetails{n, 2};
  sn = instrumentdetails{n, 3};
  fp = fullfile(mia_basedir, cls);

  clsabbrev = instrumenttypeinfo(feval(cls), 'abbreviation');
  file = fullfile(fp, sprintf('info_%s_%s_%d_data.m', clsabbrev, ...
			      abbrev, sn));

  % instrument function name
  funcname = sprintf('%s_%s_%d', clsabbrev, abbrev, sn); 
  
  % Get the modified string (if possible)
  cmd = sprintf('getfield(info(%s,''_struct''), ''modified'');', funcname);
  modified1 = eval(cmd, 'timestamp(''bad'')');
  
  url_fetch(sprintf('%s?instrument=%s&abbreviation=%s&serialnumber=%d', ...
		    defaults.url, cls, abbrev, sn), file);
  disp(sprintf('saved intrument details to %s', file));
  
  % Get the modified string again, after updating the info data
  modified2 = eval(cmd, 'timestamp(''bad'')');
  
  if logical(defaults.makeinstrumentfunction)
    remakefunction = true; % always remake
  elseif isempty(which(funcname, '-all'))
    remakefunction = true; % function is missing
  elseif any(~isvalid([modified1 modified2])) 
    % Some kind of error (may be new instrument), ensure instrument function
    % is (re)made
    remakefunction = true;
  elseif modified1 ~= modified2
    % The modification timestamp has changed, remake the instrument function
    % as some of the required information (eg location may have changed)
    disp(sprintf('details have changed, will remake %s.m', funcname));
    remakefunction = true;
  else
    remakefunction = false;
  end
  if remakefunction
    localMakeInstrumentFunction(cls, clsabbrev, abbrev, sn, fp);
  end
end


if allOfType
  % Also store details about the instrument type (eg instruments which MIA
  % is aware of and instruments which are supported by MIA).
  typeinfo = [];
  for n = 1:size(instrumentdetails, 1)
    cls = instrumentdetails{n, 1}; % instrument class
    abbrev = instrumentdetails{n, 2};  % instrument abbreviation
    sn = instrumentdetails{n, 3};  % instrument serial number
    
    if isfield(typeinfo, cls)
      ti = getfield(typeinfo, cls);
    else
      ti = [];
      ti.all = {};
      ti.supported.any = [];
    end
    
    % abbreviation for class
    clsabbrev = instrumenttypeinfo(feval(cls), 'abbreviation');

    % st = feval(
  end
  
end


if nargout
  varargout{1} = instrumentdetails;
end



function localMakeInstrumentFunction(cls, clsabbrev, abbrev, sn, fp)

% get instrument details
s = feval(sprintf('info_%s_%s_%d_data', clsabbrev, ...
		  abbrev, sn));

fname = fullfile(fp, sprintf('%s_%s_%d.m', clsabbrev, s.abbreviation, ...
			     s.serialnumber));
disp(['creating instrument function file ' fname]);

if ~isempty(s.location1) & ~isempty(s.location2)
  locStr = [s.location1 ', ' s.location2];
elseif ~isempty(s.location1)
  locStr = s.location1;
elseif ~isempty(s.location2)
  locStr = s.location2;
else
  locStr = '';
end

% have to double any quotes in the strings (except for text printed as
% part of a comment)
loc1 = strrep(s.location1, '''', '''''');
loc2 = strrep(s.location2, '''', '''''');

if isempty(s.latitude)
  lat = nan;
else
  lat = s.latitude;
end
if isempty(s.longitude)
  lon = nan;
else
  lon = s.longitude;
end

fid = url_fopen(fname, 'w');

fprintf(fid, ...
	['function r = %s_%s_%d\n' ...
	 '%%%s_%s_%d  %s object for %s.\n' ...
	 '\n' ...
	 '%% Automatically generated by %s\n' ...
	 'r = %s(''abbreviation'', ''%s'', ...\n' ...
	 '    ''serialnumber'', %d, ...\n' ...
	 '    ''name'', ''%s'', ...\n' ...
	 '    ''facility'', ''%s'', ...\n' ...
	 '    ''location'', location(''%s'', ''%s'', ...\n' ...
	 '                         %f, %f)'], ...
	clsabbrev, s.abbreviation, s.serialnumber, ...
	upper(clsabbrev), upper(s.abbreviation), ...
	upper(s.serialnumber), upper(cls), locStr, ...
	mfilename, ...
	cls, ...
	s.abbreviation, ...
	s.serialnumber, ...
	s.name, ...
	s.facility_name, ...
	loc1, ...
	loc2, lat, lon);

switch cls
 case 'riometer'
  % riometer must add frequency and name
  freq = [s.ifrequency s.wfrequency];
  if length(freq) == 2 & freq(1) ~= freq(2)
    error('imaging and widebeam frequencies are set but are different');
  end
  if length(freq)
    freqStr = sprintf('''frequency'', %g', freq(1));
  else
    freqStr = '''frequency'', []';
  end
  
  fprintf(fid, ...
	  [', ...\n' ...
	   '    %s);\n\n'], ...
	  freqStr);
  
 case {'magnetometer' 'pulsation_magnetometer' 'camera'}
  % everything written, just sign off
  fprintf(fid, ');\n\n');
  
 otherwise
  fclose(fid);
  error(['do not know what to put in ' fname]);
end

fclose(fid);


