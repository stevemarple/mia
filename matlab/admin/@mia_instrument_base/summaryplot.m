function summaryplot(in, varargin)

day = timespan(1, 'd');

% need to strip class part from mfilename results
[tmp mfname] = fileparts(mfilename);

if nargin == 0 | ~isa(in, 'mia_instrument_base')
  error('first parameter must be an instrument object');
elseif nargin == 1
  feval(mfname, in, ...
	'starttime', floor(timestamp('now'), day) - day);
  return;
elseif nargin == 2 & isa(varargin{1}, 'timestamp')
  for n = 1:prod(size(varargin{1}))
    feval(mfname, in, ...
	  'starttime', varargin{1}(n), ...
	  'endtime', varargin{1}(n) + day);
  end
  return;
elseif nargin >= 3 & isa(varargin{1}, 'timestamp') & ...
      isa(varargin{2}, 'timestamp')
  feval(mfname, in, ...
	'starttime', varargin{1}, ...
	'endtime', varargin{2}, ...
	varargin{3:length(varargin)});
  return;
elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % mfilename(<parameter name/value pairs>)
else
  error('incorrect parameters');
end




defaults.starttime = [];
defaults.endtime = day;
% defaults.resolution = timespan(2,'m');
defaults.resolution = timespan(10,'s');

defaults.spacing = [0.15 0.15 0.05 0.05];
defaults.graphpos = [0 0.0 1 0.85];
defaults.units = 'deg';

defaults.keogram = 1;     % boolean to control if keograms are created
defaults.keogramxpixelpos = [];
defaults.keogramypixelpos = [];
defaults.keogramseriesstyle = 'keogram';
defaults.keogramshading = 'interp';

defaults.movie = 1;     % boolean to control if movies are created
defaults.moviexpixelpos = [];  % compute later, when units comfirmed
defaults.movieypixelpos = [];  % compute later, when units confirmed	
defaults.moviescale = [1 1];
defaults.movietimebargap = 2;
defaults.movietimebarheight = 3;
defaults.movietimebar = 1;

defaults.check = 0; % if set only make images if missing/incorrect
defaults.remakeprelims = 1;
defaults.closefigures = 1;

defaults.print = 1; % print to a file
defaults.printsize = [576 432];
defaults.visible = [];

% arguments to pass to extract, eg to remove cloudy images, or use only
% certain filters
defaults.extract = {};

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.starttime)
  error('starttime must be set');
end

t = floor(defaults.starttime, day);
if isa(defaults.endtime, 'timespan')
  defaults.endtime = defaults.starttime + defaults.endtime;
end
defaults.endtime = ceil(defaults.endtime, day);

plotduration = day;

% extras is a struct containing additional information used by both
% summaryplot and localCheckSummaryPlots, but is not intended to be
% overriden by the user
extras.printresolution = 80;
extras.pathformatstr = fullfile(mia_datadir, 'summary', class(in), ...
				getabbreviation(in), '%Y', '%m');
extras.fileformatstr = '%s%%Y%%m%%d.png';
extras.moviefileformatstr = '%s%%Y%%m%%d.gif';
extras.movieinfoformatstr = '%Y%m%d.txt';
extras.keogramfractionalheight = 0.5;
extras.keogramprintsize = defaults.printsize .* ...
    [1 extras.keogramfractionalheight];


[tmpxpix tmpypix] = info(in, 'pixels', ...
			 'units', defaults.units);
if isempty(defaults.keogramxpixelpos)
  defaults.keogramxpixelpos = tmpxpix(floor(end/2));
end
if isempty(defaults.keogramypixelpos)
  defaults.keogramypixelpos = tmpypix;
end

% process movie options
if isempty(defaults.moviexpixelpos) 
  defaults.moviexpixelpos = tmpxpix;
end
if isempty(defaults.movieypixelpos) 
  defaults.movieypixelpos = tmpypix;
end


if isempty(defaults.visible)
  if defaults.closefigures
    defaults.visible = 'off';
  else
    defaults.visible = 'on';
  end
end

% --- end of options processing ---

if defaults.check
  localCheckSummaryPlots(defaults, extras);
  return;
end


image_class = instrumenttypeinfo(in, 'imageclass');

if defaults.keogram & defaults.movie
  xpixelpos = union(defaults.keogramxpixelpos, defaults.moviexpixelpos);
  ypixelpos = union(defaults.keogramypixelpos, defaults.movieypixelpos);
elseif defaults.keogram
  xpixelpos = defaults.keogramxpixelpos;
  ypixelpos = defaults.keogramypixelpos;
elseif defaults.movie
  xpixelpos = defaults.moviexpixelpos;
  ypixelpos = defaults.movieypixelpos;
else
  warning('No movie and no keogram?!');
  return;
end

fh = [];

while t < defaults.endtime
  t2 = t + plotduration;
  Path = strftime(t, extras.pathformatstr);
  pfilename = strftime(t, sprintf(extras.fileformatstr, 'p'));  % power
  afilename = strftime(t, sprintf(extras.fileformatstr, 'a'));  % absorption 
  kfilename = strftime(t, sprintf(extras.fileformatstr, 'k'));  % keogram
  mfilename = strftime(t, sprintf(extras.moviefileformatstr, 'm'));  % movie
  movieInfoFileName = strftime(t, extras.movieinfoformatstr);  % movie info
  
  try  
    
    mia = feval(image_class, ...
		'starttime', t, ...
		'endtime', t2, ...
		'xpixelpos', xpixelpos, ...
		'ypixelpos', ypixelpos, ...
		'resolution', defaults.resolution, ...
		'instrument', in, ...
		'load', 1)
   if ~isempty(defaults.extract);
     mia = extract(mia, defaults.extract{:});
   end
   if isempty(mia)
     error('no data');
   end
   try 
      if defaults.keogram
	% adjust graphpos to allow for footer
	footerheight = 0.1; % proportion of full window height
	graphpos = defaults.graphpos;
	graphpos(4) = graphpos(4) - footerheight;
	graphpos(2) = graphpos(2) + footerheight;
	
	% calculate new graphpos and spacing values, since we wish to scale
	% the plot in height. After scaling we aim to have the header the same
	% size, so must make fractionally bigger
	
	newgraphpos = graphpos;
	newgraphpos(2) = graphpos(2) / extras.keogramfractionalheight;
	newgraphpos(4) = 1 - newgraphpos(2) - ((1-graphpos(2)-graphpos(4))/ ...
					       extras.keogramfractionalheight);
	% adjust the spacing around the graph so that after scaling it is
	% the same (pixel) size
	spacing = defaults.spacing;
	spacing([2 4]) = spacing([2 4]) ./ extras.keogramfractionalheight;
	disp('about to call keogram()');
	fh = keogram(mia, varargin{unvi}, ...
		     'seriesstyle', defaults.keogramseriesstyle, ...
		     'xpixelpos', defaults.keogramxpixelpos, ...
		     'ypixelpos', defaults.keogramypixelpos, ...
		     'spacing', spacing, ...
		     'graphpos', newgraphpos, ...
		     'shading', defaults.keogramshading, ...
		     'visible', defaults.visible);
	disp('about to call figure2png()');
	if defaults.print
	  figure2png(fh, fullfile(Path, kfilename), ...
		     extras.keogramprintsize, 'makedirectory', 1);
	end
      end
      % try % ------------- temp
      % try % ------------- temp
    catch
      lerr = lasterr;
      if strcmp(lerr, 'Interrupt')
	error(lerr);
      end
      disp('Could not create keogram');
      disp(['Error was: ' lerr]);
    end

    if defaults.closefigures
      close(fh);
    end
    fh = [];
      
    try 
      if defaults.movie
	movie(mia, varargin{unvi}, ...
	      'filename', fullfile(Path, mfilename), ...
	      'pixelunits', defaults.units, ...
	      'xpixelpos', defaults.moviexpixelpos, ... 
	      'ypixelpos', defaults.movieypixelpos, ...
	      'makedirectory', 1, ...
	      'infofile', fullfile(Path, movieInfoFileName));
	% savemovieinfo(mia, fullfile(Path, movieInfoFileName));
      end
    catch
      lerr = lasterr;
      if strcmp(lerr, 'Interrupt')
	error(lerr);
      end
      disp('Could not create movie');
      disp(['Error was: ' lerr]);
    end
    
  catch
    if defaults.closefigures
      close all;
    end
    lerr = lasterr;
    if strcmp(lerr, 'Interrupt')
      error(lerr);
    end
    
    disp(['Could not make plot for ' char(t)]);
    disp(['Error was: ' lerr]);
    disp(' ');
  end
  
  t = t + day;
end





function localCheckSummaryPlots(defaults, extras)
% turn off to avoid recursive calls to localCheckSummaryPlots 
defaults.check = 0; 
day = timespan(1,'d');
t = defaults.starttime;
tmpfile = tempname;

while t < defaults.endtime
  Path = strftime(t, extras.pathformatstr); 
  filename = cell(1, 5);

  % keogram (1)
  filename{1} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileformatstr, 'k')));
  imsz{1} = extras.keogramprintsize;
  format{1} = 'png';
  
  % movie (2)
  filename{2} = fullfile(Path, ...
			 strftime(t, sprintf(extras.moviefileformatstr, ...
					     'm')));
  imsz{2} = [length(defaults.movieypixelpos) + ...
	     defaults.movietimebargap + defaults.movietimebarheight, ...
	     length(defaults.moviexpixelpos)];
  format{2} = '';
  
  % movie info (3)
  filename{3} = fullfile(Path, ...
			 strftime(t, extras.movieinfoformatstr));
  format{3} = '';
  remake(3) = 0; % made along with movie

  % check all standard images
  for n = 1
    if ~isempty(format{n}) & exist(filename{n}, 'file')
      try
	inf = imfinfo(filename{n});
	if ~strcmp(lower(inf.Format), format{n})
	  ;
	elseif ~isequal([inf.Width inf.Height], imsz{n})
	  ;
	elseif strcmp(inf.Warning, 'Preliminary') & defaults.remakeprelims
	  ; preliminary
	else
	  remake(n) = 0; % OK
	end
      catch
	if strcmp(lasterr, 'Interrupt')
	  error(lasterr);
	end
      end
    else
      ;
    end
  end
  
    % check movies
  if exist(filename{2}, 'file') & exist(filename{3}, 'file')
    % files exist, but are they correct
    try
      [zz zzz] = unix(['convert ''' filename{2} '[1]'' png:' tmpfile]);
      [im map] = imread(tmpfile, 'png');
      limits = load(filename{3});
      if ~isequal(size(im), imsz{2})
	% disp('wrong size');
	;
      elseif 0 & ~isequal(map(double(im(length(defaults.movieypixelpos) + ...
					1,1))+1, :), [1 1 1])
	% background color in blank part was not white
	% disp('wrong background color');
	;
      elseif limits(1) > limits(2)
	; % bad limit information
      elseif limits(3) & defaults.remakeprelims
	; % preliminary image must be remade
      else
	remake(4) = 0;
      end
      delete(tmpfile);
    catch
      disp(['Error was: ' lasterr]);
      delete(tmpfile);
      if strcmp(lasterr, 'Interrupt')
	error(lasterr);
      end
    end
  end

  if any(remake)
    disp(['remaking ', strftime(t, '%Y-%m-%d')]);
    summaryplot('starttime', t, ...
		'power', remake(1), ...
		'absorption', remake(2), ...
		'keogram', remake(3), ...
		'movie', remake(4));
  else
    disp(['checked: ', strftime(t, '%Y-%m-%d')]);
  end
  
  t = t + day;

end

