function infotest(in)
%INFOTEST  Test the INFO functions for MIA_INSTRUMENT_BASE objects
%
%   INFOTEST(in)
%   in: MIA_INSTRUMENT_BASE object(s)

num = numel(in);
if num ~= 1
  for n = 1:num
    feval(mfilename, in(n));
  end
  return;
end

% 'in' is scalar from this point on

disp(sprintf('%s: %s #%d', gettype(in), getabbreviation(in), ...
	     getserialnumber(in)));


% bestresolution
res = info(in, 'bestresolution');
if isempty(res)
  ; % accept to mean unknown
elseif length(res) > 1
  error('bestresolution should be scalar');
elseif ~isa(res, 'timespan')
  error('bestresolution should be a timespan object');
elseif ~isvalid(res)
  error('bestresolution is not a valid timespan object');
end

% defaultfilename
s = info(in, '_struct');
if isempty(s.defaultfilename)
  fn = {};
else
  fn = fieldnames(s.defaultfilename);
end
for n = 1:length(fn)
  d = info(in, 'defaultfilename', fn{n});
  % check the fields of d conform to the requirements
  if ~isfield(d, 'fstr')
    error('no fstr field for defaultfilename');
  elseif ~ischar(getfield(d, 'fstr'))
    error('fstr should be a char');
  end
  if ~isfield(d, 'duration')
    error('no duration field for defaultfilename');
  elseif ~isa(getfield(d, 'duration'), 'timespan')
    error('duration should be a timespan object');
  end
  if ~isfield(d, 'failiffilemissing')
    error('no failiffilemissing field for defaultfilename');
  end
  if ~isfield(d, 'format')
    error('no format field for defaultfilename');
  elseif ~ischar(getfield(d, 'format'))
    error('fstr should be a format');
  end
  if ~isfield(d, 'size')
    error('no size field for defaultfilename');
  end
  if ~isfield(d, 'loadfunction')
    error('no loadfunction field for defaultfilename');
  end
  if ~isfield(d, 'savefunction')
    error('no savefunction field for defaultfilename');
  end
  
end


% operatingtimes
t = info(in, 'operatingtimes');
if ~isa(t, 'timestamp')
  error('operatingtimes must be a timestamp');
elseif ~isequal(size(t), [1 2])
  error('operatingtimes must a be 1x2 timestamp');
end

% url
url = info(in, 'url');
if ~ischar(url)
  error('url must be a char');  
end


eval('w = which(infotest2(in));', 'w = {};');
if ~isempty(w)
  infotest2(in);
end
