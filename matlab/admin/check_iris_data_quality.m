function check_iris_data_quality(varargin)


defaults.starttime = timestamp('yesterday');
defaults.endtime = [];
defaults.instrument = [];
defaults.filename = '';

[defaults unvi] = interceptprop(varargin, defaults);

warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.instrument)
  error('instrument is required');
end

if isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + timespan(1, 'd');
end

if ~isempty(defaults.filename)
  [fid mesg] = fopen(defaults.filename, 'w');
  if fid == -1
    error(sprintf('cannot open %s: %s', defaults.filename, mesg));
  end
else
  fid = 1;
end

t = defaults.starttime;

df = info(defaults.instrument, 'defaultfilename', 'original_format');
while t < defaults.endtime
  filename = strftime(t, df.fstr);
  disp(sprintf('checking %s', filename));
  [mia headers] = process_iris_data_file(filename, defaults.instrument);
  
  for n = 1:numel(headers)
    h = headers(n);
    if any(h.system_error_number)
      print_iris_header(h, fid);
    end
  end
  t = t + df.duration;
end


if fid ~= 1
  fclose(fid)
end
