function [fh,gh, data] = compare_samnet_data(varargin)

defaults.instrument = [];
defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.instrument)
  error('instrument not defined');
elseif ~isa(defaults.instrument, 'magnetometer')
  error('instruments must be magnetometers');
elseif numel(defaults.instrument) ~= 2
  defaults.instrument
  error('require two magnetometers');
end

if isempty(defaults.starttime)
  error('starttime not defined');
end
if isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + timespan(1, 'd');
end
if isempty(defaults.resolution)
  defaults.resolution = info(defaults.instrument(1), 'bestresolution');
end


mia = mag_data('starttime', defaults.starttime, ...
	       'endtime', defaults.endtime, ...
	       'resolution', defaults.resolution, ...
	       'instrument', defaults.instrument);


comps = getcomponents(mia(1));
if ~isequal(getcomponents(mia(2)), comps)
  error('magnetometers have different components');
end
	       
mia(1)
mia(2)
data{1} = getdata(mia(1));
data{2} = getdata(mia(2));

ncomps = numel(comps);
for n = 1:ncomps
  ti = {['{\bf' comps{n} '}'], ...
	dateprintf(mia(1))};
  [fh gh] = makeplotfig('init', ...
			'title', ti);
			
  
  plot(data{1}(n,:), data{2}(n,:), ...
       'Marker', '.', ...
       'LineStyle', 'none', ...
       'Parent', gh);
end

