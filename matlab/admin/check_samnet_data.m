function varargout = check_samnet_data(action, varargin)
%CHECK_SAMNET_DATA  Check that SAMNET data is acceptable
%
%   fh = CHECK_SAMNET_DATA(t, ...)
%   fh: figure handle
%   t: initial time (TIMESPAN)
%
% Check the data in the SAMNET 'preliminary' data archive. Data can be
% accepted, in which case it is copied to the '1s' archive. Incorrect data
% can be edited. The presence of data in the 1s archive prevents data from
% being checked. If unvalidated data exists then data from the 1s archive
% (if it exists) is loaded to act as a reference.
%
% The following parameter name/value pairs can be specified to modify the
% default behaviour:
%
%     'archive', ['preliminary' | 'scratch']
%     Select the archive to check for unvalidated data. The scratch
%     archive will be phased out as automated processing will make it
%     redundant.
%
%     'deleteincoming', LOGICAL (default 0)
%     'Indicate if the corresponding files in the incoming archive should
%     be deleted when the data is accepted.
%
%     'magnetometers', [MAGNETOMETER]
%     Array of magnetometers whose data should be checked.
%
%     'nospikeediting', MAGNETOMETER (default mag_hll_1)
%     Vector of magnetometers for which spike editing (but not normal
%     editing) should be disabled. Note that EDITSPIKES is still called, so
%     that the same interface is present. EDIT can still be launched from
%     EDITSPIKES even when spike editing is disabled.
%
%     'referencearchive', CHAR
%     List of archives which are used to find reference data for the
%     stackplot.
%
%     'test', LOGICAL
%     'Indicate if the function should run in test mode, ie no files
%     should be moved, deleted or updated but all other functionality is
%     turned on.
%
% See also MAG_DATA, MAG_DATA_SAMNET.

r = 0;

if nargin == 0
  action = 'init';
elseif ishandle(action)
  % mfilename(cbf, action, ...)
  cbf = action;
  cbo = [];
  action = varargin{1};
  varargin(1) = [];
elseif isa(action, 'timestamp')
  varargin(end+[1:2]) = {'time', action};
  action = 'init';
else
  cbf = gcbf;
  cbo = gcbo;
end

% need to know how to identify its own window
tag = mfilename;

switch action
 case 'init'

  defaults.magnetometers = mag_samnet; % magnetometers to check
  % don't do Lancaster at the moment
  % defaults.magnetometers(defaults.magnetometers == mag_lan_1) = [];
  for n = 1:numel(defaults.magnetometers)
    disp(char(defaults.magnetometers(n)));
  end
  
  % archive to check data from
  defaults.archive = 'preliminary';

  % where (and order) to look for data to use in the stack plot. Any values
  % which exist in archive are automatically excluded
  defaults.referencearchive = {'1s' 'preliminary' 'scratch'};

  defaults.deleteincoming = 0;

  defaults.logfile = fullfile(filesep, 'home', hostname, 'samnet', ...
			      '.checksamnetdata.log'); 
  defaults.spikeslogfile = fullfile(mia_personaldir, ...
				    '.checksamnetdataspikes.log');
  defaults.test = 0;

  defaults.nospikeediting = [mag_hll_1]; % don't remove spikes on these mags
  defaults.time = timestamp('yesterday');
  
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});



  day = timespan(1, 'd');
  defaults.time = getdate(defaults.time);

  % check that no windows for this exist already
  fh = localGetControlPanelHandle(tag);
  if ~isempty(fh)
    set(fh, 'Visible', 'on');
    error('SAMNET data validation already in progress');
  end

  defaults.deleteincoming = logical(defaults.deleteincoming);
  defaults.test = logical(defaults.test);

  if defaults.test
    if ~isempty(defaults.logfile)
      defaults.logfile = [defaults.logfile '.test'];
    end
    if ~isempty(defaults.spikeslogfile)
      defaults.spikeslogfile = [defaults.spikeslogfile '.test'];
    end
    figProps = {'Color', [0.8 .4 .4], ...
	       'DefaultUicontrolBackgroundColor', [0.8 0.6 .6], ...
	       'Name', 'Check SAMNET data (TEST MODE)'};
  else
    figProps = {'Name', 'Check SAMNET data'};
  end
  

  
  % create a figure to adjust the date
  spacing = 10;
  fh = figure(figProps{:}, ...
	      'BusyAction', 'cancel', ...
	      'IntegerHandle', 'off', ...
	      'Interruptible', 'off', ...
	      'MenuBar', 'none', ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'Tag', tag);

  viewmenu(fh); % add a view menu to get to the child windows

  udOrig = get(fh, 'UserData');
  if ~isempty(udOrig) & ~isstruct(udOrig)
    error('UserData is not empty and is not a struct');
  end
  ud.fh = fh;
  ud.defaults = defaults;

  dbpos = [10 150 1 1];
  % ud.dbh = datebox(fh, dbpos);
  ud.dbh = datebox('parent', fh, ...
		   'position', dbpos, ...
		   'callback', ...
		   ['if ~isempty(gcbf)' ...
		    mfilename '(gcbf, ''setdate'', t);' ...
		    'end']);

  db = getdatebox(ud.dbh);
  dbpos = getposition(db);

  prevdaypos = [dbpos(1) dbpos(2)-spacing 1 1];
  ud.prevdayH = uicontrol('Parent', fh, ...
			  'Style', 'pushbutton', ...
			  'String', '<<', ...
			  'Position', prevdaypos, ...
			  'Tag', 'previousday', ...
			  'Callback', ...
			  [mfilename '(''previousday'');']);
  prevdaypos = uiresize(ud.prevdayH, 'l', 't');

  nextdaypos = [sum(dbpos([1 3])) - prevdaypos(3) prevdaypos(2:4)];
  ud.nextdayH = uicontrol('Parent', fh, ...
			  'Style', 'pushbutton', ...
			  'String', '>>', ...
			  'Position', nextdaypos, ...
			  'Tag', 'nextday', ...
			  'Callback', [mfilename '(''nextday'');']);
  nextdaypos = uiresize(ud.nextdayH, 'l', 'b');


  datepos = prevdaypos;
  ud.datestrH = uicontrol('Parent', fh, ...
			  'Style', 'text', ...
			  'String', '', ...
			  'HorizontalAlignment', 'center', ...
			  'BackgroundColor', get(fh, 'Color'), ...
			  'Position', datepos, ...
			  'Tag', 'date');


  % use uiresize to adjust for (maximum) height, don't worry about width
  datepos = uiresize(ud.datestrH, 'l', 'm', '0123456789- ');
  % but set horizontal extent to be the space left between the buttons
  datepos(1) = sum([prevdaypos([1 3]) spacing]);
  datepos(3) = nextdaypos(1) - spacing - datepos(1);
  set(ud.datestrH, 'Position', datepos);

  ud.childfh = [];
  ud.stackploth = [];
  ud.tempdir = tempname2('', '.d');
  mkdirhier(ud.tempdir);

  ud2 = setfield(udOrig, tag, ud);
  set(fh, 'UserData', ud2);
  feval(mfilename, fh, 'setdate', defaults.time);

  % get all the uicontrol handles before ok/cancel are added
  h = findall(fh, 'Type', 'uicontrol');

  [oh ch] = okcancel('init', fh, [10 spacing 1 1], ...
		     [mfilename '(''ok'');'], ...
		     [mfilename '(''cancel'');']);

  % ok/cancel controls are at correct height
  okpos = get(oh, 'Position');

  % neatly position all other control elements to the correct height
  % relative to ok/cancel

  moveuicontrols(h, [0 sum([spacing okpos([2 4])])-prevdaypos(2) 0 0], 'r');

  bb = boundingbox(h);
  fpos = get(fh, 'Position');
  % set(fh, 'Position', [fpos(1:2) bb(1:2) + bb(3:4) + [spacing spacing]]);
  ss = get(0, 'ScreenSize');

  fpos = [fpos(1:2) bb(1:2) + bb(3:4) + [spacing spacing]];
  % now move to top left corner, allowing a bit of space for the window
  % decorations 
  fpos(1) = 1 + 10;
  fpos(2) = ss(4) - fpos(4) - 30;
  set(fh, 'Position', fpos);

  % centre ok/cancel controls
  okcancel('resize', fh, oh, ch);

  set(fh, ...
      'CloseRequestFcn', [mfilename '(''cancel'');'], ...
      'Visible', 'on');

  if nargout
    varargout{1} = fh;
  end 

 case 'accept'
  gps = varargin{1};
  mia = varargin{2};
  modified = varargin{3};
  in = getinstrument(mia);
  st = getstarttime(mia);
  et = getendtime(mia);
  ud = localGetUserData(localGetControlPanelHandle(tag), tag);
  
  disp(sprintf('Modified: %d\n', modified));
  % code assumes single file for the day, and that the input resolution is
  % 1s. check.
  df1 = info(in, ...
	     'defaultfilename', 'mag_data', ...
	     'archive', getarchive(mia));
  if df1.duration ~= timespan(1, 'd')
    error('input file duration is not one day');
  elseif getresolution(mia) ~= timespan(1, 's');
    error('data is not at 1s resolution');
  end
  
  df2 = info(in, ...
	     'defaultfilename', 'mag_data', ...
	     'archive', '1s');
  if df2.duration ~= timespan(1, 'd')
    error('output file duration is not one day');
  end

  dfin = info(in, ...
	      'defaultfilename', 'mag_data', ...
	      'archive', 'incoming');

  pathfile1 = strftime(st, df1.fstr); % preliminary
  pathfile2 = strftime(st, df2.fstr); % target archive file
  % incoming archive file for starttime (may be multiple files)
  pathfile3 = strftime(st, dfin.fstr);

  [path2 file2 ext2] = fileparts(pathfile2);
  mkdirhier(path2); % make directory for destination file
  
  if modified
    % save modified data to our temporary directory using the same file
    % portion as the final file will have. This means mia2samnet will gzip
    % the file if necessary. Another benefit is that the preliminary file is
    % not destroyed if writing fails for some reason
    tmppathfile2 = fullfile(ud.tempdir, [file2 ext2]);
    mia2samnet(mia, 'filename', tmppathfile2, 'gpsinfo', gps);

    if ud.defaults.test
      disp(['data saved to ' tmppathfile2]);
    end

    % put modified file into the target archive. Safe because
    % tmppathfile2 is a real file, not a symbolic link.
    cmd = ['mv ' tmppathfile2 ' ' pathfile2];

  else
    % copy the unmodified file. Never use move here since the preliminary
    % file could be a symbolic link
    if localIsGzipped(pathfile1) == localIsGzipped(pathfile2)
      % compression status is the same for both source and target 
      cmd = sprintf('cp -fp %s %s', pathfile1, pathfile2);
    elseif localIsGzipped(pathfile1)
      % source is gzipped, target is not
      cmd = sprintf('rm -f %s; gunzip -c %s > %s', ...
		    pathfile2, pathfile1, pathfile2);
    else
      % source not gzipped, target is
      cmd = sprintf('rm -f %s; gzip -c %s > %s', ...
		    pathfile2, pathfile1, pathfile2);
    end
    
  end
  
  % delete the original preliminary file (but not if it's the same as the
  % incoming and they should be kept)
  if ~strcmp(pathfile1, pathfile3) | ud.defaults.deleteincoming
    cmd = [cmd ';rm -f ' pathfile1];
  end
    
  % touch the destination file so that things like the summary plots
  % scripts know it has changed and that the images should be
  % updated. (Otherwise the destination file can end up with the same
  % timestamp as the preliminary file, which means the summary plot
  % images appear upto date, yet in fact they were made with
  % preliminary data which has now been approved, so they don't need the
  % 'preliminary' watermark.
  cmd = [cmd ';touch ' pathfile2];
  
  if ud.defaults.deleteincoming
    % append commands to delete the incoming file
    t = st;
    while t < et
      incomingfile = strftime(t, dfin.fstr);
      cmd = [cmd ';rm -f ' incomingfile];
      t = t + dfin.duration;
    end
  end
  
  % do the 5s convert
  cmd = [cmd strftime(st, ['; ~samnet/bin/5s_convert -m ' ...
		    getabbreviation(in) ' -s %Y-%m-%d'])];
  
  cmd2 = strrep(cmd, ';', [';' char([10 13])]); % add newlines after ";"

  if ~isempty(ud.defaults.logfile)
    [fid mesg] = fopen(ud.defaults.logfile, 'at');
    if fid == -1
      disp(sprintf('cannot open logfile (%s): %s', ...
		   ud.defaults.logfile, mesg));
    else
      % don't worry about file locking. It's only a log file
      fprintf(fid, '%s %s %s %s_%d %d\n', ...
	      strftime(timestamp('now'), '%Y-%m-%d %H:%M:%S'), ...
	      getusername, ...
	      strftime(getstarttime(mia), '%Y-%m-%d'), ...
	      getabbreviation(in), getserialnumber(in), modified);
      fclose(fid);
    end
  end
  
  if ud.defaults.test
    if cmd2(end) == '\n'
      nl = '';
    else
      nl = sprintf('\n');
    end
    disp(sprintf('would execute:\n%s%s', cmd2, nl));
  else
    localSystem(cmd);
  end
  

 case 'fail'
  ; % do nothing
  
 case {'previousday' 'nextday'}
  % ud = get(cbf, 'UserData');
  ud = localGetUserData(cbf, tag);
  
  db = getdatebox(ud.dbh);
  date = getdate(db);
  day = timespan(1, 'd');
  if strcmp(action, 'previousday')
    date = date - day;
  else
    date = date + day;
  end
  feval(mfilename, cbf, 'setdate', date);
  
 case 'setdate'
  % ud = get(cbf, 'UserData');
  ud = localGetUserData(cbf, tag);
  db = getdatebox(ud.dbh);
  date = varargin{1};
  t = timestamp('now');
  if ~isvalid(date)
    set(ud.datestrH, 'Visible', 'off');
    set([ud.prevdayH; ud.nextdayH], 'Enable', 'off');
  else
    set(ud.datestrH, 'Visible', 'on');
    set([ud.prevdayH; ud.nextdayH], 'Enable', 'on');
    if date > t
      fprintf(2, '\a\n');
      drawnow;
      date = getdate(t);
    end
    setdate(db, date);
    set(ud.datestrH, 'String', strftime(date, '%Y-%m-%d'));
  end

  
 case 'ok'
  tic;
  ud = localGetUserData(cbf, tag);
  pointer = get(cbf, 'Pointer');
  set(cbf, 'Pointer', 'watch');

  if any(ishandle(ud.childfh)) 
    if strcmp(questdlg({'Cannot verify new data while existing ' ...
			'plots are open.', '', ...
			'Close plots from current day and continue?'}, ...
		       '', 'Yes', 'No', 'No'), 'No')
      set(cbf, 'Pointer', pointer);
      return
    end
    close(ud.childfh(ishandle(ud.childfh)));
  end
  
  db = getdatebox(ud.dbh);
  if ishandle(ud.stackploth)
    close(ud.stackploth);
  end
  ud = localMakeDailyPlots(getdate(db), ud);
  localSetUserData(cbf, tag, ud);
  set(cbf, 'Pointer', pointer);
  disp(sprintf('time taken for initialisation was %.1f seconds', toc));

  
 case 'cancel'
  ud = localGetUserData(cbf, tag);
  close([ud.childfh; ud.stackploth]);
  
  if ud.defaults.test & length(dir(ud.tempdir)) > 2
    % only ask to about deleting temporary directory if is contains more
    % than "." and ".."
    if strcmp(questdlg(['Delete temporary directory ' ud.tempdir '?'], ...
		       '', 'Yes', 'No', 'No'), 'Yes')
      rmdir(ud.tempdir, 's');
    end
  else
    rmdir(ud.tempdir, 's');
  end
  
  delete(cbf);
  
  
 otherwise
  error('Unknown action');
end


% -----
% modifies userdata argument and return the modified version
function r = localMakeDailyPlots(t, ud)
mag = ud.defaults.magnetometers;
% arrange N-S geomagnetically
loc = getlocation(mag);
if ~iscell(loc)
  loc = {loc}; % for the case of a single magnetometer
end

magLat = zeros(1, prod(size(loc)));
for n = 1:length(magLat)
  magLat(n) = getgeolat(loc{n});
end

[tmp idx] = sort(magLat);
mag = mag(idx);


t2 = t + timespan(1, 'd');
if ~iscell(ud.defaults.archive)
  ud.defaults.archive = {ud.defaults.archive};
end
if ~iscell(ud.defaults.referencearchive)
  ud.defaults.referencearchive = {ud.defaults.referencearchive};
end
refArchive = setxor(ud.defaults.referencearchive, ud.defaults.archive);

% calculate magnetometers which are operational
op = isoperational(mag);
mag2 = mag(~isnan(op) & op > 0);

% load up all data for this day
magnum = prod(size(mag2));
mia = repmat(mag_data, 1, magnum);

% need somewhere to remember the GPS data. Cannot do this in the child
% windows (ie those created by editspikes) since they are closed before
% our accept callback is run. Instead store them in the userdata area of
% our figure. However, need to know which editspikes window has closed,
% so get the editspikes updatefcn to remember this information for
% us. (Storing the GPS data there is a bad idea since it the  ASCII
% string).
gps = cell(size(mia));

% indicate if elements of mag2 have any valid data. use 1 for data
% exists (from any source), 2 for data which should be checked
haveData = zeros(size(mia));

% first try loading data only from the archives which should be checked
archive = cell(size(mag2));
for n = 1:magnum
  % master archive is 1s, so don't try loading if the 1s data exists
  df = info(mag2(n), 'defaultfilename', 'mag_data', 'archive', '1s');
  fname = strftime(t, df.fstr);
  d = dir(fname);
  if isempty(d)
    tmpMia = mag_data_samnet('starttime', t, ...
			     'endtime', t2, ...
			     'instrument', mag2(n), ...
			     'log', 0, ...
			     'loadoptions', ...
			     {'failiffilemissing', 0, ...
		    'archive', ud.defaults.archive});
    [mia(n) gps{n}] = mag_data(tmpMia);
    data = getdata(mia(n));
    if prod(size(data)) ~= 0 & ~all(isnan(data(:)));
      haveData(n) = 2;
      archive{n} = getarchive(mia(n));
      archive{n}(1) = upper(archive{n}(1));
    end
  else
    disp(['Data exists: ' fname]);
  end
end

if any(haveData == 2)
  % there is data to be checked, so now try loading data from the
  % reference archives

  for n = find(haveData ~= 2)
    mia(n) = mag_data('starttime', t, ...
		      'endtime', t2, ...
		      'instrument', mag2(n), ...
		      'log', 0, ...
		      'loadoptions', ...
		      {'failiffilemissing', 0, ...
		       'archive', refArchive});
    
    data = getdata(mia(n));
    if prod(size(data)) ~= 0 & ~all(isnan(data(:)));
      haveData(n) = 1;
      archive{n} = getarchive(mia(n));
      archive{n}(1) = upper(archive{n}(1));
    end
  end

  [ud.stackploth spgh splh spwmh] = ...
      stackplot(mia(haveData ~= 0), ...
		'components', 'H', ...
		'visible', 'off', ...
		'watermark', {archive{haveData ~= 0}});
  viewmenu(ud.fh, ud.stackploth);
  % axes(spgh);
  
  % go through the data but only check those which correspond to the
  % archive to be tested (and contain real data)
  toCheckIdx = find(haveData == 2);
  ud.childfh = zeros(length(toCheckIdx), 1);
  windowButtonMotionFcn = cell(size(ud.childfh));
  for n = 1:length(toCheckIdx)
    ud.childfh(n) = ...
	localCheckPlot(mia(toCheckIdx(n)), ud, gps{n}, ...
		       'enablespikeediting', ...
		       ~any(getinstrument(mia(toCheckIdx(n))) == ...
			    ud.defaults.nospikeediting));
    
    windowButtonMotionFcn{n} = get(ud.childfh(n), 'WindowButtonMotionFcn');
    set(ud.childfh(n), 'WindowButtonMotionFcn', '');
  end
  cf = get(0, 'CurrentFigure');
  set(ud.stackploth, 'CurrentAxes', spgh);
  % axis('tight');
  set(0, 'CurrentFigure', cf);
  set(ud.stackploth, 'Visible', 'on');
  set(ud.childfh, 'Visible', 'on', ...
		  {'WindowButtonMotionFcn'}, windowButtonMotionFcn);
  
else
  disp(sprintf('no data to be checked for %s', strftime(t, '%Y-%m-%d')));
end

r = ud;

% -----
function [fh, gh] = localCheckPlot(mia, ud, gps, varargin)
% pass the GPS information to the updatefcn. Note that updatefcn is a
% cell array which becomes part of the argument list for FEVAL, so it is
% OK to use this for the editspikes window to store the relevant GPS
% information
[tmp mfname] = fileparts(mfilename);
% closerequestfcn = ['errordlg(''Please use the "Accept" or "Fail" button ' ...
% 		   'to close the window'', ''Error'');'];
closerequestfcn = 'closereq';
name = ['Check SAMNET data: ' upper(getabbreviation(getinstrument(mia)))];
[fh gh] = editspikes('init', mia, ...
		     varargin{:}, ....
		     'windowtitle', name, ...
		     'spacing', [nan 0.1], ...
		     'footer', 1, ...
		     'busyaction', 'queue', ...
		     'interruptible', 'off', ...
		     'visible', 'off', ...
		     'okstring', 'Accept', ...
		     'cancelstring', 'Fail', ...
		     'position', 'fullscreen', ...
		     'updatefcn', {mfname, 'accept', gps}, ...
		     'closerequestfcn', closerequestfcn, ...
		     'logfile', ud.defaults.spikeslogfile);
viewmenu(ud.fh, fh);

% -----
function r = localGetUserData(fh, tag);
ud = get(fh, 'UserData');
if isfield(ud, tag)
  r = getfield(ud, tag);
else
  r = [];
end


% -----
function localSetUserData(fh, tag, ud);
s = get(fh, 'UserData');
set(fh, 'UserData', setfield(s, tag, ud));


% -----
function localSystem(cmd)
disp(cmd);
[status mesg] = system(cmd);
if status ~= 0
  error(sprintf('problem with command: %s\nerror was: %s', cmd, mesg));
end


% -----
function r = localGetControlPanelHandle(tag)
r = findall(0, 'Type', 'figure', 'Tag', tag);


% -----
function r = localIsGzipped(filename)
r = strcmp(filename(end - [1 0]), 'gz');

