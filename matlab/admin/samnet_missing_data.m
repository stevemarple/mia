function samnet_missing_data(varargin)

defaults.magnetometer = mag_samnet;
defaults.starttime = [];
defaults.endtime = timestamp('today');
defaults.filename = '';

% if all files for this day are found in any archive listed it is assumed
% to be ok.
defaults.archive = {'1s', '5s', 'preliminary', 'scratch', 'incoming'};

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.starttime)
  error('starttime required');
end

if isempty(defaults.filename)
  fid = 1; % stdout
else
  [fid mesg] = fopen(defaults.filename, 'w');
  if fid == -1
    error(sprintf('could not open %s: %s', filename, mesg));
  end
end

isoFstr = '%Y-%m-%d';
day = timespan(1, 'd');

for mag = defaults.magnetometer(:)'
  abbrev = getabbreviation(mag);
  sn = getserialnumber(mag);
  
  magSt = info(mag, 'starttime');
  magEt = info(mag, 'endtime');
  % cache the default filename information for this magnetometer, as it
  % is time-consuming to request
  for n = 1:length(defaults.archive)
    df(n) = info(mag, 'defaultfilename', 'mag_data', ...
		 'archive', defaults.archive{n});
  end
  
  % try loading data for all relevant days for which the instrument was
  % operational 
  st = defaults.starttime;
  et = defaults.endtime;
  if isvalid(magSt)
    st = max(magSt, st);
  end
  if isvalid(magEt)
    et = min(magEt, et);
  end
  
  if st < et
    disp(sprintf('Checking %s #%d ...', upper(abbrev), sn));
  end
  
  t = st;
  while t < et
    t2 = t + day;
    
    found = 0;
    % try each archive in turn until all data found
    for n = 1:length(defaults.archive)
      tf = t;
      archivePassed = 1; % flag indicating if archive under test is ok
      while tf < t2
	fname = strftime(tf, df(n).fstr);
	if isempty(dir(fname))
	  archivePassed = 0;
	  break;
	end
	tf = tf + df(n).duration; % try next file for the day
      end
      if archivePassed
	found = 1;
	break;
      end
    end
    
    if ~found
      fprintf(fid, '%s_%d %s\n', abbrev, sn, strftime(t, isoFstr));
    end
    
    t = t2;
  end
end

