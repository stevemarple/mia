#!/usr/bin/perl -T -w


#
# $Log$

use strict;
use English;

use Carp;
use DBI qw(:sql_types);

use Time::Local;
use POSIX;
use CGI qw(:all -no_xhtml);

use lib "/home/iris/ukiris/WWW/cgi-bin";
use STP::Events qw(:SQL is_number);


my $id = $ARGV[0];
die "No ID given" unless defined $id;
    

my %PG_data;
if (($ENV{SERVER_NAME} and $ENV{SERVER_NAME} =~ m/belfry/)
    or ($ENV{HOSTNAME} and $ENV{HOSTNAME} =~ m/belfry/)) {
    $PG_data{db} = "instruments";
    $PG_data{host} = "iris";
    $PG_data{user} = "test";
    $PG_data{passwd} = "poiuy";
}
elsif (($ENV{SERVER_NAME} and $ENV{SERVER_NAME} =~ "www.dcs") or
       ($ENV{SERVER_NAME} and $ENV{SERVER_NAME} =~ m/iris/)
       or ($ENV{HOSTNAME} and $ENV{HOSTNAME} =~ m/iris/)) {
    $PG_data{db} = "instruments";
    $PG_data{host} = "iris";
    $PG_data{user} = "test";
    $PG_data{passwd} = "poiuy";
}
else {
    $PG_data{db} = "instruments";
    $PG_data{host} = "mango";
    $PG_data{user} = "test";
    $PG_data{passwd} = "poiuy";
}


my $dbh;

eval {
    $dbh = DBI->connect("dbi:Pg:dbname=" . 
                        $PG_data{db} .
                        ";host=" . $PG_data{host},
                        $PG_data{user},
                        $PG_data{passwd},
                        {RaiseError => 1,
                         AutoCommit => 0,
                         InactiveDestroy => 1});
    $dbh->do("SET DATESTYLE='ISO'");
    $dbh->do("SET TIMEZONE='GMT'");
};
if ($EVAL_ERROR) {
    # fatal error
    $dbh = undef;
    die "Cannot connect to database: $DBI::errstr\n";
}

my $q = new CGI;


my $sqlh = {command => "SELECT",
	    tables => ["riometers"],
	    columns => ["*"],
	    where => ["id=?"],
	    values => [$id],
	    debug => 0
	    };
	    
my $sth = sql_select($dbh, $sqlh);
my $hr;

while ($hr = $sth->fetchrow_hashref('NAME_lc')) {
    my $abbrev = $hr->{abbreviation};
    my $ABBREV = uc $abbrev;
    my $imaging_beams = "[]";
    $imaging_beams = "1:$hr->{ibeams}" if defined $hr->{ibeams};
    my $wide_beams = "[]";
    $wide_beams = "[1:$hr->{wbeams}]" if defined $hr->{wbeams};

    my $bad_beams = "[]";
    $bad_beams = "[1:$hr->{bad_beams}]" if defined $hr->{bad_beams};
    my $default_beams = "[]";
    $default_beams = "[1:$hr->{default_beams}]" 
	if defined $hr->{default_beams};

    print 
"varargout = riometer_info_$hr->{abbreviation}(rio, info, varargin)
%RIOMETER_INFO_\U$hr->{abbreviation}", 
" Riometer info file for $hr->{location1}, $hr->{location2}.

if getserialnumber(rio) > 1
  error(sprintf('Do not have details for serial number #%d', ...
		getserialnumber(rio)));
end

imaging_beams = $imaging_beams
wide_beams = $wide_beams;
bad_beams = $bad_beams
default_beams = $default_beams;

\n";
    
}

# exit, with return value of 0. Define a label so we can use goto()
# instead of exit() from elsewhere within the program. exit() should
# NOT be used if the perl script is run under the Apache module
# mod_perl.
# exit;
 MY_EXIT:  
    ;
$dbh->disconnect or warn "Disconnection failed: $DBI::errstr\n";
$dbh = undef;
0;
# ----------------------------------------------------------




__END__
