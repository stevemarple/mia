function process_arcom_files(rio, varargin)
%PROCESS_ARCOM_FILES  Convert .arcom files to MIA matlab files.
%
%   PROCESS_ARCOM_FILES(rio, ...)
%   rio: RIOMETER object
%
% The following name/value pairs can be used to modify the default
% behaviour:
%
%     'archive', CHAR
%     Specify an laternative archive to use when loading data.
%
%     'calibrationmethod', CHAR
%     The calibration method to use. The default for the given riometer
%     is used if not specified.
%
%     'cancelhandle', DOUBLE
%     The handle of cancel button in a GUI. See CANCELCHECK for more
%     details.
%
%     'class', CHAR
%     The MIA class to be used for the final output data. If set to an
%     empty string the default class for the given RIOMETER is used.
%
%     'complexdata', LOGICAL
%     Indicate is complex data should be output. Default is FALSE.
%
%     'endtime', TIMESTAMP
%     The end time used for processing (rounded up to start of following day
%     using timestamp/CEIL). If not specified it defaults to one day
%     after the start time.
%
%     'realtimemode', LOGICAL
%     In realtime mode recent files are not merged but are
%     overwritten. (This helps to fix minor file errors?) Default is
%     FALSE.
%
%     'starttime', TIMESTAMP
%     The starting time used for processing (rounded down to start of day
%     using timestamp/FLOOR). If not specified defaults to yesterday.
%
%     'swapcalibrationonly', LOGICAL
%     Do not process .arcom files; just process any existing .mat files,
%     ensuring adjacent calibration details are swapped into each file.
%
%     'time', TIMESTAMP
%     When processing in realtime mode the time taken to be the current
%     time. It is useful if this is set by the calling program
%


processingmessage = sprintf('Created by %s', basename(mfilename));
day = timespan(1,'d');

defaults.starttime = timestamp('yesterday');
defaults.endtime = [];
defaults.calibrationmethod = '';
defaults.swapcalibrationonly = 0;
defaults.archive = '';
defaults.spaceregularly = true;

% Create temperature data too, if possible
defaults.temperature = ~isempty(info(rio, 'defaultfilename', 'temp_data'));

% for realtime processing do not merge recent files, overwrite
defaults.realtimemode = 0;
defaults.time = []; % time taken to be the current time
defaults.class = ''; % defer, so that '' means default class
defaults.complexdata = 0;
defaults.cancelhandle = [];

if rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
elseif nargin == 3
  defaults.starttime = varargin{1};
  defaults.endtime = varargin{2};
else
  error('incorrect parameters');
end

if isempty(defaults.starttime)
  error('starttime not specified');
elseif isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + day;
end

if isempty(defaults.time)
  defaults.time = timestamp('now');
end

if isempty(defaults.class)
  defaults.class = 'rio_power_calib';
end
  

res = info(rio, 'bestresolution');
beams = info(rio, 'allbeams');

% have special exception for ARIES, only process good beams
if rio == rio_ram_2
  beams = info(rio, 'goodbeams');
end


% want to have calibration values from before and after the desired
% period.
df_orig = info(rio, ...
	       'defaultfilename', 'original_format', ...
	       'archive', defaults.archive);
df_rpc = info(rio, ...
	      'defaultfilename', defaults.class, ...
	      'archive', defaults.archive);

commoncalibrationdata = info(rio, 'commoncalibrationdata');

% Modify the format specifier to include a sequence number. This requires
% the format string to be expanded with strftime, and then later
% sprintf'd with the sequence number
fstr = info(rio,'includesequencenumber',df_orig.fstr);

st = floor(defaults.starttime, df_orig.duration);
et = ceil(defaults.endtime, df_orig.duration);

if logical(defaults.swapcalibrationonly)
  t = et;
  mat_st = st;
  mat_et = et;
else
  disp('processing ARCOM files');
  t = st;
  mat_st = timestamp('bad');
  mat_et = timestamp('bad');
end


while t < et
  t2 = t + df_orig.duration;
  
  % for a given time loop through all files
  filename1 = strftime(t, fstr);
  n = 0;
  
  while 1
    % allow graphical user interface to interrupt processing
    cancelcheck(defaults.cancelhandle);
    
    filename2 = sprintf(filename1, n);
    missing = ~url_exist(filename2);

    if missing
      break;
    else
      mia = arcom_file_to_mia(filename2, ...
			      'mia', feval(defaults.class, ...
					   'instrument', rio, ...
					   'beams', beams, ...
					   'commoncalibrationdata', ...
					   commoncalibrationdata, ...
					   'log', 0, ...
					   'load', 0), ...
			      'complexdata', defaults.complexdata);
      
      if ~isempty(mia)
	% Have valid data from the file, save the MIA object as Matlab
        % files. Don't completely obliterate any existing data, instead
        % merge the new data over the existing data in the
	% files.
	mia = calibrate(mia, defaults.calibrationmethod);

        mia_st = getstarttime(mia);
        mia_et = getendtime(mia);
        if logical(defaults.spaceregularly)
          mia_samt = mia_st+(df_orig.resolution/2):df_orig.resolution:mia_et;
          mia = spaceregularly(mia, ...
                               'markmissingdata', true, ...
                               'method', 'linear', ...
			       'resolution', df_orig.resolution, ...
                               'sampletime', mia_samt);
        end
        mia = setprocessing(mia, processingmessage);
	save(mia, 'merge', 1, 'fstr', df_rpc.fstr);
	mat_st = min(mat_st, mia_st);
	mat_et = max(mat_et, mia_et);
      end
    end
    
    % if filename does not contain a sequence number don't keep going
    % around the loop
    if strcmp(filename1, filename2)
      break; 
    end
    
    n = n + 1;

  end
    
  
  t = t2;
end


if ~isvalid(mat_st)
  % no MAT files saved, so nothing to fix
  return
end

disp('copying calibration data between MAT files');

% get start time of earliest mat file to fix
mat_st = floor(mat_st, df_rpc.duration) - df_rpc.duration;

% get end time of latest mat file to fix
mat_et = floor(mat_et, df_rpc.duration) + df_rpc.duration;

t = mat_st;
mia_last = [];
while t < mat_et
  t2 = t + df_rpc.duration;
  filename = strftime(t, df_rpc.fstr);
  if url_exist(filename)
    clear mia;
    load(filename);
    if ~exist('mia', 'var')
      error(['expected to have a variable called ''mia'' but did not' ...
	     ' find one']);
    end
  else
    % file not found
    if logical(df_rpc.failiffilemissing)
      % create an empty one
      mia = feval(defaults.class, ...
		  'instrument', rio, ...
		  'starttime', t, ...
		  'endtime', t2, ...
		  'units', df_rpc.units, ...
		  'commoncalibrationdata', commoncalibrationdata, ...
		  'load', 0);
    else
      mia = [];
    end
  end
  
  if ~isempty(mia) & ~isempty(mia_last)
    disp('exchanging calibration');

    save(setprocessing(insertcalibrationdata(mia_last, mia), ...
		       processingmessage), ...
	 'merge', 1, ...
	 'fstr', df_rpc.fstr, ...
	 'filter', 'trimcalibrationdata');

    save(setprocessing(insertcalibrationdata(mia, mia_last), ...
		       processingmessage), ...
	 'merge', 1, ...
	 'fstr', df_rpc.fstr, ...
	 'filter', 'trimcalibrationdata');
  end
  
  mia_last = mia;
  t = t2;
end


if defaults.temperature
  % Process temperature data
  process_arcom_temp_data(rio, ...
			  'starttime', defaults.starttime, ...
			  'endtime', defaults.endtime, ...
			  'archive', defaults.archive);
end


