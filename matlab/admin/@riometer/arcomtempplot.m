function [fh, gh, lh] = arcomtempplot(rio, st, et, varargin)
% ARCOMTEMPPLOT Produce a plot of temperature from ARCOM temperature data
%
%   [fh gh lh] = ARCOMTEMPPLOT(rio, st, et, ...)
%   fh: FIGURE handle
%   gh: AXES handle
%   lh: LINE handle
%   rio: RIOMETER object
%   st: start time (TIMESTAMP)
%   et: end time (TIMESTAMP)
%
% ARCOMTEMPPLOT produces a temperature plot from values previously
% extracted using PROCESS_ARCOM_TEMP_DATA.
%
% For daily summary plots see TEMPERATURESUMMARYPLOT.

defaults.realtimemode = false;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% Compute start/end times of the files to load data from
df = info(rio, 'defaultfilename', 'original_format');
fst = floor(st, df.duration);
fet = ceil(et, df.duration);

d = {};
tt = repmat(timestamp, 0, 0); % temperature times
td = []; % temperature data
tsens = [];

t = fst;
while t < fet
  t2 = t + df.duration;
  temp_filename = strftime(t, ...
			   fullfile(mia_datadir(rio), 'temp', ...
				    '%Y', '%m', '%d', 'temp_%Y%m%d%H.txt'));
  
  if url_exist(temp_filename)
    disp(sprintf('loading %s', temp_filename));
    d = load(temp_filename);
    sensors = d(1, 2:2:end);
    sensors(isnan(sensors)) = [];

    % Sanity check, ensure the sensor number is the same for all rows. It is
    % necessary an error for them to change but it requires more logic to be
    % added
    for n = 1:numel(sensors)
      colnum = 2 * n;
      if any(d(1, colnum) ~= d(:, colnum))
        error(sprintf('Sensor change in column %d for file %s', ...
                      colnum, temp_filename));
      end
    end
    
    % Sanity check, ensure the sensors are in the same order as previous
    % files used
    if isempty(tsens)
      tsens = sensors;
    elseif ~isequal(tsens, sensors)
      tsens
      sensors
      error('sensors have changed from previous file');
    end
    
    datacols = 3:2:(numel(sensors)*2 + 1);
    tt = vertcat(tt, timestamp('unixtime', d(:, 1)));
    td = vertcat(td, d(:, datacols));
    
  else
    disp(sprintf('missing %s', temp_filename));
  end
  
  t = t2;
end


% Go through data columns and delete any which are all NaNs
to_delete = [];
for n = 1:size(td, 2)
  if all(isnan(td(:, n)))
    to_delete(end+1) = n;
  end
end
if ~isempty(to_delete)
  td(:, to_delete) = [];
  tsens(to_delete) = [];
end


% Create plot title
title = {'{\bf Riometer temperature}', 
	 dateprintf(st,et), 
	 char(getlocation(rio))};
logo = info(rio, 'logo');
[fh gh] = makeplotfig('init', ...
		      'title', title, ...
		      'logo', logo);
% th = makeplotfig('title', title);set(th, 'FontSize', 14);

lh = plot(tt, td, 'Parent', gh);
xlim(gh, getcdfepochvalue([st et]));
timetick2(gh);

ylabel('Temperature (deg C)', ...
       'Parent', gh, ...
       'FontSize', 14, ...
       'FontWeight', 'bold');
% set(get(gh, 'XLabel'), 'FontSize', 12);

if logical(defaults.realtimemode)
  plottime = strftime(timestamp('now'), 'Plot time: %Y-%m-%d %H:%M:%S UT');
  
  % calculate latest valid data
  idx = find(tt < et);
  if isempty(idx)
    lvd = 'No valid data';
  else
    lvd = strftime(tt(idx(end)), 'Latest valid data: %Y-%m-%d %H:%M:%S UT');
  end
  text('Parent', gh, ...
       'Units', 'normalized', ...
       'Position', [0.05 0.95], ...
       'FontSize', 12, ...
       'HorizontalAlignment', 'left', ...
       'VerticalAlignment', 'top', ...
       'Color', 'k', ...
       'String', {plottime, lvd});
end

if ~isempty(tsens) | 1
  % Add a legend (if some data is present)
  str = {};
  for n = 1:numel(tsens)
    str{n} = sprintf('Sensor #%d', tsens(n));
  end
  legend(gh, str{:}, 0);
  legend(gh, 'boxoff');
 else
   % Do not have a fixed range for the temperature so turned off Y tick
   % marks when no data is available.
   set(gh,'YTick', []);
 end


