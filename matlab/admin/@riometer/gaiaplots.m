function gaiaplots(rio, varargin)
%GAIAPLOTS  Create summary images for GAIA.
%
%   GAIAPLOTS(rio, ...)
%   rio: RIOMETER
%   
% GAIAPLOTS loads riometer data, converts to absorption images and then
% creates summary plots for GAIA <http://gaia-vxo.org/>. 
%
% The default behaviour can be modified with the following name/value
% pairs:
%
%   'starttime', TIMESTAMP
%   The start time of the period for which summary plots should be
%   created. Should be at midnight. Default is yesterday.
%
%   'endtime', TIMESTAMP
%   The end time of the period for which summary plots should be
%   created. Should be at midnight. Default is one day after the start
%   time.
%


defaults.starttime = [];
defaults.endtime = [];
defaults.mia = [];
defaults.imagesize = [64 64];
defaults.limits = [nan nan];
defaults.logscale = false;
[defaults unvi] = interceptprop(varargin, defaults);

day = timespan(1, 'd');

if isempty(defaults.mia)
  
  if isempty(defaults.starttime)
    defaults.starttime = timestamp('yesterday');
  else
    defaults.starttime = floor(defaults.starttime, day);
  end
  
  if isempty(defaults.endtime)
    defaults.endtime = defaults.starttime + day;
  else
    defaults.endtime = ceil(defaults.endtime, day);
  end
  
elseif isa(defaults.mia, 'rio_abs')
  if getinstrument(defaults.mia) ~= rio
    error('instrument does not agree');
  end
  defaults.starttime = floor(getstarttime(defaults.mia), day);
  defaults.endtime = ceil(getendtime(defaults.mia), day);
  
else
  defaults.mia
  error('bad value for data');
end

[imagingbeams widebeams] = info(rio, 'beams');

if ~isempty(defaults.mia)
  imagingbeams = intersect(imagingbeams, getbeams(defaults.mia));
end

% keogramsz = [720 200];
% imagesz = [100 100];
res = timespan(1, 'm');
keogramsz = [720 defaults.imagesize(2)];


pixunits = 'km';

[defxpix defypix] = info(rio, 'pixels', 'units', pixunits);

if (rio == rio_kil_1 | rio == rio_and_2)
  % nasty hack for Kilpisjarvi and Andoya
  defxpix = [-116 116];
  defypix = defxpix;

%   varargin{end+1} = 'logscale';
%   unvi(end+1) = numel(varargin);
%   varargin{end+1} = true;
%   unvi(end+1) = numel(varargin);

%   varargin{end+1} = 'limits';
%   unvi(end+1) = numel(varargin);
%   varargin{end+1} = [-.5 10];
%   unvi(end+1) = numel(varargin);
  
end

xpixrange = max(defxpix) - min(defxpix);
ypixrange = max(defypix) - min(defypix);
xpixstep = xpixrange ./ defaults.imagesize(1);
ypixstep = ypixrange ./ defaults.imagesize(2);
xpix = (min(defxpix)+xpixstep./2):xpixstep:max(defxpix);
ypix = (min(defypix)+ypixstep./2):ypixstep:max(defypix);


dirfstr = fullfile('/data/summary/gaia', ...
		   class(rio), ...
		   getcode(rio), ...
		   '%Y', '%m', '%d');



% Need to check whether having complete data still results in part of the
% image being missing (ie is outside of proper field of view). If so then
% the image files should use the magic value 0 (don't display) instead of
% 255 (bad data value). Therefore create a dummy dataset where all data
% is present and convert to an image. Anything missing must be due to
% being outside of the FoV, not missing data. This can be used as a mask
% later.

dummy = rio_abs('starttime', defaults.starttime, ...
		'endtime', defaults.starttime + res, ...
		'sampletime', defaults.starttime + res/2, ...
		'integrationtime', res, ...
		'instrument', rio, ...
		'beams', imagingbeams, ...
		'load', 0, ...
		'data', ones(numel(imagingbeams), 1));
		
dummyimg = rio_image(dummy, ...
		     'xpixelpos', xpix, ...
		     'ypixelpos', ypix, ...
		     'pixelunits', pixunits, ...
		     'interpmethod', 'linear');

mask = ~isnan(getdata(dummyimg, ':', ':', 1));






t = defaults.starttime;

abs = [];
qdc = [];

loadoptions = {'resolution', res, ...
	       'resolutionmethod', 'nonanmean', ...
	       'failiffilemissing', 0};
while t < defaults.endtime
  t2 = t + day;
  
  if isempty(defaults.mia)
    [abs qdc tries] = ...
        rio_abs_with_qdc('abs', abs, ...
                         'qdc', qdc, ...
                         'starttime', t, ...
                         'endtime', t2, ...
                         'instrument', rio, ...
                         'beams', imagingbeams, ...
                         'log', 0, ...
                         'load', 1, ...
                         'loadoptions', loadoptions);
  else
    abs = extract(defaults.mia, ...
                  'starttime', t, ...
                  'endtime', t2);
  end

  img = rio_image(abs, ...
		  'xpixelpos', xpix, ...
		  'ypixelpos', ypix, ...
		  'pixelunits', pixunits, ...
		  'interpmethod', 'linear');

 
  lim = defaults.limits;
  def_lim = 0;
  if isnan(lim(1)) & (isnan(lim(2)) | lim(2) > def_lim)
    data = getdata(img);
    if max(data(:)) > def_lim
      lim(1) = def_lim;
      end
  end

  gaiaplots(img, varargin{unvi}, ...
	    'limits', lim, ...
	    'logscale', defaults.logscale, ...
	    'mask', mask);

  t = t2;
end
