function summaryplot(in, varargin)
%SUMMARYPLOT Make daily summary plots for riometer data.
%
%   SUMMARYPLOT(...);
%
%   The following name/value pairs are recognised:
%
%   'starttime', TIMESTAMP:
%   The time to start plotting from.
%
%   'endtime', TIMESTAMP:
%   The last day to plot.
%
%   'beams', DOBULE:
%   The beams to include on the line plots
%
%   'archive', CHAR:
%   The archive to use, for riometers which have different data archives.
%
%   'absorption', LOGICAL
%   Boolean to control if absorption plots are created
%
%   'keogram', LOGICAL
%   Boolean to control if KEOGRAMs are created
%
%   'keogramresolution', TIMESPAN
%   Temporal resolutution of the keograms. Defaults to 2 minutes.
%
%   'movie', LOGICAL
%   Boolean to control if MOVIEs are created
%
%   'movieresolution', TIMESPAN
%   Temporal resolutution of the movies. Defaults to 2 minutes.
%
%   'power', LOGICAL
%   Boolean to control if power/QDC plots are created
%
%   'closefigures', LOGICAL
%   By default all figures are closed after saving to disk. This option
%   can be used to override the normal behaviour.
%
%   'check', LOGICAL
%   Only make images if missing/incorrect.
%
%   'qdcarchive', CHAR:
%   The archive to use when loading QDC data, for riometers which have
%   different archives.
%
%   'realtimemode', LOGICAL
%   Make plots in realtime mode, ie include features such as timestamping
%   the plots
%
%   'remakeprelims', LOGICAL
%   Remake images if preliminary, or missing/incorrect (requires "'check'
%   1")
%
%   'resolution', TIMESPAN
%   Temporal resolution of the power and absorption plots. Defaults to 2
%   minutes.
%
%   'summaryplotdir', CHAR
%   Use an alternative base directory instead of that returned by
%   MIA_SUMMARYPLOTDIR.
%
%   'filenameprefix', CHAR
%   Prefix name of generated file with specified string (default '')
%
%   'stoponerrors', NUMERIC
%   If stoponerrors is zero then do not stop on errors, continue processing
%   if possible; if >= 1 then stop on errors loading power or absorption
%   data; if >= 2 then stop on errors loading quiet day curves.

day = timespan(1, 'd');

% need to strip class part from mfilename results
[tmp mfname] = fileparts(mfilename);

if nargin == 0 | ~isa(in, 'riometer')
  error('first parameter must be a riometer object');

elseif length(in) > 1
  % accept matrix of riometers, but process one by one
  for n = 1:prod(size(in))
    feval(mfname, in(n), varargin{:});
  end
  return

elseif nargin == 1
  % mfame();
  feval(mfname, in, ...
	'starttime', floor(timestamp('now'), day) - day);
  return;
  
elseif isa(varargin{1}, 'timestamp')
  if nargin >= 3 & isa(varargin{2}, 'timestamp')
    % mfname(in, timestamp, timestamp, ...);
    feval(mfname, in, ...
	  'starttime', varargin{1}, ...
	  'endtime', varargin{2}, ...
	  varargin{3:end});
    return;
  else
    % mfilename(in, timestamp, ...);
    feval(mfname, in, ...
	  'starttime', varargin{1}, ...
	  'endtime', varargin{1} + day, ...
	  varargin{3:end});
    return;
  end

elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % mfilename(<parameter name/value pairs>)

else
  error('incorrect parameters');
end

[imagingBeams wideBeams] = info(in, 'beams');

defaults.starttime = [];
defaults.endtime = day;
defaults.resolution = timespan(2,'m');
defaults.resolutionmethod = 'nonanmean';
defaults.beams = []; % evaluate the preferred beam each time

defaults.archive = '';
defaults.qdcarchive = '';
defaults.spacing = [0.15 0.15 0.05 0.05];
defaults.graphpos = [0 0.0 1 0.85];

defaults.power = 1;       % boolean to control if power/QDC plots created
defaults.powergrid = 'on';
% We want the power to be plotted on the same limits as QDCs
defaults.powerylim = info(in, 'limits', info(in, 'qdcclass'));

defaults.maxqdctries = [];

defaults.absorption = 1;  % boolean to control if absorption plots created
defaults.absorptiongrid = 'on';

defaults.interpmethod = 'linear';
% defaults.interpmethod = 'v4';

defaults.projectionheight = info(in, 'defaultheight');

% defaults.keogram = 1;     % boolean to control if keograms are created
defaults.keogram = ~isempty(imagingBeams);  
defaults.keogrambeams = [];
defaults.keogramunits = 'deg';
defaults.keogramxpixelpos = [];
defaults.keogramypixelpos = [];
defaults.keogramseriesstyle = 'keogram';
defaults.keogramresolution = timespan(2, 'm');

% defaults.movie = 1;     % boolean to control if movies are created
defaults.movie = ~isempty(imagingBeams);
defaults.moviebeams = [];
defaults.movieunits = 'deg';
defaults.moviexpixelpos = [];  % compute later, when units comfirmed
defaults.movieypixelpos = [];  % compute later, when units confirmed	
defaults.movieunits = 'deg';
defaults.moviescale = [1 1];
defaults.movietimebargap = 2;
defaults.movietimebarheight = 3;
defaults.movietimebar = 1;
defaults.movieresolution = timespan(2, 'm');

% Create temperature plots by default if there is a way to load the data
defaults.temperature = ~isempty(info(in, 'defaultfilename', 'temp_data'));
defaults.temperaturegrid = 'on';

defaults.gaiaplots = (in == rio_kil_1 | in == rio_and_2);
defaults.gaiaplots = 0;
defaults.gaiabeams = [];

defaults.check = 0; % if set only make images if missing/incorrect
defaults.remakeprelims = 1;
defaults.closefigures = 1;

defaults.print = 1; % print to a file
defaults.printsize = [576 432];
defaults.visible = [];

% 'landscape draws figures rotated through 90 degree
% defaults.paperorientation = 'portrait'; 
% defaults.paperunits = 'centimeters';
% defaults.paperposition = [0 0 24 18];


defaults.qdc = [];
defaults.qdcontop = false;

defaults.realtimemode = false;
defaults.summaryplotdir = [];
defaults.filenameprefix = [];
defaults.stoponerrors = 0;

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.starttime)
  error('starttime must be set');
end
varargin{unvi}


if isempty(defaults.keogrambeams)
  defaults.keogrambeams = imagingBeams;
end

t = floor(defaults.starttime, day);
if isa(defaults.endtime, 'timespan')
  defaults.endtime = defaults.starttime + defaults.endtime;
end
defaults.endtime = ceil(defaults.endtime, day);

plotduration = day;
extras.printresolution = 80;

if isempty(defaults.summaryplotdir)
  defaults.summaryplotdir = mia_summaryplotdir;
end
extras.pathformatstr = fullfile(defaults.summaryplotdir, class(in), ...
				sprintf('%s_%d', getabbreviation(in), ...
					getserialnumber(in)), ...
				'%Y', '%m');

% extras is a struct containing additional information used by both
% summaryplot and localCheckSummaryPlots, but is not intended to be
% overriden by the user
extras.fileFormatStr = [defaults.filenameprefix '%s%%Y%%m%%d.png'];
extras.movieFileFormatStr = [defaults.filenameprefix '%s%%Y%%m%%d.gif'];
extras.movieInfoFormatStr = [defaults.filenameprefix '%Y%m%d.txt'];
extras.keogramfractionalheight = 0.5;
extras.keogramprintsize = defaults.printsize .* ...
    [1 extras.keogramfractionalheight];


if defaults.keogram
  [tmpxpix tmpypix] = info(in, 'pixels', ...
			   'units', defaults.keogramunits, ...
			   'beams', defaults.keogrambeams);
  if isempty(defaults.keogramxpixelpos)
    defaults.keogramxpixelpos = tmpxpix;
  end
  if isempty(defaults.keogramypixelpos)
    defaults.keogramypixelpos = tmpypix;
  end
  if ~isempty(defaults.resolution) & ~isempty(defaults.keogramresolution)
    defaults.keogramresolution = max(defaults.keogramresolution, ...
				     defaults.resolution);
  end
end

% process movie options
if defaults.movie
  if isempty(defaults.moviexpixelpos) | isempty(defaults.movieypixelpos)
    [defaults.moviexpixelpos defaults.movieypixelpos] = ...
	info(in,'pixels', ...
	     'units', defaults.movieunits, ...
	     'beams', defaults.moviebeams);
  end
  if isempty(defaults.moviebeams)
    defaults.moviebeams = imagingBeams;
  end
  if ~isempty(defaults.resolution) & ~isempty(defaults.movieresolution)
    defaults.movieresolution = max(defaults.movieresolution, ...
				   defaults.resolution);
  end
end

if isempty(defaults.gaiabeams)
  defaults.gaiabeams = imagingBeams;
end

if isempty(defaults.visible)
  if defaults.closefigures
    defaults.visible = 'off';
  else
    defaults.visible = 'on';
  end
end

if isempty(defaults.maxqdctries)
  defaults.maxqdctries = 4;
end


power_cls = '';
if isempty(info(in, 'defaultfilename', 'rio_power'))
  % cannot load power
  
  % try if raw power can be loaded
  if isempty(info(in, 'defaultfilename', 'rio_rawpower'))
    defaults.power = 0;
    loadAbsDirectly = 1;
  else
    power_cls = 'rio_rawpower';
    loadAbsDirectly = 0;
  end

else
  power_cls = 'rio_power';
  loadAbsDirectly = 0;
end

% --- end of options processing ---

if defaults.check
  localCheckSummaryPlots(defaults, extras);
  return;
end



qdcTime = [];
lastQdcTime = [];
qdcTries = 0;

if ~defaults.power & ~defaults.absorption & ~defaults.keogram ...
      & ~defaults.movie
  % Skip past loading data. Remeber temperature and/or GAIA plots might be
  % needed.
  disp('no power/absorption figures needed');
  t = defaults.endtime
end

while t < defaults.endtime
  t2 = t + plotduration;
  
  % evaluate the beams to use for every day, as a riometer may have
  % failed or additional widebeams been added, etc
  if isempty(defaults.beams)
    defaults.beams = info(in, 'preferredbeam', ...
			  'beams', [imagingBeams wideBeams], ...
			  'time', t);
  end
  beamsToMake = defaults.beams;
  if logical(defaults.keogram)
    beamsToMake = union(beamsToMake, defaults.keogrambeams); % add extra beams
  end
  if defaults.movie
    beamsToMake = union(beamsToMake, defaults.moviebeams); % add extra beams
  end
  if defaults.gaiaplots
    beamsToMake = union(beamsToMake, defaults.gaiabeams); % add extra beams
  end
  

  Path = strftime(t, extras.pathformatstr);
  mkdirhier(Path);
  pfilename = strftime(t, sprintf(extras.fileFormatStr, 'p'));  % power
  afilename = strftime(t, sprintf(extras.fileFormatStr, 'a'));  % absorption 
  kfilename = strftime(t, sprintf(extras.fileFormatStr, 'k'));  % keogram
  mofilename = strftime(t, sprintf(extras.movieFileFormatStr, 'm'));  % movie
  movieInfoFileName = strftime(t, extras.movieInfoFormatStr);  % movie info
  
  try
    if loadAbsDirectly
      ipt = [];
      iqdc = [];
    else
      iabs = [];
      % ipt = rio_power('starttime', t, ...
      % ipt = rio_rawpower('starttime', t, ...
      ipt = feval(power_cls, ...
		  'starttime', t, ...
		  'endtime', t2, ...
		  'instrument', in, ...
		  'beams', beamsToMake, ...
		  'load', 1, ...
		  'loadoptions', ...
		  {'resolution', defaults.resolution, ...
		   'resolutionmethod', defaults.resolutionmethod, ...
		   'failiffilemissing', 0, ...
		   'archive', defaults.archive}, ...
		  'log', 0);
   
      % find out the start time for the desired QDC
      qdcTime = info(in, 'qdcstarttime', t);
      
      % if it isn't the same QDC as before then load a new one
      if ~isequal(qdcTime, lastQdcTime)
	iqdc = [];
	qdcTries = 0;
	qt = qdcTime; % time we are now trying to load
	while isempty(iqdc)
	  try
	    qdcTries = qdcTries + 1;
	    disp(sprintf('QDC attempt #%d', qdcTries));
	    iqdc = rio_qdc('time', qt, ...
			   'instrument', in, ...
			   'beams', beamsToMake, ...
			   'log', 0, ...
			   'loadoptions', {'archive', defaults.qdcarchive});
	    % succeeded
	    disp(sprintf('QDC attempt #%d succeeded', qdcTries));
	    
	    if qdcTries > 1
	      iqdc = adddataquality(iqdc, 'Preliminary');
	    end
	    
	    % break;
	  catch
	    % loading the QDC failed. Go back a bit and try an older one.
	    iqdc = [];
	    
	    if defaults.stoponerrors >= 2
	      'rethrow #1'
	      rethrow(lasterror);
	    end
	     
	    if qdcTries <= defaults.maxqdctries
	      disp(sprintf('QDC attempt #%d failed', qdcTries));
	      disp(['Error was: ' lasterr]);
	      qt = qt - timespan(14, 'd');
	    else
	      break;
	    end
	  end
	end
      end
    end

    % --- power ---
    if defaults.power
      if isempty(iqdc)
	% plot just power
	disp('PLOT(ipt)')
	[fh gh] = plot(ipt, 'beams', defaults.beams, ...
		       'ylim', defaults.powerylim, ...
		       'spacing', defaults.spacing, ...
		       'visible', defaults.visible, ...
		       varargin{unvi});
	set(gh, 'XGrid', defaults.powergrid, ...
		'YGrid', defaults.powergrid);
	set(fh, 'ResizeFcn', '', ...
		'Resize', 'off', ...
		'PaperOrientation', 'portrait', ...
		'PaperPositionMode', 'auto', ...
		'Units', 'inch', ...
		'Position', [0 0 defaults.printsize ./ ...
		    extras.printresolution], ...
		'Tag', 'powerplot');
	localFixFigure(ipt, 'power_plot', fh, gh, defaults);
	
	if defaults.print
	  fn = fullfile(Path, pfilename);
	  if exist(fn)
	    delete(fn);
	  end
	  % print('-dpng', sprintf('-f%g', fh), ...
	  %  sprintf('-r%g', extras.printresolution), fn);
	  figure2imagefile(fh, fn, 'size', defaults.printsize);
	  % localPreliminaryPNG(fn, qdcTries > 1 | local_ispreliminary(ipt));
        end
	% error('Too many failed attempts to load a QDC')
	disp('Too many failed attempts to load a QDC')
      else
	
	% power and QDC plot
	disp(sprintf('PLOTQ: beams %s', printseries(defaults.beams)));
	assignin('base', 'ipt', ipt);
	assignin('base', 'iqdc', iqdc);
	[fh gh] = plotq(ipt, 'beams', defaults.beams, ...
			'ylim', defaults.powerylim, ...	
			'qdc', iqdc, ...
                        'qdcontop', defaults.qdcontop, ...
			'legendboxvisible', 1, ...
			'visible', defaults.visible, ...
			varargin{unvi});

	set(gh, 'XGrid', defaults.powergrid, ...
		'YGrid', defaults.powergrid);
	set(fh, 'ResizeFcn', '', ...
		'Resize', 'off', ...
		'PaperOrientation', 'portrait', ...
		'PaperPositionMode', 'auto', ...
		'Units', 'inch', ...
		'Position', [0 0 defaults.printsize ./ ...
		    extras.printresolution], ...
		'Tag', 'powerplot');
	
	localFixFigure(ipt, 'power_plotq', fh, gh, defaults);
	if qdcTries > 1
	  % mark as provisional
	  localPreliminary(gh);
	end
      end
      
      if defaults.print
	fn = fullfile(Path, pfilename);
	if exist(fn)
	  delete(fn);
	end
	% print('-dpng', sprintf('-f%g', fh), ...
	% sprintf('-r%g', extras.printresolution), fn);
	figure2imagefile(fh, fn, 'size', defaults.printsize);
	localPreliminaryPNG(fn, ...
			    (qdcTries > 1 & isa(iqdc, 'rio_qdc_base')) ...
			    | local_ispreliminary(ipt) ...
			    | local_ispreliminary(iqdc));
      end
      if defaults.closefigures
	close(fh);
      end
      fh = [];
      
    end
    % --- end of power plot ---
    if ~isempty(iqdc) | loadAbsDirectly
      if defaults.absorption | defaults.keogram ...
            | defaults.movie | defaults.gaiaplots
	if loadAbsDirectly
	  iabs = rio_abs('starttime', t, ...
			 'endtime', t2, ...
			 'instrument', in, ...
			 'beams', beamsToMake, ...
			 'log', 0, ...
			 'load', 1, ...
			 'loadoptions', ...
			 {'resolution', defaults.resolution, ...
			  'resolutionmethod', defaults.resolutionmethod, ...
			  'archive', defaults.archive});
	else
	  iabs = rio_abs(ipt, iqdc);
	end
	
      end
      
      % --- absorption plot ---
      if defaults.absorption
	disp('PLOT(iabs)');
	[fh gh] = plot(iabs, ...
		       'beams', defaults.beams, ...
		       'spacing', defaults.spacing, ...
		       'visible', defaults.visible, ...
		       varargin{unvi});

	% don't let absorption plot go more negative than -1dB, or more
	% positive than 0.
	ylim = get(gh, 'YLim');
	ylim(1) = max(ylim(1), -1);
	ylim(1) = min(ylim(1), 0);
	if ylim(2) < ylim(1)
	  ylim = get(gh, 'YLim');
	end
	
	set(gh, 'YLim', ylim, ...
		'XGrid', defaults.powergrid, ...
		'YGrid', defaults.powergrid);
	set(fh, 'ResizeFcn', '', ...
		'Resize', 'off', ...
		'PaperOrientation', 'portrait', ...
		'PaperPositionMode', 'auto', ...
		'Units', 'inch', ...
		'Position', [0 0 defaults.printsize./ ...
		    extras.printresolution], ...
		'Tag', 'absplot');
	localFixFigure(iabs, 'abs_plot', fh, gh, defaults);
	if qdcTries > 1
	  % mark as provisional
	  localPreliminary(gh);
	end
	
	if defaults.print
	  fn = fullfile(Path, afilename);
	  figure2imagefile(fh, fn, 'size', defaults.printsize);
	  % print('-dpng', sprintf('-f%g', fh), ...
	  %   sprintf('-r%g', extras.printresolution), fn);
	  localPreliminaryPNG(fn, qdcTries > 1 | local_ispreliminary(iabs));
	end
	if defaults.closefigures
	  close(fh);
	end
	fh = [];
      end
      
      % --- keogram ---
      if logical(defaults.keogram)
	id = rio_image(setresolution(iabs, ...
				     defaults.keogramresolution, ...
				     defaults.resolutionmethod), ...
		       'height', defaults.projectionheight, ...
		       'pixelunits', defaults.keogramunits, ...
		       'xpixelpos', defaults.keogramxpixelpos, ...
		       'ypixelpos', defaults.keogramypixelpos, ...
		       'interpmethod', defaults.interpmethod);

	% adjust graphpos to allow for footer
	footerheight = 0.1; % proportion of full window height
	graphpos = defaults.graphpos;
	graphpos(4) = graphpos(4) - footerheight;
	graphpos(2) = graphpos(2) + footerheight;

	% calculate new graphpos and spacing values, since we wish to scale
	% the plot in height. After scaling we aim to have the header the same
	% size, so must make fractionally bigger

	newgraphpos = graphpos;
	newgraphpos(2) = graphpos(2) / extras.keogramfractionalheight;
	newgraphpos(4) = 1 - newgraphpos(2) - ((1-graphpos(2)-graphpos(4))/ ...
					       extras.keogramfractionalheight);
	% adjust the spacing around the graph so that after scaling it is
	% the same (pixel) size
	spacing = defaults.spacing;
	% spacing([2 4]) = spacing([2 4]) ./ (newgraphpos(4) / graphpos(4));
	spacing([2 4]) = spacing([2 4]) ./ extras.keogramfractionalheight;

	[fh gh] = keogram(id, varargin{unvi}, ...
			  'seriesstyle', defaults.keogramseriesstyle, ...
			  'spacing', spacing, ...
			  'graphpos', newgraphpos, ...
			  'visible', defaults.visible);

	if 0 
	  % relabel graph to have ticks every 1 hour, but labels only every 4
	  hours = 0:(plotduration/timespan(1,'h'));
	  hoursLen = length(hours);
	  xtick = zeros(1, hoursLen);
	  xticklabel = cell(1, hoursLen);
	  for n = 1:hoursLen
	    xtick(n) = timespan(hours(n), 'h') / defaults.resolution;
	    if rem(hours(n), 4) == 0
	      xticklabel{n} = sprintf('%02d', hours(n));
	    end
	  end
	  set(gh, 'XTick', xtick, ...
		  'XTickLabel', xticklabel);
	end
	
	% set(gh, 'XGrid', defaults.powergrid, ...
	% 'YGrid', defaults.powergrid, ...
	% 'ResizeFcn', '', ...
	set(fh, 'Resize', 'off', ...
		'PaperOrientation', 'portrait', ...
		'PaperPositionMode', 'auto', ...
		'Units', 'inch', ...
		'Position', [0 0 extras.keogramprintsize ./ ...
		    extras.printresolution], ...
		'Tag', 'keogram');

	% saveas(fh, '/home/dcs-aurora/marple/keogramfig', 'fig');
		
	localFixFigure(id, 'keogram', fh, gh, defaults);
	if qdcTries > 1
	  % mark as provisional
	  localPreliminary(gh);
	end
	

	if defaults.print
	  fn = fullfile(Path, kfilename);
	  figure2imagefile(fh, fn, 'size', extras.keogramprintsize);
	  % print('-dpng', sprintf('-f%g', fh), ...
	  % sprintf('-r%g', extras.printresolution), fn);
	  localPreliminaryPNG(fn, qdcTries > 1 | local_ispreliminary(id));
	end
	if defaults.closefigures
	  close(fh);
	end
	fh = [];
	clear id;
      end 
      % --- end of keogram ---
      
      if logical(defaults.movie)
	% do animated gif movie
	id = rio_image(setresolution(iabs, defaults.movieresolution, ...
                                     defaults.resolutionmethod), ...      
		       'pixelunits', defaults.movieunits, ...
		       'xpixelpos', defaults.moviexpixelpos, ... 
		       'ypixelpos', defaults.movieypixelpos, ...
		       'interpmethod', defaults.interpmethod);

	% idd = getdata(id);
	% Make a movie. Pass unknown options, they may have details for
	% movie (timebar options, etc).
	disp(['movie info filename: ' fullfile(Path, movieInfoFileName)]);
	movie(id, ...
	      varargin{unvi}, ...
	      'filename', fullfile(Path, mofilename), ...
	      'makedirectory', 1, ...
	      'scale', defaults.moviescale, ...
	      'timebar', defaults.movietimebar, ...
	      'timebargap', defaults.movietimebargap, ...
	      'timebarheight', defaults.movietimebarheight, ...
	      'preliminary', (qdcTries > 1), ...
	      'infofile', fullfile(Path, movieInfoFileName)); 
	% savemovieinfo(id, fullfile(Path, movieInfoFileName), (qdcTries > 1));
	
      end
      % --- end of movie ---

      
      if logical(defaults.gaiaplots) & ~ispreliminary(iabs)
        gaiaplots(in, 'mia', iabs);
      end
      % --- end of gaiaplots ---
      
    end
    clear iabs;

  catch
    if defaults.closefigures
      close all;
    end
    lerr = lasterr;
    if defaults.stoponerrors > 0
      'rethrow #2'
      rethrow(lasterror)
    end
    if strcmp(lerr, 'Interrupt')
      rethrow(lasterror);
    end
    
    disp(['Could not make plot for ' char(t)]);
    disp(['Error was: ' lerr]);
    disp(' ');
  end

  lastQdcTime = qdcTime;
  t = t + day;
end

if logical(defaults.temperature)
  temperaturesummaryplot(in, ...
                         'starttime', defaults.starttime, ...
                         'endtime', defaults.endtime, ...
                         'printsize', defaults.printsize, ...
                         'summaryplotdir', defaults.summaryplotdir, ...
                         'realtimemode', defaults.realtimemode, ...
                         'grid', defaults.temperaturegrid);
end


function localFixFigure(mia, mode, fh, gh, defaults)
gh = gh(:); 
va = {};

set(fh, 'Color', 'w');
% iskeogram = strcmp(get(fh, 'Tag'), 'keogram');


if logical(defaults.realtimemode) & ...
      any(strcmp(mode, {'power_plot' 'power_plotq' 'abs_plot'}))
  plot_time = strftime(timestamp('now'), 'Plot time: %Y-%m-%d %H:%M:%S UT');
  
  % calculate latest valid data
  [tmp idx] = find(~isnan(getdata(mia)));
  if isempty(idx)
    lvd = 'No valid data';
  else
    lvd = strftime(getsampletime(mia, max(idx)), ...
                   'Latest valid data: %Y-%m-%d %H:%M:%S UT');
  end
else
  plot_time = ''; % don't add plot time to the plots
end

for g = gh(:)'
  wmh = findall(g, 'Type', 'text', 'Tag', 'watermark');
  set(wmh, ...
      'Color', 'r', ...
      'FontWeight', 'bold', ...
      'Units', 'normalized');

  str = get(wmh, 'String'); % always empty if no watermark
  if ~isempty(str)
    extent = get(wmh, 'Extent');
    fontsize = get(wmh, 'FontSize');

    while any(extent(3:4) >= 0.9) & fontsize > 5
      fontsize = fontsize - 1;
      set(wmh, 'FontSize', fontsize);
      extent = get(wmh, 'Extent');
    end
  end

  if ~isempty(plot_time)
    % add timestamp to plot
    text('Parent', g, ...
	 'Units', 'normalized', ...
	 'Position', [0.05 0.95], ...
	 'FontSize', 12, ...
	 'HorizontalAlignment', 'left', ...
	 'VerticalAlignment', 'top', ...
	 'Color', 'k', ...
	 'String', {plot_time, lvd});
  end
end



function localPreliminary(gh)
return
gh = gh(:); 
va = {};
if matlabversioncmp('>', '5.1')
  va = {'VerticalAlignment', 'middle'};
end
% str = strftime(timestamp('now'), 'Preliminary %H:%M:%S');
str = '<Preliminary>';
col = 'r';

for g = gh(:)'
  set(findall(g, 'Type', 'text', 'Tag', 'watermark'), ...
      'Color', col);
  th = text(0.5, 0.5, str, ...
	    'Units', 'normalized', ...
	    'Parent', g, ...
	    'FontUnits', 'pixels', ...
	    'FontSize', 29, ...
	    'FontName', 'helvetica', ...
	    'HorizontalAlignment', 'center', ...
	    va{:}, ...
	    'Color', col, ...
	    'FontWeight', 'bold');
  % 'FontUnits', 'normalized', ...
  % 'FontSize', 0.1, ...
end

return

function localPreliminaryPNG(filename, prelim)
[im map] = imread(filename);
if prelim
  args = {'Warning', 'Preliminary', 'Preliminary', '1'};
else
  args = {'Preliminary', '0'};
end
if isempty(map)
  imwrite(im, filename, args{:});
else
  imwrite(im, map, filename, args{:});
end
return


function localCheckSummaryPlots(defaults, extras)
% turn off to avoid recursive calls to localCheckSummaryPlots 
defaults.check = 0; 
day = timespan(1,'d');
t = defaults.starttime;
tmpfile = tempname2;

while t < defaults.endtime
  Path = strftime(t, extras.pathformatstr); 
  filename = cell(1, 5);
  imsz = cell(size(filename));
  format = cell(size(filename));
  remake = ones(size(filename));
  
  % power (1)
  filename{1} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileFormatStr, 'p')));
  imsz{1} = defaults.printsize;
  format{1} = 'png';

  % absorption (2)
  filename{2} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileFormatStr, 'a')));
  imsz{2} = defaults.printsize;
  format{2} = 'png';
  
  % keogram (3)
  filename{3} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileFormatStr, 'k')));
  imsz{3} = extras.keogramprintsize;
  format{3} = 'png';
  
  % movie (4)
  filename{4} = fullfile(Path, ...
			 strftime(t, sprintf(extras.movieFileFormatStr, ...
					     'm')));
  imsz{4} = [length(defaults.movieypixelpos) + ...
	     defaults.movietimebargap + defaults.movietimebarheight, ...
	     length(defaults.moviexpixelpos)];
  format{4} = '';
  
  % movie info (5)
  filename{5} = fullfile(Path, ...
			 strftime(t, extras.movieInfoFormatStr));
  format{5} = '';
  remake(5) = 0; % made along with movie
  
  % check all standard images
  for n = 1:3
    if ~isempty(format{n}) & exist(filename{n}, 'file')
      try
	inf = imfinfo(filename{n});
	if ~strcmp(lower(inf.Format), format{n})
	  ;
	elseif ~isequal([inf.Width inf.Height], imsz{n})
	  ;
	elseif strcmp(inf.Warning, 'Preliminary') & defaults.remakeprelims
	  ; preliminary
	else
	  remake(n) = 0; % OK
	end
      catch
	if defaults.stoponerrors > 0
	  'rethrow #3'
	  rethrow(lasterror)
	end
	if strcmp(lasterr, 'Interrupt')
	  rethrow(lasterror);
	end
      end
    else
      ;
    end
    
  end
  
  % check movies
  if exist(filename{4}, 'file') & exist(filename{5}, 'file')
    % files exist, but are they correct
    try
      [zz zzz] = unix(['convert ''' filename{4} '[1]'' png:' tmpfile]);
      [im map] = imread(tmpfile, 'png');
      limits = load(filename{5});
      if ~isequal(size(im), imsz{4})
	% disp('wrong size');
	;
      elseif 0 & ~isequal(map(double(im(length(defaults.movieypixelpos) + ...
					1,1))+1, :), [1 1 1])
	% background color in blank part was not white
	% disp('wrong background color');
	;
      elseif limits(1) > limits(2)
	; % bad limit information
      elseif limits(3) & defaults.remakeprelims
	; % preliminary image must be remade
      else
	remake(4) = 0;
      end
      delete(tmpfile);
    
    catch
      disp(['Error was: ' lasterr]);
      delete(tmpfile);
      if strcmp(lasterr, 'Interrupt')
	rethrow(lasterr);
      end
    end
  end

  if any(remake)
    disp(['remaking ', strftime(t, '%Y-%m-%d')]);
    summaryplot('starttime', t, ...
		'power', remake(1), ...
		'absorption', remake(2), ...
		'keogram', remake(3), ...
		'movie', remake(4));
  else
    disp(['checked: ', strftime(t, '%Y-%m-%d')]);
  end
  
  t = t + day;
end

function r = local_ispreliminary(mia)
if isnumeric(mia)
  % mia is probably [], meaning no valid data to plot
  r = 0;
else
  r = any(strcmpi(getdataquality(mia), 'Preliminary'));
end
return
