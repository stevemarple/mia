function copyqdc(rio, loaddate, savedate, varargin)
%COPYQDC Copy a QDC from one QDC period to another
%
%   COPYQDC(rio, t1, t2, ...)
%   rio: RIOMETER instrument
%   t1: any time within the source QDC period (scalar or matrix TIMESTAMP)
%   t2: any time within the destination QDC period (scalar TIMESTAMP)
%
% COPYQDC copies quiet day curves form one period to another. The two
% periods must be different. If t1 is non-scalar then multiple quiet day
% curves are loaded and the mean value taken (see rio_qdc_base/MEAN). The
% default behaviour can be modified with the following name/value pairs:
%
%   'beams', NUMERIC
%   A list of the beams for which quiet day curves should be copied. If
%   not given it default to all beams (INFO(rio, 'allbeams')).
%
%   'qdcclass', CHAR
%   The name of the QDC class to use when loading data. If not specified
%   uses the default QDC class for the given RIOMETER instrument (as
%   returned by INFO(rio, 'qdcclass')).
%
%   'cleardataquality', LOGICAL, CHAR or CELL
%   If TRUE all data quality warnings are cleared; if FALSE no data
%   quality warnings are cleared. If a CHAR or CELL is given then the
%   corresponding data quality warnings are cleared (see
%   mia_base/CLEARDATAQUALITY).
%
%   'adddataquality', CHAR or CELL
%   If non-empty the given data quality warning(s) are appended to any
%   existing warnings.
%
% See also MAKEQDC, CHECKQDC, RIO_QDC, RIO_RAWQDC, RIO_QDC_FFT, RIO_RAWQDC_FFT.

defaults.beams = info(rio, 'allbeams');
defaults.qdcclass = info(rio, 'qdcclass');
defaults.cleardataquality = true;
defaults.adddataquality = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~isa(loaddate, 'timestamp')
  error('loaddate must be a timestamp');
elseif ~isa(savedate, 'timestamp')
  error('savedate must be a timestamp');
end

% For the given loaddate calculate the start of the QDC period
loaddate_qst = loaddate; % initialise to correct size
for n = 1:numel(loaddate)
  loaddate_qst(n) = calcqdcstarttime(rio, loaddate(n), loaddate(n));
end

% For the given savedate calculate the start and end of the QDC period
savedate_qst = calcqdcstarttime(rio, savedate, savedate);
savedate_qet = savedate_qst + info(rio, 'qdcduration');

% Don't want to be copying back top the same period (makes no sense) so
% check the the start of the two periods are different
if any(loaddate_qst == savedate_qst)
  error('loaddate and savedate fall within the same QDC period');
end

for beam = defaults.beams(:)'
  disp(sprintf('processing beam %d', beam));
  
  loaddate_qst_str = cell(1, numel(loaddate));
  for n = 1:numel(loaddate)
    qdc(n) = feval(defaults.qdcclass, ...
		   'instrument', rio, ...
		   'time', loaddate_qst(n), ...
		   'beams', beam);
    loaddate_qst_str{n} = strftime(loaddate_qst(n), '%Y-%m-%d');
  end
  
  loaddate_qst_str = join(',', loaddate_qst_str);
  
  if numel(qdc) ~= 1
    % Have multiple QDCs, so take the mean
    qdc = mean(qdc);
  end
  
  qdc2 = qdc;
  qdc2 = setqdcperiodstarttime(qdc2, savedate_qst);
  qdc2 = setqdcperiodendtime(qdc2, savedate_qet);
  qdc2 = addprocessing(qdc2, ...
		       sprintf('copied from %s to %s', ...
			       loaddate_qst_str, ...
			       strftime(savedate_qst, '%Y-%m-%d')));
  
  if isequal(defaults.cleardataquality, false)
    % do nothing
  elseif isequal(defaults.cleardataquality, true)
    qdc2 = cleardataquality(qdc2);
  else
    qdc2 = cleardataquality(qdc2, defaults.cleardataquality);
  end
  
  if ~isempty(defaults.adddataquality)
    qdc2 = adddataquality(qdc2, defaults.adddataquality);
  end
  
  % qdc,qdc2
  save(qdc2);
end
