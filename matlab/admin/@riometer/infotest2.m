function infotest2(in)
%INFOTEST  Test the INFO functions specific to RIOMETER objects.
%
% Do not call this function directly. Use INFOTEST instead.


% test only the most important ones for now

imagingbeams = info(in, 'imagingbeams');
widebeams = info(in, 'widebeams');

% should always accept a list of beams, and an optional time
b = info(in, 'preferredbeam', ...
	 'beams', union(imagingbeams, widebeams), ...
	 'time', timestamp('now'));
