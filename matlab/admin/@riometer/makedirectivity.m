function makedirectivity(rio, varargin)
%MAKEDIRECTIVITY  Pre-compute the DIRECTIVITY matrix for a RIOMETER antenna.
%
%   MAKEDIRECTIVITY(rio, ...)
%   
%   rio: RIOMETER object(s);
%
% The following parameter name/value pairs are accepted:
%
%    'azimuth'
%    'zenith'
%    'geodesicmesh'
%    See DIRECTIVITY for a description of their behaviour.
%
% This function saves the DIRECTIVITY matrix using the standard filename
% (as returned by DIRECTIVITY) into a standard location, thus it is only
% intended to be called by users with admininstration rights.
%
% See also RIOMETER, DIRECTIVITY.


defaults.geodesicmesh = [];
defaults.azimuth = [];
defaults.zenith = [];
defaults.directory = fullfile(mia_basedir, 'riometer');
[defaults unvi] = interceptprop(varargin, defaults);

for n = 1:prod(size(rio))
  [imagingBeams wideBeams] = info(rio(n), 'beams');
  beams = union(imagingBeams, wideBeams);
  
  
  [r az zen fname] = ...
      directivity(rio(n), ...
		  'loadifpossible', 0, ...
		  'beams', beams, ...
		  'geodesicmesh', defaults.geodesicmesh, ...
		  'azimuth', defaults.azimuth, ...
		  'zenith', defaults.zenith);

  [Path fname ext] = fileparts(fname);
  fname = fullfile(defaults.directory, [fname ext]);
  disp(sprintf('saving to %s', fname));
  save(fname, 'r', 'az', 'zen', 'beams');
  
  if isequal(defaults.azimuth, defaults.zenith, [])
    % remember mesh for next riometer, to speed up execution
    defaults.azimuth = az;
    defaults.zenith = zen;
  end
end
