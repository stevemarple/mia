function process_arcom_temp_data(rio, varargin)
%PROCESS_ARCOM_TEMP_DATA Process temperature data from ARCOM files
%
%   PROCESS_ARCOM_TEMP_DATA(rio, st)
%   PROCESS_ARCOM_TEMP_DATA(rio, st, et)
%   rio: RIOMETER object
%   st: start time (TIMESTAMP)
%   et: end time (TIMESTAMP)
%
% Process ARCOM files and extract the temperature data.
%
% See also ARCOMTEMPPLOT, TEMPERATURESUMMARYPLOT.

day = timespan(1, 'd');

defaults.starttime = timestamp('yesterday');
defaults.endtime = [];
defaults.archive = '';


if rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
elseif nargin == 3
  defaults.starttime = varargin{1};
  defaults.endtime = varargin{2};
else
  error('incorrect parameters');
end


df = info(rio, 'defaultfilename', 'original_format', ...
	       'archive', defaults.archive);
df_temp = info(rio, 'defaultfilename', 'temp_data', ...
	       'archive', defaults.archive);
procmessage = sprintf('Created by %s', basename(mfilename));

st = floor(defaults.starttime, df.duration);
et = ceil(defaults.endtime, df.duration);


filename_fstr = info(rio, 'includesequencenumber', df.fstr);

% Process for a file duration before to one afterwards as the data is not
% guaranteed to be aligned in the ARCOM files, but we want the temp_data
% to be properly aligned.
t = st - df.duration;
et2 = et + df.duration;
while t < et2
  t2 = t + df.duration;

  % for %^&^'s sake who thought sequence numbers were a good idea?! Now we
  % have to repeatedly go through all this crap to handle them every time we
  % need to process ARCOM files.
  seq_num = 0;
  while true
    filename = sprintf(strftime(t, filename_fstr), seq_num);
    if url_exist(filename)
      s = arcom_to_structs('filename', filename, ...
                           'wantedpackets', 19);
      
      if ~isempty(s)
        s2 = [s{:}];
        clear s;
        desc_unixtime = [s2.DESCRIPTOR_UNIXTIME];
        samt = timestamp('unixtime', [desc_unixtime.unixtime]);     
        
        % find sensorids in use
        sensorids = [s2(1).DESCRIPTOR_TEMP_READING.sensorid];
        data = zeros(numel(sensorids), numel(s2));
        for n = 1:numel(s2)
          tr = s2(n).DESCRIPTOR_TEMP_READING;
          % Not sure if sensorids can change in a file (will a new file
          % with a large sequence number be generated?) Play it safe.
          if ~isequal([tr.sensorid], sensorids)
            [tr.sensorid]
            sensorids
            error(sprintf('sensor IDs have changed in file %s', filename));
          end
          data(:, n) = [tr.reading]';
        end
        
        % delete any rows for which all readings are NaN
        unusedsensors = [];
        for n = 1:numel(sensorids)
          if all(isnan(data(n, :)))
            unusedsensors(end+1) = n;
          end
        end
        sensorids(unusedsensors) = [];
        data(unusedsensors,:) = [];

        % Ensure the sample times are sorted and unique
        [samt idx] = unique(samt);
        data = data(:, idx);
        
        % Ensure that the sensor ids are sorted and unique
        [sensorids idx] = unique(sensorids);
        data = data(idx, :);
        
        integtime = repmat(timespan(1, 's'), size(samt));
        mia_st = floor(samt(1), df_temp.duration);
        mia_et = ceil(samt(end), df_temp.duration);
        
        mia = temp_data('instrument', rio, ...
                        'starttime', mia_st, ...
                        'endtime', mia_et, ...
                        'data', data, ...
                        'units', df_temp.units, ...
                        'sampletime', samt, ...
                        'integrationtime', integtime, ...
                        'processing', procmessage, ...
                        'sensors', sensorids);
        save(mia, 'merge', true);
      end
    else
      break;
    end
    seq_num = seq_num + 1;
  end
  
  
  
  t = t2;
end
