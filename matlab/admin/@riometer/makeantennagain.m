function makeantennagain(rio, varargin)
%MAKEANTENNAGAIN  Pre-compute the ANTENNAGAIN matrix for a RIOMETER antenna.
%
%   MAKEANTENNAGAIN(rio, ...)
%   
%   rio: RIOMETER object(s);
%
% The following parameter name/value pairs are accepted:
%
%    'azimuth'
%    'zenith'
%    'geodesicmesh'
%    See ANTENNAGAIN for a description of their behaviour.
%
% This function saves the ANTENNAGAIN matrix using the standard filename
% (as returned by ANTENNAGAIN) into a standard location, thus it is only
% intended to be called by users with admininstration rights.
%
% See also RIOMETER, ANTENNAGAIN.


defaults.geodesicmesh = [];
defaults.azimuth = [];
defaults.zenith = [];
defaults.directory = fullfile(mia_basedir, 'riometer');
[defaults unvi] = interceptprop(varargin, defaults);

for n = 1:prod(size(rio))
  [imagingBeams wideBeams] = info(rio(n), 'beams');
  beams = union(imagingBeams, wideBeams);
  
  
  [r az zen fname] = ...
      antennagain(rio(n), ...
		  'loadifpossible', 0, ...
		  'beams', beams, ...
		  'geodesicmesh', defaults.geodesicmesh, ...
		  'azimuth', defaults.azimuth, ...
		  'zenith', defaults.zenith, ...
                  'verbose', 1);

  [Path fname ext] = fileparts(fname);
  fname = fullfile(defaults.directory, [fname ext]);
  disp(sprintf('saving to %s', fname));
  save(fname, 'r', 'az', 'zen', 'beams');
  
  if isequal(defaults.azimuth, defaults.zenith, [])
    % remember mesh for next riometer, to speed up execution
    defaults.azimuth = az;
    defaults.zenith = zen;
  end
end
