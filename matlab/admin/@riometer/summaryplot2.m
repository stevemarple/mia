function summaryplot2(in, varargin)
%SUMMARYPLOT Make daily summary plots for riometer data.
%
%   SUMMARYPLOT(...);
%
%   The following name/value pairs are recognised:
%
%   'starttime', <TIME object>:
%   The time to start plotting from.
%
%   'endtime', <TIME object>:
%   The last day to plot.
%
%   'beams', <beams>:
%   The beams to include on the line plots
%
%   'absorption' <0|1>
%   Boolean to control if absorption plots are created
%
%   'keogram' <0|1>
%   Boolean to control if KEOGRAMs are created
%
%   'power' <0|1>
%   Boolean to control if power/QDC plots are created
%
%   'closefigures'
%   By default all figures are closed after saving to disk. This option
%   can be used to override the normal behaviour.
%
%   'check' <0|1>
%   Only make images if missing/incorrect
%
%   'remakeprelims' <0|>
%   Remake images if preliminary, or missing/incorrect (requires "'check'
%   1")
%

day = timespan(1, 'd');

% need to strip class part from mfilename results
[tmp mfname] = fileparts(mfilename);

if nargin == 0 | ~isa(in, 'riometer')
  error('first parameter must be a riometer object');
elseif nargin == 1
  % mfame();
  feval(mfname, in, ...
	'starttime', floor(timestamp('now'), day) - day);
  return;
elseif nargin == 2 & isa(varargin{1}, 'timestamp')
  % mfilename(timestamp);
  feval(mfname, in, ...
	'starttime', varargin{1}, ...
	'endtime', varargin{1} + day);
  return;
elseif nargin >= 3 & isa(varargin{1}, 'timestamp') & ...
      isa(varargin{2}, 'timestamp')
  % mfilename(timestamp, timestamp);
  feval(mfname, in, ...
	'starttime', varargin{1}, ...
	'endtime', varargin{2}, ...
	varargin{3:length(varargin)});
  return;
elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % mfilename(<parameter name/value pairs>)
else
  error('incorrect parameters');
end





defaults.starttime = [];
defaults.endtime = day;
defaults.resolution = timespan(2,'m');
defaults.beams = [];

defaults.spacing = [0.15 0.15 0.05 0.05];
defaults.graphpos = [0 0.0 1 0.85];

defaults.power = 1;       % boolean to control if power/QDC plots created
defaults.powergrid = 'on';
% We want the power to be plotted on the same limits as QDCs
defaults.powerylim = info(rio_kil, 'limits', 'rio_qdc');

defaults.maxqdctries = 4;

defaults.absorption = 1;  % boolean to control if absorption plots created
defaults.absorptiongrid = 'on';

defaults.interpmethod = 'linear';

defaults.keogram = 1;     % boolean to control if keograms are created
defaults.keogrambeams = [];
defaults.keogramunits = 'deg';
defaults.keogramxpixelpos = [];
defaults.keogramypixelpos = [];
defaults.keogramseriesstyle = 'keogram';

defaults.movie = 1;     % boolean to control if movies are created
defaults.moviebeams = [];
defaults.movieunits = 'deg';
defaults.moviexpixelpos = [];  % compute later, when units comfirmed
defaults.movieypixelpos = [];  % compute later, when units confirmed	
defaults.movieunits = 'deg';
defaults.moviescale = [1 1];
defaults.movietimebargap = 2;
defaults.movietimebarheight = 3;
defaults.movietimebar = 1;

defaults.check = 0; % if set only make images if missing/incorrect
defaults.remakeprelims = 1;
defaults.closefigures = 1;

defaults.print = 1; % print to a file
defaults.printsize = [576 432];
defaults.visible = [];

% 'landscape draws figures rotated through 90 degree
% defaults.paperorientation = 'portrait'; 
% defaults.paperunits = 'centimeters';
% defaults.paperposition = [0 0 24 18];


defaults.qdc = [];

[defaults unvi] = interceptprop(varargin, defaults);


if isempty(defaults.starttime)
  error('starttime must be set');
end
varargin{unvi}
[imagingBeams wideBeams] = info(in, 'beams');

if isempty(defaults.beams)
  % take first widebeam, else first imaging beam
  defaults.beams = [wideBeams imagingBeams];
  defaults.beams = defaults.beams(1);
end
if isempty(defaults.keogrambeams)
  defaults.keogrambeams = imagingBeams;
end

t = floor(defaults.starttime, day);
if isa(defaults.endtime, 'timespan')
  defaults.endtime = defaults.starttime + defaults.endtime;
end
defaults.endtime = ceil(defaults.endtime, day);

plotduration = day;

% extras is a struct containing additional information used by both
% summaryplot and localCheckSummaryPlots, but is not intended to be
% overriden by the user
extras.printresolution = 80;
extras.pathformatstr = fullfile(mia_datadir, 'summary', 'rio', ...
				getabbreviation(in), '%Y', '%m');
extras.fileformatstr = '%s%%Y%%m%%da.png';
extras.moviefileformatstr = '%s%%Y%%m%%d.gif';
extras.movieinfoformatstr = '%Y%m%d.txt';
extras.keogramfractionalheight = 0.5;
extras.keogramprintsize = defaults.printsize .* ...
    [1 extras.keogramfractionalheight];

beamsToMake = defaults.beams;

if defaults.keogram
  beamsToMake = union(beamsToMake, defaults.keogrambeams); % add extra beams
end

[tmpxpix tmpypix] = info(in, 'pixels', ...
			 'units', defaults.keogramunits, ...
			 'beams', defaults.keogrambeams);
if isempty(defaults.keogramxpixelpos)
  defaults.keogramxpixelpos = tmpxpix;
end
if isempty(defaults.keogramypixelpos)
  defaults.keogramypixelpos = tmpypix;
end

% process movie options
if isempty(defaults.moviexpixelpos) | isempty(defaults.movieypixelpos)
  [defaults.moviexpixelpos defaults.movieypixelpos] = ...
      info(in,'pixels', ...
	   'units', defaults.movieunits, ...
	   'beams', defaults.moviebeams);
end
if isempty(defaults.moviebeams)
  defaults.moviebeams = imagingBeams;
end
if defaults.movie
  beamsToMake = union(beamsToMake, defaults.moviebeams); % add extra beams
end

if isempty(defaults.visible)
  if defaults.closefigures
    defaults.visible = 'off';
  else
    defaults.visible = 'on';
  end
end

% --- end of options processing ---

if defaults.check
  localCheckSummaryPlots(defaults, extras);
  return;
end



qdcTime = [];
lastQdcTime = [];
qdcTries = 0;

while t < defaults.endtime
  t2 = t + plotduration;
  Path = strftime(t, extras.pathformatstr);
  mkdirhier(Path);
  pfilename = strftime(t, sprintf(extras.fileformatstr, 'p'));  % power
  afilename = strftime(t, sprintf(extras.fileformatstr, 'a'));  % absorption 
  kfilename = strftime(t, sprintf(extras.fileformatstr, 'k'));  % keogram
  mfilename = strftime(t, sprintf(extras.moviefileformatstr, 'm'));  % movie
  movieInfoFileName = strftime(t, extras.movieinfoformatstr);  % movie info
  
  try
    ipt = rio_power('starttime', t, ...
		    'endtime', t2, ...
		    'resolution', defaults.resolution, ...
		    'instrument', in, ...
		    'load', 1);
    
    % find out the start time for the desired QDC
    qdcTime = info(in, 'qdcstarttime', t);
    
    % if it isn't the same QDC as before then load a new one
    if ~isequal(qdcTime, lastQdcTime)
      iqdc = [];
      qdcTries = 0;
      qt = qdcTime; % time we are now trying to load
      while isempty(iqdc)
	try
	  qdcTries = qdcTries + 1;
	  disp(sprintf('QDC attempt #%d', qdcTries));
	  
	  % iqdc = irisqdc(qt, defaults.location, beamsToMake);
	  iqdc = rio_qdc('time', qt, ...
			 'instrument', in, ...
			 'beams', beamsToMake);
	  % succeeded
	  disp(sprintf('QDC attempt #%d succeeded', qdcTries));
				     % break;
	catch
	  % loading the QDC failed. Go back a bit and try an older one.
	  iqdc = [];
	  % if qdcTries <= 3
	  if qdcTries <= defaults.maxqdctries
	    disp(sprintf('QDC attempt #%d failed', qdcTries));
	    disp(['Error was: ' lasterr]);
	    qt = qt - timespan(14, 'd');
	  else
	    break;
	  end
	end
      end
    end
    
    % --- power ---
    if defaults.power
      if isempty(iqdc)
	% plot just power
	[fh gh] = plot(ipt, 'beam', defaults.beams, ...
		       'ylim', defaults.powerylim, ...
		       'spacing', defaults.spacing, ...
		       'graphpos', defaults.graphpos, ...
		       'visible', defaults.visible, ...
		       varargin{unvi});
	set(gh, 'XGrid', defaults.powergrid, ...
		'YGrid', defaults.powergrid);

	if defaults.print
	  fn = fullfile(Path, pfilename);
	  if exist(fn)
	    delete(fn);
	  end
	  
	  if 1
	    % new way under test
	    figure2png(fh, fn, defaults.printsize);
	  else
	    set(fh, 'ResizeFcn', '', ...
		    'Resize', 'off', ...
		    'PaperOrientation', 'portrait', ...
		    'PaperPositionMode', 'auto', ...
		    'Units', 'inch', ...
		    'Position', [0 0 defaults.printsize ./ ...
		    extras.printresolution], ...
		    'Tag', 'powerplot');
	    
	    if 1 
	      print('-dpng', sprintf('-f%g', fh), ...
		    sprintf('-r%g', extras.printresolution), fn);
	      if qdcTries > 1
		localPreliminaryPNG(fn);
	      end
	    else
	      % print function adds .ppm and does not provide anyway not to
	      tmp = [tempname '.ppm']; 
	      print('-dppm', sprintf('-f%g', fh), ...
		    sprintf('-r%g', extras.printresolution), ...
		    tmp);
	      [status message] = pnm2png(tmp, fullfile(Path, pfilename))
	      delete(tmp);
	      disp(['printed to ' fullfile(Path, pfilename)]);
	    end
	  end
	end
	error('Too many failed attempts to load a QDC')
      end
      
      % power and QDC plot
      ipt
      [fh gh] = plotq(ipt, 'beam', defaults.beams, ...
		      'ylim', defaults.powerylim, ...	
		      'qdc', iqdc, ...
		      'legendaxesvisible', 1, ...
		      'visible', defaults.visible, ...
		      varargin{unvi});
      
      set(gh, 'XGrid', defaults.powergrid, ...
	      'YGrid', defaults.powergrid);
      set(fh, 'ResizeFcn', '', ...
	      'Resize', 'off', ...
	      'PaperOrientation', 'portrait', ...
	      'PaperPositionMode', 'auto', ...
	      'Units', 'inch', ...
	      'Position', [0 0 defaults.printsize ./ ...
		    extras.printresolution], ...
	      'Tag', 'powerplot');

      if qdcTries > 1
	% mark as provisional
	localPreliminary(gh);
      end

      if defaults.print
	if 1 
	  fn = fullfile(Path, pfilename);
	  if exist(fn)
	    delete(fn);
	  end
	  print('-dpng', sprintf('-f%g', fh), ...
		sprintf('-r%g', extras.printresolution), fn);
	  if qdcTries > 1
	    localPreliminaryPNG(fn);
	  end
	else
	  tmp = [tempname '.ppm'];
	  print('-dppm', sprintf('-f%g', fh), ...
		sprintf('-r%g', extras.printresolution), ...
		tmp);
	  [status message] = pnm2png(tmp, fullfile(Path, pfilename))
	  delete([tmp '.ppm']);
	  disp(['printed to ' fullfile(Path, pfilename)]);
	end
      end
      if defaults.closefigures
	close(fh);
      end
      fh = [];
      
    end
    % --- end of power plot ---
    
    if defaults.absorption | defaults.keogram | defaults.movie
      ipt
      iqdc
      iabs = rio_abs(ipt, iqdc);
      disp('QQQQQQQQQQQQQQQQ0');
    end
    disp('QQQQQQQQQQQQQQQQ1');
    % --- absorption plot ---
    if defaults.absorption
      [fh gh] = plot(iabs, ...
		     'beams', defaults.beams, ...
		     'spacing', defaults.spacing, ...
		     'graphpos', defaults.graphpos, ...
		     'visible', defaults.visible, ...
		     varargin{unvi});

      % don't let absorption plot go more negative than -1dB, or more
      % positive than 0.
      ylim = get(gh, 'YLim');
      ylim(1) = max(ylim(1), -1);
      ylim(1) = min(ylim(1), 0);
      set(gh, 'YLim', ylim, ...
	      'XGrid', defaults.powergrid, ...
	      'YGrid', defaults.powergrid);
      set(fh, 'ResizeFcn', '', ...
	      'Resize', 'off', ...
	      'PaperOrientation', 'portrait', ...
	      'PaperPositionMode', 'auto', ...
	      'Units', 'inch', ...
	      'Position', [0 0 defaults.printsize./ ...
		    extras.printresolution], ...
	      'Tag', 'absplot');
      if qdcTries > 1
	% mark as provisional
	localPreliminary(gh);
      end
      
      if defaults.print
	fn = fullfile(Path, afilename);
	print('-dpng', sprintf('-f%g', fh), ...
	      sprintf('-r%g', extras.printresolution), fn);
	if qdcTries > 1
	  localPreliminaryPNG(fn);
	end
      end
      if defaults.closefigures
	close(fh);
      end
      fh = [];
    end
    
    % --- keogram ---
    if defaults.keogram
      disp('QQQQQQQQQQQQQQQQ2');
      id = rio_image(iabs, ...
		     'pixelunits', defaults.keogramunits, ...
		     'xpixelpos', defaults.keogramxpixelpos, ...
		     'ypixelpos', defaults.keogramypixelpos, ...
		     'interpmethod', defaults.interpmethod)
      disp('QQQQQQQQQQQQQQQQ3');
      % adjust graphpos to allow for footer
      footerheight = 0.1; % proportion of full window height
      graphpos = defaults.graphpos;
      graphpos(4) = graphpos(4) - footerheight;
      graphpos(2) = graphpos(2) + footerheight;

      % calculate new graphpos and spacing values, since we wish to scale
      % the plot in height. After scaling we aim to have the header the same
      % size, so must make fractionally bigger

      newgraphpos = graphpos;
      newgraphpos(2) = graphpos(2) / extras.keogramfractionalheight;
      newgraphpos(4) = 1 - newgraphpos(2) - ((1-graphpos(2)-graphpos(4))/ ...
					     extras.keogramfractionalheight);
      % adjust the spacing around the graph so that after scaling it is
      % the same (pixel) size
      spacing = defaults.spacing;
      % spacing([2 4]) = spacing([2 4]) ./ (newgraphpos(4) / graphpos(4));
      spacing([2 4]) = spacing([2 4]) ./ extras.keogramfractionalheight;

      disp('QQQQQQQQQQQQQQQQ4');
      [fh gh] = keogram(id, varargin{unvi}, ...
			'seriesstyle', defaults.keogramseriesstyle, ...
			'spacing', spacing, ...
			'graphpos', newgraphpos, ...
			'visible', defaults.visible);
      disp('QQQQQQQQQQQQQQQQ5');      
      if 0 
	% relabel graph to have ticks every 1 hour, but labels only every 4
	hours = 0:(plotduration/timespan(1,'h'));
	hoursLen = length(hours);
	xtick = zeros(1, hoursLen);
	xticklabel = cell(1, hoursLen);
	for n = 1:hoursLen
	  xtick(n) = timespan(hours(n), 'h') / defaults.resolution;
	  if rem(hours(n), 4) == 0
	    xticklabel{n} = sprintf('%02d', hours(n));
	  end
	end
	set(gh, 'XTick', xtick, ...
		'XTickLabel', xticklabel);
      end
      
      % set(gh, 'XGrid', defaults.powergrid, ...
      % 'YGrid', defaults.powergrid, ...
      % 'ResizeFcn', '', ...
      set(fh, 'Resize', 'off', ...
	      'PaperOrientation', 'portrait', ...
	      'PaperPositionMode', 'auto', ...
	      'Units', 'inch', ...
	      'Position', [0 0 extras.keogramprintsize ./ ...
		    extras.printresolution], ...
	      'Tag', 'keogram');

      if qdcTries > 1
	% mark as provisional
	localPreliminary(gh);
      end
      
      if defaults.print
	fn = fullfile(Path, kfilename);
	print('-dpng', sprintf('-f%g', fh), ...
	      sprintf('-r%g', extras.printresolution), fn);
	if qdcTries > 1
	  localPreliminaryPNG(fn);
	end
      end
      if defaults.closefigures
	close(fh);
      end
      fh = [];
      clear id;
    end 
    % --- end of keogram ---
    
    if defaults.movie
      % do animated gif movie
      defaults.movieunits
      defaults.moviexpixelpos
      defaults.movieypixelpos
      iabs
      id = rio_image(iabs, ...
		     'pixelunits', defaults.movieunits, ...
		     'xpixelpos', defaults.moviexpixelpos, ... 
		     'ypixelpos', defaults.movieypixelpos, ...
		     'interpmethod', defaults.interpmethod);
      id
      idd = getdata(id);
      max(idd(:))
      min(idd(:))
      ISNAN=any(isnan(idd(:)))
      % Make a movie. Pass unknown options, they may have details for
      % movie (timebar options, etc).
      movie(id, ...
	    'filename', fullfile(Path, mfilename), ...
	    'scale', defaults.moviescale, ...
	    'timebar', defaults.movietimebar, ...
	    'timebargap', defaults.movietimebargap, ...
	    'timebarheight', defaults.movietimebarheight, ...
	    varargin{unvi}); 
      % save min, max, preliminary_flag information
      movieInfo = [getlimits(id), (qdcTries > 1)];
      qdcTries
      save(fullfile(Path, movieInfoFileName), 'movieInfo', '-ascii');
    end
    % --- end of movie ---

    
    clear iabs;

  catch
    if defaults.closefigures
      close all;
    end
    lerr = lasterr;
    if strcmp(lerr, 'Interrupt')
      error(lerr);
    end
    
    disp(['Could not make plot for ' char(t)]);
    disp(['Error was: ' lerr]);
    disp(' ');
  end

  lastQdcTime = qdcTime;
  t = t + day;
end



function localPreliminary(gh)
gh = gh(:); 
va = {};
if matlabversioncmp('>', '5.1')
  va = {'VerticalAlignment', 'middle'};
end
for g = gh(:)'
  th = text(0.5, 0.5, 'Preliminary', ...
	    'Units', 'normalized', ...
	    'Parent', g, ...
	    'FontUnits', 'pixels', ...
	    'FontSize', 29, ...
	    'FontName', 'helvetica', ...
	    'HorizontalAlignment', 'center', ...
	    va{:}, ...
	    'Color', 'r', ...
	    'FontWeight', 'bold');
  % 'FontUnits', 'normalized', ...
  % 'FontSize', 0.1, ...
end

return

function localPreliminaryPNG(filename)
[im map] = imread(filename);
if isempty(map)
  imwrite(im, filename, 'Warning', 'Preliminary');
else
  imwrite(im, map, filename, 'Warning', 'Preliminary');
end
return


function localCheckSummaryPlots(defaults, extras)
% turn off to avoid recursive calls to localCheckSummaryPlots 
defaults.check = 0; 
day = timespan(1,'d');
t = defaults.starttime;
tmpfile = tempname;

while t < defaults.endtime
  Path = strftime(t, extras.pathformatstr); 
  filename = cell(1, 5);
  imsz = cell(size(filename));
  format = cell(size(filename));
  remake = ones(size(filename));
  
  % power (1)
  filename{1} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileformatstr, 'p')));
  imsz{1} = defaults.printsize;
  format{1} = 'png';

  % absorption (2)
  filename{2} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileformatstr, 'a')));
  imsz{2} = defaults.printsize;
  format{2} = 'png';
  
  % keogram (3)
  filename{3} = fullfile(Path, ...
			 strftime(t, sprintf(extras.fileformatstr, 'k')));
  imsz{3} = extras.keogramprintsize;
  format{3} = 'png';
  
  % movie (4)
  filename{4} = fullfile(Path, ...
			 strftime(t, sprintf(extras.moviefileformatstr, ...
					     'm')));
  imsz{4} = [length(defaults.movieypixelpos) + ...
	     defaults.movietimebargap + defaults.movietimebarheight, ...
	     length(defaults.moviexpixelpos)];
  format{4} = '';
  
  % movie info (5)
  filename{5} = fullfile(Path, ...
			 strftime(t, extras.movieinfoformatstr));
  format{5} = '';
  remake(5) = 0; % made along with movie
  
  % check all standard images
  for n = 1:3
    if ~isempty(format{n}) & exist(filename{n}, 'file')
      try
	inf = imfinfo(filename{n});
	if ~strcmp(lower(inf.Format), format{n})
	  ;
	elseif ~isequal([inf.Width inf.Height], imsz{n})
	  ;
	elseif strcmp(inf.Warning, 'Preliminary') & defaults.remakeprelims
	  ; preliminary
	else
	  remake(n) = 0; % OK
	end
      catch
	if strcmp(lasterr, 'Interrupt')
	  error(lasterr);
	end
      end
    else
      ;
    end
    
  end
  
  % check movies
  if exist(filename{4}, 'file') & exist(filename{5}, 'file')
    % files exist, but are they correct
    try
      [zz zzz] = unix(['convert ''' filename{4} '[1]'' png:' tmpfile]);
      [im map] = imread(tmpfile, 'png');
      limits = load(filename{5});
      if ~isequal(size(im), imsz{4})
	% disp('wrong size');
	;
      elseif 0 & ~isequal(map(double(im(length(defaults.movieypixelpos) + ...
					1,1))+1, :), [1 1 1])
	% background color in blank part was not white
	% disp('wrong background color');
	;
      elseif limits(1) > limits(2)
	; % bad limit information
      elseif limits(3) & defaults.remakeprelims
	; % preliminary image must be remade
      else
	remake(4) = 0;
      end
      delete(tmpfile);
    catch
      disp(['Error was: ' lasterr]);
      delete(tmpfile);
      if strcmp(lasterr, 'Interrupt')
	error(lasterr);
      end
    end
  end

  if any(remake)
    disp(['remaking ', strftime(t, '%Y-%m-%d')]);
    summaryplot('starttime', t, ...
		'power', remake(1), ...
		'absorption', remake(2), ...
		'keogram', remake(3), ...
		'movie', remake(4));
  else
    disp(['checked: ', strftime(t, '%Y-%m-%d')]);
  end
  
  t = t + day;
end




