function temperaturesummaryplot(rio, varargin)
%TEMPERATURESUMMARYPLOT Make a daily summary plot of riometer temperatures.
%
%   TEMPERATURESUMMARYPLOT(rio, ...)
%
% The following name/value pairs are recongised and may be used to modify
% the default behaviour:
%
%   'starttime', TIMESTAMP
%   Select the first day from which plotting should commence (inclusive).
%
%   'endtime', TIMESTAMP
%   Select the end day for plotting (exclusive).
%
%   'printsize', [1 x 2 DOUBLE]
%   Modify the size of the saved images.
%
%   'summaryplotdir', CHAR
%   Specify an alernative location for the summary plot base directory.
%
% See also ARCOMTEMPPLOT, PROCESS_ARCOM_TEMP_DATA.

defaults.starttime = [];
defaults.endtime = [];

defaults.printsize = [576 432];
defaults.filename = '';
defaults.summaryplotdir = mia_summaryplotdir;
defaults.closefigures = true;
defaults.realtimemode = false;
defaults.grid = 'on';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

day = timespan(1, 'd');
if isempty(defaults.starttime)
  defaults.starttime = timestamp('yesterday');
else
  defaults.starttime = floor(defaults.starttime, day);
end
if isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + day;
else
  defaults.endtime = ceil(defaults.endtime, day);
end


if isempty(defaults.summaryplotdir)
  defaults.summaryplotdir = mia_summaryplotdir;
end

if isempty(defaults.filename)
  defaults.filename = fullfile(defaults.summaryplotdir, class(rio), ...
			       sprintf('%s_%d', getabbreviation(rio), ...
				       getserialnumber(rio)), ...
			       '%Y', '%m', 'temp_%Y%m%d.png');
end

t = defaults.starttime;
while t < defaults.endtime
  t2 = t + day;
  try
    mia = temp_data('instrument', rio, ...
		    'starttime', t, ...
		    'endtime', t2);
    
    % mia = setresolution(mia, timespan(2, 'm'), 'nonanmean');
    % [fh gh lh] = plot(mia, 'processing', '');
    [fh gh lh] = plot(mia);

    % Ensure gh is viible (can be invisible if no data)
    set(gh, 'Visible', 'on');
    
    if logical(defaults.realtimemode)
      plottime = strftime(timestamp('now'), 'Plot time: %Y-%m-%d %H:%M:%S UT');
      
      % calculate latest valid data
      if isdataempty(mia)
	lvd = 'No temperature data';
      else
	lvd = strftime(getsampletime(mia, 'end'), ...
		       'Latest temperature: %Y-%m-%d %H:%M:%S UT');
      end
      text('Parent', gh, ...
	   'Units', 'normalized', ...
	   'Position', [0.05 0.95], ...
	   'FontSize', 12, ...
	   'HorizontalAlignment', 'left', ...
	   'VerticalAlignment', 'top', ...
	   'Color', 'k', ...
	   'String', {plottime, lvd});
    elseif isdataempty(mia)
      text('Parent', gh, ...
	   'Units', 'normalized', ...
	   'Position', [0.05 0.95], ...
	   'FontSize', 12, ...
	   'HorizontalAlignment', 'left', ...
	   'VerticalAlignment', 'top', ...
	   'Color', 'k', ...
	   'String', 'No temperature data');
    end
    
    if isdataempty(mia)
      set(gh, 'YTickLabel', {});
    end
    
    fn = strftime(t, defaults.filename);
    delete(findall(fh, 'Type', 'uitoolbar'));
    set(gh, ...
        'XGrid', defaults.grid, ...
        'YGrid', defaults.grid);
    
    set(fh, 'Position', [60 60, defaults.printsize]);
    figure2imagefile(fh, fn, ...
		     'size', defaults.printsize, ...
		     'makedirectory', true);
    if logical(defaults.closefigures)
      close(fh);
    end
    
  catch
    lerr = lasterr;
    if strcmp(lerr, 'Interrupt')
       rethrow(lerr);
    end
    
    disp(['Could not make plot for ' char(t)]);
    disp(['Error was: ' lerr]);
    disp(' ');
    
  end
  
  t = t2;
end



