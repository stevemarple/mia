function make_riometer_events(rio, varargin)

defaults.starttime = [];
defaults.endtime = [];
defaults.filename = '';
defaults.eventtype = '';



% mainly for debugging, where is is possible to pass a block of data to
% be examined for events.
defaults.abs = []; 

% make the block size default to the same duration as the data files
df = info(rio, 'defaultfilename', 'rio_abs');
if isempty(df)
  % try power instead
  df = info(rio, 'defaultfilename', 'rio_power');
end
if isempty(df)
  % still empty, not sure data can be loaded!
  defaults.blocksize = timespan(1, 'd');;
else
  defaults.blocksize = df.duration;
end

% these should not be changed unless you really know what you are doing,
% since they have a major impact on the events database. If changed then it
% is probable that the entire events detection for the dataset should be
% recomputed with the new values

defaults.resolution = timespan(1, 'm');
defaults.beams = []; % defer 
% format string for timestamps
defaults.timeformatstr = '%Y-%m-%d %H:%M:%S+00'; 

% if rio == rio_kil_1
%   defaults.table = 'riometer_kil_1';
% elseif rio = rio_pkr_1
% else
%   error(sprintf('unknown riometer (was %s)', char(rio)));
% end

defaults.table = sprintf('riometer_%s_%d', lower(getabbreviation(rio)), ...
			 getserialnumber(rio));

% defaults.absorption = 0.5; % lower limit for absorption, in dB
% defaults.interference = -0.5; % upper limit for interference, dB

rules.absorption.id = 1;
rules.interference.id = 2;
rules.missing.id = 3;

% other IDs are:
% calibration = 4
% pca = 5

% how to test for the conditions, these are passed to feval, with the
% values under test as the LAST argument. For binary comparison operators
% be prepared to invert the logic you want.
rules.absorption.cmp = {'lt', 0.5}; % =>   >= 0.5
rules.interference.cmp = {'gt', -0.5}; % =>   <= -0.5
rules.missing.cmp = {'isnan'};

% how to find the peak value. If empty matrix then just insert NULLs
rules.absorption.peak = {'max'};
rules.absorption.mean = {'mean'};
rules.interference.peak = {'min'};
rules.interference.mean = {'mean'};
rules.missing.peak = [];
rules.missing.mean = [];


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~isempty(defaults.abs)
  defaults.starttime = getstarttime(defaults.abs);
  defaults.endtime = getendtime(defaults.abs);
elseif isempty(defaults.starttime)
  error('starttime must be specified');
elseif isempty(defaults.starttime)
  error('endtime must be specified');
end

if isempty(defaults.beams)
  [ib wb] = info(rio, 'beams');
  if ~isempty(wb)
    defaults.beams = wb(1);
  elseif ~isempty(ib)
    zen = info(rio, 'zenith');
    minzen = min(zen(:));
    % take beams where zenith <= 100.01% of zenith (have some tolerance
    % for rounding errors)
    defaults.beams = ib(find(zen <= minzen * 1.0001));
  else
    error('no beams to use');
  end
else
  warning('using non-standard beams for events database');
end

if strcmpi(defaults.eventtype, 'pca')
  s = [];
  s.rules.id = 5;
  s.rules.cmp = []; % not used
  s.rules.peak = {'max'};
  s.rules.mean = {'nonanmean'};
  localAddEvent(rio, defaults, s);
  return;
elseif strcmpi(defaults.eventtype, 'calibration')
  s = [];
  s.rules = rules.missing; % same behaviour as missing data
  s.rules.id = 4;
  localAddEvent(rio, defaults, s);
  return;
elseif ~isempty(defaults.eventtype)
  error(sprintf('unknown event type (was %s)', defaults.eventtype));
end

if defaults.starttime ~= floor(defaults.starttime, defaults.blocksize)
  error(sprintf('starttime must be in a boundary of %s', ...
		defaults.blocksize));
elseif defaults.endtime ~= ceil(defaults.endtime, defaults.blocksize)
  error(sprintf('endtime must be in a boundary of %s', ...
		char(defaults.blocksize)));
end
  


if isempty(defaults.filename)
  fid = 1;  % use stdout
else
  [fid mesg] = fopen(defaults.filename, 'w');
  if fid == -1
    error(sprintf('Cannot open %s: %s', defaults.filename, mesg));
  end
end

% remove anything which intersects these times
localRemoveIntersectingEvents(defaults, fid);

fn = fieldnames(rules);
fnn = length(fn);

samples = defaults.blocksize / defaults.resolution;  
initialTimeIndex = 0:(samples-1);

lookingForStart = 1;
% variables for rio_abs_with_qdc()
mia = [];
qdc = []; % no QDC yet

% loop through all times, loading block by block
t = defaults.starttime;
while t < defaults.endtime
  t2 = t + defaults.blocksize;
  
  if isempty(defaults.abs)
    rio
    [mia qdc] = rio_abs_with_qdc('abs', mia, ...
				 'qdc', qdc, ...
				 'instrument', rio, ...
				 'starttime', t, ...
				 'endtime', t2, ...
				 'beams', defaults.beams, ...
				 'resolution', defaults.resolution, ...
				 'load', 1, ...
				 'log', 0);
  else
    disp(sprintf('extracting %s', dateprintf(t, t2)));
    mia = extract(defaults.abs, defaults.beams, t, t2);
  end
  % data = getdata(mia);
  data = mean(getdata(mia), 1);
  
  cmp = zeros(fnn, samples);
  % find the matches
  for n = 1:fnn
    s = getfield(rules, fn{n});
    cmp(n, :) = feval(s.cmp{:}, data);
  end
  ti = initialTimeIndex;
  % now find the events, in order. lookingForStart is 1 when wanting the
  % start of a an event, 0 when wanting the end, and is toggled between
  % the two states
  while 1
    if lookingForStart
      [rows cols] = find(cmp);
    else
      % when looking for the end have to restrict the search to the same
      % event type as the start time
      [rows cols] = find(cmp(rowNum, :) == 0);
    end
    
    if isempty(rows)
      if ~lookingForStart
	% have to remember event data
	event.data = [event.data data];
      end
      break; % did not find what we were looking for
    end
    
    if lookingForStart
      % found start of event, but find which event type gave the first match
      rowNum = rows(1);
      event = []; % clear any old results
      event.st = t + ti(cols(1)) * defaults.resolution; % remember start time
      event.stStr = strftime(event.st, defaults.timeformatstr);
      event.data = data(cols(1)); % copy first sample
      event.rules = getfield(rules, fn{rowNum});
      % trim unwanted samples from start of cmp, data and ti matrices. The
      % sample where the event starts is not needed for comparing (it can't
      % be the end of the event), and we have already stored the first
      % sample.      
      ti = ti([cols(1)+1]:end);
      data = data([cols(1)+1]:end);
      cmp = cmp(:, [cols(1)+1]:end);
    else
      % found end of the event, write out the details
      event.et = t + ti(cols(1)) * defaults.resolution;
      event.etStr = strftime(event.et, defaults.timeformatstr);
      event.data = [event.data data(1:[cols(1)-1])];
      
      event = localProcessEventStruct(defaults, event);
      localInsert(fid, defaults, event);

      % trim unwanted samples from start of cmp, data and ti
      % matrices. Keep the sample where the event ends, it might be be
      % the start of a different type of event
      ti = ti(cols(1):end);
      data = data(cols(1):end);
      cmp = cmp(:, cols(1):end);
      
    end
    lookingForStart = ~lookingForStart;
  end
  
  
  % for each event type need to find start time, end time, peak value/time
  t = t2;
end


if fid ~= 1
  fclose(fid); % don't close stdout
end

% -------------------------------------------
function localAddEvent(rio, defaults, event)

if isempty(defaults.filename)
  fid = 1;  % use stdout
else
  [fid mesg] = fopen(defaults.filename, 'w');
  if fid == -1
    error(sprintf('Cannot open %s: %s', defaults.filename, mesg));
  end
end

if isempty(defaults.abs)
  mia = rio_abs('instrument', rio, ...
		'starttime', t, ...
		'endtime', t2, ...
		'beams', defaults.beams, ...
		'resolution', defaults.resolution, ...
		'load', 1, ...
		'log', 0);
else
  mia = extract(defaults.abs, defaults.beams, defaults.starttime, ...
		defaults.endtime);
end
event.st = defaults.starttime;
event.stStr = strftime(event.st, defaults.timeformatstr);
event.et = defaults.endtime;
event.etStr = strftime(event.et, defaults.timeformatstr);
event.data = getdata(mia);
event = localProcessEventStruct(defaults, event);
localRemoveIntersectingEvents(defaults, fid);
localInsert(fid, defaults, event);

if fid ~= 1
  fclose(fid); % don't close stdout
end

disp(['beams: ' printseries(defaults.beams)]);



% TO DO
% Have some SQL which removes all events which are entirely within the
% time range, use this to remove events before starting.

% OR, only insert events if they do not intersect with a PCA or
% calibration event. Maybe have this set up as a function, called by a
% trigger on INSERT and ALTER

% Have an option to insert PCA and calibration events, taking the times
% passed. Do not do event detection. Have some SQL which
% removes all events which intersect with the time range. Use this when
% inserting such events.


% -------------------------------------------
function event = localProcessEventStruct(defaults, event)

if isempty(event.rules.peak)
  event.peakStr = 'NULL';
  event.peaktimeStr = 'NULL';
else
  [ep ept] = feval(event.rules.peak{:}, event.data);
  event.peakStr = sprintf('%g', ep);
  event.peaktimeStr = sprintf('''%s''', ...
			     strftime(event.st + (ept -1) * ...
				      defaults.resolution, ...
				      defaults.timeformatstr));
end
if isempty(event.rules.mean)
  event.meanStr = 'NULL';
else
  event.meanStr = sprintf('%g', feval(event.rules.mean{:}, event.data));
end


% -------------------------------------------
function r = localInsert(fid, defaults, event)
r = fprintf(fid, ['INSERT into %s (nameid, time, endtime, abspeak, ' ...
		  'absavg, peaktime) ', ...
		  'VALUES (%d, ''%s'', ''%s'', %s, %s, %s);\n'], ...
	    defaults.table, ...
	    event.rules.id, ...
	    event.stStr, ...
	    event.etStr, ...
	    event.peakStr, ...
	    event.meanStr, ...
	    event.peaktimeStr);

% -------------------------------------------
function r = localRemoveIntersectingEvents(defaults, fid)
fmt = '''%Y-%m-%d %H:%M:%S+00''';
stStr = strftime(defaults.starttime, fmt);
etStr = strftime(defaults.endtime, fmt);
r = fprintf(fid, ...
	    ['DELETE FROM %s WHERE (time >= %s and time < %s) or ' ...
	     '(endtime > %s and endtime <= %s);\n'], ...
	    defaults.table, stStr, etStr, stStr, etStr);
