function r = process_pkr_data_file(filename)

instrument = rio_pokerflat;
beams = 1:256;

r = [];
if isempty(filename)
  error('filename not specified');
end

d = dir(filename);
if isempty(d)
  return;
elseif length(d) > 1
  error('Not a file'); % directory listing or filename includes a wildcard?
elseif d.isdir
  error('Directory, not a file');
end

len = d.bytes;

[fid, message] = fopen(filename, 'r', 'ieee-be');
if fid == -1
  error(sprintf('Could not open ''%s'': %s', filename, message));
end
disp(sprintf('reading %s', filename));


r = [];
mia = [];

resolution = timespan(1, 'm');

header = localReadHeader(fid);

instrument = setfrequency(instrument, header.freq * 1e6);
starttime = [];
endtime = [];
data = repmat(nan, [256 header.samples]);

samples = 0;
while ftell(fid) < len
  rec = localReadPkrRecord(fid);
  if isempty(rec)
    break;
  end
  samples = samples + 1;
  
  % the data isn't perfectly aligned to 1m boundaries, but that seems
  % normal, so accept some deviation
  t = timestamp([header.date rec.time]);
  t1 = round(t, resolution);
  
  if abs(t-t1) > resolution * 0.1
    warning(sprintf('data file %s contains inaccurate times (was ''%s'')', ...
		    filename, char(t)));
  end
  if isempty(starttime)
    starttime = t1;
  end
  
  idx = 1 + (t1 - starttime) / resolution;
  data(:, idx) = rec.data(:);
   
end

fclose(fid);

if samples ~= header.samples
  warning(sprintf(['number of samples (%) does not match value stated' ...
		   'in header (%d)'], samples, header.samples));
end

endtime = starttime + samples * resolution;

r = rio_abs('starttime', starttime, ...
	    'endtime' , endtime, ...
	    'resolution', resolution, ...
	    'integrationtime', resolution, ...
	    'instrument', instrument, ...
	    'units', 'dB', ...
	    'data', data(:, 1:samples), ...
	    'beams', beams, ...
	    'load', 0);

% ------------------------------
function r = localReadHeader(fid)
r.blknum = fread(fid, 1, 'int32');
% combine date information, instead of using standard names
r.date = fread(fid, [1 3], 'float32');
if r.date(1) > 100
  ; % 4 digit year (hopeful!)
elseif r.date(1) < 85
  r.date(1) = r.date(1) + 2000;
else
  r.date(1) = r.date(1) + 1900;
end
r.freq = fread(fid, 1, 'float32'); % MHz
r.bandwidth = fread(fid, 1, 'int16'); % kHz?
r.timeconst = fread(fid, 1, 'int16');
r.calf = fread(fid, 1, 'int16');
r.agcf = fread(fid, 1, 'int16');
r.bsr = fread(fid, 1, 'int16');
r.t = fread(fid, 4, 'float32');

% need to read 4 more bytes, this appears to be number of samples. It
% could be 2 or 4 bytes, but since number of seconds in a day is too big
% to store in a 2-byte int guess it uses 4.
r.samples = fread(fid, 1, 'int32');
if feof(fid)
  r = [];
end
return;

% ------------------------------
function r = localReadPkrRecord(fid)
r.ifr = fread(fid, 1, 'int32');
r.tLST = fread(fid, 1, 'float32');
tGPS = fread(fid, 1, 'float32');
r.time = [0 0 0];
r.time(1) = fix(tGPS);
r.time(2) = fix((tGPS-r.time(1)) * 60);
r.time(3) = fix(((tGPS-r.time(1)) * 60 -r.time(2)) * 60);
% r.time = timespan(h, 'h', m, 'm', s, 's');
r.data = fread(fid, [16, 16], 'float32');
r.data(r.data == 999) = nan;

if feof(fid)
  r = [];
end
return;




