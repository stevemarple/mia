function r = read_iris_record(fid, delScan0)

[headerBytes count] = fread(fid, 40, 'uint8');
headerBytes = headerBytes;

year = [10 1] * headerBytes(1:2);
if year > 89
  year = year + 1900;
else
  year = year + 2000;
end

doy = [100 10 1] * headerBytes(3:5);
hour = [10 1] * headerBytes(6:7);
minute = [10 1] * headerBytes(8:9);
second = [10 1] * headerBytes(10:11);
% ignore milliseconds (can it really be milliseconds? (not enough range)

rxStep = headerBytes(17:24);
if ~isequal(rxStep, [48;49;50;51;52;53;54;55]) % '01234567'
  error('receiver step not in normal order');
end

stationId = headerBytes(25);
stepsPerSecond = headerBytes(26);

spareRX = headerBytes(27);
if spareRX ~= 64
  spareRX
  error('Spare receiver in use');
end

noiseGen = headerBytes(28);
if noiseGen
  error('Noise generator switched on');
end

adcRes = headerBytes(34);
if adcRes ~= 12
  error('not 12 bit recording');
end

tmp = headerBytes(35);

if (tmp == adcRes)
  % probably an error in recording software
  step0Rec = 0;
  logicalRecordSize = headerBytes(37);
else
  step0Rec = headerBytes(35);
  logicalRecordSize = headerBytes(36);
end

if step0Rec
  rows = 8;
else
  rows = 7;
end


[progVersion count] = fread(fid, 24, 'char');

r.starttime = timestamp(year, doy, hour, minute, second);
r.endtime = r.starttime + timespan(4032/logicalRecordSize, 's');
r.resolution = timespan(1, 's');

[r.data count] = fread(fid, [8*rows, 4032/logicalRecordSize], 'ubit12');

if step0Rec & delScan0
  % delete scan 0
  r.data = r.data(2:end, :);
end
return

[fh gh] = makeplotfig('init','miniplots',[8 8], ...
		      'title', char(r.starttime), ...
		      'pointer', 'arrow');
for n = 1:64
  axes(gh(n))
  plot(r.data(n,:))
  set(gh, 'YLim', [0 4096]);
end
set(gh,'YLim',[0, 4096])


