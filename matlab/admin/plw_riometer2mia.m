function mia = plw_riometer2mia(instrument, varargin)

defaults.save = 0;
defaults.recursive = 0;
defaults.ignoreerrors = 0;
defaults.filename = '';
defaults.starttime = [];
defaults.endtime = [];
% process only matching extensions, (case insensitive), can be a CELL
% matrix. If empty all files are processed
defaults.extension = 'plw'; 
[defaults unvi] = interceptprop(varargin, defaults);


if isempty(defaults.starttime) & isempty(defaults.starttime) 
  ;
else
  if ~isempty(defaults.starttime) & ~isempty(defaults.starttime)
    if ~isempty(defaults.filename)
      fstr = defaults.filename;
    else
      % fstr = fileparts(info(instrument, 'defaultfilename', ...
      %    'original_format'));  
      s = info(instrument, 'defaultfilename', 'original_format');
      fstr = fileparts(s.fstr);
      
    end
    t = defaults.starttime;
    while t < defaults.endtime
      fn = strftime(t, fstr);
      % do recursive, unless user specified otherwise
      feval(mfilename, instrument, ...
	    'recursive', 1, ...
	    varargin{:}, ...
	    'filename', fn, ...
	    'starttime', [], ...
	    'endtime', []);
      t = t + timespan(1, 'd');
    end
    return
  else
    error('both starttime and endtime must be defined');
  end
end

[Path file ext] = fileparts(defaults.filename);
if isempty(Path) | ~strcmp(Path(1), filesep)
  Path = fullfile(pwd, Path);
end

if isdir(defaults.filename)
  disp(sprintf('Doing directory %s', defaults.filename))
  pause(1);
  
  mia = [];
  Path = fullfile(Path, [file ext]);
  d = dir(defaults.filename);
  cmd = sprintf('%s(instrument, varargin{:}, ''filename'', fn);', mfilename);
  for n = 1:length(d)
    fn = fullfile(Path, d(n).name);
    err = 0;
    if ~d(n).isdir
      [tmp1 tmp2 ext] = fileparts(d(n).name);
      if length(ext) > 1 & ext(1) == '.'
	ext = ext(2:end);
      end
      if isempty(defaults.extension) | any(strcmpi(ext, defaults.extension))
	% if defaults.extension set then process only extensions which
        % match (case insensitive)
	eval(cmd, 'err = 1;');
      end
    elseif logical(defaults.recursive) & ...
	  (~strcmp(d(n).name, '.') & ~strcmp(d(n).name, '..'))
      % don't do current or parent directories
      % feval(mfilename, fullfile(Path, d(n).name), varargin{:});
      % fn = fullfile(Path, d(n).name);
      eval(cmd, 'err = 1;');
    end
    if err 
      disp(sprintf('error processing %s: %s', fn, lasterr));
      if ~defaults.ignoreerrors | strcmp(lasterr, 'Interrupt')
	error(lasterr);
      end
    end

  end
  return;
end

disp(sprintf('processing %s', defaults.filename));


[data ts h settings] = plw2mat(defaults.filename);

% p.starttime = timestamp([1 1 1 0 0 0]) + ...
%     timespan(h.start_date, 'd', h.start_time, 's');
p.starttime = h.start_timestamp;

p.resolution = timespan(h.interval, h.interval_units_str);
p.endtime = p.starttime + h.max_samples * p.resolution;
p.instrument = instrument;
p.data = repmat(nan, 1, (p.endtime - p.starttime)/p.resolution);
p.data(ts+1) = data;
[ib wb] = info(p.instrument, 'beams');
p.beams = sort([ib wb]);

% map any zeros to nan
p.data(p.data == 0) = nan;


if 0
  cls = 'rio_power';
  p.units = 'dBm';
else
  % raw power
  cls = 'rio_rawpower';
  p.units = 'V';
end

pc = makeprop(p);
newdata = feval(cls, pc{:}, 'units', 'V', 'load', 0);

% load up any old data (if present)


if logical(defaults.save)
  p2 = rmfield(p, {'data', 'units', 'starttime', 'endtime'});
  p2c = makeprop(p2);
  % [tmp filedur] = info(p.instrument, 'defaultfilename', cls);
  s = info(p.instrument, 'defaultfilename', cls);
  olddata = feval(cls, p2c{:}, ...
		  'starttime', floor(p.starttime, s.duration), ...
		  'endtime', ceil(p.endtime, s.duration), ...
		  'units', p.units, ...
		  'load', 1, ...
		  'log', 0);

  mia = insert(olddata, newdata);
  
  % check if data finishes at the correct boundary (may not when
  % inserting new data)
  et = getendtime(mia);
  if rem(et, s.duration) ~= timespan(0, 's')
    % need to pad to end
    et2 = ceil(et, s.duration);
    mia = insert(feval(cls, ...
		       'starttime', et, ...
		       'endtime', et2, ...
		       'resolution', p.resolution, ...
		       'instrument', p.instrument, ...
		       'beams', p.beams, ...
		       'data', repmat(nan, length(p.beams), ...
				      (et2-et)/p.resolution), ...
		       'units', getunits(mia)), ...
		 mia);
  end
  save(mia);
else
  mia = newdata;
end
