function [fh, gh] = aurorawatch2(varargin)
%AURORAWATCH Generate AuroraWatch plot

defaults.instrument = mag_lan_1;
defaults.starttime = []; % defer until activityresolution is known
defaults.endtime = [];
defaults.starttime = [];
defaults.amberlimit = +50e-9; % Tesla
defaults.redlimit = +100e-9; % Tesla

defaults.activityresolution = timespan(5, 's');
defaults.plotresolution = timespan(1, 'm');
defaults.components = 'H';
defaults.archive = {'5s', '1s', 'preliminary', 'realtime'};
defaults.plotlimits = [3000e-9 3800e-9];

% the actual heights of the activity bars does not have to be the same
% scale as the plotted data
defaults.activityscalefactor = 2;

% width of the activity bars
defaults.activitybarresolution = timespan(15, 'm');

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.starttime)
  defaults.endtime = floor(timestamp('now'), defaults.activitybarresolution);
  defaults.starttime = defaults.endtime - timespan(1, 'd');
else
  defaults.starttime = ceil(defaults.starttime, ...
			    defaults.activitybarresolution);
end

if isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + timespan(1, 'd');
end
defaults.endtime = floor(defaults.endtime, ...
			 defaults.activitybarresolution);


if isempty(defaults.plotresolution)
  defaults.plotresolution = defaults.activityresolution;
end

if isempty(defaults.components)
  defaults.components = info(defaults.instrument, 'components');
end
if iscell(defaults.components)
  defaults.components = {defaults.components{1}};
end

% hack to work around matlab 5.1 bug

% Assertion failed: ((idx >= 0) && (idx < BASE_TABLE_SIZE)), at line 644 of file "mwmem.c".
% Cache internal consistency error 4 (Table index out of range)
% Error in ==> /home/mango/marple/work/mia/magnetometer/samnet2mia.m

comp = defaults.components;
% defaults.components = {};


md = mag_data('instrument', defaults.instrument, ...
	      'starttime', defaults.starttime, ...
	      'endtime', defaults.endtime, ...
	      'resolution', defaults.activityresolution, ...
	      'components', defaults.components, ...
	      'log', 0, ...
	      'load', 1, ...
	      'loadoptions', ...
	      {'archive', defaults.archive});

disp('try loading up to N QDCs');

if 0
  qdc = mag_qdc_fft('instrument', defaults.instrument, ...
		    'starttime', defaults.starttime, ...
		    'endtime', defaults.endtime, ...
		    'resolution', defaults.activityresolution, ...
		    'components', defaults.components, ...
		    'log', 0, ...
		    'load', 1);
  disp('fix mag_qdc_fft components');
  qdc = extract(qdc, 'components', defaults.components);
else
  qt = timestamp([datevec(defaults.starttime, 1:2) 1 0 0 0]);
  
  qdc = mag_qdc('instrument', defaults.instrument, ...
		'time', qt, ...
		'starttime', defaults.starttime, ...
		'endtime', defaults.endtime, ...
		'resolution', defaults.activityresolution, ...
		'components', defaults.components, ...
		'log', 0, ...
		'load', 1)
end
  
[fh gh ph prelimH mult] = plot(setresolution(qdc, defaults.plotresolution), ...
			       'components', comp, ...
			       'color', 'r');

plot(setresolution(md, defaults.plotresolution), ...
     'components', comp, ...
     'plotaxes', gh);

ylim = defaults.plotlimits .* 10^(-mult);
set(gh, ...
    'YLim', ylim, ...
    'Layer', 'top');

act = getdata(md) - getdata(qdc);

numActBars = getduration(md) ./ defaults.activitybarresolution;
actBarSamples = defaults.activitybarresolution ./ defaults.activityresolution;

act = reshape(act, actBarSamples, numActBars);
% find the most active times. actPos is the row number for each column,
% not the linear index into the original array!
[actAbs actPos] = max(abs(act), [], 1);

% Want to subscript the activity with row and column indices due to the
% beviour of max (desrcibed above). Cannot use n-element row and column
% indices since this results in a n*n matrix. Have to get equivalent
% linear element from sub2ind.
actSigned = act(sub2ind(size(act), actPos, 1:size(act,2)));

printunits(actSigned, 'T', 'multiplier', -9)
printunits(actAbs, 'T', 'multiplier', -9)
whos
width = defaults.activitybarresolution ./ defaults.plotresolution;
for n = 1:length(actSigned)
  if actAbs(n) > defaults.redlimit
    col = 'r';
  elseif actAbs(n) > defaults.amberlimit
    col = [.9961 .6445 0];
  else
    col = 'g';
  end
  
  rectangle('Parent', gh, ...
	    'Position', [(n-1)*width ...
		    ylim(1) ...
		    width ...
		    defaults.activityscalefactor * actAbs(n) ./ 10^mult], ...
	    'FaceColor', col, ...
	    'EdgeColor', col);
end

disp('remember to write out the aurorawatch activity values and status')

% write to temp file and then use a mv
