function calcriometerdetails2(rio, varargin)
%CALCRIOMETERDETAILS  Calculate basic riometer details
%
%   CALCRIOMETERDETAILS(rio);
%
% Calculate basic riometer details and output the results to the terminal
% window.
%
% See also RIOMETER.

defaults.beams = [];
defaults.taperid = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.taperid) & rio == rio_ram_2
  defaults.taperid = 'B200';
  disp(sprintf('defaulting to taperid = %s', defaults.taperid));
end

if isempty(info(rio, 'antennaphasing'))
  % cannot do much without phasing information
  error('require antenna phasing to be set');
end

pi_2 = pi/2;
pi2 = pi * 2;
d2r = pi / 180; % factor to multiply by to convert from degrees to radians
r2d = 180 / pi; % factor to multiply by to convert from radians to degrees


theta1 = 1e-6:(pi_2/100):pi_2;
phi1 = [1e-6:(pi2/400):pi2]';

theta1 = 1e-6:(pi_2/900):pi_2;
phi1 = [1e-6:(pi2/3600):pi2]';

[theta phi] = meshgrid(theta1, phi1);

[imagingBeams wideBeams] = info(rio, 'beams');

if ~isempty(defaults.beams)
  imagingBeams = intersect(imagingBeams, defaults.beams);
  wideBeams = intersect(wideBeams, defaults.beams);
end

imagingBeamsPlan = info(rio, 'beamplan');

zenith = repmat(nan, size(imagingBeamsPlan));
azimuth = repmat(nan, size(imagingBeamsPlan));
beamwidth = zeros(1, numel(imagingBeams));

% imagingBeams = imagingBeams(1:4);



disp('');
for n = 1:numel(imagingBeams)
  % fprintf('\rbeam %d   ', imagingBeams(n));
  disp(sprintf('beam %d', imagingBeams(n)));
  
  switch lower(info(rio, 'systemtype'))
   case 'aries'
    d0 = sqrt(abs(ariesgain(rio, theta, phi, imagingBeams(n), ...
			       'taperid', defaults.taperid)));
   
   case 'iris'
    d0 = abs(irisdirectivity(rio, theta, phi, imagingBeams(n)));
  
   case 'widebeam'
    error(['cannot use ' mfilename ' on widebeam system']);
    
   otherwise
    error('unknown system type');
  end
 
  
  [tmp idx] = max(d0(:));
  zen = theta(idx);
  az = phi(idx);
  
  % find where this imaging beam is in the beam plan
  idx = find(imagingBeamsPlan == imagingBeams(n));
  if isempty(idx)
    error(sprintf('cannot find beam #%d in beam plan', imagingBeams(n)));
  elseif length(idx) > 1
    error(sprintf('beam #%d occurs %d times in beam plan', ...
		  imagingBeams(n), length(idx)));
  end
  
  zenith(idx) = zen;
  azimuth(idx) = az;
  
  % calculate average beamwidth (imaging beams only!)
  gain = power(d0, 2);
  [maxGain maxGainPos] = max(gain(:));
  gainDB = 10 * log10(gain ./ maxGain);  % dB
  maxGainTheta = theta(maxGainPos);
  maxGainPhi = phi(maxGainPos);

  % convert to lat/long type coords
  maxGainLong = maxGainPhi;
  maxGainLat = (pi/2) - maxGainTheta;

  c = contourc(theta1, phi1, gainDB, [-3 -3]);
  cc = contour2cell(c);
  
  % With side lobes there are multiple contours. Consider only the main
  % lobe. Measure the average absolute angle between each between on the
  % contour and the direction of the maximum gain. Do this for all contours,
  % but take the smallest average (that being the contour containing the
  % pointing direction of the main lobe).
  tmp = zeros(size(cc));
  for m = 1:numel(cc)
    tmp(m) = mean(abs(sphere_dist(maxGainLong, maxGainLat, ...
				  cc{m}.y, (pi/2)-cc{m}.x)));
  end
  % beamwidth(n) = min(tmp);
  % beamwidth = 2 * beamwidth * r2d;
  beamwidth(n) = min(tmp) * 2 * r2d; % beamwidth in degrees
  % whos
end
  
% convert to degrees
zenith = zenith * r2d;

disp('zenith = [ ...');
for r = 1:size(imagingBeamsPlan, 1)
  s = '    ';
  block = 8;
  for c1 = 0:((ceil(size(imagingBeamsPlan, 2))/block)-1)
    stmp = sprintf('%+06.2f ', zenith(r, (block*c1)+[1:block]));
    s = sprintf('%s%s ...\n', s, stmp);
  end
  fprintf(1, s);
end
disp('    ];');


% convert azimuth to compass angles in degrees
azimuth = unwrap(pi_2-azimuth)* r2d;


disp('azimuth = [ ...');
for r = 1:size(imagingBeamsPlan, 1)
  s = '    ';
  block = 8;
  for c1 = 0:((ceil(size(imagingBeamsPlan, 2))/block)-1)
    stmp = sprintf('%+07.2f ', azimuth(r, (block*c1)+[1:block]));
    s = sprintf('%s%s ...\n', s, stmp);
  end
  fprintf(1, s);
end
disp('    ];');


idx = find(zenith > 60);
badBeams = sort(imagingBeamsPlan(idx));

disp(sprintf('bad_beams = [%s];', printseries(badBeams)));

azmax = max(azimuth(:));
azmin = min(azimuth(:));
zenmax = max(zenith(:));
zenmin = min(zenith(:));




disp(['beamwidth = [' ...
      sprintf('%.2f', beamwidth(1)) ...
      sprintf(',%.2f', beamwidth(2:end)) ...
      '];']);
disp('');
disp(['UPDATE riometers set beamwidth=''{' ...
      sprintf('%.2f', beamwidth(1)) ...
      sprintf(',%.2f', beamwidth(2:end)) ...
      '}''' ...
      sprintf(' WHERE abbreviation=''%s'' and serialnumber=%d;', ...
	      getabbreviation(rio), getserialnumber(rio))]);

[tmp idx] = sort(imagingBeamsPlan(:));
% az2 = azimuth(idx(:)) - 90;
az2 = azimuth(idx(:)) - 90;
az2 = azimuth(idx(:));
idx = (az2 < 0);
az2(idx) = az2(idx) + 360;

disp(['UPDATE riometers set azimuth=''{' ...
      sprintf('%.2f', az2(1)) ...
      sprintf(',%.2f', az2(2:end)) ...
      '}''' ...
      sprintf(' WHERE abbreviation=''%s'' and serialnumber=%d;', ...
	      getabbreviation(rio), getserialnumber(rio))]);
disp(['UPDATE riometers set zenith=''{' ...
      sprintf('%.2f', zenith(1)) ...
      sprintf(',%.2f', zenith(2:end)) ...
      '}''' ...
      sprintf(' WHERE abbreviation=''%s'' and serialnumber=%d;', ...
	      getabbreviation(rio), getserialnumber(rio))]);
