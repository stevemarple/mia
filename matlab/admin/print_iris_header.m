function print_iris_header(headers, varargin)

if nargin > 1
  fid = varargin{1};
else
  fid = 1;
end

  
for n = 1:numel(headers)
  h = headers(n);
  
  % fprintf(fid, '-----------------\n');
  % fprintf(fid, ' #%d\n', n);
  fprintf(fid, '----------------- #%d\n', n);
  fprintf(fid, ' Start time    : %s\n', ...
	  strftime(h.starttime, '%Y-%m-%d %H:%M:%S'));
  fprintf(fid, '   End time    : %s\n', ...
	  strftime(h.endtime, '%Y-%m-%d %H:%M:%S'));
  fprintf(fid, 'ADC resolution : %d\n', h.adcresolution);
  fprintf(fid, 'GPS quality    : %d\n', h.gpsquality);
  
  errs = '';
  for n = 1:4
    if ~isempty(h.system_error_message{n})
      if isempty(errs)
	errs = h.system_error_message{n};
      else
	errs = sprintf('%s              %s', ...
		       errs, h.system_error_message{n});
      end
    end
  end
  if isempty(errs)
    errs = '<none>';
  end
  fprintf(fid, '      Error(s) : %s\n', errs);
end
