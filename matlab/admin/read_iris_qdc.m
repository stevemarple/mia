function r = read_iris_qdc(filename, instrument, st)

r = [];


if isempty(filename)
  error('filename not specified');
end

d = dir(filename);
if isempty(d)
  return;
elseif length(d) > 1
  error('Not a file'); % directory listing or filename includes a wildcard?
elseif d.isdir
  error('Directory, not a file');
end

len = d.bytes;

[fid, message] = fopen(filename, 'r', 'ieee-le');
if fid == -1
  error(sprintf('Could not open ''%s'': %s', filename, message));
end

% read header
% headerSize = 42;
% [header count] = fread(fid, headerSize, 'uint8');

headerSize = 21;
[header count] = fread(fid, headerSize, 'uint16');

% header values:
% 1 start year (2 digit!) 
% 2 start month           
% 3 start day
% 4 number of day used to make QDC
% 5 station code (DAVIS = 11)
% 6 
% 7 
% 8 number of samples in time dimension
% 9 
% 10 number of beams
% 11 
% 12 
% 13 
% 14 
% 15 
% 16 
% 17 
% 18 
% 19 
% 20 
% 21 

start_year = fread(fid, 1, 'int16');
if start_year < 80
  start_year = start_year + 2000;
elseif start_year < 100
  start_year = start_year + 1900;
else
  % assume 4 digit year
end

start_month = fread(fid, 1, 'int16'); % 1 - 12
start_day = fread(fid, 1, 'int16'); % 1 - 31
days = fread(fid, 1, 'int16'); % number of days used to create QDC

% station code:
% 11 = rio_dvs_2;
station_code =  fread(fid, 1, 'int16'); 

if count ~= headerSize
  fclose(fid);
  error('could not read header');
end

% dataSize = [49, 1440];
dataSize = header([10 8])';
[data count] = fread(fid, dataSize, 'uint16');
if count ~= prod(dataSize)
  fclose(fid);
  error('could not read data section');
end
fclose(fid);

[ibeams wbeams] = info(instrument, 'beams');
beams = sort([ibeams wbeams]);

% have to guess at start/end times
if header(1) < 80
  header(1) = header(1) + 2000;
elseif header(1) < 100
  header(1) = header(1) + 1900;
else
  % assume 4 digit year
end

qst = timestamp([header([1 2 3])' 0 0 0])
qet = qst + timespan(header(4), 'd');
res = timespan(1/header(8), 'd');
bestres = info(instrument, 'bestresolution');

if res > bestres
  % change resolution
  p = rio_rawpower('instrument', instrument, ...
		   'starttime', st, ...
		   'endtime', st+res*size(data,2), ...
		   'resolution', res, ...
		   'beams', beams, ...
		   'load', 0, ...
		   'log', 0, ...
		   'data', data, ...
		   'units', 'ADC')
  
  data2 = getdata(setresolution(p, bestres));
else
  data2 = data;
end


% data seems to be a factor of 4 too big
data2 = data2 / 4;

% need to change data around
stoff = round(stoffset(st, getlocation(instrument)) / bestres);
qdclen = ceil(siderealday/bestres)
if stoff == 0
  % no wrap around
  data3 = data2(:, 1:qdclen);
else
  mni = qdclen-stoff; % sidereal midnight index
  data3 = data2(:, [mni+1:qdclen 1:mni]);
  qdclen
  size(data3)
end

r = rio_rawqdc('instrument', instrument, ...
	       'starttime', qst, ...
	       'endtime', qet, ...
	       'resolution', bestres, ...
	       'beams', beams, ...
	       'load', 0, ...
	       'log', 0, ...
	       'data', data3, ...
	       'units', 'ADC');


