function r = getarchive(mia)
%GETARCHIVE  Indicate which data archive SAMNET MAGNETOMETER data loaded from
%
%   r = GETARCHIVE(mia)
%   r: CHAR (or CELL of CHARS if mia non-scalar)
%   mia: MAG_DATA
%
% This function is not intended for general use, but is specific to data
% from the SAMNET magnetometer data.
%
%   See also MAG_SAMNET.

if length(mia) ~= 1
  [tmp mfname] = fileparts(mfilename);
  r = cell(size(mia));
  for n = 1:prod(size(mia))
    r{n} = feval(mfname, mia(n));
  end
  return;
end

% mia is always scalar from this point on
proc = getprocessing(mia);

% assume the archive string is always in the first processing line. This
% may have to be improved upon at a later data if things change
if isempty(proc)
  % no processing, so probably means no data
  r = '';
  return
elseif iscell(proc)
  proc_with_archive = '';
  for n = 1:numel(proc)
    if ~isempty(findstr(proc{n}, 'archive'))
      proc_with_archive = proc{n};
      break;
    end
  end
  if isempty(proc_with_archive)
    error('cannot find the archive processing string');
  end
  proc = proc_with_archive;
end

% check the string does contain archive
if isempty(findstr(proc, 'archive'))
  error('cannot find the archive processing string');
end

arch = {'1s' '5s' 'preliminary' 'scratch' 'realtime' 'incoming'};
r = {};
for n = 1:length(arch)
  if ~isempty(findstr(proc, arch{n}))
    r{end+1} = arch{n};
  end
end

if isempty(r)
  error(sprintf('unknown archive (archive string was ''%s'')', proc));
elseif length(r) == 1
  r = r{1};
end

