function r = read_maryland_file(mia, filename)

tmpfile = tempname;
unix(sprintf('awk ''(op==1);($1 ~ /Hours/){op=1}'' %s > %s', ...
	     filename, tmpfile));

[tmp varname] = fileparts(tmpfile);
if varname(1) >= '0' & varname(1) <= '9'
  varname = ['X' varname];
end

eval(sprintf('load %s; data = %s;clear %s;', tmpfile, varname, varname));
delete(tmpfile);

t = data(:, 1);

data2 = data(:, 2:end);
instrument = getinstrument(mia);

mapping = fliplr(info(instrument, 'beamplan'));

% now take mapping and read across rows (not columns) first
mapping = mapping';
mapping = mapping(:);

% mapping refers to column number in data2
% Need to access columns to give ordered data,
[beams idx] = sort(mapping);

data3 = data2(:, idx)';

r = mia;
r = setdata(r, data3);
r = setunits(r, 'dB');
r = setbeams(r, beams);



