function [r, headers, validrecord] = process_iris_data_file(filename, rio, varargin)

%     'checklogfile', CHAR
%     Logfile where details of any errors are written.



% the actual headers and valid record flags are only created if nargout > 1
r = [];

defaults.validstarttime = [];
defaults.checklogfile = '';


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.validstarttime)
  defaults.validstarttime = [timestamp([1990 1 1 0 0 0]) timestamp('now')];
end

if ~isempty(defaults.checklogfile)
  [logfid mesg] = fopen(defaults.checklogfile, 'a');
  if logfid == -1
    error(sprintf('cannot open %s: %s', defaults.checklogfile, mesg));
  end
else
  logfid = 1;
end



if isempty(filename)
  error('filename not specified');
end

d = dir(filename);
if isempty(d)
  return;
elseif length(d) > 1
  error('Not a file'); % directory listing or filename includes a wildcard?
elseif d.isdir
  error('Directory, not a file');
end

len = d.bytes;

[fid, message] = fopen(filename, 'r', 'ieee-be');
if fid == -1
  error(sprintf('Could not open ''%s'': %s', filename, message));
end

r = [];



% get list of beams available
[ibeam wbeam] = info(rio, 'beams');
beams = union(ibeam, wbeam);

switch numel(ibeam)
 case 49
  % ignore every 8th element (spare riometer)
  map_exc_widebeam = { ...
      1  2  3  4  5  6  7 ...
      9  10 11 12 13 14 15  ...
      17 18 19 20 21 22 23 ...
      25 26 27 28 29 30 31 ...
      33 34 35 36 37 38 39 ...
      41 42 43 44 45 46 47 ...
      49 50 51 52 53 54 55};

  % 1:7 9:15 17:23 25:31 33:39 41:47 49:55};
  map_inc_widebeam = map_exc_widebeam;
  map_inc_widebeam{50} = 8:8:56; % use all rows to reduce noise
  
 otherwise
  warning(sprintf('not sure how to deal with riometers with %d beams', ...
		  numel(ibeam)));
  
end

switch numel(wbeam)
 case 0
  map = map_exc_widebeam;
  
 case 1
  map = map_inc_widebeam;
  
 otherwise
  error('can only handle riometers with 0 or 1 widebeams');
end


num_records = 0;

latest = timestamp('bad');

while ftell(fid) < len
  [rec mesg] = read_iris_record(fid, 1); % delete scan zero (if present)
  if ~strcmp(mesg, '')
    local_warning(logfid, filename, [], num_records, ...
                  sprintf('aborting read: %s', mesg));
    break;
  end
  
  num_records = num_records + 1;
  % ------------
  %   try
  %     fprintf('-------------\nfile pos: %d\n', ftell(fid));
  %     rec = read_iris_record(fid, 1); % delete scan zero (if present)
  %     errs = 0;
  %   catch
  %     errs = errs + 1;
  %     if strcmp(lasterr, 'Interrupt') | errs > 4
  %       error(lasterr);
  %     end
  %     fprintf('last error: %s\n', lasterr);
  %     continue
  %   end
  
  % Useful debugging option:
  fprintf('%s: %d\n', strftime(rec.starttime, '%Y-%m-%d %H:%M:%S'), ...
  	  rec.gpsquality);
  % ------------
  if rec.adcresolution ~= 12
    fprintf('blanking %d-bit data (header starts: %d)\n', ...
	    rec.adcresolution, rec.offset);
    rec.data(:) = 0;
  end
  
  if num_records == 1
    % Guess how much data is probably going to be in the file. Then
    % create the first object so it is big enough to store all of that
    % data. Trim to actual latest extent afterwards.
    
    probable_records = d.bytes ./ ftell(fid);
    probable_samples = ceil(probable_records * size(rec.data, 2));
    probable_endtime = rec.starttime + rec.resolution * probable_samples;
    latest = rec.endtime;
  end
  
  % create blank data matrix
  data = uint16(zeros(length(map), size(rec.data, 2)));
  
  % map record data into correct position for beams (NB mapping indices are
  % beam indices, so need to call getbeamindex)
  for n = 1:length(map)
    if length(map{n}) == 1
      data(n,:) = rec.data(map{n}, :);
    else
      % widebeam can have many samples (round after taking mean)
      % data(n,:) = fix(mean(rec.data(map{n}, :), 1));              % 1 error
      
      % data(n,:) = sum(fix((rec.data(map{n}, :) ./ 7) + 0.5) , 1);              % 1 error
      
      data(n,:) = round(mean(rec.data(map{n}, :), 1));
      
      % data(n,:) = round(mean(double(rec.data(map{n}, :)), 1)); % 95 errors
      % data(n,:) = fix(0.5 + mean(rec.data(map{n}, :), 1));
    end
  end

  
  if num_records == 1
    % extend inital object to the probable end time
    data(:, probable_samples) = 0;
    rec.endtime = probable_endtime;
  
  elseif (rec.endtime) > latest
    latest = rec.endtime;
  end
  
  
  % Work around the GPS bug which began in October 2011.
  gpsBugOffset = timespan(7161, 'd');
  if isvalid(rec.starttime) && getdate(rec.starttime) == getdate(defaults.validstarttime(1) - gpsBugOffset)
    rec.starttime = rec.starttime + gpsBugOffset;
    disp('Fixed GPS bug');
  % else
  %   disp('no bug fix');
  %   rec.starttime
  %   QQ = getdate(defaults.validstarttime(1))
  %   QQQ = getdate(rec.starttime)
  %   QQQ - QQ
  end
  
  % perform some data checks
  mia = [];
  if ~isvalid(rec.starttime) | ...
	rec.starttime < defaults.validstarttime(1) ...
	| rec.starttime > defaults.validstarttime(2)
    local_warning(logfid, filename, rec, num_records, ...
		  sprintf('record start time not valid (was %s)', ...
			  char(rec.starttime)));
    
  elseif any(rec.system_error_number) 
    for ei = find(rec.system_error_number)
      local_warning(logfid, filename, rec, num_records, ...
		    sprintf('system error: %s', rec.system_error_message{ei}));
    end
    
  elseif rec.gpsquality < 1
    local_warning(logfid, filename, rec, num_records, ...
		  sprintf('Bad GPS data quality (was %d)', rec.gpsquality));
    
  elseif rec.noisegenerator
    local_warning(logfid, filename, rec, num_records, 'Noise generator on');
  else
    % data is valid
    samt = (rec.starttime + 0.5*rec.resolution):rec.resolution:rec.endtime;
    integrationtime = repmat(rec.resolution, size(samt));
    mia = rio_rawpower('starttime', rec.starttime, ...
		       'endtime', rec.endtime, ...
		       'sampletime', samt, ...
		       'integrationtime', integrationtime, ...
		       'instrument', rio, ...
		       'units', 'ADC', ...
		       'data', data, ...
		       'beams', beams, ...
		       'load', 0);
    
    
    if isempty(r)
      r = mia;
    elseif ~isempty(mia)
      r = insert(r, mia);
    end
  end
  
    
  
  if nargout > 1
    rec = rmfield(rec, 'data');
    headers(num_records) = rec;
    validrecord(num_records) = ~isempty(mia);
  end
end


% if num_records > 0 & probable_endtime > latest & ~isempty(r)
%   % extract to actual latest endtime 
%   r = extract(r, 'endtime', latest);
% end

fclose(fid);
if logfid ~= 1
  fclose(logfid);
end


function local_warning(logfid, filename, record, record_number, message);
% add newline if necessary
if message(end) ~= 10
  message = sprintf('%s\n', message);
end
fprintf(logfid, 'File        : %s\n', filename);
if ~isempty(record)
  fprintf(logfid, ...
          ['File offset : %d\n' ...
           'Record num  : %d\n' ...
           'Start time  : %s\n'], ...
          record.offset, record_number, char(record.starttime));
end
fprintf(logfid, '%s\n', message);

