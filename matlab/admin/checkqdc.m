function varargout = checkqdc(action, varargin)
%CHECKQDC  Check riometer QDCs.
%
% CHECKQDC('init', ...);
%
% Visually check and approve/reject riometer QDCs. The following name/value
% pairs are recognised (some of which are mandatory):
%
%   'time', TIMESTAMP
%   A time within the standard QDC period, used to select the QDC to
%   load. This parameter must be specified.
%
%   'instrument', RIOMETER
%   The riometer for which QDCs are to be checked. Defaults to the
%   instrument given by INSTRUMENTTYPEINFO(riometer, 'preferred').
%
%   'beams', DOUBLE
%   A vector indicating the beams which should be checked. Defaults to
%   all beams for the selected instrument.
%
%   'loadfilename', CHAR
%   The BEAMSTRFTIME format string for the name of the QDC file(s).
%
%   'loadpathsuffix', CHAR
%   The suffix appended to the standard load path where unchecked QDCs are
%   saved. Defaults to '.new'.
%
%   'mia', RIO_BASE_QDC
%   The quiet-day curve object(s) to be checked. Normally loaded from
%   disk as required.
%
%   'reference',  RIO_QDC_BASE
%   The reference QDC(s). Normally loaded from disk as required (usually
%   it is a RIO_QDC_MEAN_SD object.
%
%   'referencecolor', COLORSPEC
%   The color used to plot reference QDCs.
%
%   'errorfunction', CHAR
%   The name of the function used by DCADJUST to alter the offset in the
%   reference QDCs. Defaults to 'minimisesigndiff' to minimise the
%   effects of large spikes in the data.
%
%   'qdcclass', CHAR
%   The name of the QDC class required for the selected riometer. The
%   default value is obtained by calling riometer/INFO with the
%   'qdcclass' option.
%
%   'removedirectory', LOGICAL
%   Flag indicating if the directory where the peliminary QDCs are stored
%   should be removed when empty. Default to TRUE. Set to FALSE if you are
%   checking QDCs as another Matlab process is creating them.
%
%   'tempdir', CHAR
%   The name of the directory used to store temporary files. Defaults to
%   the standard TEMPDIR directory.
%
%
% CHECKQDC('approve', ...);
%
% Approve riometer QDCs without any visual checks. The same name/value
% pairs for CHECKQDC('init', ...) can be used (see above).
%
% See also RIO_QDC, RIO_RAWQDC, RIO_QDC_BASE, RIO_QDC_MEAN_SD,
% RIO_RAWQDC_MEAN_SD, MAKEQDC.




switch action
 case {'init' 'approve'}
  % init: initialise for visual checking
  % approve: approve without any visual checks
  
  defaults.instrument = preferredinstrument('riometer');
  defaults.time = []; % must be specified
  defaults.beams = []; % defer until real instrument is known
  defaults.loadfilename = ''; % defer until real instrument is known
  defaults.loadpathsuffix = '.new';
  defaults.savefilename = ''; % defer until real instrument is known
  defaults.mia = [];
  if strcmp(action, 'approve')
    defaults.reference = 'none'; % not needed
  else
    defaults.reference = [];
  end
  defaults.referencecolor = [0 .8 0];
  defaults.errorfunction = 'minimisesigndiff';
  defaults.qdcclass = '';
  defaults.removedirectory = true;
  defaults.tempdir = '';
  % other options for different actions
  % previous/next buttons
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi});

  if isempty(defaults.qdcclass)
    % use the appropriate QDC class, but ensure it is the FFT version
    defaults.qdcclass = info(defaults.instrument, 'qdcclass');
    if isempty(findstr('_fft', defaults.qdcclass))
      defaults.qdcclass = [defaults.qdcclass '_fft'];
    end
  end

  df = info(defaults.instrument, 'defaultfilename', defaults.qdcclass);
  if isempty(df)
    error(sprintf('no file information for %s', defaults.qdcclass));
  end
  if isempty(defaults.loadfilename)
    % add suffix to the load path
    [Path fname ext] = fileparts(df.fstr);
    defaults.loadfilename = ...
	fullfile([Path defaults.loadpathsuffix], [fname ext]);
  end
  
  if isempty(defaults.savefilename)
    defaults.savefilename = df.fstr;
  end

  if isempty(defaults.time)
    error('time must be specified');
  end
  
  qdcstarttime = calcqdcstarttime(defaults.instrument, defaults.time, ...
				  defaults.time);

  % work out the beams to load
  numOfBeams = numel(defaults.beams);
  if numOfBeams == 0
    if ~isempty(defaults.mia)
      defaults.beams = getbeams(defaults.mia);
    else
      % use all available beams for which files exist
      [ib wb] = info(defaults.instrument, 'beams');
      tmpBeams = [ib wb];
      for n = 1:length(tmpBeams)
	if length(dir(beamstrftime(qdcstarttime, ...
				   tmpBeams(n), defaults.loadfilename)))
	  defaults.beams(end+1) = tmpBeams(n);
	end
      end
      disp(sprintf('Using beams %s', printseries(defaults.beams)));
      if isempty(defaults.beams)
	defaults.loadfilename
	error('could not find any QDCs to load');
      end
    end
    numOfBeams = numel(defaults.beams);
  end

  

  if isempty(defaults.tempdir)
    defaults.tempdir = tempname;
  end
  
  if strcmp(defaults.qdcclass((end-3):end), '_fft')
    refqdcclass = [defaults.qdcclass(1:(end-3)) 'mean_sd'];
  else
    refqdcclass = [defaults.qdcclass '_mean_sd'];
  end

  dfqdc = info(defaults.instrument, 'defaultfilename', refqdcclass);
  if isempty(dfqdc)
    refloadfilename = {};
  else
    refloadfilename = cell(size(defaults.beams));
    for n = 1:numOfBeams
      refloadfilename{n} = beamstrftime(qdcstarttime, defaults.beams(n), ...
					dfqdc.fstr);
    end
  end
  % refloadfilename = info(defaults.instrument, 'defaultfilename', ...
  %			  refqdcclass, defaults.beams);
  
  if ~isempty(defaults.mia)
    mia = repmat(feval(class(defaults.mia)), numOfBeams, 1);
  else
    mia = repmat(feval(defaults.qdcclass), numOfBeams, 1);
  end
  
  %    if isequal(defaults.reference, 'none')
  %      refmia = [];
  %    elseif ~isempty(defaults.reference) 
  %      refmia = repmat(feval(class(defaults.reference)), numOfBeams, 1);
  %    elseif ~isempty(refloadfilename)
  %      refmia = repmat(feval(refqdcclass), numOfBeams, 1);
  %    else
  %      refmia = [];
  %    end
  refmia = cell(1, numOfBeams);
  
  for n = 1:numOfBeams
    % load the QDCs beam by beam into independent objects. This ensures
    % each beam can have its own processing history
    
    if ~isempty(defaults.mia) 
      mia(n) = extract(defaults.mia, 'beams', defaults.beams(n));
    else
      mia(n) = feval(defaults.qdcclass, ...
		     'starttime', defaults.time, ...
		     'endtime', defaults.time, ...
		     'instrument', defaults.instrument, ...
		     'beams', defaults.beams(n), ...
		     'log', 0, ...
		     'load', 1, ...
		     'loadoptions', ...
		     {'filename', defaults.loadfilename, ...
		      'failiffilemissing', 1, ...
		      'correctclass', 1});
    end
    
    if isequal(defaults.reference, 'none')
      refmia{n} = [];
    elseif ~isempty(defaults.reference) 
      % use the reference object
      refmia{n} = extract(defaults.reference, 'beams', defaults.beams(n));

      % try loading reference QDCs if they are available
    elseif ~isempty(refloadfilename)
      % load the reference QDCs (mean + std. dev.)
      refmia{n} = feval(refqdcclass, ...
			'starttime', defaults.time, ...
			'endtime', defaults.time, ...
			'instrument', defaults.instrument, ...
			'beams', defaults.beams(n), ...
			'log', 0, ...
			'load', 1, ...
			'loadoptions', {'failiffilemissing', 0});
    end

    if isa(refmia{n}, 'rio_qdc_base')
      % If the reference is any sort of QDC then check it isn't
      % blank/empty
      if all(isnan(getdata(refmia{n}))) | isempty(getdata(refmia{n}))
	refmia{n} = [];
      end
    end
    
  end
  ud.mia = mia;
  ud.origmia = mia;
  ud.refmia = refmia;
  ud.beams = defaults.beams;
  ud.beamindex = 1;
  ud.qdcstarttime = calcqdcstarttime(defaults.instrument, defaults.time, ...
				     defaults.time);
  % save the default values, but not a few which are large and already
  % available in ud
  ud.defaults = rmfield(defaults,{'mia', 'reference'});

  if strcmp(action, 'approve')
    % loop through all beams and approve them
    for n = 1:numel(defaults.beams)
      disp(sprintf('beam: %d', defaults.beams(n)));
      ud.beamindex = n;
      localAcceptQdc(ud);
    end
    return
  end
  
  % center window initially
  pos = [1 1 800 600];
  screensize = get(0, 'ScreenSize');
  pos(1:2) = [screensize(3:4) - pos(3:4)] ./ 2;
  [fh gh lh ud] = localPlot(ud, mfilename, defaults.beams(1), pos);
  
  set(fh, 'KeyPressFcn', sprintf('%s(''keypress'');', mfilename));
  
 case {'accept' 'fail'}

  % If accepted then save the QDC into the correct place. For both actions
  % remove the current beam from the set of beams under consideration. This
  % means failed beams are just completely ignore from that point on, and
  % must be remade or checked/editted at a later date.
  
  % Quickly make the button inactive to guard against it being pressed
  % again.
  if nargin == 1
    % called as "mfilename('accept')" or "mfilename('fail')"
    cbo = gcbo;
    set(cbo, 'Enable', 'inactive');
    cbf = gcbf;
  else
    % called as "mfilename('accept', cbf)" or "mfilename('fail', cbf)"
    cbf = varargin{1};
  end
  
  set(cbf, 'Pointer', 'watch');
  % Make all other buttons inactive too
  set(findall(cbf, 'Type', 'uicontrol'), 'Enable', 'inactive');
  drawnow;
  
  ud = get(cbf, 'UserData');
  % bb = getbeambox(cbf);
  % beam = getbeams(bb);
  % mia = extract(ud.mia, 'beams', beam);
  mia = ud.mia(ud.beamindex);

  if strcmp(action, 'accept')
    localAcceptQdc(ud);
  end
    
  % delete this beam from the data
  if length(ud.beams) == 1
    disp('done');
  else
    if ud.beamindex == length(ud.beams)
      nextBeam = ud.beams(1);
    else
      nextBeam = ud.beams(ud.beamindex + 1);
    end

    
    ud.mia(ud.beamindex) = [];
    ud.refmia(ud.beamindex) = []; % delete entry at this location
    ud.origmia(ud.beamindex) = [];
    ud.beams(ud.beamindex) = [];
    ud.beamindex = find(ud.beams == nextBeam);

    % now plot the selected beam
    [fh gh lh ud] = localPlot(ud, mfilename, nextBeam, ...
			      get(cbf, 'Position'));
  end
  
  % remove existing window
  delete(cbf);
  
 case 'edit'
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  mia = miaedit('init', ud.mia(ud.beamindex), ...
		'reference', ud.refmia{ud.beamindex}, ...
		'windowstyle', 'modal', ...
		'referencecolor', ud.defaults.referencecolor);

  ud.mia(ud.beamindex) = mia;
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case {'edit_orig' 'replace_nans_interp_orig' 'replace_nans_refqdc_orig'}
  % mfilename('edit_orig')
  % mfilename('replace_nans_interp_orig')
  % mfilename('replace_nans_refqdc_orig')
  % mfilename('replace_nans_refqdc_orig',offset)
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  origqdc = getoriginalqdc(ud.mia(ud.beamindex));
  if isempty(origqdc) | ~isa(origqdc, 'rio_qdc_base')
    errordlg('Original QDC object not found', 'Not found');
    return
  end
  fitorder = getfitorder(ud.mia(ud.beamindex));

  % do the editing/filtering
  parentclass = getparentclass(ud.mia(ud.beamindex));
  switch action
   case 'edit_orig'
    
    if all(isnan(getdata(feval(parentclass, ud.mia(ud.beamindex)))))
      reference = ud.refmia{ud.beamindex};
    else
      reference = feval(parentclass, ud.mia(ud.beamindex));
    end
    % 'reference', ud.refmia{ud.beamindex}, ...
    origqdc = miaedit('init', origqdc, ...
		      'reference', reference, ...
		      'windowstyle', 'modal', ...
		      'referencecolor', ud.defaults.referencecolor);
    
   case 'replace_nans_interp_orig'
    origqdc = filter(mia_filter_replace_nans(varargin{1}), origqdc);
    
   case 'replace_nans_refqdc_orig'
    [origqdc modified] = ...
	localReplaceNansWithRefQdc(origqdc, ud.refmia{ud.beamindex}, ...
				   varargin{:});
    if ~modified
      set(cbf, 'Pointer', 'arrow');
      return
    end
    
   otherwise
    error(sprintf('unknown action (was ''%s'')', action));
  end

  % now convert the editted original QDC back to an FFT QDC
  mia = feval(ud.defaults.qdcclass, origqdc, ...
	      'preserveoriginalqdc', 1, ...
	      'fitorder', fitorder)
  
  ud.mia(ud.beamindex) = mia;
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'replace_nans_interp'
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  ud.mia(ud.beamindex) = filter(mia_filter_replace_nans(varargin{1}), ...
				ud.mia(ud.beamindex));
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'replace_nans_refqdc'
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  [ud.mia(ud.beamindex) modified] = ...
      localReplaceNansWithRefQdc(ud.mia(ud.beamindex), ...
				 ud.refmia{ud.beamindex}, ...
				 varargin{:});
  if ~modified
    set(cbf, 'Pointer', 'arrow');
    return
  end

  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'fft_fit_parameters'
  cbf = gcbf;
  ud = get(cbf, 'UserData');
  set(cbf, 'Pointer', 'watch');
  fitorder = getfitorder(ud.mia(ud.beamindex));
  str = inputdlg('FFT fit order? (Use -1 to get default value)', ...
		 'FFT fit order', 1, ...
		 {num2str(fitorder)});
  if isempty(str)
    set(cbf, 'Pointer', 'arrow'); % cancel pressed
    return
  end
  
  newfitorder = real(str2double(str));
  % if str is not a number then newfitorder will be NaN. Check for valid
  % numbers (non-reals, and non-integers) which do not make sense for fit
  % order
  if newfitorder ~= floor(real(newfitorder)) | newfitorder < -1
    newfitorder = nan; 
  end
  
  if isnan(newfitorder)
    errordlg('Invalid value for FFT fit order', 'Error');
    set(cbf, 'Pointer', 'arrow');
    return
  end
  
  % ud.mia(ud.beamindex) = feval(ud.defaults.qdcclass, ...
  %			       getoriginalqdc(ud.mia(ud.beamindex)), ...
  % 			       'fitorder', newfitorder);
  ud.mia(ud.beamindex) = setfitorder(ud.mia(ud.beamindex), newfitorder);
  
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'show_processing'
  cbf = gcbf;
  ud = get(cbf, 'UserData');
  disp(sprintf('Processing for beam %d', ud.beams(ud.beamindex)));
  proc = getprocessing(ud.mia(ud.beamindex));
  if ischar(proc)
    disp(proc);
  elseif iscell(proc)
    for n = 1:numel(proc)
      disp(proc{n});
    end
  else
    error(sprintf('unknown type for processing field (was ''%s'')', ...
		  class(proc)));
  end
  

 case 'edit_baddata'
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  parentclass = getparentclass(ud.mia(ud.beamindex));
  if all(isnan(getdata(feval(parentclass, ud.mia(ud.beamindex)))))
    reference = ud.refmia{ud.beamindex};
  else
    reference = feval(parentclass, ud.mia(ud.beamindex));
  end
  tmp = miaedit('init', getoriginalqdc(ud.mia(ud.beamindex)), ...
		'reference', reference, ...
		'filters', {mia_filter_fixed(nan)}, ...
		'configurefilters', 0, ...
		'windowstyle', 'modal', ...
		'referencecolor', ud.defaults.referencecolor);
  
  ud.mia(ud.beamindex) = setbaddata(ud.mia(ud.beamindex), ...
				    {find(isnan(getdata(tmp)))});

  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'revert'
  % revert to origmia for this beam
  cbf = gcbf;
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  ud.mia(ud.beamindex) = ud.origmia(ud.beamindex);
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));
  
  % remove existing window
  delete(cbf);
  drawnow;
  
 case 'beambox'
  % The callback figure might be the popup window. Need to get the
  % beambox object, and then its parent figure
  bbh = gcbo;
  bb = getbeambox(bbh);
  fh = getparentfigure(bb);
  set(fh, 'Pointer', 'watch');
  
  beam = getbeams(bb);
  ud = get(fh, 'UserData');
  ud.beamindex = find(beam == ud.beams);

  % now plot the selected beam
  [fh2 gh lh ud] = localPlot(ud, mfilename, beam, get(fh, 'Position'));

  % remove existing window
  delete(fh);
  
 case {'previous' 'next'}
  if nargin == 1
    % called as "mfilename('previous')" or "mfilename('next')"
    cbo = gcbo;
    set(cbo, 'Enable', 'inactive');
    cbf = gcbf;
  else
    % called as "mfilename('previous', cbf)" or "mfilename('next', cbf)"
    cbf = varargin{1};
  end
  set(cbf, 'Pointer', 'watch');
  ud = get(cbf, 'UserData');
  beep = 0;
  switch action
   case 'previous'
    ud.beamindex = ud.beamindex - 1;
    if ud.beamindex < 1
      beep = 1;
      % fprintf(1, '\a');
      ud.beamindex = length(ud.beams);
    end
    
   case 'next'
    ud.beamindex = ud.beamindex + 1;
    if ud.beamindex > length(ud.beams);
      % fprintf(1, '\a');
      beep = 1;
      ud.beamindex = 1;
    end
   otherwise
    error('unknown error'); % oops
  end

  % now plot the selected beam
  [fh gh lh ud] = localPlot(ud, mfilename, ud.beams(ud.beamindex), ...
			    get(cbf, 'Position'));

  % remove existing window
  delete(cbf);
  
  % signal wrapping around start/end of beams
  if beep
    fprintf(1, '\a');
  end
  
  
 case 'zoom'
  cbf = gcbf;
  p = get(gcbo);
  za = p.UserData;
  if ~iscell(za)
    za = {za};
  end
  for n = 1:length(za)
    zoom(cbf, za{n});
  end
  
 case 'keypress'
  % called as mfilename('keypress');
  cbf = gcbf;

  % disable further keypresses
  set(cbf, 'KeyPressFcn', '');

  switch get(cbf, 'CurrentCharacter');
   case {'a' 'A'}
    disp('Accept QDC');
    feval(mfilename, 'accept', cbf);
    
   case {'f' 'F'}
    disp('Fail QDC');
    feval(mfilename, 'fail', cbf);
    
   case {'n' 'N'}
    disp('Next beam');
    feval(mfilename, 'next', cbf);

   case {'p' 'P'}
    disp('Previous beam');
    feval(mfilename, 'previous', cbf);
    
   otherwise
    ; % unknown key do nothing
  end
  
 otherwise
  % error(sprintf('unknown action (was ''%s'')', action));
  feval(mfilename, 'init', action, varargin{:});
  
end


function [fh, gh, lh, ud2] = localPlot(ud, mfname, beam, pos)
% disp(sprintf('localPlot beam #%d', beam));
qdcnans = isnan(getdata(ud.mia(ud.beamindex)));
qdcHasNans = any(qdcnans);
qdcAllNans = all(qdcnans);
clear qdcnans;

ud.beamindex = find(ud.beams == beam);
[fh gh lh] = plot(ud.mia(ud.beamindex), ...
		  'beams', beam, ...
		  'busyaction', 'cancel', ...
		  'defaultuicontrolbusyaction','cancel', ...
		  'defaultuimenubusyaction','cancel', ...
		  'interruptible', 'off', ...
		  'defaultuicontrolinterruptible', 'off', ...
		  'defaultuimenuinterruptible', 'off', ...
		  'visible', 'off', ...
		  'grid', 'on', ...
		  'footer', 1, ...
		  'position', pos, ...
		  'zdata', [4 1]);

% Add some stuff to the tools menu
toolH = findall(fh, 'Type', 'uimenu', 'Tag', 'tools');
if ~isempty(toolH)
  set(toolH, 'Visible', 'on');
  if strcmp(ud.defaults.qdcclass((end-3):end), '_fft')
    act = 'replace_nans_interp_orig';
  else
    act = 'replace_nans_interp';
  end
  % turn off heater menu
  set(findall(toolH, 'Tag', 'heater'), 'Visible', 'off');
  tmpH = uimenu('Parent', toolH, ...
		'Label', 'Replace NaNs by interpolation', ...
		'Enable', logical2onOff(qdcHasNans), ...
		'Tag', 'replace_nans_interp_menu');
  uimenu('Parent', tmpH, ...
	 'Label', 'Linear', ...
	 'Enable', logical2onOff(qdcHasNans), ...
	 'Callback', sprintf('%s(''%s'',''linear'');', mfilename, act), ...
	 'Tag', 'replace_nans_interp_linear');
  uimenu('Parent', tmpH, ...
	 'Label', 'Spline', ...
	 'Enable', logical2onOff(qdcHasNans), ...
	 'Callback', sprintf('%s(''%s'',''spline'');', mfilename, act), ...
	 'Tag', 'replace_nans_interp_spline');

  
  if strcmp(ud.defaults.qdcclass((end-3):end), '_fft')
    callback = [mfilename '(''replace_nans_refqdc_orig'',0);'];
  else
    callback = [mfilename '(''replace_nans_refqdc'',0);'];
  end
  uimenu('Parent', toolH, ...
	 'Label', 'Replace NaNs with mean QDC', ...
	 'Enable', logical2onOff(~isempty(ud.refmia{ud.beamindex}) & qdcHasNans), ...
	 'Callback', callback, ...
	 'Tag', 'replace_nans_refqdc');
  
  if strcmp(ud.defaults.qdcclass((end-3):end), '_fft')
    callback = [mfilename '(''replace_nans_refqdc_orig'');'];
  else
    callback = [mfilename '(''replace_nans_refqdc'');'];
  end
  uimenu('Parent', toolH, ...
	 'Label', 'Replace NaNs with mean QDC and offset', ...
	 'Enable', logical2onOff(~isempty(ud.refmia{ud.beamindex}) & qdcHasNans), ...
	 'Callback', callback, ...
	 'Tag', 'replace_nans_refqdc');

  if strcmp(ud.defaults.qdcclass((end-3):end), '_fft')
    uimenu('Parent', toolH, ...
	   'Label', 'Change FFT fit parameters', ...
	   'Callback', [mfilename '(''fft_fit_parameters'');'], ...
	   'Tag', 'fft_fit_parameters');
  end
  
  uimenu('Parent', toolH, ...
	 'Label', 'Show processing', ...
	 'Callback', [mfilename '(''show_processing'');'], ...
	 'Tag', 'show_processing');
end

if ~isempty(ud.refmia{ud.beamindex})
  set(gh, 'NextPlot', 'add');

  % DC adjust the reference curve
  if strcmp(ud.defaults.qdcclass((end-3):end), '_fft') & qdcAllNans
    % FFT version of QDC all nans, so have to adjust againt the original
    % curve
    ud.refmia{ud.beamindex} = ...
	dcadjust(ud.refmia{ud.beamindex}, ...
		 getoriginalqdc(ud.mia(ud.beamindex)), ...
		 'errorfunction', ud.defaults.errorfunction);
  else
    ud.refmia{ud.beamindex} = ...
	dcadjust(ud.refmia{ud.beamindex}, ...
		 ud.mia(ud.beamindex), ...
		 'errorfunction', ud.defaults.errorfunction);
  end
  plot(ud.refmia{ud.beamindex}, ...
       'beams', beam, ...
       'plotaxes', gh, ...
       'color', ud.defaults.referencecolor, ...
       'zdata', [3 2]);
end

set(gh, ...
    'Children', flipud(allchild(gh)), ...
    'XGrid', 'on', ...
    'YGrid', 'on', ...
    'ZGrid', 'on');


% copy all fields in the user data struct into the plots user data
% area. Don't just assign since the plot window might be using userdata
% itself
ud2 = get(fh, 'UserData');
fn = fieldnames(ud);
for n = 1:length(fn)
  ud2 = setfield(ud2, fn{n}, getfield(ud, fn{n}));
end

set(fh, ...
    'UserData', ud2, ...
    'Name', beamstrftime(ud2.qdcstarttime, beam, ud2.defaults.loadfilename));

locateAxisTight(gh);

% now add some buttons
% add buttons to the footer area

% footH = findall(fh, 'Tag', 'footer');
% set(footH, 'Units', 'pixels');
% p = get(footH);

% accept button. Don't change the Tag since that is used by the separate
% callback function.

callback = sprintf('%s(''accept'');', mfname);


% 'Enable', logical2onOff(~qdcHasNans), ...
accH = uicontrol('Parent', fh, ...
		 'Position', [10 2 1 1], ...
		 'Style', 'pushbutton', ...
		 'Callback', callback, ...
		 'String', 'Accept', ...
		 'Tag', 'accept');
if qdcHasNans
  set(accH, 'BackgroundColor', [0.8 0.6 .6]);
end
if matlabversioncmp('>=', '5.2')
  set(accH, 'TooltipString', 'Accept this QDC');
end
accPos = uiresize(accH, 'l', 'b');

% fail button. Don't change the Tag since that is used by the separate
% callback function.
callback = sprintf('%s(''fail'');', mfname);
failPos = accPos + [accPos(3)+10 0 0 0];
failH = uicontrol('Parent', fh, ...
		  'Position', failPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', callback, ...
		  'String', 'Fail', ...
		  'Tag', 'fail');
if matlabversioncmp('>=', '5.2')
  set(failH, 'TooltipString', 'Fail this QDC');
end

failPos =  uiresize(failH, 'l', 'b');

% fail button. Don't change the Tag since that is used by the separate
% callback function.
callback = sprintf('%s(''edit'');', mfname);
editPos = failPos + [failPos(3)+10 0 0 0];
editH = uicontrol('Parent', fh, ...
		  'Position', editPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', callback, ...
		  'String', 'Edit', ...
		  'Tag', 'edit');
if matlabversioncmp('>=', '5.2')
  set(editH, 'TooltipString', 'Edit this QDC');
end
editPos =  uiresize(editH, 'l', 'b');


if any(strcmp(ud.defaults.qdcclass, {'rio_qdc_fft', 'rio_rawqdc_fft'}))
  % specific editing functions for FFT versions of QDCs

  editOrigQdcPos = editPos + [editPos(3)+10 0 0 0];
  editOrigQdcH = uicontrol('Parent', fh, ...
			   'Position', editOrigQdcPos, ...
			   'Style', 'pushbutton', ...
			   'Callback', [mfname '(''edit_orig'');'], ...
			   'String', 'Edit orig.', ...
			   'Tag', 'edit_orig');
  if matlabversioncmp('>=', '5.2')
    set(editOrigQdcH, 'TooltipString', 'Edit the original QDC');
  end
  editOrigQdcPos =  uiresize(editOrigQdcH, 'l', 'b');

  editBadDataPos = editOrigQdcPos + [editOrigQdcPos(3)+10 0 0 0];
  editBadDataH = uicontrol('Parent', fh, ...
			   'Position', editBadDataPos, ...
			   'Style', 'pushbutton', ...
			   'Callback', [mfname '(''edit_baddata'');'], ...
			   'String', 'Edit bad data', ...
			   'Tag', 'edit_baddata');
  if matlabversioncmp('>=', '5.2')
    set(editBadDataH, 'TooltipString', 'Edit the bad data mask');
  end
  editBadDataPos =  uiresize(editBadDataH, 'l', 'b');
  
else
  editBadDataPos = editPos;
end

callback = sprintf('%s(''revert'');', mfname);
revertPos = editBadDataPos + [editBadDataPos(3)+10 0 0 0];
revertH = uicontrol('Parent', fh, ...
		    'Position', revertPos, ...
		    'Style', 'pushbutton', ...
		    'Callback', callback, ...
		    'String', 'Revert', ...
		    'Tag', 'revert');
if matlabversioncmp('>=', '5.2')
  set(revertH, 'TooltipString', ...
	       'Undo any changes made in this session to this QDC');
end

revertPos =  uiresize(revertH, 'l', 'b');

% add a beam selection tool
[ib wb] = info(getinstrument(ud2.mia(1)), 'beams');
ibAllow = intersect(ib, ud2.beams);
wbAllow = intersect(wb, ud2.beams);

beamboxPos = revertPos + [revertPos(3)+10 0 0 0];
% need exactly one beam
beamboxH = beambox('parent', fh, ...
		   'position', beamboxPos, ...
		   'imaging', ib, ...
		   'widebeam', wb, ...
		   'beams', beam, ...
		   'title', ' to check', ...
		   'imagingallow', ibAllow, ...
		   'widebeamallow', wbAllow, ...
		   'limitedbeams', -1, ...
		   'callback', ...
		   sprintf('%s(''beambox'');', mfname));

% only need to get static information so take a temporary copy
bb = getbeambox(beamboxH);
beamboxPos = getposition(bb);
% appendcallback(bb, sprintf('%s(''beambox'');', mfname));
% setbeams(bb, beams);

% resize the series box so it is big enough to store the largest beam
% number. Do this by finding out the number of digits required (111 is
% actually narrower than 99). Then use the number of digits to produce a
% string of 8s (probably the widest number). Add an extra character to
% ensure sufficient space.
bbhandles = gethandles(bb);
uiresize(findall(bbhandles, 'Tag', 'seriesbox'), 'l', 'b', ...
	 repmat('8', 1, 2+floor(log10(max([ib wb])))));

beamboxPos = boundingbox(bbhandles);

% add prev/next beams
if length(ud.beams) == 1
  enable = 'off';
  set(bbhandles, 'Enable', enable);
else
  enable = 'on';
end

prevPos = beamboxPos + [beamboxPos(3)+10 0 0 0];
prevH = uicontrol('Parent', fh, ...
		  'Position', prevPos, ...
		  'Style', 'pushbutton', ...
		  'Enable', enable, ...
		  'Callback', ...
		  sprintf('%s(''previous'');', mfname), ...
		  'String', 'Previous', ...
		  'UserData', 'on', ...
		  'Tag', 'previous');
if matlabversioncmp('>=', '5.2')
  set(prevH, 'TooltipString', 'Go to previous QDC');
end
prevPos =  uiresize(prevH, 'l', 'b');

nextPos = prevPos + [prevPos(3)+10 0 0 0];
nextH = uicontrol('Parent', fh, ...
		  'Position', nextPos, ...
		  'Style', 'pushbutton', ...
		  'Enable', enable, ...
		  'Callback', ...
		  sprintf('%s(''next'');', mfname), ...
		  'String', 'Next', ...
		  'UserData', 'on', ...
		  'Tag', 'next');
if matlabversioncmp('>=', '5.2')
  set(nextH, 'TooltipString', 'Go to next QDC');
end
nextPos =  uiresize(nextH, 'l', 'b');


zoomPos = nextPos + [nextPos(3)+10 0 0 0];


localAddZoomControls(fh, zoomPos, sprintf('%s(''zoom'');', ...
					  mfname));

% Enable keyboard accelerator keys
set(fh, 'KeyPressFcn', sprintf('%s(''keypress'');', mfilename));
set(fh, 'Visible', 'on');
drawnow;

function localAddZoomControls(fh, pos, callback)
% Add some controls for zoom actions. UserData contains the argument(s)
% for zoom
zoom(fh, 'off');
zoomPos = [pos(1:2) 1 1];
zoomH = uicontrol('Parent', fh, ...
		  'Position', zoomPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', callback, ...
		  'String', 'Zoom on', ...
		  'UserData', 'on', ...
		  'Tag', 'zoom');
if matlabversioncmp('>=', '5.2')
  set(zoomH, 'TooltipString', 'Turn zoom on');
end
zoomPos =  uiresize(zoomH, 'l', 'b');

zoomPos(1) =  zoomPos(1) + zoomPos(3) + 10; % shift right
zoomH = uicontrol('Parent', fh, ...
		  'Position', zoomPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', callback, ...
		  'String', 'Zoom off', ...
		  'UserData', 'off', ...
		  'Tag', 'zoom');
if matlabversioncmp('>=', '5.2')
  set(zoomH, 'TooltipString', 'Turn zoom off');
end
zoomPos =  uiresize(zoomH, 'l', 'b');

zoomPos(1) =  zoomPos(1) + zoomPos(3) + 10; % shift right
zoomH = uicontrol('Parent', fh, ...
		  'Position', zoomPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', callback, ...
		  'String', 'Zoom out', ...
		  'UserData', 'out', ...
		  'Tag', 'zoom');
if matlabversioncmp('>=', '5.2')
  set(zoomH, 'TooltipString', 'Zoom out');
end
zoomPos =  uiresize(zoomH, 'l', 'b');


function locateAxisTight(h)

for n = 1:numel(h)
  p = get(h);
  yLimMin = inf;
  yLimMax = -inf;
  for m = 1:length(p.Children)
    q = get(p.Children(m));
    if isfield(q, 'YData')
      yLimMin = min(min(q.YData), yLimMin);
      yLimMax = max(max(q.YData), yLimMax);
    end
  end
  if isfinite(yLimMin)
	set(h, 'YLim', [yLimMin yLimMax]);
  end
end


function r = logical2onOff(a)
if a
  r = 'on';
else
  r = 'off';
end



function [r, modified] = localReplaceNansWithRefQdc(mia, refqdc, varargin)
data = getdata(mia);
nans = isnan(data);
r = mia;
modified = 0;
if any(nans)
  if length(varargin)
    offset = varargin{1};
  else
    offset = inputdlg(sprintf('Offset for QDC (%s)?', getunits(mia)), ...
		      'Enter offset');
    if isempty(offset)
      disp('operation cancelled');
      return
    end
    offset = str2num(offset{1});
  end
  
  refdata = getdata(refqdc);
  data(nans) = refdata(nans) + offset;
  modified = 1;
  r = setdata(r, data);
  if isa(refqdc, 'rio_qdc_mean_sd') | isa(refqdc, 'rio_rawqdc_mean_sd')
    qdcname = 'mean QDC';
  else
    qdcname = ['QDC from period ' dateprintf(mia)];
  end
  proc = sprintf(['NaNs replaced with %s at ' ...
		  'positions %s, offset %g%s'], ...
		 qdcname, printseries(find(nans)), ...
		 offset, getunits(mia));
  r = addprocessing(r, proc);
end


function localAcceptQdc(ud)
% take a backup copy of the original file
origname = beamstrftime(ud.qdcstarttime, ud.beams(ud.beamindex), ...
			ud.defaults.loadfilename);
[origpath origfile origext] = fileparts(origname);

fname = fullfile(ud.defaults.tempdir, [origfile origext]);
disp(sprintf('saving backup copy of original to %s', fname));
save(ud.origmia(ud.beamindex), fname);

% Generate new file name
fname = beamstrftime(ud.qdcstarttime, ud.beams(ud.beamindex), ...
		     ud.defaults.savefilename);

% Create directory for the new file. Does this before deleting anything,
% then if we don't have permission to create a directory nothing was
% deleted.
dname = dirname(fname);
if ~url_exist(dname)
  disp(['Creating directory ' dname]);
  url_mkdir(dname);
end

% delete original file first, in case new and old names are the same
disp(sprintf('deleting original file %s', origname));
delete(origname);

% save, after clearing Preliminary data quality marker

% attempt to remove original directory (if new and old directories
% are different)
if ~strcmp(origpath, fileparts(fname)) & ud.defaults.removedirectory
  ignoreNotEmptyMessages = rmdir(origpath);
end


% save(ud.mia(ud.beamindex), fname);
qdc = cleardataquality(ud.mia(ud.beamindex), 'Preliminary');

if isa(qdc, 'rio_qdc_fft') | isa(qdc, 'rio_rawqdc_fft')
  qdc = clearoriginalqdcdataquality(qdc, 'Preliminary');
end
save(qdc, fname);

