
% st = timestamp([1995 10 06 0 0 0]);
st = timestamp([1998 6 26 0 0 0]);
et = timestamp([1999 08 01 0 0 0]);
day = timespan(1, 'd');


fstr = '/home/belfry/pkr/%y%m%dminbin.cna';

t = st;
while t < et
  filename = strftime(t, fstr);
  if exist(filename)
    mia = process_pkr_data_file(filename);
    if getdate(getstarttime(mia)) ~= t;
      error('data start date does not match filename date');
    end
    save(mia);
  end
  t = t + day;
end


