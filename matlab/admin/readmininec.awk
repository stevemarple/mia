# keep track of current elevation value
($3 == "Azimuth"){azimuth = $6}

# output lines which start with digits, but don't try processing the
# comment line stating "38.2MHz Scaled MHz La Jolla Antenna..." 
# (NR > 10 && $1 ~ /[0-9]+/){printf("%g\t%g\t%g\t%g\t%g\n", 
#				  azimuth, $1, $2, $3, $4)}
(NR > 10 && $1 ~ /[0-9]+/){printf("%3d %s\n", azimuth, $0)}

