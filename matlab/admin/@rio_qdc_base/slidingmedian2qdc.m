function r = slidingmedian2qdc(mia, varargin)
%
%   r = SLIDINGMEDIAN2QDC3(mia, ...)
%   r:  completed QDC
%   mia: basic information for QDC, as a QDC object
%

% was slidingmedian2qdc3 in iristool

r = [];
PREC = 'double';
functionStartTime = timestamp('now');

in = getinstrument(mia);

defaults.prefilter = [];
defaults.postfilter = [];
defaults.medianwindowsize = timespan(599,'s');
defaults.exclude = [];
defaults.failiffilemissing = 0;
defaults.qdcclass = class(mia);
[defaults unvi] = interceptprop(varargin, defaults);

switch defaults.qdcclass
 case 'rio_rawqdc'
  powerclass = 'rio_rawpower';
  
 case 'rio_qdc'
  powerclass = 'rio_power';
  
 otherwise
  error(sprintf('unknown riometer QDC class (was %s)', ...
		defaults.qdcclass));
end

% print debug code ------
defaults.prefilter
defaults.postfilter
size(defaults.exclude)
% print debug code (end) ------

[tmp mfname] = fileparts(mfilename);
basename = fullfile(tempdir, [mfname '.' int2str(getpid)]);

starttime = getstarttime(mia);
endtime = getendtime(mia);
res = getresolution(mia);
loc = getlocation(in);



qdcSamples = ceil(siderealday / res);

beams = getbeams(mia);
numOfBeams = prod(size(beams));

filename = getfilename(mia);
if isempty(filename)
  filename = info(in, 'defaultfilename', mia);
  % elseif ~iscell(filename) | length(filename) ~= numOfBeams
  %    error('filename must be a cell array the same size as beams');
elseif ~ischar(filename)
  error('filename must be a string');
end


medianblocksize = floor(defaults.medianwindowsize / res);
if rem(medianblocksize, 2) == 0
  % medianblocksize is even, should be odd
  medianblocksize = medianblocksize + 1;
end

disp(sprintf('medianblocksize: %d', medianblocksize));

blockWaste = floor(medianblocksize/2);


autofillIfilqdcmeansd = 0;
if ~isempty(defaults.prefilter)
  if isa(defaults.prefilter, 'ifilqdcmeansd')
    autofillIfilqdcmeansd = isempty(getqdcmeansd(defaults.prefilter));
    autofillIfilqdcmeansdLimits = getlimits(defaults.prefilter);
    % need extra copies of mean and SD of QDC
  end
end

% format string for raw files

rawFstr = [basename, '.raw.%d'];
medianFstr =  [basename, '.median.%d'];

units = [];

for bn = 1:numOfBeams
  % STAGE 1
  % load all the data for a single beam

  if ~isempty(defaults.prefilter)
    if autofillIfilqdcmeansd
      % fill only with the beam needed, to keep memory requirements down
      disp('autofilling ifilqdcmeansd');
      defaults.prefilter = ...
	  ifilqdcmeansd(irisqdcmeansd(starttime, in, beams(bn)), ...
	  autofillIfilqdcmeansdLimits(1),  autofillIfilqdcmeansdLimits(2));
    end
  end

  % disp(['IPT LOADING ' dateprintf(starttime, endtime)]);
  % ipt = rio_power('starttime', starttime, ...
  ipt = feval(powerclass, ...
	      'starttime', starttime, ...
	      'endtime', endtime, ...
	      'resolution', res, ...
	      'instrument', in, ...
	      'log', 0, ...
	      'load', 1, ...
	      'loadoptions', {'failiffilemissing', ...
		    defaults.failiffilemissing}, ...
	      'beams', beams(bn));
  if any(strmatch(class(getdata(ipt)), ...
		  {'int8' 'int16' 'int32' 'uint8' 'uint16' 'uint32'}))
    data = getdata(ipt);
    data(data == 0) = nan;
    ipt = setdata(ipt, data);
  end
  bi = getparameterindex(ipt, beams(bn));

  if ~isempty(getunits(ipt))
    if isempty(units)
      units = getunits(ipt);
    elseif ~strcmp(units, getunits(ipt))
      error(sprintf('units were previously %s and are now %s', ...
		    units, getunits(ipt)));
    end
  end
  
  % check if any data should be excluded
  if ~isempty(defaults.exclude)
    % work through all rows (all exclusion periods)
    for er = 1:size(defaults.exclude, 1) % er = exclude row number
      disp(['checking ' dateprintf(defaults.exclude(er, 1), ...
				   defaults.exclude(er, 2))]); 

      % find common time (if any)
      excStartTime = ceil(max(starttime, defaults.exclude(er,1)), ...
			  res);
      excEndTime = floor(min(endtime, defaults.exclude(er,2)), ...
			 res);
      
      if excEndTime > excStartTime
	% exclude using this rule
	starttime
	endtime
	excStartTime
	excEndTime
	ti = fix([round((excStartTime - starttime)./res):...
		  round((excEndTime - starttime)./res)]);  % time indices
	
	% some debugging info
	disp('SLIDINGMEDIAN2QDC debuggin info:');
	disp(['bi: ', printseries(bi)]);
	disp(['ti: ', printseries(ti)]);
	%  one final sanity check, which should always pass
	if ~isempty(ti)
	  % if ti is empty then all elements are set to nan - not right!
	  disp(sprintf('Excluding data for beam %d: %s (%s)', ...
		       beams(bn), dateprintf(excStartTime, excEndTime), ...
		       printseries(ti)));
	  % ipt = setdata(ipt, bi, ti, nan)
	  iptData = getdata(ipt);
	  iptData(bi, ti) = nan;
	  ipt = setdata(ipt, iptData);
	  clear iptData;
	end
      end
    end
  end
  
  % pre-filter, if set
  if ~isempty(defaults.prefilter)
    % [fh gh] = plot(ipt, 'color', 'r'); hold on;
    disp('filtering ...');
    ipt = filter(defaults.prefilter, ipt);
    disp('done');
    % plot(ipt, 'plotaxes', gh, 'color', 'b');
  end
  
  disp(sprintf('processing beam %d', beams(bn)));
  fname = sprintf(rawFstr, beams(bn));
  fid = localFopen(fname, 'w');
  ipt
  matrixinfo(ipt)
  p = getdata(ipt, bi, ':');
  p = feval(PREC, p);
  fwrite(fid, p, PREC);
  [message errno] = ferror(fid);
  if errno
    error(['Problem writing to file ' fname ': ' message]);
  end
  fclose(fid);

  % STAGE 2
  % Call an external C++ program to do the sliding average. This can take
  % advantage of a faster algorithm for sliding median, and works best on
  % long sets of data

  slidingmedianProg = fullfile(mia_basedir, computer, 'slidingmedian');

  infilename = sprintf(rawFstr, beams(bn));
  % outfilename = sprintf('%s.%d.smoothed', basename, beams(bn));
  outfilename = sprintf(medianFstr, beams(bn));
  unixCommand = sprintf('%s %d %s %s', ...
      slidingmedianProg, medianblocksize, infilename, outfilename)
  [status message] = unix(unixCommand);
  if status ~= 0
    error(['Could not run slidingmedian program: ' message]);
  end
  
  delete(infilename);
  
  
  
  % STAGE 3
  % load the data back, into the correct sidereal time and sort into bins
  bins = 4 % number of bins data is sorted into
  % bin 1 : new data
  % bin 2 : v. large values without absorption
  % bin 3 : maximum values without absorption
  % bin 4 : largest values which will be discarded (probs interefence)
  
  qdcBeamsWithNANS = [];
  
  % filename = sprintf('%s.%d.raw', basename, beams(bn));
  ofid = localFopen(outfilename, 'r');
  
  power = repmat(-inf, [bins qdcSamples]);  
  t = starttime + blockWaste*res
  smoothet = endtime - blockWaste*res;
  
  while t < smoothet
    % offset from start of QDC, in samples
    offset = round(stoffset(t, loc) / res);
    bsz = qdcSamples - offset;
    while bsz == 0
      disp('fixing')
      t = t + res;
      offset = round(stoffset(t, loc) / res);
      bsz = qdcSamples - offset;
    end
    et =  t + bsz * res;
    if et > smoothet
      disp('truncating');
      et = smoothet;
      bsz = floor((smoothet - t) / res);
    end
    tmpPower = fread(ofid, bsz, PREC)';
    [message errno] = ferror(ofid);
    if errno
      fclose(ofid);
      error(['Problem reading from file ' fname ': ' message]);
    end
    % replace NaNs with -inf, this fall downwards out of the sort,
    % instead of rising to the top
    tmpPower(find(isnan(tmpPower))) = -inf;
    power(1, (offset+1):(offset+bsz)) = tmpPower;
%     [message errno] = ferror(ofid);
%     if errno
%       fclose(ofid);
%       error(['Problem reading from file ' fname ': ' message]);
%     end
    t = et;
    disp('sorting');
    power = sort(power, 1); % sort on dimension 1 (bin number)
    disp('sorting done');
  end
  fclose(ofid);
  delete(outfilename);
  r = setbeams(mia, beams(bn));
  r_data = mean(power(2:3, :), 1);
  % r = setdata(r, r_data);
  
  % one sidereal day is 23h 56m 4.09053s. Over a short period (<= 11
  % days) it is possible that the small sub-second part means the
  % offset=0 position is never filled. This remains -inf. Thus -inf
  % values in the first position can be ignored (post-processing should
  % fill it with an appropriate value.
  
  badData = find(isinf(r_data) | isnan(r_data));
  if ~isempty(badData)
    warning(['bad data found in QDC, positions ' printseries(badData)]);
    r_data(badData) = nan;
  end

  if ~isempty(find(isnan(r_data)))
    % give warning at the end when it is more likely to be noticed
    qdcBeamsWithNANS(length(qdcBeamsWithNANS)+1) = beams(bn);
  end
  
  r = setdata(r, r_data);
  r = setstarttime(r, starttime);
  r = setendtime(r, endtime);
  r = setunits(r, units);
  
  if ~isempty(defaults.prefilter)
    r = addprocessing(r, ['preprocessing: ' ...
	  getprocessing(defaults.prefilter, r)]);
  end
  r = addprocessing(r, sprintf('created by %s, block size %d', ...
      mfilename, medianblocksize));

  for n = 1:size(defaults.exclude, 1)
    r = addprocessing(r, ['excluded data: ', ...
		    dateprintf(defaults.exclude(n, 1), ...
			       defaults.exclude(n, 2))]);
  end
  % save(sprintf('beam%d', beams(bn)), 'r');
  if ~isempty(defaults.postfilter)
    disp('postfiltering');    
    r = filter(defaults.postfilter, r);
  end
  
  % if ~isempty(miafilename)
  % save(r, miafilename);
  % else
  % save(r);
  % end  
  % save(r, {strftime(starttime, filename{bn})});
  save(r, beamstrftime(starttime, bns, filename));
end

% load up from disk
%  this currently loads up the one using standard times, which may not be
%  correct if non-standard start/end times have been used.
if nargout

  r = feval(defaults.qdcclass, ...
   	    'starttime', starttime, ...
   	    'endtime', endtime, ...
	    'resolution', res, ...
	    'instrument', in, ...
	    'beams', beams, ...
	    'filename', filename);
end


if ~isempty(qdcBeamsWithNANS)
  warning(['QDC contains NaNs in beams ' printseries(qdcBeamsWithNANS)]);
end
disp(['time taken: ' char(timestamp('now') - functionStartTime)]);
return;


% -------------------------------------------
function fid = localFopen(filename, varargin)
[fid message] = fopen(filename, varargin{:});
if fid == -1
  error(['Could not open file ' filename ': ' message]);
end


