function rout = slidingmedian2qdc4(mia, varargin)
%
%   r = SLIDINGMEDIAN2QDC3(mia, ...)
%   r:  completed QDC
%   mia: basic information for QDC, as a QDC object
%

% was slidingmedian2qdc3 in iristool

r = [];
rout = {};

PREC = 'double';
functionStartTime = timestamp('now');

in = getinstrument(mia);

defaults.qdcperiodstarttime = [];
defaults.prefilter = [];

% Before postfiltering convert the QDC object into an FFT version, with
% selected fitorder, if fitorder is non-zero. If fitorder is empty (or nan)
% then use the default fitorder given by calcnumrioqdcfftcoeffs. fitorder
% can be either scalar or a vector the same size as beams.
defaults.fitorder = 0;
defaults.preserveoriginalqdc = 1;

defaults.postfilter = [];

defaults.resolution = [];
defaults.resolutionmethod = '';

defaults.archive = '';

defaults.medianwindowsize = timespan(599,'s');
defaults.exclude = [];
defaults.failiffilemissing = 0;
defaults.qdcclass = class(mia);
defaults.dataquality = 'Preliminary';
% number of beams to load at once, assuming 1s res data
switch hostname
 case 'dcs-tesla'
  % defaults.beamsatonce = 25;
  defaults.beamsatonce = 17;
 case 'dcs-aurora'
  % defaults.beamsatonce = 17;
  defaults.beamsatonce = 13;
 otherwise
  defaults.beamsatonce = 10;
end

defaults.beamsatonce = floor(gettotalseconds(info(in, 'bestresolution')) ...
			     * defaults.beamsatonce);

% allow user to construct QDC from ready-made power data
defaults.data = [];
[defaults unvi] = interceptprop(varargin, defaults);

switch defaults.qdcclass
 case 'rio_rawqdc'
  powerclass = 'rio_rawpower';
  
 case 'rio_qdc'
  powerclass = 'rio_power';
  
 otherwise
  error(sprintf('unknown riometer QDC class (was %s)', ...
		defaults.qdcclass));
end

if isempty(defaults.qdcperiodstarttime)
  % if the starttime is not the start of the actual QDC period then need
  % to know what period it is supposed to be
  warning('qdcperiodstarttime not given, using starttime instead');
end


% print debug code ------
defaults.prefilter
defaults.postfilter
size(defaults.exclude)
% print debug code (end) ------

[tmp mfname] = fileparts(mfilename);
basename = fullfile(tempdir, [mfname '.' int2str(getpid)]);

starttime = getstarttime(mia);
endtime = getendtime(mia);
loc = getlocation(in);

if isempty(defaults.resolution)
  defaults.resolution = info(in, 'bestresolution');
end


qdcSamples = ceil(siderealday / defaults.resolution);

beams = getbeams(mia);
numOfBeams = numel(beams);

filename = getfilename(mia);
if isempty(filename)
  % filename = info(in, 'defaultfilename', mia);
  s = info(in, 'defaultfilename', mia, 'archive', defaults.archive);
  filename = s.fstr;
end


medianblocksize = floor(defaults.medianwindowsize / defaults.resolution);
if rem(medianblocksize, 2) == 0
  % medianblocksize is even, should be odd
  medianblocksize = medianblocksize + 1;
end

disp(sprintf('medianblocksize: %d', medianblocksize));

blockWaste = floor(medianblocksize/2);


autofillIfilqdcmeansd = 0;
if ~isempty(defaults.prefilter)
  if isa(defaults.prefilter, 'ifilqdcmeansd')
    autofillIfilqdcmeansd = isempty(getqdcmeansd(defaults.prefilter));
    autofillIfilqdcmeansdLimits = getlimits(defaults.prefilter);
    % need extra copies of mean and SD of QDC
  end
end

% format string for raw files

rawFstr = [basename, '.raw.%d'];
medianFstr =  [basename, '.median.%d'];

units = [];
bao = defaults.beamsatonce; % shorten the name

if isempty(defaults.fitorder)
  defaults.fitorder = nan;
end
if length(defaults.fitorder) == 1
  defaults.fitorder = repmat(defaults.fitorder, size(beams));
end
  
disp(sprintf('Loading %d beam(s) simultaneously', bao));
iptTmpName = [];

for bg = 1:ceil(numOfBeams./bao)
  % bnv are the indexes into the beams matrix. It may be a vector. Later the
  % beams are processed singularly, and bns is used to identify that as a
  % scalar
  bnv = unique(min((((bg-1) * bao)+1):bg*bao, numOfBeams));

  % STAGE 1
  % load all the data for the beam(s)

  if ~isempty(defaults.prefilter)
    if autofillIfilqdcmeansd
      % fill only with the beam needed, to keep memory requirements down
      disp('autofilling ifilqdcmeansd');
      defaults.prefilter = ...
	  ifilqdcmeansd(irisqdcmeansd(starttime, in, beams(bnv)), ...
			autofillIfilqdcmeansdLimits(1), ...
			autofillIfilqdcmeansdLimits(2));
    end
  end

  % disp(['IPT LOADING ' dateprintf(starttime, endtime)]);
  % ipt = rio_power('starttime', starttime, ...
  if isempty(defaults.data)
    ipt = feval(powerclass, ...
		'starttime', starttime, ...
		'endtime', endtime, ...
		'instrument', in, ...
		'beams', beams(bnv), ...
		'log', 0, ...
		'load', 1, ...
		'loadoptions', ...
		{'failiffilemissing', defaults.failiffilemissing, ...
		'resolution', defaults.resolution, ...
		 'resolutionmethod', defaults.resolutionmethod, ...
		 'archive', defaults.archive});
  else
    if getinstrument(defaults.data) ~= in
      error('supplied data does not match required instrument');
    elseif ~isa(defaults.data, powerclass)
      error(sprintf(['data is incorrect type (require ''%s'' but have ''' ...
		     ' %s'')'], powerclass, class(defaults.data)));
    end
    ipt = extract(defaults.data, ...
		  'starttime', starttime, ...
		  'endtime', endtime, ...
		  'beams', beams(bnv));
    ipt = setresolution(ipt, defaults.resolution, defaults.resolutionmethod);
  end
    
  % keep a copy of the data loaded in case things go wrong
  if isempty(iptTmpName)
    iptTmpName = tempname;
  end
  if 0
    disp(sprintf('saving power data to temporary file %s', iptTmpName));
    eval('save(iptTmpName, ''ipt'');', 'disp(''save failed (ignored)'');');
  end
  
  if any(strmatch(class(getdata(ipt)), ...
		  {'int8' 'int16' 'int32' 'uint8' 'uint16' 'uint32'}))
    data = getdata(ipt);
    data(data == 0) = nan;
    ipt = setdata(ipt, data);
  end
  bi = getparameterindex(ipt, beams(bnv));

  if ~isempty(getunits(ipt))
    if isempty(units)
      units = getunits(ipt);
    elseif ~strcmp(units, getunits(ipt))
      error(sprintf('units were previously %s and are now %s', ...
		    units, getunits(ipt)));
    end
  end
  
  % check if any data should be excluded
  if ~isempty(defaults.exclude)
    % work through all rows (all exclusion periods)
    for er = 1:size(defaults.exclude, 1) % er = exclude row number
      disp(['checking ' dateprintf(defaults.exclude(er, 1), ...
				   defaults.exclude(er, 2))]); 

      % find common time (if any)
      excStartTime = ceil(max(starttime, defaults.exclude(er,1)), ...
			  defaults.resolution);
      excEndTime = floor(min(endtime, defaults.exclude(er,2)), ...
			 defaults.resolution);
      
      if excEndTime > excStartTime
	% exclude using this rule
	starttime
	endtime
	excStartTime
	excEndTime
	% time indices
	ti = fix([round((excStartTime - starttime)./ defaults.resolution):...
		  round((excEndTime - starttime)./ defaults.resolution)]);  
	
	% some debugging info
	disp('SLIDINGMEDIAN2QDC debugging info:');
	disp(['bi: ', printseries(bi)]);
	disp(['ti: ', printseries(ti)]);
	%  one final sanity check, which should always pass
	if ~isempty(ti)
	  % if ti is empty then all elements are set to nan - not right!
	  disp(sprintf('Excluding data for beam(s) %s: %s (%s)', ...
		       printseries(beams(bnv)), ...
		       dateprintf(excStartTime, excEndTime), ...
		       printseries(ti)));
	  % ipt = setdata(ipt, bi, ti, nan)
	  iptData = getdata(ipt);
	  iptData(bi, ti) = nan;
	  ipt = setdata(ipt, iptData);
	  clear iptData;
	end
      end
    end
  end
  
  % pre-filter, if set
  if ~isempty(defaults.prefilter)
    % [fh gh] = plot(ipt, 'color', 'r'); hold on;
    disp('filtering ...');
    ipt = filter(defaults.prefilter, ipt);
    disp('done');
    % plot(ipt, 'plotaxes', gh, 'color', 'b');
  end
  
  
  % having loaded and filtered the data now process beam by beam
  % for bns = bnv
  for bnsn = 1:length(bnv)
    bns = bnv(bnsn);
    
    disp(sprintf('processing beam %d', beams(bns)));
    fname = sprintf(rawFstr, beams(bns));
    fid = localFopen(fname, 'w');

    p = getdata(ipt, bi(bnsn), ':');
    p = feval(PREC, p);
    fwrite(fid, p, PREC);
    [message errno] = ferror(fid);
    if errno
      error(['Problem writing to file ' fname ': ' message]);
    end
    fclose(fid);

    % STAGE 2
    % Call an external C++ program to do the sliding average. This can take
    % advantage of a faster algorithm for sliding median, and works best on
    % long sets of data

    slidingmedianProg = fullfile(mia_basedir, 'bin', ...
				 ['slidingmedian_' lower(computer)]);

    infilename = sprintf(rawFstr, beams(bns));
    % outfilename = sprintf('%s.%d.smoothed', basename, beams(bns));
    outfilename = sprintf(medianFstr, beams(bns));
    cmd = sprintf('%s %d %s %s', ...
			  slidingmedianProg, medianblocksize, ...
			  infilename, outfilename)
    [status message] = system(cmd);
    if status ~= 0
      error(['Could not run slidingmedian program: ' message]);
    end
    
    delete(infilename);
    
    
    
    % STAGE 3
    % load the data back, into the correct sidereal time and sort into bins
    bins = 4 
    % number of bins data is sorted into
    % bin 1 : new data
    % bin 2 : v. large values without absorption
    % bin 3 : maximum values without absorption
    % bin 4 : largest values which will be discarded (probs interefence)
    
    qdcBeamsWithNANS = [];
    
    % filename = sprintf('%s.%d.raw', basename, beams(bns));
    ofid = localFopen(outfilename, 'r');
    
    power = repmat(-inf, [bins qdcSamples]);  
    t = starttime + blockWaste*defaults.resolution
    smoothet = endtime - blockWaste*defaults.resolution;
    
    while t < smoothet
      % offset from start of QDC, in samples
      offset = round(stoffset(t, loc) / defaults.resolution);
      bsz = qdcSamples - offset;
      while bsz == 0
	disp('fixing')
	t = t + defaults.resolution;
	offset = round(stoffset(t, loc) / defaults.resolution);
	bsz = qdcSamples - offset;
      end
      et =  t + bsz * defaults.resolution;
      if et > smoothet
	disp('truncating');
	et = smoothet;
	bsz = floor((smoothet - t) / defaults.resolution);
      end
      tmpPower = fread(ofid, bsz, PREC)';
      [message errno] = ferror(ofid);
      if errno
	fclose(ofid);
	error(['Problem reading from file ' fname ': ' message]);
      end
      % replace NaNs with -inf, they fall downwards out of the sort,
      % instead of rising to the top
      tmpPower(find(isnan(tmpPower))) = -inf;
      power(1, (offset+1):(offset+bsz)) = tmpPower;
      t = et;
      disp('sorting');
      power = sort(power, 1); % sort on dimension 1 (bin number)
      disp('sorting done');
    end
    fclose(ofid);
    delete(outfilename);
    r = setbeams(mia, beams(bns));
    r_data = mean(power(2:3, :), 1);
    % r = setdata(r, r_data);
    
    % one sidereal day is 23h 56m 4.09053s. Over a short period (<= 11
    % days) it is possible that the small sub-second part means the
    % offset=0 position is never filled. This remains -inf. Thus -inf
    % values in the first position can be ignored (post-processing should
    % fill it with an appropriate value.
    if r_data(1) == -inf & all(isfinite(r_data([1 end])))
      warning(['Short QDC period meant first sample was never filled with' ...
	       ' data. Interpolating.']);
      r_data(1) = mean(r_data([1 end]))
    elseif r_data(end) == -inf & all(isfinite(r_data([end-1 1])))
      % could probably happen at the end (instead, not simultaneously)
      warning(['Short QDC period meant last sample was never filled with' ...
	       ' data. Interpolating.']);
      r_data(end) = mean(r_data([end-1 1]))
    end
    
    
    badData = find(isinf(r_data) | isnan(r_data));
    if ~isempty(badData)
      warning(['bad data found in QDC, positions ' printseries(badData)]);
      r_data(badData) = nan;
    end

    if ~isempty(find(isnan(r_data)))
      % give warning at the end when it is more likely to be noticed
      qdcBeamsWithNANS(length(qdcBeamsWithNANS)+1) = beams(bns);
    end
    
    r = setdata(r, r_data);
    r = setstarttime(r, starttime);
    r = setendtime(r, endtime);
    r = setcreatetime(r, timestamp('now'));
    r = setunits(r, units);
    
    r = setsampletime(r, (defaults.resolution/2):defaults.resolution:...
			     siderealday);
    r = setintegrationtime(r, repmat(defaults.resolution, 1, qdcSamples));
    
    if ~isempty(defaults.prefilter)
      r = addprocessing(r, ['preprocessing: ' ...
		    getprocessing(defaults.prefilter, r)]);
    end
    r = addprocessing(r, sprintf('created by %s, block size %d', ...
				 mfilename, medianblocksize));

    for n = 1:size(defaults.exclude, 1)
      r = addprocessing(r, ['excluded data: ', ...
		    dateprintf(defaults.exclude(n, 1), ...
			       defaults.exclude(n, 2))]);
    end
    % save(sprintf('beam%d', beams(bns)), 'r');

    % if isempty(defaults.fitorder) | ~isequal(defaults.fitorder, 0) ...
    %   | ~isequal(defaults.fitorder(bns), 0)
    
    fitorder = defaults.fitorder(bns);
    if isnan(fitorder)
      fitorder = calcnumrioqdcfftcoeffs(in, ...
					'beams', beams(bns), ...
					'resolution', defaults.resolution);
    end
    if fitorder ~= 0
      % convert to the FFT version of the QDC

      % allow for the chance to have rio_rawqdc_fft as well as
      % rio_qdc_fft
      r = feval([defaults.qdcclass '_fft'], r, ...
		'fitorder', fitorder, ...
		'preserveoriginalqdc', defaults.preserveoriginalqdc);
    end
    
    if ~isempty(defaults.postfilter)
      disp('postfiltering');    
      r = filter(defaults.postfilter, r);
    end

    % set data quality
    if ~isempty(defaults.dataquality)
      r = adddataquality(r, defaults.dataquality);
    end
    
    if nargout
      rout{end+1} = r;
    end

    % save the object as is, to the filename we were given. If the QDC has
    % been converted to ts FFT variant then save it as that. When loading
    % mia_base/loaddata checks the class of data it actually has
    % (using class(), not isa()!) and converts to the desired class if
    % necessary, so it should be perfectly safe to store rio_qdc_fft data
    % interspersed with rio_qdc data.
    
    save(r, beamstrftime(defaults.qdcperiodstarttime, beams(bns), filename));
    
  end
  
end

% no need for temporary data now
delete(iptTmpName);

% % load up from disk
% %  this currently loads up the one using standard times, which may not be
% %  correct if non-standard start/end times have been used.
% if nargout

%   r = feval(defaults.qdcclass, ...
%    	    'starttime', starttime, ...
%    	    'endtime', endtime, ...
% 	    'resolution', res, ...
% 	    'instrument', in, ...
% 	    'beams', beams, ...
% 	    'filename', filename);
% end

% ### DEBUG
% should perhaps do something to rout to get just one object



if ~isempty(qdcBeamsWithNANS)
  warning(['QDC contains NaNs in beams ' printseries(qdcBeamsWithNANS)]);
end
disp(['time taken: ' char(timestamp('now') - functionStartTime)]);


warning(['This function does not properly set the integrationtime and' ...
	 ' sampletime fields!!! Rewrite!']);
return;


% -------------------------------------------
function fid = localFopen(filename, varargin)
[fid message] = fopen(filename, varargin{:});
if fid == -1
  error(['Could not open file ' filename ': ' message]);
end


