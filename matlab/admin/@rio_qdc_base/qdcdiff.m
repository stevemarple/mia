function r = qdcdiff(mia1, mia2)
%QDCDIFF  Report on differences between 2 QDCs
%
%   QDCDIFF(mia1, mia2)
%   mia1: RIO_QDC_BASE object
%   mia2: RIO_QDC_BASE object
%
% QDCDIFF reports on the differences between 2 QDCs.

d1 = getdata(mia1);
d2 = getdata(mia2);

dif = d1 - d2;

disp(sprintf('QDCs identical              :  %d', ...
	     isequal(mia1, mia2)));
disp(sprintf('QDCs have identical data    :  %d', ...
	     nonanisequal(getdata(mia1), getdata(mia2))));

disp(sprintf('Maximum absolute difference :  %f', max(abs(dif))));
disp(sprintf('Mean absolute difference    :  %f', mean(abs(dif))));
disp(sprintf('RMS difference              :  %f', ...
	     sqrt(mean(power(dif, 2)))));



