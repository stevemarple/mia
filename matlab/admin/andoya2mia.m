function mia = andoya2mia(filename, varargin)

defaults.save = 0;
defaults.recursive = 0;
defaults.ignoreerrors = 0;
[defaults univi] = interceptprop(varargin, defaults);


[Path file ext] = fileparts(filename);
if isempty(Path) | ~strcmp(Path(1), filesep)
  Path = fullfile(pwd, Path);
end

if isdir(filename)
  mia = [];
  % filename = fullfile(Path, [file ext]);
  Path = fullfile(Path, [file ext]);
  d = dir(filename);
  cmd = sprintf('%s(fn, varargin{:});', mfilename);
  for n = 1:length(d)
    fn = fullfile(Path, d(n).name);
    err = 0;
    if ~d(n).isdir
      % feval(mfilename, fullfile(Path, d(n).name), varargin{:});
      eval(cmd, 'err = 1;');
    elseif logical(defaults.recursive) & ...
	  (~strcmp(d(n).name, '.') & ~strcmp(d(n).name, '..'))
      % don't do current or parent directories
      % feval(mfilename, fullfile(Path, d(n).name), varargin{:});
      % fn = fullfile(Path, d(n).name);
      eval(cmd, 'err = 1;');
    end
    if err 
      disp(sprintf('error processing %s: %s', fn, lasterr));
      if ~defaults.ignoreerrors | strcmp(lasterr, 'Interrupt')
	error(lasterr);
      end
    end

  end
  return;
end

disp(sprintf('processing %s', filename));


[data ts h settings] = plw2mat(filename);

p.starttime = timestamp([1 1 1 0 0 0]) + ...
    timespan(h.start_date, 'd', h.start_time, 's');
p.resolution = timespan(h.interval, h.interval_units_str);
p.endtime = p.starttime + h.max_samples * p.resolution;
p.instrument = rio_and_3; % Lancaster widebeam riometer
p.data = repmat(nan, 1, (p.endtime - p.starttime)/p.resolution);
p.data(ts+1) = data;
[ib wb] = info(p.instrument, 'beams');
p.beams = sort([ib wb]);

% map any zeros to nan
p.data(p.data == 0) = nan;


if 0
  cls = 'rio_power';
  p.units = 'dBm';
else
  % raw power
  cls = 'rio_rawpower';
  p.units = 'V';
end

pc = makeprop(p);
newdata = feval(cls, pc{:}, 'units', 'V', 'load', 0);

% load up any old data (if present)


if logical(defaults.save)
  p2 = rmfield(p, {'data', 'units', 'starttime', 'endtime'});
  p2c = makeprop(p2);
  % [tmp filedur] = info(p.instrument, 'defaultfilename', cls);
  s = info(p.instrument, 'defaultfilename', cls);
  
  olddata = feval(cls, p2c{:}, ...
		  'starttime', floor(p.starttime, s.duration), ...
		  'endtime', ceil(p.endtime, s.duration), ...
		  'units', p.units, ...
		  'load', 1, ...
		  'log', 0);

  mia = insert(olddata, newdata);
  
  % check if data finishes at the correct boundary (may not when
  % inserting new data)
  et = getendtime(mia);
  if rem(et, s.duration) ~= timespan(0, 's')
    % need to pad to end
    et2 = ceil(et, s.duration);
    mia = insert(feval(cls, ...
		       'starttime', et, ...
		       'endtime', et2, ...
		       'resolution', p.resolution, ...
		       'instrument', p.instrument, ...
		       'beams', p.beams, ...
		       'data', repmat(nan, length(p.beams), ...
				      (et2-et)/p.resolution), ...
		       'units', getunits(mia)), ...
		 mia);
    mia
  end
  save(mia);
else
  mia = newdata
end
