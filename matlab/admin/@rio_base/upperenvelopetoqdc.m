function r = upperenvelopetoqdc(mia, varargin)
%UPPERENVELOPETOQDC Convert riometer data into a quiet-day curve (QDC).
%
%   r = UPPERENVELOPETOQDC(mia, ...)
%   r: quiet day curve
%   mia: RIO_POWER or RIO_RAWPOWER object 
%
% UPPERENVELOPETOQDC converts riometer data (from a single beam only) into a
% quiet day curve. Data is binned into sidereal times and sorted. Typically
% the highest samples are discarded and the output is the mean of the second
% and third largest values. To obtain satisfactory performance the data
% should be prefiltered with a sliding median filter (see
% MIA_FILTER_SLDING_AVERAGE).
%
% The default behaviour can be modified with the following name/value
% pairs:
%
%   'qdcclass', CHAR
%   The class of the return object. Must be specified.
%
%   'qdcresolution', TIMESPAN
%   The temporal resolution at which the QDC is calculated.
%
%   'outputrows', NUMERIC
%   The rows used of sorted binned data used for the QDC
%   calculation. Default is [2 3] so that some interference is ignored.
%
% UPPERENVELOPETOQDC is normally called by MAKEQDC.
%
% See also MAKEQDC, MIA_FILTER_SLDING_AVERAGE.

beam = getbeams(mia);
if numel(beam) > 1
  mia
  error('mia must be data from a single beam');
end

% standard parameters
defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcperiodstarttime = [];
defaults.qdcperiodendtime = [];

% parameters specific to this method
defaults.outputrows = [2 3];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);


if isempty(defaults.qdcresolution)
  defaults.qdcresolution = info(rio, 'bestresolution');
end

% remove any samples which are nan
mia = removemissingdata(mia);

% convert sample times to sidereal time
sampletime = getsampletime(mia);
[sidt sidd] = stoffset(sampletime, getlocation(rio), sampletime(1));

% we want first sidereal day to be #1
sidd = sidd + 1; 

% convert sidereal times to index in binned data (counting from 1)
sidi = 1 + round(sidt ./ defaults.qdcresolution);


% bin the data according to sidereal time
qdcsamples = floor(siderealday ./ defaults.qdcresolution);

% preallocate some space for the binned data
binned = repmat(-inf, max(max(sidd), max(defaults.outputrows(:))), ...
                qdcsamples);

% Get the data. Convert NaNs and +/- Inf to -inf. This will ensure they are
% discarded in the sorting process.
data = getdata(mia);
data(~isfinite(data)) = -inf; 

% insert each sidereal day's data into the bins
for n = unique(sidd)
  idx = find(sidd == n);
  binned(n, sidi(idx)) = data(1, idx);
end

% sort the data (ascending order)
binned = flipud(sort(binned, 1));


% Hopefully there isn't any missing data in the QDC, but if so make it
% clear it is missing
binned(binned == -inf) = nan;

% Take the mean values from the selected rows in the sorted binned data
qdcdata = nonanmean(binned(defaults.outputrows, 1:qdcsamples), 1);
qdcsampletime = [0.5:qdcsamples] * defaults.qdcresolution;

% Compute integration time
integrationtime = getintegrationtime(mia);
integrationtime(~isfinite(integrationtime)) = [];
qdcintegrationtime = repmat(median(integrationtime), size(qdcsampletime));

r = feval(defaults.qdcclass, ...
	  'instrument', rio, ...
	  'starttime', getstarttime(mia), ...
	  'endtime', getendtime(mia), ...
	  'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	  'qdcperiodendtime', defaults.qdcperiodendtime, ...
          'beams', beam, ...
	  'data', qdcdata, ...
	  'units', getunits(mia), ...
	  'sampletime', qdcsampletime, ...
	  'integrationtime', qdcintegrationtime, ...
	  'processing', getprocessing(mia));

r = addprocessing(r, sprintf('Created by %s with outputrows=%s', ...
                             basename(mfilename), ...
                             printseries(defaults.outputrows)));


