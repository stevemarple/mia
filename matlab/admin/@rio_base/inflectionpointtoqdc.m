function r = inflectionpointtoqdc(mia, varargin)

beam = getbeams(mia);
if numel(beam) > 1
  mia
  error('mia must be data from a single beam');
end

% standard parameters
defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcperiodstarttime = [];
defaults.qdcperiodendtime = [];

% parameters specific to this method
defaults.powerbins = 4000;
defaults.integerpowerbins = [];
defaults.windowsize = timespan(599, 's');

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);

if isempty(defaults.qdcresolution)
  defaults.qdcresolution = info(rio, 'bestresolution');
end

% remove any samples which are nan
mia = removemissingdata(mia);

% convert sample times to sidereal time
sampletime = getsampletime(mia);
[sidt sidd] = stoffset(sampletime, getlocation(rio), sampletime(1));

% we want first sidereal day to be #1
sidd = sidd + 1; 

% convert sidereal times to index in binned data (counting from 1)
sidi = round(1 + round(sidt ./ defaults.qdcresolution));


% bin the data according to sidereal time
qdcsamples = floor(siderealday ./ defaults.qdcresolution);

% Find range of data
data = getdata(mia);
range = [min(data(:)) max(data(:))];

dataclass = class(data);
switch dataclass
 case {'single' 'double'}
  if strcmp(getunits(mia), 'dBm')
    step = 0.05;
  else
    step = diff(range) ./ defaults.powerbins;
  end
  
 case {'int8' 'int16' 'int32' 'uint8' 'uint16' 'uint32'}
  if isempty(defaults.integerpowerbins)
    step = 1;
  else
    step = diff(range) ./ defaults.integerpowerbins;
  end
 otherwise
  error(sprintf('unknown class (was ''%s'')', dataclass));
end

binrange = [floor2(range(1), step) ceil2(range(2), step)];
powerbins = round(diff(binrange) ./ step) + 1;


% Round the data down to the edge of the bin
powerbinidx = round(1 + ((floor2(data, step) - binrange(1)) ./ step));

clear data

% Create a sparse matrix to hold the binned data.
binned = sparse(powerbinidx, sidi, ones(getdatasize(mia)));

% Trim unwanted column
binned_cols = floor(siderealday ./ defaults.qdcresolution)
binned = binned(:, 1:binned_cols);

qdcdata = zeros(1, binned_cols);

% Aggregate columns to get a cleaner distribution, find maximum and then
% find upper inflection point
ws_2 = round(defaults.windowsize ./ (2 * defaults.qdcresolution));


for n = 1:binned_cols
  idx = (n-ws_2):(n+ws_2);
  if idx(1) < 1
    idx(idx < 1) = idx(idx < 1) + binned_cols;
  elseif idx(end) > binned_cols
    idx(idx > binned_cols) = idx(idx > binned_cols) - binned_cols;
  end

  distrib = full(sum(binned(:, idx), 2));
  [peak peakidx] = max(distrib);
  [ipgrad ipidx] = min(diff(distrib(peakidx:end)));
  ipidx = ipidx + peakidx - 1;
  if 0
  disp('---')
  size(qdcdata)
  size(n)
  disp(n)
  ipidx
  QDCDAT = binrange(1) + (ipidx - 0.5) * step
  assignin('base','distrib',distrib);
  end
  if isempty(ipidx)
    qdcdata(n) = nan;
    % qdcdata(n) = binrange(1) + (peakidx - 0.5) * step;
  else
    qdcdata(n) = binrange(1) + (ipidx - 0.5) * step;
  end
end

qdcsampletime = [0.5:binned_cols] * defaults.qdcresolution;

whos
% Compute integration time
integrationtime = getintegrationtime(mia);
integrationtime(~isfinite(integrationtime)) = [];
qdcintegrationtime = repmat(median(integrationtime), size(qdcsampletime));


r = feval(defaults.qdcclass, ...
	  'instrument', rio, ...
	  'starttime', getstarttime(mia), ...
	  'endtime', getendtime(mia), ...
	  'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	  'qdcperiodendtime', defaults.qdcperiodendtime, ...
          'beams', beam, ...
	  'data', qdcdata, ...
	  'units', getunits(mia), ...
	  'sampletime', qdcsampletime, ...
	  'integrationtime', qdcintegrationtime, ...
	  'processing', getprocessing(mia));

r = addprocessing(r, ...
		  sprintf('Created by %s with windowsize=%s', ...
			  basename(mfilename), ...
			  char(defaults.windowsize)));

r

% step
% disp('ASSIGNIN');
% assignin('base','distrib',distrib);
% assignin('base','binned',binned);

