function r = chauvenetupperenvelopetoqdc(mia, varargin)
%CHAUVENETUPPERENVELOPETOQDC Convert riometer data to a quiet-day curve (QDC).
%
%   r = CHAUVENETUPPERENVELOPETOQDC(mia, ...)
%   r: quiet day curve
%   mia: RIO_POWER or RIO_RAWPOWER object 
%
% CHAUVENETUPPERENVELOPETOQDC converts riometer data (from a single beam
% only) into a quiet day curve. Data is binned into sidereal times and the
% MODE calculated for each sidereal time. As the data forms a continuous
% function the simple method of counting how many times each value occurs is
% not useful, so other approaches are employed.

% The default behaviour can be modified with the following name/value
% pairs:
%
%   'qdcclass', CHAR
%   The class of the return object. Must be specified.
%
%   'qdcresolution', TIMESPAN
%   The temporal resolution at which the QDC is calculated.
%
%   'chauvenetwindowsize', TIMESPAN
%   The window size used when applying Chauvenet's criterion (see
%   CHAUVENET). The number of coulmns used is always odd and never less
%   than one. 
%
%   'outputrows', NUMERIC
%   The rows used of sorted binned data used for the QDC
%   calculation. Default is [2 3] so that some interference is ignored.
%
% CHAUVENETUPPERENVELOPETOQDC is normally called by MAKEQDC.
%
% See also MAKEQDC, MIA_FILTER_SLDING_AVERAGE, CHAUVENET.

beam = getbeams(mia);
if numel(beam) > 1
  mia
  error('mia must be a single beam');
end

defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcperiodstarttime = [];
defaults.qdcperiodendtime = [];

defaults.chauvenetwindowsize = timespan(2,'m');
defaults.outputrows = [2 3];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);


if isempty(defaults.qdcresolution)
  defaults.qdcresolution = info(rio, 'bestresolution');
end

% ensure the data is at regular intervals
mia = spaceregularly(mia, 'resolution', defaults.qdcresolution);

% remove any samples which are nan
mia = removemissingdata(mia);

% convert sample times to sidereal time
sampletime = getsampletime(mia);
[sidt sidd] = stoffset(sampletime, getlocation(rio), sampletime(1));

% we want first sidereal day to be #1
sidd = sidd + 1; 

% convert sidereal times to index in binned data (counting from 1)
sidi = 1 + round(sidt ./ defaults.qdcresolution);


% bin the data according to sidereal time
qdcsamples = floor(siderealday ./ defaults.qdcresolution);

% preallocate some space for the binned data
binned = repmat(nan, max(sidd), qdcsamples);

binned2 = binned;

% Get the data. Map any bad values to nan
data = getdata(mia);
data(~isfinite(data)) = nan; 

for n = unique(sidd)
  idx = find(sidd == n);
  binned(n, sidi(idx)) = data(1, idx);
end
clear data;

% calculate windowsize in terms of columns to use
wsz = round(defaults.chauvenetwindowsize ./ defaults.qdcresolution);
if rem(wsz, 2) == 0
  wsz = wsz - 1;
end
wsz = max(1, wsz); % have to use at least one sample

qdcdata = repmat(nan, 1, qdcsamples);

for n = 1:qdcsamples
  if wsz == 1
    idx = n;
  else
    idx = (n-(wsz-1)./2):(n+(wsz-1)./2);
    idx(idx <= 0) = idx(idx <= 0) + qdcsamples;
    idx(idx > qdcsamples) = idx(idx > qdcsamples) - qdcsamples;
  end
  
  % Apply Chauvenet's criterion to each point in turn. Don't put the
  % modified values back into the binned data
  d = chauvenet(binned(:, idx));
  
  % put the column we cleaned up back into a second set of bins
  binned2(:, n) = d(:, floor(wsz/2) + 1);
end

clear binned;

% sort the data (ascending order)
binned2 = flipud(sort(binned2, 1));

% Hopefully there isn't any missing data in the QDC, but if so make it
% clear it is missing
binned2(binned2 == -inf) = nan;

% Take the mean values from the selected rows in the sorted binned data
qdcdata = nonanmean(binned2(defaults.outputrows, 1:qdcsamples), 1);
qdcsampletime = [0.5:qdcsamples] * defaults.qdcresolution;

% Compute integration time
integrationtime = getintegrationtime(mia);
integrationtime(~isfinite(integrationtime)) = [];
qdcintegrationtime = repmat(median(integrationtime), size(qdcsampletime));

r = feval(defaults.qdcclass, ...
	  'instrument', rio, ...
	  'starttime', getstarttime(mia), ...
	  'endtime', getendtime(mia), ...
	  'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	  'qdcperiodendtime', defaults.qdcperiodendtime, ...
          'beams', beam, ...
	  'data', qdcdata, ...
	  'units', getunits(mia), ...
	  'sampletime', qdcsampletime, ...
	  'integrationtime', qdcintegrationtime, ...
	  'processing', getprocessing(mia));

r = addprocessing(r, ...
                  sprintf(['Created by %s with chauvenetwindowsize=%s ', ...
                    'outputrows=%s'], ...
                          basename(mfilename), ...
                          char(defaults.chauvenetwindowsize), ...
                          printseries(defaults.outputrows)));
