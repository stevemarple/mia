function r = aggregateffttoqdc(mia, varargin)
%UPPERENVELOPETOQDC Convert riometer data into a quiet-day curve (QDC).
%
%   r = AGGREGATEFFTTOQDC(mia, ...)
%   r: quiet day curve
%   mia: RIO_POWER or RIO_RAWPOWER object 
%
% AGGREGATEFFTTOQDC converts riometer data (from a single beam only) into a
% quiet day curve. 
%
% The default behaviour can be modified with the following name/value
% pairs:
%
%   'qdcclass', CHAR
%   The class of the return object. Must be specified.
%
%   'qdcresolution', TIMESPAN
%   The temporal resolution at which the QDC is calculated.
%
%   'outputrows', NUMERIC
%   The rows used of sorted binned data used for the QDC
%   calculation. Default is [2 3] so that some interference is ignored.
%
% AGGREGATEFFTTOQDC is normally called by MAKEQDC.
%
% See also MAKEQDC.

beam = getbeams(mia);
if numel(beam) > 1
  mia
  error('mia must be data from a single beam');
end

% standard parameters
defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcperiodstarttime = [];
defaults.qdcperiodendtime = [];

% parameters specific to this method
% defaults.aggregatefunction = {'aggregateffttoqdc_modal'};
defaults.aggregatefunction = {'aggregateffttoqdc_median'};
defaults.outlierremovalfunction = {}; % eg aggregateffttoqdc_removeworst.m
defaults.maxiterations = 1;
defaults.fitorder = [];
defaults.plot = false;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);

if isempty(defaults.qdcresolution)
  defaults.qdcresolution = info(rio, 'bestresolution');
end

% Append _fft to qdcclass if not already
qdcclass_fft = defaults.qdcclass;
if ~strcmp(qdcclass_fft((end-3):end), '_fft')
  qdcclass_fft = [defaults.qdcclass '_fft'];
end
qdcclass = qdcclass_fft(1:(end-4));

% If function names are just chars convert to cell arrays
if ischar(defaults.aggregatefunction) & ~isempty(defaults.aggregatefunction)
  defaults.aggregatefunction = {defaults.aggregatefunction};
end
if ischar(defaults.outlierremovalfunction) ...
      & ~isempty(defaults.outlierremovalfunction)
  defaults.outlierremovalfunction = {defaults.outlierremovalfunction};
end

if isempty(defaults.fitorder)
  defaults.fitorder = ...
      calcnumrioqdcfftcoeffs(rio, ...
			     'beams', beam, ...
			     'resolution', defaults.qdcresolution);
end

% remove any samples which are nan
mia = removemissingdata(mia);

% convert sample times to sidereal time
sampletime = getsampletime(mia);
[sidt sidd] = stoffset(sampletime, getlocation(rio), sampletime(1));

% we want first sidereal day to be #1
sidd = sidd + 1; 

% convert sidereal times to index in binned data (counting from 1)
sidi = 1 + round(sidt ./ defaults.qdcresolution);


% bin the data according to sidereal time
qdcsamples = floor(siderealday ./ defaults.qdcresolution);

% preallocate space for all of the binned data
binned = repmat(nan, max(sidd), qdcsamples);

% Get the data.
data = getdata(mia);
data(~isfinite(data)) = nan; 

% Insert each sidereal day's data into the bins
for n = unique(sidd)
  idx = find(sidd == n);
  binned(n, sidi(idx)) = data(1, idx);
end

% Trim exccess length off binned
binned(:, (qdcsamples+1):end) = [];


% Items for inclusion in final object
integrationtime = getintegrationtime(mia);
integrationtime(~isfinite(integrationtime)) = [];
qdcsampletime = [0.5:qdcsamples] * defaults.qdcresolution;
qdcintegrationtime = repmat(median(integrationtime), size(qdcsampletime));
proc = getprocessing(mia);
if isempty(defaults.outlierremovalfunction)
  olrf = '<none>';
else
  olrf = defaults.outlierremovalfunction{1};
end
proc{end+1} = sprintf(['Created by %s with ' ...
		    'maxiterations=%d, ' ...
		    'aggregatefunction=%s, ' ...
		    'outlierremovalfunction=%s'], ...
		      basename(mfilename), ...
		      defaults.maxiterations, ...
		      defaults.aggregatefunction{1}, ...
		      olrf);

% initialise aggdata and fitted data to empty matrices
aggdata = [];
fitteddata = [];

cont = true;
for iteration = 1:defaults.maxiterations
  % flush the graphics events queue
  drawnow

  disp(sprintf('iteration %d', iteration));
  % Remove outliers (if specified)
  if ~isempty(defaults.outlierremovalfunction)
    disp(['calling ' defaults.outlierremovalfunction{1}]);
    [binned cont] = feval(defaults.outlierremovalfunction{1}, ...
			  binned, ...
			  fitteddata, ...
			  aggdata, ...
			  iteration, ...
			  defaults.outlierremovalfunction{2:end});
  end
  disp('assignin binned');
  assignin('base', 'binned', binned);
  
  % Convert the multiple samples in each bin to a single value (aggregate)
  disp(['calling ' defaults.aggregatefunction{1}]);
  aggdata = feval(defaults.aggregatefunction{1}, ...
		  binned, ...
		  defaults.aggregatefunction{2:end});

  disp('assignin aggdata');
  assignin('base', 'aggdata', aggdata);

  % Check if the aggregate data contains NaNs, if so use cubic
  % interpolation to remove them
  if any(isnan(aggdata))
    warning(['aggregate data contains nans, ' ...
	     'using cubic interpolation to remove']);
    x = 1:qdcsamples;
    y = aggdata;
    y(isnan(aggdata)) = []; % remove elements which are nan
    x(isnan(aggdata)) = []; % remove corresponding elements in x
			    
    % replicate x,y; NaNs could be at start/end
    x = [x-qdcsamples x x+qdcsamples];
    y = repmat(y, 1, 3);
    aggdata = interp1(x, y, 1:qdcsamples);
  end

  % From the aggregate data calculate the FFT, truncate coefficients (ie in
  % frequency domain) and return to time domain.
  disp('doing FFT');
  ft = fft(aggdata);
  coeffs = ft(1:(defaults.fitorder+1));
  % zero out unwanted coefficients
  ft((defaults.fitorder+2):(end-defaults.fitorder)) = 0;
  
  fitteddata = real(ifft(ft, [], 2));
  
  % %%%%% PLOTTING CODE
  if logical(defaults.plot)
    origqdc = feval(qdcclass, ...
		    'instrument', rio, ...
		    'starttime', getstarttime(mia), ...
		    'endtime', getendtime(mia), ...
		    'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
		    'qdcperiodendtime', defaults.qdcperiodendtime, ...
		    'beams', beam, ...
		    'data', aggdata, ...
		    'units', getunits(mia), ...
		    'sampletime', qdcsampletime, ...
		    'integrationtime', qdcintegrationtime, ...
		    'processing', proc, ...
		    'dataquality', 'Preliminary');
    r = feval(qdcclass_fft, ...
	      'instrument', rio, ...
	      'starttime', getstarttime(mia), ...
	      'endtime', getendtime(mia), ...
	      'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	      'qdcperiodendtime', defaults.qdcperiodendtime, ...
	      'beams', beam, ...
	      'data', fitteddata, ...
	      'units', getunits(mia), ...
	      'sampletime', qdcsampletime, ...
	      'integrationtime', qdcintegrationtime, ...
	      'processing', proc, ...
	      'dataquality', 'Preliminary', ...
	      'originalqdc', origqdc);
    ah = axes('Parent', figure('Name', sprintf('Beam %d, iteration %d', ...
					       beam, iteration)));
    plot(qdcsampletime, binned, 'b.', ...
	 'MarkerSize',1, ...
	 'Parent', ah);
    plot(r, ...
	 'title', maketitle(r, 'comment', ...
			    sprintf('iteration %d', iteration)));
    
  end
  % %%%%% PLOTTING CODE (end)
  
  if ~cont
    % outlierremovalfunction indicated processing should terminate
    break;
  end
end


origqdc = feval(qdcclass, ...
		'instrument', rio, ...
		'starttime', getstarttime(mia), ...
		'endtime', getendtime(mia), ...
		'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
		'qdcperiodendtime', defaults.qdcperiodendtime, ...
		'beams', beam, ...
		'data', aggdata, ...
		'units', getunits(mia), ...
		'sampletime', qdcsampletime, ...
		'integrationtime', qdcintegrationtime, ...
		'processing', proc, ...
		'dataquality', 'Preliminary');

r = feval(qdcclass_fft, ...
	  'instrument', rio, ...
	  'starttime', getstarttime(mia), ...
	  'endtime', getendtime(mia), ...
	  'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	  'qdcperiodendtime', defaults.qdcperiodendtime, ...
          'beams', beam, ...
	  'data', fitteddata, ...
	  'units', getunits(mia), ...
	  'sampletime', qdcsampletime, ...
	  'integrationtime', qdcintegrationtime, ...
	  'processing', proc, ...
	  'dataquality', 'Preliminary', ...
	  'originalqdc', origqdc);


