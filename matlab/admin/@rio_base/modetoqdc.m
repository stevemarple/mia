function r = modetoqdc(mia, varargin)
%MODETOQDC Convert riometer data into a quiet-day curve (QDC).
%
%   r = MODETOQDC(mia, ...)
%   r: quiet day curve
%   mia: RIO_POWER or RIO_RAWPOWER object 
%
% MODETOQDC converts riometer data (from a single beam only) into a
% quiet day curve. Data is binned into sidereal times and the MODE
% calculated for each sidereal time. As the data forms a continuous
% function the simple method of counting how many times each value occurs
% is not useful, so other approaches are employed.

% The default behaviour can be modified with the following name/value
% pairs:
%
%   'qdcclass', CHAR
%   The class of the return object. Must be specified.
%
%   'qdcresolution', TIMESPAN
%   The temporal resolution at which the QDC is calculated.
%
%   'modewindowsize', TIMESPAN
%   The window size used when calculating the mode. The number of coulmns
%   used is always odd and never less than one.
%
%   'modemethod', CHAR ('closest' | 'fixedprecision')
%   The method used to calculate the mode. The default mode,
%   'fixedprecision', calculates the mode by rounding values to a fixed
%   precision, then computing the MODE as normal. The 'closest' method
%   aims to compute the mode by taking the mean of the closest two
%   samples. Behaviour when two or more pairs of values are equally close
%   is not defined.
%
%   'precision', NUMERIC
%   The precision to which the data round are truncated to. Required for
%   modemethod=fixedprecision.
%
%   'ignoreuniformdistribution', LOGICAL
%   When the input values occur only once (ie are uniformly distributed)
%   ignore their value assign the a NAN into the QDC. USe only by the
%   fixedprecision method.
%
% MODETOQDC is normally called by MAKEQDC.
%
% See also MAKEQDC, MIA_FILTER_SLDING_AVERAGE.

beam = getbeams(mia);
if numel(beam) > 1
  mia
  error('mia must be a single beam');
end

defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcperiodstarttime = [];
defaults.qdcperiodendtime = [];

defaults.modewindowsize = timespan(11,'s');
defaults.modemethod = 'fixedprecision';
defaults.precision = [];
defaults.ignoreuniformdistribution = 1;
defaults.overlappingbinwidth = 0.05
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);


if isempty(defaults.qdcresolution)
  defaults.qdcresolution = info(rio, 'bestresolution');
end

switch defaults.modemethod
 case 'closest'
  ; % no special requirements
  
 case 'fixedprecision'
  if isempty(defaults.precision) 
    error('precision must be specified');
  end
 case 'overlappingbins'
  if isempty(defaults.precision) 
    error('precision must be specified');
  end
  if isempty(defaults.overlappingbinwidth) 
    error('overlappingbinwidth must be specified');
  end
  binwidth_2 = defaults.overlappingbinwidth / 2;
 
  bin_adjuster = sort([0:defaults.precision:binwidth_2 ...
		    -defaults.precision:-defaults.precision:-binwidth_2]);
 otherwise
  error(sprintf('unknown mode method (was ''%s'')', defaults.modemethod));
end


% ensure the data is at regular intervals
mia = spaceregularly(mia, 'resolution', defaults.qdcresolution);

% remove any samples which are nan
mia = removemissingdata(mia);

% convert sample times to sidereal time
sampletime = getsampletime(mia);
[sidt sidd] = stoffset(sampletime, getlocation(rio), sampletime(1));

% we want first sidereal day to be #1
sidd = sidd + 1; 

% convert sidereal times to index in binned data (counting from 1)
sidi = 1 + round(sidt ./ defaults.qdcresolution);


% bin the data according to sidereal time
qdcsamples = floor(siderealday ./ defaults.qdcresolution);

% preallocate some space for the binned data
binned = repmat(nan, max(sidd), qdcsamples);

% Get the data. Map any bad values to nan
data = getdata(mia);
data(~isfinite(data)) = nan; 

for n = unique(sidd)
  idx = find(sidd == n);
  binned(n, sidi(idx)) = data(1, idx);
end
clear data;

% Find the mode
wsz = round(defaults.modewindowsize ./ defaults.qdcresolution);
if rem(wsz, 2) == 0
  wsz = wsz - 1;
end
wsz = max(1, wsz); % have to use at least one sample

qdcdata = repmat(nan, 1, qdcsamples);

for n = 1:qdcsamples
  if wsz == 1
    idx = n;
  else
    idx = (n-(wsz-1)./2):(n+(wsz-1)./2);
    idx(idx <= 0) = idx(idx <= 0) + qdcsamples;
    idx(idx > qdcsamples) = idx(idx > qdcsamples) - qdcsamples;
  end

  d = binned(:, idx); % select all data within windowed region
  
  switch defaults.modemethod
   case 'closest'
    d = sort(d(:)); % sort into ascending order
    [tmp m] = min(diff(d)); % find minimum difference between neighbours
    qdcdata(n) = mean(d(m:(m+1))); % take midpoint between min distance
   
   case 'fixedprecision'
    % round to a fixed precision value and take the mode
    [mo mon] = mode(round2(d(:), defaults.precision));
    if mon == 1 & logical(defaults.ignoreuniformdistribution)
      % if the number of samples have the modal value is only 1 then
      % either their is only one sample (unreliable) or the distribution
      % is uniform (also unreliable). Note that we don't bother checking
      % for the case that all unique values occur > 1 times, as this is
      % extremely unlikely.
      qdcdata(n) = nan;
    else
      % Could have multiple values for the mode, take their mean and use
      % that as the QDC value
      qdcdata(n) = mean(mo);
    end
    
   case 'overlappingbins'
    % Calculate the mode by binning data values into overlapping bins (ie
    % each sample can end up in > 1 bin). To avoid having many bins, most of
    % which may be empty, round all the data values to a fixed
    % precision. The unique values then give a central value for a
    % bin. However, there may be bins, whose central value are not in the
    % unique set but should still be calculated (eg a bin in between two
    % humps in the distribution). Consider these points as valid bins. It is
    % then easy to loop through each bin and find out how many samples occur
    % within its edges.
    
    % Round to a fixed precision value, then calculate how many times
    % each unique value occurs.
    [uniq count] = uniquewithcount(round2(d(:), defaults.precision));

    bin_centers = unique(repmat(uniq(:), 1, numel(bin_adjuster)) + ...
			 repmat(bin_adjuster, numel(uniq), 1));
    
    % Now sum up the number of values in the overlapping bins. 
    overlappingcount = zeros(size(bin_centers));
    for m = 1:numel(bin_centers)
      overlappingcount(m) ...
	  = sum(count(uniq > bin_centers(m) - binwidth_2 & ...
		      uniq < bin_centers(m) + binwidth_2));
    end
    
    % Find the highest number of counts, then choose the bin(s) with that
    % number of counts. As several bins may have the same, highest count
    % take the mean of them all
    bin_idx = find(overlappingcount == max(overlappingcount));
    
    qdcdata(n) = mean(bin_centers(bin_idx));
    
   otherwise
    error(sprintf('unknown mode method (was ''%s'')', ...
		  defaults.modemethod));
  end
end

% Take the mean values from the selected rows in the binned data
qdcsampletime = [0.5:qdcsamples] * defaults.qdcresolution;

% Compute integration time
integrationtime = getintegrationtime(mia);
integrationtime(~isfinite(integrationtime)) = [];
qdcintegrationtime = repmat(median(integrationtime), size(qdcsampletime));

r = feval(defaults.qdcclass, ...
	  'instrument', rio, ...
	  'starttime', getstarttime(mia), ...
	  'endtime', getendtime(mia), ...
	  'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
	  'qdcperiodendtime', defaults.qdcperiodendtime, ...
          'beams', beam, ...
          'data', qdcdata(1, 1:qdcsamples), ...
	  'units', getunits(mia), ...
	  'sampletime', qdcsampletime, ...
	  'integrationtime', qdcintegrationtime, ...
          'processing', getprocessing(mia));


switch defaults.modemethod
 case 'closest'
  r = addprocessing(r, sprintf(['Created by %s with modemethod=%s ' ...
		    'windowsize=%s'], ...
			       basename(mfilename), ...
			       defaults.modemethod, ...
			       char(defaults.modewindowsize)));
  
 case 'fixedprecision'
  r = addprocessing(r, sprintf(['Created by %s with modemethod=%s, ' ...
		    'windowsize=%s precision=%g %s'], ...
			       basename(mfilename), ...
			       defaults.modemethod, ...
			       char(defaults.modewindowsize), ...
			       defaults.precision, ...
			       getunits(mia)));
 
 case 'overlappingbins'
  r = addprocessing(r, sprintf(['Created by %s with modemethod=%s, ' ...
		    'windowsize=%s precision=%g%s ' ...
		    'overlappingbinwidth=%g%s'], ...
			       basename(mfilename), ...
			       defaults.modemethod, ...
			       char(defaults.modewindowsize), ...
			       defaults.precision, getunits(mia), ...
			       defaults.overlappingbinwidth, getunits(mia)));
  
 otherwise
  error(sprintf('unknown mode method (was ''%s'')', ...
		defaults.modemethod));
end

