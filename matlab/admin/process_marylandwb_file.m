function r = process_marylandwb_file(rio, t)

day = timespan(1, 'd');
res = timespan(10, 's');

freq = getfrequency(rio);
if length(freq) == 2
  cols = '$2, $3';
elseif length(freq) == 3
  cols = '$2, $3, $4';
else
  error('unknown number of frequencies');
end

[ib wb] = info(rio, 'beams');
beams = union(ib, wb);

for n = 1:prod(size(t))
  st = floor(t(n), day);
  et = st + day;
  
  % can't use strftime, can't have leading zeros
  ab = getabbreviation(rio);
  switch ab
   case 'spa'
    ab = 'spa';
   case 'mcm'
    ab = 'McM';
  end
  
  fname = sprintf('%sB%04d_%d_%d.dat', ab, datevec(t(n), 1:3));
  
  tmpfile = tempname;
  cmd = sprintf(['awk --posix ''($1 ~ /[0-9]{4}\\:[0-9]{2}/){secs = ' ...
		 'substr($1,1,2)*3600+substr($1,3,2)*60+substr($1,6,2);' ...
		 'print secs, %s}'' %s > %s'], ...
		cols, fname, tmpfile);
  unix(cmd);
  
  [tmp varname] = fileparts(tmpfile);
  if varname(1) >= '0' & varname(1) <= '9'
    varname = ['X' varname];
  end

  load('-ascii', tmpfile);
  eval(sprintf('data = %s''; clear %s', varname, varname));
  
  % col 1: seconds offset from midnight
  % [tmp1 tmp2 tmp3 tmp4 datasz] = info(rio, 'defaultfilename', 'rio_abs');
  s = info(rio, 'defaultfilename', 'rio_abs');
  
  data2 = repmat(nan, s.size);
  
  idx = 1 + data(1, :) ./ gettotalseconds(res);
  data2(1:length(freq), idx) = data(1 + [1:length(freq)], :);
  
  % load existing data (if present)
  mia = rio_abs('starttime', st, ...
		'endtime', et, ...
		'resolution', res, ...
		'beams', beams, ...
		'instrument', rio, ...
		'failiffilemissing', 0, ...
		'load', 1, ...
		'units', 'dB', ...
		'log', 0);
  
  mia2 = rio_abs('starttime', st, ...
		 'endtime', et, ...
		 'resolution', res, ...
		 'beams', beams, ...
		 'instrument', rio, ...
		 'obliquity', 'standard', ...
		 'obliquityfactors', calcobliquity(rio, ...
						   'style', 'standard', ...
						   'beams', beams), ...
		 'data', data2, ...
		 'units', 'dB');
  
  r = insert(mia, mia2);
  save(r)
end
  
