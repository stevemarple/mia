function r = create(mia, varargin)
%CREATE Create a magnetometer quiet day curve
%
%   r = CREATE(mia, ...)
%
% r: completed QDC
% mia: basic information for the QDC, as a MAG_QDC_FFT object
%
% The following parameter name/value pairs are accepted:
%
%   'exclude', [n x 2 TIMESTAMP]
%     A set of intervals which should not be used in the creation of the
%     MAG_QDC_FFT object. Note that the exculsion is performed by setting
%     any data from these times to NAN, and that this if performed before
%     any prefiltering.
%
%   'loadoptions', CELL
%     Any load options which should be passed to the MAG_DATA
%     constructor. Unless specified otherwise 'failiffilemissing' is set
%     to zero and 'resolutionmethod' is set to 'nonanmean'.
%
%   'mag_data', MAG_DATA
%     Create the MAG_QDC_FFT object from this data, instead of loading
%     the required data from disk. Of most use for test purposes.
%
%   'numquietdays', integer
%     The number of quietest days which should be used to create the
%     MAG_QDC_FFT object. Default is 5.
%
%   'orderingcomponent', CHAR
%     The name of the component which should be used when ordering the
%     days into ascending activity. The default component is the first
%     one (i.e., H for HDZ systems or X for XYZ systems).
%
%   'prefilter', MIA_FILTER_BASE
%     prefilter the MAG_DATA. Note that the filtering occurs after data
%     periods are excluded (set to NAN).
%    
%
%   'postfilter', MIA_FILTER_BASE
%     Filter the almost-completed MAG_QDC object.

r = mia;

mag = getinstrument(r);

comps = getcomponents(r);
if isempty(comps)
  comps = info(mag, 'components');
  r = setcomponents(r, comps);
end
ncomps = numel(comps);

defaults.exclude = [];
defaults.prefilter = [];
defaults.postfilter = [];

% allow the caller to pass the data to be used for the conversion,
% otherwise load from file

defaults.mag_data = [];
defaults.numquietdays = 5;
defaults.orderingcomponent = comps{1};

defaults.loadoptions = {};

[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(unvi)
  disp(sprintf(['The following parameter names were not ' ...
		'recognised: %s%s'], varargin{unvi(1)}, ...
	       sprintf(', %s', varargin{unvi(3:2:end)})));
end
		
st = r.qdcstarttime;
if isempty(st)
  disp('no start time given, doing last month');
  dv = datevec(timestamp('now'));
  dv(3:6) = [1 0 0 0];
  if dv(2) == 1
    dv(1) = dv(1) - 1;
    dv(2) = 12;
  else
    dv(2) = dv(2) - 1;
  end
  st = timestamp(dv);
  r.qdcstarttime = st;
end

et = r.qdcendtime;
if isempty(et)
  % set to be start of next month
  dv = datevec(st);
  dv(3:6) = [1 0 0 0];
  dv(2) = dv(2) + 1; 

  if dv(2) > 12
    % year boundary
    dv(1) = dv(1) + 1;
    dv(2) = 1;
  end
  et = timestamp(dv);
  r.qdcendtime = et;
end

res = getresolution(r);
if isempty(res)
  res = info(mag, 'bestresolution');
  % update the object, don't attempt to alter the contents of the data
  r = setresolutionfield(r, res);
end

day = timespan(1, 'd');
day_res = day ./ res;
if day_res ~= fix(day_res)
  error('resolution must divide exactly into 1 day');
end

% both ways of getting data should use same resolutionmethod
resmethod = 'nonanmean';
if isempty(defaults.mag_data)
  % unless explicitly set otherwise in loadoptions ignore any missing
  % files. Use nonanmean if resolution is changed
  mdata = mag_data('starttime', st, ...
		   'endtime', et, ...
		   'instrument', mag, ...
		   'resolution', res, ...
		   'load', 1, ...
		   'log', 0, ...
		   'loadoptions', ...
		   {'failiffilemissing', 0, ...
		    'resolutionmethod', resmethod, ...
		    defaults.loadoptions{:}});
else
  mdata = defaults.mag_data;
  mdata = setresolution(mdata, res, resmethod);
  st = getstarttime(mdata);
  et = getendtime(mdata);  
  r.qdcstarttime = st;
  r.qdcendtime = et;
end

if ~isempty(defaults.exclude)
  [mdata, excludeStr] = excludedata(mdata, defaults.exclude);
else
  excludeStr = '';
end

if isa(defaults.prefilter, 'mia_filter_base')
  mdata = filter(defaults.prefilter, mdata);
end

% if mdata does not start on a day boundary pad with nans
if st ~= getdate(st)
  tmp = mag_data('starttime', getdate(st), ...
		 'endtime', st, ...
		 'instrument', mag, ...
		 'resolution', res, ...
		 'components', comp, ...
		 'load', 0, ...
		 'units', getunits(r), ...
		 'data', repmat(nan, ncomps, gettimeofday(st)/res));
  mdata = insert(mdata, tmp);
  st = getdate(st);
end
% if mdata does not end on a day boundary pad with nans
if et ~= getdate(et)
  tmp = mag_data('starttime', et, ...
		 'endtime', ceil(et, day), ...
		 'instrument', mag, ...
		 'resolution', res, ...
		 'components', comp, ...
		 'load', 0, ...
		 'units', getunits(r), ...
		 'data', repmat(nan, ncomps, day - gettimeofday(et)/res));
  mdata = insert(mdata, tmp);
  et = getendtime(mdata);
end

numDays = (et-st)/day;
% new data
% data = repmat(nan, length(comps), day_res);
data = cell(ncomps, 1);
monthlymean = zeros(ncomps, 1);

orderingComponentIdx = getparameterindex(mdata, defaults.orderingcomponent);

for cn = 1:length(comps)
  % reshape, day number is row number, sample number in days is col number
  data{cn} = reshape(getdata(mdata, cn, ':'), [day_res numDays])';
  monthlymean(cn) = nonanmean(data{cn}(:));
  
  % subtract monthly mean
  data{cn} = data{cn} - monthlymean(cn);

  if cn == orderingComponentIdx
    % estimate daily activity based on RMS departure from monthly mean
    activity = sqrt(nonanmean(data{cn}.^2, 2));
  end
end

% based on the selected component choose the quietest days
[tmp days] = sort(activity);
r.quietdays = days(1:defaults.numquietdays)';

% generate unfiltered QDC
% ufdata = cell(ncomps, 1);
rdata = zeros(ncomps, day_res);
for cn = 1:ncomps
  tmp = nonanmean(data{cn}(r.quietdays, :), 1);
  rdata(cn, :) = tmp + monthlymean(cn);
end

r = setdata(r, rdata);
r = setstarttime(r, st); % st already moved to the start of a day
r = setendtime(r, st + day);

r = setunits(r, getunits(mdata));
r = setcomponents(r, comps);
r = setresolutionfield(r, res);
r = settimingfield(r, gettiming(mdata));
r = setprocessing(r, sprintf('created by %s', mfilename), excludeStr);


if isa(defaults.postfilter, 'mia_filter_base')
  r = filter(defaults.postfilter, r);
end

