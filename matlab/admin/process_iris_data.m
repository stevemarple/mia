function r = process_iris_data(varargin)
%PROCESS_IRIS_DATA  Convert IRIS binary data files to Matlab format
%
%   PROCESS_IRIS_DATA(rio, st, et)
%   rio: riometer [RIOMETER]
%   st: start time [TIMESTAMP]
%   et: end time [TIMESTAMP]
%
%   PROCESS_IRIS_DATA(...)
%   Name/value pair interface
%   The following name/value pairs are recognised
%
%     'starttime', TIMESTAMP
%     Start time for processing data.
%
%     'endtime', TIMESTAMP
%     End time for processing data.
%
%     'instrument', RIOMETER
%     RIOMETER to process data for.
%
%     'test', LOGICAL
%     Go through all of the actions but don't actually save any
%     files. Defaults to FALSE.
%
%
% Convert IRIS riometer binary files to MIA matlab files, reading from
% and writing to standard file locations as specified by INFO.
%
% See also PROCESS_IRIS_DATA_FILE, READ_IRIS_RECORD.

day = timespan(1,'d');
defaults.starttime = timestamp('yesterday');
defaults.endtime = [];
defaults.instrument = [];
defaults.successfile = '';
defaults.test = false;

defaults.class = 'rio_rawpower';

if nargin == 3
  defaults.instrument = varargin{1};
  defaults.starttime = varargin{2};
  defaults.endtime = varargin{3};
  unvi = [];
elseif rem(nargin, 2) == 0 & mia_ischar(varargin{1:2:end})
  [defaults unvi] = interceptprop(varargin, defaults);
else
  error('incorrect parameters');
end

if isempty(defaults.starttime)
  error('starttime not specified');
elseif isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + day;
end
if isempty(defaults.instrument)
  error('instrument not specified');
elseif ~isa(defaults.instrument, 'riometer');
  error('instrument must be a riometer');
end


s = info(defaults.instrument, 'defaultfilename', 'original_format');
sdata = info(defaults.instrument, 'defaultfilename', defaults.class);

beams = info(defaults.instrument, 'allbeams');
bestres = info(defaults.instrument, 'bestresolution');

st = floor(defaults.starttime, s.duration);
et = ceil(defaults.endtime, s.duration);

% start early because if time mia-alignment some data for 13UT hour can be in
% the last record of 12UT hour file

t = st - s.duration;

% since records can extend over file boundaries process over and trim to
% length later
while t < et
  tnext = t + s.duration;

  data = [];
  filename = strftime(t, s.fstr);
  
  dirlist = dir(filename);
  if isempty(dirlist)
    disp(['--- file missing: ' filename]);

  elseif dirlist.bytes == 0
    disp(['--- file empty: ' filename]);
  
  else
    disp(['processing ' filename]);

    validstarttime = [t tnext + timespan(48, 's')];
    data = process_iris_data_file(filename, ...
				  defaults.instrument, ...
				  'validstarttime', validstarttime, ...
				  varargin{unvi});
    
    if ~isempty(data)
      % the data almost certainly crosses a blocksize boundary, so cannot
      % just call save or the existing data on disk will be destroyed. Need
      % to insert this data into the gaps in the files on disk.
      dst = getstarttime(data);
      det = getendtime(data);
      t1 = floor(dst, sdata.duration); % start of a file
      et1 = ceil(det, sdata.duration);
      while t1 < et1
	t1end = t1 + sdata.duration; % end of this block of data
	datablock = extract(data, ...
			    'starttime', t1, ...
			    'endtime', t1end, ...
			    'beams', beams);
	
	
	if numel(dir(strftime(t1, sdata.fstr))) == 1
	  % there appears to be existing data of the right type
	  
	  % load up data from a single file
	  mia = feval(defaults.class, ...
		      'starttime', t1, ...
		      'endtime', t1 + sdata.duration, ...
		      'instrument', defaults.instrument, ...
		      'beams', beams, ...
		      'log', 0, ...
		      'load', 1);
	else
	  % cannot load existing data but since the data almost certainly
          % crosses a block boundary then it is almost certain that the
          % datablock here will not be full size. Create a blank block and
          % insert into that to create padding around the data we have
	  samt = (t1+ 0.5*bestres):bestres:t1end;
	  integrationtime = repmat(bestres, size(samt));
	  mia = feval(defaults.class, ...
		      'starttime', t1, ...
		      'endtime', t1end, ...
		      'sampletime', samt, ...
		      'integrationtime', integrationtime, ...
		      'instrument', defaults.instrument, ...
		      'beams', beams, ...
		      'load', 0, ...
		      'units', getunits(data), ...
		      'data', feval(class(getdata(data)), ...
				    repmat(nan, sdata.size)));
	end
	miaSave = insert(mia, datablock);
	if logical(defaults.test)
	  disp(sprintf('Test mode: not saving data %s', dateprintf(miaSave)));
	else
	  save(miaSave); % NB overloaded save function
	end
	t1 = t1end; % deal with data that overlaps the next block
      end
    else
      disp(['--- processing failed on: ' filename]);
    end
  end
  
  t = tnext;
end



% We have now processed all input files, if any were missing some of the
% output files may be missing (depends several factors, such as whether
% s.duration == sdata.duration and if any input data records crossed over
% file boundaries). If sdata.failiffilemissing == 0 then ignore this
% problem. Otherwise test for missing files, create blank data and save.

if ~sdata.failiffilemissing
  return
end

st = floor(defaults.starttime, sdata.duration);
et = ceil(defaults.endtime, sdata.duration);


if logical(defaults.test)
  disp('Test mode: not creating blank output file(s)');
else
  t = st;
  while t < et
    t2 = t + sdata.duration;
    filename = strftime(t, sdata.fstr);
    dirlist = dir(filename);
    if isempty(dirlist)
      disp('creating blank output file');
      mia = feval(defaults.class, ...
		  'starttime', t, ...
		  'endtime', t2, ...
		  'instrument', defaults.instrument, ...
		  'beams', beams, ...
		  'log', 0, ...
		  'load', 1, ...
		  'loadoptions', {'failiffilemissing', 0});
      % because the load function had to create it from scratch rather than
      % load something off disk the data matrix is the wrong type (double)
      % and the units aren't set
      mia = setdata(mia, uint16(getdata(mia)));
      mia = setunits(mia, 'ADC');
      save(mia);
    else
      [status mesg]  = touch(filename);
      if status
	warning(mesg);
      end
    end
    
    t = t2;
  end
end



% If we have got this far then processing is successful (any data errors
% have been notified by process_iris_data_file to the checklogfile, if
% requested). Create the successfile (if requested) to indicate success
if ~isempty(defaults.successfile)
  [fid mesg] = fopen(defaults.successfile, 'w');
  if fid == -1 
    error(sprintf('cannot open %s: %s', defaults.successfile, mesg));
  end
  fprintf(fid, ['%s successful\n' ...
		'start time: %s\n' ...
		'end time : %\n', ...
		'instrument: %s\n'], ...
	  mfilename, ...
	  strftime(defaults.starttime, '%Y-%m-%d %H:%M:%S'), ...
	  strftime(defaults.endtime, '%Y-%m-%d %H:%M:%S'), ...
	  getcode(defaults.instrument, 1));
  fclose(fid);
end
