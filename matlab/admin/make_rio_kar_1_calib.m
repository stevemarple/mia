function r = make_rio_kar_1_calib(varargin)
% Make Karholl imaging riometer calibration table from recorded
% calibration dataset.

if nargin
  doSave = varargin{1}
else
  doSave = false
end

rio = rio_kar_1
% Calibration data recorded 2019-07-03.

st = timestamp([2019 07 03 19 00 00])

% Time (minutes after 1900), dBm
d = [
	35 -120;
	36 -119;
	37 -118;
	38 -117;
	39 -116;
	40 -115;
	41 -114;
	% error was made
	43 -113;
	44 -112;
	45 -111;
	46 -110;
	47 -109;
	48 -108;
	49 -107;
	50 -106;
	51 -105;
	52 -104;
	53 -103;
	54 -102;
	55 -101;
	56 -100;
	57 -99;
	58 -98;
	59 -97;
	60 -96;
	61 -95;
	62 -94;
	63 -93;
	64 -92;
	65 -91;
	66 -90;
	];
t = st + timespan(d(:,1)', 'm')
values = unique(d(:,2)');

numbeams = numel(info(rio, 'allbeams'))

linearisetable.instrument = rio;
linearisetable.beams = info(rio, 'allbeams');
linearisetable.starttime = timestamp('bad'); % Valid from any start time
linearisetable.endtime = timestamp('bad'); % Valid to any end time
linearisetable.x = repmat(nan, numbeams, length(values));
linearisetable.y = repmat(values, numbeams, 1);

calibdata = rio_rawpower('instrument', rio, ...
						 'starttime', min(t), ...
						 'endtime', max(t) + timespan(1, 'm'))

for vn = 1:numel(values)
  v = values(vn)
  n = find(d(:,2)' == v)
  % Data is 1 minute long, trim first and last 5 seconds
  st = t(n) + timespan(5, 's');
  et = t(n) + timespan(55, 's');
  
  % Imaging beams. Each column uses the same riometer, so share
  % calibration data to make the same curve for al beams in a column.
  for col = 1:7
	beams = col:7:49;
	rp = extract(calibdata, ...
				 'starttime', st, ...
				 'endtime', et, ...
				 'beams', beams);
	avg = mean(getdata(rp), 'all');
	if avg > 2.0
	  avg = nan;
	end
	linearisetable(1).x(beams, vn) = avg;
  end
  
  % Widebeam
  beams = 50;
  rp = extract(calibdata, ...
			   'starttime', st, ...
			   'endtime', et, ...
			   'beams', beams);
  avg = mean(getdata(rp), 'all');
	if avg > 2.0
	  avg = nan;
	end
  linearisetable(1).x(beams, vn) = avg;
  

end

r = linearisetable


if 1
  figure;
  hold on
  labels = {}
  for bn = [1:7 50]
	plot(linearisetable.x(bn, :), linearisetable.y(bn, :))
	labels{end+1} = sprintf('Beam %d', bn);
  end
  legend(labels);
end

if doSave
  f = fullfile(mia_basedir, 'riometer', ...
			   ['linearise_rio_table_'  getcode(rio) '.mat']);
  disp(['Saving to ' f])
  save(f, 'linearisetable');
end
