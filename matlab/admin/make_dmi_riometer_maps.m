function [fh, gh] = make_dmi_riometer_maps(varargin)

defaults.bitmapformat = 'png';
defaults.bitmapsize = [480 400];
defaults.closefigure = 1;

[defaults unvi] = interceptprop(varargin, defaults);

% [fh gh] = scand('latitude', [60 85], 'longitude', [-55 -25]);
[fh gh] = scand('latitude', [60 85], 'longitude', [-55 20]);

set(fh, 'Color', 'w');

rio = rio_dmi;

h = map(rio, gh, ...
	'abbreviation', 1, ...
	'formatabbreviation', 'upper');

ht = h(:,2); % text handles

set(ht, 'FontSize', 8);


facname = lower(getfacility(rio(1)));
facname(facname == ' ') = '_';

figure2imagefile(fh, [facname '.' defaults.bitmapformat], ...
		 'size', defaults.bitmapsize);

print(fh, '-depsc2', [facname '.eps']);

print_fmts = print('-d');
if any(any(strcmp(print_fmts, 'pdf')))
  print(fh, '-dpdf', [facname '.pdf']);
elseif any(any(strcmp(print_fmts, 'pdfwrite')))
  print(fh, '-dpdfwrite', [facname '.pdf']);
else
  'unix'
  unix(sprintf('ps2pdf %s.ps %s.pdf', facname, facname));
end
if defaults.closefigure
  close(fh);
end

