function [fh, gh] = make_canopus_maps(varargin)

defaults.bitmapformat = 'png';
defaults.bitmapsize = [480 400];
defaults.closefigure = 1;

[defaults unvi] = interceptprop(varargin, defaults);

[fh gh] = canada;
set(fh, 'Color', 'w');

rio = rio_canopus;

h = map(rio, gh, ...
	'abbreviation', 1, ...
	'formatabbreviation', 'upper');

ht = h(:,2); % text handles

set(ht, 'FontSize', 8);

% set these to be aligned left, center and with a space
ri = [rio_rank_1 rio_eski_1 rio_back_1];
for n = 1:length(ri)
  tmph = ht((ri(n) == rio));
  s = get(tmph, 'String');
  set(tmph, ...
      'String', [' ' s], ...
      'HorizontalAlignment', 'left', ...
      'VerticalAlignment', 'middle');
end

% set these to be aligned left, top
ri = [rio_gill_1 rio_isll_1 rio_pina_1];
for n = 1:length(ri)
  tmph = ht((ri(n) == rio));
  s = get(tmph, 'String');
  set(tmph, ...
      'String', s, ...
      'HorizontalAlignment', 'left', ...
      'VerticalAlignment', 'top');
end


set(ht(rio == rio_mcmu_1), ...
    'HorizontalAlignment', 'right', ...
    'VerticalAlignment', 'top');

set(ht(rio == rio_rabb_1), ...
    'HorizontalAlignment', 'center', ...
    'VerticalAlignment', 'baseline');

set(ht(rio == rio_fsim_1), ...
    'HorizontalAlignment', 'right', ...
    'VerticalAlignment', 'top');

tmph = ht((rio_back_1 == rio));
s = get(tmph, 'String');
set(tmph, ...
    'String', [s ' '], ...
    'HorizontalAlignment', 'left', ...
    'VerticalAlignment', 'middle');


facname = lower(getfacility(rio(1)));
facname(facname == ' ') = '_';

figure2imagefile(fh, [facname '.' defaults.bitmapformat], ...
		 'size', defaults.bitmapsize);

print(fh, '-depsc2', [facname '.ps']);

print_fmts = print('-d');
if any(any(strcmp(print_fmts, 'pdf')))
  print(fh, '-dpdf', [facname '.pdf']);
elseif any(any(strcmp(print_fmts, 'pdfwrite')))
  print(fh, '-dpdfwrite', [facname '.pdf']);
else
  'unix'
  unix(sprintf('ps2pdf %s.ps %s.pdf', facname, facname));
end
if defaults.closefigure
  close(fh);
end

% figure2imagefile(fh, ['canopus.' defaults.bitmapformat], ...
% 		 'size', defaults.bitmapsize);

% print(fh, '-depsc2', 'canopus.eps');

% print_fmts = print('-d');
% if any(any(strcmp(print_fmts, 'pdf')))
%   print(fh, '-dpdf', 'canopus.pdf');
% else
%   unix(sprintf('ps2pdf %s %s', fname, fname2));
% end
% if defaults.closefigure
%   close(fh);
% end

