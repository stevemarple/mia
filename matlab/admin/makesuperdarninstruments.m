function makesuperdarninstruments(varargin)
%MAKESUPERDARNINSTRUMENTS  Generate data files for SUPERDARN radars
%
% Generate the instrument function file and the associated INFO data
% files for the SUPERDARN radars.
%
% By default the files are saved into MIA_BASEDIR/radar, which will
% require administrator permissions.
%
% The following parameter name/value pairs may be specified to modify the
% default behaviour.

defaults.path = fullfile(mia_basedir, 'radar');

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi});



% Southern hemisphere
tmp = 1;
sd(tmp).location = {'Halley', 'Antartica'};
sd(tmp).abbreviation = 'hal';
sd(tmp).name = 'Halley';
sd(tmp).id = 4;
sd(tmp).code = 'h';
sd(tmp).latitude = -75.52;
sd(tmp).longitude = -26.63;
sd(tmp).boresight = 165;
sd(tmp).beamseparation = -3.24;

tmp = tmp+1;
sd(tmp).location = {'Vesleskarvet (SANAE IV)', 'Antarctica'};
sd(tmp).abbreviation = 'san';
sd(tmp).name = 'SANAE';
sd(tmp).id = 11;
sd(tmp).code = 'd';
sd(tmp).latitude = -71.68;
sd(tmp).longitude = -2.85;
sd(tmp).boresight = 173.2;
sd(tmp).beamseparation = -3.24;

tmp = tmp+1;
sd(tmp).location = {'Syowa', 'Antarctica'};
sd(tmp).abbreviation = 'sys';
sd(tmp).name = 'Syowa South';
sd(tmp).id = 12;
sd(tmp).code = 'j';
sd(tmp).latitude = -69.00;
sd(tmp).longitude = 39.58;
sd(tmp).boresight = 159;
sd(tmp).beamseparation = -3.33;

tmp = tmp+1;
sd(tmp).location = {'Syowa', 'Antarctica'};
sd(tmp).abbreviation = 'sye';
sd(tmp).name = 'Syowa East';
sd(tmp).id = 13;
sd(tmp).code = 'n';
sd(tmp).latitude = -69.01;
sd(tmp).longitude = 39.61;
sd(tmp).boresight = 106.5;
sd(tmp).beamseparation = -3.33;

tmp = tmp+1;
sd(tmp).location = {'Tasmania', 'Australia'};
sd(tmp).abbreviation = 'tig';
sd(tmp).name = 'TIGER Tasmania';
sd(tmp).id = 14;
sd(tmp).code = 'r';
sd(tmp).latitude = -43.38;
sd(tmp).longitude = 147.23;
sd(tmp).boresight = 180;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'', 'Kerguelen Island'};
sd(tmp).abbreviation = 'ker';
sd(tmp).name = 'Kerguelen';
sd(tmp).id = 15;
sd(tmp).code = 'p';
sd(tmp).latitude = -49.35;
sd(tmp).longitude = 70.26;
sd(tmp).boresight = 168;
sd(tmp).beamseparation = -3.24;

tmp = tmp+1;
sd(tmp).location = {'Unwin', 'New Zealand'};
sd(tmp).abbreviation = 'unw';
sd(tmp).name = 'TIGER New Zealand';
sd(tmp).id = 17;
sd(tmp).code = 'u';
sd(tmp).latitude = -46.51;
sd(tmp).longitude = 168.38;
sd(tmp).boresight = 215;
sd(tmp).beamseparation = -3.24;

% Northern hemisphere
tmp = tmp+1;
sd(tmp).location = {'Goose Bay', 'Canada'};
sd(tmp).abbreviation = 'gbr';
sd(tmp).name = 'Goose Bay';
sd(tmp).id = 1;
sd(tmp).code = 'g';
sd(tmp).latitude = 53.32;
sd(tmp).longitude = -60.46;
sd(tmp).boresight = 5;
sd(tmp).beamseparation = 3.24;

% tmp = tmp+1;
% sd(tmp).location = {'Schefferville', '?'};
% sd(tmp).abbreviation = '???';
% sd(tmp).name = 'Schefferville';
% sd(tmp).id = 2;
% sd(tmp).code = 's';
% sd(tmp).latitude = 54.8;
% sd(tmp).longitude = -66.8;
% sd(tmp).boresight = 15;
% sd(tmp).beamseparation = 3.20;

tmp = tmp+1;
sd(tmp).location = {'Kapuskasing', 'Canada'};
sd(tmp).abbreviation = 'kap';
sd(tmp).name = 'Kapuskasing';
sd(tmp).id = 3;
sd(tmp).code = 'k';
sd(tmp).latitude = 49.39;
sd(tmp).longitude = -83.32;
sd(tmp).boresight = -12;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'Saskatoon', 'Canada'};
sd(tmp).abbreviation = 'sas';
sd(tmp).name = 'Saskatoon';
sd(tmp).id = 5;
sd(tmp).code = 't';
sd(tmp).latitude = 52.16;
sd(tmp).longitude = -106.530;
sd(tmp).boresight = 23.1;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'Prince George', 'Canada'};
sd(tmp).abbreviation = 'pgr';
sd(tmp).name = 'Prince George';
sd(tmp).id = 6;
sd(tmp).code = 'b';
sd(tmp).latitude = 53.98;
sd(tmp).longitude = -122.59;
sd(tmp).boresight = -5;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'Kodiak', 'Alaska'};
sd(tmp).abbreviation = 'kod';
sd(tmp).name = 'Kodiak';
sd(tmp).id = 7;
sd(tmp).code = 'a';
sd(tmp).latitude = 57.60;
sd(tmp).longitude = -152.20;
sd(tmp).boresight = 30;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'Stokkseri', 'Iceland'};
sd(tmp).abbreviation = 'sto';
sd(tmp).name = 'Iceland West';
sd(tmp).id = 8;
sd(tmp).code = 'w';
sd(tmp).latitude = 63.86;
sd(tmp).longitude = -22.02;
sd(tmp).boresight = -59;
sd(tmp).beamseparation = 3.29;

tmp = tmp+1;
sd(tmp).location = {'Pykkvibaer', 'Iceland'};
sd(tmp).abbreviation = 'pyk';
sd(tmp).name = 'Iceland East';
sd(tmp).id = 9;
sd(tmp).code = 'e';
sd(tmp).latitude = 63.77;
sd(tmp).longitude = -20.54;
sd(tmp).boresight = 30;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'Hankasalmi', 'Finland'};
sd(tmp).abbreviation = 'han';
sd(tmp).name = 'Finland';
sd(tmp).id = 10;
sd(tmp).code = 'f';
sd(tmp).latitude = 62.32;
sd(tmp).longitude = 26.61;
sd(tmp).boresight = -12;
sd(tmp).beamseparation = 3.24;

tmp = tmp+1;
sd(tmp).location = {'King Salmon', 'Alaska'};
sd(tmp).abbreviation = 'ksr';
sd(tmp).name = 'King Salmon';
sd(tmp).id = 16;
sd(tmp).code = 'c';
sd(tmp).latitude = 58.68;
sd(tmp).longitude = -156.65;
sd(tmp).boresight = -20;
sd(tmp).beamseparation = 3.24;

funcs = cell(1, length(sd));

test = 0;

inabbrev = instrumenttypeinfo(cs_radar, 'abbreviation');

for n = 1:length(sd)
  a = sd(n);
  a.serialnumber = 1; % allow option of adding individually later on

  if isempty(a.code)
    error('code is not set');
  end
  
  f = sprintf('%s_%s_%d', inabbrev, a.code, a.serialnumber);
  funcs{n} = f;
  
  fname = fullfile(defaults.path, [f '.m'] );

  if test
    fid = 1;
  else
    fid = url_fopen(fname, 'w');
  end

  fprintf(fid, ...
	  ['function r = %s\n' ...
	   '%%%s  SUPERDARN %s radar.\n' ...
	   '\n' ...
	   '%% automatically generated by %s\n' ...
	   'loc = location(%s, %s, %g, %g);\n' ...
	   'r = cs_radar(''name'', ''%s'', ...\n' ...
	   '             ''abbreviation'', ''%s'', ...\n' ...
	   '             ''facility'', ''SUPERDARN'', ...\n' ...
	   '             ''location'', loc, ...\n' ...
	   '             ''serialnumber'', %d, ...\n', ...
	   '             ''tx'', 1, ...\n' ...
	   '             ''rx'', 1);\n\n'], ...
	  f, ...
	  upper(f), a.name, ...
	  mfilename, ...
	  localEncode(a.location{1}), localEncode(a.location{2}), ...
	  a.latitude, a.longitude, ...
	  a.name, ...
	  a.code, ...
	  a.serialnumber);
	  
  if test
    disp(sprintf('----- end of %s -----', fname));
  else
    fclose(fid);
  end
  
  f2 = ['info_' f '_data'];
  datafile = fullfile(defaults.path, [f2 '.m'] );
  
  if test
    fid = 1;
  else
    fid = url_fopen(datafile, 'w');
  end
  
  %  save(fname2, 's'); % write out structure for info file
   fprintf(fid, ...
	  ['function r = %s\n' ...
	   '%%%s  Return basic information about %s\n' ...
	   '%%\n' ...
	   '%% This function is not intended to be called directly, use\n' ...
	   '%% the INFO function to access data about %s. To override\n' ...
	   '%% the information given in this file see the instructions\n' ...
	   '%% in INFO.\n' ...
	   '%%\n' ...
	   '%% This function was generated automatically by %s\n' ...
	   '\n'], ...
	   f2, ...
	   upper(f2), f, ...
	   f, ...
	   mfilename);
   fprintf(fid, 'r.abbreviation = %s;\n', localEncode(a.abbreviation));
   fprintf(fid, 'r.beamseparation = %g;\n', a.beamseparation);
   fprintf(fid, 'r.boresight = %g;\n', a.boresight);
   fprintf(fid, 'r.code = %s;\n', localEncode(a.code));
   fprintf(fid, 'r.latitude = %g;\n', a.latitude);
   fprintf(fid, 'r.location1 = %s;\n', localEncode(a.location{1}));
   fprintf(fid, 'r.location2 = %s;\n', localEncode(a.location{2}));
   fprintf(fid, 'r.longitude = %g;\n', a.longitude);
   fprintf(fid, 'r.name = %s;\n', localEncode(a.name));
   fprintf(fid, 'r.st_id = %d;\n', a.id);
   
   fprintf(fid, '\n%% end of function\n');
   
   if ~test
     fclose(fid);
   end
   
  
end

% generate list of SUPERDARN instruments
if test
  fid = 1;
else
  fname = fullfile(defaults.path, 'superdarn.m');
  fid = url_fopen(fname, 'w');
end

fprintf(fid, ...
	['function r = superdarn\n' ...
	 '%%SUPERDARN  Return list of SUPERDARN radars\n' ...
	 '\n' ...
	 '%% automatically generated by %s\n' ...
	 'r = [%s ...\n'], ...
	mfilename, ...
	funcs{1});
fprintf(fid, '     %s ...\n', funcs{2:(end-1)});
fprintf(fid, '     %s];\n\n', funcs{end});

if ~test
  fclose(fid);
end

% function to wrap chars with single quotes and i18n_char with its
% constructor call
function r = localEncode(c, enc)
if ischar(c)
  r = ['''' c ''''];
elseif isa(c, 'i18n_char')
  [d e] = getdata(c);
  r = sprintf('i18n_char(''%s'', [%s])', e, printseries(d));
end
