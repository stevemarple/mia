function gaiaplots(mia, varargin)
%GAIAPLOTS  Create summary images for GAIA.
%
%   GAIAPLOTS(mia, ...)
%   mia: MIA_IMAGE_BASE data
%
% GAIAPLOTS creates summary plots for GAIA <http://gaia-vxo.org/> from a
% set of MIA_IMAGE_BASE data. The start and end times of the data should
% be midnight, and the duration should be one day. The default behaviour
% can be modified with the following name/value pairs:
%
%    'mask', DOUBLE
%    A mask which is applied to the image to indicate which parts of the
%    image are missing. This allows pixels which are outside of the field
%    of view to be distinguished from missing data values.
%

defaults.gaiasummarydir = '/summary/gaia_v2';
defaults.mask = [];
defaults.facility = '';
defaults.channelrefname = gettype(mia, 'l');
defaults.zip = true;

% limits for the data. NaN means no limit
defaults.limits = [nan nan];
defaults.logscale = false;
defaults.writemissingthumbnails = false;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

day = timespan(1, 'd');
st = getstarttime(mia);
et = getendtime(mia);

if getresolution(mia) ~= timespan(1, 'm')
  error('data resolution MUST be 1 minute');
end

instrument = getinstrument(mia);

% Replace underscores with hyphens
code = lower(getcode(instrument));
code(code == '_') = '-';

if isempty(defaults.facility)
  facilty = getfacility(instrument);
else
  facilty = defaults.facilty;
end

if isempty(facilty)
  facilty = 'GLORIA'; % ensure some same fallback is set
end

% convert channel refname to lower case and map spaces to underscores
chanrefname = lower(defaults.channelrefname);
chanrefname(chanrefname == ' ') = '_';

% perform some cursory checks
if st ~= getdate(st)
  error('data must start at midnight');
end
if et - st ~= day
  error('data must be one day duration');
end

if ~all(getresolution(mia) == timespan(1, 'm'))
  error('resolution must be one minute');
end

if isempty(defaults.mask)
  error('please supply mask');
elseif ~isequal(getdatasize(mia, 1:2), size(defaults.mask))
  error('mask is not same size as images');
end
  

dirfstr = fullfile(defaults.gaiasummarydir, ...
		   lower(facilty), ...
		   code, ...
		   chanrefname, ...
		   '%Y', '%m');
% 		   '%Y', '%m', '%d');


t = st;

abs = [];
qdc = [];

d = strftime(t, dirfstr);

data = getdata(mia);
datasz = getdatasize(mia);

units = getunits(mia);
if logical(defaults.logscale)
  units = [units ' (log scale)'];
end


if all(isnan(data(:)))
  % delete old image files and do nothing else
  wildcard = fullfile(d, sprintf('%s_%s_%s_%s*.*', ...
                           lower(facilty), ...
                           code, ...
                           chanrefname, ...
                           strftime(st, '%Y%m%d')));
  disp(strftime(st, 'No valid data for %Y-%m-%d'));
  disp(['delete ' wildcard]);
  delete(wildcard);
  return
end

url_mkdir(d);
  
  

[xpix ypix pixunits] = getpixels(mia);
xpixmin = min(xpix);
xpixmax = max(xpix);
ypixmin = min(ypix);
ypixmax = max(ypix);

qual = join(', ', getdataquality(mia));
if isempty(qual)
  qual = {'DataQuality' ' '};
else
  qual = {'DataQuality' qual};
end

[data2 datarange limits] = local_data_to_8_bit(data, ...
					       defaults.limits, ...
					       defaults.logscale);

% do images
if logical(defaults.zip)
  zipdir = tempname2;
  url_mkdir(zipdir);
else
  zipdir = d;
end
% imgname = fullfile(zipdir, ...
% 		   sprintf('%s_%s_%s_%s_%%02d%%02d.png', ...
% 			   lower(facilty), ...
% 			   code, ...
% 			   chanrefname, ...
% 			   strftime(st, '%Y%m%d')));
imgfstr = fullfile(zipdir, ...
		   [sprintf('%s_%s_%s_', ...
                            lower(facilty), ...
                            code, ...
                            chanrefname) ...
                    '%Y%m%d_%H%M.png']);


% timefstr = [strftime(st, '%Y-%m-%d ') '%02d:%02d:00'];
samt = getsampletime(mia);
integ = getintegrationtime(mia);
imgst = samt - integ./2;
imget = samt + integ./2;
fstr = '%Y-%m-%d %H:%M:%SZ';
files = cell(1, datasz(3));
num_files = 0;
for n = 1:datasz(3)
  filename = strftime(imgst(n), imgfstr);
  %   nminus1 = n - 1;
  %   ho = floor((nminus1) ./ 60);
  %   mi = rem((nminus1), 60);
  %   filename = sprintf(imgname, ho, mi);
  if ~rem(n - 1, 60)
    disp(filename);
  end

  % Create the image file, but only if there is some data (or
  % defaults.writemissingthumbnails is set)
  if logical(defaults.writemissingthumbnails) | any(~isnan(data(:, :, n)))
    num_files = num_files + 1;
    files{num_files} = filename;

    imgdata = flipud(data2(:, :, n)');
    imgdata(~defaults.mask) = 255; % apply the mask

%     ho2 = floor((n) ./ 60);
%     mi2 = rem((n), 60);
    
    url_imwrite(imgdata, filename, 'png', ...
		'DataValueMin', sprintf('%g', datarange(1)), ...
		'DataValueMax', sprintf('%g', datarange(2)), ...
		'DataLimitMin', sprintf('%g', limits(1)), ...
		'DataLimitMax', sprintf('%g', limits(2)), ...
		'DataUnits', units, ...
		qual{:}, ...
		'PositionXMin', sprintf('%g', xpixmin), ...
		'PositionXMax', sprintf('%g', xpixmax), ...
		'PositionYMin', sprintf('%g', ypixmin), ...
		'PositionYMax', sprintf('%g', ypixmax), ...
		'PositionUnits', pixunits, ...
		'InstrumentType', class(instrument), ...
		'InstrumentCode', upper(getabbreviation(instrument)), ...
		'InstrumentSerialNumber', num2str(getserialnumber(instrument)), ...
                'DateTimeStart', strftime(imgst(n), fstr), ...
                'DateTimeEnd', strftime(imget(n), fstr));

    % 'DateTimeStart', sprintf(timefstr, ho, mi), ...
                % 'DateTimeEnd', sprintf(timefstr, ho2, mi2));
  end
end
files = files(1:num_files);

if logical(defaults.zip)
  zipfile = fullfile(d, sprintf('%s_%s_%s_%s.zip', ...
				lower(facilty), ...
				code, ...
				chanrefname, ...
				strftime(st, '%Y%m%d')));
  
  if url_exist(zipfile)
    delete(zipfile);
  end
  
  list_file_name = fullfile(zipdir, 'filelist');
  cmd = sprintf('zip -j %s -r %s -i''@%s''', zipfile, zipdir, list_file_name);
  
  list_fid = url_fopen(list_file_name, 'w');
  fprintf(list_fid, '%s\n', files{:});
  fclose(list_fid);
  
  [s w] = system(cmd);
  if s
    error(sprintf('problem with system: %s\ncommand was: %s', w, cmd));
  end

  % remove temporary files
  delete(list_file_name);
  for n = 1:numel(files)
    delete(files{n});
  end
  rmdir(zipdir);
end

% do keogram
% filename = fullfile(d, 'keogram.png');
filename = fullfile(d, sprintf('%s_%s_%s_%s.png', ...
			       lower(facilty), ...
			       code, ...
			       chanrefname, ...
			       strftime(st, '%Y%m%d')));

keoxpix = xpix(floor(numel(xpix) ./ 2)); % take central column
keomia = setresolution(extract(mia, 'xpixelpos', keoxpix), ...
		       timespan(2, 'm'), 'nonanmean');

keodata = getdata(keomia);

% [keodata2 keodatamin keodatamax] = local_data_to_8_bit(keodata);
[keodata2 keodatarange] ...
    = local_data_to_8_bit(keodata, limits, defaults.logscale);

keopos = floor(numel(xpix) ./ 2); % take central column
keodata2(:, ~defaults.mask(keopos, :), :) = 255; % apply mask
keodata2 = reshape(keodata2, [size(keodata2, 2) size(keodata2, 3)]);

disp(filename);
url_imwrite(flipud(keodata2), filename, 'png', ...
	    'DataValueMin', sprintf('%g', keodatarange(1)), ...
	    'DataValueMax', sprintf('%g', keodatarange(2)), ...
	    'DataLimitMin', sprintf('%g', limits(1)), ...
	    'DataLimitMax', sprintf('%g', limits(2)), ...
	    'DataUnits', units, ...
	    qual{:}, ...
	    'PositionX', sprintf('%g', xpix(keopos)), ...
	    'PositionYMin', sprintf('%g', ypixmin), ...
	    'PositionYMax', sprintf('%g', ypixmax), ...
	    'PositionUnits', pixunits, ...
	    'InstrumentType', class(instrument), ...
	    'InstrumentCode', upper(getabbreviation(instrument)), ...
	    'InstrumentSerialNumber', num2str(getserialnumber(instrument)), ...
	    'DateTimeStart', strftime(st, '%Y-%m-%d %H:%M:%S'), ...
	    'DateTimeEnd', strftime(et, '%Y-%m-%d %H:%M:%S'));


% function [r, datamin, datamax, datarangemin, datarangemax] = ...
%     local_data_to_8_bit(data, datamin_in, datamax_in, logscale)
function [r, datarange, limits_out] = ...
    local_data_to_8_bit(data, limits, logscale)

% 0:253 are valid data values. 254 means bad data. 255 means don't plot (eg
% outside of field of view).

% datarange is the range of the input data
datarange = [min(data(:)) max(data(:))];

% limits_out is the set of limits actually used
limits_out = limits(1:2);  
if isnan(limits(1))
  limits_out(1) = min(data(:));
end
if isnan(limits(2))
  limits_out(2) = max(data(:));
end

if logical(logscale)
  % It doesn't make much sense to use autoscaling values with a logscale but
  % it might help to show small values better so allow it
  r = local_logmap(data, limits_out);
  return
end

% put in range 0 -> limits_out(2)-limits_out(1)
r = data - limits_out(1); 

% put in range 0 -> 253
r = r .* 253 ./ (limits_out(2) - limits_out(1));

r = round(r);
r(r < 0) = 0;
r(r > 253) = 253;

% map nans to bad data value
r(isnan(r)) = 254;

r = uint8(r);


function r = local_logmap(x, limits)

if numel(limits) ~= 2
  error('limits must be 1x2 matrix');
end

if any(~isfinite(limits))
  error('limit values must be finite');
end

% Map the lower limit to +1, when logs are taken it will be 0
x2 = x - limits(1) + 1;


% Want the upper limit to be 253 when logs have been taken
% and a scaling adjustment applied. Calculate the scaling adjustment.

% After taking logs we get
x2_upper = limits(2) - limits(1) + 1;

% Therefore

m = 253 / x2_upper;

r = round(x2 .* m);

r(r < 0) = 0;
r(r > 253) = 253;

% map nans to bad data value
r(isnan(r)) = 254;

r = uint8(r);

