function [fh, gh, gh2, activity, signedActivity, abh, actcode] = ...
    aurorawatchplot(varargin)
%AURORAWATCHPLOT  Generate Aurorawatch-Style plots
%
%  Also calculate activity values, and return these. A brilliant
%  example of non-separation of responsibilities.
%
%  Adding to the non-existent help text, note that this function
%  does only work OKish in combination with "updateauroraplots" at
%  the moment; for it to be able to load data properly itself, more
%  modifications will need to be made to adjust to the "new"
%  MIA way. This is a painful process and a lot of time has been
%  wasted already, so we "postpone" this until later. (MG)

% update 2007-04-24 (MG): now scale activity data for mag_crk_1 by
% "1/1.5" so that the aurorawatch state machine can use the same
% thresholds as for Lancaster. This is the least invasive way
% of making the existing AuroraWatch state machine instrument-aware.

% specify either mag_data or enough things for it to be loaded
defaults.mag_data = [];
% or ...
defaults.starttime = [];
defaults.endtime = [];

% when making realtime plots set the plot time so that we know which
% activity bar to have as an outline
defaults.plottime = [];
defaults.instrument = [];
defaults.resolution = [];
defaults.loadoptions = {};
% defaults.thresholds = [50e-9 100e-9];
defaults.thresholds = [50 100];

% other options
defaults.component = 'H';
% defaults.limits = [2800 3800];
defaults.limits = [];
defaults.limitsrange = [];

defaults.activityresolution = timespan(10, 'm');

% some fudge factor for modify the activy (sensitivity)
defaults.activityscalefactor = 0.5; 

% another fudge factor to modify the size when plotting activity bars
defaults.activityplotfactor = 3;

defaults.yorkcompatibility = 1;
[defaults unvi] = interceptprop(varargin, defaults);

% Introduce instrument-dependent scale factors, for now
% this will only apply to CRK data:
%if defaults.instrument==mag_crk_1
%    disp( '** adjusting activityscalefactor for SAMNET CRK magnetometer **' );
%    defaults.activityscalefactor = defaults.activityscalefactor / 1.5;
%end

if isempty(defaults.mag_data)
  st = defaults.starttime;
  if isempty(defaults.endtime)
    et = defaults.starttime + timespan(1, 'd');
  else
    et = defaults.endtime;
  end
  res = defaults.resolution;
  if isempty(res)
    res = info(defaults.instrument, 'bestresolution');
  end
  mag = defaults.instrument;
  md = mag_data('starttime', st, ...
		'endtime', et, ...
		'components', defaults.component, ...
		'instrument', mag, ...
		'load', 1, ...
		'loadoptions', {defaults.loadoptions{:}, 'resolution',res} );
  md = extract(md,'components',defaults.component); % mag_data does not honour 'components'
elseif numel(defaults.mag_data) == 1 & isa(defaults.mag_data, 'mag_data')
  md = extract(defaults.mag_data, 'components', defaults.component);
  st = getstarttime(md);
  et = getendtime(md);
  [res mesg] = guessresolution(md); 
  if ~isempty(mesg)
    res = timespan(1, 's'); % cannot get resolution from data
  end
  mag = getinstrument(md);
else
  error(['either mag_data must be a valid mag_data object else require ' ...
	 'starttime, endtime, resolution and instrument details']);
end

%md = spaceregularly(md, ...
%		    'markmissingdata', true, ...
%		    'sampletime', [(st+res./2):res:et])
%		    % 'resolution', res)

duration = et - st;
activitybars = duration ./ defaults.activityresolution;
if floor(activitybars) ~= activitybars
  error('duration must be a multiple of the activity resolution');
end
activitysamples = defaults.activityresolution ./ res;

% load up a suitable QDC
qdc = localLoadQdc(st, et, res, mag);
% align to the correct times
% was: qdc2 = extract(qdc, defaults.component, st, et);
% but "new" MIA doesn't have "extract" for re-aligning QDCs
% according to given start- and endtimes.
% We implement a "realign" method instead, and make this a
% two-stage process: 
%  (1) extract component of interest, 
%  (2) align to time of interest.
% (MG)
qdc2=extract(qdc, 'components',defaults.component );
qdc2=realign(qdc2,st,et);

% plot data, and QDC
if getdate(st) ~= st
  dateStr = [strftime(st, '{\bf %Y-%m-%d - ') ...
	      strftime(et, '%Y-%m-%d}')];
else
  % dateStr = strftime(st, '{\bf Geomagnetic activity %Y-%m-%d}');
  dateStr = strftime(st, '{\bf %Y-%m-%d}');
end
title = {'{\bf Geomagnetic activity}', dateStr, char(getlocation(getinstrument(md)))};

% set a suitable set of limits
if isempty(defaults.limits)
  % limits need to be set in nano Tesla, but the QDC is in Tesla.
  if isempty(defaults.limitsrange)
    defaults.limitsrange = 1000;
  end
  
  %qdcmean = nonanmean(getdata(qdc2));
  qdcmean = mean(getdata(qdc2));
  if isnan(qdcmean) 
    % something is wrong with this QDC, stop processing here
    error( 'QDC contains NaNs.' );
  end
  
  % round to nearest 100 nT
  qdcmean = round(qdcmean./100e-9) * 100e-9;
  defaults.limits = round(qdcmean*1e9 + ([-.5 .5] * defaults.limitsrange(1)));
end

% we don't want the 'preliminary' marker in these plots.
md = setdataquality(md, '' );

[fh gh mdh] = plot(md, ...
		   'title', title, ...
		   'receivedraganddrop', 0, ...
		   'components', defaults.component, ...
		   'color', 'k', ...
		   'ylim', defaults.limits);
[tmp1 tmp2 qdch] = plot(qdc2, ...
			'receivedraganddrop', 0, ...
			'components', defaults.component, ...
			'color', 'b', 'plotaxes', gh);


set(fh, 'Color', 'w');
% set(get(gh, 'XLabel'), 'String', 'Universal time (h)');
set(get(gh, 'XLabel'), 'String', 'UT (h)');

% create a second axis overlaying the first
set(gh, ...
    'Color', 'none', ...
    'Layer', 'top');
ghProp = get(gh);
gh2 = [];

% onto the second axis plot the activity bars
  
% chop qdc2 to size in case of incomplete days
s = size(getdata(md));
qdc2_data = getdata(qdc2);
qdc2_data = qdc2_data( 1:s(1), 1:s(2) );

if logical(defaults.yorkcompatibility)
  % The IDL version uses an integer array to store the nT data. Replicate
  % this by calling fix. Can remove when IDL compatibility not required
  mdSubQdc = (1e-9 * fix(getdata(md)*1e9)) - qdc2_data;
else
  mdSubQdc = getdata(md) - qdc2_data;
end


activity = repmat(nan, activitybars, 1);
signedActivity = activity;

abh = zeros(activitybars, 1);
actcode = repmat(' ', 1, activitybars);
for n = 1:activitybars
  % work in nT
  tmp = mdSubQdc((n-1) * activitysamples + [1:activitysamples]) * 1e9;
  
  tmp = tmp * defaults.activityscalefactor;
  [activity(n) idx] = max(abs(tmp));
  signedActivity(n) = tmp(idx);
  
  if ~isnan(activity(n))
    if activity(n) > defaults.thresholds(2)
      color = 'r';
      actcode(n) = 'r';
    elseif activity(n) > defaults.thresholds(1)
      color = [.996 .745 .016]; % amber
      actcode(n) = 'a';
    else
      color = [.2 1 .2]; % light green
      actcode(n) = 'g';
    end
    % add a colored patch
    ydata = ghProp.YLim(1) + ...
	    [0 0 activity(n) activity(n)] * defaults.activityplotfactor;
    xdata = ghProp.XLim(1) + ...
        ([0 activitysamples activitysamples 0] + ...
		   (n-1) * activitysamples) * 1000;
    abh(n) = patch('Parent', gh, ...
		   'XData', xdata, ...
		   'YData', ydata, ...
		   'ZData', [-1 -1 -1 -1], ...
		   'FaceColor', color, ...
		   'EdgeColor', color);
    
  end
end

% % reorder the figure to have the plot axes in front of the activity axes
% hh = get(0, 'ShowHiddenHandles');
% set(0, 'ShowHiddenHandles', 'on');

% ch = get(fh, 'Children');
% ch = [ch(find(ch ~= gh)); gh];
% ch = [ch(find(ch ~= gh2)); gh2];

% set(0, 'ShowHiddenHandles', hh);

% set Zdata to have the QDC behind the data
set(qdch, 'ZData', zeros(size(get(qdch, 'XData'))));
set(mdh, 'ZData', ones(size(get(mdh, 'XData'))));


if ~isempty(defaults.plottime)
  barnumber = fix(((defaults.plottime - st) ...
		   ./ defaults.activityresolution) + 1);
  if barnumber > 0 & barnumber <= activitybars
    if abh(barnumber)
      set(abh(barnumber), 'FaceColor', 'none', 'LineWidth',2);
    end
  end
end




function r = localLoadQdc(st, et, res, mag)
t = mean([st et]);

tv = repmat(t, 1, 5);              %
tv(2) = localSubOneMonth(tv(1));   % t - 1 month
tv(3) = localSubOneMonth(tv(2));   % t - 2 months
tv(4) = localSubOneYear(tv(1));    % t - 1 year
tv(5) = localSubOneMonth(tv(4));   % t - 1 year - 1 month

% try loading a suitable QDC
for n = 1:length(tv)
  r = mag_qdc('time', tv(n), ...
	      'instrument', mag, ...
	      'loadoptions', {'failiffilemissing', 0});
  if any(~isnan(getdata(r)))
    r = localMangleQdc(r, res);
    return; % found a QDC
  end
end

% things are getting desperate, look back for every month
t2 = t;
samnetst = timestamp([1987 10 1 0 0 0]);
while t2 >= samnetst
  r = mag_qdc('time', t2, ...
	      'instrument', mag, ...
	      'loadoptions', {'failiffilemissing', 0});
  if any(~isnan(getdata(r)))
    r = localMangleQdc(r, res);
    return; % found a QDC
  end
  t2 = localSubOneMonth(t2);
end
error('cannot find a suitable QDC');


function r = localMangleQdc(qdc, res)

    qdc = setresolution(qdc, res, 'nearest');   
    % changed method from "rebin" to "nearest" as "rebin" not
    % supported when "increasing" resolution
    % surely this must have worked in "old" MIA, it's yet another
    % incompatibility :-(
    % This is a bit of a pain anyway, as we always end up with 
    % a few NaNs at the end because resampledata doesn't
    % extrapolate.
    
    % In line with the name of this function, we simply replace 
    % those NaNs with the last valid value.
    % There should only be a few NaNs for this application (the
    % final 4 values for each component to be precise).
    
    % We flag up an error if we can't find the NaNs exactly where
    % we expect them
    d = getdata(qdc);
    if find(isnan(d)) ~= [ 259189; 259190; 259191; 259192; 259193; ...
		    259194; 259195; 259196; 259197; 259198; ...
		    259199; 259200 ];
      
      error( sprintf( ['expecting to find 12 NaNs but found something else.'] ));
    end
    
    d(:,86397) = d(:,86396);
    d(:,86398) = d(:,86396);
    d(:,86399) = d(:,86396);
    d(:,86400) = d(:,86396);
    
    if any(isnan(d))
      error( ['Still some NaNs left in QDC, don''t know how to deal' ...
	      ' with this :-('] );
    end
    
    r = setdata( qdc, d );
    
% save('/miadata/summary/aurorawatch/aurorawatchplot_mangle_qdc.mat', 'qdc');
% save('/miadata/summary/aurorawatch/aurorawatchplot_mangle_r.mat', 'r');
return


function r = localSubOneMonth(t)
dv = datevec(t);
dv(2) = dv(2) - 1;
if dv(2) == 0
  dv(2) = 12;
  dv(1) = dv(1) - 1;
end

dm = daysinmonth(dv(1), dv(2));
if dv(3) > dm
  dv(3) = dm;
end
r = timestamp(dv);
return

function r = localSubOneYear(t)
dv = datevec(t);
dv(1) = dv(1) - 1;
dm = daysinmonth(dv(1), dv(2));
if dv(3) > dm
  dv(3) = dm;
end
r = timestamp(dv);
return
