function updateaurorawatchplots(varargin)
%UPDATEAURORAWATCHPLOTS  Update the AuroraWatch activity plots
%
%   UPDATEAURORAWATCHPLOTS(...)
%
% Update the AuroraWatch activity plots. UPDATEAURORAWATCHPLOTS calls
% AURORAWATCHPLOT to make plots of 24 hours duration. 'starttime' and
% 'endtime' may be specified so that multiple day plots are made. starttime
% is normally a TIMESTAMP but may also be one of 'yesterday', 'today',
% 'rolling', or 'rolling&yesterday'. Rolling plots are plots made for the
% latest 24 hours.
%
% A number of options are also recognised to modify the default
% behaviour. See also AURORAWATCH for additional options which are
% recognised. If you wish to make plots which do not follow the normal
% AuroraWatch practice call AURORAWATCHPLOT directly.
%
% Amongst the more useful options, one might find the following:
%
%  starttime  - specify timestamp or any of
%               'rolling+yesterday' 'rolling&yesterday' 
%               'yesterday+rolling' 'yesterday&rolling'
%               'yesterday' 'today' 'rolling'
%               
%               Note that interestingly enough "rolling"
%               will also generate "today". Hence "rolling&yesterday"
%               will actually generate "rolling", "today" and
%               "yesterday".
%
%  instrument - specify a MIA magnetometer object, defaults
%               to mag_lan_1
%
% See also AURORAWATCHPLOT.

% allow option of passing in mag_data to speed up processing when it is
% available. Don't publically advertise it though.
defaults.mag_data = [];

defaults.starttime = [];
defaults.endtime = [];
defaults.instrument = mag_lan_1;
defaults.resolution = timespan(1, 's');
% defaults.plotresolution = timespan(1, 'm');
% defaults.activityresolution = timespan(15, 'm');
defaults.activityresolution = timespan(60, 'm');

defaults.closefigures = 1;
defaults.components = 'H';
defaults.archive = {'1s', 'preliminary', 'realtime2'};
  % NOTE: see also note on mixed archives below (MG)

% defaults.loadoptions = {};
defaults.failiffilemissing = 0;

% output options
defaults.size = [450 300];

% filenames must be absolute

% MG 2007-04-02:
%
% We include the instrument abbreviation in the directory name
% to allow "aurorawatch" plots for different instruments
%
% To do this, we have a peek into the varargins to determine
% the requested instrument. This will still allow people to
% override the default for individual filenames.
[tmpdef unvi] = interceptprop(varargin, defaults);
instr_abbrev = getabbreviation(tmpdef.instrument);

% For "recent" plots (i.e. "yesterday, today, etc...") we
% only use the "realtime2" archive as there appears to be
% a problem with loading data that is partially contained
% in, say, "preliminary" and partially (only) in "realtime2"
if isfield(tmpdef,'starttime')
    if ischar(tmpdef.starttime)
        defaults.archive = {'realtime2'};
    end
end

aw_summary_dir = fullfile(mia_datadir, 'summary', ...
			  'aurorawatch', instr_abbrev);

defaults.dayplotfilename = fullfile(aw_summary_dir, '%Y', '%m', '%Y%m%d.png');
defaults.rollingplotfilename = fullfile(aw_summary_dir, 'rolling.png');
defaults.todaysymlink = fullfile(aw_summary_dir, 'today.png');
defaults.yesterdaysymlink = fullfile(aw_summary_dir, 'yesterday.png');
% could calculate how to get a relative link, but specifying here is easier
defaults.symlinktarget = fullfile('%Y', '%m', '%Y%m%d.png');

% keep the activity file outside of the summary directory as it is quite
% likely that the summary directory will be NFS mounted.
defaults.todayactivityfile = fullfile(mia_datadir, 'aurorawatch', ...
				      ['activity_' instr_abbrev], ...
				      'today.txt');
defaults.yesterdayactivityfile = fullfile(mia_datadir, 'aurorawatch', ...
					  ['activity_' instr_abbrev], ... 
					  'yesterday.txt');

defaults.activityfile =  fullfile(mia_datadir, 'aurorawatch', ...
				  ['activity_' instr_abbrev], ...
				  '%Y', '%m','%Y%m%d.txt');

% Display a comment, useful for debugging or identifying the various cron
% jobs
defaults.comment = '';
[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(defaults.comment)
  disp(sprintf('updateaurorawatchplots (%s): %s', ...
	       getcode(defaults.instrument, true), defaults.comment));
end

now = floor(timestamp('now'), timespan(1, 's'));
% disp(strftime(now, 'time now is %Y-%m-%d %H:%M:%S'));
day = timespan(1, 'd');

if isempty(defaults.starttime)
  defaults.starttime = 'rolling';
end

if isa(defaults.starttime, 'timestamp')
  if ~isvalid(defaults.starttime)
    error('start time not valid');
  end
  starttime = getdate(defaults.starttime(1));

  if ~isempty(defaults.endtime)
    % make many day plots
    if ~isvalid(defaults.starttime)
      error('end time not valid');
    end
    endtime = getdate(defaults.endtime(1));
    t = starttime;
    while t < endtime
      feval(mfilename, varargin{:}, ...
	    'starttime', t, ...
	    'endtime', []);
      t = t + day;
    end
    return
  else
    endtime = starttime + day;
  end
 
  
else
  switch defaults.starttime
   case 'yesterday'
    endtime = getdate(now);
    starttime = endtime - day;
    
   case 'today'
    starttime = getdate(now);
    endtime = starttime + day;
    
   case 'rolling'
    endtime = ceil(now, defaults.activityresolution);
    starttime = endtime - day;
    
   case {'rolling+yesterday' 'rolling&yesterday' 'yesterday+rolling' ...
	 'yesterday&rolling'} 
    % Load up data for two plots at one time, then extract the necessary
    % parts later. This is faster as the rolling plot and yesterday's plots
    % have overlapping data requirements.
    % OK we don't do this after all as it is more awkward and would
    % require a better implementation of extract which is very awkward
    % to do in the "new" "timestamped" version of MIA.
    
     % do rolling plot
    feval(mfilename, varargin{:}, ...
	  'starttime', 'rolling');
    % do yesterday plot
    feval(mfilename, varargin{:}, ...
	  'starttime', 'yesterday');
    return
    
   otherwise
    error(['start time must be ''yesterday'', ''today'', ''rolling'',' ...
	   ' ''rolling&yesterday'' or a valid timestamp']);
  end
end

if isempty(defaults.mag_data)

  md = mag_data('instrument', defaults.instrument, ...
		'starttime', starttime, ...
		'endtime', endtime, ...
		'components', defaults.components, ...
		'log', 0, ...
		'load', 1, ...
		'log', 0, ...
		'loadoptions', ...
		{'archive', defaults.archive, ...
		 'failiffilemissing', defaults.failiffilemissing, ...
		 'multiplearchives', 1, ...
		 'resolution', defaults.resolution });
elseif isa(defaults.mag_data, 'mag_data')
  md = defaults.mag_data;
else
  error('mag_data not empty but not an object of type mag_data');
end

createtime = getcreatetime(md);

if ischar(defaults.starttime) 
  switch defaults.starttime
 
   case 'rolling'
    % also update the today plot
    
    feval(mfilename, varargin{:}, ...
	  'starttime', 'today');
  
  end
end

% after this point data should be for 24 hours and only ever one plot to
% save

% [fh gh] = aurorawatch('mag_data', md, varargin{:});
[fh gh gh2 activity signedActivity abh] = ...
    aurorawatchplot(varargin{:}, ...
		    'activityresolution', defaults.activityresolution, ...
		    'mag_data', md, ...
		    'plottime', now);

if isa(defaults.starttime, 'timestamp') ...
      | any(strcmp(defaults.starttime, {'today', 'yesterday'}))
  filename = strftime(starttime, defaults.dayplotfilename);
  symlinktarget = strftime(starttime, defaults.symlinktarget);
elseif strcmp(defaults.starttime, 'rolling')
  filename = defaults.rollingplotfilename;
else
  error('unknown value for starttime');
end

figure2imagefile(fh, filename, ...
		 'makedirectory', 1, ...
		 'size', defaults.size);

if ischar(defaults.starttime)
  switch defaults.starttime
   case 'today'
    delete(defaults.todaysymlink);
    system(sprintf('ln -s %s %s', symlinktarget, defaults.todaysymlink));
    
    localSaveActivity(defaults.todayactivityfile, signedActivity, ...
		      defaults.activityresolution, starttime, createtime);
    
   case 'yesterday'
    delete(defaults.yesterdaysymlink);
    cmd = sprintf('ln -s %s %s', symlinktarget, defaults.yesterdaysymlink);
    if system(cmd) ~= 0
      warning(sprintf('Could not run command: %s', cmd));
    end

    localSaveActivity(defaults.yesterdayactivityfile, signedActivity, ...
		      defaults.activityresolution, starttime, createtime);

   case 'rolling'
    ; % do nothing
    % update the activity symlink
    
  end
  % save(fullfile(filesep, 'miadata', 'summary', 'aurorawatch', date));
else
  localSaveActivity(strftime(starttime, defaults.activityfile), ...
		    signedActivity, defaults.activityresolution, ...
		    starttime, createtime);
end 


if logical(defaults.closefigures)
  close(fh)
end



function localSaveActivity(filename, activity, activityresolution, starttime, createtime)

% ensure the directory for acitivity information exists
mkdirhier(fileparts(filename));

[fid message] = fopen(filename, 'w');
if fid == -1
  error(sprintf('could not open %s: %s', filename, message));
end

act = activity;
act(isnan(act)) = 0;

t = starttime;
for n = 1:length(activity)
  t2 = t + activityresolution;
  if t2 > createtime
    break;
  end
  fprintf(fid, 'ACT     %s      %4.2f\n', ...
	  strftime(t, '%H%M'), act(n));
  t = t2;
end

fclose(fid);
