function r = makeqdc(rio, varargin)
%MAKEQDC  Make a RIOMETER quiet day curve.
%
%   MAKEQDC(rio, ...)
%   rio: RIOMETER object (scalar)
%
% MAKEQDC constructs quiet day curve(s) for the selected riometer. The
% following name/value pairs can be used to modify the default
% behaviour (some are required):
%
%   'time', TIMESTAMP
%   A time within the QDC period. If set it can be used to calculate the
%   default start and end times of the data used to make the QDC.
%
%   'qdcperiodstarttime', TIMESTAMP
%   The start time of the period for which the QDC is valid. Can be
%   computed from 'time'.
%
%   'starttime', TIMESTAMP
%   Defines the start time of the data used to create the QDC. Default
%   start time can be computed from 'time'.
%
%   'endtime', TIMESTAMP
%   Defines the end time of the data used to create the QDC. Default end
%   time can be computed from 'time'.
%
%   'beams', [1x n] NUMERIC
%   Beam numbers of the QDCs to construct. Defaults to all beams if unset
%   or empty.
%
%   'beamsatonce', NUMERIC (scalar)
%   When loading the input data defines how many beams should be read at
%   once. Defaults to the number of beams, but can be reduced to minimise
%   peak memory usage.
%
%   'savepathsuffix', CHAR
%   The suffix added to the default path. Defaults to '.new', so that
%   unchecked QDCs are not stored in the standard directories. See
%   CHECKQDC.
%
%   'qdcclass', CHAR
%   The CLASS of QDC object. Defaults to the value given by INFO(rio,
%   'qdcclass'). See also riometer/INFO for more details.
%
%   'qdcresolution', TIMESPAN
%   The resolution of the QDC object. Default to INFO(rio,
%   'bestresolution'). See also riometer/INFO for more details.
%
%   'archive', CHAR
%   The default archive for loading data. It is assumed the same archive
%   will be used when saving the newly created QDC. For most instruments
%   it will be ignored.
%
%   'exclude', [n x 2] TIMESTAMP
%   A list of periods which should be not be used when constructing the
%   QDCs. Each row defines one period, given by the inclusive (>=) start
%   time and exclusive ( <) end time.
%
%   'createmethod', CHAR or CELL
%   The algorithm to use for creating QDCs. Defaults to
%   UPPERENVELOPETOQDC. If createmethod is a CELL array the first element
%   should be the create method function name, and the remaining elements
%   are name/value pairs which are passed to the create method as extra
%   parameters, thereby allowing the default behaviour of the
%   createmethod to be modified.
%
%   'prefilter', CELL
%   A list of filters to be applied to the RIO_POWER or RIO_RAWPOWER data,
%   before the createmethod is called. Filters can be given as CHARs
%   (function names), FUNCTION_HANDLEs or as objects of a class which
%   implements the FILTER method (eg of classes derived from
%   MIA_FILTER_BASE).
%
%   'postfilter', MIA_FILTER_BASE
%   A list of filters to be applied to the quiet-day curve data object
%   returned by the createmethod. Filters can be given as CHARs (function
%   names), FUNCTION_HANDLEs or as objects of a class which implements the
%   FILTER method (eg of classes derived from
%   MIA_FILTER_BASE). Post-filtering is applied before conversion to a
%   RIO_QDC_FFT/RIO_RAWQDC_FFT object.
%
%   'fitorder', NUMERIC
%   If fit order is non-zero then the newly-created QDC is converted to a
%   RIO_QDC_FFT or RIO_RAWQDC_FFT as appropriate, with the given
%   fitorder. See RIO_QDC_FFT or RIO_RAWQDC_FFT. If the fit order is zero
%   the QDC is not converted. If the fitorder is empty or NAN then the
%   default fitorder given by CALCNUMRIOQDCFFTCOEFFS. Fit order can be
%   either scalar or a vector the same size as beams.
%
%   'preserveoriginalqdc', LOGICAL
%   Determines if the original QDC object should be preserved inside the
%   RIO_QDC_FFT or RIO_RAWQDC_FFT object. Default is 1.
%
%   'dataquality', CHAR or CELL
%   Any data quality flags which should be included in the QDC. The
%   default is to set 'Preliminary', whihc is then removed after checking
%   with CHECKQDC. See also SETDATAQUALITY, particularly the comment
%   regarding avoiding a proliferation of data quality warnings.
%
%   'powerclass', CHAR
%   Name of class to use when loading received power data. Default is '',
%   meaning it is computed automatically based on 'qdcclass'.
%
%   'powerloadoptions', CELL
%   If not empty a list of loadoptions to be passed to be passed when
%   loading RIO_POWER or RIO_RAWPOWER data. Default is {}.
%
%   'powerfilename', CHAR
%   If not empty the intermediate power date is saved to the file defined by
%   powerfilename. powerfilename may also be a timestamp/STRFTIME format
%   string (where the time is the qdc period start time). In addition to the
%   standard STRFTIME format specifiers, %Q can be used to output the beam
%   numbers, and %c to output the riometer code (see GETCODE).

%
% MAKEQDC is a generic function to create QDCs. It is responsible for
% loading the data, prefiltering the data, postfiltering the QDC and
% converting to a RIO_QDC_FFT or RIO_RAWQDC_FFT object, and saving the
% final QDC. MAKEQDC delegates the conversion of RIO_POWER or
% RIO_RAWPOWER data to a QDC to a help function.
%
% See also CHAUVENETUPPERENVELOPETOQDC, UPPERENVELOPETOQDC, MODETOQDC,
% RIO_QDC_FFT, RIO_RAWQDC_FFT.

function_starttime = timestamp('now');

% a time for which the QDC is valid for (any time within the QDC period)
defaults.time = []; 

% the beginning of the normal QDC start time. This is the time under
% which the QDC is saved. Default to standard QDC start time
defaults.qdcperiodstarttime = [];


defaults.mia = [];
% start and end times of the period used. These are computed but may be
% overriden
defaults.starttime = [];
defaults.endtime = [];
defaults.beams = [];
defaults.beamsatonce = [];

defaults.savefilename = ''; % defer until real instrument is known
defaults.savepathsuffix = '.new';
defaults.qdcclass = '';
defaults.qdcresolution = [];
defaults.qdcarchive = '';

defaults.exclude = [];
defaults.createmethod = 'upperenvelopetoqdc';
defaults.prefilter = {};
defaults.postfilter = {};
defaults.dataquality = 'Preliminary';

defaults.fitorder = 0;
defaults.preserveoriginalqdc = 1;

defaults.powerclass = ''; % compute automatically based on qdcclass
defaults.powerloadoptions = {};
defaults.powerfilename = '';
defaults.powerarchive = '';
defaults.failiffilemissing = 0;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.time)
  error('time must be specified');
end
if isempty(defaults.qdcclass)
  defaults.qdcclass = info(rio, 'qdcclass');
end


if isempty(defaults.mia)
  % load data
  if isempty(defaults.beams)
    defaults.beams = info(rio, 'allbeams');
  end
  defaults.beams = unique(defaults.beams); % also in ascending order

else
  if getinstrument(defaults.mia) ~= rio
    error('supplied data does not match instrument');
  end
  defaults.starttime = getstarttime(defaults.mia);
  defaults.endtime = getendtime(defaults.mia);
  defaults.beams = getbeams(defaults.mia);
end

numOfBeams = numel(defaults.beams);
if isempty(defaults.beamsatonce)
  defaults.beamsatonce = numOfBeams;
end

% check exclude is formatted correctly
if isempty(defaults.exclude)
  ; % ignore
elseif ~isa(defaults.exclude, 'timestamp') ...
      | ndims(size(defaults.exclude)) > 2 ...
      | size(defaults.exclude, 2) ~= 2
  error('exclude must be an n by 2 array of timestamps');
end


df_qdc = info(rio, ...
	      'defaultfilename', defaults.qdcclass, ...
	      'qdcarchive', defaults.qdcarchive);

if isempty(defaults.qdcresolution)
  % defaults.qdcresolution = info(rio, 'bestresolution');
  defaults.qdcresolution = df_qdc.resolution;
end

if isempty(defaults.savefilename)
  [Path fname ext] = fileparts(df_qdc.fstr);
  defaults.savefilename = ...
      fullfile([Path defaults.savepathsuffix], [fname ext]);
end




qdcst = calcqdcstarttime(rio, defaults.time, defaults.time);
qdcet = qdcst + df_qdc.duration;

if isempty(defaults.starttime)
  defaults.starttime = qdcst;
end
if isempty(defaults.endtime)
  defaults.endtime = qdcet;
end
if isempty(defaults.qdcperiodstarttime)
  defaults.qdcperiodstarttime = qdcst;
end



% get the name of the QDC processing method, and any options which
% should be passed to it
if ischar(defaults.createmethod) ...
      | isa(defaults.createmethod, 'function_handle')
  createmethod = defaults.createmethod;
  createargs = {};
elseif iscell(defaults.createmethod)
  createmethod = defaults.createmethod{1};
  createargs = defaults.createmethod(2:end);
else
  error('incorrect create method');
end
    
if isempty(defaults.powerclass)
  if isa(feval(defaults.qdcclass), 'rio_qdc')
    defaults.powerclass = 'rio_power';
  elseif isa(feval(defaults.qdcclass), 'rio_rawqdc')
    defaults.powerclass = 'rio_rawpower';
  else
    error(sprintf('cannot identify correct power class for %s', ...
		  defaults.qdcclass));
  end
end

if isempty(defaults.fitorder)
  defaults.fitorder = nan;
end
if length(defaults.fitorder) == 1
  defaults.fitorder = repmat(defaults.fitorder, size(defaults.beams));
end
 
% Load the data. To minimise the number of passes needed to load the
% necessary files allow for multiple beams to be loaded at once.
for n = 1:defaults.beamsatonce:numOfBeams
  beams = defaults.beams(n:min(n+defaults.beamsatonce-1, numOfBeams));
  disp(sprintf('Processing for beam(s) %s', printseries(beams)));
  
  if isempty(defaults.mia)
    % load the data
    mia = feval(defaults.powerclass, ...
		'instrument', rio, ...
		'starttime', defaults.starttime, ...
		'endtime', defaults.endtime, ...
		'beams', beams, ...
		'load', 1, ...
		'log', 0, ...
		'loadoptions', ...
		{defaults.powerloadoptions{:}, ...
		 'archive', defaults.powerarchive, ...
		 'failiffilemissing', defaults.failiffilemissing});

    if ~isempty(defaults.powerfilename)
      % save intermediate data to a file
      s.Q = printseries(beams, [], ',');
      s.c = getcode(rio);
      tmpfilename = strftime(defaults.qdcperiodstarttime, ...
                             defaults.powerfilename, ...
                             'additionalreplacements', s);
      % map any spaces to underscores
      tmpfilename(tmpfilename == ' ') = '_';
      disp(sprintf('saving intermediate data to %s', tmpfilename));
      url_save(tmpfilename, 'mia');
    end
    
  else
    mia = defaults.mia;
  end
  
  % discard any details about processing done to power object, as it is not
  % relevant to be stored in the QDC.
  mia = setprocessing(mia, {});
  
  % loop through all beams, one by one, to minimise peak memory usage
  for bn = 1:numel(beams)
    % extract desired beam, and remove any unwanted periods
    mia2 = extract(mia, ...
		   'beams', beams(bn), ...
		   'exclude', defaults.exclude);
  
    if any(strcmp(class(getdata(mia2)), {'uint8' 'uint16' 'uint32'}))
      data = double(getdata(mia2));
      data(data == 0) = nan;
      mia2 = setdata(mia2, data);
      clear data;
    end

    % Ensure data is spaced regularly
    mia2 = spaceregularly(mia2);

    
    % pre-filter the power data; note it might be necessary add
    % 'spaceregularly' calls after using certain filter options
    if ~isempty(defaults.prefilter)
      disp('Pre-filtering data');
      mia2 = mia_filter(mia2, defaults.prefilter);
    end

    % Ensure data is spaced regularly
    mia2 = spaceregularly(mia2);

    % Call the create function, take a note of how long it took. Pass any extra
    % information from us at the end of the argument list, as interceptprop
    % will use those values to intercept anything which may have been in the
    % user-suplied data.
    qdcperiodendtime = defaults.qdcperiodstarttime + df_qdc.duration;
    disp(sprintf('Calling %s', createmethod));
    qdc_function_starttime = timestamp('now');
    qdc = feval(createmethod, mia2, createargs{:}, ...
		'qdcclass', defaults.qdcclass, ...
		'qdcresolution', defaults.qdcresolution, ...
		'qdcperiodstarttime', defaults.qdcperiodstarttime, ...
    		'qdcperiodendtime', qdcperiodendtime);
    qdc_function_endtime = timestamp('now');

    disp(sprintf('Creating QDC for beam %d took %s', beams(bn), ...
		 char(qdc_function_endtime - qdc_function_starttime)));
    
    % post-filter the QDCs
    if ~isempty(defaults.postfilter)
      disp('Post-filtering data');
      qdc = mia_filter(qdc, defaults.postfilter);
    end

    % add any data quality warnings
    if ~isempty(defaults.dataquality)
      qdc = adddataquality(qdc, defaults.dataquality);
    end

    fitorder = defaults.fitorder(bn);
    if isnan(fitorder)
      % compute fitorder to suit the instrument, resolution and beam
      fitorder = calcnumrioqdcfftcoeffs(rio, ...
					'beams', beams(bn), ...
					'resolution', defaults.qdcresolution);
    end
    
    qdcclass = class(qdc);
    if fitorder ~= 0 & ~strcmp(qdcclass((end-3):end), '_fft')
      % convert to the Fourier transform version of the QDC (if not already)
      
      % allow for the chance to have rio_rawqdc_fft as well as
      % rio_qdc_fft
      qdc = feval([defaults.qdcclass '_fft'], qdc, ...
                  'fitorder', fitorder, ...
                  'preserveoriginalqdc', defaults.preserveoriginalqdc);
    end
    
    % ensure QDCs are saved with a name which matches their qdcperiod start
    % time, not the actual start time
    savefilename = beamstrftime(defaults.qdcperiodstarttime, ...
				beams(bn), ...
				defaults.savefilename);
    save(qdc, savefilename);
  end
  
end

function_endtime = timestamp('now');
disp(sprintf('Made %d QDCs in %s', numOfBeams, ...
	     char(function_endtime - function_starttime)));

