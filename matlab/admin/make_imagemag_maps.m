function make_imagemag_maps(varargin)

defaults.map = 'scand';
defaults.coastline = 'gshhs_l';
% defaults.latitude = [50 80];
defaults.latitude = [55 80];
defaults.longitude = [5 32];
defaults.bitmapsize = [512 384];

[defaults unvi] = interceptprop(varargin, defaults);

mags = mag_image;

% two mags at Hankasalmi, delete the first one (too close to show both)
mags(mags == mag_han_2) = [];

currentCol = 'y';
% pastCol = 'b';
pastCol = 'y';
t = floor(timestamp('now'), timespan(1, 'd'));

[fh gh] = scand('latitude', defaults.latitude, ...
		'longitude', defaults.longitude, ...
		'coastline', defaults.coastline);

operatingH = [];
closedH = [];

for n = 1:length(mags)
  m = mags(n);
  ot = info(m, 'operatingtimes');
  current = 1;
  col = currentCol; % assume current unless know otherwise
  if isvalid(ot(2))
    if t > ot(2)
      col = pastCol;
      current = 0;
    end
  end
  
  h = map(m, gh, 'abbreviation', 1, ...
	  'formatabbreviation', 'upper');
  
  set(h(1), 'Color', col, 'Marker', '+', 'LineWidth', 1);
  set(h(2), 'FontSize', 8);
  abbrev = getabbreviation(m);

  switch abbrev
   case {'gml' 'ups' 'oul'}
    set(h(2), 'HorizontalAlignment', 'right');
   case 'kvi'
    set(h(2), 'VerticalAlignment', 'top');
    
  end
    
  if current
    operatingH = h(1);
    % set(h(2), 'FontWeight', 'bold');
  else
    closedH = h(1);
    % set(h(2), 'FontAngle', 'oblique');
  end
end

% lh = legend([operatingH; closedH], char({'In operation'; 'Closed'}), 2);
set(fh, 'Color', 'w');



figure2imagefile(fh, 'image.png', 'size', defaults.bitmapsize);
print(fh, '-depsc2', '-tiff', 'image2.eps');
fname = 'image.eps';
fname2 = 'image.pdf';

print(fh, '-dpdf', fname);
  
print_fmts = print('-d');
if any(any(strcmp(print_fmts, 'pdf')))
  print(fh, '-dpdf', fname2);
else
  unix(sprintf('ps2pdf %s %s', fname, fname2));
end
close(fh);


