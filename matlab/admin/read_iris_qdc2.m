function r = read_iris_qdc2(filename, st)

r = [];


if isempty(filename)
  error('filename not specified');
end

d = dir(filename);
if isempty(d)
  return;
elseif length(d) > 1
  error('Not a file'); % directory listing or filename includes a wildcard?
elseif d.isdir
  error('Directory, not a file');
end

len = d.bytes;

[fid, message] = fopen(filename, 'r', 'ieee-le');
if fid == -1
  error(sprintf('Could not open ''%s'': %s', filename, message));
end

% read header
headerSize = 42;

start_year = fread(fid, 1, 'int16');
if start_year < 80
  start_year = start_year + 2000;
elseif start_year < 100
  start_year = start_year + 1900;
else
  % assume 4 digit year
end

start_month = fread(fid, 1, 'int16'); % 1 - 12
start_day = fread(fid, 1, 'int16'); % 1 - 31
days = fread(fid, 1, 'int16'); % number of days used to create QDC

% station code:
station_code =  fread(fid, 1, 'int16'); 

% longitude is stored in seconds
longitude = fread(fid, 1, 'long');
% convert to double (degrees)
longitude = longitude / 3600;

switch station_code
 case 11
  instrument = rio_dvs_2; % SHIRE

 otherwise
  fclose(fid);
  error(sprintf('unknown station code (was %d, longitude=%g)', ...
		station_code, longitude));
end

% check longitude agrees with that stored in instrument (NB resolution is
% nearest second)

if abs(longitude - getgeolong(instrument)) > 2/3600
  warning(sprintf(['longitude in QDC file (%g) does not agree with ' ...
		   'longitude in instrument (%g)'], longitude, ...
		  getgeolong(instrument)));
end
  


ntime = fread(fid, 1, 'int16'); % number of points in QDC



nvolts = fread(fid, 1, 'int16'); % may not be set in versions created by IDL 

nbeams = fread(fid, 1, 'int16');
vmin = fread(fid, 1, 'float'); % minimum voltage considered (0 Volts)
vmax = fread(fid, 1, 'float'); % maximum voltage cosidered (10 Volts)

%  these may not be set in versions created by IDL 
nwpts = fread(fid, 1, 'int16');
npass = fread(fid, 1, 'int16');
ndiff = fread(fid, 1, 'int16');
rejrat = fread(fid, 1, 'float');
qfold = fread(fid, 1, 'float');



if ftell(fid) ~= headerSize
  headerSize
  pos = ftell(fid) 
  fclose(fid);
  error('could not read header');
end

% dataSize = [49, 1440];
dataSize = [nbeams ntime];
[data count] = fread(fid, dataSize, 'uint16');
if count ~= prod(dataSize)
  fclose(fid);
  error('could not read data section');
end
fclose(fid);

[ibeams wbeams] = info(instrument, 'beams');
beams = sort([ibeams wbeams]);


qst = timestamp([start_year start_month start_day 0 0 0]);
qet = qst + timespan(days, 'd');
res = timespan(1/ntime, 'd');
bestres = info(instrument, 'bestresolution');

if res > bestres
  % change resolution
  p = rio_rawpower('instrument', instrument, ...
		   'starttime', st, ...
		   'endtime', st+res*size(data,2), ...
		   'resolution', res, ...
		   'beams', beams, ...
		   'load', 0, ...
		   'log', 0, ...
		   'data', data, ...
		   'units', 'ADC');
  
  data2 = getdata(setresolution(p, bestres));
else
  data2 = data;
end


% data seems to be a factor of 4 too big
data2 = data2 / 4;

% need to change data around
stoff = round(stoffset(st, getlocation(instrument)) / bestres);
qdclen = ceil(siderealday/bestres);
if stoff == 0
  % no wrap around
  data3 = data2(:, 1:qdclen);
else
  mni = qdclen-stoff; % sidereal midnight index
  data3 = data2(:, [mni+1:qdclen 1:mni]);
end

r = rio_rawqdc('instrument', instrument, ...
	       'starttime', qst, ...
	       'endtime', qet, ...
	       'resolution', bestres, ...
	       'beams', beams, ...
	       'load', 0, ...
	       'log', 0, ...
	       'data', data3, ...
	       'units', 'ADC');


