function [fh,gh,mags,mh] = make_samnet_maps(varargin)

defaults.map = 'scand';
defaults.coastline = 'gshhs_l';
% defaults.latitude = [50 80];
defaults.latitude = [50 70];
defaults.longitude = [-15 40];
defaults.bitmapsize = [512 384];
defaults.filename = 'samnet';

defaults.instrument = mag_samnet;
% mag_nur_1 is too close to mag_nur_3 to bother plotting, so plot only
% the currently operating one
defaults.instrument(defaults.instrument == mag_nur_1) = [];

% mag_han_1 is too close to mag_han_3 to bother plotting, so plot only
% the currently operating one
defaults.instrument(defaults.instrument == mag_han_1) = [];

[defaults unvi] = interceptprop(varargin, defaults);

% mags = [mag_samnet mag_kil_1 mag_han_1 mag_ler_1 mag_esk_1 mag_had_1];


currentCol = 'y';
% pastCol = 'b';
pastCol = 'y';
t = floor(timestamp('now'), timespan(1, 'd'));

[fh gh] = scand('latitude', defaults.latitude, ...
		'longitude', defaults.longitude, ...
		'coastline', defaults.coastline, ...
		varargin{unvi});

operatingH = [];
closedH = [];
numMags = numel(defaults.instrument);
mh = zeros(numMags, 2);

for n = 1:numMags
  m = defaults.instrument(n);
  ot = info(m, 'operatingtimes');
  current = 1;
  col = currentCol; % assume current unless know otherwise
  if isvalid(ot(2))
    if t > ot(2)
      col = pastCol;
      current = 0;
    end
  end
  
  h = map(m, gh, 'abbreviation', 1, ...
	  'formatabbreviation', 'upper');
  
  set(h(1), 'Color', col, 'Marker', '+', 'LineWidth', 1);
  set(h(2), 'FontSize', 8);
  abbrev = getabbreviation(m);

  % fix horizontal alignment
  switch abbrev
   case {'gml' 'ups' 'oul' 'lan'}
    set(h(2), 'HorizontalAlignment', 'right');
  end
    
  % fix vertical alignment
  switch abbrev
   case {'kvi' 'lan' 'yor'}
    set(h(2), 'VerticalAlignment', 'top');
  end
  
  if current
    operatingH = h(1);
    % set(h(2), 'FontWeight', 'bold');
  else
    closedH = h(1);
    % set(h(2), 'FontAngle', 'oblique');
  end
  mh(n, :) = h;
end

% lh = legend([operatingH; closedH], char({'In operation'; 'Closed'}), 2);
set(fh, 'Color', 'w');

[Path filename ext] = fileparts(defaults.filename);
if ~isempty(filename)
  disp(sprintf('saving to to %s.{png,eps,pdf}', fullfile(Path, filename)));

  fnameImg = fullfile(Path, [filename '.png']); 
  fname = fullfile(Path, [filename '.eps']); 
  fname2 = fullfile(Path, [filename '.pdf']); 
  
  % figure2imagefile(fh, fname, ...
  figure2imagefile(fh, fnameImg, ...
		   'size', defaults.bitmapsize);
  print(fh, '-depsc2', fname)
  print(fh, '-dpdf', fname2);
  
  print_fmts = print('-d');
  if any(any(strcmp(print_fmts, 'pdf')))
    print(fh, '-dpdf', fname2);
  else
    unix(sprintf('ps2pdf %s %s', fname, fname2));
  end

  if nargout == 0
    close(fh);
  end
else
  disp('not saving');
end

mags = defaults.instrument;
