function makeriomaps(rio, varargin)
%MAKERIOMAPS  Make riometer maps suitable for riometer database webpages.
%
%   MAKERIOMAPS(rio, ...)
%   rio: RIOMETER object(s)
%
%   Make maps of the riometer location, and if possible, the beam projection
%   and field of view (for km_antenna coordinate system). rio may be a
%   vector, in which case multiple sets of maps are created. By default maps
%   are created in EPS, PDF (if possible) and PNG formats.
%
%   The default behaviour can be modified with the following parameter
%   name/value pairs:
%
%     'axes', axes handle
%     The handle of the AXES to use. If empty a new figure will be opened.
%
%     'path', CHAR
%     The directory (path) to save the maps into. Note that the filename
%     is fixed, and is created from the RIOMETER's abbreviation and
%     serial number, with the extension being dependent upon the image
%     format.
%
%     'xsize', [1x2 DOUBLE]
%     'ysize', [1x2 DOUBLE]
%     The extent of the map from the RIOMETER LOCATION, measured in
%     metres (SI units of distance).
%
%     'coastline', CHAR 
%     The coastline style to use in the M_MAP functions. Default is M_MAP's
%     default coastline. Other valid options are 'gshhs_c', 'gshhs_l',
%     'gshhs_i', 'gshhs_h' and 'gshhs_f'. (GSHHS, coarse, low, intermediate,
%     high and full resolutions, respectively.)
%
%     'land', COLORSPEC
%     'sea', COLORSPEC
%     'lake', COLORSPEC
%     'landedgecolor', COLORSPEC
%     'beamcolor', COLORSPEC
%     The color to use for land, sea, inland waters, the land edge color
%     and beam colors respectively. Note that 'lake' is supported only
%     for the GSHHS coastline styles.
%
%     'bitmapformat', CHAR
%     The format to use for the bitmap images, default is 'png'.
%
%     'bitmapsize', [1x2 DOUBLE]
%     The width and height of the bitmap images (in pixels).
%
%     'eliminategratinglobes', LOGICAL
%     Eliminate grating lobes. Grating lobes are detected by
%     BEAMPROJECTION as multiple contours for one beam outline. This
%     options allows grating lobes to eliminated, which improves the
%     clarity for some beam patterns.
%
%     'closefigure', LOGICAL
%     Determines if FIGUREs should be closed after the maps have been
%     saved to the image files. Default is true (close the figures).
%
%     See also M_MAP, RIOMETER, PLOTBEAMPROJECTION, PLOTFOV.



%  

% loop through all riometers one by one, don't stop on errors
if length(rio) > 1
  cmd = sprintf('%s(rio(n), varargin{:});', mfilename);
  failures = repmat(riometer, 0, 0);
  for n = 1:prod(size(rio))
    % feval(mfilename, rio(n), varargin{:});
    disp(sprintf('Making maps for %s', char(rio(n))));
    eval(cmd, 'failures(end+1)=rio(n);');
  end
  for n = 1:length(failures)
    disp(sprintf('Could not process %s', char(failures(n))));
  end
  return;
end

defaults.axes = [];

defaults.path = pwd;
defaults.xsize = [-320 +320] .* 1e3; 
defaults.ysize = [-320 +320] .* 1e3;
defaults.coastline = 'gshhs_i';
% defaults.land = [0 0.6 0];
defaults.land = [];
defaults.sea = [0 0.8 1];

defaults.lake = [0 0.6 0.9];
defaults.landedgecolor = 'none';
defaults.beamcolor = '';
defaults.bitmapformat = 'png';
defaults.bitmapsize = [480 400];

defaults.beams = []; % defer
defaults.beamnumbering = []; % defer
defaults.eliminategratinglobes = 1;

defaults.latitude = [];
defaults.longitude = [];

defaults.closefigure = 1;
[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.beams)
  defaults.beams = info(rio, 'allbeams');
end

if getgeolat(getlocation(rio)) < -30
  disp('nasty hack to get coastlines for Antarctica');
  defaults.coastline = '';
end


h = info(rio, 'defaultheight');
loc = getlocation(rio);

[X Y] = meshgrid(defaults.xsize, defaults.ysize);
% rotate mesh to point in same direction as antenna. Remember that the
% antenna pointing direction is degrees CW (as compass), and trig needs
% radians CCW
[th phi r] = cart2sph(X, Y, 0);
[XX YY ZZ] = sph2cart(th - info(rio, 'antennaazimuth') .* (pi/180), ...
		      phi, r);

[lon lat] = xy2longlat(XX, YY, h, loc);
lon2 = [min(lon(:)) max(lon(:))];
lat2 = [min(lat(:)) max(lat(:))];

if ~isempty(defaults.latitude)
  lat2 = defaults.latitude;
end
if ~isempty(defaults.longitude)
  lon2 = defaults.longitude;
end

if isempty(defaults.beamcolor)
  if getgeolat(loc) >= 0
    bc = 'y';
  else
    bc = 'r';
  end
else
  bc = defaults.beamcolor;
end

if isempty(defaults.land)
  if getgeolat(loc) >= -30
    land = [0 0.6 0];
  else
    land = [1 1 1];
  end
else
  land = defaults.land;
end

[fh gh] = scand('axes', defaults.axes, ...
		'longitude', lon2, ...
		'latitude', lat2, ...
		'land', land, ...
		'sea', defaults.sea, ...
		'lake', defaults.lake, ...
		'coastline', defaults.coastline);


set(fh, 'Color', 'w');

h = plotbeamprojection(rio, gh, ...
		       'beams', defaults.beams, ...
		       'eliminategratinglobes', ...
		       defaults.eliminategratinglobes, ...
		       'lineargs', {'Color', bc}, ...
		       'patchargs', {'EdgeColor', bc, 'FaceColor', 'none'});

do_plot_fov = false;
s = info(rio, '_struct');
if isfield(s.pixels, 'km_antenna')
  do_plot_fov = true;
end

if do_plot_fov
  if 1
    plotfov(rio, gh, 'units', 'km_antenna');
  else
    % UNTESTED
    % plot the fields of view for the coordinates systems
    fov.km_antenna = 'k';
    fov.aacgm = 'r';
    fov.deg = 'g';
    fn = fieldnames(fov);
    for n = 1:length(fn)
      fovh = [];
      cmd = sprintf('fovh = plotfov(rio, gh, ''units'', ''%s'');', fn{n});
      eval(cmd);
      if ~isempty(fovh)
	set(fovh, 'EdgeColor', getfield(fov, fn{n}));
      end
    end
  end
end

[x y] = info(rio, 'beamlocations', ...
	     'beams', defaults.beams, ...
	     'units', 'deg');
bh = m_line(x, y, 'Color', bc, 'Marker', '+', 'Linestyle', 'none');
rh = m_line(getgeolong(loc), getgeolat(loc), 'Color', 'k', 'Marker', 'x');

if rio == rio_ram_2
  set(bh, 'Marker', '.', 'MarkerSize', 3);
end


% plot the beam number for the corner beams
[ib wb] = info(rio, 'beams');

if isempty(defaults.beamnumbering)
  defaults.beamnumbering = (numel([ib(:); wb(:)]) > 1)
end

if defaults.beamnumbering
  beamplan = info(rio, 'beamplan');
  beams = unique(beamplan([1 end], [1 end]))';
  
  [x y] = info(rio, 'beamlocations', 'units', 'deg', 'beams', beams);
  
  lat = getgeolat(getlocation(rio));
  lon = getgeolong(getlocation(rio));
  for n = 1:length(beams)
    if x(n) < lon
      ha = 'right';
    else
      ha = 'left';
    end
    if y(n) < lat
      va = 'top';
    else
      va = 'bottom';
    end
    m_text(x(n), y(n), num2str(beams(n)), ...
	   'HorizontalAlignment', ha, ...
	   'VerticalAlignment', va, ...
	   'FontSize', 14);
  end
end


fname =  fullfile(defaults.path, sprintf('%s_%d.%s', ...
					 getabbreviation(rio), ...
					 getserialnumber(rio), ...
					 defaults.bitmapformat));

figure2imagefile(fh, fname, 'size', defaults.bitmapsize);



fname =  fullfile(defaults.path, sprintf('%s_%d.eps', ...
					 getabbreviation(rio), ...
					 getserialnumber(rio)));
print(fh, '-depsc2', fname);

fname2 =  fullfile(defaults.path, sprintf('%s_%d.pdf', ...
					  getabbreviation(rio), ...
					  getserialnumber(rio)));

print_fmts = print('-d');
if any(any(strcmp(print_fmts, 'pdf')))
  print(fh, '-dpdf', fname2);
else
  unix(sprintf('ps2pdf %s %s', fname, fname2));
end

if isempty(defaults.axes) & defaults.closefigure
  close(fh);
end


