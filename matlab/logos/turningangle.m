function r = turningangle(x, y)
%TURNINGANGLE Find angle through which a path turns
%
% r = TURNINGANGLE(x, y)
% r: angle
% x: x coordinates
% y: y coordinates
%
% TURNINGANGLE measures the angle through which a path turns, hence it
% can be used to determine whether a path turns clockwise or
% anticlockwise.
%

% first and last elements are supposed to be the same point
xdiffs = x(1:(end-1)) - x(2:end);
ydiffs = y(1:(end-1)) - y(2:end);
a = atan2(ydiffs, xdiffs);
adiff = myunwrap(a - a([end 1:(end-1)]));
r = sum(adiff);

function r = myunwrap(a)
r = a;
% idx = (r > 2*pi);
% r(idx) = rem(r(idx), 2*pi);
r = rem(r, 2*pi);

idx = (r > pi);
r(idx) = r(idx) - 2 * pi;

idx = (r < -pi);
r(idx) = r(idx) + 2 * pi;


