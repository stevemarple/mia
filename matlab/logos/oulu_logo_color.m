function [fh,ah] = oulu_logo_color(varargin)

[fh ah] = oulu_logo(varargin{:});

blue = [0.06 .23 .46];
grey = [.75 .75 .75];
% grey = [1 1 1];

set(findall(ah, 'FaceColor', [0 0 0]), ...
    'Facecolor', blue);
set(findall(ah,'Tag','oulu_logo.mma: 10'), 'FaceColor', grey);
set(findall(ah,'Tag','oulu_logo.mma: 11'), 'FaceColor', grey);
set(findall(ah,'Tag','oulu_logo.mma: 12'), 'FaceColor', grey);
set(findall(ah,'Type','line'),'Units', 'data', 'LineWidth', 10, 'Color', grey);


 
