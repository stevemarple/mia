function varargout = fixtextpatches(varargin)
%FIXTEXTPATCHES Convert patches in letters to holes.
%
% FIXTEXTPATCHES is used by the perl program mma2mfile to fix letters
% which should contain holes. It finds the closest point between the two
% paths and cuts out the hole, ensuring that the inside path (the hole)
% turns in the opposite direction to the outside path.
%
% See also TURNINGANGLE.

h = [];
if nargin == 1 & all(ishandle(varargin{1}))
  % array of handles. Outermost patch should be the first
  h = varargin{1};
  if length(h) < 2
    error('require 2 or more handles');
  end
  x = cell(size(h));
  y = cell(size(h));
  for n = 1:numel(h)
    x{n} = get(h(n), 'XData');
    y{n} = get(h(n), 'YData');
  end
else
  error('incorrect parameters');
end

num = numel(h);

% ensure the inside paths turn in the opposite direction
sgn = sign(turningangle(x{1}([1:end 1]), y{1}([1:end 1])));
for n = 2:num
  if sign(turningangle(x{n}([1:end 1]), y{n}([1:end 1]))) == sgn
    x{n} = x{n}(end:-1:1);
    y{n} = y{n}(end:-1:1);
  end
end

for n = 2:num
  % find the pair of points on the outside and inside which are closest
  Xo = repmat([x{1}(:)], 1, numel(x{n}));
  Yo = repmat([y{1}(:)], 1, numel(x{n}));

  Xi = repmat([x{n}(:)]', numel(x{1}), 1);
  Yi = repmat([y{n}(:)]', numel(x{1}), 1);
  % calulate distance squared (no need to find true distance, just need
  % closest points so don't waste time taking square roots)
  dist = power(Xo-Xi, 2) + power(Yo-Yi, 2);
  [tmp idx] = min(dist(:));
  [outIdx inIdx] = ind2sub(size(dist), idx(1));
  
  if 0
    % debug code to show closest points
    line('Parent', get(h(1), 'Parent'), ...
	 'XData', x{1}(outIdx), ...
	 'YData', y{1}(outIdx), ...
	 'Marker', '+', ...
	 'Color', 'r');
    line('Parent', get(h(1), 'Parent'), ...
	 'XData', x{n}(inIdx), ...
	 'YData', y{n}(inIdx), ...
	 'Marker', 'x', ...
	 'Color', 'r');
    set(gcf, 'Visible', 'on');
  end
  
  % now insert the inside path into the outside path. The two closest points
  % are duplicated so that the perimeter travels from the outside to the
  % inside, round in the opposite direction (keeping the filled area on the
  % same side of the line). The path rejoins the outside perimeter by
  % following the same path used to travel to the inside.
  x{1} = [x{1}(1:outIdx); x{n}([inIdx:end 1:inIdx]); x{1}(outIdx:end)];
  y{1} = [y{1}(1:outIdx); y{n}([inIdx:end 1:inIdx]); y{1}(outIdx:end)];
end

if ~isempty(h)
  if 1
    set(h(1), 'XData', x{1}, 'YData', y{1});
    delete(h(2:end));
  else
    size(Xo)
    h1 = copyobj(h(1), get(h(1), 'Parent'));
    set(h(1), 'FaceColor', 'none', 'EdgeColor', 'm');
    set(h(2:end), 'FaceColor', 'none', 'EdgeColor', 'g');
    set(h1, 'XData', x{1}, 'YData', y{1}, ...
	    'EdgeColor', 'b', ...
	    'FaceColor', 'none', ...
	    'Tag', [get(h(1), 'Tag') 'a']);
  end
end
