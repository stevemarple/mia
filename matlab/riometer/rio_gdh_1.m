function r = rio_gdh_1
%RIO_GDH_1  RIOMETER object for Godhavn, Greenland.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'gdh', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'DMI riometer chain', ...
    'location', location('Godhavn', 'Greenland', ...
                         69.300000, -53.500000), ...
    'frequency', []);

