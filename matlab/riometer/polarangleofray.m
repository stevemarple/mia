function [alpha, Re] = polarangleofray(theta, h)
%POLARANGLEOFRAY Convert zenith angle taking into account a curved ionosphere.
%
%   r = POLARANGLEOFRAY(theta, h);
%
%   r: angle at centre of Earth (radians)
%   theta: zenith angle (radians)
%   h: height (metres)
%

% For a given angle from the zenith (in radians) return the angle at the
% Earth's centre between the point on the ionosphere where the ray
% intercepts the ionosphere and IRIS.

% Maple output:
% 
% eq := (Re+h)*cos(a) = Re + (((Re+h)*sin(a))/tan(theta));
%                                               (Re + h) sin(a)
%                  eq := (Re + h) cos(a) = Re + ---------------
%                                                 tan(theta)
% solve(eq,a);
% bytes used=1107804, alloc=720764, time=0.68
% bytes used=2108208, alloc=1441528, time=1.99
%                                                 -2 Re + 2 %1
%                                       Re + 1/2 ---------------
%                                                              2
%           (-2 Re + 2 %1) tan(theta)            1 + tan(theta)
% arctan(1/2 --------------------------, ------------------------),
%                          2                     Re + h
%           (1 + tan(theta) ) (Re + h)
%
%                                                     -2 Re - 2 %1
%                                           Re + 1/2 ---------------
%                                                                  2
%               (-2 Re - 2 %1) tan(theta)            1 + tan(theta)
%    arctan(1/2 --------------------------, ------------------------)
%                              2                     Re + h
%               (1 + tan(theta) ) (Re + h)
%
%         2             2               2                  2  2 1/2
%%1 := (Re  + 2 Re h + h  + 2 tan(theta)  Re h + tan(theta)  h )
%

% Radius of Earth in metres
Re = earthradius; % take a local copy


h2 = h .* h;

subterm = sqrt((Re.*Re) + (2.*Re.*h) + h2 ...
+ (2 .* tan(theta) .* tan(theta) .* Re .* h) ...
+ (tan(theta) .* tan(theta) .* h2)...
);


% result 1, real part
result1r = atan(...
(  (-2 .*Re + 2 .* subterm)   .*   tan(theta)  )   ./ ...
(2   .*   (1 + tan(theta) .* tan(theta))   .*   (Re+h) )...
);

%% result 1, complex part
%result1c = atan(...
%(Re + 0.5*( (-2*Re + 2*subterm) / (1 + tan(theta)*tan(theta)) )  ) ...
%/...
%(Re+h)...
%)

%% result 2, real part
%result2r = atan(...
%(  (-2*Re - 2*subterm)   *   tan(theta)  )   /...
%(2   *   (1 + tan(theta)*tan(theta))   *   (Re+h) )...
%)

%% result 2, complex part
%result2c = atan(...
%(Re + 0.5*((-2*Re - 2*subterm)/(1 + tan(theta)*tan(theta))) )/...
%(Re+h) )

% total circumference of Earth = 2*pi*Re
% fraction of circumference (radians) = a/(2*pi)
% fraction of circumference (km) = (a/(2*pi)) * (2*pi*Re)
%curved_dist = result1r * Re

alpha = result1r;
return;
