function r = info_rio_spa_4_data
%INFO_RIO_SPA_4_DATA Return basic information about rio_spa_4.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_spa_4. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=spa;serialnumber=4

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.limits.rio_rawqdc = [0 4096];
r.pixels.deg.x.max = -23.2;
r.pixels.deg.x.min = -29.5;
r.pixels.deg.x.step = 0.3;
r.pixels.deg.y.max = -74.8;
r.pixels.deg.y.min = -76.4;
r.pixels.deg.y.step = 0.1;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'spa';
r.antennaazimuth = 150.5;
r.antennaphasingx = [-0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 ...
    -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 ...
    -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 ...
    -0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 ...
    0.75];
r.antennaphasingy = [0.75 0.75 0.75 0.75 0.75 0.75 0.75 0.5 0.5 0.5 ...
    0.5 0.5 0.5 0.5 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0 0 0 0 0 0 ...
    0 -0.25 -0.25 -0.25 -0.25 -0.25 -0.25 -0.25 -0.5 -0.5 -0.5 -0.5 ...
    -0.5 -0.5 -0.5 -0.75 -0.75 -0.75 -0.75 -0.75 -0.75 -0.75];
r.antennas = [8 8];
r.antennaspacing = 0.5;
r.azimuth = [-45 -33.7 -18.6 0 18.6 33.7 45 -56.3 -45 -26.6 0 26.6 ...
    45 56.3 -71.4 -63.4 -45 0 45 63.4 71.4 -90 -90 -90 0 90 90 90 ...
    -108.6 -116.6 -135 180 135 116.6 108.6 -123.7 -135 -153.4 180 ...
    153.4 135 123.7 -135 -146.3 -161.4 180 161.4 146.3 135];
r.badbeams = [1 7 43 49];
r.beamplanc = 7;
r.beamplanr = 7;
r.beamwidth = [12.461 13.8935 13.3758 13.1156 13.3758 13.8935 ...
    12.461 13.8935 12.8756 12.1678 11.95 12.1678 12.8756 13.8935 ...
    13.3758 12.1678 11.5448 11.3729 11.5448 12.1678 13.3758 13.1156 ...
    11.95 11.3729 11.169 11.3729 11.95 13.1156 13.3758 12.1678 ...
    11.5448 11.3729 11.5448 12.1678 13.3758 13.8935 12.8756 12.1678 ...
    11.95 12.1678 12.8756 13.8935 12.461 13.8935 13.3758 13.1156 ...
    13.3758 13.8935 12.461];
r.bibliography = 'art:67';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = 'crossed-dipole';
r.ibeams = 49;
r.id = 14;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49];
r.latitude = -90;
r.location1 = 'South Pole';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 0;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 28;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 4;
r.standardobliquity = [2.574 1.817 1.502 1.423 1.502 1.817 2.574 ...
    1.817 1.355 1.187 1.143 1.187 1.355 1.817 1.502 1.187 1.065 ...
    1.031 1.065 1.187 1.502 1.423 1.143 1.031 1 1.031 1.143 1.423 ...
    1.502 1.187 1.065 1.031 1.065 1.187 1.502 1.817 1.355 1.187 ...
    1.143 1.187 1.355 1.817 2.574 1.817 1.502 1.423 1.502 1.817 ...
    2.574];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [69.2 57.9 49.2 46.2 49.2 57.9 69.2 57.9 43.2 33.1 29.4 ...
    33.1 43.2 57.9 49.2 33.1 20.4 14.2 20.4 33.1 49.2 46.2 29.4 ...
    14.2 0 14.2 29.4 46.2 49.2 33.1 20.4 14.2 20.4 33.1 49.2 57.9 ...
    43.2 33.1 29.4 33.1 43.2 57.9 69.2 57.9 49.2 46.2 49.2 57.9 ...
    69.2];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_abs.default.failiffilemissing = true;
r.defaultfilename.rio_abs.default.format = 'mat';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_4/rio_abs/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [49 3600];
r.defaultfilename.rio_abs.default.units = 'dB';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.default.failiffilemissing = true;
r.defaultfilename.rio_power.default.format = 'mat';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_4/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power.default.loadfunction = '';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [49 3600];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_4/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [49 3600];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'int16';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.default.failiffilemissing = true;
r.defaultfilename.rio_rawpower.default.format = 'mat';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_4/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_rawpower.default.loadfunction = '';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [49 3600];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_4/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [49 3600];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.institutions{1} = 'Siena College';
% end of function
