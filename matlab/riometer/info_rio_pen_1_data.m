function r = info_rio_pen_1_data
%INFO_RIO_PEN_1_DATA Return basic information about rio_pen_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_pen_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://www.dcs.lancs.ac.uk/iono/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=pen;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'pen';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = 27;
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'NRCAN';
r.facility_url = 'http://www.nrcan.gc.ca/';
r.facilityid = 9;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 122;
r.ifrequency = [];
r.imaging = logical([]);
r.imagingbeams = [];
r.latitude = 49.345;
r.location1 = 'Penticton';
r.location1_ascii = '';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = -119.627;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 27;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([2007 02 27 00 00 00]);
r.systemtype = '';
r.url = '';
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [1];
r.zenith = [];
r.defaultfilename = [];
% end of function
