function mia = canopus_file_to_mia(filename, varargin)
%CANOPUS_FILE_TO_MIA  Convert CANOPUS file to MIA object.
%
%   mia = CANOPUS_FILE_TO_MIA(filename)
%   mia: RIO_ABS object
%   filename: CHAR
%

defaults.archive = ''; % accept but ignore archive
defaults.cancelhandle = [];
defaults.mia = [];
defaults.starttime = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

mia = [];
[fid mesg] = url_fopen(filename, 'r');
if fid == -1
  error(sprintf('could not open %s: %s', filename, mesg));
end

rio.bac = rio_back_1;
rio.con = rio_cont_1;
rio.daw = rio_daws_1;
rio.esk = rio_eski_1;
rio.chu = rio_fchu_1;
rio.sim = rio_fsim_1;
rio.smi = rio_fsmi_1;
rio.gil = rio_gill_1;
rio.isl = rio_isll_1;
rio.mcm = rio_mcmu_1;
rio.pin = rio_pina_1;
rio.rab = rio_rabb_1;
rio.ran = rio_rank_1;
rio.tal = rio_talo_1;

s = fgetl(fid);

if length(s) < 1
  error(sprintf('line too short (was ''%s'')', s));
elseif s(1) ~= '#'
  error(sprintf('first line not a comment (was ''%s'')', s));
end

words_tmp = split(' ', s);
words = {};
for n = 1:numel(words_tmp)
  if ~isempty(words_tmp{n})
    words{end+1} = words_tmp{n};
  end
end
s_abbrev = words{5};
instrument = getfield(rio, s_abbrev);

year = str2num(words{6}(1:4));
month = str2num(words{6}(5:6));
mday = str2num(words{6}(7:8));

st = timestamp([year month mday 0 0 0]);
et = st + timespan(1, 'd');
res = info(instrument, 'bestresolution');

if st > timestamp('2005-06-01')
  % NORSTAR
  fstr = '%d/%d/%d %d:%d:%d %f';
else
  % CANOPUS
  fstr = '%d/%d/%d %d:%d:%d %f %f';
end

% preallocate space for samples and their timestamps
samples = repmat(nan, 1, (et - st) ./ res);
secs_of_day = repmat(nan, size(samples));
valid_samples = 0;

while 1
  cancelcheck(defaults.cancelhandle);
  
  s = fgetl(fid);
  %   if rem(valid_samples, 60) == 0
  %     disp(['read: ' s]);
  %   end
  
  if feof(fid)
    break;
  end
  if length(s) == 0
    % do nothing
  
  elseif s(1) == '#'
    % do nothing
  
  elseif any(s == '*')
    % ignore any line with '*' in
    
  else
    % process line
    [line_data count errmesg nextindex] = sscanf(s, fstr);
    if ~isempty(errmesg)
      if matlabversioncmp('<', '6') & ...
	    isequal(strmatch('nan', lower(s(nextindex:end))), 1)
	% earlier versions of Matlab cannot parse NaN as a floating-point
        % number
	line_data(7:8) = nan;
      else
	error(sprintf('sccanf error: %s\nline was: ''%s''', ...
		      errmesg, s));
      end
    end
    if count == 7 | count == 8
      valid_samples = valid_samples + 1;
      
      secs_of_day(valid_samples) = [3600 60 1] * line_data(4:6);
      samples(valid_samples) = line_data(7);
    end
  end
  
  
end


fclose(fid);

% trim matrices to have just valid_samples elements
secs_of_day = secs_of_day(1:valid_samples);
samples = samples(1:valid_samples);

t = st + timespan(secs_of_day, 's');

% Ensure sample times are unique
[t tidx] = unique(t);
samples = samples(tidx);


it = timespan(repmat(5, size(t)), 's');

mia = rio_abs('starttime', st, ...
	      'endtime', et, ...
	      'sampletime', t, ...
	      'integrationtime', it, ...
	      'instrument', instrument, ...
	      'beams', 1, ...
	      'data', samples, ...
	      'units', 'dB');
processing = '';

if 0 
  % t = t(1:valid_samples);
  secs_of_day((valid_samples+1):end) = [];
  t = st + timespan(secs_of_day, 's');
  
  samples = samples(1:valid_samples);
  
  if ~valid_samples
    data = repmat(nan, 1, (et - st) ./ res);
    processing = '';
  elseif 0
    % interpolate between actual data samples to the correct times
    data = interp1(getcdfepochvalue(t), samples, ...
		   getcdfepochvalue(t_valid));
  else
    % convert to regularly-spaced data
    data = spaceregularly(st, et, res, t, samples, 2);
    processing = 'converted to regularly-spaced data';
  end
  
  mia = rio_abs('starttime', st, ...
		'endtime', et, ...
		'resolution', res, ...
		'instrument', instrument, ...
		'timing', 'centred', ...
		'beams', 1, ...
		'data', data, ...
		'units', 'dB');
end


if ~isempty(processing)
  mia = addprocessing(mia, processing);
end
