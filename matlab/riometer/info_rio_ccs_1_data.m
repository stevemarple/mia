function r = info_rio_ccs_1_data
%INFO_RIO_CCS_1_DATA Return basic information about rio_ccs_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_ccs_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=ccs;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'ccs';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'AARI riometer chain';
r.facility_url = 'http://www.aari.nw.ru/clgmi/geophys/station.htm';
r.facilityid = 13;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 43;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 77.7;
r.location1 = 'Cape Chelyuskin';
r.location1_ascii = '';
r.location2 = 'Russia';
r.logo = '';
r.logurl = '';
r.longitude = 104.3;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 9;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = 'http://www.aari.nw.ru/clgmi/geophys/station.htm';
r.url2 = {'http://www.aari.nw.ru/clgmi/geophys/station.htm'};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Arctic and Antarctic Research Institute';
% end of function
