function mia = arcom_file_to_mia(filename, varargin)
%ARCOM_FILE_TO_MIA  Read an ARCOM format file and return as a MIA object.
%
%   [mia FPGAinfo] = ARCOM_FILE_TO_MIA(filename, ...)
defaults.mia = [];
defaults.cancelhandle = [];
% flag to indicate if common calibration data is wanted
defaults.calibrationdata = false;

% Flag to indicate if the calibration data is common for all beams. If empty
% use the riometer default.
defaults.commoncalibrationdata = [];

% List of beams for calibration data. If empty use the riometer default.
defaults.calibrationbeams = [];

defaults.complexdata = 0;
defaults.starttime = []; % accept but ignore
defaults.verbosity = 1;

defaults.archive = ''; 

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object must be set');
end
cls = class(defaults.mia);
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('mia object must have instrument set');
end
rio_code = getcode(rio);
systemtype = info(rio, 'systemtype');

bestres = info(rio, 'bestresolution');

beams = getbeams(defaults.mia);
if isempty(beams)
  beams = info(rio, 'allbeams');
end

[imagingbeams widebeams] = info(rio, 'beams'); % Values for the instrument
ibeams = intersect(beams, imagingbeams); % Request imaging beams
wbeams = intersect(beams, widebeams); % Request wide beams
% if ~isempty(wbeams)
%   disp('Only storing imaging beams');
%   beams = ibeams;
% end
orig_beams = beams;

% get the position where the beam occurs in the arcom data array
arcombeammap = info(rio, 'arcombeammap');
requested_indices = arcombeammap(beams(:));

if isa(defaults.mia, 'rio_rawpower_calib') ...
      | isa(defaults.mia, 'rio_power_calib')
  want_calib_data = []; % get all values
  uses_calib_data = 1;

  % Insist that if the user sets commoncalibrationdata that s/he also set
  % calibrationbeams
  if xor(isempty(defaults.commoncalibrationdata), ...
	 isempty(defaults.calibrationbeams))
    % one of the two is empty
    error('Please set both commoncalibrationdata and calibrationbeams');
  end
  
  if isempty(defaults.commoncalibrationdata)
    commoncalibrationdata = info(rio, 'commoncalibrationdata');
  else
    commoncalibrationdata = logical(defaults.commoncalibrationdata);
  end
  if isempty(defaults.calibrationbeams)
    calibrationbeams = info(rio, 'calibrationbeams');
  else
    calibrationbeams = defaults.calibrationbeams;
  end
    
else
  want_calib_data = defaults.calibrationdata;
  uses_calib_data = 0;
end

opts.filename = filename;
opts.verbosity = defaults.verbosity;
opts.progress = 0;

info_descriptor_name = 'DESCRIPTOR_RX_INFO';
switch rio_code
 case {'and_2' 'nal_2'}
  opts.wantedpackets = [17];
  opts.wanteddescriptors = [8 9 11 37];

 case 'mai_2'
  opts.wantedpackets = [17];
  % 0x08 = 08 = DESCRIPTOR_HOPF_GPS_INFO
  % 0x0b = 11 = DESCRIPTOR_AIRIS_CPLX_DATA
  % 0x10 = 16 = DESCRIPTOR_BITSTREAM_VERSION
  % 0x30 = 48 = DESCRIPTOR_RX_POWER
  % 0x31 = 49 = DESCRIPTOR_RX_INFO_NXN
  % 0x32 = 50 = DESCRIPTOR_RX_DC_OFFSETS_NXN
  opts.wanteddescriptors = [8 11 16 48 49];
  
  info_descriptor_name = 'DESCRIPTOR_RX_INFO_NXN';
  
 case 'ram_2'
  opts.wantedpackets = [17];
  opts.wanteddescriptors = [8 9 10 36 37];
 
 case 'ram_3'
  opts.wantedpackets = [17];
  opts.wanteddescriptors = [7 8 9 36 37];
 
 case 'ram_4'
  % ARIES fan-beam data isn't currently output. When it is fix which
  % descriptor to use
  error('ARIES fan beam not output');

  opts.wantedpackets = [17];
  opts.wanteddescriptors = [7 8 9 37];
 
 otherwise
  error(sprintf('Unknown riometer (%s), please fix %s', ...
		rio_code, mfilename));
end
opts_cell = makeprop(opts);

% only read data packets
s = arcom_to_structs(opts_cell{:});
% assignin('base','S',s); disp('assigning S in base');

% all structures have same fields, convert to an array of structs
s2 = [s{:}];
clear s;

if ~isfield(s2, 'DESCRIPTOR_HOPF_GPS_INFO')
  % no packets of the correct type were found, no valid data exists in
  % this file
  mia = [];
  return
end

% convert hopf descriptors to an array of structs
hopf_descriptors = [s2.DESCRIPTOR_HOPF_GPS_INFO];

% calculate sample times
sampletime = timestamp({hopf_descriptors.timestamp}) + (bestres ./ 2);
% clear hopf_descriptors;

% Check that the sample times are valid. Causes for not being valid
% include leap seconds (observed as [2008 12 31 23 59 60]).
badtimes = find(~isvalid(sampletime));
if ~isempty(badtimes)
  disp(sprintf(['sample time is not valid (was ' ...
                '[%04d %02d %02d %02d %02d %02d]), ' ...
                'ignoring affected ARCOM packet\n'], ...
               [hopf_descriptors(badtimes).timestamp]));
  % Delete the ARCOM packets corresponding to bad timestamps and update
  % sampletime
  s2(badtimes) = [];
  sampletime(badtimes) = [];
end
clear hopf_descriptors;

% Now ensure that the times are not repeating and are in the correct
% order
snum = numel(sampletime);
[sampletime idx] = unique(sampletime);
s2 = s2(idx);
if numel(sampletime) ~= snum
  disp(sprintf('deleting %d repeated samples', snum - numel(sampletime)));
end

% rx_info_descriptors = [s2.DESCRIPTOR_RX_INFO];
% rx_info_descriptors = [getfield(s2, info_descriptor_name)];

% Cannot use getfield here ("Illegal right hand side in assignment. Too
% many elements.")
switch info_descriptor_name
 case 'DESCRIPTOR_RX_INFO'
  rx_info_descriptors = [s2.DESCRIPTOR_RX_INFO];
 case 'DESCRIPTOR_RX_INFO_NXN'
  rx_info_descriptors = [s2.DESCRIPTOR_RX_INFO_NXN];
 otherwise
  error(sprintf('Unknown info_descriptor_name: %s', ...
		info_descriptor_name));
end

switch rio_code
 case {'and_2' 'nal_2'}
  % convert ARIES complex data descriptors to an array of structs
  airis_cplx_data = [s2.DESCRIPTOR_AIRIS_CPLX_DATA];
  
  % create complex data and normalise
  data = complex([airis_cplx_data.real], [airis_cplx_data.imag]);
  clear airis_cplx_data;

 case 'mai_2'
  % convert ARIES complex data descriptors to an array of structs
  airis_cplx_data = [s2.DESCRIPTOR_AIRIS_CPLX_DATA];
  
  % create complex data and normalise
  data = complex([airis_cplx_data.real], [airis_cplx_data.imag]);
  clear airis_cplx_data;
  
  % append the widebeam data
  power_data = [s2.DESCRIPTOR_RX_POWER];
  wdata = complex([power_data.real], [power_data.imag]);
  data = [data ; wdata];
  clear power_data wdata;
      
 case 'ram_2'
  % convert ARIES complex data descriptors to an array of structs
  aries_cplx_data = [s2.DESCRIPTOR_ARIES_CPLX_DATA];

  % create complex data and normalise
  data = complex([aries_cplx_data.real], [aries_cplx_data.imag]);
  clear aries_cplx_data;

 case 'ram_3'
  % convert ARIES power data descriptors to an array of structs
  aries_cplx_data = [s2.DESCRIPTOR_64_CPLX_POW];
  % create complex data and normalise
  % data = complex([aries_cplx_data.real], [aries_cplx_data.imag]);

  % data is x.real, x.imag, y.real, y.imag
  tmp = [aries_cplx_data.data];
  rows_4 = size(tmp, 1) / 4;
  real_idx = [1:rows_4  (2*rows_4+1):(3*rows_4)];
  imag_idx = [(rows_4+1):(2*rows_4) (3*rows_4+1):(4*rows_4)];
  data = complex(tmp(real_idx, :), tmp(imag_idx, :));
  clear aries_cplx_data tmp;
 
 case 'ram_4'
  % convert ARIES fan-beam power data descriptors to an array of structs
 
  % ARIES fan-beam data isn't currently output. When it is fix which
  % descriptor to use
  error('ARIES fan beam not output');
  aries_cplx_data = [s2.DESCRIPTOR_64_CPLX_POW];
  
  % create complex data and normalise
  data = complex([aries_cplx_data.real], [aries_cplx_data.imag]);
  clear aries_cplx_data;
  
 otherwise
  error(sprintf('Unknown riometer (%s), please fix %s', ...
		rio_code, mfilename));
end

% normalise the data
nr_of_samples = [rx_info_descriptors.nr_of_samples];
for n = 1:size(data, 1)
  data(n, :) = data(n, :) ./ nr_of_samples;
end

dataquality = {};
switch systemtype
 case 'aries'
  % For ARIES systems compensate for the scaling. In performing the FFT the
  % bits are left-shifted by n bits, at 3 points in the process. Normally
  % each shift is one bit.
  if isfield(s2, 'DESCRIPTOR_FFT_TAPER_INFO')
    fft_taper_descriptors = [s2.DESCRIPTOR_FFT_TAPER_INFO];
    scaling_a = [fft_taper_descriptors.scaling_a];
    scaling_b = [fft_taper_descriptors.scaling_b];
    scaling_c = [fft_taper_descriptors.scaling_c];
    for n = 1:size(data, 1)
      data(n, :) = data(n, :) .* power(2, 2*(scaling_a+scaling_b+scaling_c));
    end
    clear scaling_a scaling_b scaling_c;
    
    if any([fft_taper_descriptors.overflow])
      dataquality{end+1} = 'FFT overflow';
    end
    clear fft_taper_descriptors;
  end

 case 'iris'
  % IRIS-type filled array, with optional widebeam data. As used by
  % rio_and_2, rio_nal_2 and rio_mai_2.
  
  ; % do nothing  
  
 case 'widebeam'
  % Pure widebeam system, as for the experimental rio_ram_6 set up.
  
  % check if the power output was after tapering, if so set the data to
  % NaN (at a later date can implement a method to undo tapering)
  if isfield(s2, 'DESCRIPTOR_FFT_TAPER_INFO')
    fft_taper_descriptors = [s2.DESCRIPTOR_FFT_TAPER_INFO];
    pow_select = [fft_taper_descriptors.pow_select];
    post_taper_idx = find([fft_taper_descriptors.pow_select]);
    if ~isempty(post_taper_idx)
      % Find out which tapering methods were used (assume > 1 !) when
      % post-tapered output selected
      taper_ids = [fft_taper_descriptors.taper_id]; 
      taper_id_list = unique(taper_ids(post_taper_idx));
      
      for m = 1:numel(taper_id_list)
        disp(sprintf(['post-tapering output selected, undoing ' ...
                      'effects of taper ID %d (0x%04X)'], ...
                     taper_id_list(m), taper_id_list(m)));
        
        [xtaper ytaper] = info(rio, 'tapering', taper_id_list(m));
        tapers = [xtaper ytaper];
        
        % find elements with current tapering pattern and post-tapered output
        idx = find(pow_select & taper_ids == taper_id_list(m));
        for n = 1:size(data, 1)
          data(n, :) = data(n, :) ./ tapers(n);
        end
      end
      
      % disp('post-tapering output selected, inserting NaNs');
      % data(:, post_taper_idx) = nan;
      clear taper_ids;
    end
    
    clear fft_taper_descriptors pow_select post_taper_idx;
    
  else
    warning('Tapering information not available');
  end
    
 otherwise
  error(sprintf('unknown system type (was ''%s'')', systemtype));
end


if logical(defaults.complexdata)
  % keep data as complex values, but only keep the relevant beams 
  data = data(requested_indices, :);
else
  % convert from complex data real values and extract only the relevant
  % beams
  data = abs(data(requested_indices, :));
end

if isempty(data)
  mia = [];
  return;
end

if length(sampletime) ~= size(data, 2)
  whos
  error('number of timestamps does not match number of samples');
end

% For postintegrated data use the mean of start and (end+1s) times. Note
% that endtime is the start of the last sample, not the end of the last
% sample, hence we must add bestres
integrationtime = repmat(bestres, size(sampletime));
if isfield(s2, 'DESCRIPTOR_POST_INT_INFO')
  ispostintegrated = (postinteg.nestinglevel > 0);
  unixepoch = timestamp([1970 1 1 0 0 0]);
  bestres_s = gettotalseconds(bestres);
  sampletime(ispostintegrated) = ...
      unixepoch + timespan(mean([postinteg.starttime(ispostintegrated);
		    (postinteg.endtime(ispostintegrated)+bestres_s)], 1), 's');
  
  integrationtime(ispostintegrated) = ...
      timespan(postinteg.secondcounter(ispostintegrated), 's');
end

if uses_calib_data
  rx_info_1 = getfield(s2(1), info_descriptor_name);
%   calibrationinterval = ...
%       timespan(s2(1).DESCRIPTOR_RX_INFO.noise_off_cnt + ...
% 	       s2(1).DESCRIPTOR_RX_INFO.noise_on_cnt, 's');
%   calibrationduration = ...
%       timespan(s2(1).DESCRIPTOR_RX_INFO.noise_on_cnt, 's');
  calibrationinterval = ...
      timespan(rx_info_1.noise_off_cnt + ...
	       rx_info_1.noise_on_cnt, 's');
  calibrationduration = ...
      timespan(rx_info_1.noise_on_cnt, 's');

end

clear s2;
% put data into the correct order using the timestamps
% [sampletime idx] = sort(sampletime);
% snum = numel(sampletime);
% [sampletime idx] = unique(sampletime);
% if numel(sampletime) ~= snum
%   disp(sprintf('deleting %d repeated samples', snum - numel(sampletime)));
% end
% data2 = data(:, idx);
% data2 = data;

% Set start and end times
st = min(sampletime - integrationtime./2);
et = max(sampletime + integrationtime./2);

% construct the MIA object, of the appropriate type. data matrix to be
% added later (may need to convert to dBm or delete calibration values)
% df = info(rio, 'defaultfilename', cls);
df = info(rio, 'defaultfilename', cls, 'archive', defaults.archive);
mia = feval(cls, ...
	    'starttime', st, ...
	    'endtime', et, ...
	    'sampletime', sampletime, ...
	    'integrationtime', integrationtime, ...    
	    'instrument', rio, ...
	    'load', 0, ...
	    'data', [], ...
	    'units', df.units, ...
	    'processing', ['Created by ', mfilename],...
	    'beams', beams);

if isa(mia, 'rio_power')
  % need power in dBm
  mia = setdata(mia, data);
  [data units] = linearise(mia);
  mia = setunits(mia, units);
end

% Apply some data checks
too_few_samples = ...
    find([rx_info_descriptors.nr_of_adc_in_cycle] < nr_of_samples);
clear nr_of_samples;
if ~isempty(too_few_samples)
  data(:, too_few_samples) = nan; % too few samples to be valid
end

% get the calibration data into a useful form
if uses_calib_data
  % get an array of noise flags
  if isfield(rx_info_descriptors, 'noise_active_flag')
    noise_active = [rx_info_descriptors.noise_active_flag];
  else
    % Maitri and similar systems. Be strict about the criteria for
    % calibration
    calib_on = [rx_info_descriptors.calib_on];
    calib_switch = [rx_info_descriptors.calib_switch];
    noise_level = [rx_info_descriptors.noise_level];
    noise_active = calib_on & calib_switch & (noise_level == 0);
    clear calib_on calib_switch noise_level;
  end
  
  calib_idx = find(noise_active);
  clear noise_active;
  
  if ~isempty(calib_idx)
    % copy the good calibration values
    if iscell(calibrationbeams)
      calibrationdata = repmat(nan, numel(calibrationbeams), numel(calib_idx));
      for n = 1:numel(calibrationbeams)
	bi = getparameterindex(mia, calibrationbeams{n});
	tmp = data(bi, calib_idx);
	if size(tmp, 1) > 1
	  calibrationdata(n, :) = nonanmean(tmp, 1);
	else
	  calibrationdata(n, :) = tmp;
	end
      end
    else
      bi = getparameterindex(mia, calibrationbeams);
      calibrationdata = data(bi, calib_idx);
      if commoncalibrationdata & numel(calibrationbeams) > 1
	% multiple calibration beams used to derive a single value
	calibrationdata = nonanmean(calibrationdata, 1);
      end
    end

    calibrationtimestamps = sampletime(calib_idx);

    % Explicitly set common calibration data flag
    mia = setcommoncalibrationdata(mia, commoncalibrationdata);
    mia = setcalibrationdata(mia, calibrationdata, calibrationtimestamps);
    
    % delete all calibration times in the data, then do the same for
    % sampletime and integrationtime
    data(:, calib_idx) = [];
    sampletime(calib_idx) = [];
    mia = setsampletime(mia, sampletime);

    integrationtime(calib_idx) = [];
    mia = setintegrationtime(mia, integrationtime);
    mia = setcalibrationinterval(mia, calibrationinterval);
    mia = setcalibrationduration(mia, calibrationduration);
  else
    mia = local_setforemptycalibrationdata(mia, commoncalibrationdata);
  end
end

if ~isempty(dataquality)
  mia = adddataquality(mia, dataquality);
end
mia = adddataquality(mia, 'Uncalibrated');


mia = setdata(mia, data);

if ~isequal(beams, orig_beams)
  % had to add extra beams to get the calibration information. Extract
  % only the requested beams
  mia = extract(mia, 'beams', orig_beams);
end

function r = local_setforemptycalibrationdata(mia, commoncalibrationdata)
r = mia;
r = setcommoncalibrationdata(r, commoncalibrationdata);
if commoncalibrationdata
  r = setcalibrationdata(r, zeros(1, 0), repmat(timestamp, 1, 0));
else
  dsz = getdatasize(mia);
  r = setcalibrationdata(r, zeros(dsz(1), 0), repmat(timestamp, 1, 0));
end
