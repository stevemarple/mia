function r = char(mia, varargin)
%CHAR Convert RIO_IMAGE object into a CHAR representation.
%
%   s = CHAR(mia)
%   mia: MIA object
%   s: CHAR representation.
%
%   See also MIA_IMAGE_BASE/CHAR.

% NB Make all objects derived from mia_base print a trailing newline
% character 

if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_image_base_char(mia, varargin{:});
  return;
end

r = sprintf(['%s' ...
	     'original class    : %s\n'], ...
	    mia_image_base_char(mia, varargin{:}),...
	    char(mia.originalclass)); 
	     


