function [fh, gh, cbh] = keogram(mia, varargin)
%KEOGRAM  Plot a  RIO_IMAGE object in keogram format.
%
%   See mia_image_base/KEOGRAM for details.

[fh gh, cbh] = mia_image_base_keogram(mia, varargin{:});

if strcmp(mia.originalclass, 'rio_abs')
  clim = get(gh(1), 'CLim');
  if clim(2) > 0
    clim(1) = 0;
  end
  set(gh, 'CLim', clim);
  if ~isempty(cbh)
    delete(get(cbh, 'Children'));
    makeplotfig('addcolorbar', fh, ...
		'axes', cbh, ...
		'clim', clim, ...
		'xlim', clim);
  end

end


