function r = getclim(mia)
%GETCLIM  Return appropriate color limits.

if strcmp(mia.originalclass, 'rio_abs');
  r = [0 nan];
else
  r = [nan nan];
end
