function [fstr, valuesStr] = datalabel2(mia, varargin)

if ~isempty(mia.originalclass)
  tmp = feval(mia.originalclass, 'units', getunits(mia), ...
	      'instrument', getinstrument(mia), 'load', 0);
  [fstr, valuesStr] = datalabel2(tmp);
end
