function r = mia_image_base(rio)
%MIA_IMAGE_BASE  Extract the parent MIA_IMAGE_BASE object from RIO_IMAGE.
%
%   r = MIA_IMAGE_BASE(rio)
%   r: MIA_IMAGE_BASE object
%   rio: RIO_IMAGE object
%
%   See also MIA_IMAGE_BASE, RIO_IMAGE.

r = rio.mia_image_base;

