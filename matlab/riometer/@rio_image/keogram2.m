function [fh, gh, cbh] = keogram2(mia, varargin)
%KEOGRAM  Plot a  RIO_IMAGE object in keogram format.
%
%   See mia_image_base/KEOGRAM for details.

[fh gh, cbh] = mia_image_base_keogram2(mia, varargin{:});

if strcmp(mia.originalclass, 'rio_abs')
  clim = get(gh(1), 'CLim');
  clim(1) = 0;
  set(gh, 'CLim', clim);
  if ~isempty(cbh)
    delete(get(cbh, 'Children'));
    makeplotfig('addcolorbar', fh, ...
		'axes', cbh, ...
		'clim', clim, ...
		'xlim', clim);
  end

end


