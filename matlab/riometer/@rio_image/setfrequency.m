function r = setfrequency(mia, f)
%SETFREQUENCY  Adjust absorption data to an equivalent operating frequency.
%
%   r = SETFREQUENCY(mia, f)
%   r: modified RIO_ABS object(s)
%   mia: RIO_ABS object(s)
%   f: new operating frequency (Hz) (scalar or matrix same size as "rio")
%
%   The absorption measurements are scaled according to the square of the
%   ratio of old to new oprtating frequencies. This is correct to a first
%   approximation. 
%
%   See also RIO_ABS, RIOMETER, GETFREQUENCY.

if ~all(strcmp({mia.originalclass}, 'rio_abs'))
  error('not absorption images');
end

if prod(size(f)) == 1
  f = repmat(f, size(mia));
end

r = mia;
for n = 1:prod(size(mia))
  in = getinstrument(r(n));
  fOld = getfrequency(in);
  r(n) = setinstrument(r(n), setfrequency(in, f(n)));
  % r(n).data = r(n).data * power(fOld / f(n), 2);
  r(n) = setdata(r(n), getdata(r(n)) * power(fOld / f(n), 2));
  r(n) = addprocessing(r(n), sprintf('Frequency converted to %s', ...
				     printunits(f(n), 'Hz')));
end


