function r = gettype(mia, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: RIO_IMAGE object
%
%   See also RIO_IMAGE, MIA_IMAGE_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

if ~isempty(mia.originalclass)
  r = gettype(feval(mia.originalclass), mode);
else
  r = '<unknown type>';
end

return;

switch mode
 case {'l' 'c'}
  r = [t ' image'];
  
 case 'u'
  r = [t ' IMAGE'];
  
 case 'C'
  r = [t ' Image'];
  
 otherwise
  error('unknown mode');
end


