function r = getlimits(rio, varargin)
%GETLIMITS  Return the limits used for plotting.

r = getlimits(rio.mia_image_base);

if strcmp(rio.originalclass, 'rio_abs') & r(2) > 0
  r(1) = 0;
end
