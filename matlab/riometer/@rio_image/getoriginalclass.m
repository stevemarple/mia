function r = getoriginalclass(mia)
%GETORIGINALCLASS  Return the original class of the RIOMETER data.
%
% r = GETORIGINALCLASS(mia)

r = reshape({mia.originalclass}, size(mia));



