function mia = rio_image(varargin)
%RIO_IMAGE  Constructor for RIO_IMAGE class.
%
%
%   See also MIA_IMAGE_BASE.

latestversion = 1;
mia.versionnumber = latestversion;

% it is important to distinguish between absorption images and power images
mia.originalclass = ''; % e.g., 'rio_power', 'rio_abs'

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  parent = mia_image_base;
  mia = class(mia, 'rio_image', parent);
 
elseif nargin == 1 & (isa(varargin{1}, 'rio_image') | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  a = struct(varargin{1});
  if ~isfield(a, 'versionnumber')
    error('need a versionnumber field'); % absolutely must have
  end
  

  % have to copy field-by-field (and not use a simple assign (=)) because
  % we may have a derived class

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {'mia_image_base'});
  init = mia; % assign all used fields to default values
  mia = class(mia, 'rio_image', mia_image_base);
  mia = repmat(mia, size(a));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    tmp = init;
    an = a(n);
    for m = 1:length(fn)
      tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
    end
    % update base class to latest version
    base = mia_image_base(an.mia_image_base); 
    fieldnames(mia)
    fieldnames(tmp)
    tmp = class(tmp, 'rio_image', base);
    mia(n) = tmp;
  end 

 
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [mia unvi] = interceptprop(varargin, mia);
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  parent = mia_image_base(varargin{unvi});
  mia = class(mia, 'rio_image', parent);
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
