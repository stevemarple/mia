function r = info_rio_sod_1_data
%INFO_RIO_SOD_1_DATA Return basic information about rio_sod_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_sod_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=sod;serialnumber=1

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.pixels = [];
r.abbreviation = 'sod';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [60];
r.bibliography = '';
r.comment = '';
r.credits = 'Data courtesy of the Sodankylš Geophysical Observatory.';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'SGO riometer chain';
r.facility_url = 'http://www.sgo.fi/';
r.facilityid = 2;
r.groupids = [1];
r.iantennatype = '';
r.ibeams = [];
r.id = 38;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 67.42;
r.location1 = 'Sodankyla';
r.location1_ascii = 'Sodankyla';
r.location2 = 'Finland';
r.logo = 'oulu_logo_color';
r.logurl = '';
r.longitude = 26.393;
r.modified = timestamp([2011 07 14 12 52 21.001976]);
r.name = '';
r.piid = 33;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 01, 'm', 00, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1];
r.starttime = timestamp([1994 10 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.sgo.fi/';
r.url2 = {'http://www.sgo.fi/'};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 3e+07;
r.widebeams = [1];
r.zenith = [0];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'sgoriodata';
r.defaultfilename.rio_abs.default.fstr = '@data_location_rio_sgo';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan(00, 'h', 01, 'm', 00, 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [1 1440];
r.defaultfilename.rio_abs.default.units = 'dB';
r.institutions{1} = 'Sodankyla Geophysical Observatory';
% end of function
