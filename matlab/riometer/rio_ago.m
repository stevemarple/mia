function r = rio_ago
%RIO_AGO  Return a list of the AGO RIOMETER instruments.
%
%   r = RIO_AGO
%   r: vector of RIOMETER objects
%
%   See also RIOMETER.

tmp = instrumenttypeinfo(riometer, 'aware');

r = tmp(strcmp(getfacility(tmp), 'AGONET'));

% Add some others which don't have facility AGONET
r = [r rio_asuka rio_mir_1 rio_molodezhnaya ...
     rio_vos_1 rio_novolazarevskaya];
r = unique(r);

% r = [rio_a77 rio_a80 rio_a81 rio_a84 ...
%      rio_agop1 rio_agop2 rio_agop3 rio_agop4 rio_agop5 rio_agop6 ...
%      rio_asuka rio_mir_1 rio_molodezhnaya ...
%      rio_vos_1 rio_novolazarevskaya];

