function r = info_rio_tnb_2_data
%INFO_RIO_TNB_2_DATA Return basic information about rio_tnb_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_tnb_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=tnb;serialnumber=2

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'tnb';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = 'art:349,inproc:230';
r.comment = '';
r.credits = '';
r.datarequestid = 32;
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 93;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -74.7;
r.location1 = 'Terra Nova';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 164.11;
r.modified = timestamp([2011 07 12 15 44 16.672733]);
r.name = '';
r.piid = 21;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [];
r.starttime = timestamp([1994 11 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.eswua.ingv.it/';
r.url2 = {'http://www.eswua.ingv.it/'};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = 3.82e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Istituto Nazionale di Geofisica';
% end of function
