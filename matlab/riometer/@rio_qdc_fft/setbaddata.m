function r = setbaddata(mia, baddata)
%SETBADDATA  Set the positions of bad data inside the QDC.

if length(mia) ~= 1
  error('only scalar objects are allowed');
end

beams = getbeams(mia);
bn = numel(beams);

if isempty(baddata)
  baddata = cell(bn, 1);
elseif ~iscell(baddata)
  error('baddata must be a cell array');
elseif numel(baddata) ~= bn
  error('baddata must have one element per beam');
end

r = mia;
for n = 1:bn
  if islogical(baddata{n})
    baddata{n} = find(baddata{n});
  end
end
r.baddata = reshape(baddata, bn, 1);

r = recomputedata(r);
