function r = getbaddata(mia)
%GETBADDATA  Return the positions of bad data inside the QDC.

num = numel(mia);
if num ~= 1
  r = cell(size(mia));
  for n = 1:num
    tmp = mia(n);
    r{n} = tmp.baddata;
  end
  
else
  r = mia.baddata;
end



