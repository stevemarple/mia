function r = loadobj(mia)
%LOADOBJ  Convert a RIO_QDC_FFT object from its minimal form.
%
%   r = SAVING(mia)
%   r: RIO_QDC_FFT
%   mia: RIO_QDC_FFT
%
% If necessary Convert a RIO_QDC_FFT object from its minimal form to one
% where the data array contains meaningful data. This allows RIO_QDC_FFT
% objects to be treated as normal RIO_QDC objects, even though they are
% stored on disk with just their FFT coefficients.
%
% SAVEOBJ and LOADOBJ are only called automatically for matlab versions
% >5.1.
%
% See also SAVEOBJ.

if isstruct(mia)
  fn = fieldnames(mia);
  r = feval([fn{end} '_fft'], mia);
else
  r = mia;
end

if prod(getdatasize(r)) == 0
  r = recomputedata(r);
end
