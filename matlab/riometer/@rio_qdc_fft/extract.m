function [r,sr,s] = extract(mia, varargin)
%EXTRACT Extract a subset of data from RIO_QDC_FFT or RIO_RAWQDC_FFT data.
%
% If extracting based on start or end time then the returned data is
% converted to RIO_QDC or RIO_RAWQDC as approriate.
%
% See mia_base/EXTRACT.

% if mia is non-scalar process each element independently
if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

% from this point on mia is scalar

% parameter name/value interface
defaults.starttime = [];
defaults.endtime = [];
[defaults unvi] = interceptprop(varargin, defaults);

% extract main parameters
[r,sr,s] = rio_base_extract(mia, varargin{:});

% extract the FFT coefficients
sr2.type = '{}';
sr2.subs = { sr.subs{1} };
r.coefficients =  { feval(s.subsref, r.coefficients, sr2) };

