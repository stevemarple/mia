function r = getcoefficients(mia)
%GETCOEFFICIENTS  Return the coefficients used in a RIO_QDC_FFT object

num = numel(mia);
if num ~= 1
  r = cell(size(mia));
  for n = 1:num
    tmp = mia(n);
    r{n} = tmp.coefficients;
  end

else
  r = mia.coefficients;
end



