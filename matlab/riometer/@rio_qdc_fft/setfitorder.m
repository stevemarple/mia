function r = setfitorder(mia, fit)
%SETFITORDER  Set the fit order used in a RIO_QDC_FFT object
%
%   r = SETFITORDER(mia, fit)
%   r: modified RIO_QDC_FFT object
%   mia: RIO_QDC_FFT object
%   fit: scalr or vector of fit orders
%
% The RIO_QDC_FFT functions are also used to implement the RIO_RAWQDC_FFT
% class, if using RIO_RAWQDC_FFT objects then subsitute RIO_RAWQDC_FFT
% for RIO_QDC_FFT in the HELP pages.
%
% See also RIO_QDC_FFT.

num = numel(mia);
if num ~= 1
  error('can only operate on scalar objects');
end

fn = numel(fit);
bn = numel(getbeams(mia));
if fn == 1
  fit = repmat(fit, 1, bn);
elseif fn ~= bn
  error('number of fit order must match number of beams');
end


r = feval(class(mia), ...
	  getoriginalqdc(mia), ...
	  'fitorder', fit);


