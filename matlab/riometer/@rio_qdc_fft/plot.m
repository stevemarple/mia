function [fh, gh, ph] = plot(mia, varargin)
%PLOT  Plot data from a RIO_QDC_FFT object.
%
% The 'zdata' option can be used to specify the ZData values for the FFT
% QDC, and the original QDC (if present). It can be given as either a
% NUMERIC or CELL array. If just one value is given it is used for both
% QDCs, otherwise the first value is used for the FFT QDC and the second
% for the original QDC.
%
% See rio_base/PLOT and mia_base/PLOT for more details about the specific
% options available.
%
% See also rio_base/PLOT, mia_base/PLOT.

% write function in a generic way so that if a RIO_RAWQDC_FFT is ever
% created the functions can be symlinks to the RIO_QDC_FFT functions.

instrument = getinstrument(mia);
beams = getbeams(mia);

defaults.beams = info(instrument, 'preferredbeam', 'beams', beams);
defaults.plotaxes = [];
defaults.title = [];
defaults.windowtitle = [];
defaults.originalqdccolor = 'r';
defaults.visible = 'on';
defaults.zdata = [];
[defaults unvi] = interceptprop(varargin, defaults);

% calculate title now that beams are known. Base the title on the FFT
% QDCs.
[title windowtitle] = maketitle(mia, 'beams', defaults.beams);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

switch numel(defaults.zdata)
 case 0
  zdata = {[] []};
 case 1
  if iscell(defaults.zdata)
    zdata = {defaults.zdata{1} defaults.zdata{1}};
  else
    zdata = {defaults.zdata(1) defaults.zdata(1)};
  end
 otherwise
  if iscell(defaults.zdata)
    zdata = {defaults.zdata{1} defaults.zdata{2}};
  else
    zdata = {defaults.zdata(1) defaults.zdata(2)};
  end
end

if isempty(mia.originalqdc)
  % plot the parent QDC object as it is normally plotted.
  [fh gh ph] = plot(getparent(mia), varargin{unvi}, ...
		    'beams', defaults.beams, ...
		    'plotaxes', defaults.plotaxes, ...
		    'title', defaults.title, ...
		    'windowtitle', defaults.windowtitle, ...
		    'visible', defaults.visible, ...
		    'zdata', zdata{1});
  return
end

if ~isempty(defaults.plotaxes)
  np = get(defaults.plotaxes, 'NextPlot');
  set(defaults.plotaxes, 'NextPlot', 'add');
end

% plot the original QDC object as it is normally plotted. Give the titles
% as calculated for the FFT QDCs
[fh gh ph2] = plot(mia.originalqdc, varargin{:}, ...
		   'beams', defaults.beams, ...
		   'plotaxes', defaults.plotaxes, ...
		   'title', defaults.title, ...
		   'windowtitle', defaults.windowtitle, ...
		   'visible', 'off', ...
		   'color', defaults.originalqdccolor, ...
		   'zdata', zdata{2});

if isempty(defaults.plotaxes)
  np = get(gh(:), 'NextPlot');
  set(gh, 'NextPlot', 'add');
end

% plot the parent QDC object as it is normally plotted
[tmp1 tmp2 ph1] = plot(getparent(mia), varargin{unvi}, ...
		       'beams', defaults.beams, ...
		       'plotaxes', gh, ...
		       'title', defaults.title, ...
		       'windowtitle', defaults.windowtitle, ...
		       'visible', 'off', ...
		       'zdata', zdata{1});

if ~iscell(np)
  np = {np};
end
set(gh(:), {'NextPlot'}, np);

if isempty(defaults.plotaxes)
  set(fh, 'Visible', defaults.visible);
end

ph = [ph1 ph2];
