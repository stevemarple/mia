function r = setrioqdc(mia, a)
%SETRIOQDC  Set the RIO_QDC part of a RIO_QDC_FFT object.
%
%  This function is not intended for general use, only for the
%  construction of RIO_QDC_FFT objects.

if isa(mia, 'rio_qdc_fft')
  c = 'rio_qdc';
elseif isa(mia, 'rio_rawqdc_fft')
  c = 'rio_rawqdc';
else
  error(sprintf('unknown class (was ''%s'')', class(mia)));
end

% require a rio_qdc object, and not something else or something derived
% from it (otherwise the inheritance goes wrong)
if ~strcmp(class(a), c)
  error(['class is not ' c]);
elseif ~isequal(size(mia), size(a))
  error('both parameters must be the same size');
end

r = mia;

for n = 1:numel(mia)
  tmp = mia(n); % can't subscript and use . operator with matlab v5.1
  switch c
   case 'rio_qdc'
    tmp.rio_qdc = a(n);
   case 'rio_rawqdc'
    tmp.rio_rawqdc = a(n);
   otherwise
    % oops should have updated this as well as the input checking!
    error(sprintf('unknown field (was ''%s'')', c));
  end
  r(n) = tmp;
end

