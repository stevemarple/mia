function r = setoriginalqdc(mia, qdc)
%SETORIGINALQDC  Set the original QDC object
%
% r = SETORIGINALQDC(mia, qdc)
% r: RIO_(RAW)QDC_FFT object
% mia: RIO_(RAW)QDC_FFT
% qdc: RIO_QDC or RIO_RAWQDC object (or empty matrix)
%
% SETORIGINALQDC sets the originalqdc field. qdc may be an empty matrix, to
% clear the originalqdc object.
%
% See also GETORIGINALQDC.

if length(mia) ~= 1
  error('mia must be scalar')
end

r = mia;
if isempty(qdc)
  r.originalqdc = [];
  return
end

% need exact match of class, not isa
if ~strcmp(class(qdc), getparentclass(mia))
  error(sprintf('qdc class not ''%s''', getparentclass(mia)));
end

r.originalqdc = qdc;

