function r = gettype(mia, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: RIO_QDC_FFT object
%
%   See also RIO_QDC_FFT, RIO_BASE, RIOMETER.

% do this in a generic way, as later we may create a rio_rawqdc_fft
% class, and most functions can be symlinks to the 

if length(mia) > 1
  % assume all objects have the same parent(s)
  mia = mia(1);
end

% parent is the last field
s = struct(mia);
fn = fieldnames(s);
tmp = getfield(s, fn{end});
r = gettype(tmp, varargin{:});
