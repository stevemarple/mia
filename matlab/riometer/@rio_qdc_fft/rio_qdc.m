function r = rio_qdc(mia)
%RIO_QDC  Convert RIO_QDC_FFT to RIO_QDC.
%
%   r = RIO_QDC(mia)
%   r: RIO_QDC
%   mia: RIO_QDC_FFT
%
% See also RIO_QDC.


% this function is called as rio_qdc for rio_qdc_fft objects, and
% rio_rawqdc for rio_rawqdc_fft objects. Don't let the two get mixed up
parentclass = getparentclass(mia);
[tmp mfname] = fileparts(mfilename);
if strcmp(parentclass, 'rio_rawqdc') & strcmp(mfname, 'rio_qdc')
  % cannot convert directly. Convert to a rio_rawqdc and then linearise
  % to a rio_qdc.
  r = rio_qdc(rio_rawqdc(mia));
  return
end

if ~strcmp(mfname, parentclass)
  error(sprintf('%s can only be called with %s objects', mfname, ...
		[mfname '_fft']));
end


num = numel(mia);
if num ~= 1
  [tmp mfname] = fileparts(mfname);
  r = repmat(feval(mfname), size(mia));
  for n = 1:num
    r(n) = feval(mfname, mia(n));
  end
  return
end

% mia is always scalar from this point on

st = getstarttime(mia);
et = getendtime(mia);

r = feval(parentclass, ...
	  'starttime', st, ...
	  'endtime', et, ...
          'qdcperiodstarttime', getqdcperiodstarttime(mia), ...
          'qdcperiodendtime', getqdcperiodendtime(mia), ...
	  'sampletime', getsampletime(mia), ...
	  'integrationtime', getintegrationtime(mia), ...
	  'instrument', getinstrument(mia), ...
	  'data', getdata(recomputedata(mia)), ...
	  'units', getunits(mia), ...
	  'creator', getcreator(mia), ...
	  'processing', getprocessing(mia), ...
	  'beams', getbeams(mia));


r = addprocessing(r, sprintf('created from %s class', class(mia)));

% % now convert to the desired resolution and start/end times
% r = setresolutionfield(r, getresolution(mia));

r = adddataquality(r, getdataquality(mia.originalqdc));

switch parentclass
 case 'rio_qdc'
  r = adddataquality(r, getdataquality(mia.rio_qdc));
 case 'rio_rawqdc'
  r = adddataquality(r, getdataquality(mia.rio_rawqdc));
 otherwise
  error(sprintf('unknown parent class (was ''%s'')', parentclass));
end
