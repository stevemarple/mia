function r = setresolution(mia, res, varargin)
%SETRESOLUTION Set resolution of RIO_QDC_FFT objects.
%
%  Cannot set resolution for RIO_QDC_FFT object. This functions calls ERROR. 

% would have to deal with recomputing FFT coefficients and the mask
error(sprintf('Cannot set resolution of %s objects', class(mia)));



