function r = getparentclass(mia)
%GETPARENTCLASS  Return parent class for a RIO_QDC_FFT or RIO_RAWQDC_FFT object
%
%   r = GETPARENTCLASS(mia)
%   r: 'rio_qdc' when mia ISA rio_qdc_fft object
%   r: 'rio_rawqdc' when mia ISA rio_rawqdc_fft object
%
% See also RIO_QDC_FFT, RIO_RAWQDC_FFT.

% Cannot simply use class, may have the case of derived classes

if isa(mia, 'rio_qdc_fft')
  r = 'rio_qdc';
elseif isa(mia, 'rio_rawqdc_fft')
  r = 'rio_rawqdc';
else
  error(sprintf('unknown parentage for object of class %s', class(mia)));
end
