function r = getoriginalqdc(mia)
%GETORIGINALQDC  Return the QDC object used to make the FFT version
%
%   r = GETORIGINALQDC(mia);
%   r: original QDCs (of type derived from RIO_QDC_BASE)
%   mia: RIO_QDC_FFT
%
% GETORIGINALQDC returns the original QDC object, assuming it was
% preserved. To convert a RIO_QDC_FFT object to a plain RIO_QDC object use
% the convert function RIO_QDC.
%
% See also rio_qdc_fft_/RIO_QDC, rio_rawqdc_fft/RIO_RAWQDC.

if length(mia) ~= 1
  tmp = mia(1);
  r = repmat(tmp.originalqdc, size(mia));
  for n = 1:numel(mia)
    tmp = mia(n);
    r(n) = tmp.originalqdc;
  end
  return
end

r = mia.originalqdc;
