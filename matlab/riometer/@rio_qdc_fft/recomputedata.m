function r = recomputedata(mia)

num = numel(mia);
if num ~= 1
  [tmp mfname] = fileparts(mfilename);
  r = repmat(feval(mfilename), size(mia));
  for n = 1:num
    r(n) = feval(mfname, mia(n));
  end
  return
end
% mia is always scalar from this point on

beams = getbeams(mia);
nbeams = length(beams);


% convert stored coefficients to the full array
% ft = zeros(nbeams, ceil((siderealday) ./ mia.originalresolution));
ft = zeros(nbeams, floor((siderealday) ./ mia.originalresolution));

for n = 1:nbeams
  fitorder = size(mia.coefficients{n}, 2) - 1;
  ft(n, 1:(fitorder + 1)) = mia.coefficients{n};
  ft(n, (end-fitorder+1):end) = conj(mia.coefficients{n}(end:-1:2));
end
data = real(ifft(ft, [], 2));

% now blank the bad data
if ~isempty(mia.baddata)
  for n = 1:nbeams
    if islogical(mia.baddata{n})
      baddata = find(mia.baddata{n});
    else
      baddata = mia.baddata{n};
    end
    data(n, baddata) = nan;
  end
end

r = setdata(mia, data);
