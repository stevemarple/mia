function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for RIO_QDC_FFT data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = rio_base_insertcheck(a, b);

if isempty(a.originalqdc) ~= isempty(b.originalqdc)
  error(['can only insert when items both have (or do not have) ' ...
	 ' originalqdc data']);
end

if ~isempty(a.originalqdc)
  if ~isequal(a.originalresolution, b.originalresolution)
    error('cannot insert unless original resolutions are equal');
  end
end

rs.originalresolution = a.originalresolution;
