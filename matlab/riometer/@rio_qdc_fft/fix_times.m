function r = fix_times(mia)
%FIX_TIMES  Fix TIMESTAMP/TIMESPAN objects in a RIO_(RAW)QDC_FFT object


r = mia;
r.originalqdc = fix_times(r.originalqdc);
r.rio_qdc = fix_times(r.rio_qdc);

if isstruct(r.originalresolution)
  r.originalresolution = timespan(r.originalresolution);
end
