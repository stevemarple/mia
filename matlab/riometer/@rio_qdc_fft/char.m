function r = char(mia, varargin)
%CHAR  Convert a RIO_QDC_FFT object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

if isempty(mia.originalresolution)
  res = '';
else 
  res = char(mia.originalresolution);
end
 
if ~isempty(mia.originalqdc)
  qdc = matrixinfo(mia.originalqdc);
else
  qdc = '';
end
pc = class(getparent(mia)); % parent class

r = sprintf(['%s' ...
	     'original res.     : %s\n' ...
	     'original QDC      : %s\n' ...
	     'FFT coefficients  : %s\n'], ...
	    rio_qdc_base_char(mia), res, qdc, matrixinfo(mia.coefficients));
% char(getparent(mia)), res, qdc, matrixinfo(mia.coefficients));
    
