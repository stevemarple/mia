function r = setcoefficients(mia, c)
%SETCOEFFICIENTS 

if length(mia) ~= 1
  error('only scalar objects are allowed');
end
r = mia;
r.coefficients = c;

