function r = clearoriginalqdcdataquality(mia, varargin)

r = mia;
for n = 1:numel(r)
  tmp = r(n);
  tmp.originalqdc = cleardataquality(tmp.originalqdc, varargin{:});
  r(n) = tmp;
end
