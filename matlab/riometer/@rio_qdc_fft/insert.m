function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  INSERT function for RIO_QDC_FFT objects.
%
% See also mia_base/INSERT, rio_qdc_base/INSERT.


[r aa ar ba br rs] = rio_qdc_base_insert(a, b);

% now insert the original QDCs
if ~isempty(a.originalqdc) & ~isempty(b.originalqdc)
  r.originalqdc = insert(a.originalqdc, b.originalqdc);
else
  r.originalqdc = [];
end

r.originalresolution = rs.originalresolution;

% adapt the subsasgn/subsref mapping for single index
aa2 = aa;
aa2.subs(2:end) = [];

ar2 = ar;
ar2.subs(2:end) = [];

ba2 = ba;
ba2.subs(2:end) = [];

br2 = br;
br2.subs(2:end) = [];

% insert the coefficients
r.coefficients = cell(numel(rs.beams), 1);
r.coefficients = feval(rs.subsasgn, r.coefficients, aa2, ...
                       feval(rs.subsref, a.coefficients, ar2));
r.coefficients = feval(rs.subsasgn, r.coefficients, ba2, ...
                       feval(rs.subsref, b.coefficients, br2));
  
% insert the bad data values (if any)
a_baddata = a.baddata;
b_baddata = b.baddata;
if isempty(a_baddata) & isempty(b_baddata)
  % no bad data at all, leave empty
  r.baddata = {};
  
else
  if isempty(a_baddata)
    a_baddata = {[]};
  end
  if isempty(b_baddata)
    b_baddata = {[]};
  end

  
  r_baddata = cell(numel(rs.beams), 1);
  r_baddata = feval(rs.subsasgn, r_baddata, aa2, ...
                    feval(rs.subsref, a_baddata, ar2));
  r_baddata = feval(rs.subsasgn, r_baddata, ba2, ...
                    feval(rs.subsref, b_baddata, br2));
 
 
  
end

