function mia = rio_qdc_fft(varargin)
%RIO_QDC_FFT  Constructor for RIO_QDC_FFT class.
%
% mia = RIO_QDC_FFT(...)
%
% Construct a RIO_QDC_FFT.



% This function is constructor for both rio_qdc_fft and rio_rawqdc_fft
% classes. Need to work out the class name and parent
[tmp cls] = fileparts(mfilename);
parent = cls(1:(end-4)); % remove '_fft' part from class name

% Make it easy to change the class definition at a later date
latestversion = 2;
mia.versionnumber = latestversion;
mia.coefficients = {};

% mask holding positions of bad data
mia.baddata = {};

% the resolution of the QDC the FFT version was generated from. Must be set
mia.originalresolution = [];

% the original object used to construct the FFT version. Can be discarded.
mia.originalqdc = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), parent)
  % construct from base object
  p = varargin{1};
  mia = class(mia, cls, p);
  
% elseif nargin == 1 & isa(varargin{1}, cls)
%  % copy constructor / converter to base class    
%  p = feval(parent);
%  
%  mia = class(struct(varargin{1}), cls, p);
  
elseif isstruct(varargin{1}) | isa(varargin{1}, cls)
  v = varargin{1};
  if ~isstruct(v)
    v = struct(v);
  end
  
  % preserve parent information
  if isfield(v, parent)
    p = getfield(v, parent);
    v = rmfield(v, parent);
  else
    p = feval(parent);
  end

  if v.versionnumber ~= latestversion
    disp(sprintf('upgrading object to %s version %d', cls, latestversion));
  end
  
  switch v.versionnumber
   case 1
    % no mask field and coefficients was a double array
    v.baddata = [];
    v.coefficients = {v.coefficients};
    if length(getbeams(p)) ~= 1
      % don't allow multiple beam versions, something badly wrong
      error(sprintf(['version 1 of %s class could not contain ' ...
		     'multiple beams'], cls));
    end
    
   case 2
    % no changes needed
    ;
    
   otherwise
    error(sprintf('unknown version (was %d)', v.versionnumber));
  end
  v = rmfield(v, 'versionnumber');
  
  fn = fieldnames(v);
  for n = 1:length(fn)
    mia = setfield(mia, fn{n}, getfield(v, fn{n}));
  end
  mia = class(mia, cls, p);

elseif isa(varargin{1}, 'rio_rawqdc_fft')
  % converting a rio_rawqdc_fft object to a rio_qdc_fft one
  mia = rio_qdc_fft(rio_qdc(getoriginalqdc(varargin{1})));
  return
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);

  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  defaults.time = [];
  
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log' 'time'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi}, 'load', 0);
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, defaults.time, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end

else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end


% mask should contain only positions of bad data, but if the mask is logical
% then convert to positions
for n = 1:numel(mia)
  tmp = mia(n);
  if ~isempty(tmp.baddata)
    for m = 1:numel(tmp.baddata)
      if islogical(tmp.baddata)
	tmp.baddata{m} = find(tmp.baddata{m});
      end
    end
    mia(n) = tmp;
  end
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
