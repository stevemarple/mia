function r = getfitorder(mia)
%GETFITORDER  Return the fit order used in a RIO_QDC_FFT object

num = numel(mia);
if num ~= 1
  r = cell(size(mia));
  for n = 1:num
    r{n} = feval(mfilename, mia(n));
  end
  
else
  coeffs = getcoefficients(mia);
  cn = numel(coeffs);
  r = zeros(cn, 1);
  for n = 1:cn
    r(n) = length(coeffs{n});
  end
  r = r - 1;
end



