function r = info_rio_syo_1_data
%INFO_RIO_SYO_1_DATA Return basic information about rio_syo_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_syo_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=syo;serialnumber=1

r.limits = [];
r.limits.rio_power = [0 4096];
r.limits.rio_qdc = [0 4096];
r.limits.rio_rawpower = [0 4096];
r.limits.rio_rawqdc = [0 4096];
r.pixels.aacgm.x.max = 73.1;
r.pixels.aacgm.x.min = 70.1;
r.pixels.aacgm.x.step = 0.2;
r.pixels.aacgm.y.max = -66.4;
r.pixels.aacgm.y.min = -67.5;
r.pixels.aacgm.y.step = 0.1;
r.pixels.deg.x.max = 41.1;
r.pixels.deg.x.min = 38.3;
r.pixels.deg.x.step = 0.2;
r.pixels.deg.y.max = -68.5;
r.pixels.deg.y.min = -69.5;
r.pixels.deg.y.step = 0.1;
r.pixels.km.x.max = 80;
r.pixels.km.x.min = -80;
r.pixels.km.x.step = 8;
r.pixels.km.y.max = 80;
r.pixels.km.y.min = -80;
r.pixels.km.y.step = 8;
r.pixels.km_antenna.x.max = 80;
r.pixels.km_antenna.x.min = -80;
r.pixels.km_antenna.x.step = 8;
r.pixels.km_antenna.y.max = 80;
r.pixels.km_antenna.y.min = -80;
r.pixels.km_antenna.y.step = 8;
r.pixels.m.x.max = 80000;
r.pixels.m.x.min = -80000;
r.pixels.m.x.step = 8000;
r.pixels.m.y.max = 80000;
r.pixels.m.y.min = -80000;
r.pixels.m.y.step = 8000;
r.pixels.m_antenna.x.max = 80000;
r.pixels.m_antenna.x.min = -80000;
r.pixels.m_antenna.x.step = 8000;
r.pixels.m_antenna.y.max = 80000;
r.pixels.m_antenna.y.min = -80000;
r.pixels.m_antenna.y.step = 8000;
r.abbreviation = 'syo';
r.antennaazimuth = -47.9385;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.62;
r.azimuth = [315 324 336.6 351.9 8.1 23.4 36 45 306 315 329.4 348.3 ...
    11.7 30.6 45 54 293.4 300.6 315 342 18 45 59.4 66.6 278.1 281.7 ...
    288 315 45 72 78.3 81.9 261.9 258.3 252 225 135 108 101.7 98.1 ...
    246.6 239.4 225 198 162 135 120.6 113.4 234 225 210.6 191.7 ...
    168.3 149.4 135 126 225 216 203.4 188.1 171.9 156.6 144 135];
r.badbeams = [1 2 7 8 9 16 49 56 57 58 63 64];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [13.8 13.7 12.7 12.2 12.2 12.7 13.7 13.8 13.7 12.2 ...
    11.5 11.2 11.2 11.5 12.2 13.7 12.7 11.5 10.9 10.6 10.6 10.9 ...
    11.5 12.7 12.2 11.2 10.6 10.4 10.4 10.6 11.2 12.2 12.2 11.2 ...
    10.6 10.4 10.4 10.6 11.2 12.3 12.7 11.5 10.9 10.6 10.6 10.9 ...
    11.5 12.7 13.7 12.2 11.5 11.2 11.2 11.5 12.2 13.7 13.8 13.7 ...
    12.7 12.2 12.2 12.7 13.7 13.8];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2010 12 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1 9];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 15;
r.ifrequency = 3e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 ...
    62 63 64];
r.latitude = -69;
r.location1 = 'Syowa';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 39.58;
r.modified = timestamp([2011 07 29 12 10 48.801594]);
r.name = '';
r.piid = 14;
r.qdcclass = 'rio_rawqdc';
r.qdcduration = timespan(28 , 'd');
r.qdcoffset = timespan(00, 'h', 00, 'm', 00, 's');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [2.33729 1.78777 1.49817 1.3583 1.3583 ...
    1.49817 1.78777 2.33729 1.7558 1.37961 1.22817 1.15345 1.15345 ...
    1.22817 1.37961 1.7558 1.45374 1.22712 1.09387 1.05364 1.05364 ...
    1.09387 1.22712 1.45374 1.371 1.15142 1.05244 1.0095 1.0095 ...
    1.05244 1.15142 1.371 1.371 1.15142 1.05244 1.0095 1.0095 ...
    1.05244 1.15142 1.371 1.45374 1.22712 1.09387 1.05364 1.05364 ...
    1.09387 1.22712 1.45374 1.7558 1.37961 1.22817 1.15345 1.15345 ...
    1.22817 1.37961 1.7558 2.33729 1.78777 1.49817 1.3583 1.3583 ...
    1.49817 1.78777 2.33729];
r.starttime = timestamp([1992 03 01 00 00 00]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [67.5 56.7 48.6 44.1 44.1 48.6 56.7 67.5 56.7 44.1 35.1 ...
    30.6 30.6 35.1 44.1 56.7 48.6 35.1 25.2 18 18 25.2 35.1 48.6 ...
    44.1 30.6 18 8.1 8.1 18 30.6 44.1 44.1 30.6 18 8.1 8.1 18 30.6 ...
    44.1 48.6 35.1 25.2 18 18 25.2 35.1 48.6 56.7 44.1 35.1 30.6 ...
    30.6 35.1 44.1 56.7 67.5 56.7 48.6 44.1 44.1 48.6 56.7 67.5];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1 , 'd');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'niprriodata';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/original_format/%Y/%m/%y%m%d%H.SOY';
r.defaultfilename.original_format.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 86400];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'niprriodata';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/original_format/%Y/%m/%y%m%d%H.SOY';
r.defaultfilename.rio_power.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 86400];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = false;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [64 3600];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = false;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [64 -1];
r.defaultfilename.rio_qdc_fft.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'niprriodata';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/original_format/%Y/%m/%y%m%d%H.SOY';
r.defaultfilename.rio_rawpower.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 86400];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = false;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [64 3600];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.defaultfilename.rio_rawqdc_fft.default.archive = 'default';
r.defaultfilename.rio_rawqdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc_fft.default.failiffilemissing = false;
r.defaultfilename.rio_rawqdc_fft.default.format = 'mat';
r.defaultfilename.rio_rawqdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/syo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc_fft.default.loadfunction = '';
r.defaultfilename.rio_rawqdc_fft.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc_fft.default.savefunction = '';
r.defaultfilename.rio_rawqdc_fft.default.size = [64 -1];
r.defaultfilename.rio_rawqdc_fft.default.units = 'ADC';
r.institutions{1} = 'National Institute for Polar Research';
% end of function
