function r = info_rio_gak_2_data
%INFO_RIO_GAK_2_DATA Return basic information about rio_gak_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_gak_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=gak;serialnumber=2

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'gak';
r.antennaazimuth = 0;
r.antennaphasingx = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
r.antennaphasingy = [0.9375 0.8125 0.6875 0.5625 0.4375 0.3125 ...
    0.1875 0.0625 -0.0625 -0.1875 -0.3125 -0.4375 -0.5625 -0.6875 ...
    -0.8125 -0.9375];
r.antennas = [16 1];
r.antennaspacing = 0.5;
r.azimuth = [];
r.badbeams = [];
r.beamplanc = 1;
r.beamplanr = 16;
r.beamwidth = [];
r.bibliography = 'art:327';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = 16;
r.id = 11;
r.ifrequency = 3.86e+07;
r.imaging = true;
r.imagingbeams = [];
r.latitude = 62.3917;
r.location1 = 'Gakona';
r.location1_ascii = '';
r.location2 = 'Alaska';
r.logo = '';
r.logurl = '';
r.longitude = -145.147;
r.modified = timestamp([2010 09 02 15 15 44.558655]);
r.name = 'HAARP';
r.piid = 6;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'University of Maryland';
% end of function
