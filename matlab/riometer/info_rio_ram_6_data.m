function r = info_rio_ram_6_data
%INFO_RIO_RAM_6_DATA Return basic information about rio_ram_6.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_ram_6. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=ram;serialnumber=6

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'ram';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [15];
r.bibliography = '';
r.comment = 'Test riometer at EISCAT';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1];
r.iantennatype = '';
r.ibeams = [];
r.id = 129;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 69.6441;
r.location1 = 'Ramfjordmoen';
r.location1_ascii = '';
r.location2 = 'Norway';
r.logo = 'eiscat_logo';
r.logurl = '';
r.longitude = 19.4905;
r.modified = timestamp([2010 07 01 12 39 14.56437]);
r.name = '';
r.piid = [];
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(1 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 6;
r.standardobliquity = [1.0353];
r.starttime = timestamp([2008 06 27 23 00 00]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = 7.953e+06;
r.widebeams = [1];
r.zenith = [18];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(00, 'h', 00, 'm', 15, 's');
r.defaultfilename.original_format.default.failiffilemissing = true;
r.defaultfilename.original_format.default.format = 'ram_6';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_6/original_format/%Y/%m/%d/%d%m%y_%H%M%S.txt';
r.defaultfilename.original_format.default.loadfunction = 'ram_6_to_mia';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [1 15];
r.defaultfilename.original_format.default.units = 'dBm';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'mat';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_6/rio_power/%Y/%m/%Y%m%d.mat';
r.defaultfilename.rio_power.default.loadfunction = '';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [1 86400];
r.defaultfilename.rio_power.default.units = 'dBm';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_6/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [1 86164];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.institutions = {};
% end of function
