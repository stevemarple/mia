function r = info_rio_ram_1_data
%INFO_RIO_RAM_1_DATA Return basic information about rio_ram_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_ram_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=ram;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'ram';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = 4;
r.beamplanr = 4;
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([1995 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = 16;
r.id = 98;
r.ifrequency = [];
r.imaging = true;
r.imagingbeams = [];
r.latitude = 69.586;
r.location1 = 'Ramfjordmoen';
r.location1_ascii = '';
r.location2 = 'Norway';
r.logo = '';
r.logurl = '';
r.longitude = 19.227;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 23;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = '';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Max-Planck-Institut f�r Aeronomie';
% end of function
