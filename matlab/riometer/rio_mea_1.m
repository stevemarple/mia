function r = rio_mea_1
%RIO_MEA_1  RIOMETER object for Meanook, Canada.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'mea', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'NRCAN', ...
    'location', location('Meanook', 'Canada', ...
                         54.600000, -113.300000), ...
    'frequency', 3e+07);

