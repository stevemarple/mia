function r = rio_sgo
%RIO_SGO  Return a list of the SGO RIOMETER instruments.
%
%   r = RIO_SGO
%   r: vector of RIOMETER objects
%
%   For more details about SGO riometers see http://www.sgo.fi/
%
%   See also RIOMETER.

% Get list of all known riometers
tmp = instrumenttypeinfo(riometer, 'aware');

r = tmp(strcmp(getfacility(tmp), 'SGO riometer chain'));

% r = [rio_abi_1 rio_hor_1 rio_iva_1 rio_jyv_1 rio_oul_1 rio_oul_2 ...
%      rio_rov_1 rio_sod_1 rio_sod_2];
