function r = niprriodata_to_mia(mia, varargin)
%NIPRRIODATA_TO_MIA. Load NIPR riometer data.
%
%   r = NIPRRIODATA_TO_MIA(mia, ...)
%   
% NIPRRIODATA_TO_MIA is not intended to be called directly by users but
% is part of the automated process for loading data from NIPR riometers,
% and is also used for the Zhongshan and Ny Alesund stations as their
% data is stored in the same file format.
%
% To convert a single NIPR data file to a MIA object see
% NIPRRIODATA_FILE_TO_MIA.


cls = class(mia);

defaults.cancelhandle = [];

defaults.filename = '';
defaults.archive = ''; % accepted but ignored
defaults.failiffilemissing = [];
defaults.resolution = [];
defaults.resolutionmethod = '';
defaults.calibrationdata = 0;
defaults.complexdata = 0;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

rio = getinstrument(mia);
if ~isa(rio, 'riometer')
  error('instrument must be a riometer');
end

st = getstarttime(mia);
et = getendtime(mia);

bestres = info(rio, 'bestresolution');
beams = getbeams(mia);
if isempty(beams)
  beams = info(rio, 'allbeams');
end


r = [];

% get default filename information (struct)
df = info(rio, 'defaultfilename', mia);

if isempty(defaults.failiffilemissing)
  % use default for this riometer
  defaults.failiffilemissing = df.failiffilemissing; 
end

if isempty(defaults.filename)
  defaults.filename = df.fstr;
end

% Normally files contain a whole day of data, however if data recording is
% interrupted the file is opened with a non-zero hour. Therefore have to
% work around the possibility of having multiple files for a given
% day. Check that the basename of the file matches '%y%m%d00' (or is already
% '%y%m%d%H'), then insert hours into the filename format specifier.

[p f e] = fileparts(defaults.filename);

switch f
 case '%y%m%d%H'
  % already correct
  ;
 case '%y%m%d00'
  % insert %H into filename, but don't use fullfile because on windows
  % this will reconstruct a URL to use backslashes!
  defaults.filename(end-length(e)-[1 0])='%H';
 otherwise
  error('filename does not match original naming convention');
end

% round start time to start of file
st2 = floor(st, df.duration);

% round up end time to end of following file (data can run over into next
% file)
et2 = ceil(et, df.duration);
hour = timespan(1, 'h');
t = st2;
tmp_et = st;

tmp_mia = feval(cls, ...
                'load', 0, ...
                'instrument', rio, ...
                'beams', beams);
while t < et2
  
  filename = strftime(t, defaults.filename);
  
  % allow graphical user interface to interrupt loading of data
  cancelcheck(defaults.cancelhandle);

  if ~url_exist(filename)
    % only generate errors if file is supposed to be present, and the
    % hour is 0. If recording wasn't interrupted then the zero hour file
    % contains all the data, and the other hours files should be missing.
    if defaults.failiffilemissing & datevec(t) == 0
      error(sprintf('Cannot load %s', filename));
    end
  else
    
    tmp = niprriodata_file_to_mia(filename, ...
                                  'mia', tmp_mia, ...
                                  'cancelhandle', defaults.cancelhandle);
    
    if isequal(r, [])
      r = tmp;
    else
      r = nonaninsert(r, tmp);
      % r = insert(r, tmp);  
    end
    
    if getendtime(tmp) >= et
      % file just read contains the last data needed. Stop.
      break;
    end
  end
  
  % move on to next hour file
  t = t + hour; 
end

if isequal(r, [])
  % no data could be loaded
  r = feval(cls, ...
            'starttime', st, ...
            'endtime', et, ...
            'integrationtime', bestres,...    
            'instrument', rio, ...
            'data', [], ...
            'units', 'ADC', ...
            'load', 0, ...
            'beams', beams);
else
  % trim to requested start/end times
  r = extract(r, ...
              'starttime', st, ...
              'endtime', et);      

  % have start/end times set to exactly what was requested
  r = setstarttime(r, st);
  r = setendtime(r, et);
  
  if ~isempty(defaults.resolution)
    r = setresolution(r, defaults.resolution, defaults.resolutionmethod);
  end
end
  
