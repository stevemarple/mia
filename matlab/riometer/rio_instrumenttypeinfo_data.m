function r = rio_instrumenttypeinfo_data
%RIO_INSTRUMENTTYPEINFO_DATA  Type information data function for riometer class.
% Do not access this function directly, use the INSTRUMENTTYPEINFO
% function instead.
%
% See INSTRUMENTTYPEINFO

r.aware = [rio_a77_1 rio_a80_1 rio_a81_1 rio_a84_1 rio_abi_1 rio_ale_1 ...
		   rio_amk_1 rio_and_1 rio_and_2 rio_ask_1 rio_back_1 rio_blo_1 ...
		   rio_brd_1 rio_cbb_1 rio_ccs_1 rio_cont_1 rio_csy_1 rio_daws_1 ...
		   rio_dik_1 rio_dmh_1 rio_dnb_1 rio_drao_1 rio_drv_1 rio_dvs_1 ...
		   rio_dvs_2 rio_eski_1 rio_eua_1 rio_fchu_1 rio_fhb_1 rio_fsim_1 ...
		   rio_fsmi_1 rio_gak_1 rio_gak_2 rio_gdh_1 rio_ghb_1 rio_gill_1 ...
		   rio_hal_1 rio_hal_2 rio_his_1 rio_hor_1 rio_hus_1 rio_hus_2 ...
		   rio_inu_1 rio_iqa_1 rio_iqa_2 rio_isll_1 rio_iva_1 rio_jyv_1 ...
		   rio_kar_1 rio_kar_2 rio_kil_1 rio_kir_1 rio_kir_2 rio_lyc_1 ...
		   rio_lyr_1 rio_mai_1 rio_mai_2 rio_maw_1 rio_mcm_1 rio_mcm_2 ...
		   rio_mcm_3 rio_mcmu_1 rio_mcq_1 rio_mea_1 rio_mir_1 rio_mol_1 ...
		   rio_nal_1 rio_nal_2 rio_naq_1 rio_nrd_1 rio_nvl_1 rio_ott_1 ...
		   rio_oul_1 rio_oul_2 rio_p1_1 rio_p2_1 rio_p3_1 rio_p4_1 rio_p5_1 ...
		   rio_p6_1 rio_pina_1 rio_pkr_1 rio_pon_1 rio_qik_1 rio_rabb_1 ...
		   rio_ram_1 rio_ram_2 rio_ram_3 rio_ram_4 rio_ram_6 rio_rank_1 ...
		   rio_res_1 rio_rov_1 rio_sas_1 rio_sco_1 rio_skt_1 rio_sms_1 ...
		   rio_sna_1 rio_sna_2 rio_sna_3 rio_sna_4 rio_sna_5 rio_sna_6 ...
		   rio_snk_1 rio_sod_1 rio_sod_2 rio_spa_1 rio_spa_2 rio_spa_3 ...
		   rio_spa_4 rio_stf_1 rio_stj_1 rio_syo_1 rio_syo_2 rio_syo_3 ...
		   rio_talo_1 rio_thl_1 rio_tik_1 rio_tjo_1 rio_tjo_2 rio_tnb_1 ...
		   rio_tnb_2 rio_viz_1 rio_vos_1 rio_ykc_1 rio_zhs_1];
r.supported.any = [rio_abi_1 rio_ale_1 rio_and_2 rio_back_1 rio_brd_1 ...
				   rio_cbb_1 rio_cont_1 rio_csy_1 rio_daws_1 rio_dmh_1 ...
				   rio_drao_1 rio_dvs_1 rio_dvs_2 rio_eski_1 rio_eua_1 ...
				   rio_fchu_1 rio_fsim_1 rio_fsmi_1 rio_gill_1 rio_hal_1 ...
				   rio_hal_2 rio_hor_1 rio_hus_1 rio_inu_1 rio_iqa_1 ...
				   rio_iqa_2 rio_isll_1 rio_iva_1 rio_jyv_1 rio_kar_1 ...
				   rio_kar_2 rio_kil_1 rio_lyr_1 rio_mai_2 rio_maw_1 ...
				   rio_mcm_1 rio_mcm_2 rio_mcm_3 rio_mcmu_1 rio_mcq_1 ...
				   rio_mea_1 rio_mir_1 rio_nal_1 rio_nal_2 rio_ott_1 ...
				   rio_oul_1 rio_oul_2 rio_pina_1 rio_pkr_1 rio_pon_1 ...
				   rio_qik_1 rio_rabb_1 rio_ram_2 rio_ram_3 rio_ram_4 ...
				   rio_ram_6 rio_rank_1 rio_res_1 rio_rov_1 rio_sas_1 ...
				   rio_sna_1 rio_snk_1 rio_sod_1 rio_sod_2 rio_spa_1 ...
				   rio_spa_2 rio_spa_3 rio_spa_4 rio_stf_1 rio_stj_1 ...
				   rio_syo_1 rio_talo_1 rio_tjo_1 rio_vos_1 rio_ykc_1 ...
				   rio_zhs_1];
r.supported.original_format = [ rio_and_2 rio_dmh_1 rio_dvs_2 rio_hal_1 ...
					rio_hus_1 rio_kar_1 rio_kar_2 rio_kil_1 rio_lyr_1 ...
					rio_mai_2 rio_nal_1 rio_nal_2 rio_ram_2 rio_ram_3 ...
					rio_ram_4 rio_ram_6 rio_sna_1 rio_syo_1 rio_tjo_1 ...
					rio_zhs_1];

r.supported.rio_abs = [ rio_abi_1 rio_ale_1 rio_and_2 rio_back_1 rio_brd_1 ...
					rio_cbb_1 rio_cont_1 rio_csy_1 rio_daws_1 rio_dmh_1 ...
					rio_drao_1 rio_dvs_1 rio_dvs_2 rio_eski_1 rio_eua_1 ...
					rio_fchu_1 rio_fsim_1 rio_fsmi_1 rio_gill_1 rio_hal_1 ...
					rio_hal_2 rio_hor_1 rio_hus_1 rio_inu_1 rio_iqa_1 ...
					rio_iqa_2 rio_isll_1 rio_iva_1 rio_jyv_1 rio_kar_1 ...
					rio_kar_2 rio_kil_1 rio_lyr_1 rio_mai_2 rio_maw_1 ...
					rio_mcm_1 rio_mcm_2 rio_mcm_3 rio_mcmu_1 rio_mcq_1 ...
					rio_mea_1 rio_mir_1 rio_nal_1 rio_nal_2 rio_ott_1 ...
					rio_oul_1 rio_oul_2 rio_pina_1 rio_pkr_1 rio_pon_1 ...
					rio_qik_1 rio_rabb_1 rio_ram_2 rio_ram_3 rio_ram_4 ...
					rio_ram_6 rio_rank_1 rio_res_1 rio_rov_1 rio_sas_1 ...
					rio_sna_1 rio_snk_1 rio_sod_1 rio_sod_2 rio_spa_1 ...
					rio_spa_2 rio_spa_3 rio_spa_4 rio_stf_1 rio_stj_1 ...
					rio_syo_1 rio_talo_1 rio_tjo_1 rio_vos_1 rio_ykc_1 ...
					rio_zhs_1];
r.supported.rio_power = [rio_abi_1 rio_and_2 rio_dmh_1 rio_dvs_2 rio_hal_1 ...
					rio_hus_1 rio_iqa_1 rio_kar_1 rio_kar_2 rio_kil_1 ...
					rio_lyr_1 rio_mai_2 rio_mcm_3 rio_nal_1 rio_nal_2 ...
					rio_ram_2 rio_ram_3 rio_ram_4 rio_ram_6 rio_sna_1 ...
					rio_spa_4 rio_stf_1 rio_syo_1 rio_tjo_1 rio_zhs_1];
r.supported.rio_power_calib = [rio_and_2 rio_mai_2 rio_nal_2 rio_ram_2 ...
					rio_ram_3 rio_ram_4];
r.supported.rio_qdc = [rio_abi_1 rio_and_2 rio_dmh_1 rio_dvs_2 rio_hal_1 ...
					rio_hus_1 rio_iqa_1 rio_kar_1 rio_kar_2 rio_kil_1 ...
					rio_lyr_1 rio_mai_2 rio_mcm_3 rio_nal_1 rio_nal_2 ...
					rio_pkr_1 rio_ram_2 rio_ram_3 rio_ram_4 rio_ram_6 ...
					rio_sna_1 rio_spa_4 rio_stf_1 rio_syo_1 rio_tjo_1 ...
					rio_zhs_1];
r.supported.rio_qdc_fft = [rio_abi_1 rio_and_2 rio_kar_1 rio_kar_2 rio_kil_1 ...
					rio_lyr_1 rio_mai_2 rio_nal_1 rio_nal_2 rio_ram_2 ...
					rio_ram_3 rio_ram_4 rio_syo_1 rio_tjo_1 rio_zhs_1];
r.supported.rio_qdc_mean_sd = [rio_abi_1 rio_and_2 rio_kar_1 rio_kar_2 ...
					rio_kil_1 rio_mai_2 rio_nal_2];
r.supported.rio_rawpower = [rio_abi_1 rio_and_2 rio_back_1 rio_cont_1 ...
					rio_daws_1 rio_dmh_1 rio_dvs_2 rio_eski_1 rio_fchu_1 ...
					rio_fsim_1 rio_fsmi_1 rio_gill_1 rio_hal_1 rio_hus_1 ...
					rio_iqa_1 rio_isll_1 rio_kar_1 rio_kar_2 rio_kil_1 ...
					rio_lyr_1 rio_mai_2 rio_mcm_3 rio_mcmu_1 rio_nal_1 ...
					rio_nal_2 rio_pina_1 rio_rabb_1 rio_ram_2 rio_ram_3 ...
					rio_ram_4 rio_rank_1 rio_sna_1 rio_spa_4 rio_stf_1 ...
					rio_syo_1 rio_talo_1 rio_tjo_1 rio_zhs_1];
r.supported.rio_rawpower_calib = [rio_and_2 rio_mai_2 rio_ram_2 ...
    rio_ram_3];
r.supported.rio_rawqdc = [rio_abi_1 rio_and_2 rio_dvs_2 rio_hal_1 ...
    rio_hus_1 rio_iqa_1 rio_lyr_1 rio_mai_2 rio_mcm_3 rio_nal_1 ...
    rio_nal_2 rio_sna_1 rio_spa_4 rio_stf_1 rio_syo_1 rio_tjo_1 ...
    rio_zhs_1];
r.supported.rio_rawqdc_fft = [rio_abi_1 rio_hus_1 rio_lyr_1 ...
    rio_nal_1 rio_syo_1 rio_tjo_1 rio_zhs_1];
r.supported.temp_data = [rio_and_2 rio_mai_2 rio_nal_2];

% end of function
