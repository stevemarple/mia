function r = info_rio_sna_5_data
%INFO_RIO_SNA_5_DATA Return basic information about rio_sna_5.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_sna_5. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=sna;serialnumber=5

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'sna';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = 'Digital data available from June 1979.';
r.credits = '';
r.datarequestid = 19;
r.defaultheight = [];
r.endtime = timestamp([1994 12 31 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 112;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -70.312;
r.location1 = 'SANAE III';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = -2.41;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 19;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 5;
r.standardobliquity = [];
r.starttime = timestamp([1979 01 01 00 00 00]);
r.systemtype = '';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = 3e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Potchefstroom University for Christian Higher Education';
% end of function
