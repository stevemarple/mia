function r = info_rio_nvl_1_data
%INFO_RIO_NVL_1_DATA Return basic information about rio_nvl_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_nvl_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=nvl;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'nvl';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'AARI riometer chain';
r.facility_url = 'http://www.aari.nw.ru/clgmi/geophys/station.htm';
r.facilityid = 13;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 80;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -70.77;
r.location1 = 'Novolazarevskaya';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 11.83;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 9;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = 'http://www.aari.nw.ru/clgmi/geophys/station.htm';
r.url2 = {'http://www.aari.nw.ru/clgmi/geophys/station.htm'};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Arctic and Antarctic Research Institute';
% end of function
