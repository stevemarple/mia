function r = rio_rov_1
%RIO_ROV_1  RIOMETER object for Rovaniemi, Finland.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'rov', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'SGO riometer chain', ...
    'location', location('Rovaniemi', 'Finland', ...
                         66.780000, 25.940000), ...
    'frequency', 3.24e+07);

