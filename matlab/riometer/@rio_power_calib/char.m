function r = char(mia, varargin)
%CHAR  Convert a RIO_POWER_CALIB object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let rio_base decide. 
  r = rio_base_char(mia, varargin{:});
  return;
end

datatype_str = gettype(mia);
if ~isempty(datatype_str)
  datatype_str = ['riometer ' datatype_str];
end



method = getcalibrationmethod(mia);

if isa(mia.calibrationinterval, 'timespan')
  if numel(mia.calibrationinterval) == 1
    calib_int_str = char(mia.calibrationinterval);
  else
    calib_int_str = matrixinfo(mia.calibrationinterval);
  end
else
  calib_int_str = char(mia.calibrationinterval);
end

if isa(mia.calibrationduration, 'timespan')
  if numel(mia.calibrationduration) == 1
    calib_dur_str = char(mia.calibrationduration);
  else
    calib_dur_str = matrixinfo(mia.calibrationduration);
  end
else
  calib_dur_str = char(mia.calibrationduration);
end

if islogical(mia.commoncalibrationdata)
  if mia.commoncalibrationdata
    common_str = 'true';
  else
    common_str = 'false';
  end
else
  common_str = matrixinfo(mia.commoncalibrationdata);
end

r = sprintf(['%s' ...
	     'calibration method: %s\n' ...
             'calib. interval   : %s\n' ...
             'calib. duration   : %s\n' ...
	     'calibration data  : %s\n' ...
             'common calib. data: %s\n' ...
	     'calibration times : %s\n'], ...
	    rio_base_char(mia, varargin{:}), ...
	    method, ...
            calib_int_str, ...
            calib_dur_str, ...
	    matrixinfo(mia.calibrationdata), ...
            common_str, ...
	    matrixinfo(mia.calibrationtimestamps));



