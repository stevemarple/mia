function r = getparentclass(mia)
%GETPARENTCLASS  Get parent class for RIO_RAWPOWER_CALIB or RIO_POWER_CALIB
%
%   r = GETPARENTCLASS(mia)
%   r: 'rio_rawpower' when mia ISA rio_rawpower_calib object
%   r: 'rio_power' when mia ISA rio_power_calib object
%
% See also RIO_QDC_FFT, RIO_RAWQDC_FFT.

% Cannot simply use class, may have the case of derived classes

if isa(mia, 'rio_power_calib')
  r = 'rio_power';
elseif isa(mia, 'rio_rawpower_calib')
  r = 'rio_rawpower';
else
  error(sprintf('unknown parentage for object of class %s', class(mia)));
end

