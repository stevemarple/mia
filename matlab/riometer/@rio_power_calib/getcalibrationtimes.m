function r = getcalibrationtimes(mia, varargin)
%GETCALIBRATIONDATA  Return the calibration data matrix, or part of.
%
%   r = getdata(mia)
%   r = getdata(mia, n);
%   r: calibration times (TIMESTAMP)
%   mia: RIO_POWER_CALIB object
%   n: index into calibration timestamps (numeric)
%
%   Return the calibration times, or some of them.
%
%   See also mia_base/GETDATA, GETBEAMINDEX, SUBSREF.

switch numel(varargin)
 case 0
  r = mia.calibrationtimestamps;
  
 case 1
  if strcmp('end', varargin{1})
    r = mia.calibrationtimestamps(end);
  elseif isnumeric(varargin{1})
    r = mia.calibrationtimestamps(varargin{1});
  else
    error('incorrect type for subscript');
  end
    
 otherwise
  error('incorrect parameters');
end
