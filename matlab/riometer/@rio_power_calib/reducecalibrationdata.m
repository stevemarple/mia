function r = reducecalibrationdata(mia)
%REDUCECALIBRATIONDATA  Reduce calibration blocks to single values.
%
%  r = REDUCECALIBRATIONDATA(mia)
%  r: RIO_(RAW)POWER_CALIB object
%  mia: RIO_(RAW)POWER_CALIB object
%
% REDUCECALIBRATIONDATA replaces a contiguous block of calibration samples
% by a single value.
%
% See also GETCALIBRATIONBLOCKDATA.

[c t] = getcalibrationblockdata(mia);
r = mia;
r = setcalibrationdata(r, c, t);
r = setcalibrationduration(r, info(getinstrument(mia), 'bestresolution'));
