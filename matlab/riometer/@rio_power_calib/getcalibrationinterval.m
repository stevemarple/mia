function r = getcalibrationinterval(mia)
%GETCALIBRATIONINTERVAL  Return nominal interval between calibration.
%
%   r = GETCALIBRATIONINTERVAL(mia);
%   r: TIMESPAN
%   mia: RIO_(RAW)POWER_CALIB
%
% See also GETCALIBRATIONINTERVAL.

r = reshape([mia.calibrationinterval], size(mia));

