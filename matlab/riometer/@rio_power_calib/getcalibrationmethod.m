function r = getcalibrationmethod(mia)
%GETCALIBRATIONMETHOD  Return the calibration method used on the data
%
%   r = GETCALIBRATIONMETHOD(mia)
%   r: CHAR describing the interpolation method (see INTERP1)
%   mia: RIO_POWER_CALIB object
%
% Return the interpolation method used by INTERP1 to produce the
% calibration curves. If the data is not calibrated the method is
% 'uncalibrated'.
%
% See also GETCALIBRATION, GETCALIBRATIONCURVES, INTERP1.

if numel(mia) ~= 1
  error('mia must be scalar');
end

r = mia.calibrationmethod;

if ~any(strcmp(r, getcalibrationmethods(mia)))
  r = sprintf('illegal calibration method (''%s'')', mia.calibrationmethod);
end


