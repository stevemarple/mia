function r = trimcalibrationdata(mia)
%TRIMCALIBRATIONDATA  Trim excess calibration data from object.
%
%   r = TRIMCALIBRATIONDATA(mia)
%   r: RIO_(RAW)POWER_CALIB
%   mia: RIO_(RAW)POWER_CALIB
%
% TRIMCALIBRATIONDATA removes excess calibration data (and associated
% timestamps) from RIO_POWER_CALIB or RIO_RAWPOWER_CALIB objects.
%
% See also GETCALIBRATIONDATA.

r = mia;

for n = 1:numel(mia)
  if ~isempty(r(n).calibrationtimestamps)
    st = getstarttime(r(n));
    et = getendtime(r(n));
    
    % let the extent outside the data period be the sum of the (nominal)
    % calibration interval and (nominal) calibration duration
    extent = r(n).calibrationinterval + r(n).calibrationduration;
    % find the timestamps which should be kept
    idx = find(r(n).calibrationtimestamps >= st - extent & ...
               r(n).calibrationtimestamps < et + extent);
    
    r(n).calibrationdata = r(n).calibrationdata(:, idx);
    r(n).calibrationtimestamps = r(n).calibrationtimestamps(idx);  
  end
end


