function r = loadobj(a)
%LOADOBJ  Load filter for RIO_(RAW)POWER_CALIB object.
%

if a.versionnumber == 3
  % Same field structure as v4 but need to load via the constructor call
  % to fix the calibrationmethod field
  a = struct(a);
end

if isstruct(a)  
  % wrong version, call class constructor to convert to object. Problem
  % is, what class should it be? rio_rawpower_calib or rio_power_calib?
  if isfield(a, 'rio_power')
    r = rio_power_calib(a);
  elseif isfield(a, 'rio_rawpower')
    r = rio_rawpower_calib(a);
  else
    a
    fieldnames(a)
    error('unknown parentage for object');
  end
else
  r = a;
end

r = checkcalibrationdata(r);
