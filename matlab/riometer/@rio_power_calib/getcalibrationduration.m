function r = getcalibrationduration(mia)
%GETCALIBRATIONDURATION  Return nominal duration of calibration period.
%
%   r = GETCALIBRATIONDURATION(mia);
%   r: TIMESPAN
%   mia: RIO_(RAW)POWER_CALIB
%
% See also GETCALIBRATIONINTERVAL.

r = reshape([mia.calibrationduration], size(mia));

