function r = getdefaultcalibrationmethod(mia)
%GETDEFAULTCALIBRATIONMETHOD  Return default used method for calibration.
%
%   r = GETDEFAULTCALIBRATIONMETHOD(mia)
%   r: CHAR describing the interpolation method used (see INTERP1)
%   mia: any RIO_POWER_CALIB object
%
% See also GETCALIBRATIONMETHOD.

% r = 'cubic';
rio = getinstrument(mia);
r = info(rio, 'defaultcalibrationmethod');


