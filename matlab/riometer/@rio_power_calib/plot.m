function [fh,gh,ph,ch,cdch] = plot(mia, varargin)

rio = getinstrument(mia);
beams = getbeams(mia);
preferredbeam = info(rio, ...
                     'preferredbeam', 'beams', beams, ...
                     'time', getmidtime(mia));
defaults.beams = [];
defaults.ylim = [];
[defaults unvi] = interceptprop(varargin, defaults);
if isempty(defaults.beams)
  defaults.beams = preferredbeam;
end

% plot, don't use rio_power's limits
[fh gh ph] = plot(getparent(mia), ...
		  'beams', defaults.beams, ...
		  'ylim', [], ...
		  varargin{unvi});

if numel(gh) == 1 & numel(defaults.beams) > 1
  gh_tmp = repmat(gh, size(defaults.beams));
else
  gh_tmp = gh;
end

samt = getsampletime(mia);
ct = getcalibrationtimes(mia);
xx = [getstarttime(mia) getendtime(mia)];
if ~isempty(samt)
  xx(1) = min(xx(1), samt(1));
  xx(2) = max(xx(2), samt(end));
end
if ~isempty(ct)
  xx(1) = min(xx(1), ct(1));
  xx(2) = max(xx(2), ct(end));
end

ss = info(rio, 'calibrationsignalstrength', ...
          'beams', defaults.beams, ...
          'units', getunits(mia), ...
          'time', getstarttime(mia));

for n = 1:numel(defaults.beams)
  h = plot(xx, [ss(n) ss(n)], ...
           'LineStyle', '--', ...
           'Color', [.5 .5 .5], ...
           'Parent', gh_tmp(n));
end


samt_e = getcdfepochvalue(samt);
calmethod = getcalibrationmethod(mia);
if ~strcmp(calmethod, 'uncalibrated') & hascalibrationdata(mia)
  % For convenience get a version of mia cut down for just the requested
  % beams
  mia2 = extract(mia, 'beams', defaults.beams);
  cdc = getcalibrationdatacurves(mia2, ...
                                 'method', calmethod, ...
                                 'sampletime', samt);
  cdch = zeros(numel(defaults.beams), 1);
  for n = 1:numel(defaults.beams)
    cdch(n) = plot(samt_e, cdc(n, :), ...
                   'Parent', gh_tmp(n), ...
                   'Color', 'r', ...
                   'LineStyle', '-', ...
                   'Marker', 'none');
  end
  clear cdc samt samt_e;
  caldata_linestyle = 'none';
else
  caldata_linestyle = '-';
end

% xdata = getcdfepochvalue(ct);
if isempty(ct)
  % no calibration data
  ch = [];
  cdch = [];
  return
end

bi = getparameterindex(mia, defaults.beams);

if getcommoncalibrationdata(mia)
  % Common calibration data, plot on each plot axes
  ch = zeros(numel(gh), 1);
  for n = 1:numel(gh)
    ydata = getcalibrationdata(mia, bi(n), ':');
    ch(n) = plot(ct, ydata, ...
                 'Parent', gh(n), ...
                 'Color', 'r', ...
                 'LineStyle', caldata_linestyle, ...
                 'Marker', 'x');
  end
else
  % calibration data different for each beam, plot for every beam on
  % appropriate plot axes.
  ch = zeros(numel(defaults.beams), 1);
  for n = 1:numel(defaults.beams)
    ydata = getcalibrationdata(mia, bi(n), ':');
    
    ch(n) = plot(ct, ydata, ...
                 'Parent', gh_tmp(n), ...
                 'Color', 'r', ...
                 'LineStyle', caldata_linestyle, ...
                 'Marker', 'x');
  end
end



if isempty(defaults.ylim)
  defaults.ylim = info(rio, 'limits', mia);
end

if any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  for n = 1: prod(size(gh))
    curLim = get(gh(n), 'YLim');
    curLim(notNans) = defaults.ylim(notNans);
    set(gh(n), 'YLim', curLim);
  end
elseif ~isempty(defaults.ylim)
  set(gh, 'YLim', defaults.ylim);
end
