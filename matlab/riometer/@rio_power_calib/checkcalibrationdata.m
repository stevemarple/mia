function [r,data,common] = checkcalibrationdata(mia, varargin)
%CHECKCALIBRATIONDATA Private function.
%
%   [r data common] = checkcalibrationdata(mia)
%   [r data common] = checkcalibrationdata(mia, calibdata)
%   r: revised RIO_(RAW)POWER_CALIB object
%   data: calibration data
%   common: LOGICAL flag indicating if calibration data common to all beams
%   mia: RIO_(RAW)POWER_CALIB object
%   calibdata: optional matrix to override objects own calibration data
%
% This function should be considered private and used only by
% RIO_(RAW)POWER_CALIB.

switch numel(varargin)
 case 0
  data = mia.calibrationdata;
  
 case 1
  data = varargin{1};
  
 otherwise
  error('incorrect parameters');
end

r = mia;

% Removed for Maitri
% if isempty(data)
%   common = true;
% else
%   common = true;
%   for m = 2:size(data, 1)
%     if ~isequalwithequalnans(data(1, :), data(m, :))
%       common = false;
%       break
%     end
%   end
%   if common
%     data = data(1, :);
%   end
% end

r.calibrationdata = data;
% r.commoncalibrationdata = common;
common = r.commoncalibrationdata;

if isempty(data)
  if r.commoncalibrationdata
    r.calibrationdata = zeros(1, 0);
    r.calibrationtimestamps = repmat(timestamp, 1, 0);
  else
    beams = getbeams(mia);
    r.calibrationdata = zeros(numel(beams), 0);
    r.calibrationtimestamps = repmat(timestamp, numel(beams), 0);
  end
end

