function r = setcommoncalibrationdata(mia, ccd)
%SETCOMMONCALIBRATIONDATA Indicate if calibration data common for all beams.
%
%   r = SETCOMMONCALIBRATIONDATA(mia, ccd)
%   r: modified RIO_(RAW)POWER_CALIB object
%   mia: RIO_(RAW)POWER_CALIB object
%   ccd: common calibration data flag (LOGICAL)
%
% See also RIO_POWER_CALIB, RIO_RAWPOWER_CALIB, GETCOMMONCALIBRATIONDATA.

r = mia;
r.commoncalibrationdata = logical(ccd(1));

