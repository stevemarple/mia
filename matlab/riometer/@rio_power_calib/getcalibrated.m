function r = getcalibrated(mia)
%GETCALIBRATED  Indicate if data is calibrated or not.
%
%   r = GETCALIBRATED(mia)
%   r: LOGICAL
%   mia: RIO_POWER_CALIB
%   r is the same size as mia
%
% See also RIO_POWER_CALIB.

r = logical(zeros(size(mia)));

for n = 1:numel(mia)
  r(n) = ~isempty(mia(n).calibrationmethod);
end
