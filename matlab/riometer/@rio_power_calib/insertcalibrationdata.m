function r = insertcalibrationdata(a, b)
%INSERTCALIBRATIONDATA Insert calibration data and times.
%
%   r = INSERTCALIBRATIONDATA(a, b);
%   r: RIO_(RAW)POWER_CALIB
%   a: RIO_(RAW)POWER_CALIB
%   b: RIO_(RAW)POWER_CALIB
%
% Insert calibration data from b into a. Only calibration data for beams
% which are in a and b are inserted in b.
%
% Note that EXTRACT will remove extraneous calibration data.
%
% See also INSERT, EXTRACT.

if isempty(b.calibrationtimestamps)
  r = a;
  return; % no calibration values to insert
end

% IMPORTANT: have to insert the calibration data into uncalibrated objects,
% otherwise the new calibration data will produce different calibration
% curves and the data cannot be correctly converted to its uncalibrated
% state. Therefore uncalibrate, insertcalibrationdata then recalibrate.
calmethod_orig = getcalibrationmethod(a);
a = calibrate(a, 'uncalibrated');

r = a;


% To use insertcaldatahelper we must calculate the subsasgn and subsref
% structs. We can get insertcheck to do this for us, however, we need to
% ignore any beams in b which are not in a.
beams_a = getbeams(a);
beams_b = getbeams(b);
if ~isequal(beams_a, beams_b)
  common_beams = intersect(beams_a, beams_b);
  if isempty(common_beams)
    return; % no common calibration data to insert
  end
  b = extract(b, 'beams', common_beams);
end

[aa, ar, ba, br, rs] = insertcheck(a, b);

[r.calibrationdata r.calibrationtimestamps r.commoncalibrationdata] ...
    = insertcaldatahelper(a, b, aa, ar, ba, br, rs);

r.calibrationinterval = max(a.calibrationinterval, ...
			    b.calibrationinterval);

r.calibrationduration = max(a.calibrationduration, ...
			    b.calibrationduration);


% revert to the original calibration method used for 'a'
r = calibrate(r, calmethod_orig);

