function r = gettype(mia, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_POWER_CALIB, RIO_POWER, RIOMETER.


if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

s = gettype(getparent(mia), mode);
switch mode
 case 'l'
  c = ' (with calibration data)';
  
 case 'u'
  c = ' (WITH CALIBRATION DATA)';
  
 case 'c'
  c = ' (with calibration data)';
  
 case 'C'
  % don't capitalise first letters of calibration comments
  c = ' (with calibration data)';
  
 otherwise
  error('unknown mode');
end

r = [s c];



