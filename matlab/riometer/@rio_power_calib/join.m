function [r, reversed] = join(a, b)
%See mia_base/JOIN.


if ~strcmp(a.calibrationmethod, b.calibrationmethod)
  error('cannot join objects, calibration method differs');
end

[r reversed] = mia_base_join(a, b, logical(1));


if reversed
  tmp = a;
  a = b;
  b = tmp;
end

r.calibrationmethod = a.calibrationmethod;
r.calibrationtimestamps = [a.calibrationtimestamps b.calibrationtimestamps];

if a.commoncalibrationdata == b.commoncalibrationdata
  r.commoncalibrationdata = a.commoncalibrationdata;
  r.calibrationdata = [a.calibrationdata b.calibrationdata];
else
  % one has common calibration data, the other doesn't. Get the
  % calibration data replicated per beam. Then join together.
  r.commoncalibrationdata = false;
  r.calibrationdata = [getcalibrationdata(a) getcalibrationdata(b)];
end




  

