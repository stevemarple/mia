function [r,t] = getcalibrationblockdata(mia)
%GETCALIBRATIONBLOCKDATA Get the calibration data aggregated for each block.
%
%   [r t] = GETCALIBRATIONBLOCKDATA(mia)
%   r: aggregated calibration data (DOUBLE)
%   t: timestamps for the aggregated data (TIMESTAMP)
%
% GETCALIBRATIONBLOCKDATA identifies contiguous calibration samples. Each
% block of samples is aggregated to a single value (using NONANMEAN of the
% samples which fall within the valid calibration window). A timestamp for
% the aggregated samples is computed. Note that if the calibration data
% is common to all beams the data will have just one row, otherwise the
% number of rows will match the number of beams.
%
% See also GETCOMMONCALIBRATIONDATA, RIO_POWER_CALIB, RIO_RAWPOWER_CALIB.

rio = getinstrument(mia);
bestres = info(rio, 'bestresolution');
calibrationwindow = info(rio, 'calibrationwindow');

% Need to know when each set calibration interval starts. Each ends when the
% next starts, so for convenience add a fake interval immediately the last
% ends
calib_blocks = [1 ...
		find(diff(mia.calibrationtimestamps) > bestres) ...
		numel(mia.calibrationtimestamps)+1];

t = repmat(timestamp, 1, numel(calib_blocks) - 1);
r = zeros(size(mia.calibrationdata, 1), numel(t));

% For each calibration block take the nonanmedian calibration value
bad_blocks = [];
for n = 1:length(t)
  idx = calib_blocks(n):(calib_blocks(n+1)-1);
  IDX_OLD = idx;
  % apply the calibration window (but only to blocks of sufficient size)
  if isfinite(calibrationwindow(2)) & (calibrationwindow(2) > length(idx))
    idx((calibrationwindow(2)+1):end) = []; % delete from end
  end
  
  if isfinite(calibrationwindow(1)) & (calibrationwindow(1) <= length(idx))
    % idx(1:(calibrationwindow(1)-1)) = []; % delete from start
    idx(1:min((calibrationwindow(1)-1), end)) = []; % delete from start
  end
  
  % disp([printseries(IDX_OLD) '    ' printseries(idx)]);
  
  switch numel(idx)
   case 0
    % After deleting samples which fell outside of the calibration window
    % no samples were left. Store nans and fix later.
    r(:, n) = nan;
    t(n) = timestamp('bad');
    bad_blocks(end+1) = n;
   
   case 1
    r(:, n) = mia.calibrationdata(:, idx);
    t(n) = mia.calibrationtimestamps(idx);
    
   otherwise
    r(:, n) = nonanmedian(mia.calibrationdata(:, idx), 2);
    t(n) = mean(mia.calibrationtimestamps(idx));
  end
end

% Remove data from the blocks where no valid was left.
t(bad_blocks) = [];
r(:, bad_blocks) = [];



