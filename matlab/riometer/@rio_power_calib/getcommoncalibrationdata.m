function r = getcommoncalibrationdata(mia)
%GETCOMMONCALIBRATIONDATA Indicate if calibration data common for all beams.
%
%   r = GETCOMMONCALIBRATIONDATA(mia)
%   r: flag indicating calibration data shared between all beams (LOGICAL)
%   mia: RIO_(RAW)POWER_CALIB
%
% See also RIO_POWER_CALIB, RIO_RAWPOWER_CALIB.

r = repmat(true, size(mia));
for n = 1:numel(mia)
  tmp = mia(n);
  r(n) = tmp.commoncalibrationdata;
end
