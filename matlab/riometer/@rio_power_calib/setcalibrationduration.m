function r = setcalibrationduration(mia, t)
%SETCALIBRATIONDURATION  Set calibration duration.
%
%   r = SETCALIBRATIONDURATION(mia, t)
%   r: modified RIO_POWER_CALIB object(s)
%   mia: RIO_POWER_CALIB object(s)
%   t: calibration duration TIMESTAMP(s)
%
% See also GETCALIBRATIONDURATION.


if ~isa(t, 'timespan')
  error('calibration duration not a timespan');
end

if ~isequal(size(mia), size(t))
  error('mia and t must be the same size');
end

r = mia;
for n = 1:numel(r)
  tmp = r(n);
  tmp.calibrationduration = t(n);
  r(n) = tmp;
end

