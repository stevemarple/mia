function [r, method, samt]  = getcalibrationdatacurves(mia, varargin)
%GETCALIBRATIONDATACURVES
%
%   [r m] = GETCALIBRATIONDATACURVES(mia)
%   [r m] = GETCALIBRATIONDATACURVES(mia, ...)
%
%   r: DOUBLE, same size as GETDATASIZE(mia)
%   m: CHAR method used for interpolation
%   mia: RIO_POWER_CALIB bject (scalar)
%
% GETCALIBRATIONDATACURVES returns the calibration data interpolated to a
% smooth curve for all the sampletimes.  The default behaviour can be
% modified with the following name/value pairs.
%
%   method: CHAR
%   method to use for calibration. Method should be one known to
%   INTERP1, or be 'uncalibrated', in which case no calibration adjustment
%   is made.
%
%   'sampletime': TIMESTAMP
%   The times for which the calibration data curve should be
%   evaluated. Defaults to the sample times of the MIA object (see
%   GETSAMPLETIME).
%
% See also RIO_POWER_CALIB, RIO_RAWPOWER_CALIB.

if numel(mia) ~= 1
  error('object must be scalar');
end

defaults.method = '';
defaults.sampletime = getsampletime(mia);
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.method)
  defaults.method = getdefaultcalibrationmethod(mia);
end

if isempty(mia.calibrationtimestamps) 
  % no calibration data, no adjustment
  disp('no calibration data found');
  method = 'uncalibrated';
  return
elseif any(strcmp(defaults.method, {'none' 'uncalibrated'}))
  % this is used when converting to an uncalibrated rio_power object
  defaults.method = 'uncalibrated';
  % return
end

rio = getinstrument(mia);
st = getstarttime(mia);
et = getendtime(mia);

% Have the calibration data aggregated so that each period of calibration
% results in only one value (per beam).
[cy t] = getcalibrationblockdata(mia);
cx = getcdfepochvalue(t);

% Loop over all rows in cy. Note that it is possible that some of the
% calibration data will be nans. (For ARIES some of the beams are cropped
% and so some beams will have nans where others may not.)

xi = getcdfepochvalue(defaults.sampletime);
st_e = getcdfepochvalue(st);
et_e = getcdfepochvalue(et);
r = repmat(nan, size(cy, 1), numel(defaults.sampletime));

for n = 1:size(cy, 1)
  cx2 = cx;
  cy2 = cy(n, :);
  
  % remove points where cy2 is a nan
  cx2(isnan(cy2)) = [];
  cy2(isnan(cy2)) = [];
  
  switch length(cx2)
   case 0
    % no calibration data, so do nothing
    ;
    
   case 1
    % only one value, apply same to all
    r(n, :) = repmat(cy2, 1, numel(xi));
    return
    
   otherwise
    % if the first calibration value we have is after the first time we are
    % evaluating for then pretend we had the same value just before it
    if cx2(1) > xi(1)
      cx2 = [xi(1)-1 cx2];
      cy2 = [cy2(1) cy2];
    end
    
    % if the last calibration value we have is before the last time we are
    % evaluating for then pretend we had the same value just after it
    if cx2(end) < xi(end)
      cx2(end+1) = xi(end) + 1;
      cy2(end+1) = cy2(end);
    end
    
    r(n, :) = interp1(cx2, cy2, xi, defaults.method);
  end
end
method = defaults.method;
samt = defaults.sampletime;

if getcommoncalibrationdata(mia)
  % replicate the calibration curve for each beam
  r = repmat(r, getdatasize(mia, 1), 1);
end

