function [r, samt, op, undoop] = getcalibrationcurves(mia, varargin)
%GETCALIBRATIONCURVES
%
%   [r m samt] = GETCALIBRATIONCURVES(mia)
%   [r m samt] = GETCALIBRATIONCURVES(mia, ...)
%
%   r: DOUBLE, same size as GETDATASIZE(mia)
%   m: CHAR method used for interpolation
%   samt: sample TIMESTAMPs for the calibration curve data
%   mia: RIO_POWER_CALIB bject (scalar)
%
% GETCALIBRATIONCURVES returns the calibration data interpolated to a
% smooth curve for all the sampletimes.  The default behaviour can be
% modified with the following name/value pairs.
%
%   method: CHAR
%   method to use for calibration. Method should be one known to
%   INTERP1, or be 'uncalibrated', in which case no calibration adjustment
%   is made.
%
%   'sampletime': TIMESTAMP
%   The times for which the calibration data curve should be
%   evaluated. Defaults to the sample times of the MIA object (see
%   GETSAMPLETIME).
%
% See also RIO_POWER_CALIB, RIO_RAWPOWER_CALIB.

if numel(mia) ~= 1
  error('object must be scalar');
end

defaults.method = '';
defaults.sampletime = getsampletime(mia);
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.method)
  error('interpolation method must be set');
elseif strcmp(defaults.method, 'uncalibrated')
  error('cannot get calibration curves for uncalibrated method');
elseif ~any(strcmp(defaults.method, getcalibrationmethods(mia)))
  error(sprintf('unknown calibration method (was ''%s'')', ...
                defaults.method));
elseif isempty(mia.calibrationtimestamps) 
  error('cannot get calibration curves when calibration data is missing');
end

samt = defaults.sampletime;
dsz = getdatasize(mia);
dsz(end) = numel(defaults.sampletime);

units = getunits(mia);
% The operation used to apply calibration depends if we are working in
% the log domain (dBm units) or not (raw units)
switch units
 case ''
  error('units must be defined');
  
 case 'raw'
  op = 'times';
  undoop = 'rdivide';
  null_calib_value = 1;
    
 case 'dBm'
  op = 'plus';
  undoop = 'minus';
  null_calib_value = 0;
  
 otherwise
  error(sprintf('unknown units (was ''%s'')', units));
end

rio = getinstrument(mia);
st = getstarttime(mia);
et = getendtime(mia);
beams = getbeams(mia);

% find out which beam(s) used for calibration
commoncalibrationdata = getcommoncalibrationdata(mia);
if commoncalibrationdata
  calibbeams = info(rio, 'calibrationbeams');
else
  calibbeams = beams;
end
calibsig = info(rio, 'calibrationsignalstrength', ...
		'time', st, ...
		'beams', calibbeams, ...
		'units', units);

if commoncalibrationdata
  calibsig = mean(calibsig);  % aggregate to one value
  % repeat so that each beam has own value
  calibsig = repmat(calibsig, numel(beams), 1);
end

% Have the calibration data aggregated so that each period of calibration
% results in only one value (per beam).
[cy t] = getcalibrationblockdata(mia);
cx = getcdfepochvalue(t);

% Loop over all rows in cy. Note that it is possible that some of the
% calibration data will be nans. (For ARIES some of the beams are cropped
% and so some beams will have nans where others may not.)

xi = getcdfepochvalue(defaults.sampletime);
st_e = getcdfepochvalue(st);
et_e = getcdfepochvalue(et);
r = repmat(nan, size(cy, 1), numel(defaults.sampletime));

for n = 1:size(cy, 1)
  cx2 = cx;
  cy2 = cy(n, :);
  
  % remove points where cy2 is a nan
  cx2(isnan(cy2)) = [];
  cy2(isnan(cy2)) = [];
  
  switch length(cx2)
   case 0
    % no calibration data, so do nothing
    ;
    
   case 1
    % only one value, apply same to all
    r(n, :) = repmat(cy2, 1, numel(xi));
    
   otherwise
    % if the first calibration value we have is after the first time we are
    % evaluating for then pretend we had the same value just before it
    if cx2(1) > xi(1)
      cx2 = [xi(1)-1 cx2];
      cy2 = [cy2(1) cy2];
    end
    
    % if the last calibration value we have is before the last time we are
    % evaluating for then pretend we had the same value just after it
    if cx2(end) < xi(end)
      cx2(end+1) = xi(end) + 1;
      cy2(end+1) = cy2(end);
    end
    
    r(n, :) = interp1(cx2, cy2, xi, defaults.method);
  end
end

if commoncalibrationdata
  % replicate the calibration curve for each beam
  r = repmat(r, getdatasize(mia, 1), 1);
end

% Scale r relative to the desired calibration signal. Remember to use the
% operation appropriate to the data units
dsz2 = dsz;
dsz2(1) = 1;
r = feval(undoop, repmat(calibsig, dsz2), r);
