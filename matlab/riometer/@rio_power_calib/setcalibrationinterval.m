function r = setcalibrationinterval(mia, t)
%SETCALIBRATIONINTERVAL  Set calibration interval.
%
%   r = SETCALIBRATIONINTERVAL(mia, t)
%   r: modified RIO_POWER_CALIB object(s)
%   mia: RIO_POWER_CALIB object(s)
%   t: calibration interval TIMESTAMP(s)
%
% See also GETCALIBRATIONINTERVAL.


if ~isa(t, 'timespan')
  error('calibration interval not a timespan');
end

if ~isequal(size(mia), size(t))
  error('mia and t must be the same size');
end

r = mia;
for n = 1:numel(r)
  tmp = r(n);
  tmp.calibrationinterval = t(n);
  r(n) = tmp;
end

