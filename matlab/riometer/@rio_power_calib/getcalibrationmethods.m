function r = getcalibrationmethods(mia)
%GETCALIBRATIONMETHODS  Return the list of possible calibration methods.
%
%   r = GETCALIBRATIONMETHODS(mia)
%   r: CELL array of CHARs
%   mia: any RIO_(RAW)POWER_CALIB object
%
% GETCALIBRATIONMETHODS returns the list of valid calibration methods
% which can be chosen.
%
% See also GETCALIBRATIONMETHOD, GETDEFAULTCALIBRATIONMETHOD, CALIBRATE.

r = {'uncalibrated', 'nearest', 'linear', 'spline', 'pchip', 'cubic'};

