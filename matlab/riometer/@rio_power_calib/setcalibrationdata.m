function r = setcalibrationdata(mia, data, t)
%SETCALIBRATIONDATA  Set calibration data/timestamps
%
%   r = SETCALIBRATIONDATA(mia, data, t)
%   r: modified RIO_POWER_CALIB object
%   mia: RIO_POWER_CALIB object (scalar)
%   data: calibration data
%   t: TIMESTAMPs for calibration data
%
% See also rio_power_calib/CALIBRATE, RIO_POWER_CALIB.

if numel(mia) ~= 1
  error('mia must be scalar');
end

% if size(data, 1) ~= numel(getbeams(mia))
%   error('number of rows of calibration data must match number of beams');
% end

if size(data, 2) ~= numel(t)
  error('number of calibration samples must match number of timestamps');
elseif ~isa(t, 'timestamp')
  error('calibration timestamps are not timestamps');
end


r = mia;
r.calibrationdata = data;
r.calibrationtimestamps = t;

r = checkcalibrationdata(r);
