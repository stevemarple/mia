function  [r,sr,s] = extract(mia, varargin)
%EXTRACT EXTRACT overloaded for RIO_POWER_CALIB object.
%
% See mia_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end


% leave all intelligence to rio_base
[r sr s] = rio_base_extract(mia, varargin{:});

if strcmp(sr.subs{end}, ':')
  ; % extract all sampletimes, don't trim
elseif isequal(sr.subs{end}, 1:getdatasize(mia, 'end'))
  ; % extract all sampletimes, don't trim
else
  % trim the calibration data and timestamps according to new start/end times
  r = trimcalibrationdata(r);
end


% Trim the calibration data if number of beams has changed. Don't attempt
% to alter if there are no calibration timestamps or the calibration data
% is common for all beams
if ~isempty(r.calibrationtimestamps) & ~strcmp(sr.subs(1), ':') ...
      & ~r.commoncalibrationdata
  % only extract calibrationdata on the beams part
  sr2 = sr;
  sr2.subs(2:end)= {':'};
  r.calibrationdata = feval(s.subsref, r.calibrationdata, sr2);
end
