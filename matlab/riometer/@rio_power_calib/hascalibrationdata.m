function r = hascalibrationdata(mia)
%HASCALIBRATIONDATA  Indicate if object contains calibration data
%
%   r = HASCALIBRATIONDATA(mia);
%   r: LOGICAL
%   mia = RIO_(RAW)POWER_CALIB object
%
% HASCALIBRATIONDATA indicates if the object(s) passed contain
% calibration data. 
%
% See also RIO_POWER_CALIB, RIO_RAWPOWER_CALIB.

r = logical(zeros(size(mia)));

for n = 1:numel(mia);
  m = mia(n);
  r(n) = ~isempty(m.calibrationdata);
end
