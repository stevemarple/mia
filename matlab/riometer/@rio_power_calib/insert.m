function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  Insert RIO_POWER_CALIB object into another RIO_POWER_CALIB object.

% let this function be used for both insert and nonaninsert
switch basename(mfilename);
 case {'insert' 'rio_power_insert' 'rio_rawpower_insert'}
  action = 'insert';

 case {'nonaninsert' 'rio_power_nonaninsert' 'rio_rawpower_nonaninsert'}
  action = 'nonaninsert';
 otherwise
  error('do not know what type of insert to perform');
end


calmethod = getcalibrationmethod(a);
if ~strcmp(getcalibrationmethod(b), calmethod)
  b = calibrate(b, calmethod);
end

% call appropriate (nonan)insert function
[r,aa,ar,ba,br,rs] = feval(['rio_base_' action], a, b);

r.calibrationmethod = calmethod;

[r.calibrationdata r.calibrationtimestamps r.commoncalibrationdata] ...
    = insertcaldatahelper(a, b, aa, ar, ba, br, rs);

r.calibrationinterval = max(a.calibrationinterval, ...
			    b.calibrationinterval);

r.calibrationduration = max(a.calibrationduration, ...
			    b.calibrationduration);
