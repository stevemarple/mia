function [data,t,common] = insertcaldatahelper(a, b, aa, ar, ba, br, rs)
%INSERTCALDATAHELPER Helper function for inserting calibration data.
%
% INSERTCALDATAHELPER is intended as a helpder function for INSERT and
% INSERTCALIBRATIONDATA; it is not intended to be called by user
% programs.
%
%   [data, t] = INSERTCALDATAHELPER(a, b, aa, ar, ba, br, rs);
%
%   data: modified calibration data
%   t: modified calibration TIMESTAMPs
%   a: RIO_(RAW)POWER_CALIB
%   b: RIO_(RAW)POWER_CALIB
%   aa: SUBSASGN structure for a
%   ar: SUBSREF structure for a
%   ba: SUBSASGN structure for b
%   br: SUBSREF structure for b
%   rs: STRUCT containing details about return object
%
% aa, ar, ba, br and rs are identical to the vairables returned by
% INSERTCHECK.
%
% See also INSERTCHECK.
[t idx] = ...
    unique([a.calibrationtimestamps(:); b.calibrationtimestamps(:)]');

if isempty(t)
  % unique doesn't preserve the dimensions for empty matrices, but this
  % is a bug in the builtin unique function! Fix here for our purposes
  % instead of altering the timestamp/unqiue behaviour from the builtin
  % unique behaviour.
  t = repmat(timestamp, [1 0]);
end

aa_tmp = aa;
aa_tmp.subs{2} = ':';
ar_tmp = ar;
ar_tmp.subs{2} = ':';

ba_tmp = ba;
ba_tmp.subs{2} = ':';
br_tmp = br;
br_tmp.subs{2} = ':';

if a.commoncalibrationdata & b.commoncalibrationdata
  % both datasets use common calibration data, so do the same with the
  % result
  a_calibrationdata = a.calibrationdata;
  b_calibrationdata = b.calibrationdata;
  common = true;

  cal_data_a = repmat(nan, 1, size(a_calibrationdata, 2));
  cal_data_b = repmat(nan, 1, size(b_calibrationdata, 2));

  aa_tmp.subs{1} = 1;
  ar_tmp.subs{1} = 1;
  ba_tmp.subs{1} = 1;
  ar_tmp.subs{1} = 1;
  
else
  % one or both don't use common calibration data so ensure the
  % calibration data is replicated to match the number of beams
  a_calibrationdata = getcalibrationdata(a);
  b_calibrationdata = getcalibrationdata(b);
  common = false;
  
  
  % need to remap A's calibration data into different rows, to match the
  % final object. Don't remap columns just yet. Do the same for B.
  cal_data_a = repmat(nan, rs.datasize(1), size(a_calibrationdata, 2));
  cal_data_b = repmat(nan, rs.datasize(1), size(b_calibrationdata, 2));
end


% copy calibration data from a into a temporary array. Use feval to
% select between subsref/subsref2 and subsasgn/subsag2 for matlab 5.1
% compatibility
cal_data_a = feval(rs.subsasgn, cal_data_a, aa_tmp, ...
		   feval(rs.subsref, a_calibrationdata, ar_tmp));
cal_data_b = feval(rs.subsasgn, cal_data_b, ba_tmp, ...
		   feval(rs.subsref, b_calibrationdata, br_tmp));


% Splice cal_data_a and cal_data_b together in the same way as was done
% for the calibration timestamps. idx gives us the columns we need to
% combine in order to get the data which matches the selected unique
% timestamps
data = [cal_data_a cal_data_b];
data = data(:, idx);


