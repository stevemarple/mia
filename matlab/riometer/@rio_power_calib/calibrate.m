function [r, method] = calibrate(mia, varargin)
%CALIBRATE  CALIBRATE A RIO_POWER_CALIB object.
%
%   [r, meth] = CALIBRATE(mia)
%   [r, meth] = CALIBRATE(mia, method)
%
%   r: calibrated RIO_POWER_CALIB object(s)
%   meth: actual method used to generate the calibration curves
%   mia: RIO_POWER_CALIB object(s)
%   method: CHAR, optional method used when interpolating between
%   calibration data points. See GETCALIBRATIONCURVES. Empty string uses
%   the default method, see GETDEFAULTCALIBRATIONMETHOD.

if numel(mia) ~= 1
  % recursively convert all objects, one-by-one
  r = repmat(rio_power, size(mia));
  for n = 1:numel(mia)
    r(n) = feval(mfilename, mia(n), varargin{:});
  end
  return
end

% mia is scalar from here on

method = '';
if length(varargin) >= 1
  method = varargin{1};
end
if isempty(method)
  method = getdefaultcalibrationmethod(mia);
end

if ~any(strcmp(method, getcalibrationmethods(mia)))
  error(sprintf('unknown calibration method (was ''%s'')', method));
end

r = mia;
current_method = getcalibrationmethod(mia);

if strcmp(method, current_method)
  % already calibrated with the method required, no need to apply any
  % calibration adjustments
  return
end

if prod(getdatasize(mia)) == 0
  % no data, so can claim to have calibrated to whatever we want
  r = local_set_method_and_dataquality(r, method);
  return
end

if isempty(mia.calibrationtimestamps)
  % if the calibration data is missing we have to assume the data is
  % perfect, therefore no adjustment is actually needed
  warning('calibration data is missing');
  r = local_set_method_and_dataquality(r, method);
  return
end

if ~strcmp(current_method, 'uncalibrated')
  % already calibrated, but with a different method, undo that
  % calibration
  [cc samt op undoop] = ...
      getcalibrationcurves(mia, 'method', current_method);
  d = getdata(r);
  r = setdata(r, feval(undoop, d, cc)); % remove calibration!
end

if ~strcmp(method, 'uncalibrated')
  % apply new calibration method
  [cc samt op undoop] = ...
      getcalibrationcurves(mia, 'method', method);
  d = getdata(r);
  r = setdata(r, feval(op, d, cc)); % apply new calibration
end

r = local_set_method_and_dataquality(r, method);

function r = local_set_method_and_dataquality(mia, method)
r = mia;
r.calibrationmethod = method;

if strcmp(method, 'uncalibrated')
  r = adddataquality(r, 'Uncalibrated');
else
  r = cleardataquality(r, 'Uncalibrated');
end
