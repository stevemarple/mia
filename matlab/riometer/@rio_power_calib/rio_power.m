function r = rio_power(mia, varargin)
%RIO_POWER  Convert RIO_POWER_CALIB object to RIO_POWER.
%
%   r = RIO_POWER(mia)
%   r = RIO_POWER(mia, method)
%
%   r: calibrated RIO_POWER object
%   mia: RIO_POWER_CALIB object(s)
%   method: CHAR, optional method used when interpolataing between
%   calibration data points. See GETCALIBRATIONCURVES.

mfname = basename(mfilename);
pc = getparentclass(mia);
if ~strcmp(pc, mfname)
  if strcmp(pc, 'rio_rawpower') & strcmp(mfname, 'rio_power')
    % do conversion in two stages
    r = rio_power(rio_rawpower(mia));
    return
  end
  
  error(sprintf('cannot convert %s to %s', class(mia), mfname));
end

if numel(mia) ~= 1
  % recursively convert all objects, one-by-one
  r = repmat(feval(getparentclass(mia)), size(mia));
  for n = 1:numel(mia)
    r(n) = feval(mfilename, mia(n), varargin{:});
  end
  return
end

% mia is scalar from here on

% ensure that the data is calibrated
[mia_calib method] = calibrate(mia, varargin{:});

r = getparent(mia_calib);
% if isa(mia, 'rio_power_calib')
%   r = mia_calib.rio_power;
% elseif isa(mia, 'rio_rawpower_calib')
%   r = mia_calib.rio_rawpower;
% else
%    error(sprintf('unknown parentage for object of class %s', class(mia)));
% end

if ~strcmp(method, 'uncalibrated')
  r = addprocessing(r, ['Calibrated using interp method ' method]);
end

