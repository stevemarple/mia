function mia = rio_power_calib(varargin)
%RIO_POWER_CALIB  Constructor for RIO_POWER_CALIB class.
%

cls = basename(mfilename);

% Get parent class. Cannot call the useful class method since we need an
% object of the class to do that, and at this point we cannot construct an
% object without knowing the parent class! 
switch cls
  case 'rio_power_calib'
   parent_cls = 'rio_power';
  
 case 'rio_rawpower_calib'
   parent_cls = 'rio_rawpower';
   
 otherwise
  error(sprintf('unknown parentage for object of class %s', cls));
end

% Make it easy to change the class definition at a later date. Remember to
% check whether any modifications are needed to LOADOBJ
latestversion = 4;
mia.versionnumber = latestversion;

% method used for interpolating between calibration points. In versions <
% 4 empty meant uncalibrated, but now it must always be set
mia.calibrationmethod = 'uncalibrated';

% data calibration values, one row per beam or one row only if
% commoncalibrationdata, number of columns should match number of
% calibration timestamps
mia.calibrationdata = zeros(1, 0);

% times when the calibration values were recorded. First dimension should
% always be equal to 1, second dimension gives the number of calibration
% timestamps.
mia.calibrationtimestamps = repmat(timestamp, 1, 0);

% record how often and how long calibration signals are, in version >= 2
mia.calibrationinterval = timespan('bad');
mia.calibrationduration = timespan('bad');
mia.commoncalibrationdata = true;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  mia = class(mia, cls, feval(parent_cls));
  
elseif nargin == 1 & isa(varargin{1}, cls)
  % called function names matches the type passed so this is a copy
  % constructor or a converter to base class    
  mia = varargin{1};

elseif nargin == 1 & strcmp(cls, 'rio_power_calib') ...
      & isa(varargin{1}, 'rio_rawpower_calib')
  % function called as 'rio_power_calib' on an object of type
  % rio_rawpower_calib, so convert from raw power to power.
  
  disp('rio_power_calib(rio_rawpower_calib) not fully tested');
  s = struct(varargin{1});
  % remove the parent class field
  s = rmfield(s, 'rio_rawpower');
  
  % get parent object
  parent = getparent(varargin{1});
  
  % convert parent to rio_power and set as field in s
  s = setfield(s, parent_cls, feval(parent_cls, parent));
  
  % Convert the calibration data. Do so by faking a rio_rawpower which
  % contains the calibration data. Then convert to rio_power and extract the
  % data.
  cal = rio_rawpower('instrument', getinstrument(parent), ...
                     'data', s.calibrationdata, ...
                     'units', getunits(varargin{1}));
  s.calibrationdata = getdata(rio_power(cal));


  % Ensure that the returned object is marked with the latest version
  % number
  s.versionnumber = latestversion;

  % Construct object from the data in the struct and the parent object
  mia = feval(cls, s);
    
  % Ensure that the commoncalibrationdata flag is set correctly
  mia = checkcalibrationdata(mia);
  return
  
elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)

  a = varargin{1};
  requiredFields = {'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end
  
  fn = intersect(fieldnames(a), fieldnames(mia));
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent_cls));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    an = a(n);
    for m = 1:length(fn)
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end
    
    base = getfield(an, parent_cls);
    if tmp.versionnumber < 2
      % no calibrationinterval or calibrationduration. The only data
      % which use rio_(raw)power_calib version 1 did so with 1h/10s times
      tmp.calibrationinterval = timespan(1, 'h');
      tmp.calibrationduration = timespan(10, 's');
    end
    
    if tmp.versionnumber < 3
      % introduced the commoncalibrationdata flag. Check if all rows of
      % calibrationdata are the same
      [q tmp.calibrationdata tmp.commoncalibrationdata] = ...
          checkcalibrationdata(feval(cls), tmp.calibrationdata);
    end

    if tmp.versionnumber < 4
      % v4 required that the calibrationmethod always be set, either to
      % 'uncalibrated', or to one of the method returned by
      % GETCALIBRATIONMETHODS.
      valid_methods = getcalibrationmethods(rio_power_calib);
      switch tmp.calibrationmethod
       case {'' 'none'}
        tmp.calibrationmethod = 'uncalibrated';
        
       case valid_methods
        ; % no need to do anything
        
       otherwise
        error(sprintf('unknown calibration method (was ''%s'')', ...
                      tmp.calibrationmethod));
      end
    end
    
    if tmp.versionnumber > latestversion
      error('unknown version');
    end

    tmp.versionnumber = latestversion;
    mia(n) = class(tmp, cls, base);
  end

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  parent = feval(parent_cls, varargin{unvi}, 'load', 0);
  mia = class(mia, cls, parent);

  if isempty(defaults.load) 
    defaults.load = isdataempty(mia);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;

% Ensure that the commoncalibrationdata flag is set correctly
mia = checkcalibrationdata(mia);
