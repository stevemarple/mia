function r = rio_aari
%RIO_AARI  Return a list of the AARI instruments.
%
%   r = RIO_AARI
%   r: vector of RIOMETER objects
%
%   For more details about AARI riometers see
%   http://www.aari.nw.ru/clgmi/geophys/station.htm
%
%   See also RIOMETER.

% Artic, then Antarctic.
r = [rio_capechelyuskin rio_heissisland rio_viezeisland ...
     rio_vos_1 rio_mir_1];
