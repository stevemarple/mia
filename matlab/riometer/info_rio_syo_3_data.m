function r = info_rio_syo_3_data
%INFO_RIO_SYO_3_DATA Return basic information about rio_syo_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_syo_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=syo;serialnumber=3

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'syo';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'AGONET';
r.facility_url = 'http://iacg.org/iacg/ground_stations/agonet.html';
r.facilityid = 3;
r.groupids = [];
r.iantennatype = '';
r.ibeams = 49;
r.id = 139;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [];
r.latitude = [];
r.location1 = 'Syowa';
r.location1_ascii = '';
r.location2 = 'Antartica';
r.logo = '';
r.logurl = '';
r.longitude = [];
r.modified = timestamp([2011 07 29 12 20 57.218054]);
r.name = '';
r.piid = 14;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 3;
r.standardobliquity = [];
r.starttime = timestamp([2004 02 01 00 00 00]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'National Institute for Polar Research';
% end of function
