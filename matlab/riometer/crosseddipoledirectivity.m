function r = crosseddipoledirectivity(theta, phi, varargin)
%CROSSEDDIPOLEDIRECTIVITY Calculate the directivity of crossed-dipole antenna.
%
%   r = CROSSEDDIPOLEDIRECTIVITY(theta, phi, ...);
%   r: complex directivity
%   theta: zenith angle (radians)
%   phi:   azimuth angle (radians)
%   
%   If theta and phi are matrices then they must be the same size.
%
%   See also DIRECTIVITY, TURNSTILEDIRECTIVITY.

defaults.height = 0.25; % height, as fraction of wavelength
[defaults unvi] = interceptprop(varargin, defaults);

r = 2 * sin(defaults.height * 2 * pi * sin(pi/2 - theta));
