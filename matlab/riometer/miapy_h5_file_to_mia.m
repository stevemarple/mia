function mia = miapy_h5_file_to_mia(filename, varargin)

defaults.mia = [];

defaults.starttime = [];

defaults.archive = '';
defaults.cancelhandle = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object must be set');
end
cls = class(defaults.mia);
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('mia object must have instrument set');
end

rio_code = getcode(rio);
systemtype = info(rio, 'systemtype');

bestres = info(rio, 'bestresolution');

beams = getbeams(defaults.mia);
if isempty(beams)
  beams = info(rio, 'allbeams');
end

[imagingbeams widebeams] = info(rio, 'beams'); % Values for the instrument
ibeams = intersect(beams, imagingbeams); % Request imaging beams
wbeams = intersect(beams, widebeams); % Request wide beams

samt_data = h5read(filename, '/data/sample_start')';
samt_units = h5readatt(filename, '/data/sample_start', 'units');
samt_units = samt_units{1};
samt = timestamp([1970 01 01 00 00 00]) + ...
       timespan(samt_data, samt_units);

integtime = timespan(repmat(5, size(samt)), 's');

h5data.rio_abs.data_path = '/data/data'; % Path to data
h5data.rio_abs.data_units_attr = 'units'; % Attribute name
h5data.rio_rawpower.data_path = '/data/raw_data'; % Path to data
h5data.rio_rawpower.data_units_attr = 'units'; % Attribute name

loading_failed = true;
if isfield(h5data, cls)
  d = getfield(h5data, cls);
  try
    data = double(h5read(filename, d.data_path))';
    data_units = h5readatt(filename, d.data_path, d.data_units_attr);
    data_units = data_units{1};
    loading_failed = false;
  catch
    data = [];
    df = info(rio, 'defaultfilename', cls);
    data_units = df.units;
    
    samt = timestamp('unixtime',[]);
    integtime = samt;
  end
else

  error(sprintf('Do not know how to load %s data', cls));
end

if strcmpi(data_units, 'volts')
  % Use SI abbreviation in common with elsewhere in MIA
  data_units = 'V';
end

% Update riometer information from the data file
rio = getinstrument(defaults.mia);
loc = getlocation(rio);
loc = setgeolat(loc, h5readatt(filename, '/instrument_info', 'latitude'));
loc = setgeolong(loc, h5readatt(filename, '/instrument_info', 'longitude'));
rio = setlocation(rio, loc);

freq_units = h5readatt(filename, '/instrument_info', ...
		       'receive_frequency_units');
freq_units = freq_units{1};
if strcmp(freq_units, 'Hz')
  freq = h5readatt(filename, '/instrument_info', 'receive_frequency') ;
  rio = setfrequency(rio, double(freq));
end

if isa(feval(cls), 'rio_abs')
  obliq = {'obliquity', 'simple', ...
	   'obliquityfactors', 1};
else
  obliq = {};
end

mia = feval(cls, ...
	    'starttime', defaults.starttime, ...
	    'endtime', defaults.starttime + timespan(1, 'd'), ...
	    'instrument', rio, ...
	    'sampletime', samt, ...
	    'integrationtime', integtime, ...
	    'data', data, ...
	    'units', data_units, ...
	    'beams', info(rio, 'allbeams'), ...
	    'load', 0, ...
	    obliq{:});

% if loading_failed
%   disp('loading failed')
  
%   mia
% end

if loading_failed == false
  disp('loading worked!!!!')
  mia
end  
%   mia
% end



