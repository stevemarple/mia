function varargout = rio_imagedlg(action, varargin)
%IMAGEPLOTDLG  Dialogue box for creating a RIO_IMAGE object.
%
%

switch action
  % --------------------------------------------------------------
 case 'init'
  % mfilename('init', mia, parentFh, getDataFcn);
  
  defaults.data = [];
  defaults.miniplots = [8 8]; % NB this is (x y)
  defaults.startsample = 1;
  defaults.endsample = [];
  defaults.step = 1;
  
  defaults.pixelunits = 'deg';
  defaults.height = [];
  defaults.interpmethod = 'invdist (v4)';
  defaults.beams = [];
  defaults.xpixelpos = [];
  defaults.ypixelpos = [];
  defaults.getdatafcn = {};
  
  [defaults unvi] = interceptprop(varargin, defaults);

  mia = defaults.data;
  defaults = rmfield(defaults, 'data');
  instrument = getinstrument(mia);
  
  if ~isa(mia, 'rio_base')
    error('mia must be derived from rio_base');
  end

  if isempty(defaults.endsample)
    defaults.endsample = min(defaults.startsample - 1 + ...
			     prod(defaults.miniplots), getdatasize(mia, 2)); 
  end
  
  if isempty(defaults.beams)
    defaults.beams = getbeams(mia); % use all beams
  end
  
  if isempty(defaults.height)
    defaults.height = info(instrument, 'defaultheight');
  end
  
  [xpixelpos ypixelpos] = info(instrument, 'pixels', ...
			       'units', defaults.pixelunits, ...
			       'height', defaults.height);
  if isempty(defaults.xpixelpos) 
    defaults.xpixelpos = xpixelpos;
  end
  if isempty(defaults.ypixelpos) 
    defaults.ypixelpos = ypixelpos;
  end
  
  
  
  
  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text
  fh = figure('Name', ['Image data for ' gettype(mia)], ...
	      'NumberTitle', 'off', ...
	      'Visible', 'on', ...
	      'HandleVisibility', 'callback', ...	
	      'MenuBar', 'None');

  % Add standard menus
  helpData = {'Creating image data', ...
	      'miahelp(''/imagedata/mia2id.html'')', 'help_images'};
  miamenu(fh, 'help', helpData);
  
  % add a view menu linking back to the parent window. This gives us a
  % way to link together any children created by this dialog, with
  % those created by the parent window
  if 0
    if nargin < 3
      viewmenu(fh); % no parent to link to, create a new family of views
    else
      % link to existing family of views
      viewmenu(varargin{2}, fh);
    end
  end
  bgColour = get(fh, 'Color');

  % store some information in UserData are. Put as a struct so it could
  % be shared later
  ud = get(fh, 'UserData');
  
  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  longestStr = 'Interpolation method: ';
  longestStr = [longestStr ' '];
  
  
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', longestStr);
  textPos = uiresize(textH, 'l', 'b');
  % frameColour = get(textH, 'BackgroundColor');
  frameColour = get(fh, 'Color');

  delete(textH);
  
  % allocate space for the handles and positions
  advH     = zeros(14,1);  % handles to uicontrols in advanced frame 
  advPos   = zeros(12,4);  % positions  "                    "

  % have to guess something for framewidth. Use a rule-of-thumb. Since all
  % the buton widths are related to text size having the frame width to be a
  % constant factor times larger than textPos(3) seems ok.
  frameWidth = 3 * textPos(3);
  advPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+8*textPos(4)+7*lineSpacing]; 
  
  rhCol = advPos(1,1) + spacing(1) + textPos(3) + 30;
  
  
  % resize figure to right size, keep top right in same place
  figWH = advPos(1,1:2) + advPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  % -----------------------------
  % Advanced frame
  % create all the buttons / boxes / menus but call uiresize after all
  % the controls in the frame have been created. This will reduce the
  % number of calls to uiresize.
  % frame = 1
  advH = uicontrol(fh, 'Style', 'frame', ...
		   'BackgroundColor', frameColour, ...
		   'Position', advPos(1,:), ...
		   'Tag', 'advframe');
  
  % 'Advanced' = 2
  advPos(2,:) = [advPos(1,1)+spacing(1), ...
		 advPos(1,2)+advPos(1,4)-spacing(1)-textPos(4), ...
		 advPos(1,3)-2*spacing(2), textPos(4)];
  advH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Convert to image format', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', advPos(2,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % 'Beams' = 15
  advPos(15, :) = [advPos(1,1)+spacing(1), ...
		   advPos(2,2)-lineSpacing- textPos(4), ...
		   textPos(3), textPos(4)];
  advH(15) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Beams: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', advPos(15,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % beambox = 16
  [imagingBeams wideBeams badBeams] = info(getinstrument(mia), 'beams');
  beams = getbeams(mia); % beams in this data
  imagingAllow = intersect(imagingBeams, beams);
  % wideAllow    = intersect(wideBeams, beams);
  wideAllow = []; % don't allow wide beams!

  advPos(16, :) = [rhCol advPos(15,2), 1 1];
  advH(16) = beambox('parent', fh, ...
		     'position', advPos(16,:), ...
		     'imaging', imagingBeams, ...
		     'imagingallow', imagingAllow, ...
		     'widebeam', wideBeams, ...
		     'widebeamallow', wideAllow, ...
		     'badbeams', badBeams, ...
		     'title', [' to plot']);
  
  setuserdata(getbeambox(advH(16)), fh);

  % preselect to goodbeams for convenience
  goodbeams = setdiff(imagingAllow, badBeams);
  setbeams(getbeambox(advH(16)), 'beams', goodbeams);
  
  % 'Pixel units' = 3
  advPos(3,:) = [advPos(1,1)+spacing(1), ...
		 advPos(15,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  advH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Pixel units: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', advPos(3,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % km/degrees menu
  advPos(4,:) = [rhCol advPos(3,2), 1 1]; % will call uiresize later
  advH(4) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'HorizontalAlignment', 'center', ...
		      'String', {'deg' 'km' 'km_antenna' 'aacgm'}, ...
		      'Position', advPos(4,:), ...
		      'Callback', [mfilename '(''pixelunits'');'], ...
		      'Tag', 'pixelunitsmenu');
  ud.pixelunitsmenuH = advH(4);
  setpopupmenu(ud.pixelunitsmenuH, defaults.pixelunits);
  
  % 'X grid points'
  advPos(5,:) = advPos(3,:) - [0 textPos(4)+lineSpacing 0 0];
  advH(5) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'X grid points: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', advPos(5,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % x grid points box
  advPos(6,:) = [rhCol advPos(5,2) textPos(3:4)];
  advH(6) = uicontrol(fh, 'Style', 'edit', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', advPos(6,:), ...
		      'Callback', [mfilename '(''grid'');'], ...
		      'Tag', 'xgridpoints');

  % 'Y grid points'
  advPos(7,:) = advPos(5,:) - [0 textPos(4)+lineSpacing 0 0];
  advH(7) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Y grid points: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', advPos(7,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % x grid points box
  advPos(8,:) = [rhCol advPos(7,2) textPos(3:4)];
  advH(8) = uicontrol(fh, 'Style', 'edit', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', advPos(8,:), ...
		      'Callback', [mfilename '(''grid'');'], ...
		      'Tag', 'ygridpoints');
  
  % 'Mapping height'
  advPos(9,:) = advPos(7,:) - [0 textPos(4)+lineSpacing 0 0];
  advH(9) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Mapping height: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', advPos(9,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % mapping height numberbox (convert from metres to km)
  advPos(10,:) = [rhCol advPos(9,2) 1 1];
  advH(10) = numberbox(fh, advPos(10,:), '%d', ...
		       'n=round(min(max(50,n),300));', defaults.height/1e3);
  set(advH(10), 'Tag', 'mapheight');

  % 'km'
  advPos(11,:) = [0 0 1 1]; % cannot position until after uiresize
  advH(11) = uicontrol(fh, 'Style', 'text', ...
		       'String', ' km', ...
		       'HorizontalAlignment', 'left', ...
		       'Position', advPos(11,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');

  % 'Interpolation method: '
  advPos(13,:) = advPos(9,:) - [0 textPos(4)+lineSpacing 0 0];
  advH(13) = uicontrol(fh, 'Style', 'text', ...
		       'String', 'Interpolation method: ', ...
		       'HorizontalAlignment', 'right', ...
		       'Position', advPos(13,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');
  % Interpolation method menu
  interpMethods = {'invdist (v4)' 'linear' 'cubic' 'nearest'};
  advPos(14,:) = [rhCol advPos(9,2)-textPos(4)-lineSpacing 1 1];
  advH(14) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'String', interpMethods, ...
		       'HorizontalAlignment', 'center', ...
		       'Position', advPos(14,:), ...
		       'Tag', 'interpmethod');
  
  % 'reset to defaults' button
  advPos(12,:) = [rhCol advPos(13,2)-textPos(4)-lineSpacing 1 1];
  advH(12) = uicontrol(fh, 'Style', 'pushbutton', ...
		       'String', 'Reset to defaults', ...
		       'HorizontalAlignment', 'center', ...
		       'Position', advPos(12,:), ...
		       'Callback', [mfilename '(''advdefaults'');'], ...
		       'Tag', 'advdefaults');
  %	       'String', 'Default positions', ...
  
  advPos([4:2:14 11],:) = uiresize(advH([4:2:14 11]), 'l', 'b', ...
				   {[], '12345678901234567890', ...
		    '12345678901234567890', ...
		    '9999', [], [], []});
  
  advPos(11,1:2) = [advPos(10,1)+advPos(10,3) advPos(9,2)];
  set(advH(11), 'Position', advPos(11,:));
  
  okPos = [10 10 1 1];
  [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'')'], ...
			   [mfilename '(''cancel'')']);
  

  ud.defaults = defaults;
  set(fh, 'UserData', ud);
  
  % get the rows/columns text printed corretly
  % feval(mfilename, 'miniplots', 'u', fh);

  % set units
  setpopupmenu(ud.pixelunitsmenuH, ud.defaults.pixelunits);
  
  % now get the defaults set correctly. Tell function which figure to
  % update since this isn't a callback
  feval(mfilename, 'advdefaults', fh);
  
  
  set(fh, 'Visible', 'on');
  return;

  % --------------------------------------------------------------
 case 'configfilter'
  % mfilename('configfilter');
  fh = gcbf;
  ud = get(fh, 'UserData');
  filterNum = get(findobj(fh, 'Tag', 'filtermenu'), 'Value');
  ud.filters{filterNum} = configure(ud.filters{filterNum});
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
 case 'grid'
  % mfilename('grid', 'x');
  % mfilename('grid', 'y');
  fh = gcbf;
  h = gcbo;
  s = get(h, 'String'); 
  n = unique(mystr2num(s)); % also sorts
  set(h, 'String', printseries(n));

  % --------------------------------------------------------------
 case 'pixelunits'
  % mfilename('pixelunits')
  feval(mfilename, 'defaultpixels');
  
  % --------------------------------------------------------------
 case 'defaultpixels'
  % mfilename('pixels')
  % mfilename('pixels', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  mia = feval(ud.defaults.getdatafcn{:});
  [x y] = info(getinstrument(mia), 'pixels', ...
	       'beams', ud.defaults.beams, ...
	       'units', popupstr(ud.pixelunitsmenuH), ...
	       'height', ud.defaults.height);
  set(findobj(fh, 'Tag', 'xgridpoints'), 'String', printseries(x));
  set(findobj(fh, 'Tag', 'ygridpoints'), 'String', printseries(y));
  
  % --------------------------------------------------------------
 case 'advdefaults'
  switch nargin 
   case 1
    % callback, gcbf better be valid!
    fh = gcbf;
   case 2
    % not callback
    fh = varargin{1};
   otherwise
    error('incorrect parameters');
  end
  ud = get(fh, 'UserData');

  % set default pixel positions
  feval(mfilename, 'defaultpixels', fh);

  % set mapping height
  setvalue(getnumberbox(findall(fh, 'Tag', 'mapheight')), ...
	   ud.defaults.height / 1e3);
  
  % set interpolation method
  interpmethodh = findobj(fh, 'Tag', 'interpmethod');
  setpopupmenu(interpmethodh, ud.defaults.interpmethod);
  
  % --------------------------------------------------------------
 case 'ok'
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  ud = get(fh, 'UserData');
  
  mia = feval(ud.defaults.getdatafcn{:});
  starttime = getstarttime(mia);
  endtime = getendtime(mia); 
  resolution = getresolution(mia);
  [imageBeams wideBeams] = info(getinstrument(mia), 'beams');
  beams = getbeams(getbeambox(findobj(fh, 'Tag', 'mapbutton')))
  
  removeNegValues = 1;
  if removeNegValues & ~isa(mia, 'rio_abs')
    removeNegValues = 0; % don't remove except for irisabs
  end
  % % check we have at least 3 beams left (ignore for imagedata)
  % if prod(size(beams)) < 3
  %   errordlg('At least 3 imaging beams must remain', 'Error', ...
  %       'modal');
  %   set(fh, 'Pointer', pointer);
  %   return;
  % end
  
  %   units = get(findobj(fh, 'Tag', 'unitsmenu'), 'Value');
  %   switch units
  %    case 1
  %     units = 'km',
  %    case 2
  %     units = 'deg'
  %    otherwise
  %     error('unknown units');
  %   end
  pixelunits = popupstr(ud.pixelunitsmenuH);
  
  % get and check the grid points
  xpixelpos = str2num(get(findobj(fh, 'Tag', 'xgridpoints'), ...
			  'String')); 
  ypixelpos = str2num(get(findobj(fh, 'Tag', 'ygridpoints'), ...
			  'String'));
  if prod(size(xpixelpos)) < 1 | prod(size(ypixelpos)) < 1
    errordlg('Need at least 1 grid point on each axis', 'Error', ...
	     'modal'); 
    set(fh, 'Pointer', pointer);
    return;
  end
  
  % height box is km, need m
  height = 1e3 * getvalue(getnumberbox(findobj(fh, 'Tag', 'mapheight')));
  interpmethod = popupstr(findobj(fh, 'Tag', 'interpmethod'));

  % for printing keep the selected method, but for the function calls
  % use the correct method name (dependent upon matlab version. Bloody
  % Mathworks, why did they change the name and not let the old version
  % work still?!)
  if strcmp(interpmethod, 'invdist (v4)')
    interpmethod2 = 'v4';
  else
    interpmethod2 = interpmethod;
  end

  % drawnow;
  %   iwb = iwaitbar('Creating image data');
  %   id = imagedata(mia, 'xpixelpos', xpixelpos, ...
  % 		 'ypixelpos', ypixelpos, ...
  % 		 'height', height, ...
  % 		 'pixelunits', units, ...
  % 		 'beams', beams, ...
  % 		 'interpmethod', interpmethod2, ...
  % 		 'iwaitbar', iwb);
  t = timestamp('now');
  cancelH = findobj(fh, 'Type', 'uicontrol', ...
		'Style', 'pushbutton', ...
		'Tag', 'cancel');
  ri = rio_image(mia, ...
		 'xpixelpos', xpixelpos, ...
   		 'ypixelpos', ypixelpos, ...
   		 'height', height, ...
   		 'pixelunits', pixelunits, ...
   		 'beams', beams, ...
   		 'interpmethod', interpmethod2, ...
		 'cancelhandle', cancelH);
  if ~ishandle(fh)
    return; % cancel pressed
  end;
  set(fh, 'Pointer', pointer);
  buildtime = timestamp('now') - t;

  % close(iwb);
  miashow('init', ri, ...
	  'buildtime', buildtime);

  close(fh);
  
  % --------------------------------------------------------------
 case 'cancel'
  varargout = cell(1,nargout);
  close(gcbf);
  
  
  % --------------------------------------------------------------
 otherwise
  error('unknown action');
  
end


% --------------------------------------------------------------
% function r = floortoval(n, m)
% floor, rounding n down to a multiple of m. E.g. if m is 2 r is always
% odd, and if m is 10 r always ends in a zero.
% r = n - rem(n, m);

% --------------------------------------------------------------
function r = mystr2num(s)
t = s;
[tmp tmpIndex] = setdiff(t, '0123456789:-.'); % keep 0-9:
t(tmpIndex) = ' ';
if ~strcmp(s, t) 
  disp(sprintf('\a')); % string was changed - make an annoying beep
end
r = str2num(['[' t ']']);



