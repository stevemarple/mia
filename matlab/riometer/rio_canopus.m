function r = rio_canopus
%CANOPUS  Return a list of the CANOPUS RIOMETER instruments.
%
%   r = CANOPUS
%   r: vector of RIOMETER objects
%
%   For more details about CANOPUS see http://??
%
%   See also RIOMETER.

r = [rio_back_1 rio_cont_1 rio_daws_1 rio_eski_1 rio_fchu_1 ...
     rio_fsim_1 rio_fsmi_1 rio_gill_1 rio_isll_1 rio_mcmu_1 ...
     rio_pina_1 rio_rabb_1 rio_rank_1 rio_talo_1];
