function mia = arcom2mia(miaIn, varargin)
cls = class(miaIn);

defaults.cancelhandle = [];
defaults.createargs = {};
defaults.extractargs = {};
defaults.asciiloadfunc = '';
defaults.asciiloadfuncextraargs = {};
defaults.emptydatafunc = '';
defaults.emptydatafuncargs = {};

defaults.filename = '';
defaults.failiffilemissing = [];
% flag indicating if calibration data is wanted
defaults.calibrationdata = false;

% Flag to indicate if the calibration data is common for all beams. If empty
% use the riometer default.
defaults.commoncalibrationdata = [];

% List of beams for calibration data. If empty use the riometer default.
defaults.calibrationbeams = [];

defaults.complexdata = 0;
defaults.archive = '';

defaults.resolution = [];
defaults.resolutionmethod = '';

% If set a callback function which can adjust a block of aligned data before
% it is inserted into the return variable. Useful to convert blocks of
% calibration values to a single value (eg "reducecalibrationdata").
defaults.alignedblockcallback = '';

% allow user to 'filter' data as it is loaded
defaults.filter = '';

defaults.verbosity = 1;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});


rio = getinstrument(miaIn);
if ~isa(rio, 'riometer')
  error('instrument must be a riometer');
end

st = getstarttime(miaIn);
et = getendtime(miaIn);

bestres = info(rio, 'bestresolution');
beams = getbeams(miaIn);
if isempty(beams)
  beams = info(rio, 'allbeams');
end


mia = feval(cls, ...
	    'starttime', st, ...
	    'endtime', et, ...
	    'sampletime', repmat(timestamp, 0, 0), ...
	    'integrationtime', timespan([], 's'), ...
	    'instrument', rio, ...
	    'data', [], ...
	    'load', 0, ...
	    'beams', beams);
dfmia = info(rio, 'defaultfilename', cls);
mia = setunits(mia, dfmia.units);

mia_c = cell(1, ceil((et - st) ./ dfmia.duration));
cnt = 0;

% get default filename information (struct)
df = info(rio, 'defaultfilename', 'original_format', ...
          'archive', defaults.archive);

if isempty(defaults.failiffilemissing)
  % use default for this riometer
  defaults.failiffilemissing = df.failiffilemissing; 
end

if isempty(defaults.filename)
  defaults.filename = df.fstr;
end

if isempty(defaults.resolution)
  res = []; % don't set resolution at all
else
  if defaults.resolution > df.duration
    % resolution too large to set whilst loading, attempt to do so afterwards
    res = []; 
  else
    res = defaults.resolution;
  end
  if defaults.resolution > (et - st)
    error('requested resolution larger than duration');
  end
  
  % If desired resolution requires interpolating then call setresolution,
  % otherwise use spaceregularly
  usesetresolution = (defaults.resolution < df.resolution);
end
usesetresolution = true;

% Have to work around horrid possibility of having multiple files for a
% given time. Add a numeric specifier into format string, but only on the
% file portion
%  [fstr_path fstr_file fstr_ext] = fileparts(df.fstr);
%  fstr_file = strrep(fstr_file, '0000_f', '%%04d_f');
%  fstr = fullfile(fstr_path, [fstr_file fstr_ext]);

% Modify the format specifier to include a sequence number. This requires
% the format string to be expanded with strftime, and then later
% sprintf'd with the sequence number
fstr = info(rio,'includesequencenumber',df.fstr);


% Method: load each file (or group of files in the case sequence numbers
% are in use). Extract the samples which correpsond to within that
% period and process. Reserve the samples which correspond to the
% following set of data to be processed.

last_seq_mia = []; % Data left over from the previous sequence files

% round start time to start of previous file
st2 = floor(st, df.duration) - df.duration;

% round up end time to end of following file (data can run over into next
% file)
et2 = ceil(et, df.duration) + df.duration;

t = st2;
t_next = t + df.duration;
while 1
  
  
  % for a given time loop through all files, incrementing the sequence
  % number
  filename1 = strftime(t, fstr);
  n = 0;
  seq_mia = []; % the data from all files in the sequence
  while 1
    % allow graphical user interface to interrupt loading of data
    cancelcheck(defaults.cancelhandle);

    filename2 = sprintf(filename1, n);
    missing = ~url_exist(filename2);

    if n == 0 & defaults.failiffilemissing & missing
      error(sprintf('cannot continue loading, %s missing', filename2));
    end
    
    if missing
      break;
    else
      tmp = arcom_file_to_mia(filename2, ...
			      'mia', feval(cls, ...
					   'instrument', rio, ...
					   'beams', beams, ...
					   'log', 0, ...
					   'load', 0), ...
			      'calibrationdata', defaults.calibrationdata, ...
			      'commoncalibrationdata', ...
			      defaults.commoncalibrationdata, ...
			      'calibrationbeams', ...
			      defaults.calibrationbeams, ...
			      'complexdata', defaults.complexdata, ...
			      'verbosity', defaults.verbosity);
      if ~isempty(tmp)
	% Insert newly-loaded data into the chunk of data for all
        % sequence files
	if isempty(seq_mia)
	  seq_mia = tmp;
	else
	  seq_mia = nonaninsert(seq_mia, tmp);
	end
      end
      
    end
    
    % if filename does not contain a sequence number don't keep going
    % around the loop
    if strcmp(filename1, filename2)
      break; 
    end
    
    n = n + 1;
  end
  % Finished loading all sequence files for time t
  
  % If any data was left over from last time time include it now
  if ~isempty(last_seq_mia)
    if isempty(seq_mia)
      seq_mia = last_seq_mia;
    else
      seq_mia = nonaninsert(last_seq_mia, seq_mia);
    end
    last_seq_mia = [];
  end
  
  if ~isempty(seq_mia)
    % Extract any data after the true end time
    last_seq_mia = extract(seq_mia, ...
			   'starttime', t_next);
    if prod(getdata(last_seq_mia)) == 0
      last_seq_mia = []; % forget about it if it has no samples
    end
    
    % Extract the block of data which should be within the file.
    seq_mia = extract(seq_mia, ...
		      'starttime', t, ...
		      'endtime', t_next);
    
    % Now we can at last change the resolution
    if ~isempty(res)
      if usesetresolution
	% global SEQ_MIA; SEQ_MIA = seq_mia
	seq_mia = setresolution(seq_mia, res, defaults.resolutionmethod);
      else
	seq_mia = spaceregularly(seq_mia, ...
				 'resolution', res, ...
				 'method', defaults.resolutionmethod);
      end
    end
    
    % Give the calling function the opportunity to make any adjustments
    % (eg reduce blocks of calibration values to single values)
    if ~isempty(defaults.alignedblockcallback)
      seq_mia = feval(defaults.alignedblockcallback, seq_mia);
    end
    
    % Do filtering like loaddata does
    seq_mia = mia_filter(seq_mia, defaults.filter);
        
    % Save the object and insert later when it is more efficient
    cnt = cnt + 1;
    mia_c{cnt} = seq_mia;
  end
  
  
  
  % Maybe we don't have all the data, but either the files just after our
  % end time were missing or didn't have any valid packets. No point in
  % advancing to a new time since that won't have our data either. Stop.
  if t + df.duration > et
    break;
  end
  
  % move on to next time
  t = t_next;
  t_next = t_next + df.duration; 
end

if cnt > 0
  mia = nonaninsertmany(mia, mia_c{:});
end


% trim to requested start/end times
mia = extract(mia, ...
	      'starttime', st, ...
	      'endtime', et);      


% mia = adddataquality(mia, 'Uncalibrated');

if ~isempty(defaults.resolution) & isempty(res)
  % resolution was too large to set earlier, so do so afterwards
  if getdatasize(mia, 'end') > 1
    % can only set a resolution if have > 1 sample
    if usesetresolution
      mia = setresolution(mia, defaults.resolution, defaults.resolutionmethod);
    else
      mia = spaceregularly(mia, ...
			   'resolution', res, ...
			   'method', defaults.resolutionmethod);
    end
    
  end
end


