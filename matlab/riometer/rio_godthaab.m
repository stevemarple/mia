function r = rio_godthaab
%RIO_GODTHAAB  RIOMETER object for Godthaab.

% http://www.iacg.org/iacg/ground_stations/rio.html
r = riometer('name', '', ...
	     'abbreviation', 'ghb', ...
	     'location', location('Godthaab', 'Greenland', ...
				  64.2, 308.3), ...
	     'frequency', nan);


