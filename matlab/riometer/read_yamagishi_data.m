function mia = read_yamagishi_data(rio, varargin)


% to save _huge_ amounts of execution time this function does not make full
% use of the time classes, nor does it use INSERT. Instead the data is
% loaded as full days (which is how the files are saved), with each record
% inserted into the correct place. Calculations are made as seconds in the
% day, which is why it is important to check that the record belongs the
% current day. After loading the necessary data files the mia object is
% trimmed to the appropriate start and end times. Trimming for the
% desired beams is done as each day of data is loaded.


% interval to read data
defaults.starttime = [];
defaults.endtime = [];

defaults.beams = [];

defaults.class = '';
defaults.units = 'ADC';
% temporal resolution of the input file(s)
defaults.inputresolution = [];

% write data out at this resolution. If not set then it is assumed that
% the blocks are on boundaries of inputresolution, a warning will be
% produced if that is not the case
defaults.outputresolution = [];

% the position of each beam in the file. The data is sorted so that data
% is in order of ascending beam number
% defaults.beamorder = [];
defaults.beamorder = reshape(fliplr(reshape(1:64, [8 8])'), [1 64]);

% a strftime format string to indicate how to load the data
defaults.filename = '';
defaults.limits = [0 4095];
defaults.sample = 'uint16';

[defaults unvi] = interceptprop(varargin, defaults);

isofstr = '%Y-%m-%d %H:%M:%S';

resFactor = 1;

if isempty(defaults.class)
  error('class must be specified');
elseif isempty(defaults.starttime)
  error('starttime must be specified');
elseif isempty(defaults.endtime)
  error('endtime must be specified');
elseif isempty(defaults.inputresolution)
  error('inputresolution must be specified');
elseif isempty(defaults.beamorder)
  error('beamorder must be specified');
elseif ~isempty(defaults.outputresolution)
  if defaults.outputresolution > defaults.inputresolution
    error('output resolution cannot be lower than input resolution');
  end
  resFactor = defaults.inputresolution / defaults.outputresolution;
  if resFactor ~= floor(resFactor)
    error('input resolution must be a multiple of the output resolution');
  end
end


[ib wb] = info(rio, 'beams');
allBeams = sort([ib wb]);
if isempty(defaults.beams)
  defaults.beams = allBeams;
end
defaults.beams = reshape(defaults.beams, [1 prod(size(defaults.beams))]);

% The data is organised as files of one day duration, but if errors occur
% then files for hours other than 0 may be present.

day = timespan(1, 'd');
% hour = timespan(1, 'h');

t0 = floor(defaults.starttime, day);
et = ceil(defaults.endtime, day);

res = defaults.outputresolution;
if isempty(res)
  res = defaults.inputresolution;
end
resSecs = gettotalseconds(res);

inputresolutionSecs = gettotalseconds(defaults.inputresolution);

mia = feval(defaults.class, ...
	    'starttime', t0, ...
	    'endtime', et, ...
	    'resolution', res, ...
	    'beams', defaults.beams, ...
	    'instrument', rio, ...
	    'data', repmat(nan, length(defaults.beams), (et-t0) / res), ...
	    'units', defaults.units, ...
	    'load', 0, ...
	    'log', 1);

obl = '';
oblFac = [];
if strcmp(defaults.class, 'rio_abs')
  mia = setobliquityfield(mia, obl, oblFac);
end

samplesPerDay = day / res;

boundaryCounter = [0 0]; % count proportion of records not on boundary
while t0 < et
  dateDv = datevec(t0, 1:3);
  
  tNextDay = t0 + day;
  data = repmat(nan, length(defaults.beams), samplesPerDay);

  % files should be for hour=00, but could be other times if there was a
  % problem
  for h = 0:23
    t = t0 + timespan(h, 'h');
    fname = strftime(t, defaults.filename);
    if ~isempty(dir(fname))
      % read the file
      disp(['loading ' fname]);
      [fid mesg] = fopen(fname, 'r', 'ieee-le'); % PC format
      if fid == -1
	error(sprintf('could not open %s: %s', fname, mesg));
      end
      idx2 = -1;
      % read records
      while ~feof(fid)
	[rt rd rpos] = localReadRecord(fid, fname, defaults);
	if isempty(rt)
	  break;
	elseif ~isequal(dateDv, rt(1:3))
	  % sanity check, since we are not using insert to add data
	  warning(sprintf(['record beginning %s does not belong day %s\n' ...
			   'file name: %s\n' ...
			   'file position: %d\n' ...
			   'YMD: %d %d %d'], ...
			  strftime(timestamp(rt), isofstr), ...
			  strftime(t0, isofstr), ...
			  fname, ...
			  rpos, ...
			  rt(1:3)));
	else

	  if isempty(defaults.outputresolution)
	    % check record starts on a boundary value
	    rts = rt(4:6) * [3600; 60; 1];
	    if rem(rts, inputresolutionSecs)
	      if 0
		warning(sprintf(['record for %s does not start on a %s ' ...
				 'boundary, moving to nearest boundary'], ...
				strftime(timestamp(rt), isofstr), ...
				char(defaults.inputresolution)));
	      end
	      rts = (round(rts / inputresolutionSecs)) * ...
		    inputresolutionSecs;
	      boundaryCounter = boundaryCounter + [1 1];
	    else
	      boundaryCounter = boundaryCounter + [0 1];
	    end
	  elseif resFactor > 1
	    % increase number of samples (never decrease)
	    rd2 = zeros(size(rd) .* [1 resFactor]);
	    for n = 0:(size(rd, 2)-1)
	      rd2(:, n*resFactor + [1:resFactor]) = rd(:, n+1);
	    end
	    rd = rd2;
	  end
	  
	  % offset = (rt - t0) / res;
	  offset = rts / resSecs;
	  idx1 = offset + 1;
	  idx2 = offset + 8*resFactor;
	  % data(:, offset + [1:(8*resFactor)]) = ...
	  data(defaults.beamorder(defaults.beams), idx1:idx2) = ...
	      rd(:, :);
	end
      
      end
      fclose(fid);
      fid = [];
      if idx2 >= 86400
	break; % don't need to look for any more files from this day
      end
    end

  
  end % matches for h = 0:23

  if strcmp(defaults.class, 'rio_rawpower')
    % store raw data in same class
    data = feval(defaults.sample, data);
  end
  miaDay = feval(defaults.class, ...
		 'starttime', t0, ...
		 'endtime', tNextDay, ...
		 'resolution', res, ...
		 'beams', defaults.beams, ...
		 'instrument', rio, ...
		 'data', data, ...
		 'units', defaults.units, ...
		 'load', 0, ...
		 'log', 0);
  if strcmp(defaults.class, 'rio_abs')
    miaDay = setobliquityfield(miaDay, obl, oblFac);
  end
  
  if isempty(mia)
    mia = miaDay;
  else
    mia = insert(mia, miaDay);
  end
  
  t0 = tNextDay;
end

mia = extract(mia, ...
	      'starttime', defaults.starttime, ...
	      'endtime', defaults.endtime);

if any(boundaryCounter)
  disp(sprintf('proportion of records not on boundary: %d/%d (%.1f%%)', ...
	       boundaryCounter, 100*boundaryCounter(1)/ ...
	       boundaryCounter(2)));
end

% ----------------------------------
function [rt, rd, fpos] = localReadRecord(fid, fname, defaults)

rt = [];
rd = [];
fpos = ftell(fid);
% read the time
sz = [1 6];
[dv c] = fread(fid, sz, 'uint8');
if c ~= prod(sz)
  return;
end


if any(dv > [99 12 31 23 59 59])
  % NB two-digit year at this point
  disp(sprintf(['bad timestamp: [%d %d %d %d %d %d]\n' ...
		'file name: %s\n' ...
		'file position: %d\n' ...
		'block start: %d'], ...
	       dv(1:6), fname, ftell(fid), fpos));
  return; % year not valid, discard block
elseif dv(1) < 80
  dv(1) = dv(1) + 2000;
else
  dv(1) = dv(1) + 1900;
end
% rt = timestamp(dv);
rt = dv;

% skip the next 10 bytes
fseek(fid, 10, 0);

% read 8 frames
sz = [prod(size(defaults.beamorder)) 8];
[rd c] = fread(fid, sz, defaults.sample);
% [rd c] = fread(fid, sz, 'uint16');
if c ~= prod(sz)
  rt = [];
  rd = [];
  return;
end

% rd(rd <= 0) = nan;
% rd(rd >= 4095) = nan;

% rd(rd <= defaults.limits(1)) = nan;
% rd(rd >= defaults.limits(2)) = nan;

