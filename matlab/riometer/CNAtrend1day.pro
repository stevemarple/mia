; Reading 1-MIN BINARY Data which was made by "conv"

;########################################################################
;
; Data Plot Only For One-Beam, Having Been Modified From Overlplot.pro
;
; Re-ordering data time series according to LOCAL SIDEREAL TIME.
; Y Murayama, 15 DEC 1995
;
; Plot Quiet Day Curves by Reading QDC data file.
; Y Murayama, 4 JAN 1996
;
;########################################################################



;****************************************
PRO ReadCNA, cnadb, timLST, timGPS, year, month, day, maxn, fn
;****************************************
COMMON Headers, freq, band_width, time_const, $
  calf, agcf, bsr, t

blknum = LONG(0)
year = 0B & month = 0B & day = 0B
freq = 0. & band_width = 0
time_const = 0
calf = 0 & agcf = 0
bsr = 0
t = [0., 0., 0., 0.]
iframe = 0
ifr = LONG(0)
year = 0.
month = 0.
day = 0.

Lmaxn = LONG(0)

print, 'Reading data in a file "', fn, '"...', format = '(a,a,a,$)'
openr, 50, fn

readu, 50, blknum, year, month, day
readu, 50, freq, band_width, time_const
readu, 50, calf, agcf, bsr
readu, 50, t

readu, 50, Lmaxn

;### Lmaxn = 10

maxn = Lmaxn
print, year, month, day, format = '(", date of the data:",3i2.2,"...")'
; print, "maxn=",  maxn

tLST = 0.
tGPS = 0.
timLST = FLTARR(maxn)
timGPS = FLTARR(maxn)
cnadb = FLTARR(maxn, 16, 16)

FOR i = 0, maxn-1 DO BEGIN
    
    
    tmpcna = FLTARR(16, 16)
    readu, 50, ifr, tLST, tGPS, tmpcna
    timLST(i) = tLST
    timGPS(i) = tGPS
    
;    Convdate, timGPS(i), hh, mm, ss
    
    FOR ins = 0, 15 DO BEGIN
        FOR iew = 0, 15 DO BEGIN
            cnadb(i, ins, iew) = tmpcna(ins, iew)
        ENDFOR
    ENDFOR
    
;    IF i MOD 100 EQ 0 THEN $
;      print, i, timLST(i), timGPS(i), cnadb(i, 7, 7)
    execute_this = 'N'
    IF execute_this EQ 'Y' THEN BEGIN
        IF i LT 100 THEN BEGIN
            tm = timGPS(i)
            h = FIX(tm)
            m = FIX( (tm-h)*60. )
            s = FIX( ( (tm-h)*60.-m )*60. )
            print, i, timGPS(i), h, m, s, cnadb(i, 7, 7), $
              format = '(i4,f6.2," ",3i2.2,f6.2)'
        ENDIF
        IF i EQ 100 THEN BEGIN
            close, 50 & stop
        ENDIF
    ENDIF
    
    
        
;    IF i MOD 100 EQ 0 THEN $
;      print, i, '(', timGPS(i), 'UT) ', format = '(i4,a,f6.2,a,$)'
    
ENDFOR

close, 50

END

    




;**************************************************
PRO PlotCNATrend, cnadb, timGPS, maxn, fn
;*************************************************

x = fltarr(maxn)
y1 = fltarr(maxn, 16)
y2 = y1

FOR i = 0, maxn-1 DO BEGIN
    x(i) = timGPS(i)
    y1(i, *) = cnadb(i, 7, *)
    y2(i, *) = cnadb(i, *, 7)
ENDFOR



xrng = [0., 24.]
yrng = [0., 20.]
yshift = 1.

;*** CNA plot for a E-W beam scan ***

px = x(0:maxn-1)
py = y1(0:maxn-1, 0)    

plot, px, py, /nodata, $
  title = "!6"+strmid(fn, 0, 6), $
  xtitle = 'UNIVERSAL TIME (HR)', $
;  ytitle = '!94!8West (N07E00 - N07E15) East!96!6!C!N!6CNA (dB)', $
  ytitle = '!A!94!8West     East!96!6'+'!C!A!8(N07E00 - N07E15)'+$
    '!C!N!6CNA (dB)', $  
  xrange = xrng, xstyle = 1, $
  yrange = yrng, ystyle = 1

xyouts, 15., yrng(1)*1.05, $
  string("     (Curves are shifted by ", yshift, " dB.)", $
         format = '(a,f6.2,a)')


x0 = xrng
FOR k = 0, 15 DO BEGIN
;    yk = 10*k
    yk = yshift*(15-k)
    y0 = [yk, yk]
    oplot, x0, y0, linestyle = 1, thick = 0.001
    py = y1(0:maxn-1, k)+yk
    oplot, px, py, MAX_VALUE = 90., thick = 1, /NOCLIP
ENDFOR



;*** CNA Plot For A N-S Beam Scan ***

py = y2(0:maxn-1, 0)
plot, px, py, /nodata, $
  title = "!6"+strmid(fn, 0, 6), $
  xtitle = 'UNIVERSAL TIME (HR)', $
;  ytitle = '!94!8South (!8N15E07 - N00E07) North!96!6!C!N!6CNA (dB)', $
  ytitle = '!A!94!8South     North!96'+'!C!A!8(N15E07 - N00E07)'+$
    '!C!N!6CNA (dB)', $
  xrange = xrng, xstyle = 1, $
  yrange = yrng, ystyle = 1
xyouts, 15., yrng(1)*1.05, $
  string("     (Curves are shifted by ", yshift, " dB.)", $
         format = '(a,f6.2,a)')
  

FOR k = 0, 15 DO BEGIN
;    yk = 10*k
    yk = yshift*(15-k)
    y0 = [yk, yk]
    oplot, x0, y0, linestyle = 1, thick = 0.001
    py = y2(0:maxn-1, k)+yk
    oplot, px, py, MAX_VALUE = 90., thick = 1, /NOCLIP
ENDFOR

END








;=============================================================================
; Main Program
;=============================================================================
;
;
ch		= 1.1
thickness	= 3.
!P.CHARSIZE	= ch
!P.CHARTHICK	= thickness
!P.THICK	= thickness
!X.THICK	= thickness
!Y.THICK	= thickness
!X.CHARSIZE	= ch
!Y.CHARSIZE	= ch
!P.multi 	= [0, 1, 2]

!X.margin = [6, 3]
!Y.margin = [1, 5]

set_plot,'ps'
device, /landscape, $ ;  /color, bits = 8, $
;  scale = 0.95, xoffset = 3, yoffset = 25
  scale = 0.92, xoffset = 3.2, yoffset = 25.5
; device, /portrait, YOFFSET = 2.5, YSIZE = 24.


;********************
; GET FILE NAME
;********************
fnd = " "
read, "date of data (YYMMDD)? ", fnd
print, fnd
fn = fnd+"minbin.cna"
;fn = "../data/"+fnd+"minbin.cna"


;********************
; READ CNA DATA
;********************
ReadCNA, cnadb, timLST, timGPS, year, month, day, maxn, fn

;********************
; PLOT CNA's
;********************
PlotCNATrend, cnadb, timGPS, maxn, fn

print, " finished."
;close, 60
device, /close


END

