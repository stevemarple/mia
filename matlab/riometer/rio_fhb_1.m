function r = rio_fhb_1
%RIO_FHB_1  RIOMETER object for Frederikshab, Greenland.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'fhb', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'DMI riometer chain', ...
    'location', location('Frederikshab', 'Greenland', ...
                         62.000000, -49.680000), ...
    'frequency', []);

