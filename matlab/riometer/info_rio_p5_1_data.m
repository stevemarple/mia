function r = info_rio_p5_1_data
%INFO_RIO_P5_1_DATA Return basic information about rio_p5_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_p5_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=p5;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'p5';
r.antennaazimuth = 0;
r.antennaphasingx = [-0.75 -0.25 0.25 0.75 -0.75 -0.25 0.25 0.75 ...
    -0.75 -0.25 0.25 0.75 -0.75 -0.25 0.25 0.75];
r.antennaphasingy = [0.75 0.75 0.75 0.75 0.25 0.25 0.25 0.25 -0.25 ...
    -0.25 -0.25 -0.25 -0.75 -0.75 -0.75 -0.75];
r.antennas = [4 4];
r.antennaspacing = 0.5;
r.azimuth = [-45 -18.7 18.7 45 -71.3 -45 45 71.3 -108.7 -135 135 ...
    108.7 -135 -161.3 161.3 135 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
r.badbeams = [];
r.beamplanc = 4;
r.beamplanr = 4;
r.beamwidth = [25.5403 27.6941 27.6943 25.5403 27.6943 26.3305 ...
    26.3305 27.6941 27.6943 26.3305 26.3305 27.6941 25.5403 27.6941 ...
    27.6943 25.5403 119.9 119.9 119.9 119.9 119.9 119.9 119.9 119.9 ...
    119.9 119.9 119.9 119.9 119.9 119.9 119.9 119.9];
r.bibliography = 'art:977';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([]);
r.facility_name = 'AGONET';
r.facility_url = 'http://iacg.org/iacg/ground_stations/agonet.html';
r.facilityid = 3;
r.groupids = [];
r.iantennatype = 'crossed-dipole';
r.ibeams = 16;
r.id = 74;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16];
r.latitude = -77.23;
r.location1 = 'AGO P5';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 123.52;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = 'PENGUIN P5';
r.piid = 28;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan(00, 'h', 00, 'm', 12, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = 'http://www.polar.umd.edu/instruments.html';
r.url2 = {'http://www.polar.umd.edu/instruments.html'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [57.8 43.7 43.7 57.8 43.7 19.3 19.3 43.7 43.7 19.3 19.3 ...
    43.7 57.8 43.7 43.7 57.8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
r.defaultfilename = [];
r.institutions{1} = 'Siena College';
% end of function
