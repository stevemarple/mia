function r = rio_haarpmsr
%RIO_HAARPMSR  HAARP meridian scanning RIOMETER object (not widebeam).

% http://www.polar.umd.edu/haarp/riometer_paper
% http://www.haarp.alaska.edu/haarp/faq.html
r = riometer('name', 'HAARP', ...
	     'abbreviation', 'haarp', ...
	     'facility', 'HAARP', ...
	     'location', location('Gakona', 'Alaska', ...
				  62.3917, -145.1467), ...
	     'frequency', nan);


% http://www.polar.umd.edu/haarp/riometer_paper/node3.html

% pointing direction is geographic North.

