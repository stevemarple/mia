function r = lajolladirectivity(theta, phi, varargin)
%LAJOLLADIRECTIVITY Calculate the directivity of a La Jolla antenna.
%
%   r = LAJOLLADIRECTIVITY(theta, phi);
%   r: complex directivity
%   theta: zenith angle (radians)
%   phi:   azimuth angle (radians)
%   
%   If theta and phi are matrices then they must be the same size.
%
%   See also DIRECTIVITY, TURNSTILEDIRECTIVITY.

load lajolladirectivityvals.txt;

% columns are:
% 1: azimuth
% 2: elevation (all the way from 0 to 180!)
% 3: V dB
% 4: H dB
% 5: Total power (dBi, that is realtive to isotropoic antenna)

% use only the values where elevation < 90
idx = find(lajolladirectivityvals(:, 2) < 90);

% use only one point for zenith. It doesn't matter which for total power,
% but it does vayr for the V and H values
zen = find(lajolladirectivityvals(:, 2) == 90);

idx(end+1) = zen(1);

r2d = 180 / pi;
if matlabversioncmp('<=', '5.1')
  r = griddata(lajolladirectivityvals(idx, 2), ...
	       lajolladirectivityvals(idx, 1), ...
	       lajolladirectivityvals(idx, 5), ...
	       (pi/2 - theta) .* r2d, ...
	       phi .* r2d);
else  
  r = sphinterp(lajolladirectivityvals(idx, 2), ...
		lajolladirectivityvals(idx, 1), ...
		lajolladirectivityvals(idx, 5), ...
		(pi/2 - theta) .* r2d, ...
		phi .* r2d, ...
		'linear');
end

r = r - max(lajolladirectivityvals(:, 5));
