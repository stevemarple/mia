function r = rio_rawqdc_fft(mia, varargin)
%RIO_QDC_FFT Convert a plain riometer QDC to one described by an FFT.
%
% This function is also used to convert a RIO_RAWQDC to one described by
% an FFT. All documentation refers to RIO_QDC/RIO_QDC_FFT, but substitute
% RIO_RAWQDC/RIO_RAWQDC_FFT is using raw (non-linearised QDCs).
%
%   r = RIO_QDC_FFT(mia, ...)
%
% r: completed QDC
% mia: original RIO_QDC object, to which the FFT fit is applied
%
% The following parameter name/value pairs are accepted:
%
%   'fitorder', integer
%     The order of the FFT fit. Must be specified.
%
%   'prefilter', MIA_FILTER_BASE
%     prefilter the RIO_QDC object before applying the FFT fit.
%
%   'preserveoriginalqdc', LOGICAL
%     By default the original QDC with all of its data is kept, but this
%     can be discarded if space is an issue.
%

% This function should work as both rio_qdc_fft and rio_rawqdc_fft (using
% a symlink)
cls = mfilename;

num = numel(mia);
if num ~= 1
  % do recursive call
  tmp = mia(1);
  r = repmat(feval(cls), size(mia));
  for n = 1:num
    r(n) = feval(cls, mia(n), varargin{:});
  end
  return
end

rio = getinstrument(mia);
beams = getbeams(mia);
res = getresolution(mia);

data = getdata(mia);
% dsz = getdatasize(mia);
dsz = size(data);

defaults.prefilter = [];
defaults.fitorder = [];
defaults.preserveoriginalqdc = 1;
defaults.baddata = {};

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.fitorder) | isequal(defaults.fitorder, -1)
  defaults.fitorder = calcnumrioqdcfftcoeffs(rio, ...
					     'beams', beams, ...
					     'resolution', res);
elseif any(defaults.fitorder == -1)
  idx = find(defaults.fitorder == -1)
  defaults.fitorder(idx) = calcnumrioqdcfftcoeffs(rio, ...
						  'beams', beams(idx), ...
						  'resolution', res);
elseif length(defaults.fitorder) == 1
  defaults.fitorder = repmat(defaults.fitorder, dsz(1), 1);
end

if isa(defaults.prefilter, 'mia_filter_base')
  mia = filter(defaults.prefilter, mia);
end

res = getresolution(mia);

coeffs = cell(dsz(1), 1);
for bn = 1:dsz(1)
  % do sine/raised-cosine series fit
  ft = fft(data(bn, :));
  % save only the terms required
  coeffs{bn} = ft(1:(defaults.fitorder(bn)+1));
end

if logical(defaults.preserveoriginalqdc)
  originalqdc = mia;
else
  originalqdc = [];
end

rqf = feval(cls, ...
	    'instrument', rio, ...
	    'beams', beams, ...
	    'originalresolution', res, ...
	    'originalqdc', originalqdc, ...
	    'coefficients', coeffs, ...
	    'baddata', defaults.baddata, ...
	    'load', 0);
% copy all existing processing information, creator, creation time etc
% into the parent object. It also sets the data to the original data in
% the qdc object.
r = setrioqdc(rqf, mia);

% now set the data to be calculated from the FFT coefficients
% r = setdata(r, getdata(rio_qdc(r)));
r = recomputedata(r);

r = addprocessing(r, sprintf('%s converted to %s', ...
			     class(mia), class(r)));



