function mia = rio_qdc(varargin)
%RIO_QDC/RIO_RAWQDC  Constructor for RIO_QDC/RIO_RAWQDC classes.
%
% mia = RIO_QDC(...)
% mia = RIO_RAWQDC(...)
%
% Construct a RIO_QDC or RIO_RAWQDC. On unix systems this function is a
% symbolic link to the RIO_QDC constructor and written in a way to ensure
% the same behaviour for RIO_QDC and RIO_RAWQDC objects.

% cls = 'rio_qdc';
[tmp cls] = fileparts(mfilename);
parent = 'rio_qdc_base';

% Make it easy to change the class definition at a later date
latestversion = 1;
mia.versionnumber = latestversion;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), parent)
  % construct from base object
  p = varargin{1};
  mia = class(mia, cls, p);
  
elseif nargin == 1 & (isa(varargin{1}, cls) | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  p = feval(parent);
  mia = class(mia, cls, p);

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);

  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  defaults.time = [];
  
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log' 'time'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);
  
  if 0
  if isempty(getinstrument(mia))
    error('instrument must be specified');
  elseif isempty(getbeams(mia))
    error('no beams specified');
  end
  end

  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, defaults.time, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end

else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
