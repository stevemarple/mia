function r = gettype(rio, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(rio)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_QDC, RIO_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'QDC power';
  
 case 'u'
  r = 'QDC POWER';
  
 case 'c'
  r = 'QDC power';
  
 case 'C'
  r = 'QDC Power';
  
 otherwise
  error('unknown mode');
end

return

