function r = loadobj(mia)

r = mia;

if isstruct(mia)
  if isfield(mia, 'rio_base')
    % parent class should be rio_qdc_base
    s = localFlatten(mia);
    s = rmfield(s, 'versionnumber');
    p = makeprop(s);
    r = rio_qdc(p{:});
  else
    error('unknown error, cannot convert object');
  end
end




function r = localFlatten(s)
fn = fieldnames(s);
r = [];
for n = 1:length(fn)
  f = getfield(s, fn{n});
  if isa(f, 'mia_base');
    f = struct(f);
  end
  if isstruct(f)
    f = localFlatten(f);
    % copy all fields in f to r
    fn2 = fieldnames(f);
    for n2 = 1:length(fn2)
      f2 = getfield(f, fn2{n2});
      r = setfield(r, fn2{n2}, f2);
    end
  else
    r = setfield(r, fn{n}, f);
  end
    
end
