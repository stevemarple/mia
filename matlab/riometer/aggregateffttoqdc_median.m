function r = aggregateffttoqdc_median(binned)

% Convert binned data to single value
% r = nonanmedian(binned, 1);

qdcsamples = size(binned, 2);
r = zeros(1, qdcsamples);

for n = 1:qdcsamples
  d = binned(:, n);
  d2 = d(~isnan(d));
  if isempty(d2)
    r(n) = nan;
  else
    r(n) = median(d2);
  end
end


