function r = info_rio_eua_1_data
%INFO_RIO_EUA_1_DATA Return basic information about rio_eua_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_eua_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=eua;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'eua';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [60];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = 27;
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'NRCAN';
r.facility_url = 'http://www.nrcan.gc.ca/';
r.facilityid = 9;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 132;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 79.97;
r.location1 = 'Eureka';
r.location1_ascii = '';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = -85.92;
r.modified = timestamp([2014 09 30 14 50 52.857529]);
r.name = '';
r.piid = 27;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1];
r.starttime = timestamp([2008 08 29 00 00 00]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 3e+07;
r.widebeams = [1];
r.zenith = [0];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'nrcanriodata';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/nrcan/%Y/%m/%Y%m%d_eua_abs.txt';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [1 86400];
r.defaultfilename.rio_abs.default.units = 'dB';
r.institutions{1} = 'Natural Resources Canada';
% end of function
