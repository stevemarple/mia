function r = data_location_rio_sgo(mia, t)

if ~isa(mia, 'rio_abs')
  error('only know absorption data locations');
end

rio = getinstrument(mia);
if ~isa(rio, 'riometer')
  error('instrument must be a riometer');
end




ABR = upper(getabbreviation(rio));

% From SGI README file:
% In file name system two Sodankyla riometers have been separated by S05 =
% 30.0 Mhz and S07 = 51.4 MHz.

if rio == rio_sod_1
  ABR = 'S05';
elseif rio == rio_sod_2
  ABR = 'S07';
end


year = getyear(t);
t_now = timestamp('now');

if year < 2000
  % Before 2000
      % http://sgodata.sgo.fi/pub_rio/rio_abs_data/1999/ABI_1999_01/RA990101.ABI
      fstr = ['http://sgodata.sgo.fi/pub_rio/rio_abs_data/%Y/' ABR ...
	   '_%Y_%m/RA%y%m%d.' ABR];

elseif year == getyear(t_now) && getmonth(t) == getmonth(t_now)
  % Current month (year-only directory missing)
  % http://sgodata.sgo.fi/pub_rio/rio_abs_data/ABI_2006_05/RA_20060501.ABI

  fstr = ['http://sgodata.sgo.fi/pub_rio/rio_abs_data/' ABR ...
       '_%Y_%m/RA_%Y%m%d.' ABR];

else
  % 2000 and after
  % http://sgodata.sgo.fi/pub_rio/rio_abs_data/2000/ABI_2000_01/RA_20000101.ABI
  fstr = ['http://sgodata.sgo.fi/pub_rio/rio_abs_data/%Y/' ABR ...
	  '_%Y_%m/RA_%Y%m%d.' ABR];
end

r = strftime(t, fstr);
