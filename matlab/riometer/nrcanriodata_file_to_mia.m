function r = nrcanriodata_file_to_mia(filename, varargin)
%NRCANRIODATA_FILE_TO_MIA  Convert NRCAN riometer data file to MIA object.
%
%   mia = NRCAN(filename, ...)
%   mia: RIO_ABS object
%   filename: CHAR
%


defaults.mia = [];
defaults.cancelhandle = [];
defaults.starttime = [];
defaults.archive = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object must be set');
end
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('instrument object must be set in mia object');
end

if isempty(defaults.starttime)
  error('starttime must be set')
end

beams = info(rio, 'allbeams');
df = info(rio, 'defaultfilename', defaults.mia, 'archive', defaults.archive);

r = feval(class(defaults.mia), ...
	  'load', 0, ...
	  'instrument', rio, ...
	  'beams', beams, ...
	  'starttime', defaults.starttime, ...
	  'endtime', defaults.starttime + df.duration, ...
	  'obliquity', 'simple', ...
	  'obliquityfactors', ones(numel(beams),1));

%r = setstarttime(r, defaults.starttime);
%r = setendtime(r, et);
%r = setbeams(r, beams);

fid = [];
num_lines = 0;
data = zeros(df.size);
dv = zeros(df.size(2), 6);

try
  fid = url_fopen(filename);
  while true
    s = fgetl(fid);
    if isequal(s, -1)
      break
    end
    num_lines = num_lines + 1;
    cols = sscanf(s, '%d %d %d %d:%d:%d %f');
    dv(num_lines, :) = cols(1:6)';
    data(num_lines) = cols(7);
    % [res, ree] = regexp(s, '[0-9.]+');
    % data(num_lines) = str2double(s(res(7):ree(7)));
    
    % for n = 1:6
    %   dv(num_lines, n) = str2double(s(res(n):ree(n)));
    % end
    
  end
  fclose(fid);
  fid = [];
  data = feval(df.dataclass, data(1, 1:num_lines));
  dv = dv(1:num_lines, :); % trim unused part
  
  if all(dv(:, 1) == dv(1, 1)) & ...
	all(dv(:, 2) == dv(1, 2)) & ...
	all(dv(:, 3) == dv(1, 3))
    % Crosses day boundary or some error, use slow method
      samt = timestamp(dv)';
  else
    % Same day. Convert first to timestamp, add offsets.
    sampletime_date = timestamp([dv(1, 1:3), 0 0 0]);
    secs = dv(1:num_lines, 4:6) * [3600; 60; 1];
    sampletime_time = timespan(secs', 's');
    samt = sampletime_date + sampletime_time;
  end
  
  r = setdata(r, data);
  r = setunits(r, df.units);
  r = setsampletime(r, samt + (df.resolution/2));
  r = setintegrationtime(r, repmat(df.resolution, size(samt)));
catch
  if ~isempty(fid)
    fclose(fid)
  end
  disp(lasterr)
end
