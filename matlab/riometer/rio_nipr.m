function r = rio_nipr
%RIO_NIPR  Return list of NIPR riometers
%
%   r = RIO_NIPR
%   r: vector of RIOMETER objects
%
% See also RIOMETER.

r = [rio_tjo_1 rio_hus_1 rio_syo_1 rio_zhs_1 rio_dmh_1 rio_lyr_1];


