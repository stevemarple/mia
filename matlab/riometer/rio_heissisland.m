function r = rio_heissisland
%RIO_HEISSISLAND  RIOMETER object for Heiss Island. Analogue recording.
%
% Operated by Arctic and Antarctic Research Institute (AARI), Russia.


% http://www.aari.nw.ru/clgmi/geophys/station.htm
r = riometer('name', '', ...
	     'abbreviation', 'his', ...
	     'location', location('Heiss Island', 'Arctica', ...
				  80.6, 58.0), ...
	     'frequency', nan);


