function r = info_rio_tik_1_data
%INFO_RIO_TIK_1_DATA Return basic information about rio_tik_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_tik_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=tik;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'tik';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'AARI riometer chain';
r.facility_url = 'http://www.aari.nw.ru/clgmi/geophys/station.htm';
r.facilityid = 13;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 138;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 71.6;
r.location1 = 'Tiksi';
r.location1_ascii = '';
r.location2 = 'Russia';
r.logo = '';
r.logurl = '';
r.longitude = 129;
r.modified = timestamp([2011 07 08 10 38 37.202082]);
r.name = '';
r.piid = 9;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = 'http://www.aari.nw.ru/clgmi/geophys/real_rio.htm';
r.url2 = {'http://www.aari.nw.ru/clgmi/geophys/real_rio.htm' 'http://www.aari.nw.ru/clgmi/geophys/interface2.html'};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Arctic and Antarctic Research Institute';
% end of function
