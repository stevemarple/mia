function r = info_rio_dmh_1_data
%INFO_RIO_DMH_1_DATA Return basic information about rio_dmh_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_dmh_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=dmh;serialnumber=1

r.limits = [];
r.limits.rio_qdc = [0 4096];
r.limits.rio_rawpower = [0 4096];
r.pixels.aacgm.x.max = 114.5;
r.pixels.aacgm.x.min = 108.7;
r.pixels.aacgm.x.step = 0.2;
r.pixels.aacgm.y.max = 76.8;
r.pixels.aacgm.y.min = 75.6;
r.pixels.aacgm.y.step = 0.05;
r.pixels.deg.x.max = 15.4;
r.pixels.deg.x.min = 8.2;
r.pixels.deg.x.step = 0.4;
r.pixels.deg.y.max = 79.64;
r.pixels.deg.y.min = 78.2;
r.pixels.deg.y.step = 0.08;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'dmh';
r.antennaazimuth = 321.818;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.55;
r.azimuth = [-45 -35.5095 -23.2416 -8.10516 8.10516 23.2416 35.5095 ...
    -315 -54.4905 -45 -31.0424 -11.2871 11.2871 31.0424 -315 ...
    -305.509 -66.7584 -58.9576 -45 -18.346 18.346 -315 -301.042 ...
    -293.242 -81.8948 -78.7129 -71.654 -45 -315 -288.346 -281.287 ...
    -278.105 -98.1052 -101.287 -108.346 -135 -225 -251.654 -258.713 ...
    -261.895 -113.242 -121.042 -135 -161.654 -198.346 -225 -238.958 ...
    -246.758 -125.509 -135 -148.958 -168.713 -191.287 -211.042 -225 ...
    -234.491 -135 -144.491 -156.758 -171.895 -188.105 -203.242 ...
    -215.509 -225];
r.badbeams = [1 8 57 64];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [];
r.bibliography = 'art:67';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'DMI riometer chain';
r.facility_url = '';
r.facilityid = 6;
r.groupids = [1 9];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 2;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 ...
    62 63 64];
r.latitude = 76.77;
r.location1 = 'Danmarkshavn';
r.location1_ascii = '';
r.location2 = 'Greenland';
r.logo = '';
r.logurl = '';
r.longitude = -18.66;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 4;
r.qdcclass = 'rio_rawqdc';
r.qdcduration = timespan(28 , 'd');
r.qdcoffset = timespan(00, 'h', 00, 'm', 00, 's');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1.92775 1.58473 1.41796 1.34535 1.34535 ...
    1.41796 1.58473 1.92775 1.58473 1.31972 1.19497 1.1416 1.1416 ...
    1.19497 1.31972 1.58473 1.41796 1.19497 1.09196 1.04834 1.04834 ...
    1.09196 1.19497 1.41796 1.34535 1.1416 1.04834 1.00905 1.00905 ...
    1.04834 1.1416 1.34535 1.34535 1.1416 1.04834 1.00905 1.00905 ...
    1.04834 1.1416 1.34535 1.41796 1.19497 1.09196 1.04834 1.04834 ...
    1.09196 1.19497 1.41796 1.58473 1.31972 1.19497 1.1416 1.1416 ...
    1.19497 1.31972 1.58473 1.92775 1.58473 1.41796 1.34535 1.34535 ...
    1.41796 1.58473 1.92775];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = 'http://web.dmi.dk/fsweb/solar-terrestrial/ground_ionosph_obs/';
r.url2 = {'http://web.dmi.dk/fsweb/solar-terrestrial/ground_ionosph_obs/'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [60.1111 51.879 45.9699 42.7182 42.7182 45.9699 51.879 ...
    60.1111 51.879 41.4349 33.7226 29.2865 29.2865 33.7226 41.4349 ...
    51.879 45.9699 33.7226 24.0375 17.7216 17.7216 24.0375 33.7226 ...
    45.9699 42.7182 29.2865 17.7216 7.78988 7.78988 17.7216 29.2865 ...
    42.7182 42.7182 29.2865 17.7216 7.78988 7.78988 17.7216 29.2865 ...
    42.7182 45.9699 33.7226 24.0375 17.7216 17.7216 24.0375 33.7226 ...
    45.9699 51.879 41.4349 33.7226 29.2865 29.2865 33.7226 41.4349 ...
    51.879 60.1111 51.879 45.9699 42.7182 42.7182 45.9699 51.879 ...
    60.1111];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1 , 'd');
r.defaultfilename.original_format.default.failiffilemissing = true;
r.defaultfilename.original_format.default.format = 'niprriodata';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/dmh_1/original_format/%Y/%m/%y%m%d%H.DMH';
r.defaultfilename.original_format.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 86400];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = true;
r.defaultfilename.rio_power.default.format = 'niprriodata';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/dmh_1/original_format/%Y/%m/%y%m%d%H.DMH';
r.defaultfilename.rio_power.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 86400];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/dmh_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [64 3600];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = true;
r.defaultfilename.rio_rawpower.default.format = 'niprriodata';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/dmh_1/original_format/%Y/%m/%y%m%d%H.DMH';
r.defaultfilename.rio_rawpower.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 86400];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.institutions{1} = 'Danish Meteorological Institute';
% end of function
