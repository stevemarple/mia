function r = info_rio_mai_1_data
%INFO_RIO_MAI_1_DATA Return basic information about rio_mai_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_mai_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=mai;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'mai';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 59;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -70.77;
r.location1 = 'Maitri';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 11.73;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = [];
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = 'http://dod.nic.in/vsdod/antarc1.htm';
r.url2 = {'http://dod.nic.in/vsdod/antarc1.htm'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = 3e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions = {};
% end of function
