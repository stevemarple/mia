function r = rio_mol_1
%RIO_MOL_1  RIOMETER object for Molodezhnaya, Antarctica.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'mol', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', '', ...
    'location', location('Molodezhnaya', 'Antarctica', ...
                         -67.660000, 45.850000), ...
    'frequency', []);

