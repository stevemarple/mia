function r = rio_drao_1
%RIO_DRAO_1  RIOMETER object for Penticton, Canada.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'drao', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'NRCAN', ...
    'location', location('Penticton', 'Canada', ...
                         49.450000, -119.600000), ...
    'frequency', 3e+07);

