function varargout = rio_s4plotdlg(action, varargin)
%S4PLOTDLG  Dialogue box for creating a (line) PLOT.
%
% 
%   See also S4PLOT, S4.


switch action
  % --------------------------------------------------------------
 case 'init'
  % mfilename('init', mia, parentFh, getdatafcn);
  
  defaults.data = [];
  defaults.getdatafcn = {}; % how to get data
  defaults.miniplots = [1 1]; % NB this is (x y)

  % % attempt to be a little smarter with the limits
  % show REAL min and max 
  % defaults.limits = getlimits(mia);
  defaults.limits = [0 1];
  
  % the initial heater_data (if heatermenu created)
  defaults.heater_data = [];
  
  [defaults unvi] = interceptprop(varargin, defaults); 
  
  mia = defaults.data; % for convenience/clarity
  defaults = rmfield(defaults, 'data');
  instrument = getinstrument(mia);
  
  if ~isa(mia, 'rio_power')
    error('mia must be riometer power');
  end

  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text
  fh = figure('Name', ['S4 plot of ' gettype(mia)], ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'HandleVisibility', 'callback', ...
	      'MenuBar', 'None');

  % Add standard menus
  helpData = {'Creating plots', ...
	      'miahelp(''/plots/creating.html'')', 'help_plots'};
  miamenu(fh, 'help', helpData);
  
  bgColour = get(fh, 'Color');
  
  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'S4 window:  ');
  textPos = uiresize(textH, 'l', 'b');
  % frameColour = get(textH, 'BackgroundColor');
  frameColour = get(fh, 'Color');

  delete(textH);
  
  % allocate space for the handles and positions. Call them gen in
  % case we choose to add other frames
  genH     = zeros(19,1);   % handles to uicontrols in general frame
  genPos   = zeros(19,4);   % positions  "                    "
  
  % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the buton widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  frameWidth = 3.9 * textPos(3);
  linesOfText = 6;
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 ...
       2*spacing(2)+linesOfText*textPos(4)+(linesOfText-1)*lineSpacing];  
  % [0 50 0 2*spacing(2)+5*textPos(4)+4*lineSpacing]; 
  
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
  % -----------------------------
  % General frame
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', frameColour, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'General' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'S4 Plot', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');

  % 'Plots' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Plots: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % rowbox = 4, 'rows' = 5, colbox = 6, 'columns' = 7
  % Make the row and column text plural. Call s4plotdlg('miniplots') to
  % print the correct plural/singular version depending on the value of
  % defaults.miniplots. This way will ensure sufficient room is left
  % for the longest string.
  genH(4) = numberbox(fh, [0 0 1 1], '%d', ...
		      'n=round(min(max(1,n),30));', defaults.miniplots(2));
  set(genH(4), 'Tag', 'rowbox');
  appendcallback(genH(4), [mfilename '(''miniplots'',''r'');']);
  
  genH(5) =  uicontrol(fh, 'Style', 'text', ...
		       'String', ' rows, ', ...
		       'HorizontalAlignment', 'left', ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive', ...
		       'Tag', 'rows_text');
  
  genH(6) = numberbox(fh, [0 0 1 1], '%d', ...
		      'n=round(min(max(1,n),30));', defaults.miniplots(1));
  set(genH(6), 'Tag', 'colbox');
  appendcallback(genH(6), [mfilename '(''miniplots'',''c'');']);
  
  genH(7) = uicontrol(fh, 'Style', 'text', ...
		      'String', ' columns.', ...
		      'HorizontalAlignment', 'left', ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive', ...
		      'Tag', 'columns_text');
  
  genPos([4:7],:) = uiresize(genH([4:7]), 'l', 'b', {'999'; []; '999'; []}); 

  % x coord of RH column in figure
  % textPos(3);
  rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;
  

  % correct the left,bottom parts of the position vectors
  genPos(4:7,2) = genPos(3,2);
  genPos(4,1) = rhCol;
  genPos(5,1) = genPos(4,1) + genPos(4,3);
  genPos(6,1) = genPos(5,1) + genPos(5,3);
  genPos(7,1) = genPos(6,1) + genPos(6,3);
  % move all together
  set(genH(4:7), {'Position'}, num2cell(genPos(4:7,:),2));

  % 'Beams' = 8
  genPos(8,:) = [genPos(3,1), genPos(3,2)-textPos(4)-lineSpacing, ...
		 textPos(3) textPos(4)];
  genH(8) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Beams: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(8,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % beambox = 9
  genPos(9,:) = [rhCol genPos(8,2) 1 1]; % beambox sets it w & h
					 % need to know the numbers of the beams for this station
					 [imagingBeams wideBeams badBeams] = info(instrument, 'beams');
					 beams = getbeams(mia); % beams in this data
					 imagingAllow = intersect(imagingBeams, beams);
					 wideAllow    = intersect(wideBeams, beams);
					 
					 % We wish to update the limits box when the beams have changed. We can
					 % use the BEAMBOX class appendcallback function to add our callback
					 % function, but GCBF will not return a handle to this figure when the
					 % 'map' button is pressed. Printing the figure handle into the string is
					 % VERY, VERY BAD because the handle may be a float. To overcome this
					 % store the handle to this figure in the BEAMBOX userdata area (ie
					 % inside class 'userdata' area), and retrieve it in the callback.
					 % gcbo will return a handle to an object created by beambox
					 
					 genH(9) = beambox(fh, genPos(9,:), ...
							   'imaging', imagingBeams, ...
							   'imagingallow', imagingAllow, ...
							   'widebeam', wideBeams, ...
							   'widebeamallow', wideAllow, ...
							   'badbeams', badBeams, ...
							   'title', [' to plot']);
					 
					 setuserdata(getbeambox(genH(9)), fh);
					 appendcallback(getbeambox(genH(9)), ...
							[mfilename '(''limits'',''u'',getuserdata(getbeambox(gcbo)));']);
					 % [mfilename '(''beams'',getbeambox(gcbo));']);
					 if length(beams) == 1
					   % no choice of beams, preselect for convenience
					   setbeams(getbeambox(genH(9)), beams);
					 end
					 
					 
					 % 'S4 window'
					 genPos(15,:) = [genPos(8,1), genPos(8,2)-textPos(4)-lineSpacing, ...
							 textPos(3) textPos(4)];
					 genH(15) = uicontrol(fh, 'Style', 'text', ...
							      'String', 'S4 window: ', ...
							      'HorizontalAlignment', 'right', ...
							      'Position', genPos(15,:), ...
							      'BackgroundColor', frameColour, ...
							      'Enable', 'inactive');

					 genPos(16,:) = [rhCol genPos(15,2) 1 1]; % timespanbox sets its w & h
					 genH(16) = timespanbox(fh, genPos(16,:), 5, timespan(1,'s'));
					 
					 % 'Limit' = 10, menu = 11, min box = 12
					 % genPos(10:11,:) = genPos(8:9,:);
					 % genPos(10:11,2) = genPos(8,2) - textPos(4) - lineSpacing;
					 genPos(10:11,:) = genPos(15:16,:);
					 genPos(10:11,2) = genPos(15,2) - textPos(4) - lineSpacing;

					 % uiresize called later
					 genPos(12,:) = [rhCol, ...
							 genPos(10,2) - textPos(4) - lineSpacing, 1, 1];
					 
					 
					 genH(10) = uicontrol(fh, 'Style', 'text', ...
							      'String', 'Limit: ', ...
							      'HorizontalAlignment', 'right', ...
							      'Position', genPos(10,:), ...
							      'BackgroundColor', frameColour, ...
							      'Enable', 'inactive');
					 genH(11) = uicontrol(fh, 'Style', 'popupmenu', ...
							      'HorizontalAlignment', 'center', ...
							      'String', {'Automatic' 'Fixed'}, ...
							      'Position', genPos(11,:), ...
							      'Callback', [mfilename '(''limits'',''m'');'], ...
							      'Tag', 'limitsmenu');
					 genH(12) = numberbox(fh, genPos(12,:), '%2.2f', ...
							      [], defaults.limits(1));
					 
					 genPos(11:12,:) = uiresize(genH(11:12), 'l', 'b', {[]; '-999.9'});
					 set(genH(12), 'Tag', 'minlimitsbox');
					 appendcallback(genH(12), [mfilename '(''limits'',''b'');']);

					 % ' to ' = 13
					 genPos(13,:) = [genPos(12,1)+genPos(12,3), ...
							 genPos(10,2) - textPos(4) - lineSpacing, 1, 1];
					 genH(13) = uicontrol(fh, 'Style', 'text', ...
							      'String', ' to ', ...
							      'HorizontalAlignment', 'left', ...
							      'Position', genPos(13,:), ...
							      'BackgroundColor', frameColour, ...
							      'Enable', 'inactive');
					 genPos(13,:) = uiresize(genH(13), 'l', 'b');
					 
					 % max box = 14. 
					 genPos(14,:) = genPos(12,:); % Make same size as min box.
					 genPos(14,1) = genPos(13,1) + genPos(13,3); % shift right
					 genH(14) = numberbox(fh, genPos(14,:), '%2.2f', ...
							      [], defaults.limits(2));
					 set(genH(14), 'Tag', 'maxlimitsbox');
					 appendcallback(genH(12), [mfilename '(''limits'',''b'');']);
					 
					 % For now turn off limits
					 set(genH([11 12 14]), 'Enable', 'off');
					 
					 % make frame the right size. Bottom left of a bounding box is the
					 % bottom left of the frame.
					 bb = boundingbox(get(fh, 'Children'));
					 genPos(1,3:4) = bb(3:4) + spacing;
					 figPos(3:4) = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
					 set([genH(1) fh], {'Position'}, {genPos(1,:); figPos});
					 
					 okPos = [10 10 1 1];
					 [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'')'], ...
								  [mfilename '(''cancel'')']);
					 
					 % print the right sinuglar / plural strings.
					 feval(mfilename, 'miniplots', 'u', fh);
					 
					 % store some information in UserData. Put as a struct so it could
					 % be shared later
					 ud = get(fh, 'UserData');
					 ud.tsH = genH(16);
					 
					 % ud.mia = mia;
					 ud.defaults = defaults;
					 set(fh, 'UserData', ud, ...
						 'Visible', 'on');
					 varargout{1} = fh;
					 return;

					 % --------------------------------------------------------------
 case 'ok'
  fh = gcbf;
  % Check beams. For case with just one window we can print all into
  % one window. Otherwise we must not have more beams than plot
  % windows. 
  r = getvalue(getnumberbox(findobj(fh, 'Tag', 'rowbox')));
  c = getvalue(getnumberbox(findobj(fh, 'Tag', 'colbox')));
  rc = r * c;
  beams = getbeams(getbeambox(findobj(fh, 'Tag', 'mapbutton')));
  
  numOfBeams = prod(size(beams));

  if isempty(beams)
    errordlg('Please select at least one beam', 'Error', 'modal'); 
    return;
  elseif rc ~= 1 & length(beams) > rc
    errordlg([int2str(numOfBeams) ' beams requested but only ' ...
	      int2str(rc) ' plot windows'], 'Error', 'modal');
    return;
  end
  if get(findobj(fh, 'Tag', 'limitsmenu'), 'Value') ~= 1
    % set limits (manual)
    limits = [ ...
	getvalue(getnumberbox(findobj(fh, 'Tag', 'minlimitsbox'))) ...
	getvalue(getnumberbox(findobj(fh, 'Tag', 'maxlimitsbox'))) ...
	     ];
  else
    % automatic
    limits = []; % use default (automatic)
  end
  ud = get(fh, 'UserData');

  mia = feval(ud.defaults.getdatafcn{:});
  miaRes = getresolution(mia);
  res = gettimespan(gettimespanbox(ud.tsH));

  numOfSamples = res / miaRes;
  if numOfSamples < 1
    errordlg(['S4 window resolution must be larger than the data ' ...
	      'resolution'], 'Error', 'modal');
    return;
  elseif floor(numOfSamples) ~= ceil(numOfSamples)
    errordlg(['The S4 resolution must be an integer factor larger ' ...
	      'than the resolution of the data'], 'Error', 'modal');
    return;
  end
  
  % [newFh gh] = feval(plotFunc, mia, 'miniplots', [c r], ...
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  [newFh gh] = s4plot(mia, res, ...
		      'miniplots', [c r], ...
		      'beams', beams, ...
		      'limits', limits, ...
		      'heater_data', ud.defaults.heater_data);
  
  if nargout > 0
    varargout{1} = fh;
  end
  if nargout > 1
    varargout{2} = gh;
  end
  set(fh, 'Pointer', pointer);
  viewmenu(fh, newFh);
  close(fh);
  
  % --------------------------------------------------------------
 case 'cancel'
  varargout = cell(1,nargout);
  close(gcbf);
  
  % --------------------------------------------------------------
 case 'limits'
  % plotdlg('limits','m'): called by menu
  % plotdlg('limits','b'): called by numberbox
  % plotdlg('limits','u'): 
  % plotdlg('limits','u', fh): 
  % called by neither - but update if on automatic (used when start or
  % end samples are changed). If the callback is not from within the
  % figure in question set fh! 
  switch nargin
   case 2
    fh = gcbf;
   case 3
    fh = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  minlbh = findobj(fh, 'Tag', 'minlimitsbox');
  maxlbh = findobj(fh, 'Tag', 'maxlimitsbox');
  % who called us
  switch varargin{1} 
   case 'm'
    % menu
    lmh = gcbo;
    
    % find out which way it has been set. For manual do nothing.
    if get(lmh, 'Value') == 1
      % set to automatic, need to recompute the limits since the
      % start and end sample may have changed
      ud = get(fh, 'UserData');
      beams = getbeams(getbeambox(findobj(fh, 'Tag', 'mapbutton')));
      if ~isempty(beams)
	% cannot be smart if we don't know what beams to plot!
	% tmp = getlimits(ud.mia, beams);
	% tmp = getlimits(feval(ud.defaults.getdatafcn{:}), beams);
	data = feval(ud.defaults.getdatafcn{:});
	tmp = getlimits(data, getparameterindex(data, beams));

	setvalue(getnumberbox(minlbh), tmp(1));
	setvalue(getnumberbox(maxlbh), tmp(2));
      end
    end
    
   case 'b' 
    % box
    % min or max numberbox, so set menu to manual
    lmh = findobj(fh, 'Tag', 'limitsmenu');
    set(lmh, 'Value', 2);
    
   case 'u'
    % update
    % called from elsewhere - do an update
    lmh = findobj(fh, 'Tag', 'limitsmenu');
    if get(lmh, 'Value') == 1
      % automatic
      ud = get(fh, 'UserData');
      beams = getbeams(getbeambox(findobj(fh, 'Tag', 'mapbutton')));
      if ~isempty(beams)
	% tmp = getlimits(feval(ud.defaults.getdatafcn{:}), beams);
	data = feval(ud.defaults.getdatafcn{:});
	tmp = getlimits(data, getparameterindex(data, beams));

	setvalue(getnumberbox(minlbh), tmp(1));
	setvalue(getnumberbox(maxlbh), tmp(2));
      end
    end
    
   otherwise
    error('unknown uicontrol'); % oops
  end
  
  % --------------------------------------------------------------
 case 'miniplots'
  % plotdlg('miniplots', 'r'): row changed
  % plotdlg('miniplots', 'c'): column changed   
  % plotdlg('miniplots', 'u', fh): general update/initialisation
  % h: figure handle

  switch(varargin{1})
   case 'r'
    fh = gcbf;
    val = getvalue(getnumberbox(gcbo));
    if val > 1
      str = ' rows, ';
    else
      str = ' row, ';
    end
    set(findobj(fh, 'Tag', 'rows_text'), 'String', str);
    
   case 'c'
    fh = gcbf;
    val = getvalue(getnumberbox(gcbo));
    if val > 1
      str = ' columns.';
    else
      str = ' column. ';
    end
    set(findobj(fh, 'Tag', 'columns_text'), 'String', str);
    
   case 'u'
    fh = varargin{2};
    rVal = getvalue(getnumberbox(findobj(fh, 'Tag', ...
					 'rowbox')));
    cVal = getvalue(getnumberbox(findobj(fh, 'Tag', ...
					 'colbox')));
    if rVal > 1
      str = ' rows, ';
    else
      str = ' row, ';
    end
    set(findobj(fh, 'Tag', 'rows_text'), 'String', str);

    if cVal > 1
      str = ' columns.';
    else
      str = ' column, ';
    end
    set(findobj(fh, 'Tag', 'columns_text'), 'String', str);
    
   otherwise
    error('unknown box');
  end

  % --------------------------------------------------------------
 otherwise
  error('unknown action');
  
end


