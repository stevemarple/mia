function r = info_rio_blo_1_data
%INFO_RIO_BLO_1_DATA Return basic information about rio_blo_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_blo_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=blo;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'blo';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 128;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 41.9;
r.location1 = 'Bear Lake, Utah';
r.location1_ascii = 'Bear Lake, Utah';
r.location2 = 'USA';
r.logo = '';
r.logurl = '';
r.longitude = -111.4;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = [];
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([2005 08 19 23 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.spacenv.com/~rice/riometer/';
r.url2 = {'http://www.spacenv.com/~rice/riometer/'};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 2.55e+07;
r.widebeams = [];
r.zenith = [0];
r.defaultfilename = [];
r.institutions = {};
% end of function
