function r = data_location_rio_canopus(mia, t)

if ~isa(mia, 'rio_abs')
  error('only know absorption data locations');
end

rio = getinstrument(mia);
if ~isa(rio, 'riometer')
  error('instrument must be a riometer');
end

abr = lower(getabbreviation(rio));
switch abr
 case 'fchu'
  abr = 'chu';
 
 case 'fsim'
  abr = 'sim';
 
 case 'fsmi'
  abr = 'smi';
 
 case {'back', 'cont', 'daws', 'eski', 'gill', 'isll', 'mcmu', 'pina', ...
       'rabb', 'rank', 'talo'}
  abr = abr(1:3);
 
 otherwise
  error(sprintf('do not know the 3 letter abbreviation for %s', ...
		getcode(rio, 1)));
  
end


if t > timestamp('2005-06-01')
  % NORSTAR
   fstr = ['ftp://aurora.phys.ucalgary.ca/data/riometer/txt/' ...
	   '%Y/%m/%d/' abr '_norstar_rio_%Y%m%d_v0.txt'];
else
  % CANOPUS
  fstr = ['ftp://aurora.phys.ucalgary.ca/data/riometer/txt/' ...
	  '%Y/%m/%d/' abr '_rio_%Y%m%d_v1a.txt'];
end

r = strftime(t, fstr);
