function r = rio_asuka
%RIO_ASUKA  RIOMETER object for Asuka.

% http://iacg.org/iacg/ground_stations/agonet.html
r = riometer('name', '', ...
	     'abbreviation', 'ask', ...
	     'location', location('Asuka', 'Antarctica', -71.53, 335.86), ...
	     'frequency', nan);


