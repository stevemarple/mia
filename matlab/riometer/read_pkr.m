function r = read_pkr(filename, want_rio_data)

fid = url_fopen(filename, 'r', 'ieee-le')
fseek(fid, 0, 'eof');
file_size = ftell(fid);
fseek(fid, 0, 'bof');

header_size = 566;

records = ceil(file_size / header_size)

r.status = repmat(nan, 1, records);

r.beam_scan_rate = repmat(nan, 1, records);
r.rx_frequency = repmat(nan, 1, records);
r.bandwidth = repmat(nan, 1, records);
r.rx_time_constant = repmat(nan, 1, records);
r.cal_interval = repmat(nan, 1, records);
r.agc_interval = repmat(nan, 1, records);
r.leap_second = repmat(nan, 1, records);
r.frame_number = repmat(nan, 1, records);
r.gps_time = repmat(timestamp('bad'), 1, records);
% r.pc_time = repmat(timestamp('bad'), 1, records);
% r.gps_status = repmat(nan, 1, records);

% allocate space for datevecs instead of timestamps. Convert all to
% timestamps afterwards to save time.
gps_datevec = repmat(nan, records, 6);
pc_datevec = repmat(nan, records, 6);


% dummy byte

r.temperature = repmat(nan, 4, records);


r.hdz = repmat(nan, 3, records);
if want_rio_data
  r.rio_data = repmat(nan, 256, records);
end
r


for n = 1:records
  marker = fread(fid, 1, 'int8');
  r.status(n) = fread(fid, 1, 'uint8');
  
  r.beam_scan_rate(n) = fread(fid, 1, 'uint16');
  
  % r.rx_frequency(n) = fread(fid, 1, 'uint32');
  r.rx_frequency(n) = fread(fid, 1, 'float32');

  r.bandwidth(n) = fread(fid, 1, 'uint16');
  r.rx_time_constant(n) = fread(fid, 1, 'uint16');
  r.cal_interval(n) = fread(fid, 1, 'uint16');
  r.agc_interval(n) = fread(fid, 1, 'uint16');
  r.leap_second(n) = fread(fid, 1, 'uint16');
  
  r.frame_number(n) = fread(fid, 1, 'uint32');
  
  gps_ymdhms = fread(fid, 6, 'uint8')';
  gps_subsec = fread(fid, 2, 'uint8');
  if gps_ymdhms(1) < 80
    gps_ymdhms(1) = gps_ymdhms(1) + 2000;
  else
    gps_ymdhms(1) = gps_ymdhms(1) + 1900;
  end
  gps_ymdhms(6) = gps_ymdhms(6) + [0.100 0.010] * gps_subsec;

  pc_ymdhms = fread(fid, 6, 'uint8')';
  pc_subsec = fread(fid, 2, 'uint8');
  if pc_ymdhms(1) < 80
    pc_ymdhms(1) = pc_ymdhms(1) + 2000;
  else
    pc_ymdhms(1) = pc_ymdhms(1) + 1900;
  end
  pc_ymdhms(6) = pc_ymdhms(6) + [0.100 0.010] * pc_subsec;
  
  % r.gps_time(n) = timestamp(gps_ymdhms);
  % r.pc_time(n) = timestamp(pc_ymdhms);
  gps_datevec(n, :) = gps_ymdhms;
  pc_datevec(n, :) = pc_ymdhms;
  
  r.gps_status(n) = fread(fid, 1, 'uint8');
  dummy = fread(fid, 1, 'uint8');
  
  r.temperature(:, n) = fread(fid, 4, 'uint16');
  r.hdz(:, n) = fread(fid, 3, 'uint16');
  
  if want_rio_data
    r.rio_data(:, n) = fread(fid, 256, 'uint16');
  else
    % riometer data not wanted, skip past it
    fseek(fid, 'cof', 512);
  end
  
end
  
r.gps_time = timestamp(gps_datevec)';
r.pc_time = timestamp(pc_datevec)';
  
fclose(fid);
