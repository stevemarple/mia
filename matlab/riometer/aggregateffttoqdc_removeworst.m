function [r, cont] = aggregateffttoqdc_removeworst(binned, fitteddata, aggdata, iterations, varargin)


defaults.ignorefirstrun = true;

% terminate conditions
defaults.terminate = 'never';
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

r = binned;
cont = true;

qdcsamples = size(binned, 2);

if iterations == 1
  if defaults.ignorefirstrun
    return
  else
    % On the first run fitteddata is empty, so use the mean instead
    fitteddata = nonanmean(binned, 1);
  end
end

% Enlarge to match size of binned data
fitteddata = repmat(fitteddata, size(binned, 1), 1);

% Find difference
dif = abs(fitteddata - binned);

% Find the largest value in each column
[tmp idx] = max(dif, [],  1);

% Set largest value to NaN

for n = 1:qdcsamples
  numnotnan = numel(find(~isnan(r(:, n))));
  if numnotnan > 1
    % Set to NaN only if some other values are not nan
    r(idx(n), n) = nan;
  else
    cont = false;
  end
end

switch defaults.terminate
 case 'never'
  cont = true;
  
 otherwise
  error(sprintf('unknown terminate condition (was ''%s'')', ...
		defaults.terminate));
end
