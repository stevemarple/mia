function r = info_rio_hus_2_data
%INFO_RIO_HUS_2_DATA Return basic information about rio_hus_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_hus_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=hus;serialnumber=2

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'hus';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 88;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = [];
r.location1 = 'Husafell';
r.location1_ascii = '';
r.location2 = 'Iceland';
r.logo = '';
r.logurl = '';
r.longitude = [];
r.modified = timestamp([2010 09 02 14 46 10.762115]);
r.name = '';
r.piid = 17;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'National Institute for Polar Research';
% end of function
