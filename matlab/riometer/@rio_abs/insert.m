function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  Insert RIO_ABS object into another RIO_ABS object.
%
%   r = INSERT(a, b)
%
%   If times do not overlap NAN is inserted.

[r,aa,ar,ba,br,rs] = rio_base_insert(a, b);


switch a.obliquity
 case {'none' 'simple'}
  % one obliquity value per beam
  r.obliquityfactors = zeros([rs.datasize(1) 1]);

  of_ar.type = '()';
  of_ar.subs = {ar.subs{1} 1};
  of_aa.type = '()';
  of_aa.subs = {aa.subs{1} 1};
  
  of_br.type = '()';
  of_br.subs = {br.subs{1} 1};
  of_ba.type = '()';
  of_ba.subs = {ba.subs{1} 1};
  
  
 case 'effective'
  % one obliquity value per beam per sample
  r.obliquityfactors = zeros(rs.datasize([1 end]));
  of_aa = aa;
  of_ar = ar;
  of_ba = ba;
  of_br = br;
  
 case ''
  % obliquity isn't set but insertcheck should have ensured that the
  % obliquityfactors are both empty
  of_ar.type = '()';
  of_ar.subs = {};
  of_aa.type = '()';
  of_aa.subs = {};
  
  of_br.type = '()';
  of_br.subs = {};
  of_ba.type = '()';
  of_ba.subs = {};
  
 otherwise
  stack = dbstack; % get full name of current function
  error(sprintf(['do not know how to insert riometer absorption for ' ...
		 'obliquity method %s, please fix %s'], ...
		a.obliquity, stack(1).name));
end

% copy a
r.obliquityfactors = feval(rs.subsasgn, r.obliquityfactors, of_aa, ...
			   feval(rs.subsref, a.obliquityfactors, of_ar));

% copy b
r.obliquityfactors = feval(rs.subsasgn, r.obliquityfactors, of_ba, ...
			   feval(rs.subsref, b.obliquityfactors, of_br));

r.obliquity = a.obliquity;


