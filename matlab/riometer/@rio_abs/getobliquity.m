function [r, f] = getobliquity(mia)
%GETOBLIQUITY  Return the obliquity style and factors for absorption data.
%
%   [r f] = GETOBLIQUITY(mia);
%   r: obliquity style (CHAR)
%   f: obliquity factors
%   mia: RIO_ABS object
%
%   Note that when the obliquity factors are a function of the absorption
%   (as is the case for 'effective') they are not normally stored, as they
%   would occupy the same amount of memory as the actual data. This has
%   implications when calling SETOBLIQUITY, once absorption data has been
%   converted to use effective obliquity factors it cannot be set to any
%   other method.
%
%   See also SETOBLIQUITY, RIO_ABS, CALCOBLIQUITY, RIOMETER.

if length(mia) == 1
  r = mia.obliquity;
  f = mia.obliquityfactors;
else
  r = cell(size(mia));
  f = cell(size(mia));
  for n = 1:prod(size(mia));
    r{n} = mia(n).obliquity;
    f{n} = mia(n).obliquityfactors;
  end
end
