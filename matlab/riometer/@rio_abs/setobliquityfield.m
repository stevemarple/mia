function r = setobliquityfield(mia, obl, varargin)
%SETOBLIQUITY  Set the obliquity style, and optionally obliquity factors.
%
%   r = SETOBLIQUITYFIELD(mia, obl)
%   r = SETOBLIQUITYFIELD(mia, obl, fac)
%
%   r: RIO_ABS object
%   mia: RIO_ABS object
%   obl: obliquity style
%   fac: obliquity factors
%
%   See also SETOBLIQUITYFIELD, GETOBLIQUITY, RIO_ABS, CALCOBLIQUITY, RIOMETER.

r = mia;
for n = 1:numel(r)
  r(n).obliquity = obl;
  if length(varargin)
    r(n).obliquityfactors = varargin{1};
  end
end

