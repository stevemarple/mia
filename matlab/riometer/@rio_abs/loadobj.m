function r = loadobj(a)
%LOADOBJ  Load filter for RIO_ABS object.
%

if isstruct(a)
  % r = repmat(rio_abs, size(a));
  % for n = 1:prod(size(a))
   %  r(n) = rio_abs(a(n));
  % end
  r = rio_abs(a);
else
  r = a;
end

