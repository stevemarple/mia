function [fh, gh, cbh] = keogram(mia, varargin)
%KEOGRAM  Display a set of RIO_ABS objects as a keogram.
%
%   [fh, gh, cbh] = KEOGRAM(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle
%   cbh: colorbar handle
%   mia: RIO_ABS objects
%
% KEOGRAM requires at least 2 RIO_ABS objects to produce a keogram. For a
% latitudinal keogram no adjustment is made for longitudinal variations (and
% vice versa). Unless the frequency is specified (see below) the datasets
% are all adjusted to the operating frequency used for the first
% dataset. Start/end times are determined by the common interval of all
% datasets. If the datasets contain multiple data then only one beam is
% selected, as given by the 'preferredbeam' request to INFO (see
% riometer/INFO2).
%
% The following parameter name/values pairs may be supplied to modify the
% standard behaviour:
%
%   'clim', [1x2 DOUBLE]
%   Color limits for keogram. Use NAN for autoscaled upper/lower
%   limit(s).
%
%   'direction, ['x' | 'y']
%   Direction in which keogram should vary. Defaults to 'y' to produce
%   latitudinal keograms.
%
%   'endtime', TIMESTAMP
%   End time of the plot. If not specified defaults to the end of the
%   interval for which common data exists. Must not be outside of the
%   common interval.
%
%   'frequency', DOUBLE
%   Effective operating frequency (in Hertz).
%
%   'height', DOBULE
%   Mapping height (in metres) used for the beam projections.
%
%   'method', ['surface' | 'surfaceimage']
%   Method used for plotting the keogram.
%
%   'plotaxes', AXES handle
%   Handle used for AXES.
%
%   'resolution', TIMESPAN
%   Resolution of the plots. If not specified defaults to the resolution
%   of the first dataset.
%
%   'resolutionmethod', CHAR
%   Method used when altering the resolution. See SETRESOLUTION.
%
%   'shading' ['flat' | 'interp']
%   Shading method used.
%
%   'starttime', TIMESTAMP
%   Start time of the plot. If not specified defaults to the start of the
%   interval for which common data exists. Must not be outside of the
%   common interval.
% 
%   'title', CHAR
%   Plot title.
%
%   'units', CHAR
%   Coordinate units. Valid units include 'deg' and 'aacgm'.
%
%   'visible ['on' | 'off']
%   Figure visibility property. Defaults to 'on'.
%
%   'windowtitle', CHAR
%   Window title.
%
% Unknown parameters are passed to MAKEPLOTFIG.
%
% See also mia_image_base/KEOGRAM, RIO_ABS, MAKEPLOTFIG and INFO.

if numel(mia) < 2
  error('require a least 2 datasets');
end

% find the shortest common inverval
st = min(getstarttime(mia));
et = max(getendtime(mia));
if st >= et
  error('no common interval');
end

meant = mean([st et]);

in1 = getinstrument(mia(1));

defaults.frequency = getfrequency(in1);
defaults.height = info(in1, 'defaultheight');
defaults.miniplots = [1 1];
defaults.units = 'deg';
defaults.direction = 'y';
defaults.resolution = getresolution(mia(1));
defaults.resolutionmethod = '';
defaults.starttime = [];
defaults.endtime = [];

defaults.method = ''; % method used to plot a surface/image
defaults.shading = 'flat';
defaults.visible = 'on';
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.clim = [0 nan]; % autoscale


[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(defaults.starttime) & ~isempty(defaults.endtime) ...
      & defaults.starttime >= defaults.endtime
  % only give error if both specified (wrongly)
  error('starttime must be before endtime');
end
if isempty(defaults.starttime)
  defaults.starttime = st;
end
if isempty(defaults.endtime)
  defaults.endtime = et;
end
  
  
if defaults.starttime < st 
  disp(['requested start time before common interval, requested start ' ...
	'time ignored']);
elseif defaults.starttime >= et
  disp(['requested start time after common interval, requested start ' ...
	'time ignored']);
else
  st = defaults.starttime;
end
if defaults.endtime > et
  disp(['requested end time after common interval, requested end ' ...
	'time ignored']);
elseif defaults.endtime <= st
  disp(['requested end time before common interval, requested end ' ...
	'time ignored']);
else
  et = defaults.endtime;
end




% extract the preferred beam from each dataset and map to the desired
% frequency
numMia = numel(mia);

mia2 = repmat(feval(class(mia)), [1 numMia]);
data = zeros(numMia, (et - st) / defaults.resolution);

pos = zeros(1, numMia);
abbrev = cell(1, numMia);
% loc = repmat(feval(class(mia)), [1 numMia]);


timing = '';
for n = 1:numMia
  in = getinstrument(mia(n));
  abbrev{n} = upper(getabbreviation(in));

  prefbeam = info(in, 'preferredbeam', ...
		  'beams', getbeams(mia(n)), ...
		  'time', meant);
  tmp = extract(mia(n), ...
		'beams', prefbeam, ...
		'starttime', st, ...
		'endtime', et);
  tmp = setresolution(tmp, defaults.resolution, defaults.resolutionmethod);
  tmp = setfrequency(tmp, defaults.frequency);
  
  % warn about known timing problems (centred/offset)
  tmptiming = gettiming(tmp);
  if isempty(timing)
    timing = tmptiming; % remember first known method
  elseif ~isempty(tmptiming) & ~strcmp(timing, tmptiming)
    % have offset and centred means. Give warning but carry on regardless
    warning('timing methods do not agree (ignored)');
  end

  [x y] = info(in, 'beamlocations', ...
	       'beams', prefbeam, ...
	       'height', defaults.height, ...
	       'units', defaults.units, ...
	       'year', getyear(mean([st et])));
  
  switch defaults.direction
   case {'x' 'X'}
    pos(n) = x;
    latlon = 'longitude';
    
   case {'y' 'Y'}
    pos(n) = y;
    latlon = 'latitude';

   otherwise
    error('unknown value for direction');
  end

  data(n, :) = getdata(tmp);
end


[sortpos idx] = sort(pos);
% reorder data according to beamlocations in given coordinate system
data = data(idx, :);
abbrev = abbrev(idx);
mia2 = mia2(idx);


switch timing
 case 'centred'
  xdata = 0:(size(data,2)-1);
  
 case {'offset' ''}
  xdata = 0.5:size(data,2);
  
 otherwise
  error(sprintf('unknown type for timing (was ''%s'')', timing));
end

ydata = sortpos;


if ~isempty(defaults.plotaxes)
  % defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes(1);
  fh = get(gh, 'Parent');
else
  gh = [];
end


locStr = [abbrev{1} sprintf(' %s', abbrev{2:end})];
[plottitle windowtitle] = ...
    maketitle(tmp, ...
	      'style', 'image', ...
	      'customstring', [gettype(mia, 'c'), ' keogram'], ...
	      'location', locStr, ...
	      'starttime', st, ...
	      'endtime', et);

if isempty(defaults.title)
  defaults.title = plottitle;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end


if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', varargin{unvi}, ...
			'miniplots', defaults.miniplots, ...
			'title', defaults.title, ...
			'visible', 'off', ...
			'footer', 'on');
  set(fh, 'Name', defaults.windowtitle);   % Write name into figures title bar 
  makeplotfig('addshading', fh);  % Add pixel shading options to menus

end



% choose a method. No point in allowing images since the spacing is
% extremely unlikely to be the same
if isempty(defaults.method)
  if length(xdata) * length(pos) < 1000
    defaults.method = 'surfaceimage';
  else
    defaults.method = 'surface';
  end
end


switch defaults.method
 case 'surface'
  surface('Parent', gh(1), ...
	  'XData', xdata, ...
	  'YData', ydata, ...
	  'ZData', data, ...
	  'CData', data);
  shading(defaults.shading);
  view(2);
  
 case 'surfaceimage'
  [xMesh yMesh] = meshgrid(xdata, ydata);
  surfaceimage(xMesh, yMesh, data, data, ...
               'parent', gh(1));
  
 otherwise
  error(sprintf('unknown method (was ''%s'')', defaults.method));
end

switch defaults.units
 case 'deg'
  % ylab = ['Geographic ' latlon ' (' i18n_entity('deg') ')'];
  ylab = ['Geographic ' latlon ' (deg)'];
  
 case {'aacgm' 'cgm'}
  % ylab = ['Geomagnetic ' latlon '(' upper(defaults.units) ', ' ...
  %   i18n_entity('deg') ')'];
  ylab = ['Geomagnetic ' latlon '(' upper(defaults.units) ', deg)'];
 otherwise
  % hint to fix this function
  warning('unknown coordinate system, cannot label Y axis');
  ylab = '';
end


clim = get(gh(1), 'CLim');
defaults.clim(isnan(defaults.clim)) = clim(isnan(defaults.clim));

set(gh, ...
    'XLim', [0, size(data, 2)], ...
    'YLim', sortpos([1 end]), ...
    'CLim', defaults.clim, ...
    'ZLim', defaults.clim, ...
    'Box', 'on', ...
    'TickDir', 'out');

set(get(gh(1), 'YLabel'), ...
    defaultaxeslabelparams('String', char(ylab)));

cbh = makeplotfig('addcolorbar', fh, ...
		  'shading', defaults.shading, ...
		  'clim', defaults.clim, ...
		  'zlim', defaults.clim, ...
		  'xlim', defaults.clim, ...
		  'title', datalabel(tmp));

timetick(gh(1), 'X', st, et, defaults.resolution, 1);

set(fh, ...
    'Pointer', 'arrow', ...
    'Visible', defaults.visible);
