function r = getfrequency(mia)
%GETFREQUENCY  Return the (effective) operating frequency.
%
%   r = GETFREQUENCY(mia)
%   r: operating frequency (Hz)
%   mia: RIO_ABS object
%
%   See also riometer/GETFREQUENCY.

r = getfrequency(getinstrument(mia));
