function r = char(mia, varargin)
%CHAR  Convert a RIO_ABS object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = rio_base_char(mia, varargin{:});
  return;
end

if size(mia.obliquityfactors, 1) == 1;
  plural = ' ';
else
  plural = 's';
end
 
r = sprintf(['%s' ...
	     'obliquity         : %s\n' ...
	     'obliquity factor%s : %s\n'], ...
 	    rio_base_char(mia, varargin{:}), mia.obliquity, plural, ...
	    matrixinfo(mia.obliquityfactors));
    



