function mia = rio_abs(varargin)
%RIO_ABS  Constructor for RIO_ABS class.
%
%   r = RIO_ABS;
%   default constructor
%
%   r = RIO_ABS(mia)
%   copy constructor
%
%   r = RIO_ABS(...)
%   constructor taking parameter name/value pairs
%
%   In addition to parameters accepted by MIA_BASE the following parameters
%   are accepted:
%
%     'beams', DOUBLE vector
%     The beams for which data should be loaded/created.
%
%   Other parameters may be specified which modify the way in which data
%   is loaded or created.
%
%     'load', [boolean or CHAR]
%     A flag indicating if data should be loaded from disk (either
%     directly or by computation from QDC and power data, as defined by
%     the appropriate riometer/INFO function).
%
%     'obliquity', CHAR 
%     The obliquity factors method. See CALCOBLIQUITY for more details.
%
%     'height' DOUBLE (metres)
%     The assumed height of the infinitely thin absorbing layer. See
%     CALCOBLIQUITY for more details. 
%
%     'qdc', [RIO_QDC or TIMESTAMP]
%     The quiet day curve used is normally chosen automatically, but can be
%     overridden if a quiet day curve is given. This parameter is only used
%     when loading existing data which is computed from a quiet day curve
%     and power data. If a TIMESTAMP is supplied then the QDC corresponding
%     to that time will be used instead the normal choice.
%
%     'loadoptions', [CELL]
%     A CELL array of name/value pairs passed to LOADDATA to alter
%     load behaviour. Notable options are:
%
%         'resolution', [TIMESPAN]
%         The resolution of the data to be returned. If not set data is
%         loaded at its normal resolution, and may not be spaced
%         regularly (see SPACEREGULARLY).
%
%         'resolutionmethod', [CHAR]
%         The method to use when changing resolution. Valid options include
%         'mean', 'nonanmean', 'median' and 'nonanmedian'. See SETRESOLUTION
%         for more details.
%
%
%     'powerloadoptions', [CELL]
%     Like loadoptions a CELL array of name/value pairs, but passed to
%     LOADDATA only when loading the RIO_POWER object.
%
%     'qdcloadoptions', [CELL]
%     Like loadoptions a CELL array of name/value pairs, but passed to
%     LOADDATA only when loading the RIO_QDC object.
%
% See also MIA_BASE, RIOMETER, RIO_QDC, RIO_POWER.

% Other parameters deliberately not documented in the help section:
%
% 'log' [boolean or CHAR]
%  flag indicating if data loaded from disk should be logged. Normally it
%  should be, but a method to disable logging is required at times. (For
%  instance when loading absorption data it is not desirable to log both the
%  absorption data created and the power data used to create it.)

cls = 'rio_abs';
parent = 'rio_base';

% Make it easy to change the class definition at a later date
latestversion = 2;
mia.versionnumber = latestversion;
mia.obliquity = '';    % name of obliquity factors used see CALCOBLIQUITY
mia.obliquityfactors = []; % actual obliquity factors

defaultObliquity = 'simple';

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = rio_base;
  mia = class(mia, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};
    
elseif nargin == 1 & isstruct(varargin{1})
    % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {parent, 'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {parent});
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    tmp2 = tmp;
    an = a(n);
    for m = 1:length(fn)
      % tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
      tmp2 = setfield(tmp2, fn{m}, getfield(an, fn{m}));
    end

    base = getfield(an, parent); % parent class should already be current
    switch tmp2.versionnumber
     case 1
      tmp2.obliquity = defaultObliquity;
      tmp2.obliquityfactors = calcobliquity(getinstrument(base), ...
					   'style', tmp2.obliquity, ...
					   'beams', getbeams(base));
      if all(isnan(tmp2.obliquityfactors))
	% only set obliquity factors if real values are known
	tmp2.obliquity = '?';
	tmp2.obliquityfactors = [];
      end
      
     case 2
      ; % latest

     otherwise
      error('unknown version');
    end
    tmp2 = class(tmp2, cls, base);
    mia(n) = tmp2;
  end

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia; % take on standard settings
  defaults.load = []; % load from disk or create new
  defaults.loadoptions = {};
  defaults.powerloadoptions = {};
  defaults.qdcloadoptions = {};
  defaults.log = 1;
  
  defaults.qdc = []; % use standard qdc if empty, else this one
  defaults.height = [];
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'powerloadoptions' ...
		    'qdcloadoptions' 'log' 'qdc' 'height'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = rio_base(varargin{unvi});
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    instrument = getinstrument(mia);
    % --------
    % construct when passed multiple instruments
    if numel(instrument) > 1
      r = repmat(mia, size(instrument));
      for n = 1:numel(instrument)
	r(n) = feval(cls, varargin{:}, 'instrument', instrument(n));
      end
      mia = r;
      return
    end
    % --------
  
    if isempty(instrument)
      error('instrument must be specified');
    end
    if isempty(getbeams(mia))
      allbeams = info(instrument, 'allbeams');
      mia = setbeams(mia, allbeams);
    end
    
    % Are the files (for selected archive) on disk? Firstly need to find out
    % if any archive was specified
    loadoptions.archive = '';
    loadoptions = interceptprop(defaults.loadoptions, loadoptions);
    [df mesg] = info(instrument, 'defaultfilename', mia, ...
		     'archive', loadoptions.archive);
    
    if ~isempty(df) & isempty(defaults.qdc)
      % these files are on disk, load directly

      requestedObl = mia.obliquity; % remember requested obliquity
      createargs = {'obliquity', defaultObliquity, ...
		    'obliquityfactors', ...
		    calcobliquity(instrument, ...
				  'style', defaultObliquity, ...
				  'beams', getbeams(mia))};

      oblFactors = calcobliquity(instrument, ...
				 'style', defaultObliquity, ...
				 'beams', getbeams(mia));
      mia = loaddata(mia, ...
		     defaults.loadoptions{:}, ...
                     'createargs', ...
                     {'obliquity', defaultObliquity, ...
                      'obliquityfactors', oblFactors});
      
      % BUG: rio_base/loaddata blocks the createargs and
      % asciiloadfunc from working (to fix it would need
      % mia_base/loaddata to accept multiple function names (as
      % a CELL?). Therefore if ascii data AND obliquity is empty assume
      % it is the standard obliquity values
      if strcmp(df.format, 'ascii')
	mia = setobliquityfield(mia, defaultObliquity, oblFactors);
      end
            
      if ~isempty(requestedObl)
	% mia = setobliquity(mia, obl, 'height', defaults.height);
	mia = setobliquity(mia, requestedObl, 'height', defaults.height);
      end

      if isempty(getunits(mia))
	mia = setunits(mia, 'dB');
      end

    else
      % create from power and qdc
      beams = getbeams(mia);
      plural = '';
      if length(beams) > 1
	plural = 's';
      end
      if isempty(mia.obliquity)
	% mia.obliquity = 'standard';
	mia.obliquity = defaultObliquity;
      end
      numOfBeams = prod(size(beams));
      rp = rio_power('starttime', getstarttime(mia), ...
		     'endtime', getendtime(mia), ...
		     'instrument', instrument, ...
		     'beams', beams, ...
		     'load', 1, ...
		     'loadoptions', {defaults.loadoptions{:} ...
		    defaults.powerloadoptions{:}}, ...
		     'log', 0);
      units = '';
      mia = adddataquality(mia, getdataquality(rp));
      % disp('ASSIGNING rio_abs_pow'); assignin('base', 'rio_abs_pow', rp);
      
      if isempty(defaults.qdc)
	% Use default QDC. Since we know the rio_qdc class stores beams in
        % separate files use that to minimise memory usage by creating
        % absorption beam-by-beam.
	
	rpBi = getparameterindex(rp, beams);
	miaBi = getparameterindex(mia, beams);
	mia.obliquityfactors = repmat(nan, numOfBeams, 1);
	
	for n = 1:numOfBeams
	  % forward any load options but load QDCSs at native resolution
	  rqdc = rio_qdc('starttime', getstarttime(mia), ...
			 'endtime', getendtime(mia), ...
			 'instrument', instrument, ...
			 'beams', beams(n), ...
			 'load', 1, ...
			 'log', 0, ...
			 'loadoptions', {defaults.loadoptions{:} ...
		    defaults.qdcloadoptions{:} 'resolution', []});
	  
	  [data units proc obl oblf] = ...
	      qdcsubpower(rqdc, rp, beams(n), ...
			  'obliquity', mia.obliquity, ...
			  'height', defaults.height);
	  mia = setdata(mia, data, miaBi(n), ':');
	  mia = setsampletime(mia, getsampletime(rp));
	  mia = setintegrationtime(mia, getintegrationtime(rp));
	  if size(oblf, 2) == 1
	    mia.obliquityfactors(n) = oblf;
	  else
	    mia.obliquityfactors = []; % don't save if func of abs
	  end
	  mia = adddataquality(mia, getdataquality(rqdc));

	end
      else
	% use specified qdc
	if isa(defaults.qdc, 'rio_qdc_base')
	  ; % already have a QDC
	elseif isa(defaults.qdc, 'timestamp')
	  defaults.qdc = rio_qdc('time', defaults.qdc, ...
				 'instrument', instrument, ...
				 'beams', beams);
	else
	  error('qdc must be a QDC or a timestamp');
	end
	if ~all(ismember(beams, getbeams(defaults.qdc)))
	  error('QDC does not contain required beam(s)');
	end

	[data units proc obl oblf] = ...
	    qdcsubpower(defaults.qdc, rp, beams, ...
			'obliquity', mia.obliquity, ...
			'height', defaults.height);
	mia = setdata(mia, data);
	mia = setsampletime(mia, getsampletime(rp));
	mia = setintegrationtime(mia, getintegrationtime(rp));
	if size(oblf, 2) == 1
	  % mia.obliquityfactors(n) = oblf;
	  mia.obliquityfactors = oblf;
	else
	  mia.obliquityfactors = []; % don't save if func of abs
	end
	
	mia = addprocessing(mia, sprintf('Created from QDC: %s', ...
					 dateprintf(defaults.qdc)));
	qdcProc = getprocessing(defaults.qdc);
	if 0 & ~isempty(qdcProc)
	  mia = addprocessing(mia, 'QDC processing was:', ...
			      qdcProc, '(QDC processing ends)');
	end
	mia = adddataquality(mia, getdataquality(defaults.qdc));

      end
      mia = setunits(mia, units);
      mia = addprocessing(mia, proc);
    end
    

    if defaults.log
      mialog(mia);
    end
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
