function [r, f] = setobliquity(mia, obl, varargin)
%SETOBLIQUITY  Set the obliquity style.
%
%   r = SETOBLIQUITY(mia, obl, ...)
%   r: modified RIO_ABS object
%   mia: RIO_ABS object
%   obl: obliquity style (CHAR)
%
% Set the obliquity style to obl, and modify the absorption values
% accordingly. For the valid obliquity styles see CALCOBLIQUITY. Note that
% is is not normally possible to convert from 'effective' obliquity factors
% to any other type. If this is the case then a warning message is printed
% and the data is returned unchanged.
%
% Additional parameter name/value pairs may be specified, see CALCOBLIQUITY
% for more details.
%
% To set the obliquity field (and optionally obliquity factors) without
% changing the absorption vlaues see the SETOBLIQUITYFIELD function.
%
%   See also SETOBLIQUITYFIELD, GETOBLIQUITY, RIO_ABS, CALCOBLIQUITY, RIOMETER.

r = mia;

for n = 1:prod(size(r))
  rio = getinstrument(r(n));
  data = getdata(r(n));
  [newFactors proc] = calcobliquity(rio, varargin{:}, ...
				    'beams', getbeams(r(n)), ...
				    'absorption', data, ...
				    'style', obl);
  oldFactors = mia(n).obliquityfactors;
  
  if isempty(oldFactors) | isempty(newFactors) 
    % cannot change if don't have obliquity
    warning('cannot change obliquity style');
    mia(n)
    
  elseif ~isequal(oldFactors, newFactors)
    % factors are different, so change data
    
    for m = 1:size(data, 1)
      % size(oldFactors(m, :))
      % size(newFactors(m, :))
      data(m, :) = data(m, :) .* (oldFactors(m, :) ./ newFactors(m, :));
    end
    r(n).obliquity = obl;
    % save obliquity factors, unless a function of absorption (actually
    % factors are saved for case of just one sample period) 
    if size(newFactors, 2) > 1
      r(n).obliquityfactors = [];
    else
      r(n).obliquityfactors = newFactors;
    end
    r(n) = setdata(r(n), data);
    r(n) = addprocessing(r(n), proc);
  end
    
end
