function [varargout] = stackplot(mia, varargin)
%STACKPLOT  Override STACKPLOT for RIO_ABS objects
%
% See mia_base/STACKPLOT.

if isempty(mia)
  error('no data to plot');
end

defaults.frequency = [];
defaults.logo = '';
[defaults unvi] = interceptprop(varargin, defaults);

in = getinstrument(mia);

if isempty(defaults.frequency)
  defaults.frequency = getfrequency(in(1));
end
mia = setfrequency(mia, defaults.frequency);

if isempty(defaults.logo)
  logo = info(in, 'logo');
  if ischar(logo)
    defaults.logo = logo;
  elseif all(strcmp(logo{1}, logo(2:end)))
    defaults.logo = logo{1};
  end
end

if nargout
  narg = nargout;
else
  narg = 1;
end


[varargout{1:narg}] = mia_base_stackplot(mia, ...
					 'logo', defaults.logo, ...
					 varargin{unvi});
