function [r,sr,s] = extract(mia, varargin)
%EXTRACT Overload of EXTRACT for RIO_ABS. See rio_base/EXTRACT for details.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end


% do the normal extract
[r sr s] = rio_base_extract(mia, varargin{:});

% now ensure the obliquity factos are set correctly
[oblStyle oblFactors] = getobliquity(mia);

if ~isempty(oblFactors)
  oblFactors = oblFactors(sr.subs{1});
end

r.obliquity = oblStyle;
r.obliquityfactors = oblFactors;


