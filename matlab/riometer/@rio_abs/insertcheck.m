function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for RIO_ABS data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = rio_base_insertcheck(a, b);

if ~strcmp(a.obliquity, b.obliquity)
  error('cannot insert, obliquity methods differ');
end

if isempty(a.obliquity)
  % both must be empty, but check that the obliquityfactors are also
  % empty
  if ~isempty(a.obliquityfactors)
    a
    error('obliquity type is unset but obliquity factors are not empty');
  elseif ~isempty(b.obliquityfactors)
    b
    error('obliquity type is unset but obliquity factors are not empty');
  end
end
