function r = gettype(rio, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(rio)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_ABS, RIO_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'absorption';
  
 case 'u'
  r = 'ABSORPTION';
  
 case {'c' 'C'}
  r = 'Absorption';
 
 otherwise
  error('unknown mode');
end

return

