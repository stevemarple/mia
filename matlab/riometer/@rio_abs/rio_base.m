function r = rio_base(mia)


if length(mia) == 1
  r = mia.rio_base;
else
  r = repmat(rio_base, size(mia));
  for n = 1:prod(size(mia))
    r(n) = mia(n).rio_base;
  end
end
