function r = info_rio_sms_1_data
%INFO_RIO_SMS_1_DATA Return basic information about rio_sms_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_sms_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=sms;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'sms';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [4 4];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = 4;
r.beamplanr = 4;
r.beamwidth = [];
r.bibliography = 'art:438';
r.comment = 'Similar design to NAL, but 4x4';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = 16;
r.id = 115;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16];
r.latitude = -29.44;
r.location1 = 'Sao Martinho da Serra';
r.location1_ascii = 'Sao Martinho da Serra';
r.location2 = 'Brazil';
r.logo = '';
r.logurl = '';
r.longitude = -53.8;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = [];
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions = {};
% end of function
