function r = info_rio_ask_1_data
%INFO_RIO_ASK_1_DATA Return basic information about rio_ask_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_ask_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=ask;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'ask';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([1991 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 76;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -71.53;
r.location1 = 'Asuka';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = -24.14;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 17;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([1989 01 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'National Institute for Polar Research';
% end of function
