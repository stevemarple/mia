function r = rio_skt_1
%RIO_SKT_1  RIOMETER object for Sukkertoppen, Greenland.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'skt', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'DMI riometer chain', ...
    'location', location('Sukkertoppen', 'Greenland', ...
                         65.420000, -52.900000), ...
    'frequency', []);

