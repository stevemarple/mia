function r = rio_nrcan
%RIO_NRCAN  Return a list of the NRCAN riometers.
%
%   r = RIO_NRCAN
%   r: vector of RIOMETER objects
%
%   For more details about NRCAN riometers see
%   http://aurora.phys.ucalgary.ca/nrcan/
%
%   See also RIOMETER.

% Get list of all known riometers
c = instrumenttypeinfo(riometer,'aware');

r = c(strcmp(getfacility(c), 'NRCAN'));
