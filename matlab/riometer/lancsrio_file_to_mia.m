function mia = lancsrio_file_to_mia(filename, varargin)
%LANCSRIO_FILE_TO_MIA  Convert Lancaster riometer text file to MIA object.
%
%   mia = LANCSRIO_FILE_TO_MIA(filename)
%   mia: RIO_POWER object
%   filename: CHAR
%

defaults.archive = '';
defaults.cancelhandle = [];
defaults.mia = [];
defaults.starttime = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

mia = defaults.mia;


[fid mesg] = url_fopen(filename, 'r');
if fid == -1
  error(sprintf('could not open %s: %s', filename, mesg));
end

d = url_load(filename)';

% Ensure sample times are unique
[tnum tidx] = unique(d(1,:));

% Round sample start time down to beginning of second
t = floor(timestamp('unixtime', tnum), timespan(1, 's'));

% Match the data to the correct sample order
data = d(2:end, tidx);

% Integration time
it = repmat(timespan(1, 's'), size(t));

% Instrument
rio = getinstrument(defaults.mia);
df = info(rio, 'defaultfilename', mia, 'archive', defaults.archive);

beams = info(rio, 'allbeams');

% mia = rio_power('starttime', st, ...
% 				'endtime', et, ...
% 				'sampletime', t, ...
% 				'integrationtime', it, ...
% 				'instrument', rio, ...
% 				'beams', beams, ...
% 				'data', data, ...
% 				'units', 'dB');

mia = setsampletime(mia, t);
mia = setintegrationtime(mia, it);
mia = setdata(mia, data);
mia = setunits(mia, df.units);
processing = ''; 

if isa(mia, 'rio_power')
  [data, units] = linearise(mia);

  s = info(rio, '_struct');
  if isfield(s, 'lancsrio')
	if isfield(s.lancsrio, 'beamorder')
	  % Fix the column ordering
	  beamorder = s.lancsrio.beamorder;
	  data = data(beamorder, :);
	end
  end
  mia = setdata(mia, data);
  mia = setunits(mia, units);
  processing = 'Created from rio_rawpower data';
end



if ~isempty(processing)
  mia = addprocessing(mia, processing);
end
