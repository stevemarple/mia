function r = sgoriodata_file_to_mia(filename, varargin)
%SGORIODATA_FILE_TO_MIA  Convert SGO riometer data file to MIA object.
%
%   mia = SGORIODATA_FILE_TO_MIA(filename)
%   mia: RIO_ABS object
%   filename: CHAR
%
defaults.mia = [];
defaults.cancelhandle = [];
defaults.starttime = [];
defaults.archive = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object must be set');
end
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('instrument object must be set in mia object');
end


r = defaults.mia;

df = info(rio, 'defaultfilename', r, 'archive', defaults.archive);

if isempty(defaults.starttime)
  error('starttime must be set')
end

r = setstarttime(r, defaults.starttime);
et = defaults.starttime + df.duration;
r = setendtime(r, et);

r = setunits(r, df.units);

% ascii
tmp = url_load(filename);
if numel(tmp) ~= df.size(2)
  warning(sprintf('incorrect datasize for %s: disregarding data', filename));
  tmp = repmat(nan, df.size)';
end

r = setdata(r, feval(df.dataclass, tmp)');

samt = (defaults.starttime + df.resolution./2):df.resolution:et;
r = setsampletime(r, samt);
r = setintegrationtime(r, repmat(df.resolution, size(samt)));

