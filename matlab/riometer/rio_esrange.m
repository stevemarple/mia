function r = rio_esrange
%RIO_ESRANGE  RIOMETER object for Esrange.

% Latitude: 67deg 54' N 
% Longitude: 21deg 05' E
r = riometer('abbreviation', '', ...
	     'location', location('Esrange', 'Sweden', ...
				  67.9000, 21.0833));
