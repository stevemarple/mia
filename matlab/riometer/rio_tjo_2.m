function r = rio_tjo_2
%RIO_TJO_2  RIOMETER object for Tjornes, Iceland.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'tjo', ...
    'serialnumber', 2, ...
    'name', '', ...
    'facility', '', ...
    'location', location('Tjornes', 'Iceland', ...
                         NaN, NaN), ...
    'frequency', []);

