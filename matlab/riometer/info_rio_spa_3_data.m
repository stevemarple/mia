function r = info_rio_spa_3_data
%INFO_RIO_SPA_3_DATA Return basic information about rio_spa_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_spa_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=spa;serialnumber=3

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.pixels = [];
r.abbreviation = 'spa';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 96;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -90;
r.location1 = 'South Pole';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 0;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 28;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 3;
r.standardobliquity = [1];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = 'crossed-dipole';
r.wbeams = 1;
r.wfrequency = 3.82e+07;
r.widebeams = [1];
r.zenith = [];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'mat';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/spa_3/rio_abs/%Y/%m/%Y%m%d.mat';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [1 8640];
r.defaultfilename.rio_abs.default.units = 'dB';
r.institutions{1} = 'Siena College';
% end of function
