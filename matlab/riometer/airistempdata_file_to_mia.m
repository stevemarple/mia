function r = airistempdata_file_to_mia(filename, varargin)

defaults.cancelhandle = [];

% mia_base/loaddata passes the basic object and other details
defaults.mia = [];
defaults.starttime = [];

defaults.archive = '';


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

cls = class(defaults.mia);

instrument = getinstrument(defaults.mia);

df = info(rio_and_2, ...
	  'defaultfilename', 'temp_data', ...
	  'archive','original_format');

filename = strftime(defaults.starttime, df.fstr);
dat = url_load(filename);

sampletime = timestamp(dat(:, 1:6));
sampletime = reshape(sampletime, 1, numel(sampletime));
integrationtime = repmat(timespan(1, 's'), size(sampletime));
st = sampletime(1);
et = sampletime(end) + timespan(1, 's');
data = dat(:, 7)';

fileinfo = dir(filename);

r = feval(cls, ...
	  'starttime', st, ...
	  'endtime', et, ...
	  'sampletime', sampletime, ...
	  'integrationtime', integrationtime, ...
	  'instrument', instrument, ...
	  'creator', 'unknown', ...
	  'createtime', local_get_file_mod_time(filename), ...
	  'load', 0, ...
	  'log', 0, ...
	  'data', data, ...
	  'units', df.units, ...
	  'sensors', 1);



function r = local_get_file_mod_time(filename)
d = dir(filename);
if numel(d) ~= 1
  filename
  error('file not found or is a directory');
end

months.Jan = 1;
months.Feb = 2;
months.Mar = 3;
months.Apr = 4;
months.May = 5;
months.Jun = 6;
months.Jul = 7;
months.Aug = 8;
months.Sep = 9;
months.Oct = 10;
months.Nov = 11;
months.Dec = 12;


s = split(' ', d.date);
dmy = split('-', s{1});
hms = split(':', s{2});

year = str2num(dmy{3});
day = str2num(dmy{1});
if isfield(months, dmy{2})
  month = getfield(months, dmy{2});
else
  error(sprintf('Could not get month number for ''%s''', dmy{2}));
end

hour = str2num(hms{1});
minute = str2num(hms{2});
second = str2num(hms{3});

r = timestamp([year month day hour minute second]);
