function r = rio_capechelyuskin
%RIO_CAPECHELYUSKIN  RIOMETER object for Cape Chelyuskin. Analogue recording.
%
% Operated by Arctic and Antarctic Research Institute (AARI), Russia.


% http://www.aari.nw.ru/clgmi/geophys/station.htm
r = riometer('name', '', ...
	     'abbreviation', 'ccs', ...
	     'location', location('Cape Chelyuskin', 'Arctica', ...
				  77.7, 104.3), ...
	     'frequency', nan);


