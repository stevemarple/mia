function r = rio_base(rio)
%RIO_BASE  Extract the parent RIO_BASE object from a RIO_RAWPOWER object.
%
%   r = RIO_BASE(rio)
%   r: RIO_BASE object
%   rio: RIO_RAWPOWER object
%
%   See also RIO_BASE, RIO_RAWPOWER.

r = rio.rio_base;

