function mia = rio_rawpower(varargin)
%RIO_RAWPOWER  Constructor for RIO_RAWPOWER class.
%
%   The RIO_RAWPOWER class is intended as a low-level class which ensures
%   raw data cannot be confused with linearised data. Raw data is
%   typically stored in uint16. Missing data is indicated with 0, since a
%   received power of level of zero is also meaningless.
%
%  

cls = 'rio_rawpower';

% Make it easy to change the class definition at a later date
latestversion = 1;
mia.versionnumber = latestversion;

if nargin == 0 | (nargin == 1 && isempty(varargin{1}))
  % default constructor
  parent = rio_base;
  mia = class(mia, cls, parent);
  
elseif nargin == 1 && (isa(varargin{1}, cls) | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  parent = rio_base;
  mia = class(mia, cls, parent);

elseif rem(nargin, 2) == 0 && all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  parent = rio_base(varargin{unvi});
  mia = class(mia, cls, parent);

  if isempty(defaults.load) 
    defaults.load = isdataempty(mia);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;

