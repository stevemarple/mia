function r = rio_power(mia)
%RIO_POWER  Convert a RIO_RAWPOWER object to RIO_POWER object.
%
%   r = RIO_RAWPOWER(mia)
%   r: RIO_POWER object
%   mia: RIO_RAWPOWER object
%
%   See also RIO_RAWPOWER, RIO_POWER.

% Have to linearise

% in = getinstrument(mia);
% abbrev = getabbreviation(in);
% func = ['linearise_' abbrev];
 
% if ~exist(func)
%   func = ['linearise_' lower(getfacility(in))];
% end

% if ~exist(func)
%   error(['Cannot linearise riometer data from ' abbrev]);
% else
%   [p units] = feval(func, mia);
% end

[p units] = linearise(mia);

r = rio_power(rio_base(mia));
r = setdata(r, p);
r = setunits(r, units);
