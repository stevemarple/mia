function mia_to_lancsrio_file(mia, filename, varargin)

defaults.archive = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

d = [unixtime(getsamplestarttime(mia)); getdata(mia)];

fh = -1;
try
  url_mkdir(dirname(filename));
  fh = url_fopen(filename, 'wt');
  fmt = ['%.6f' repmat('\t%.6f', 1, numel(getbeams(mia))) '\n'];
  fprintf(fh, fmt, d);
  fclose(fh);
  fh = -1;
catch
  if fh ~= -1
	fclose(fh);
  end
  error(lasterr);
end

