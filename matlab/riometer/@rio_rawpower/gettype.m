function r = gettype(mia, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_RAWPOWER, RIO_BASE, RIOMETER.


if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'raw power';
  
 case 'u'
  r = 'RAW POWER';
  
 case 'c'
  r = 'Raw power';

 case 'C'
  r = 'Raw Power';
  
 otherwise
  error('unknown mode');
end

return


