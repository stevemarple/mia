function [r, runits, proc, obl, oblf, samt] = qdcsubpower(rq, rp, beams, varargin)
%QDCSUBPOWER  Private function.
%
%   [r, runits, proc] = QDCSUBPOWER(rq, rp, beams)
%
%   r: qdc-power, taking into account obliquity
%   runits: units for r
%   proc: processing string describing obliquity factor(s) used
%   rq: RIO_QDC object
%   rp: RIO_POWER object
%   beams: beams to use for the object
%
%   This should be considered a private function of the riometer toolbox.
%
%   See intead RIO_ABS, rio_power/RIO_ABS.

numOfBeams = numel(beams);

st = getstarttime(rp);
et = getendtime(rp);

Qbi = getparameterindex(rq, beams);
Pbi = getparameterindex(rp, beams);

instrument = getinstrument(rq);

% some sanity checking
units = getunits(rq);
if ~isequal(getunits(rp), units)
  if strcmp(info(instrument, 'qdcclass'), 'rio_rawpower')
    % linearise the QDC object
    [rqd units] = linearise(rq);
    rq = setdata(rq, linearise(rq));
    rq = setunits(rq, units);
  else
    error(sprintf(['units do not match (power units are ''%s'' and ' ...
		   'QDC units are ''%s'')'], getunits(rp), units));
  end
end


defaults.obliquity = 'simple';
defaults.height = []; % intercept and pass on to some obliquity methods

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% align QDC to same times as rp
samt = getsampletime(rp);
% rqa = align(rq, samt); 
if isdataempty(rq)
  % Handle case of missing QDC
  ds = getdatasize(rp);
  ds(1) = numel(intersect(getbeams(rp), getbeams(rq)));
  rqad = repmat(nan, ds);
else
  rqa = align(rq, rp); 
  if isdataempty(rqa)
    rqad = [];
  else
    rqad = getdata(rqa, Qbi, ':'); 
  end
end

% don't try this with rio_rawpower (ie non linearised data)
if ~isa(rp, 'rio_power')
  error(sprintf('rp is not a rio_power object (is ''%s'')', class(rp)));
end

if isdataempty(rp)
  rpd = [];
else
  rpd = getdata(rp, Pbi, ':');
end

switch units
 case {'raw' 'ADC', 'K'}
  units,rp,rq
  
  if isempty(rqad) | isempty(rpd)
    r = [];
  else
    r = 10 * log10(rqad ./ rpd);
  end
  runits = 'dB'; % linearised data, so units are dB
  
 case 'dBm'
  if isempty(rqad) | isempty(rpd)
    r = [];
  else
    r = rqad - rpd;
  end
  runits = 'dB';
  
 otherwise
  error(sprintf('unknown units (was ''%s'')', units));
end

if isempty(r)
  r = zeros(numOfBeams, 0);
end

plural = '';
if numOfBeams > 1
  plural = 's';
end

proc = getprocessing(rp);
if ~iscell(proc)
  proc = {proc};
end
[oblf, proc{end+1}] = calcobliquity(instrument, ...
				    'style', defaults.obliquity, ...
				    'beams', beams, ...
				    'height', defaults.height, ...
				    'absorption', r);
for n = 1:numOfBeams
  % r(n,:) = r(n,:) ./ oblf(n, :);
  r(n,:) = r(n,:) ./ oblf(n, :);
end

obl = defaults.obliquity;
