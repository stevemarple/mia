function mia = rio_qdc_base(varargin)
%RIO_QDC_BASE  Constructor for RIO_QDC_BASE class.
%
%
%   Abstract base class.

cls = 'rio_qdc_base';
parent = 'rio_base';

% Make it easy to change the class definition at a later date
latestversion = 4;
mia.versionnumber = latestversion;

% Introduced in version 4. The start and end times of the period for
% which this QDC applies. mia_base start/end times only give the period
% of the data used in its construction.
mia.qdcperiodstarttime = timestamp('bad');
mia.qdcperiodendtime = timestamp('bad');

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};

elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {parent, 'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {parent});
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    an = a(n);
    for m = 1:length(fn)
      % tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end

    base = getfield(an, parent); % parent class should already be current
    switch tmp.versionnumber
     case 1
      % data field in rio_base, set in parent
      if isfield(an, 'data')
        base = setdata(base, an.data);
      end
     case 2
      ; % sampletime did not exist in mia_base
     case 3
      ; % version without qdcperiodstarttime and qdcperiodendtime
     case 4
      ; % latest version
     otherwise
      error('unknown version');
    end
    tmp = class(tmp, cls, base);
    tmp = localfixmissingsampletime(tmp);

    mia(n) = tmp;
    
  end

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [mia unvi] = interceptprop(varargin, mia);
  % cannot warn about unknown parameters since it might be valid in a
  % p class, so pass them all up and leave parent to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;




function r = localfixmissingsampletime(mia)
r = mia;
if r.versionnumber < 3
  % mia_base did not have a sampletime field, so must set it
  % correctly. Unfortunately at this point the information need in mia_base
  % (resolution, timing) has been deleted. It can be recovered however,
  % since the difference between the first sample time and the starttime
  % will indicate if timing was centred. Need to keep the same difference,
  % but with sidereal midnight
  
  offset = getsampletime(r, 1) - getstarttime(r);
  r = setsampletime(r, getresolution(mia) * [0:(getdatasize(r, 'end')-1)] ...
		       + offset);
  
  % previously the integration time was a scalar. mia_base has probably
  % converted it to a vector, but with too many elements since start/end
  % time have different meanings here
  r = setintegrationtime(r, getintegrationtime(r, 1:getdatasize(r, 'end')));
end
