function r = shiftqdc2qdc(mia, varargin)


functionStartTime = timestamp('now');

in = getinstrument(mia);
defaults.pre_filter = ...
    mia_filter_sliding_average('method', 'median', ...
			       'windowsize', timespan(10, 'm'));
defaults.post_filter = [];
defaults.qdcclass = class(mia);
defaults.failiffilemissing = 0;
defaults.exclude = [];

% options for dcadjust
defaults.tolerance = [];
defaults.errorfunction = 'minimisesigndiff';
[defaults unvi] = interceptprop(varargin, defaults);

[tmp mfname] = fileparts(mfilename);
basename = fullfile(tempdir, [mfname '.' int2str(getpid)]);

starttime = getstarttime(mia);
endtime = getendtime(mia);
res = getresolution(mia);
loc = getlocation(in);



switch defaults.qdcclass
 case 'rio_rawqdc'
  powerclass = 'rio_rawpower';
  
 case 'rio_qdc'
  powerclass = 'rio_power';
  
 otherwise
  error(sprintf('unknown riometer QDC class (was %s)', ...
		defaults.qdcclass));
end

beams = getbeams(mia);
numOfBeams = prod(size(beams));
bestRes = info(in, 'bestresolution');

autofillMia_Filter_Qdc_Mean_Stddev = 0;
if ~isempty(defaults.pre_filter)
  if isa(defaults.pre_filter, 'mia_filter_qdc_mean_stddev')
    autofillMia_Filter_Qdc_Mean_Stddev = isempty(getqdcmeansd(defaults.pre_filter));
    autofillMia_Filter_Qdc_Mean_StddevLimits = getlimits(defaults.pre_filter);
    % need extra copies of mean and SD of QDC
  end
end

miaIn = mia;
load(fullfile(rio_datadir(in), 'qdc', [defaults.qdcclass '_template.mat']));
miaRef = mia;
mia = miaIn;
miaRef = setresolution(miaRef, res);
mia = setdata(mia, getdata(miaRef));
mia = setunits(mia, getunits(miaRef));

r = mia;
units = getunits(mia);
if isempty(defaults.tolerance)
  switch units
   case 'ADC'
    defaults.tolerance = 0.3;
    
   case 'dBm'
    defaults.tolerance = 0.002;
    
   case 'mV'
    defaults.tolerance = 0.3;
    
   otherwise
    error(sprintf('unknown units (were %s)', units));
  end
end
    
for bn = 1:numOfBeams
    % STAGE 1
  % load all the data for a single beam

  if ~isempty(defaults.pre_filter)
    if autofillMia_Filter_Qdc_Mean_Stddev
      % fill only with the beam needed, to keep memory requirements down
      disp('autofilling mia_filter_qdc_mean_stddev');
      defaults.pre_filter = ...
	  mia_filter_qdc_mean_stddev(irisqdcmeansd(starttime, in, beams(bn)), ...
				     autofillMia_Filter_Qdc_Mean_StddevLimits(1),  autofillMia_Filter_Qdc_Mean_StddevLimits(2));
    end
  end
  
  p = feval(powerclass, ...
	      'starttime', starttime, ...
	      'endtime', endtime, ...
	      'resolution', bestRes, ...
	      'instrument', in, ...
	      'load', 1, ...
	      'failiffilemissing', defaults.failiffilemissing, ...
	      'beams', beams(bn));
  if any(strmatch(class(getdata(p)), ...
		  {'int8' 'int16' 'int32' 'uint8' 'uint16' 'uint32'}))
    data = getdata(p);
    data(data == 0) = nan;
    p = setdata(p, data);
  end
  bi = getparameterindex(p, beams(bn));

  if ~isempty(getunits(p))
    if isempty(units)
      units = getunits(p);
    elseif ~strcmp(units, getunits(p))
      error(sprintf('units were previously %s and are now %s', ...
		    units, getunits(p)));
    end
  end
  
  % check if any data should be excluded
  if ~isempty(defaults.exclude)
    % work through all rows (all exclusion periods)
    for er = 1:size(defaults.exclude, 1) % er = exclude row number
      disp(['checking ' dateprintf(defaults.exclude(er, 1), ...
				   defaults.exclude(er, 2))]); 

      % find common time (if any)
      excStartTime = ceil(max(starttime, defaults.exclude(er,1)), ...
			  bestRes);
      excEndTime = floor(min(endtime, defaults.exclude(er,2)), ...
			 bestRes);
      
      if excEndTime > excStartTime
	% exclude using this rule
	starttime
	endtime
	excStartTime
	excEndTime
	ti = [((excStartTime - starttime)/bestRes):...
	      ((excEndTime - starttime)/bestRes)];  % time indices

	%  one final sanity check, which should always pass
	if ~isempty(ti)
	  % if ti is empty then all elements are set to nan - not right!
	  disp(sprintf('Excluding data for beam %d: %s (%s)', ...
		       beams(bn), dateprintf(excStartTime, excEndTime), ...
		       printseries(ti)));
	  p = setdata(p, bi, ti, nan)
	end
      end
    end
  end
  
  % pre-filter, if set
  if ~isempty(defaults.pre_filter)
    % [fh gh] = plot(p, 'color', 'r'); hold on;
    disp('filtering ...');
    p = filter(defaults.pre_filter, p);
    disp('done');
    % plot(p, 'plotaxes', gh, 'color', 'b');
  end
  
  disp(sprintf('processing beam %d', beams(bn)));

  % align QDC to specified time
  q = align(extract(mia, 'beams', beams(bn)), starttime, endtime);
  
  qdata = double(getdata(q));
  pdata = double(getdata(p));
  [tmp err] = dcadjust(qdata, pdata);
  bir = getparameterindex(r, beams(bn));
  r = setdata(r, getdata(r, bir, ':')-err, bir, ':');
  err
  if 1
    % debug code
    [fh gh] = plot(p);
    hold on;
    plot(q, 'plotaxes', gh, 'color','g');
    q2 = align(extract(r, 'beams', beams(bn)), starttime, endtime);
    plot(q2, 'plotaxes', gh, 'color','r');
  end
  
end

r = addprocessing(r, sprintf('created by %s with error function %s', ...
			     mfilename, defaults.errorfunction));
if ~isempty(defaults.pre_filter)
  r = addprocessing(r, ['preprocessing: ' ...
		    getprocessing(defaults.pre_filter, r)]);
end
for n = 1:size(defaults.exclude, 1)
  r = addprocessing(r, ['excluded data: ', ...
		    dateprintf(defaults.exclude(n, 1), ...
			       defaults.exclude(n, 2))]);
end
% save(sprintf('beam%d', beams(bn)), 'r');
if ~isempty(defaults.post_filter)
  disp('postfiltering');    
  r = filter(defaults.post_filter, r);
end

