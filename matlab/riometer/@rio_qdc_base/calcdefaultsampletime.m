function r = calcdefaultsampletime(mia, res)
%CALCDEFAULTSAMPLETIME Calculate default sample times for RIO_QDC_BASE object.
%
%   r = CALCDEFAULTSAMPLETIME(mia, res)
%   r: TIMESPAN objects
%   mia: RIO_QDC_BASE object.
%   res: TIMESPAN resolution
%
% CALCDEFAULTSAMPLETIME calculates the default sample times to use when
% changing the resolution of the data. Unlike the MIA_BASE version of
% this function the return type is TIMESPAN, as quiet-day curves are
% stored in sidereal time.
%
% See also SETRESOLUTION, mia_base/CALCDEFAULTSAMPLETIME.

r = (res./2):res:floor(siderealday, res);

