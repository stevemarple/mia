function readme(varargin)
%README file for RIO_QDC_BASE
%
%   It is recognised that some institutions create their QDCs based on
%   linearised power data, while others use non-linearised data. The
%   advantage of the first method is that numerical functions such as MEAN
%   do give the mean power. The advantage of the second method is that QDCs
%   do not need to be recreated if the linearising function is updated.
%
%   To allow either approach it is desirable to keep the different QDCs
%   separate. It would be possible to use a linearised flag in the QDC
%   class, but this would rely on each function checking it and modifying
%   its behaviour accordingly. Instead the approach used for received power
%   was used, namely separate classes. However, it is desirable that the
%   same methods to create the QDCs be available, regardless of whether they
%   are used on linearised or non-linearised data. It is therefore necessary
%   to have a common base class for the rio_rawqdc and rio_qdc classes. (It
%   is not possible to use rio_base for this purpose - both classes need a
%   specialised CONSTRUCTFROMFILES function to handle QDCs stored one beam
%   per file, which is incompatible with the way RIO_RAWPOWER, RIO_POWER and
%   RIO_ABS are stored.)
%
%   (A separate base class for just RIO_RAWPOWER and RIO_POWER was not
%   needed since they do not have any functions which cannot be also be
%   applied to RIO_ABS).
%
%   To allow RIO_RAWQDC and RIO_QDC classes to mix freely they should be
%   stored in the same directory (e.g., 'qdc', not 'rio_rawqdc' and
%   'rio_qdc'). When CONSTRUCTFROMFILES is loading RIO_QDC power, but loads
%   RIO_RAWQDC it can automatically convert to RIO_QDC provided a suitable
%   function is provided. Therefore the RIO_ABS constructor should only ever
%   request RIO_QDC data, as RIO_RAWQDC data can be converted
%   transparently. Of course it is not (easily) possible to convert from
%   RIO_QDC to RIO_RAWQDC, but there seems little reason to do so.
%
%   See also RIO_QDC_BASE, RIO_RAWQDC, RIO_QDC.
