function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  INSERT function for RIO_QDC_BASE objects.
%
% See mia_base/INSERT.


[r aa ar ba br rs] = rio_base_insert(a, b);

r.qdcperiodstarttime = min(a.qdcperiodstarttime, b.qdcperiodstarttime);
r.qdcperiodendtime = max(a.qdcperiodendtime, b.qdcperiodendtime);
