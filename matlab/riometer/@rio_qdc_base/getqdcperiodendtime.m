function r = getqdcperiodendtime(mia)
%GETQDCPERIODENDTIME  Return the QDC period end time.
%
%   r = GETQDCPERIODENDTIME(mia)
%   r: TIMESTAMP object
%   mia: RIO_QDC_BASE object
%
% See also GETQDCPERIODSTARTTIME, GETSTARTTIME, GETENDTIME, TIMESTAMP.

if length(mia) == 1
  r = mia.qdcperiodendtime;
else
  r = repmat(timestamp, size(mia));
  for n = 1:prod(size(mia))
    tmp = mia(n);
    r(n) = tmp.qdcperiodendtime;
  end
end
