function r = getqdcperiodstarttime(mia)
%GETQDCPERIODSTARTTIME  Return the QDC period start time.
%
%   r = GETQDCPERIODSTARTTIME(mia)
%   r: TIMESTAMP object
%   mia: RIO_QDC_BASE object
%
% See also GETQDCPERIODENDTIME, GETSTARTTIME, GETENDTIME, TIMESTAMP.

if length(mia) == 1
  r = mia.qdcperiodstarttime;
else
  r = repmat(timestamp, size(mia));
  for n = 1:prod(size(mia))
    tmp = mia(n);
    r(n) = tmp.qdcperiodstarttime;
  end
end
