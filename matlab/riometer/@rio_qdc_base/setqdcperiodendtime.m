function r = setqdcperiodendtime(mia, t)
%SETQDCPERIODENDTIME  Set the QDC period end time of a RIO_QDC_BASE object.
%
%   r = SETQDCPERIODENDTIME(mia, endtime)
%   r: modified RIO_QDC_BASE object
%   mia: RIO_QDC_BASE object whose QDC period end time is to be set
%   t: new QDC period end time (TIMESTAMP)
%
% See also GETQDCPERIODENDTIME, SETQDCPERIOSTARTTIME.

r = mia;
r.qdcperiodendtime = t;



