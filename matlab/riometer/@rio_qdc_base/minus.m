function r = minus(a, b)

if isa(b, 'rio_power')
  if ~isequal(size(a), size(b))
    error('sizes do not match');
  end
  for m = 1:prod(size(a))
    st = getstarttime(b(m));
    et = getendtime(b(m));
    res = getresolution(b(m));
    qdc = a(m);
    instrument = getinstrument(qdc);
    
    % some sanity checking
    units = getunits(qdc);
    if ~strcmp(getunits(b(m)), units)
      error('units do not match');
    elseif instrument ~= getinstrument(b(m))
      error('instruments do no match');
    end
    beams = intersect(getbeams(qdc), getbeams(b(m)));
    if isempty(beams)
      error('no common beams');
    end

    qdcbi = getparameterindex(qdc, beams);
    bbi = getparameterindex(b(m), beams);
    
    powa = align(qdc, st, et); % QDC aligned to set times (now is rio_power)
    powa = setresolution(powa, res);
    powad = getdata(powa, qdcbi, []);
    
    switch units
     case 'raw'
      data = 10 * log10(powad ./ getdata(b(m), bbi, []));
      
     case 'dBm'
      data = powad - getdata(b(m), bbi, []);
     otherwise
      error('unknown units');
    end
    obl = info(instrument, 'obliquity', beams);
    for n = 1:length(obl)
      data(n,:) = data(n,:) ./ obl(n);
    end
    r(m) = rio_abs('starttime', st, ...
		   'endtime', et, ...
		   'resolution', res, ...
		   'instrument', instrument, ...
		   'units', 'dB', ...
		   'processing', sprintf('Created from QDC: %s', ...
					 dateprintf(qdc)), ...
		   'data', data, ...
		   'beams', beams);
  end
  r = reshape(r, size(a));
  
else
  error('incorrect parameters');
end
