function [fh, gh, ph] = plot(mia, varargin)
%PLOT  Plot data from a RIO_QDC_BASE object.
%
%   PLOT(mia)
%   PLOT(mia, ...)

fh = [];
gh = [];
ph = [];

% use the standard base class plot function
[fh gh ph] = rio_base_plot(mia, varargin{:});

% now fix the minor problems with the duration actually being one
% sidereal day long. Make the plot show 24 hours.

day = timespan(1, 'd');

set(gh, 'XLim', [0 getcdfepochvalue(day)]);
% timetick(gh, 'X', timespan(0, 's'), day, timespan(1, 'ms'), 1);
timetick2(gh, 'updateonly', 1);


			   
