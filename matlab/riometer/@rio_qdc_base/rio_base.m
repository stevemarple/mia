function r = rio_base(mia)
%RIO_BASE  Extract the parent RIO_BASE object from a RIO_QDC_BASE object.
%
%   r = RIO_BASE(rio)
%   r: RIO_BASE object
%   rio: RIO_QDC object
%
%   See also RIO_BASE, RIO_QDC.

r = mia.rio_base;

