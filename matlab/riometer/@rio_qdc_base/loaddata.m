function r = loaddata(in, t, varargin)
%LOADDATA  Private function to load RIO_QDC data.
%

% =================================
% TEMPORARY WORKAROUND FOR RESOLUTION. Placed at start so that recursive
% function calls also get the same resolution value
global RESOLUTION_WORKAROUND;
if ~isempty(RESOLUTION_WORKAROUND)
  disp('**************************************************************');
  disp('TEMPORARY workaround for resolution: reading from global variable');
  disp('**************************************************************');
  varargin = {'resolution', RESOLUTION_WORKAROUND, varargin{:}};
  RESOLUTION_WORKAROUND = []; % reset after use
end
% =================================


r = [];

instrument = getinstrument(in);
cls = class(in);

st = getstarttime(in);
et = getendtime(in);
beams = getbeams(in);
if isempty(beams)
  beams = info(instrument, 'allbeams');
end

bi = getparameterindex(in, beams);

numOfBeams = numel(beams);

qdcduration = info(instrument, 'qdcduration');

if ~isempty(t)
  ; % t set so use it
  tqdc = info(instrument, 'qdcstarttime', t);
elseif isempty(st) & isempty(et)
  error('no time specified');
elseif ~isempty(st) & ~isempty(et)
  tqdc = calcqdcstarttime(instrument, st, et);
elseif ~isempty(st)
  % t = st;
  error('insufficent time information given');
elseif ~isempty(et)
  % t = et;
  error('insufficent time information given');
else
  error('unknown error'); % oops. some case missed
end

defaults.cancelhandle = [];
defaults.failiffilemissing = [];
defaults.filename = getfilename(in);
defaults.resolution = [];
defaults.resolutionmethod = '';
defaults.archive = '';

% if the data loaded didn't match the class required then indicate if it
% should be cast to that class
defaults.correctclass = 1;

defaults.qdctries = 1;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});
% delete unknown pairs (don't warn multiple time when qdctries > 1)
varargin(unvi) = []; 

df = info(instrument, ...
	  'defaultfilename', in, ...
	  'archive', defaults.archive);

if ~isvalid(getqdcperiodstarttime(in))
  in = setqdcperiodstarttime(in, tqdc);
end
if ~isvalid(getqdcperiodendtime(in))
  in = setqdcperiodendtime(in, tqdc + df.duration);
end


if defaults.qdctries > 1
  tmpn = ceil((defaults.qdctries - 1)/2);
  tmp = [-1:-1:-tmpn; 1:tmpn];
  mult = [0 tmp(:)'];
  
  for n = 1:defaults.qdctries
    tn = tqdc + mult(n) .* df.duration;
    disp(sprintf('QDC try %d (%s)', n, strftime(tn, '%Y-%m-%d %H:%M:%S')));
    cmd = sprintf(['%s(in, tn, varargin{:}, ' ...
		   '''qdctries'', 1, ''failiffilemissing'', 1)'], ...
		  basename(mfilename));
    
    tmpr = eval(cmd, '[]');
    if ~isequal(tmpr, [])
      r = tmpr;
      if n > 1
	% had to fall back to using an alternative QDC, so mark the QDC
        % as preliminary. This should ensure any data made with it is
        % also marked preliminary.
	r = adddataquality(r, 'Preliminary');
      end
      return
    end
  end
  error(sprintf('could not load any suitable QDCs, last error was:\n%s', ...
		lasterr));
  return
end


if isempty(defaults.filename)
  defaults.filename = df.fstr;
end

if isempty(defaults.failiffilemissing)
  defaults.failiffilemissing = df.failiffilemissing;
end

[tmp mfname] = fileparts(mfilename);

% Allocate a cell array big enough to hold data from all of the files, join
% them in an efficient way later
mia_c = cell(1, numOfBeams);

for n = 1:numOfBeams
  cancelcheck(defaults.cancelhandle);
  if strcmp(defaults.filename(1), '@')
    % 'filename' is actually a function we should execute
    filename = feval(defaults.filename(2:end), in, t);
  else
    % filename = strftime(t, defaults.filename);
    filename = beamstrftime(tqdc, beams(n), defaults.filename);
  end
  
  
  if ~url_exist(filename)
    if defaults.failiffilemissing
      error(sprintf('Cannot load %s', filename));
    else
      disp(sprintf('\rmissing %s', filename));
    end
    mia = feval(cls, ...
		'starttime', tqdc, ...
		'endtime', tqdc + qdcduration, ...
		'instrument', instrument, ...
		'units', df.units, ...
		'data', repmat(feval(df.dataclass, 0), 1, 0), ...
		'beams', beams(n), ...
		'qdcperiodstarttime', tqdc, ...
		'qdcperiodendtime', tqdc + qdcduration, ...
		'load', 0, ...
		'log', 0);
  else
    disp(sprintf('\rloading %s', filename));
    clear mia;
    
    url_load(filename);
    % check we got the variable we expected
    if ~exist('mia', 'var')
      whos
      error('variable called mia not found');
    end
    loaded_ok = 1;

  
     %   % --- compatibility with old iris toolkit
     %   if ~exist('mia', 'var')
     %     s = url_whos('-file', filename);
     %     if size(s) > 1
     %       error(sprintf('file (%s) contains more than one variable', ...
     % 		    filename));
     %     end
     %     mia = eval(sprintf('ifb2rio_base(%s)', s.name));
     %   end
     %   % --- (end)
     
     if ~strcmp(class(mia), cls) & logical(defaults.correctclass)
       % convert to desired class. This means we can convert rio_rawqdc to
       % rio_qdc transparently. The conversion has to be done here, not
       % after changing resolution since that uses mean/median and
       % rio_rawqdc is not linearised.
       mia = feval(cls, mia);
     end
     
     if ~isempty(defaults.resolution)
       mia = setresolution(mia, defaults.resolution, ...
				defaults.resolutionmethod);
     end
     
  end
  mia_c{n} = mia;

end

if numel(mia_c) > 1
  fprintf('joining files ');
  while numel(mia_c) > 1
    % remove any empty elements in mia_c
    fprintf('.');
    mia_c(cellfun('isempty', mia_c)) = [];
    for n = 1:2:(numel(mia_c)-1)
      mia_c{n} = insert(mia_c{n}, mia_c{n+1});
      mia_c{n + 1} = [];
    end
  end
  fprintf('\n');
  
  r = mia_c{1};
else
  r = mia_c{1};
end


