function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK EXTRACTCHECK overloaded for RIO_QDC_BASE objects.
%
% See mia_base/EXTRACTCHECK.


if length(varargin) == 1
  % subscript interface
  [sr s] = rio_base_extractcheck(mia, varargin{:});

else
  % parameter name/value interface
  defaults.starttime = [];
  defaults.endtime = [];
  [defaults unvi] = interceptprop(varargin, defaults);

  if ~isempty(defaults.starttime) & ~isa(defaults.starttime, 'timespan')
    error('starttime must be a timespan');
  elseif ~isempty(defaults.endtime) & ~isa(defaults.endtime, 'timespan')
    error('endtime must be a timespan');
  end
  
  [sr s] = rio_base_extractcheck(mia, varargin{:});

  % For QDCs start/end time is the start of the QDC period
  s.starttime = getstarttime(mia);
  s.endtime = getendtime(mia);
  
end

