function r = align(mia, varargin)
%ALIGN  Time-align a RIO_QDC object to a given start and end time.
%
%   r = ALIGN(mia, p)
%   r = ALIGN(mia, p, 'method', method)
%   r: RIO_(RAW)POWER object
%   mia: RIO_(RAW)QDC object
%   p: RIO_(RAW)POWER object
%   method: interpolation method used by INTERP1 (default is 'cubic')
%   Align QDC to the sample times in p.
%
%   r = ALIGN(mia, t)
%   r = ALIGN(mia, t, 'method', method)
%   r: RIO_(RAW)POWER object
%   mia: RIO_(RAW)QDC object
%   t: sample times of data points (TIMESTAMP)
%   method: interpolation method used by INTERP1 (default is 'cubic')
%   Align QDC to the sample times in t.
%
%   r = ALIGN(mia, st, et)
%   r: RIO_(RAW)POWER object
%   mia: RIO_(RAW)QDC object
%   st: start time (TIMESTAMP)
%   et: end time (TIMESTAMP)
%   Align to given start and end times, with the same resolution as the
%   QDC. Note this method is superceded by the sample times version.
%
%
%   See also RIO_QDC, RIO_POWER, TIMESTAMP.

% There are two parts to the problem of aligning a QDC. One is to align
% the sampling periods of the QDC such that they fall on the same points
% in time as starttime:resolution:endtime. This is not so important for
% 10s resolution data. However sampling errors at 30 minutes resolution
% would be too big.
%
% The second part is to shift the QDC such that the local mean sidereal
% times of the data and QDC match.

beams = getbeams(mia);
rio = getinstrument(mia);
loc = getlocation(rio);
if length(loc) ~= 1
  % what location is used to calculate local siderealtime?
  error('riometer must have a single location');
end
bestRes = info(rio, 'bestresolution');
duration = getduration(mia);

approxSidDay = round(siderealday, bestRes); % closest we can measure it too
integrationtime = getintegrationtime(mia);

defaultmethod = 'linear';
if nargin == 3 & isa(varargin{1}, 'timestamp') & isa(varargin{2}, 'timestamp')
  % parameters are: mia, starttime, endtime
  res = getresolution(mia);
  defaults.starttime = varargin{1};
  defaults.endtime = varargin{2};
  defaults.method = defaultmethod;
  sampletime = (defaults.starttime+(res./2)):res:defaults.endtime;

elseif nargin >= 2 & (isa(varargin{1}, 'rio_power') | ...
		      isa(varargin{1}, 'rio_rawpower'))
  % parameters are: QDC, power
  % or parameters are: QDC, power, method
  p = varargin{1};
  sampletime = getsampletime(p);
  defaults.starttime = getstarttime(p);
  defaults.endtime = getendtime(p);
  defaults.method = defaultmethod;
  if nargin > 2
    defaults.method = varargin{2};
  elseif numel(sampletime) >= 2
    [mia_guess_res tmp] = guessresolution(mia);
    [p_guess_res tmp] = guessresolution(p);
    if mia_guess_res > 5 * p_guess_res
      % poor QDC resolution, use cubic instead of linear interpolation
      defaults.method = 'cubic';
    end
  end

elseif nargin >= 2 & isa(varargin{1}, 'timestamp') ...
      & rem(nargin, 2) == 0 & all(mia_ischar(varargin{2:2:end}))
  sampletime = varargin{1};
  varargin(1) = [];
  defaults.method = defaultmethod;
  if numel(sampletime) >= 2
    if guessresolution(mia) > 5 * nonanmedian(diff(sampletime))
      % poor QDC resolution, use cubic instead of linear interpolation
      defaults.method = 'cubic';
    end
  end
  defaults.starttime = sampletime(1) - integrationtime(1)./2;
  defaults.endtime = sampletime(end) + integrationtime(1)./2;

  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
else
  varargin{:}
  error('incorrect parameters');
end

% repallocate data array
data = zeros(numel(beams), numel(sampletime));

% calculate offsets from start of local mean sidereal day
stoff_cdf = getcdfepochvalue(stoffset(sampletime, loc));

% Use interp1 to interpolate the QDC values to the exact sidereal times
% needed. If necessary 'nearest' can be used to prevent
% interpolation. Extrapolation is undesirable, but add padding at each
% end as QDCs wrap around

qdc_sampletime = getsampletime(mia);
x = getcdfepochvalue(qdc_sampletime);
y = getdata(mia);
zeros_s = timespan(0, 's');

if qdc_sampletime(1) >= zeros_s ...
      & qdc_sampletime(end) - siderealday < zeros_s
  % safe to pad on LHS
  x = [getcdfepochvalue(qdc_sampletime(end) - siderealday) x];
  y = [y(:, end) y];
end

if qdc_sampletime(end) <= siderealday ...
      & qdc_sampletime(1) > zeros_s
  % safe to pad on RHS
  x(end+1) = getcdfepochvalue(qdc_sampletime(1) + siderealday);
  y(:, end+1) = y(:, 1);
end

% do interpolation
if ~isempty(stoff_cdf)
  for n = 1:numel(beams)
    data(n, :) = interp1(x, y(n, :), stoff_cdf, defaults.method);
  end
end

if isa(mia, 'rio_qdc')
  cls = 'rio_power';
elseif isa(mia, 'rio_rawqdc')
  cls = 'rio_rawpower';
else
  'unknown QDC class';
end


if ~isempty(integrationtime)
  integrationtime2 = repmat(integrationtime(1), size(sampletime));
else
  integrationtime2 = timespan(repmat(nan, size(sampletime)));
end
r = feval(cls, ...
	  'instrument', rio, ...
	  'starttime', defaults.starttime, ...
	  'endtime', defaults.endtime, ...
	  'sampletime', sampletime, ...
	  'integrationtime', integrationtime2, ...
	  'units', getunits(mia), ...
	  'processing', getprocessing(mia), ...
	  'data', data, ...
	  'beams', beams);
r = addprocessing(r, sprintf('produced from QDC using method=%s', ...
                             defaults.method));


% join data quality
r = setdataquality(r, getdataquality(mia));
	      
	      
