function r = loadobj(a)

if isstruct(a)
  r = rio_qdc_base(a);
else
  r = a;
end


