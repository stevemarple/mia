function r = spaceregularly(mia, varargin)
%SPACEREGULARLY  Space samples at a regular interval
%
%   r = SPACEREGULARLY(mia, ...)
%   r: modified MIA_BASE object
%   mia: MIA_BASE object
%
% The following parameter name/value pairs are accepted:
%
% 'markmissingdata', LOGICAL
% Flag to indicate if missing data periods should be marked with the
% insertion of NANs before processing commences. See MARKMISSINGDATA for
% more details. Default is TRUE.
%
% 'method', CHAR
% The method interp1 should use for its interpolation. See INTERP1 for
% details of valid methods.
%
% 'resolution', TIMESPAN (scalar)
% The resolution of the regularly spaced samples. If empty (or not given)
% the result of GUESSRESOLUTION is used.
%
% 'sampletime', TIMESPAN
% The sample times to which data should be aligned to. 
%
% It is not necessary to specify both resolution and sampletime, but if
% both are given they must not conflict.
%
% See also GUESSRESOLUTION, INTERP1.

r = mia;
defaults.markmissingdata = true;
defaults.method = '';
defaults.resolution = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

[res resmesg] = getresolution(mia);

if isempty(defaults.resolution)
  if ~isempty(resmesg)
    % samples are not spaced equally, so guess resolution
    defaults.resolution = guessresolution(mia);
  else
    defaults.resolution = res;
    if isempty(defaults.sampletime)
      % data is already spaced regularly, no resolution specified and no
      % sampletime so current object is ok
      return
    end
  end
end

samt = calcdefaultsampletime(mia, defaults.resolution);

if logical(defaults.markmissingdata)
  r = markmissingdata(r, 'resolution', defaults.resolution);
end

r = resampledata(r, samt, ...
		 'resolution', defaults.resolution, ...
		 'method', defaults.method);
