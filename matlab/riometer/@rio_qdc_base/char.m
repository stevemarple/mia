function r = char(mia, varargin)
%CHAR  Convert a RIO_QDC_BASE object to a CHAR.
%
%   r = CHAR(mia)
%
% See mia_base/CHAR.

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

if isempty(mia.qdcperiodstarttime)
  qdcst_str = '';
else
  qdcst_str = char(mia.qdcperiodstarttime);
end

if isempty(mia.qdcperiodendtime)
  qdcet_str = '';
else
  qdcet_str = char(mia.qdcperiodendtime);
end

r = sprintf(['%s' ...
	     'QDC period start  : %s\n' ...
             'QDC period end    : %s\n'], ...
 	    rio_base_char(mia, varargin{:}), ...
            qdcst_str, qdcet_str);
