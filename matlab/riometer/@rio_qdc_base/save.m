function save(miaIn, varargin)
%SAVE  Save a RIO_QDC_BASE object
%
% Overloaded version of SAVE for RIO_QDC_BASE objects. Filenames are derived
% using BEAMSTRFTIME, and if the %Q format specifier is present only one
% beam is stored per file, otherwise this function is identical to
% mia_base/SAVE. The time passed to STRFTIME is the QDC period start time.
%
% See mia_base/SAVE, BEAMSTRFTIME. GETQDCPERIODSTARTTIME.


beams = getbeams(miaIn);
if isempty(beams)
  error('no beams');
end

% ---------------
instrument = getinstrument(miaIn);
s = info(instrument, 'defaultfilename', miaIn);
if ~isempty(s)
  defaults.fstr = s.fstr;
else
  % class does not have a default filename. Make one up
  defaults.fstr = gettype(miaIn);
  defaults.fstr(' ') = '_'; % map any spaces to underscores
end

% if the object has its filename set then use that if preference to the
% class default. however, object's filename can be overridden by a supplied
% name.
if ~isempty(getfilename(miaIn))
  defaults.fstr = getfilename(miaIn);
end
  
defaults.merge = 0;
defaults.overwrite = 1;

if nargin == 1
  if isempty(s)
    error(sprintf('no default filename for ''%s'' class', class(miaIn)));
  end
  
elseif nargin == 2
  defaults.fstr = varargin{1};
  
elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
else
  error('Incorrect number of parameters');
end

if logical(defaults.merge)
  error('merge not implemented');
end


% Before saving check some basic details are correct
qdcst = getqdcperiodstarttime(miaIn);
if ~isa(qdcst, 'timestamp')
  error('QDC period start time must be a timestamp');
elseif ~isvalid(qdcst)
  error('QDC period start time is not valid');
end
qdcet = getqdcperiodendtime(miaIn);
if ~isa(qdcet, 'timestamp')
  error('QDC period end time must be a timestamp');
elseif ~isvalid(qdcet)
  error('QDC period end time is not valid');
end

dsize = getdatasize(miaIn);
samt = getsampletime(miaIn);
if ~isa(samt, 'timespan')
  error('sample time must be a timespan');
elseif ~isequal(size(samt), [1, dsize(end)])
  miaIn
  assignin('base', 'miaIn', miaIn);
  error('sample time is incorrect size');
end
if dsize(end)
  if samt(1) < timespan(0, 's')
    error('last sample before sidereal midnight');
  elseif samt(end) > siderealday
    error('last sample after sidereal midnight');
  end
end

it = getintegrationtime(miaIn);
if ~isa(it, 'timespan')
  error('integration time must be a timespan');
elseif ~isequal(size(it), [1, dsize(end)])
  error('integration time is incorrect size');
end




% clear the filename field
miaIn = setfilename(miaIn, '');

% Does the filename we have been given have contain '%Q' to include beam
% number?
[tmp, rep] = beamstrftime(qdcst, beams(1), defaults.fstr);

if any(strcmp(rep, 'Q'))
  % save one beam per file
  for n = 1:numel(beams)
    mia = extract(miaIn, 'beams', beams(n));
    filename = beamstrftime(qdcst, beams(n), defaults.fstr);

    if ~logical(defaults.overwrite) & url_exist(filename)
      error(sprintf('%s exists and overwriting forbidden', filename));
    end

    disp(sprintf('saving beam #%d as mia to %s', beams(n), filename));
    url_mkdir(fileparts(filename));

    % Get the save options every time, allows them to be dependent on what
    % is being saved
    opts = saveopts(mia);

    url_save(filename, 'mia', opts{:});
  end
  
else
  % save all beams into the same file
  mia = miaIn;
  filename = strftime(qdcst, defaults.fstr);
  
  if ~logical(defaults.overwrite) & url_exist(filename)
    error(sprintf('%s exists and overwriting forbidden', filename));
  end

  % Get the save options every time, allows them to be dependent on what
  % is being saved
  opts = saveopts(mia);

  disp(sprintf('saving all beams as mia to %s', filename));
  url_mkdir(fileparts(filename));  
  url_save(filename, 'mia', opts{:});
end
