function r = mean(varargin)
%MEAN Calculate the mean QDC.
%
%   r = MEAN(...);
%
% MEAN calculates the MEAN value of all given quiet day curves. The
% result contains only the beams which are common to all quiet day
% curves. Any number of QDCs can be passed as function parameters, and
% each can be scalars, vectors or matrices. The default behaviour can be
% modified with the following name/value pairs, which must be supplied
% after the QDCs.
%
%   'starttime', TIMESTAMP
%   The start time assigned to the returned QDC object.
%
%   'endtime', TIMESTAMP
%   The end time assigned to the returned QDC object.
%
%   'qdcclass', CHAR
%   The class of QDC to return. Defaults to rio_rawqdc or rio_qdc
%   (determined by class of first QDC).
%
% See also RIO_QDC_BASE, RIO_RAWQDC, RIO_QDC, COPYQDC.

% allow varargin to contain any number of entries, each of which can be
% scalar or vectors

qdc = {};
opts = [];
beams = getbeams(varargin{1}(1));
for n = 1:length(varargin)
  if ~isa(varargin{n}, 'rio_qdc_base')
    % detect parameter name value pairs at end
    opts = n:length(varargin);
    break;
  end
  for m = 1:numel(varargin{n})
    qdc{end+1} = varargin{n}(m);
    beams = intersect(beams, getbeams(qdc{end}));
  end
end

if isempty(beams)
  error('no common beams');
end

defaults.starttime = [];
defaults.endtime = [];
defaults.qdcclass = '';

% only some of varargin are options, and need to know unknown name/values
% indices within those. Make life easy and use a spearate matric just for
% the options
varargin2 = varargin(opts); 
[defaults unvi] = interceptprop(varargin2, defaults);

if isempty(defaults.qdcclass)
  if isa(qdc{1}, 'rio_qdc')
    defaults.qdcclass = 'rio_qdc';
  elseif isa(qdc{1}, 'rio_rawqdc')
    defaults.qdcclass = 'rio_qdc';
  else
    % some new class we are not aware of, but still derived from
    % rio_qdc_base. Handle it but issue a warning
    warning(sprintf(['first QDC is not a rio_qdc or a rio_rawqdc, ' ...
		     'creating a %s object'], class(qdc{1})));
    defaults.qdcclass = class(qdc{1});
  end
end

% prepare some sanity checks
beams = getbeams(qdc{1});
in = getinstrument(qdc{1});
units = getunits(qdc{1});
sampletime = getsampletime(qdc{1});
it = getintegrationtime(qdc{1});
freq = getfrequency(in);
dsz = getdatasize(qdc{1});


alldata = zeros([dsz length(qdc)]);

st = getstarttime(qdc{1});
et = getendtime(qdc{1});

dates = cell(1, length(qdc));
for n = 1:length(qdc)
  tmp = extract(qdc{n}, 'beams', beams);
  tmpin = getinstrument(tmp);
  if in ~= tmpin
    error('instruments are not the same');
  elseif ~isequal(units, getunits(tmp))
    error('units are not the same');
  elseif ~isequal(sampletime, getsampletime(tmp))
    error('sample times are not the same');
  elseif ~isequal(it, getintegrationtime(tmp))
    error('integration times are not the same');
  elseif freq ~= getfrequency(tmpin)
    error('effective operating frequencies are not the same');
  elseif ~isequal(dsz, getdatasize(tmp))
    error('datasizes for the common beams are not the same');
  end
  
  % looks ok
  alldata(:,:,n) = getdata(qdc{n});
  dates{n} = dateprintf(getstarttime(tmp), getendtime(tmp));
  
  if isempty(defaults.starttime)
    st = min(st, getstarttime(tmp));
  end
  if isempty(defaults.endtime)
    et = max(et, getendtime(tmp));
  end
end

if isempty(defaults.starttime)
  defaults.starttime = st;
end
if isempty(defaults.endtime)
  defaults.endtime = et;
end


processing = ['Created from nonanmean of QDCs dated:' ...
	      sprintf('\n  %s', dates{:})];

meandata = nonanmean(alldata, 3);
r = feval(defaults.qdcclass, ...
	  varargin2{unvi}, ...
	  'starttime', defaults.starttime, ...
	  'endtime', defaults.endtime, ...
          'sampletime', sampletime, ...
          'integrationtime', it, ...
	  'instrument', in, ...
	  'units', units, ...
	  'processing', processing, ...
	  'beams', beams, ...
	  'data', meandata);
	  
	  
