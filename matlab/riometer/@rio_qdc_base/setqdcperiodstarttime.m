function r = setqdcperiodstarttime(mia, t)
%SETQDCPERIODSTARTTIME  Set the QDC period start time of a RIO_QDC_BASE object.
%
%   r = SETQDCPERIODSTARTTIME(mia, starttime)
%   r: modified RIO_QDC_BASE object
%   mia: RIO_QDC_BASE object whose QDC period start time is to be set
%   t: new QDC period start time (TIMESTAMP)
%
% See also GETQDCPERIODSTARTTIME, SETQDCPERIODENDTIME.

r = mia;
r.qdcperiodstarttime = t;



