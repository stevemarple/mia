function siderealplotcb(action)
cbo = gcbo;
cbf = gcbf;

switch action
 case 'click'
  % mouse click on a line

  ah = get(cbo,'Parent');
  units = get(ah, 'Units');
  p = get(ah, 'CurrentPoint');
  set(ah, 'Units', units);
  ud = get(cbo,'UserData');
  t = ud.starttime + ud.resolution * p(2,1);
  disp(char(t));

  % highlight selected curve

  set(findall(ah, 'Type', 'line'), 'Color', ud.color);
  set(cbo, 'Color', ud.selectedcolor);

  % put selected curve on top
  ch = allchild(ah);
  ch(ch == cbo) = [];
  set(ah, 'Children', [cbo;ch]);

  fud = get(cbf, 'UserData');

  if ~isfield(fud, 'siderealplot')
    fud.siderealplot.t = t;
    fud.siderealplot.exclude = repmat(timestamp, 0, 0);
    fud.siderealplot.lastline = 0;
  end


  if cbo == fud.siderealplot.lastline & ~isempty(fud.siderealplot.t)
    % second point selected, add to exclude matrix
    if t > fud.siderealplot.t
      fud.siderealplot.exclude(end+1, 1:2) = [fud.siderealplot.t t];
    elseif t < fud.siderealplot.t
      fud.siderealplot.exclude(end+1, 1:2) = [t fud.siderealplot.t];
    end
    fud.siderealplot.t = [];
    
  else
    % first point, or new line
    fud.siderealplot.t = t;
  end
  fud.siderealplot.lastline = cbo;

  set(cbf, 'UserData', fud);

  fud.siderealplot

 case 'windowclose'
  fud = get(cbf, 'UserData');
  
  if isstruct(fud) & isfield(fud, 'siderealplot')
    % find list of variables in base workspace
    w = evalin('base', 'who');
    
    n = 0;
    var = 'exclude';
    while any(strcmp(w, var))
      n = n + 1;
      var = sprintf('exclude%d', n);
    end
    
    disp(sprintf('copying exclude times to base workspace variable %s', ...
		 var));
    assignin('base', var, fud.siderealplot.exclude);
  end
  
  closereq;
  
 otherwise
  error(sprintf('unknown action (was ''%s'')', action));
end
