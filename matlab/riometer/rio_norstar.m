function r = rio_norstar
%RIO_NORSTAR  Return a list of the NORSTAR riometers.
%
%   r = RIO_NORSTAR
%   r: vector of RIOMETER objects
%
%   For more details about NORSTAR riometers see
%   http://aurora.phys.ucalgary.ca/norstar/
%
%   See also RIOMETER.

% Get list of all known riometers
c = instrumenttypeinfo(riometer,'aware');

r = c(strcmp(getfacility(c), 'NORSTAR'));
