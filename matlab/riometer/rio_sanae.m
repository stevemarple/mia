function r = rio_sanae
%RIO_SANAE  RIOMETER object for SANAE.

% http://www.sprl.umich.edu/mist/star/Ant_riometers.html
r = riometer('name', '', ...
	     'abbreviation', 'sna', ...
	     'location', location('SANAE', 'Antarctica', ...
				  -71.67, 357.15), ...
	     'frequency', 38e6);
% 64 beams

