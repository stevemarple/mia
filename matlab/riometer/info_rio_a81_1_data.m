function r = info_rio_a81_1_data
%INFO_RIO_A81_1_DATA Return basic information about rio_a81_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_a81_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=a81;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'a81';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = 'art:977';
r.comment = '';
r.credits = '';
r.datarequestid = 31;
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'AGONET';
r.facility_url = 'http://iacg.org/iacg/ground_stations/agonet.html';
r.facilityid = 3;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 68;
r.ifrequency = [];
r.imaging = true;
r.imagingbeams = [];
r.latitude = -81.5;
r.location1 = '?';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = 'bas_logo_text';
r.logurl = '';
r.longitude = 3;
r.modified = timestamp([2011 07 08 11 56 35.593515]);
r.name = 'AGO A81';
r.piid = 3;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([1996 01 11 00 00 00]);
r.systemtype = '';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'British Antarctic Survey';
% end of function
