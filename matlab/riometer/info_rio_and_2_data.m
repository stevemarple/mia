function r = info_rio_and_2_data
%INFO_RIO_AND_2_DATA Return basic information about rio_and_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_and_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=and;serialnumber=2

r.limits = [];
r.limits.rio_qdc = [-104 -96];
r.limits.rio_rawpower = [nan nan];
r.pixels.deg.x.max = 18.1;
r.pixels.deg.x.min = 13.7;
r.pixels.deg.x.step = 0.2;
r.pixels.deg.y.max = 70.1;
r.pixels.deg.y.min = 68.5;
r.pixels.deg.y.step = 0.1;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'and';
r.antennaazimuth = 0;
r.antennaphasingx = [-0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 ...
    -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 ...
    -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 ...
    -0.75 -0.5 -0.25 0 0.25 0.5 0.75 -0.75 -0.5 -0.25 0 0.25 0.5 ...
    0.75];
r.antennaphasingy = [0.75 0.75 0.75 0.75 0.75 0.75 0.75 0.5 0.5 0.5 ...
    0.5 0.5 0.5 0.5 0.25 0.25 0.25 0.25 0.25 0.25 0.25 0 0 0 0 0 0 ...
    0 -0.25 -0.25 -0.25 -0.25 -0.25 -0.25 -0.25 -0.5 -0.5 -0.5 -0.5 ...
    -0.5 -0.5 -0.5 -0.75 -0.75 -0.75 -0.75 -0.75 -0.75 -0.75];
r.antennas = [8 8];
r.antennaspacing = 0.5;
r.azimuth = [-45 -33.7 -18.6 0 18.6 33.7 45 -56.3 -45 -26.6 0 26.6 ...
    45 56.3 -71.4 -63.4 -45 0 45 63.4 71.4 -90 -90 -90 0 90 90 90 ...
    -108.6 -116.6 -135 180 135 116.6 108.6 -123.7 -135 -153.4 180 ...
    153.4 135 123.7 -135 -146.3 -161.4 180 161.4 146.3 135];
r.badbeams = [1 7 43 49];
r.beamplanc = 7;
r.beamplanr = 7;
r.beamwidth = [12.461 13.8935 13.3758 13.1156 13.3758 13.8935 ...
    12.461 13.8935 12.8756 12.1678 11.95 12.1678 12.8756 13.8935 ...
    13.3758 12.1678 11.5448 11.3729 11.5448 12.1678 13.3758 13.1156 ...
    11.95 11.3729 11.169 11.3729 11.95 13.1156 13.3758 12.1678 ...
    11.5448 11.3729 11.5448 12.1678 13.3758 13.8935 12.8756 12.1678 ...
    11.95 12.1678 12.8756 13.8935 12.461 13.8935 13.3758 13.1156 ...
    13.3758 13.8935 12.461];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2060 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1 15];
r.iantennatype = 'crossed-dipole';
r.ibeams = 49;
r.id = 64;
r.ifrequency = 3.8235e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49];
r.latitude = 69.1458;
r.location1 = 'Andoya';
r.location1_ascii = 'Andoya';
r.location2 = 'Norway';
r.logo = 'asc_logo';
r.logurl = '';
r.longitude = 16.0292;
r.modified = timestamp([2015 07 17 13 34 34.545008]);
r.name = 'AIRIS';
r.piid = 13;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [2.574 1.817 1.502 1.423 1.502 1.817 2.574 ...
    1.817 1.355 1.187 1.143 1.187 1.355 1.817 1.502 1.187 1.065 ...
    1.031 1.065 1.187 1.502 1.423 1.143 1.031 1 1.031 1.143 1.423 ...
    1.502 1.187 1.065 1.031 1.065 1.187 1.502 1.817 1.355 1.187 ...
    1.143 1.187 1.355 1.817 2.574 1.817 1.502 1.423 1.502 1.817 ...
    2.574];
r.starttime = timestamp([2006 01 17 00 00 00]);
r.systemtype = 'iris';
r.url = 'http://alomar.rocketrange.no/iris-and.html';
r.url2 = {'http://alomar.rocketrange.no/iris-and.html'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [69.2 57.9 49.2 46.2 49.2 57.9 69.2 57.9 43.2 33.1 29.4 ...
    33.1 43.2 57.9 49.2 33.1 20.4 14.2 20.4 33.1 49.2 46.2 29.4 ...
    14.2 0 14.2 29.4 46.2 49.2 33.1 20.4 14.2 20.4 33.1 49.2 57.9 ...
    43.2 33.1 29.4 33.1 43.2 57.9 69.2 57.9 49.2 46.2 49.2 57.9 ...
    69.2];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'arcom';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/original_format/%Y/%m/%d_f/airis%Y%m%d%H_0000_f.arcom';
r.defaultfilename.original_format.default.loadfunction = 'arcom2mia';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 3600];
r.defaultfilename.original_format.default.units = 'raw';
r.defaultfilename.original_format.original_format.archive = 'original_format';
r.defaultfilename.original_format.original_format.dataclass = 'double';
r.defaultfilename.original_format.original_format.defaultarchive = false;
r.defaultfilename.original_format.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.original_format.original_format.failiffilemissing = false;
r.defaultfilename.original_format.original_format.format = 'arcom';
r.defaultfilename.original_format.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/original_format/%Y/%m/%d_f/airis%Y%m%d%H_0000_f.arcom';
r.defaultfilename.original_format.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.original_format.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.original_format.savefunction = '';
r.defaultfilename.original_format.original_format.size = [64 3600];
r.defaultfilename.original_format.original_format.units = 'raw';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'mat';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_power/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power.default.loadfunction = '';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [49 3600];
r.defaultfilename.rio_power.default.units = 'dBm';
r.defaultfilename.rio_power_calib.default.archive = 'default';
r.defaultfilename.rio_power_calib.default.dataclass = 'double';
r.defaultfilename.rio_power_calib.default.defaultarchive = true;
r.defaultfilename.rio_power_calib.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power_calib.default.failiffilemissing = false;
r.defaultfilename.rio_power_calib.default.format = 'mat';
r.defaultfilename.rio_power_calib.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_power/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power_calib.default.loadfunction = '';
r.defaultfilename.rio_power_calib.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power_calib.default.savefunction = '';
r.defaultfilename.rio_power_calib.default.size = [49 3600];
r.defaultfilename.rio_power_calib.default.units = 'dBm';
r.defaultfilename.rio_power_calib.original_format.archive = 'original_format';
r.defaultfilename.rio_power_calib.original_format.dataclass = 'double';
r.defaultfilename.rio_power_calib.original_format.defaultarchive = false;
r.defaultfilename.rio_power_calib.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power_calib.original_format.failiffilemissing = true;
r.defaultfilename.rio_power_calib.original_format.format = 'arcom';
r.defaultfilename.rio_power_calib.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/original_format/%Y/%m/%d_f/airis%Y%m%d%H_0000_f.arcom';
r.defaultfilename.rio_power_calib.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_power_calib.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power_calib.original_format.savefunction = '';
r.defaultfilename.rio_power_calib.original_format.size = [49 3600];
r.defaultfilename.rio_power_calib.original_format.units = 'raw';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'int16';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [49 3600];
r.defaultfilename.rio_qdc.default.units = 'ADC';
r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'int16';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [49 86164];
r.defaultfilename.rio_qdc_fft.default.units = 'ADC';
r.defaultfilename.rio_qdc_mean_sd.default.archive = 'default';
r.defaultfilename.rio_qdc_mean_sd.default.dataclass = 'double';
r.defaultfilename.rio_qdc_mean_sd.default.defaultarchive = true;
r.defaultfilename.rio_qdc_mean_sd.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc_mean_sd.default.failiffilemissing = true;
r.defaultfilename.rio_qdc_mean_sd.default.format = 'mat';
r.defaultfilename.rio_qdc_mean_sd.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_qdc_mean_sd/rio_and_2_qdc_mean_sd_b%Q.mat';
r.defaultfilename.rio_qdc_mean_sd.default.loadfunction = '';
r.defaultfilename.rio_qdc_mean_sd.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc_mean_sd.default.savefunction = '';
r.defaultfilename.rio_qdc_mean_sd.default.size = [49 86164];
r.defaultfilename.rio_qdc_mean_sd.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'arcom';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_rawpower.default.loadfunction = 'arcom2mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [49 3600];
r.defaultfilename.rio_rawpower.default.units = 'raw';
r.defaultfilename.rio_rawpower_calib.default.archive = 'default';
r.defaultfilename.rio_rawpower_calib.default.dataclass = 'double';
r.defaultfilename.rio_rawpower_calib.default.defaultarchive = true;
r.defaultfilename.rio_rawpower_calib.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower_calib.default.failiffilemissing = true;
r.defaultfilename.rio_rawpower_calib.default.format = 'arcom';
r.defaultfilename.rio_rawpower_calib.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/original_format/%Y/%m/%d_f/airis%Y%m%d%H_0000_f.arcom';
r.defaultfilename.rio_rawpower_calib.default.loadfunction = 'arcom2mia';
r.defaultfilename.rio_rawpower_calib.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower_calib.default.savefunction = 'arcomnosave';
r.defaultfilename.rio_rawpower_calib.default.size = [49 3600];
r.defaultfilename.rio_rawpower_calib.default.units = 'raw';
r.defaultfilename.rio_rawpower_calib.original_format.archive = 'original_format';
r.defaultfilename.rio_rawpower_calib.original_format.dataclass = 'double';
r.defaultfilename.rio_rawpower_calib.original_format.defaultarchive = false;
r.defaultfilename.rio_rawpower_calib.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower_calib.original_format.failiffilemissing = true;
r.defaultfilename.rio_rawpower_calib.original_format.format = 'arcom';
r.defaultfilename.rio_rawpower_calib.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/original_format/%Y/%m/%d_f/airis%Y%m%d%H_0000_f.arcom';
r.defaultfilename.rio_rawpower_calib.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_rawpower_calib.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower_calib.original_format.savefunction = 'arcomnosave';
r.defaultfilename.rio_rawpower_calib.original_format.size = [49 ...
    3600];
r.defaultfilename.rio_rawpower_calib.original_format.units = 'raw';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [49 3600];
r.defaultfilename.rio_rawqdc.default.units = 'raw';
r.defaultfilename.temp_data.default.archive = 'default';
r.defaultfilename.temp_data.default.dataclass = 'double';
r.defaultfilename.temp_data.default.defaultarchive = true;
r.defaultfilename.temp_data.default.duration = timespan(1 , 'd');
r.defaultfilename.temp_data.default.failiffilemissing = false;
r.defaultfilename.temp_data.default.format = 'airistempdata';
r.defaultfilename.temp_data.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/temp_data/%Y/%m/%Y%m%d.txt';
r.defaultfilename.temp_data.default.loadfunction = '';
r.defaultfilename.temp_data.default.resolution = timespan([], 's');
r.defaultfilename.temp_data.default.savefunction = '';
r.defaultfilename.temp_data.default.size = [-1 -1];
r.defaultfilename.temp_data.default.units = 'C';
r.defaultfilename.temp_data.original_format.archive = 'original_format';
r.defaultfilename.temp_data.original_format.dataclass = 'double';
r.defaultfilename.temp_data.original_format.defaultarchive = true;
r.defaultfilename.temp_data.original_format.duration = timespan(1 , 'd');
r.defaultfilename.temp_data.original_format.failiffilemissing = false;
r.defaultfilename.temp_data.original_format.format = 'airistempdata';
r.defaultfilename.temp_data.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/and_2/temp_data/%Y/%m/%Y%m%d.txt';
r.defaultfilename.temp_data.original_format.loadfunction = '';
r.defaultfilename.temp_data.original_format.resolution = timespan([], 's');
r.defaultfilename.temp_data.original_format.savefunction = '';
r.defaultfilename.temp_data.original_format.size = [-1 -1];
r.defaultfilename.temp_data.original_format.units = 'C';
r.institutions{1} = 'Andoya Rocket Range';
% end of function
