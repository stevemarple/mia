function r = info_rio_zhs_1_data
%INFO_RIO_ZHS_1_DATA Return basic information about rio_zhs_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_zhs_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=zhs;serialnumber=1

r.limits = [];
r.limits.rio_power = [0 4096];
r.limits.rio_qdc = [0 4096];
r.limits.rio_rawpower = [0 4096];
r.limits.rio_rawqdc = [0 4096];
r.pixels.deg.x.max = 79.6;
r.pixels.deg.x.min = 73.3;
r.pixels.deg.x.step = 0.3;
r.pixels.deg.y.max = -68.3;
r.pixels.deg.y.min = -70.5;
r.pixels.deg.y.step = 0.1;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'zhs';
r.antennaazimuth = -76.8;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.5;
r.azimuth = [342.9 324.9 336.6 351.9 8.1 23.4 35.1 72.9 305.1 315 ...
    329.4 348.3 11.7 30.6 45 54.9 293.4 300.6 315 341.1 18.9 45 ...
    59.4 66.6 278.1 281.7 288.9 315 45 71.1 78.3 81.9 261.9 258.3 ...
    251.1 225 135 108.9 101.7 98.1 246.6 239.4 225 198.9 161.1 135 ...
    120.6 113.4 234.9 225 210.6 191.7 168.3 149.4 135 125.1 252.9 ...
    215.1 203.4 188.1 171.9 156.6 144.9 162.9];
r.badbeams = [1 8 57 64];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [13.7 14.6 16.5 16.2 16.2 16.5 14.6 13.7 14.6 16.4 ...
    15.2 14.4 14.4 15.2 16.4 14.6 16.5 15.2 13.9 13.3 13.3 13.9 ...
    15.2 16.5 16.2 14.4 13.3 12.9 12.9 13.3 14.4 16.2 16.2 14.4 ...
    13.3 12.9 12.9 13.3 14.4 16.4 16.5 15.2 13.9 13.3 13.3 13.9 ...
    15.2 16.5 14.6 16.4 15.2 14.4 14.4 15.2 16.4 14.6 13.7 14.6 ...
    16.5 16.2 16.2 16.5 14.6 13.7];
r.bibliography = 'inproc:604';
r.comment = 'Installed and operated jointly by National Institute for Polar Research (Japan) and Polar Research Insitute of China. PIs are Prof H Yamagishi and Prof H Yang';
r.credits = '';
r.datarequestid = 29;
r.defaultheight = 90000;
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1 9 13];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 17;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 ...
    62 63 64];
r.latitude = -69.37;
r.location1 = 'Zhongshan';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = 'pric_logo';
r.logurl = '';
r.longitude = 76.38;
r.modified = timestamp([2012 07 03 13 13 39.249302]);
r.name = '';
r.piid = [];
r.qdcclass = 'rio_rawqdc';
r.qdcduration = timespan(28 , 'd');
r.qdcoffset = timespan(00, 'h', 00, 'm', 00, 's');
r.resolution = timespan(00, 'h', 00, 'm', 04, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1.8129 2.70693 1.94549 1.7028 1.7028 1.94549 ...
    2.70693 1.8129 2.66306 1.72881 1.41405 1.25924 1.25924 1.41405 ...
    1.72881 2.66306 1.98609 1.39501 1.15198 1.07937 1.07937 1.15198 ...
    1.39501 1.98609 1.75749 1.27608 1.08342 1.01563 1.01563 1.08342 ...
    1.27608 1.75749 1.75749 1.27608 1.08342 1.01563 1.01563 1.08342 ...
    1.27608 1.75749 1.98609 1.39501 1.15198 1.07937 1.07937 1.15198 ...
    1.39501 1.98609 2.66306 1.72881 1.41405 1.25924 1.25924 1.41405 ...
    1.72881 2.66306 1.8129 2.70693 1.94549 1.7028 1.7028 1.94549 ...
    2.70693 1.8129];
r.starttime = timestamp([1997 02 01 00 00 00]);
r.systemtype = 'iris';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [58.5 70.2 62.1 56.7 56.7 62.1 70.2 58.5 70.2 56.7 45 ...
    38.7 38.7 45 56.7 70.2 62.1 45 31.5 22.5 22.5 31.5 45 62.1 56.7 ...
    38.7 22.5 9.9 9.9 22.5 38.7 56.7 56.7 38.7 22.5 9.9 9.9 22.5 ...
    38.7 56.7 62.1 45 31.5 22.5 22.5 31.5 45 62.1 70.2 56.7 45 38.7 ...
    38.7 45 56.7 70.2 58.5 70.2 62.1 56.7 56.7 62.1 70.2 58.5];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1 , 'd');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'niprriodata';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/original_format/%Y/%m/%y%m%d%H.ZHS';
r.defaultfilename.original_format.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.original_format.default.resolution = timespan([], 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 21600];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'niprriodata';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/original_format/%Y/%m/%y%m%d%H.ZHS';
r.defaultfilename.rio_power.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_power.default.resolution = timespan([], 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 21600];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 04, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [64 900];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan([], 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [64 -1];
r.defaultfilename.rio_qdc_fft.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'niprriodata';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/original_format/%Y/%m/%y%m%d%H.ZHS';
r.defaultfilename.rio_rawpower.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan([], 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 21600];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 04, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [64 900];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.defaultfilename.rio_rawqdc_fft.default.archive = 'default';
r.defaultfilename.rio_rawqdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc_fft.default.format = 'mat';
r.defaultfilename.rio_rawqdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/zhs_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc_fft.default.loadfunction = '';
r.defaultfilename.rio_rawqdc_fft.default.resolution = timespan([], 's');
r.defaultfilename.rio_rawqdc_fft.default.savefunction = '';
r.defaultfilename.rio_rawqdc_fft.default.size = [64 -1];
r.defaultfilename.rio_rawqdc_fft.default.units = 'ADC';
r.institutions = {};
% end of function
