function [mia, qdc, tries, p, q] = rio_starpower_with_qdc(varargin)
%RIO_STARPOWER_WITH_QDC Load RIO_POWER data, extracting radio star contribution.
%
%   [mia, qdc, tries] = RIO_STARPOWER_WITH_QDC(...)
%
%   mia: RIO_POWER object for the period/instrument requested
%   qdc: RIO_QDC object used to construct the RIO_POWER object (filtered)
%   tries: the number of tries made to find the RIO_QDC object used (0
%   means the supplied QDC was used)
%
% RIO_STARPOWER_WITH_QDC loads RIO_POWER data and subtracts the cosmic
% background contribution to obtain the power contribution from the selected
% radio source(s). For efficient processing the previously used QDC can be
% passed basck for reuse (if possible), thus avoiding unnecessary loading of
% QDCs (in the same manner as RIO_ABS_WITH_QDC operates).
%
% RIO_STARPOWER_WITH_QDC accepts the parameter name/value pairs accepted by
% RIO_POWER, and also the following parameters. Note that 'power' and 'qdc'
% are required to get the proper behaviour by this function.
%
%   'power', RIO_POWER
%   The previous RIO_POWER object returned by this function (pass an empty
%   matrix for the first function call). This information is used to
%   determine if the previous RIO_QDC can be used for the period now
%   requested.
%
%   'qdc', RIO_QDC
%   The previous RIO_QDC object returned by this function (pass an empty
%   matrix for the first function call). If possible the previous RIO_QDC
%   will be used to construct the RIO_POWER object requested (thus saving
%   time loading the files).
%
%   'maxqdctries' DOUBLE
%   An integer limiting the number of tries for loading QDCs. If maxqdctries
%   > 1 then RIO_STARPOWER_WITH_QDC will attept to load older QDCs if the
%   standard QDCs cannot be loaded. This feature is mainly intended for
%   loading recent data for which a QDC may not be present. If the value
%   returned for tries > 1 then the processing string of the data will
%   contain preliminary, and the data should not be considered
%   final. Default value is 1. An ERROR will result if maxqdctries is
%   exceeded.
%
%
% NOTE
%
% The 'loadoptions' parameter is intercepted and the value for
% 'failiffilemissing' is always set to 1 for the RIO_QDC object, and
% defaults to 0 for the RIO_POWER object, ie, a quiet day curve is always
% required but (unless overridden) data files are never required (missing
% data is indicated by NANs).
%
% See also RIO_POWER, RIO_QDC.

defaults.instrument = [];
defaults.qdc = [];
defaults.power = [];
defaults.starttime = [];
defaults.endtime = [];
defaults.maxqdctries = 1;
defaults.beams = [];
defaults.qdcfilter = [];
defaults.powerfilter = [];
defaults.threshold = -3;
defaults.sources = [];

defaults.log = 1;
defaults.loadoptions = {};
defaults.cancelhandle = [];

[defaults unvi] = interceptprop(varargin, defaults);
if isempty(defaults.instrument)
  error('instrument not specified');
end
if isempty(defaults.starttime)
  error('starttime not specified');
end
if isempty(defaults.starttime)
  error('endtime not specified');
end

if isempty(defaults.sources)
  if getgeolat(getlocation(defaults.instrument)) >= 0
    defaults.sources = 'cassiopeia_a';
  else
    defaults.sources = 'sagittarius_astar';
  end
end

if isempty(defaults.qdcfilter)
  defaults.qdcfilter = { ...
      mia_filter_remove_scintillation('sources', defaults.sources, ...
				      'method', 'antennagainmainlobe', ...
				      'threshold', defaults.threshold), ...
      mia_filter_replace_nans('interpmethod', 'linear')};
end

if isempty(defaults.powerfilter)
  defaults.powerfilter = { ...
      mia_filter_remove_scintillation('sources', defaults.sources, ...
				      'method', 'antennagainmainlobe', ...
				      'threshold', defaults.threshold, ...
				      'invertmask', true)};
end


qdcLoadOptions = {defaults.loadoptions{:}, 'failiffilemissing', 1};
powerLoadOptions = {'failiffilemissing', 0, defaults.loadoptions{:}};

tries = 0;

% only try loading from power + QDC if power data is available
if ~isempty(info(defaults.instrument, 'defaultfilename', 'rio_power'))

  qdcstarttime = calcqdcstarttime(defaults.instrument, defaults.starttime, ...
				  defaults.endtime);

  if ~isempty(defaults.power) 
    % attempt to determine if current QDC can be used again.
    if calcqdcstarttime(defaults.power) ~= qdcstarttime
      % find and load qdc
      defaults.qdc = [];
    end
  end

  % [filename interval] = info(defaults.instrument, 'defaultfilename', ...
  % 'rio_qdc', defaults.beams);
  s = info(defaults.instrument, 'defaultfilename', 'rio_qdc');
  
  if isempty(defaults.qdc)
    t = qdcstarttime;
    for tries = 1:defaults.maxqdctries
      % does the QDC exist for this time
      missing = 0;
      for n = 1:numel(defaults.beams)
	if ~url_exist(beamstrftime(t, defaults.beams(n), s.fstr))
	  missing = 1;
	  break;
	end
      end
      if ~missing
	% load QDC, don't log since we are really creating power data
	defaults.qdc = rio_qdc('starttime', defaults.starttime, ...
			       'endtime', defaults.endtime, ...
			       'beams', defaults.beams, ...
			       'instrument', defaults.instrument, ...
			       'load', 1, ...
			       'log', 0, ...
			       'loadoptions', qdcLoadOptions);
	break;
      end
      t = t - s.duration;
    end
  end

  if isempty(defaults.qdc)
    % still empty, then cannot have found one within permitted number of
    % tries
    error('Cannot load suitable QDC');
  end
  
end

% Filter the QDC (if any given)
if tries
  disp('filtering the QDC');
  defaults.qdc = mia_filter(defaults.qdc, defaults.qdcfilter);
end

% set logging to be whatever was originally requested
p = rio_power('starttime', defaults.starttime, ...
	      'endtime', defaults.endtime, ...
	      'beams', defaults.beams, ...
	      'instrument', defaults.instrument, ...
	      'load', 1, ...
	      'log', defaults.log, ...
	      'loadoptions', powerLoadOptions ,...
	      varargin{unvi});

q = align(defaults.qdc, p);

mia = subpower(p, q);

disp('filtering power');
mia = mia_filter(mia, defaults.powerfilter);

% if the incorrect QDC was used set the dataquality flag to preliminary
if tries > 1
  mia = adddataquality(mia, 'Preliminary');
end

qdc = defaults.qdc;
