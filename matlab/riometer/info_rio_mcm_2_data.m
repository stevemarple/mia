function r = info_rio_mcm_2_data
%INFO_RIO_MCM_2_DATA Return basic information about rio_mcm_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_mcm_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=mcm;serialnumber=2

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.pixels = [];
r.abbreviation = 'mcm';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [60];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 94;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -77.85;
r.location1 = 'McMurdo';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 166.67;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 28;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 10, 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [1];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 5.14e+07;
r.widebeams = [1];
r.zenith = [0];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'mat';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/mcm_2/rio_abs/%Y/%m/%Y%m%d.mat';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan([], 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [1 8640];
r.defaultfilename.rio_abs.default.units = 'dB';
r.institutions{1} = 'Siena College';
% end of function
