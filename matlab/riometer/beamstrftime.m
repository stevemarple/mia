function [r,rep] = beamstrftime(t, beam, fstr);
%BEAMSTRFTIME  Print time and beam number into a string.
%
%   [r, rep] = BEAMSTRFTIME(t, beam, fstr);
%   r: CHAR
%   rep: CELL, list of replacements made
%   t: TIMESTAMP
%   beam: beam number (numeric)
%   fstr: format specifier
%
% Similar to timestamp/STRFTIME but %Q may be used as a format specifier
% for the beam number.
%
% See also timestamp/STRFTIME.

s.Q = int2str(beam);
[r,rep] = strftime(t, fstr, 'additionalreplacements', s);

