function r = rio_abs(mia, qdc, varargin)
%RIO_ABS  Converter from RIO_POWER to RIO_ABS.
%
%   r = RIO_ABS(mia, qdc);
%   r: RIO_ABS object(s)
%   mia: RIO_POWER object(s)
%   qdc: RIO_QDC object(s)
%
%   See also rio_abs/RIO_ABS, RIO_POWER, RIO_QDC.

if ~isa(mia, 'rio_power') | ~isa(qdc, 'rio_qdc')
  % check for case when parameters have been swapped around
  error('incorrect parameters');
end


in = getinstrument(mia);
if in ~= getinstrument(qdc)
  mia = mia
  qdc = qdc
  error(sprintf('instruments differ (were ''%s'' and ''%s'')', ...
		char(in), char(getinstrument(qdc))));
end

pbeams = getbeams(mia);
qbeams = getbeams(qdc);
beams = intersect(pbeams, qbeams);
if isempty(beams)
  error('no common beams');
end
[data units proc obl oblf] = qdcsubpower(qdc, mia, beams, varargin{:});

if size(oblf, 2) == 1
  obliquityfactors = oblf;
else
  obliquityfactors = []; % don't save if func of absorption
end


r = rio_abs('starttime', getstarttime(mia), ...
	    'endtime', getendtime(mia), ...
	    'sampletime', getsampletime(mia), ...
	    'integrationtime', getintegrationtime(mia), ...
	    'instrument', in, ...
	    'beams', beams, ...
	    'data', data, ...
	    'units', units, ...
	    'load', 0, ...
	    'log', 0);

r = addprocessing(r, proc);
r = setobliquityfield(r, obl, obliquityfactors);

% add details of any dataquality issues from the rio_power and rio_qdc
% objects
r = adddataquality(r, getdataquality(mia));
r = adddataquality(r, getdataquality(qdc));

