function r = gettype(rio, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(rio)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_POWER, RIO_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'power';
  
 case 'u'
  r = 'POWER';
  
 case {'c' 'C'}
  r = 'Power';
  
 otherwise
  error('unknown mode');
end

return

