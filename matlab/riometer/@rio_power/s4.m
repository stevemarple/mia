function [r, rt] = s4(mia, windowsize, varargin)
%S4 Calculate the S4 scintillation index.
%   
%   [r t] = S4(mia, windowsize, ...)
%
%   r: S4 index (DOUBLE)
%   t: sample times for S4 values (TIMESTAMP)
%   mia: RIO_POWER object
%   windowsize: window size (TIMESPAN)
%
% Compute the S4 index (ignoring NANs). The following name/value pairs can
% be used to modify the default behaviour:
%
%   'slidingwindow', LOGICAL
%   Use a sliding window, default is FALSE. If slidingwindow is FALSE then
%   the first window will start at the object's start time, the next will
%   start at starttime+windowsize, etc until the end time is reached. The
%   last S4 value computed may use fewer samples if the object's duration is
%   not an exact multiple of windowsize. The first value of rt will be
%   starttime+(windowsize/2). If slidingwindow is TRUE then the
%   window is applied centrally over each sample in turn and the S4 index
%   computed. Thus rt will be the same as the input sample times.
%
% 
% For details on S4 see The Solar-terrestrial Environment (Hargreaves). The
% S4 index is computed as standard deviation / mean.
%
% See also S4PLOT, RIO_POWER.

defaults.slidingwindow = false;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

data = getdata(mia);
units = getunits(mia);

if isempty(windowsize)
  error('windowsize must be defined');
elseif ~isa(windowsize, 'timespan')
  error('windowsize must be a timespan object');
elseif numel(windowsize) ~= 1
  error('windowsize must be scalar');
end



switch units
 case 'dBm'
  % convert to linear power
  data = power(10, data ./ 10);

 case {'raw' 'ADC'}
  ; % do nothing, already linear
  
 otherwise
  error(sprintf('unknown units (were ''%s'')', units));
end

st = getstarttime(mia);
et = getendtime(mia);
duration = et - st;
samt = getsampletime(mia);


if logical(defaults.slidingwindow)
  % slide window along over every sample
  r = zeros(size(data));
  rt = repmat(timestamp, 1, numel(samt));
  ws_2 = windowsize ./ 2;
  for n = 1:numel(samt)
    idx = find(samt >= samt(n) - ws_2 & samt < samt(n) + ws_2);
    r(:, n) = localS4(data(:, idx));
    rt(n) = mean(samt(idx));
  end
  
else
  % load block by block, starting at start time
  blocks = ceil(duration ./ windowsize);
  r = zeros(size(data, 1), blocks);
  rt = repmat(timestamp, 1, blocks);
  t1 = st;
  n = 0;
  while t1 < et
    n = n + 1;
    t2 = t1 + windowsize;
    
    % Find all samples within the window
    idx = find(samt >= t1 & samt < t2);
    r(:, n) = localS4(data(:, idx));
    rt(n) = mean(samt(idx));
    
    t1 = t2;
  end
end

function r = localS4(d)
r = zeros(size(d, 1), 1);
for n = 1:size(d, 1)
  d2 = d(n, :);
  d2(isnan(d2)) = []; % delete nans
  if isempty(d2)
    r(n) = nan;
  else
    r(n) = std(d2, 0, 2) ./ mean(d2, 2);
  end
end

function [s4, cnt, sm, smsq] = sliding_s4(cnt, sm, smsq, insert, remove)
% s4: s4 value
% cnt: count (number of samples in use)
% sm: sum of samples
% smsq: sum of the squared samples
% insert: array of samples to add
% remove: array of samples to remove

% Ignore any NaNs
insert(isnan(insert)) = [];
remove(isnan(remove)) = [];

sm = sm + sum(insert) - sum(remove); % Adjust sum
cnt = cnt + numel(insert) - numel(remove); % Adjust count
mn = sm ./ cnt; % Calculate new mean

