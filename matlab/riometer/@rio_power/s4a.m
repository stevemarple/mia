function [r, rt] = s4a(mia, windowsize, varargin)

defaults.slidingwindow = false;
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

data = getdata(mia);
units = getunits(mia);

if isempty(windowsize)
  error('windowsize must be defined');
elseif ~isa(windowsize, 'timespan')
  error('windowsize must be a timespan object');
elseif numel(windowsize) ~= 1
  error('windowsize must be scalar');
end



switch units
 case 'dBm'
  % convert to linear power
  data = power(10, data ./ 10);

 case {'raw' 'ADC'}
  ; % do nothing, already linear
  
 otherwise
  error(sprintf('unknown units (were ''%s'')', units));
end

st = getstarttime(mia);
et = getendtime(mia);
duration = et - st;
samt = getsampletime(mia);


if logical(defaults.slidingwindow)
  % slide window along over every sample
  r = zeros(size(data));
  rt = repmat(timestamp, 1, numel(samt));
  ws_2 = windowsize ./ 2;
  for n = 1:numel(samt)
    idx = find(samt >= samt(n) - ws_2 & samt < samt(n) + ws_2);
    r(:, n) = localS4(data(:, idx));
    rt(n) = mean(samt(idx));
  end
  
else
  % load block by block, starting at start time
  blocks = ceil(duration ./ windowsize);
  r = zeros(size(data, 1), blocks);
  rt = repmat(timestamp, 1, blocks);
  t1 = st;
  n = 0;
  while t1 < et
    n = n + 1;
    t2 = t1 + windowsize;
    
    % Find all samples within the window
    idx = find(samt >= t1 & samt < t2);
    r(:, n) = localS4(data(:, idx));
    rt(n) = mean(samt(idx));
    
    t1 = t2;
  end
end

disp('fix to use nonanstd');


function r = localS4(d)
r = std(d, 0, 2) ./ nonanmean(d, 2);
