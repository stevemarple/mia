function [fh, gh, ph] = plotq(mia, varargin)
%PLOTQ Plot RIO_POWER with the QDC superimposed.
%
%   [fh gh] = PLOTQ(mia, ...);
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   mia: RIO_POWER object to plot
%
%   'legend', LOGICAL
%   Flag controlling whether a LEGEND is added to the plot. Default is to
%   add a LEGEND.
%
%   'legendposition', DOUBLE
%   Argument to the LEGEND command, to determine placement of the LEGEND.
%
%   'legendboxvisible', LOGICAL
%   'Flag to indicate whther the box around the LEGEND should be visible.
%   
%   'ylim', [1x2 DOUBLE]
%   Limits used by rio_power/PLOT and rio_qdc/PLOT
%
%   'qdc', RIO_QDC
%   'qdc', TIMESTAMP
%   'qdc', CHAR
%   By default the standard QDC for the RIO_POWER data is plotted, but an
%   alternative QDC may be used, specified either by providing a QDC
%   object, a TIMESTAMP object or a BEAMSTRFTIME path.
%
%   'qdcontop' LOGICAL
%   By default the QDC is plotted over the RIO_POWER data.
%
%   See also LEGEND.

st = getstarttime(mia);
et = getendtime(mia);
% res = getresolution(mia);
in = getinstrument(mia);
beams = getbeams(mia);

% our default values
defaults.miniplots = [1 1];
% defaults.ylim = []; % defer calculation of automatic limits until later
defaults.ylim = info(in, 'limits', mia);
defaults.legend = 1;
defaults.legendboxvisible = 1;
if matlabversioncmp('ge', [9, 5])
  defaults.legendlocation = 'best';
else
  defaults.legendposition = 0;
end
defaults.qdc = [];
defaults.qdcontop = 1; % QDC plotted on top of power

% choose default beam
defaults.beams = info(in, 'preferredbeam', ...
                      'beams', beams, ...
                      'time', getmidtime(mia));

defaults.miniplots = [1 1]; % x,y

[defaults unvi] = interceptprop(varargin, defaults);

% ensure beams is a row vector
defaults.beams = defaults.beams(:)';

[fh gh ph1] = plot(mia, varargin{unvi}, ...
                   'beams', defaults.beams, ...
                   'ylim', defaults.ylim);

if isa(mia, 'rio_power')
  qdcclass = 'rio_qdc';
  powerclass = 'rio_power';
elseif isa(mia, 'rio_rawpower')
  qdcclass = 'rio_rawqdc';
  powerclass = 'rio_rawpower';
else
  error('unknown class, cannot get QDC class');
end

if isempty(defaults.qdc)
  defaults.qdc = feval(qdcclass, ...
		       'time', st, ...
		       'instrument', in, ...
		       'beams', defaults.beams);
elseif ischar(defaults.qdc)
  % Must be a filename fstr
  defaults.qdc = feval(qdcclass, ...
		       'time', st, ...
		       'instrument', in, ...
		       'beams', defaults.beams, ...
		       'filename', defaults.qdc);  
elseif isa(defaults.qdc, 'timestamp')
  defaults.qdc = feval(qdcclass, ...
		       'time', defaults.qdc, ...
		       'instrument', in, ...
		       'beams', defaults.beams)  
else
  % only align the beams we need
  defaults.qdc = extract(defaults.qdc, 'beams', defaults.beams);
end


% not all are stored to best resolution
% defaults.qdc = setresolution(defaults.qdc, timespan(1,'s'));

miaq = align(defaults.qdc, st, et);

if isa(miaq, 'double')
  % construct from parts, since align is still returning a double
  tmpmiaq = feval(powerclass, ...
		  st, et, timespan(1,'s'), loc, ...
		  repmat(nan, length(defaults.beams), ...
			 (et-st)/timespan(1,'s')));
  tmpmiaq = setbeams(tmpmiaq, defaults.beams);
  miaq_bi = getparameterindex(defaults.qdc, defaults.beams);
  tmpmiaq_bi = getparameterindex(tmpmiaq, defaults.beams);
  tmpmiaq.power(tmpmiaq_bi, :) = miaq(miaq_bi, :);
  miaq = tmpmiaq;
  clear tmpmiaq;
end
% miaq = setresolution(miaq, res);

set(gh, 'NextPlot', 'add');


[fh2 gh2 ph2] = plot(miaq, varargin{unvi}, ...
                     'beams', defaults.beams, ...
                     'plotaxes', gh, ...
                     'ylim', defaults.ylim, ...
                     'color', 'red');

if defaults.legend
  if matlabversioncmp('ge', [9, 5])
	lh = legend({'RX Power', 'QDC'}, 'Location', ...
				defaults.legendlocation);
  else
	lh = legend({'RX Power', 'QDC'}, defaults.legendposition);
  end
  if defaults.legendboxvisible
    set(lh, 'Visible', 'on');
  else
    set(lh, 'Visible', 'off');
  end
  % set legend text to be same size as the axes text, with the same
  % properties so that it scales in the same way
  p = get(gh(1));
  lth = findall(lh, 'Type', 'text');
  set(lth, ...
      'FontUnits', p.FontUnits, ...
      'FontName', p.FontName, ...
      'FontSize', p.FontSize, ...
      'FontWeight', p.FontWeight);
end


% plot watermark should show data quality details for mia and miaq
wmh = findall(gh, 'Type', 'text', 'Tag', 'watermark');
dq = combinedataquality(mia, miaq);
set(wmh, 'String', dq);
delete(wmh(2:end)); % ensure only 1 watermark!

ph = [ph1(:); ph2(:)];

if logical(defaults.qdcontop)
  zq = 1;
  zp = 0;
else
  zq = 0;
  zp = 1;
end

% set ZData for power
for n = 1:numel(ph1)
  set(ph1(n), 'ZData', repmat(zp, size(get(ph1(n), 'XData'))));
end

% set ZData for QDC
for n = 1:numel(ph2)
  set(ph2(n), 'ZData', repmat(zq, size(get(ph2(n), 'XData'))));
end
