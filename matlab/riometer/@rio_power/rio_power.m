function mia = rio_power(varargin)
%RIO_POWER  Constructor for RIO_POWER class.
%
%   r = RIO_POWER;
%   default constructor
%
%   r = RIO_POWER(mia)
%   copy constructor
%
%   r = RIO_POWER(...)
%   constructor taking parameter name/value pairs
%
%   The following parameters are accepted.
%
%     'starttime', [1x1 TIMESTAMP]
%     'endtime',  [1x1 TIMESTAMP] 
%     Start and end times of the RIO_ABS object.
%
%     'resolution', [1x1 TIMESPAN]
%     Resolution of the data, that is, how often the samples are repeated.
%
%     'instrument', [1x1 RIOMETER]
%     The instrument type must always be a RIOMETER instrument.
%
%     'beams', DOUBLE vector
%     The beams for which data should be loaded/created.
%
%
%   When loading existing data the above parameters should be considered
%   mandatory. Other parameters which may be useful for creating new
%   objects are:
%
%     'data', DOUBLE
%      One row per beam (in same order as beams matrix), and one
%      column per sample.
%
%     'units', CHAR
%     The units of the data matrix. This should normally be dB.
%
%     'integrationtime', [1x1 TIMESPAN]
%     The integration time, per sample. Normally this will not be
%     larger than the resolution.
%
%
%   Other parameters may be specified which modify the way in which data
%   is loaded or created.
%
%     'load', [boolean or CHAR]
%     A flag indicating if data should be loaded from disk (either
%     directly or by computation from QDC and power data, as defined by
%     the appropriate riometer/INFO function).
%
%     'resolutionmethod', [CHAR]
%     The method to use when changing resolution. Valid options include
%     'mean', 'nonanmean', 'median' and 'nonanmedian'. See SETRESOLUTION
%     for more details.
%
%     'failiffilemissing', [boolean or CHAR]
%     A flag indicating if load of existing absorption data should fail
%     (or not) if a file is missing. The default behaviour is defined in
%     the appropriate riometer/INFO file. An empty matrix forces the
%     default behaviour.
%
%     'cancelhandle' [GUI handle]
%     Used by LOADDATA to detect if the cancel button has been
%     pressed when data is loaded through graphical user interface
%     routines. This is not intended for normal user routines.


cls = 'rio_power';

% Make it easy to change the class definition at a later date
latestversion = 1;
mia.versionnumber = latestversion;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  parent = rio_base;
  mia = class(mia, cls, parent);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'rio_base')
  % construct from base object
  parent = varargin{1};
  mia = class(mia, cls, parent);
  
elseif nargin == 1 & (isa(varargin{1}, cls) | isstruct(varargin{1}))
  % copy constructor / converter to base class    
  parent = rio_base;
  mia = class(mia, cls, parent);

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions', 'log'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  parent = rio_base(varargin{unvi});
  mia = class(mia, cls, parent);

  if isempty(defaults.load)
    defaults.load = isdataempty(parent);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end


% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
