function [r,data_r] = subpower(a, b, varargin)
%SUBPOWER  Subtract A RIO_POWER object from another RIO_POWER object
%
%   mia = SUBPOWER(a, b)
%  a, b: RIO_POWER objects
%
% SUBPOWER subtracts one RIO_POWER object from another. The subtraction
% takes into account the possibility that the power is stored in dBm (log
% scale).
%
% See also RIO_POWER.

%if isa(a, 'rio_power') & isa(b, 'rio_power')
data_a = getdata(a);
data_b = getdata(b);
%elseif isnumeric(a) & isnumeric(b)
%  data_a = a;
%  data_b = b;
  
units = getunits(a);
if ~strcmp(units, getunits(b))
  error('units differ');
end
 
if getinstrument(a) ~= getinstrument(b)
  error('instruments differ');
end

switch units
 case 'dBm'
  tmp = power(10, data_a / 10) - power(10, data_b / 10);
  % tmp(tmp == 0) = nan;
  tmp(tmp <= 0) = nan;
  data_r = 10 * log10(tmp);
  
 case {'raw' 'ADC'}
  data_r = data_a - data_b;
  
 otherwise
  error(sprintf('unknown units (was ''%s'')', units));
end

r = setdata(a, data_r);

  
  
