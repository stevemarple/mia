function r = rio_base(rio)
%RIO_BASE  Extract the parent RIO_BASE object from a RIO_POWER object.
%
%   r = RIO_BASE(rio)
%   r: RIO_BASE object
%   rio: RIO_POWER object
%
%   See also RIO_BASE, RIO_POWER.

r = rio.rio_base;

