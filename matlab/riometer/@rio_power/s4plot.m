function [fh, gh, lh] = s4plot(mia, windowsize, varargin)
%S4PLOT Plot the S4 scintillation index for a RIO_POWER object.
%
% [fh gh lh] = S4PLOT(mia, windowsize, ...)
%
% fh: FIGURE handle
% gh: AXES handle(s)
% lh: LINE handle(s)
% mia: RIO_POWER object
% windowsize: window size used for S4 calculation
%
% S4PLOT plots the S4 index, see S4 for more details. The following
% name/value pairs may be used to modify the default behaviour:
%
%   'beams', NUMERIC
%   Restrict plotting to a subset of beams. Default to plotting all
%   available beams.
%
%   'starttime, TIMESTAMP
%   'endtime', TIMESTAMP
%   Restrict plotting to a subset based on start/end time. Defaults to
%   plotting the whole available time.
%
%   'miniplots', [1 x 2] NUMERIC
%   The number of axes to create. Defaults to [1 1].
%
%   'plotaxes', NUMERIC
%   Handle to existing axis/axes. Default is empty, meaning create new
%   FIGURE (using MAKEPLOTFIG).
%
%   'slidingwindow', LOGICAL
%   Flag to indicate if a sliding window should be used in the S4
%   calculation.
%
%   'color',  CHAR or [1 x 3] NUMERIC
%   Color used for the lines. See COLORSPEC for more details.
%
% S4PLOT uses rio_power/S4 to compute the S4 index. See S4 for more
% details. Unknown name/value pairs are passed onto MAKEPLOTFIG, see
% MAKEPLOTFIG for details.
%
% See also S4, MAKEPLOTFIG, RIO_POWER.

rio = getinstrument(mia);
beams = getbeams(mia);

% our default values
defaults.beams = []; % defer choice
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
defaults.title  = ''; % defer
defaults.windowtitle = ''; % defer
defaults.miniplots = [1 1]; % x,y
defaults.plotaxes = [];
defaults.slidingwindow = false;
defaults.color = '';

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.beams)
  defaults.beams = info(rio, 'preferredbeam', ...
                        'beams', beams, ...
                        'time', defaults.starttime);
end

mia2 = extract(mia, ...
               'beams', defaults.beams, ...
               'starttime', defaults.starttime, ...
               'endtime', defaults.endtime);

% calculate title now that beams are known
[title windowtitle] = maketitle(mia2, 'beams', defaults.beams, ...
    'customstring', 'S4 scintillation index', ...
    'comment', ['S4 window ' char(windowsize, 'c')]);

if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

if isempty(defaults.plotaxes)
  % Get a figure complete with menus etc
  [fh gh] = makeplotfig('init', ...
                        'miniplots', defaults.miniplots, ...
                        'title', defaults.title, ...
                        'name', defaults.windowtitle, ...
                        'pointer', 'watch', ...
                        varargin{unvi});
else
  gh = defaults.plotaxes;
  fh = get(gh(1), 'Parent');
  set(fh, 'Pointer', 'watch');
end

[s4val s4t] = s4(mia2, windowsize, ...
                 'slidingwindow', defaults.slidingwindow);

numBeams = numel(defaults.beams);

if numel(gh) == 1
  lh = plot(s4t, s4val, 'Parent', gh);
  
  if numBeams > 1
    % Add legend
    for n = 1:numBeams
      legstr{n} = sprintf('%d', defaults.beams(n));
    end
    legend(gh, legstr);
  end
  set(get(gh, 'YLabel'), ...
      'String', '\bf{S4 index}', ...
      'FontSize', 14);
  xlim(gh, [defaults.starttime defaults.endtime]);
  timetick2(gh, 'label', true);
else

  bn = 0;
  for gy = 1:size(gh, 1)
    for gx = 1:size(gh, 2)
      bn = bn + 1;
      if bn <= numBeams
        % beam = defaults.beams(bn);
        lh(bn) = plot(s4t, s4val(bn, :), 'Parent', gh(gx, gy));
        legend(gh(gx, gy), {sprintf('%d', defaults.beams(bn))});
        
        set(get(gh(gx, gy), 'YLabel'), ...
            'String', '\bf{S4 index}', ...
            'FontSize', 14, ...
            'Visible', 'off');
        xlim(gh(gx, gy), [defaults.starttime defaults.endtime]);
        timetick2(gh(gx, gy), 'label', false);
      end
    end
  end
end

for n = 1:numel(lh)
  set(lh(n), 'Tag', sprintf('S4 scintillation index beam %d', ...
                            defaults.beams(n)));
end

if ~isempty(defaults.color)
  set(lh, 'Color', defaults.color);
end

set(fh, 'Pointer', 'arrow');

