function r = rio_novolazarevskaya
%RIO_NOVOLAZAREVSKAYA  RIOMETER object for Novolazarevskaya.

% http://www.sprl.umich.edu/mist/star/Ant_riometers.html
r = riometer('name', '', ...
	     'abbreviation', 'nvl', ...
	     'location', location('Novolazarevskaya', 'Antarctica', ...
				  -70.77, 11.83), ...
	     'frequency', nan);


