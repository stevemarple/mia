function [r, units] = linearise_rio_kil_1(mia)
%LINEARISE_RIO_KIL_1  Linearise riometer data from rio_kil_1.
%
%   r = LINEARISE_RIO_KIL_1(mia)
%   r: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

r = repmat(nan, (getdatasize(mia)));
units = 'dBm';
if isempty(r)
  return
end

if matlabversioncmp('>=', '5.2')
  persistent linearisetable;
else
  % make sure it exists
  linearisetable = [];
end
if isempty(linearisetable)
  % first time round (or every time round for versions < 5.2)
  in = getinstrument(mia);
  code = sprintf('%s_%d', getabbreviation(in), getserialnumber(in));

  if matlabversioncmp('>=', '5.2')
    disp(['loading linearisation table for ' code]);
  end
  load(['linearise_rio_table_' code]); 
end

% convert any values not in the valid range to nan
d = round(double(getdata(mia)));
d(d <= 0) = nan;
% d(find(d(:,:) > size(linearisetable,2))) = nan;
d(d > 4095) = nan;
d(~isfinite(d)) = nan;


beams = getbeams(mia);
bi = getparameterindex(mia, beams);

receiver = rem(beams,7);
receiver(find(receiver == 0)) = 7; % beam 7,14,.. is 7, not 0

% widebeam is recorded on all channels and averaged as part of the
% processing - so do the same here.
wbi = getparameterindex(mia, 50);
if wbi ~= 0
  receiver(wbi) = 8;
end


if 0
for n = 1:length(beams)
  % find all valid data
  validDataIndex = find(~isnan(d(bi(n),:)));
  if ~isempty(validDataIndex)
    % only linearise if there is data to do
    d(bi(n),validDataIndex) = ...
	linearisetable(receiver(n), d(bi(n),validDataIndex));
  end
end

end

samt = getsampletime(mia);
% linearisetable is an array of structs
% starttime: earliest time linearisation table valid (inclusive)
% endtime:  latest time linearisation table valid (exclusive)
% table: the linearisation table data
for tn = 1:numel(linearisetable)
  timemask = ones(size(samt));
  if isvalid(linearisetable(tn).starttime)
    timemask = timemask & (samt >= linearisetable(tn).starttime);
  end
  if isvalid(linearisetable(tn).endtime)
    timemask = timemask & (samt < linearisetable(tn).endtime);
  end
    
  if any(timemask)
    % some samples fall into this period
    
    for n = 1:length(beams)
      % find all valid data
      validDataIndex = find(~isnan(d(bi(n),:)) & timemask);
      if ~isempty(validDataIndex)
        % only linearise if there is data to do
        r(bi(n),validDataIndex) = ...
            linearisetable(tn).table(receiver(n), d(bi(n),validDataIndex));
      end
    end
    
    if timemask(end)
      % the last sample just linearised is the last sample in the dataset
      break; 
    end
  end
end











