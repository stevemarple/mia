function r = setbeams(rio, beams)
%SETBEAMS  Set the beams of an RIO_base object.
%
%   SETBEAMS(rio, beams)
%   rio: rio_base object
%   beams: the new beams (DOUBLE vector)
%
%   This functions should be considered 'protected', and only used by
%   member functions of RIO_BASE and derived classes.
%
%   See also rio_base/GETBEAMS.

% Ensure that the value we are passed is a vector (prefer row vectors but
% accept column vectors) and is unique (and hence sorted)

if ~isnumeric(beams) || length(beams) ~= numel(beams) 
  error(sprintf('beams is not a numeric vector (was %s)', ...
                matrixinfo(beams)));
elseif any(beams ~= fix(beams))
  error('beam numbers must be integers');
elseif ~isequal(unique(beams), beams)
  error(sprintf('beams are not unique (was [%s])', printseries(beams)));
end

r = rio;
r.beams = double(beams(:)');
