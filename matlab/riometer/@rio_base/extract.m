function [r,sr,s] = extract(mia, varargin)

% if mia is non-scalar process each element independently
if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

% from this point on mia is scalar
[r,sr,s] = mia_base_extract(mia, varargin{:});

if ~strcmp(sr.subs{1}, ':')
  r.beams = r.beams(sr.subs{1});
end



