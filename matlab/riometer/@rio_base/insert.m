function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  INSERT function for RIO_BASE objects.
%
% See mia_base/INSERT.


[r aa ar ba br rs] = mia_base_insert(a, b);

r = setbeams(r, rs.beams);
