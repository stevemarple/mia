function [d, units] = linearise_rio_dmh_1(rio)
%LINEARISE_RIO_DMH_1  Linearise riometer data from Danmarkshavn.
%
%   d = LINEARISE_RIO_DMH_1(rio)
%   d: linearised data
%   rio: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

d = getdata(rio);
if isa(d, 'uint16')
  d(d >= 4095) = 0; % map to nan
end

d = double(d);
d(d == 0) = nan; % any power that was 0 must be replaced with nan
units = 'ADC';
