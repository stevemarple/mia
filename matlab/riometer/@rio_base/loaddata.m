function r = loaddata(in, varargin)
%LOADDATA  Private function to load data derived from RIO_BASE.
%


r = [];


% accept qdctries but do nothing with it here (it is only valid for QDC
% objects, but accepting it here prevents warning by
% mia_base/loaddata.

defaults.qdctries = [];
[defaults unvi] = interceptprop(varargin, defaults);


% some sanity tests
instrument = getinstrument(in);
if isempty(instrument)
  error('instrument not specified');
end

beams = getbeams(in);
% [imagingBeams wideBeams] = info(instrument, 'beams');
% allBeams = [imagingBeams(:); wideBeams(:)];
allBeams = info(instrument, 'allbeams');

if isempty(beams)
  beams = allBeams;
  in = setbeams(in, beams);
end

units = getunits(in);
cls = class(in);

r = mia_base_loaddata(in, varargin{unvi}, ...
		      'createargs', {'beams', beams}, ...
                      'extractargs', {'beams', beams});

