function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  EXTRACTCHECK for RIO_BASE.
%
% See mia_base/EXTRACTCHECK.

if length(varargin) == 1
  % subscript interface
  [sr s] = mia_base_extractcheck(mia, varargin{:});
  return
end  


% parameter name/value interface

% accept beams and parameters as aliases
defaults.beams = [];
defaults.parameters = defaults.beams;

[defaults unvi] = interceptprop(varargin, defaults, ...
				{'beams', 'parameters'});

[sr s] = mia_base_extractcheck(mia, varargin{unvi});

if ~isempty(defaults.beams)
  defaults.beams = unique(defaults.beams);
  bi = getparameterindex(mia, defaults.beams);
  if any(bi == 0)
    error(sprintf(['beam number(s) %s do not exist in this object ' ...
		   '(beams are [%s])'], ...
		  printseries(defaults.beams(bi == 0)), ...
		  printseries(getbeams(mia))));
  end

  if strcmp(sr.subs{1}, ':')
    sr.subs{1} = bi;
  else
    sr.subs{1} = intersect(sr.subs{1}, bi);
  end

end

