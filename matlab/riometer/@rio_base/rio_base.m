function mia = rio_base(varargin)
%RIO_BASE  Constructor for RIO_BASE class.
%
%
%   Abstract base class.

cls = 'rio_base';
parent = 'mia_base';

% Make it easy to change the class definition at a later date
latestversion = 2;
mia.versionnumber = latestversion; % integer

mia.beams = [];
% mia.data = []; % not in version 2

if nargin == 0 | (nargin == 1 && isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 && strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};

elseif nargin == 1 && isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {parent, 'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {parent});
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    an = a(n);
    for m = 1:length(fn)
      % tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
      tmp = setfield(tmp, fn{m}, getfield(an, fn{m}));
    end

    base = getfield(an, parent); % parent class should already be current
    switch tmp.versionnumber
     case 1
      % data field in rio_base, set in parent
      base = setdata(base, an.data);
     case 2
      ; % latest
     otherwise
      error('unknown version');
    end
    tmp = class(tmp, cls, base);
    mia(n) = tmp;
  end

elseif rem(nargin, 2) == 0 && all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [mia unvi] = interceptprop(varargin, mia);
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave parent to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
