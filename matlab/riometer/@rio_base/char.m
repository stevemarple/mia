function r = char(mia, varargin)
%CHAR  Convert a RIO_BASE object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

datatype_str = gettype(mia);
if ~isempty(datatype_str)
  datatype_str = ['riometer ' datatype_str];
end

r = sprintf(['%s' ...
	     'data type         : %s\n' ...
	     'beams             : %s\n'], ...
 	    mia_base_char(mia, varargin{:}), datatype_str, ...
	    printseries(mia.beams));
    



