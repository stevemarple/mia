function  h = map(mia, ah, varargin)
%MAP  Map RIO_BASE object(s) onto a map.
%
%   h = MAP(mia, ...)
%
%

h = [];

if length(mia) ~= 1
  [tmp mfname] = fileparts(mfilename);
  for n = 1:prod(size(mia))
    tmph = feval(mfname, mia(n), ah, ...
		 varargin{:});
    h = [h; tmph];
  end
  return;
end



st = getstarttime(mia);
res = getresolution(mia);

instrument = getinstrument(mia);
[imagingBeams wideBeams] = info(instrument, 'beams');
% default to using only imaging beams if possible
beams = getbeams(mia);
defaults.beams = intersect(beams, imagingBeams);
if isempty(defaults.beams)
  defaults.beams = beams;
end

defaults.axes = [];
defaults.colormap = [];
defaults.map = 'scand';
defaults.time = 1;

defaults.clim = getlimits(mia);
defaults.patchfunction = '';

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.colormap)
  defaults.colormap = get(get(ah,'Parent'),'Colormap');
end

if isa(defaults.time, 'timestamp')
  % convert to sample number
  defaults.time = round((defaults.time - st) / res);
end


% since beams may have more than one handle plot beam by beam
numOfBeams = prod(size(defaults.beams));
bi = getparameterindex(mia, defaults.beams);
data = getdata(mia, bi, defaults.time);

for n = 1:numOfBeams
  cdata = data(n, :);
  patchArgs = {'FaceColor', cdata, ...
	       'EdgeColor', 'none'};

  patchArgs = {'FaceColor', 'flat', ...
	       'EdgeColor', 'none', ...
	       'CData', cdata, ...
	       'CDataMapping', 'scaled'};

  tmph = plotbeamprojection(instrument, ah, ...
			 'beams', defaults.beams(n), ...
			 'units', 'deg', ...
			 varargin{unvi}, ...
			 'style', 'patch', ...
			 'patchargs', patchArgs);
  h = [h; tmph];
end


