function [fh,gh,ph] = plot(mia, varargin)
%PLOT  Plot data from a RIO_BASE object.
%
%   [fh gh ph] = PLOT(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   ph: LINE handle(s)
%
% PLOT plots RIOMETER data into a new FIGURE (created by MAKEPLOTFIG). The
% following parameter name/value pairs may be used to alter the default
% plotting behaviour.
%
%   'beams', NUMERIC
%   Select the beam(s) to plot. By default the preferred beam for a given
%   RIOMETER is used (see riometer/INFO2 for details about preferred
%   beam). If only one plot axes is in operation and mutliple beams are
%   selected then they are all plotted on the same axes, otherthewise the
%   number of beams chosen should match the number of available plot
%   axes.
%
%   'color', COLORSPEC
%   Select the color used for the plotted data. See COLORSPEC for valid
%   methods to specify the color.
%
%   'miniplots' [x y]
%   Adjust the number of plot axes used. A [1x2] NUMERIC array is
%   required, specifying the number of plots in the x and y directions.
%   
%   'plotaxes', AXES handle(s)
%   Do not create a new FIGURE, instead plot into the selected AXES. The
%   number of AXES handles should match the number of beams selected.
%
%   'ylim', [1x2 NUMERIC]
%   Adjust the Y limits used when plotting.
%
% Any unknown parameters are passed onto MAKEPLOTFIG.
%
% See also GETBEAMS, GETDATA, MAKEPLOTFIG, INFO.

instrument = getinstrument(mia);
[imagingBeams, wideBeams] = info(instrument, 'beams');

beams = getbeams(mia);

% accept beams and parameters as aliases
defaults.beams = [];
defaults.parameters = defaults.beams;

[defaults unvi] = interceptprop(varargin, defaults, ...
				{'beams', 'parameters'});

if isempty(defaults.beams)
  defaults.beams = info(instrument, 'preferredbeam', 'beams', beams, ...
			'time', getmidtime(mia));
end

missingbeams = setdiff(defaults.beams, beams);
if ~isempty(missingbeams)
  error(sprintf('cannot plot, beam(s) [%s] not in data', ...
		printseries(missingbeams)));
end


[fh,gh,ph] = mia_base_plot(mia, varargin{unvi(:)}, ...
				       'parameters', defaults.beams);

s = char(instrument, 'c');
for n = 1:numel(ph)
  set(ph(n), 'Tag', sprintf('%s beam %d @%s', s, defaults.beams(n), ...
			 printunits(getfrequency(instrument),'Hz')));
end

