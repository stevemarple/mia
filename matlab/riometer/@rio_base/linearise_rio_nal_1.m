function [d, units] = linearise_rio_nal_1(rio)
%LINEARISE_NAL  Linearise riometer data from nal.
%
%   d = LINEARISE_NAL(rio)
%   d: linearised data
%   rio: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

d = double(getdata(rio));
d(d == 0) = nan; % any power that was 0 must be replaced with nan
units = 'ADC';
