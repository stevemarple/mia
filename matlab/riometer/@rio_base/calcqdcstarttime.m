function [st,et] = calcqdcstarttime(mia)
%CALCQDCSTARTTIME  Calculate the start time of the required QDC.
%
%   [st et] = CALCQDCSTARTTIME(mia)
%   st: start time of required RIO_QDC object (TIMESTAMP)
%   et: start time of required RIO_QDC object (TIMESTAMP)
%   mia: RIO_BASE object
%
%   This function is just a wrapper to riometer/CALCQDCSTARTTIME.
%
%   See also riometer/CALCQDCSTARTTIME.

[st,et] = calcqdcstarttime(getinstrument(mia), getstarttime(mia), ...
			   getendtime(mia));


