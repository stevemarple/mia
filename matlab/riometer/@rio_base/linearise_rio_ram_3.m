function [d, units] = linearise_rio_ram_3(mia)
%LINEARISE_RIO_RAM_3  Linearise riometer data from rio_ram_3.
%
%   d = LINEARISE_RIO_RAM_3(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

% From comparison with Kilpisjarvi IRIS adjust the power by an empirical
% value

d = info(getinstrument(mia), 'converttodbm', getdata(mia), ...
         getstarttime(mia));
units = 'dBm';
% data = getdata(mia);
% data(data == 0) = nan; % avoid taking logs of zero
% d = 10 .* log10(data) + ...
%     info(getinstrument(mia), 'dbmconversion', getstarttime(mia));
% units = 'dBm';
