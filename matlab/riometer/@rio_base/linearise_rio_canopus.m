function [d, units] = linearise_rio_canopus(mia)
%LINEARISE_KIL  Linearise riometer data from kil.
%
%   d = LINEARISE_KIL(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

in = getinstrument(mia);
% code = sprintf('%s_%d', getabbreviation(in), getserialnumber(in));
code = lower(getfacility(in));

if matlabversioncmp('>=', '5.2')
  persistent linearisetable;
else
  % make sure it exists
  linearisetable = [];
end
if isempty(linearisetable)
  % first time round (or every time round for versions < 5.2)
  if matlabversioncmp('>=', '5.2')
    disp(['loading linearisation table for ' code]);
  end
  load(['linearise_rio_table_' code]); 
end


if isa(mia, 'rio_rawpower')
  rp = double(getdata(mia));
elseif isa(mia, 'rio_rawqdc')
  rp = round(getdata(mia));
else
  error('incorrect class');
end
d = repmat(nan, size(rp));

idx = rp > 0 & rp <= length(linearisetable); % only look up good values
% convert millivolt values to dBm
d(idx) = linearisetable(rp(idx));
units = 'dBm';
 
% % table is from 0 to 5V
% len = length(linearisetable);
% rp = double(getdata(mia));

% d = repmat(nan, size(rp));
% valid = isfinite(rp);
% d(valid) = interp1([1:len] * (5/len), linearisetable, rp(valid));

% units = 'dBm';
