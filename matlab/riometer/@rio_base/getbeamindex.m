function r = getbeamindex(varargin)
%GETBEAMINDEX Return the indices for the beams requested.


r = getparameterindex(varargin{:});
