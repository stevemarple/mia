function [r, units] = linearise_lancsrio(mia)
%LINEARISE_LANCSRIO  Linearise riometer data utilising the lancs rio format.
%
%   [r, units = LINEARISE_LANCSRIO(mia)
%   r: linearised data
%   units: CHAR giving the output units
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.


r = repmat(nan, getdatasize(mia));
units = 'dBm';
if isempty(r)
  return
end

if matlabversioncmp('>=', '5.2')
  persistent linearisetable;
else
  % make sure it exists
  linearisetable = [];
end
if isempty(linearisetable)
  % first time round (or every time round for versions < 5.2)
  in = getinstrument(mia);
  code = sprintf('%s_%d', getabbreviation(in), getserialnumber(in));

  if matlabversioncmp('>=', '5.2')
    disp(['loading linearisation table for ' code]);
  end
  load(['linearise_rio_table_' code]); 
end

% Which set of linearisation data should be used?
lin_num = 0;
lin_time = getmidtime(mia);
for n = 1:numel(linearisetable)
  start_ok = false;
  if ~isvalid(linearisetable(n).starttime)
	start_ok = true;
  elseif lin_time >= linearisetable(n).starttime
	start_ok = true;
  end
  
  end_ok = false;
  if ~isvalid(linearisetable(n).endtime)
	end_ok = true;
  elseif lineTime < linearisetable(n).endtime
    end_ok = true
  end
  
  if start_ok & end_ok
	lin_num = n;
	break
  end
end

if lin_num == 0
  % No data is valid for this time
  error('Cannot find suitable linearisation data');
end

% Linearise each beam independently
for beam = getbeams(mia)
  bn = find(beam == linearisetable(n).beams);
  bi = getparameterindex(mia, beam);

  x = linearisetable(lin_num).x(bn, :);
  y = linearisetable(lin_num).y(bn, :);
  notNan = and(~isnan(x), ~isnan(y));
  r(bi, :) = interp1(x(notNan), y(notNan), getdata(mia, bi, ':'));
end



