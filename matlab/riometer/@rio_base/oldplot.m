function varargout = plot(mia, varargin)
%PLOT  Plot data from a RIO_BASE object.
%
%   PLOT(mia)
%   PLOT(mia, ...)

fh = [];
gh = [];
ph = [];

instrument = getinstrument(mia);
[imagingBeams, wideBeams] = info(instrument, 'beams');

beams = getbeams(mia);

% % our default values
% defaults.beams = [intersect(wideBeams, beams) ...
% 		  intersect(imagingBeams, beams)];
% if ~isempty(defaults.beams)
%   % choose first wide beam, else imaging beam 
%   defaults.beams = defaults.beams(1); 
% end

defaults.beams = info(instrument, 'preferredbeam', 'beams', beams, ...
		      'time', getmidtime(mia));

defaults.miniplots = [1 1];
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.ylim = info(instrument, 'limits', mia);
defaults.heater_data = [];

[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  if any(~ishandle(gh))
    error('invalid handle');
  end
  % args = {varargin{unvi} 'plotaxes', defaults.plotaxes};
  args = {varargin{unvi}};
  fh = get(gh, 'Parent');
  if iscell(fh)
    fh = cell2mat(fh);
  end
  fh = unique(fh);
else
  gh = [];
  args = {varargin{unvi}};
end

numOfBeams = prod(size(defaults.beams));
if defaults.miniplots ~= [1 1] & prod(defaults.miniplots) < numOfBeams
  error(sprintf(['%d plot windows specified, but %d beams ' ...
	'requested!'], prod(defaults.miniplots), numOfBeams));
end    

% calculate title now that beams are known
[title windowtitle] = maketitle(mia, 'beams', defaults.beams);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc
if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', ...
			'logo', info(instrument', 'logo'), ...
			args{:}, ...
			'miniplots', defaults.miniplots, ...
			'pointer', 'watch', ...
			'title', defaults.title);
end

% multiple beams - make subplots for all beams requested
if defaults.miniplots == [1 1] & numOfBeams > 1
  n = 1:numOfBeams; % plot all at once
else
  n = 1;
end
beamindex = getparameterindex(mia, defaults.beams);
if any(beamindex == 0)
  mia
  error(sprintf('the following beam(s) are not present in the data: %s', ...
		printseries(defaults.beams(beamindex==0))));
end

data = getdata(mia);
xdata = 0.5:1:size(data, 2);
dataquality = getdataquality(mia);

for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    if n <= numOfBeams
      keepKeys = {'Tag' 'UserData'};
      keepVal = get(gh(x,y), keepKeys); 

      if 1
	ph2 = plot(xdata, data(beamindex(n),:), ...
	    'Parent', gh(x,y), ...
	    'Color', defaults.color); 
	ph2 = flipud(ph2); % last line is first
      else
	ph2 = line('Parent', gh(x,y), ...
		  'XData', xdata, ...
		  'YData', data(beamindex(n),:), ...
		  'Color', defaults.color, ...
		  'Tag', sprintf('beam %d', beams(n))); 
      end
      set(gh(x,y), keepKeys, keepVal);
      set(get(gh(x,y), 'XLabel'), 'String', int2str(defaults.beams(n)));
      if length(n) == 1
	set(ph2, 'Tag', sprintf('beam %d', defaults.beams(n)));
      else
	% NOT WORKING
	% multiple plots in one window
	% for m = 1:prod(size(n))
	  % set(ph2(m), 'Tag', sprintf('beam %d', beams(m)));
	% end
	% tags = cell(prod(size(n), 1));
	% for m = 1:length(tags)
	%   tags{m, 1} = sprintf('beam %d', beams(m));
	% end
	% set(ph2, {'Tag'}, tags);
      end	
      
      ph = [ph; ph2];
      
      % if dataquality issues exist warn about them
      if ~isempty(dataquality)
	makeplotfig('addwatermark', gh(x,y), {dataquality});
      end
      
    else
      % axis blank 
      set(gh(x,y), 'Visible', 'off');
    end
    n = n + 1;
  end
end





set(gh, 'XLim', [0 size(data,2)]);
% timetick(ifb, gh, 'X', 0);  % don't have the axes labelled

if any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  for n = 1: prod(size(gh))
    curLim = get(gh(n), 'YLim');
    curLim(notNans) = defaults.ylim(notNans);
    set(gh(n), 'YLim', curLim);
  end
elseif ~isempty(defaults.ylim)
  set(gh, 'YLim', defaults.ylim);
end

% -------
if isempty(defaults.plotaxes) 
  if exist('heatermenu')
    % make tools menus visible
    toolsMenuHandle = findall(fh, 'Tag', 'figMenuTools', 'Type', 'uimenu');
    if isempty(toolsMenuHandle)
      toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
    end
    
    if ~isempty(toolsMenuHandle)
      set(toolsMenuHandle, 'Visible', 'on');
      heatermenu('init', toolsMenuHandle, ...
		 'onofftimes', 'on', ...
		 'heater_data', defaults.heater_data);
    end
  end

end
    

if length(gh) == 1 
  if isempty(defaults.plotaxes)
    set(get(gh, 'YLabel'), ...
	'String', ['\bf ' datalabel(mia)], ...
	'FontSize', 14);
  end
else
  % turn off tick labels - too crowded
  set(gh, 'XTick', [], 'YTick', []);
  % label, but invisibly because they can make the plot too busy
  set(cell2mat(get(gh, 'YLabel')), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14, ...
      'Visible', 'off');
end

timetick(mia, gh, 'X', 1);

if isempty(defaults.plotaxes)
  set(fh, 'Name', defaults.windowtitle, ...
	  'Pointer', 'arrow');
end

varargout{1} = fh;
varargout{2} = gh;
varargout{3} = ph;

return;





