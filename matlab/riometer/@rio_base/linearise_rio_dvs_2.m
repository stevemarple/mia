function [d, units] = linearise_rio_dvs_2(mia)
%LINEARISE_KIL  Linearise riometer data from Davis.
%
%   d = LINEARISE_KIL(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

in = getinstrument(mia);
code = sprintf('%s_%d', getabbreviation(in), getserialnumber(in));

if matlabversioncmp('>=', '5.2')
  persistent linearisetable;
else
  % make sure it exists
  linearisetable = [];
end
if isempty(linearisetable)
  % first time round (or every time round for versions < 5.2)
  if matlabversioncmp('>=', '5.2')
    disp(['loading linearisation table for ' code]);
  end
  load(['linearise_rio_table_' code]); 
end

% convert any values not within the table to nan
d = double(getdata(mia));
d(find(d(:,:) <= 0)) = nan;
d(find(d(:,:) > size(linearisetable,2))) = nan;

beams = getbeams(mia);
bi = getparameterindex(mia, beams);

receiver = rem(beams,7);
receiver(find(receiver == 0)) = 7; % beam 7,14,.. is 7, not 0

% widebeam is recorded on all channels and averaged as part of the
% processing - so do the same here.
wbi = getparameterindex(mia, 50);
if wbi ~= 0
  receiver(wbi) = 8;
end

for n = 1:length(beams)
  % find all valid data
  validDataIndex = find(~isnan(d(bi(n),:)));
  if ~isempty(validDataIndex)
    % only linearise if there is data to do
    d(bi(n),validDataIndex) = ...
	linearisetable(receiver(n), d(bi(n),validDataIndex));
  end
end

units = 'dBm';



