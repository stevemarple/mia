function r = getlimits(mia, varargin)
%GETLIMITS  Return the min. and max. data values for a RIO_BASE object.
%
%   r = GETLIMITS(mia)
%   r = GETLIMITS(mia, beams)
%   r = GETLIMITS(mia, beams, st, et)
%   r: [min max] vector
%   mia: IRISPOWERTIME object
%   beams: vector of beams numbers to use
%   st: start sample or time (first sample is 1)
%   et: end sample or time
%
%   Return the upper and lower limits for all beams, or the beams
%   specified. 

miadata = getdata(mia);
sz = size(miadata);
switch nargin
  case 1
    beams = getbeams(mia);
    st = 1;
    et = sz(2);
    
  case 2
    beams = varargin{1};
    st = 1;
    et = sz(2);
    
  case 4
    beams = values{1};
    st = varargin{2};
    et = varargin{3};
    if isa(st, 'timestamp')
      st = 1 + ((st - getstarttime(mia)) / getresolution(mia));
    end
    if isa(et, 'timestamp')
      et = 1 + ((et - getstarttime(mia)) / getresolution(mia));
    end
 
  otherwise
    error('incorrect parameters');
end

r = [min(min(miadata(getparameterindex(mia, beams),st:et))), ...
      max(max(miadata(getparameterindex(mia, beams),st:et)))];

