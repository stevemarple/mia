function r = extract(mia, varargin)
%EXTRACT Extract a subset of data as another RIO_BASE object
%
%   r = EXTRACT(mia, beams);
%   r = EXTRACT(mia, beams, st, et);
%
%   r: RIO_BASE object
%   mia: original RIO_BASE object
%   beams: vector of beams (currently must be same as original data)
%   st: starttime
%   et: endtime
%
%   EXTRACT returns a subset of the data. It is similar to GETPOWER (or
%   GETDATA) but instead returns an object.
%
%   See also GETDATA, RIO_BASE, TIME

if length(mia) ~= 1
  % vectorise
  sz = size(mia);
  for n = 1:prod(sz)
    r(n) = extract(mia(n), varargin{:});
  end
  r = reshape(r, sz);
  return;
end


st = getstarttime(mia);
res = getresolution(mia);

defaults.starttime = st;
defaults.endtime = getendtime(mia);
defaults.beams = mia.beams;

r = mia;

if nargin == 2
  % mfilename(mia, beams)
  defaults.beams = varargin{1};

elseif nargin == 4 & isnumeric(varargin{1})
  % mfilename(mia, beams, starttime, endtime)
  defaults.beams = varargin{1};
  defaults.starttime = varargin{2};
  defaults.endtime = varargin{3};
  
elseif rem(nargin, 2) == 1 & mia_ischar(varargin{1:2:end})
  % parameter name/value pair interface
  [defaults unvi] = interceptprop(varargin, defaults);
  
  if length(unvi)
    unknowns = varargin{unvi(1)};
    plural = '';
    for n = 3:2:length(unvi)
      plural = 's';
      unknowns = [unknowns ', ' varargin{unvi(1)}];
    end
    warning(sprintf('unknown parameter%s: ', plural, unknowns));
  end

else
  mia
  varargin{:}
  error('incorrect parameters')
end


stIdx = max(((defaults.starttime - st)/res) + 1, 1);
% etIdx = min((defaults.endtime - st) / res, size(r.data, 2));
etIdx = min((defaults.endtime - st) / res, getdatasize(r, 2));

defaults.starttime = st + res * (stIdx - 1);
defaults.endtime = st + res * etIdx;

r = setstarttime(r, defaults.starttime);
r = setendtime(r, defaults.endtime);

r = setdata(r, getdata(mia, getparameterindex(mia, defaults.beams), ...
			    stIdx:etIdx));
r = setbeams(r, defaults.beams);



