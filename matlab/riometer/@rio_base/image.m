function [r, t, defaults] = image(rio, varargin)
%IMAGE Convert data to an image.
%
%   [r, t, params] = IMAGE(ifb, ...)
%
%   r: CELL array of images
%   t: image times (TIMESTAMP vector)
%   params: parameters used to create the image(s)
%
%   A generic function to map data onto an image. Two calling conventions
%   are possible, firstly with numerical values (faster?), or alternatively
%   with parameter name/value pairs (more flexible/readable). The parameter
%   names are the same as for the first method. The function returns data
%   suitable to use by the specgraph/IMAGE function.
%
% See also specgraph/IMAGE.

st = getstarttime(rio);
res = getresolution(rio);
beams = getbeams(rio);
instrument = getinstrument(rio);
[imagingBeams wideBeams badBeams] = info(instrument, 'beams');
preferredBeams = setdiff(imagingBeams, badBeams);

% Fields must exist for interceptprop
defaults.samples = []; % convert all samples to images
defaults.beams = intersect(beams, preferredBeams);
defaults.units = 'deg';
defaults.xpixelpos = [];
defaults.ypixelpos = [];
% height is always in metres
defaults.height = info(instrument, 'defaultheight');
defaults.beamlocations = [];
defaults.interpmethod = 'linear';


[defaults unvi] = interceptprop(varargin, defaults);

bi = getparameterindex(rio, defaults.beams);
data = getdata(rio, bi, defaults.samples);
if isempty(defaults.samples)
  numSamples = size(data, 2);
  defaults.samples = 1:numSamples;
else
  numSamples = length(defaults.samples);
end


[xpixelpos ypixelpos] = info(instrument, 'pixels', 'units', defaults.units);
if isempty(defaults.xpixelpos) 
  defaults.xpixelpos = xpixelpos;
end
if isempty(defaults.ypixelpos) 
  defaults.ypixelpos = ypixelpos;
end

if isempty(defaults.beamlocations)
  defaults.beamlocations = info(instrument, 'beamlocations', ...
				'beams', defaults.beams, ...
				'units', defaults.units, ...
				'height', ...
				defaults.height);
end

switch defaults.interpmethod
 case {'invdist', 'v4'}
  defaults.interpmethod = 'v4';
  usesDelaunay = 0;
  
 otherwise
  usesDelaunay = 1;
end

correctSize = [prod(size(defaults.ypixelpos)) ...
	       prod(size(defaults.xpixelpos))];

r = cell(1, numSamples);
t(1, numSamples) = timestamp; % pre-allocate time marix

for n = 1:numSamples
  im = zeros(correctSize);
  % if ~all(isnan(beams))
  [xMesh yMesh] = meshgrid(defaults.xpixelpos, defaults.ypixelpos);
  im = griddata(defaults.beamlocations(1, :), ...
		   defaults.beamlocations(2, :), data(:, n)', ...
		   xMesh, yMesh, defaults.interpmethod);
  % error('oops')

  % else
  % im = [];
  % end
  if ~isequal(size(im), correctSize)
    % ensure matrix has correct size since the triangulation failed
    % warning('DATA NOT TRIANGULATED'); 
    % set to all nans and the correct size
    im = repmat(nan, correctSize);
  end
  r{n} = im;
  t(n) = st + (n-1)*res;
end



