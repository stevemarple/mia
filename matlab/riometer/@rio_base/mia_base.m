function r = mia_base(mia)

r = repmat(mia_base, size(mia));
for n = 1:numel(mia)
  tmp = mia(n);
  r(n) = tmp.mia_base;
end

