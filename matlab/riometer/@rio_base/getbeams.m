function r = getbeams(rio)
%GETBEAMS  Return beams.
%
%   r = GETBEAMS(rio)
%   r: DOUBLE
%   rio: RIO_BASE object
%
%   See also RIO_BASE, RIOMETER.

r = rio.beams;

