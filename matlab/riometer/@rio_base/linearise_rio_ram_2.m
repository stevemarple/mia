function [d, units] = linearise_rio_ram_2(mia)
%LINEARISE_RIO_RAM_2  Linearise riometer data from rio_ram_2.
%
%   d = LINEARISE_RIO_RAM_2(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

d = 10 .* log10(getdata(mia)) + ...
    info(getinstrument(mia), 'dbmconversion', getstarttime(mia));
units = 'dBm';
