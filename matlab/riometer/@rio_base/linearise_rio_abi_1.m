function [d, units] = linearise_rio_abi_1(mia)
%LINEARISE_RIO_ABI_1  Linearise riometer data from rio_abi_1.
%
%   d = LINEARISE_RIO_ABI_1(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

% Linearisation details taken from email by Tero Raita, 2010-12-14
%
% Time, temp(K), voltage from riometer output:
% 1631UT, 1kK, 0.39V
% 1632UT, 2kK, 0.65V
% 1632UT, 3kK, 0.89V
% 1634UT, 4kK, 1.13V
% 1635UT, 5kK, 1.37V
% 1636UT, 6kK, 1.61V
% 1637UT, 7kK, 1.85V
% 1638UT, 8kK, 2.08V
% 1639UT, 9kK, 2.30V
% 1640UT, 10kK, 2.55V
% 1641UT, 11kK, 2.78V
% 1642UT 12kK, 3.00V 

% Scale +/- 10V, 16 bit,

% 0 = 0, power(2,15)-1 = 10V

x = [.39 .65 .89 1.13 1.37 1.61 1.85 2.08 2.30 2.55 2.78 3.0] ...
    * (power(2,15)-1) / 10;
y = [1:12] * 1e3;

d = interp1(x, y, getdata(mia), 'cubic');
units = 'K';
