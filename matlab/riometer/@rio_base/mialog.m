function mialog(mia, varargin)
%MIALOG  Log data usage, overloaded for RIO_BASE.
%
% See mia_base/MIALOG.

for n = 1:numel(mia)
  m = mia(n);
  if ~isempty(getdata(m))
    mia_base_mialog(m, varargin{:}, 'beams', printseries(getbeams(m)));
  end
end
