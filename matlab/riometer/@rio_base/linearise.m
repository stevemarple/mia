function [d, units] = linearise(mia)
%LINEARISE  Linearise riometer data.
%
%   [d units] = LINEARISE(mia)
%   d: linearised data (DOUBLE)
%   units: units of the linearised data (CHAR)
%   mia: RIO_BASE object (normally RIO_RAWPOWER or RIO_RAWQDC)
%

in = getinstrument(mia);
code = lower(getcode(in));
func = ['linearise_rio_' code];
 
if isempty(which('-all', func))
  func = ['linearise_rio_' lower(getfacility(in))];
end

if isempty(which('-all', func))
  error(['Cannot linearise riometer data from ' code]);
else
  [d units] = feval(func, mia);
end
