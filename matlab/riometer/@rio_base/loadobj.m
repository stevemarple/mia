function r = loadobj(a)
%LOADOBJ  Load filter for RIO_BASE object.
%

if isstruct(a)
  r = rio_base(a);
else
  r = a;
end

