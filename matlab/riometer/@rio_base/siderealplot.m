function [fh, gh, ph] = siderealplot(mia, varargin)
%SIDEREALPLOT  Plot RIO_BASE data aligned to sidereal days

if isa(mia, 'rio_qdc_base')
  error('QDC data is already aligned to sidereal time. Use plot instead');
end

instrument = getinstrument(mia);
[imagingBeams, wideBeams] = info(instrument, 'beams');

beams = getbeams(mia);

defaults.beams = info(instrument, 'preferredbeam', 'beams', beams);
defaults.miniplots = [1 1];
defaults.title = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.selectedcolor = 'r';
defaults.ylim = info(instrument, 'limits', mia);

[defaults unvi] = interceptprop(varargin, defaults);

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  args = {varargin{unvi} 'plotaxes', defaults.plotaxes};
  fh = [];
else
  gh = [];
  args = {varargin{unvi}};
end

numOfBeams = prod(size(defaults.beams));
if defaults.miniplots ~= [1 1] & prod(defaults.miniplots) < numOfBeams
  error(sprintf(['%d plot windows specified, but %d beams ' ...
	'requested!'], prod(size(defaults.miniplots)), numOfBeams));
end

% calculate title now that beams are known
[title windowtitle] = maketitle(mia, 'beams', defaults.beams);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc
if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to override
  [fh gh] = makeplotfig('init', args{:}, ...
			'miniplots', defaults.miniplots, ...
			'title', defaults.title, ...
			'closerequestfcn', 'siderealplotcb(''windowclose'');');
end

% multiple beams - make subplots for all beams requested
if defaults.miniplots == [1 1] & numOfBeams > 1
  n = 1:numOfBeams; % plot all at once
else
  n = 1;
end


beamindex = getparameterindex(mia, defaults.beams);
data = getdata(mia);
% xdata = 0.5:1:size(data, 2);
st = getstarttime(mia);
et = getendtime(mia);
res = getresolution(mia);
loc = getlocation(instrument);
% stoff = stoffset(st + xdata*res, getlocation(instrument))

% find the times of sidereal midnight
t = st;
sm = repmat(timestamp([]), [0 0]);
smi = [];
while t < et
  % sidereal midnight, rounded to nearest sample
  sm(end+1) = round(lmst2ut(timespan(0, 's'), loc, t), res);
  smi(end+1) = round((sm(end) - st) / res) + 1;
  if getdate(sm(end)) == getdate(sm(end) + siderealday)
    % two sidereal midnights in one day (rare, but it happens)
    % sm(end+1) = sm(end) + siderealday;
    sm(end+1) = round(sm(end) + siderealday, res);
    smi(end+1) = round((sm(end) - st) / res) + 1;
  end
  t = t + timespan(1,'d');
end

ph = [];
ud.resolution = res;
ud.color = defaults.color;
ud.selectedcolor = defaults.selectedcolor;

for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    if n <= numOfBeams
      ud.starttime = st;
      % start to first sidereal midnight
      ph(end+1) = line('Parent', gh(x,y), ...
		       'XData', (stoffset(st, loc)/res) + [0.5:1:smi(1)], ...
		       'YData', data(beamindex(n), 1:smi(1)), ...
		       'Parent', gh(x,y), ...
		       'Color',  defaults.color, ...
		       'UserData', ud, ...
		       'ButtonDownFcn', 'siderealplotcb(''click'');');
      for s = 1:(length(sm)-1)
	% between sidereal midnights
	% ud.starttime = st + res*(smi(s)-1);
	% ud.starttime = st + res*(smlast-1);
	ud.starttime = sm(s);
	ph(end+1) = line('Parent', gh(x,y), ...
			 'XData', 0.5:1:(smi(s+1) - smi(s)), ...
			 'YData', data(beamindex(n), (smi(s)+1):smi(s+1)), ...
			 'Parent', gh(x,y), ...
			 'Color', defaults.color, ...
			 'UserData', ud, ...
			 'ButtonDownFcn', 'siderealplotcb(''click'');'); 
      end
      % from sidereal midnight to end
      % ud.starttime = st + res*(smlast-1);
      ud.starttime = sm(s);
      % 'XData', 0.5:1:(size(data,2)+1-smi(s)), ...
      ydata = data(beamindex(n), (smi(end)+1):end);
      ph(end+1) = line('Parent', gh(x,y), ...
		       'XData', 0.5:length(ydata), ...
		       'YData', ydata, ...
		       'Parent', gh(x,y), ...
		       'Color', defaults.color, ...
		       'UserData', ud, ...
		       'ButtonDownFcn', 'siderealplotcb(''click'');'); 
      
      
      set(get(gh(x,y), 'XLabel'), 'String', int2str(defaults.beams(n)));
      if length(n) == 1
	set(ph, 'Tag', sprintf('beam %d', defaults.beams(n)));
      else
	% NOT WORKING
	% multiple plots in one window
	% for m = 1:prod(size(n))
	% set(ph(m), 'Tag', sprintf('beam %d', beams(m)));
	% end
	% tags = cell(prod(size(n), 1));
	% for m = 1:length(tags)
	%   tags{m, 1} = sprintf('beam %d', beams(m));
	% end
	% set(ph, {'Tag'}, tags);
      end	
      
    else
      % axis blank 
      set(gh(x,y), 'Visible', 'off');
    end
    n = n + 1;
  end
end
  
  
% set(gh, 'XLim', [0 size(data,2)]);
set(gh, ...
    'XLim', [0 ceil(siderealday / res)], ...
    'Box', 'on');

if any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  for n = 1: prod(size(gh))
    curLim = get(gh(n), 'YLim');
    curLim(notNans) = defaults.ylim(notNans);
    set(gh(n), 'YLim', curLim);
  end
elseif ~isempty(defaults.ylim)
  set(gh, 'YLim', defaults.ylim);
end


if length(gh) == 1
  set(get(gh, 'YLabel'), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14);
else
  % turn off tick labels - too crowded
  set(gh, 'XTick', [], 'YTick', []);
  % label, but invisibly because they can make the plot too busy
  set(cell2mat(get(gh, 'YLabel')), ...
      'String', ['\bf ' datalabel(mia)], ...
      'FontSize', 14, ...
      'Visible', 'off');
end

% timetick(mia, gh, 'X', 1);
timetick(gh, 'X', timespan(0, 's'), ceil(siderealday, res), res, 1);
set(fh, 'Name', defaults.windowtitle, ...
    'Pointer', 'arrow');


varargout{1} = fh;
varargout{2} = gh;
varargout{3} = ph;

return;
