function [d, units] = linearise_rio_lan_1(mia)
%LINEARISE_RIO_LAN_1  Linearise riometer data from rio_lan_1.
%
%   d = LINEARISE_RIO_LAN_1(mia)
%   d: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

% Linearisation details taken from email by Anton Bogdanov, 2011-02-23
%
% Pin (dBm)	Vout (mV)
% -120	273
% -115	274
% -110	279
% -105	294
% -100	340
% -95	464
% -90	585
% -85	710
% -80	913
% -75	1292
% -70	1562

x = [273 274 279 294 340 464 585 710 913 1292 1562] * 1e-3; % Volts
y = [-120:5:-70]; % dBm

d = interp1(x, y, getdata(mia), 'linear','extrap');
d(d > -70) = nan;
units = 'dBm';

