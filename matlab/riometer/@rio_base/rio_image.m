function r = rio_image(mia, varargin)
%RIO_IMAGE  Convert a RIO_BASE object to a RIO_IMAGE object.
%
%   r = RIO_IMAGE(mia, ...)
%   r: RIO_IMAGE object
%   mia: RIO_BASE object
%
%   The following parameter name/value pairs may be used to modify the
%   standard behaviour of this function.
%
%     samples
%     beams
%     pixelunits
%     xpixelpos
%     ypixelpos
%     height
%     interpmethod
%
%   See also RIO_BASE, RIO_IMAGE.

st = getstarttime(mia);
et = getendtime(mia);
beams = getbeams(mia);
instrument = getinstrument(mia);
[imagingBeams wideBeams badBeams] = info(instrument, 'beams');
preferredBeams = setdiff(imagingBeams, badBeams);

% Fields must exist for interceptprop
defaults.samples = []; % convert all samples to images
defaults.beams = intersect(beams, preferredBeams);
defaults.pixelunits = 'deg';
defaults.xpixelpos = [];
defaults.ypixelpos = [];
% height is always in metres
defaults.height = info(instrument, 'defaultheight');
defaults.beamlocations = [];
defaults.interpmethod = 'linear';
defaults.cancelhandle = [];

[defaults unvi] = interceptprop(varargin, defaults);

% --- trap likely errors
if strcmp({varargin{unvi(1:2:end)}}, 'units')
  warning('Do you mean really mean units, not pixelunits?');
end
% ---

bi = getparameterindex(mia, defaults.beams);
if isempty(bi)
  bi = ':';
end
if isempty(defaults.samples)
  defaults.samples = ':';
end
data = getdata(mia, bi, defaults.samples);
numSamples = size(data, 2);

[xpixelpos ypixelpos] = info(instrument, 'pixels', ...
			     'units', defaults.pixelunits);
if isempty(defaults.xpixelpos) 
  defaults.xpixelpos = xpixelpos;
end
if isempty(defaults.ypixelpos) 
  defaults.ypixelpos = ypixelpos;
end

if isempty(defaults.beamlocations)
  defaults.beamlocations = info(instrument, 'beamlocations', ...
				'beams', defaults.beams, ...
				'units', defaults.pixelunits, ...
				'height', ...
				defaults.height, ...
				'year', getyear(getmidtime(mia)));
end

switch defaults.interpmethod
  case {'invdist', 'v4'}
   defaults.interpmethod = 'v4';
   usesDelaunay = 0;
    
  otherwise
    usesDelaunay = 1;
end

correctSize = [prod(size(defaults.xpixelpos)) ...
	       prod(size(defaults.ypixelpos))];

imagedata = zeros([correctSize numSamples]);

[xMesh yMesh] = meshgrid(defaults.xpixelpos, defaults.ypixelpos);

for n = 1:numSamples
  cancelcheck(defaults.cancelhandle);
  
  im = zeros(correctSize);
  im = griddata(defaults.beamlocations(1, :), ...
		defaults.beamlocations(2, :), data(:, n)', ...
		xMesh, yMesh, defaults.interpmethod)';
  if ~isequal(size(im), correctSize)
    % ensure matrix has correct size since the triangulation failed
    warning('DATA NOT TRIANGULATED'); 
    % set to all nans and the correct size
    im = repmat(nan, correctSize);
  end
  imagedata(:,:,n) = im;
end

r = rio_image('instrument', getinstrument(mia), ...
	      'starttime', st, ...
	      'endtime', et, ...
	      'sampletime', getsampletime(mia), ...
	      'integrationtime', getintegrationtime(mia), ...
	      'units', getunits(mia), ...
	      'height', defaults.height, ...
	      'processing', getprocessing(mia), ...
	      'xpixelpos', defaults.xpixelpos, ...
	      'ypixelpos', defaults.ypixelpos, ...
	      'pixelunits', defaults.pixelunits, ...
	      'data', imagedata, ...
	      'originalclass', class(mia), ...
	      varargin{unvi});

r = addprocessing(r, sprintf(['Converted to image: ' ...
		    'beams=<%s>, interpmethod=%s'], ... 
			     printseries(defaults.beams), ...
			     defaults.interpmethod));

r = adddataquality(r, getdataquality(mia));
