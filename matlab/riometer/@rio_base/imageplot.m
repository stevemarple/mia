function  [fh, gh] = imageplot(mia, varargin)
%IMAGEPLOT Plot riometer data as images, without gridding
%
%   [fh, gh] = IMAGEPLOT(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle
%   mia: RIO_BASE object
%
% IMAGEPLOT plots imaging riometer data (ie imaging beams only) as images
% using SURFACEIMAGE. The FIGURE is created by MAKEPLOTFIG. The default
% behaviour can be modified with the following name/value pairs.
%
%   'miniplots', 1x2 NUMERIC vector
%   The number of (x,y) plot axes to add to the FIGURE.
%
%   'starttime', TIMESTAMP or NUMERIC
%   The start time or sample number of the first image to be plotted.
%
%   'endtime', TIMESTAMP or NUMERIC
%   The end time or sample number of the first image to be plotted.
%
%   'title', CELL or CHAR
%   The title given to the plot.
%
%   'windowtitle', CHAR
%   The title given to the FIGURE window.
%
%   'step', NUMERIC
%   The step between samples; defaults to 1, meaning every image between
%   start and end is displayed.
%
%   'plotaxes', AXES handle
%   If not empty plot into existing AXES handle(s), do not create a new
%   plot FIGURE. The default is empty, ie create a new FIGURE.
%
%   'shading', 'flat', 'interp' or 'faceted'
%   The type of SHADING to use. Default is flat.
%
%   'clim', 1x2 NUMERIC
%   The colour limits. NAN can be used to indicate that a given limit
%   should be automatic. Default is [NAN NAN].
%
%   'units', CHAR
%   The units to use when determining the beam locations. Default is
%   'km'. See under beamlocations for riometer/INFO2 for other valid
%   settings.
%
%   'xlim', 1x2 NUMERIC
%   'ylim', 1x2 NUMERIC
%   The X and Y limits used for the plot. The default is to use the
%   instrument's default field of view for the given bema location
%   units. See under fov for riometer/INFO2.
%
%   'beams', NUMERIC
%   Restrict the plotting to use only the listed beams. The default it to
%   use all imaging beams present in the data.
%
%   'autoprint', LOGICAL
%   Allow auotmatic printing. Default if FALSE.
%
%   'autoprintoverlap', NUMERIC
%   Number of images whihc should be common between successive pages when
%   autoprinting is in operation.
%
%   'autoprinttries', NUMERIC
%   Number of times page aligning should be attempted when in autoprint
%   mode. Default is 1.
%
%   'scale', 'automatic' | 'fixed'
%   When autoprinting determine is the scale should be set automatically
%   per page (the default) or be fixed at the same lebel for all pages.
%
%   'autoprintfilename', STRFTIME format string
%   The name of the file when autoprinting. If empty a suitable name
%   based on time is automatically selected. The string is passed to
%   STRFTIME with the selected starttime. See also the autoprintappend
%   option.
%
%   'autoprintfilenameextension', CHAR
%   The filename extension. If empty it is selected automatically based
%   on the print deveice.
%
%   'autoprintdevice', CHAR
%   The print device for autoprinting. Defaults to '-dpsc2'.
%
%   'autoprintappend', LOGICAL
%   Indicate if the printed output should append to the same
%   file. Default is FALSE. If not appending then the filename is
%   appended with '_pX' where X is the page number.
%
%   'imagelabelstyle', 'timestamp' | 'number'
%   The initial label used when label images. Default is 'timestamp'.
%
%   'imagelabelposition', 1x2 NUMERIC
%   The position on the axes where the image label should be
%   printed. Defaults to [0.05 0.05].
%
%   'imagelabeltimeformat', CHAR
%   The STRFTIME format used when printing image times. Defaults to
%   '%H:%M:%S'
%
%   'imagelabelnumberformat', CHAR
%   The SPRINTF format string used when printing image sample
%   numbers. Defaults ot '%d'.
%
%   'imagelabelcolor', COLORSPEC
%   The image label color. Defaults to 'w'.
%
%   'imagelabelfontsize', NUMERIC
%   The image label font size. Default is 10.
%
%   'imagelabelverticalalignment', CHAR
%   The vertical alignment used on the image label TEXT object. Default
%   is 'bottom'.
%
%   'imagelabelhorizontalalignment', CHAR
%   The horizontal alignment used on the image label TEXT object. Default
%   is 'left'.
%
%   'imagelabelvisible', 'on' | 'off'
%   The inital condition of the image labels. Default is off. Change via
%   the plot figure menus.
%
% See also MAKEPLOTFIG, SURFACEIMAGE.

% some local copies
starttime = 1; % getstarttime(mia);
endtime = []; % getendtime(mia);
% resolution = getresolution(mia);
instrument = getinstrument(mia);

% our default values
defaults.miniplots = [8 8];
defaults.starttime = starttime;
defaults.endtime = []; % defer (miniPlots might be changed)
defaults.title = []; % defer
defaults.windowtitle = []; % defer
defaults.step = 1;   % number of frames to advance each time
defaults.plotaxes = [];
defaults.shading = 'flat';
defaults.clim = [nan nan]; % not set for either limit
defaults.units = 'km';
defaults.xlim = []; % defer
defaults.ylim = []; % defer

defaults.heaterevent = [];

%  use all the imaging beams which are present
[imagingBeams wideBeams badBeams] = info(instrument, 'beams');
defaults.beams = intersect(getbeams(mia), imagingBeams);

% get flipaxes names from makeplotfig, then if the printed name changes this
% will still work properly
fanames = makeplotfig('flipaxesnames');
defaults.flipaxes = fanames{1};

% allow automatic plotting/printing
defaults.autoprint = 0;
defaults.autoprintoverlap = 6;
defaults.autoprinttries = 1;
defaults.scale = 'automatic';
defaults.autoprintfilename = ''; % generate based on start/end times
defaults.autoprintfilenameextension = '';
defaults.autoprintdevice = '-dpsc2';
defaults.autoprintappend = 0;

% plot labelling defaults
% Valid styles are 'number', 'timestamp'
defaults.imagelabelstyle = 'timestamp'; 
defaults.imagelabelposition = [0.05 0.05];
defaults.imagelabeltimeformat = '%H:%M:%S';
defaults.imagelabelnumberformat = '%d';
defaults.imagelabelcolor = 'w';
defaults.imagelabelfontsize = 10;
defaults.imagelabelverticalalignment = 'bottom';
defaults.imagelabelhorizontalalignment = 'left';
defaults.imagelabelvisible = 'off'; % inital condition, change via menu

[defaults unvi] = interceptprop(varargin, defaults);

% ----- autoprint -----
if strcmp(defaults.autoprint, 'on') ...
      | (isnumeric(defaults.autoprint) & defaults.autoprint > 0)
  % [fh gh] = localAutoPrint(mia, beams, defaults, varargin{unvi});
  [fh gh] = localAutoPrint(mia, defaults, varargin{unvi});
  return;
end
% ----- autoprint (end)




if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  fh = [];
else
  gh = [];
end
 
samt = getsampletime(mia);
it = getintegrationtime(mia);
maxSamples = numel(samt);

% check an image corresponding to desired start time exists
if isempty(defaults.starttime)
  sampleStart = 1;
elseif isa(defaults.starttime, 'timestamp')
  % sampleStart = 1 + (defaults.starttime - starttime) / resolution;
  % sampleStart = find(samt == defaults.starttime);
  sampleStart = find(samt - (0.5*it) == defaults.starttime);
  %if isempty(sampleStart)
  %  sampleStart = find(samt - it == defaults.starttime);
  %end
elseif isnumeric(defaults.starttime)
  sampleStart = defaults.starttime;
else
  error('bad value for start time');
end
if isempty(sampleStart)
  error('Could not find sample corresponding to start time')
elseif sampleStart < 1
  warning('Incorrect start time');
  sampleStart = 1;
elseif sampleStart > maxSamples
  warning('Incorrect start time');
  sampleStart = maxSamples;
elseif sampleStart ~= floor(sampleStart)
  warning('Incorrect start time');
  sampleStart = floor(sampleStart)
end
defaults.starttime = samt(sampleStart) - 0.5*it(sampleStart);

if isempty(defaults.endtime)
  sampleEnd = min(maxSamples, ...
		  sampleStart + defaults.step*prod(defaults.miniplots) -1);
elseif (sampleEnd < 1)
  warning('Incorrect end time');
  sampleEnd = 1;
elseif (sampleEnd > maxSamples)
  warning('Incorrect end time');
  sampleEnd = maxSamples;
elseif (sampleEnd ~= floor(sampleEnd))
  sampleEnd = floor(sampleEnd);
end
defaults.endtime = samt(sampleEnd) + 0.5*it(sampleEnd);

% calculate title now that starttime etc are known
[title windowtitle] = maketitle(mia, ...
				'starttime', defaults.starttime, ...
				'endtime', defaults.endtime, ...
				'step', defaults.step, ...
				'flipaxes', defaults.flipaxes);
% 'endtime', defaults.endtime, ...
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end


% Calculate the spatial limits
[xlim ylim] = info(instrument, 'fov', 'units', defaults.units);

if ~isempty(defaults.xlim)
  xlim = defaults.xlim;
else
  xlim = [min(xlim(:)) max(xlim(:))];
end
if ~isempty(defaults.ylim)
  ylim = defaults.ylim;
else
  ylim = [min(ylim(:)) max(ylim(:))];
end

if isempty(gh)
  % get a figure window decorated with the standard controls.
  % except for miniPlots, allow other functions to overrmiae
  [fh gh] = makeplotfig('init', varargin{unvi}, ...
      'miniplots', defaults.miniplots, ...
      'title', defaults.title, ...
      'footer', 'on');
  % add shading options
  makeplotfig('addshading', fh, 'shading', defaults.shading);
  % add option to reverse images
  makeplotfig('addreverseimages', fh);
end

% Write name into figures title bar, preserve image label color, but
% don't have a background color in the figure (ought to be blank)
fh_color = 'none';
if matlabversioncmp('<=', '5.1')
  fh_color = get(0, 'DefaultFigureColor');
end
set(fh, 'Name', windowtitle, ...
	'InvertHardCopy', 'off', ...
	'Color', fh_color);


% turn off axes visibility on all plot windows since it leaves a black
% line on the bottom and left of the image(s). This does not affect
% children of the axes.
set(gh, 'Visible', 'off');

m = 1;
n = sampleStart;
beamPlan = info(instrument, 'beamplan');
bi = getparameterindex(mia, defaults.beams);
[xBeamPos yBeamPos] = info(instrument, 'beamlocations', ...
			   'units', defaults.units, ...
			   'beams', imagingBeams);
xMesh = repmat(nan, size(beamPlan));
yMesh = repmat(nan, size(beamPlan));
xMesh(beamPlan) = xBeamPos(beamPlan);
yMesh(beamPlan) = yBeamPos(beamPlan);

for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    axes(gh(x,y));

    if n <= sampleEnd
      im = repmat(nan, size(beamPlan));
      im(defaults.beams) = getdata(mia, bi, n);
            
      % check if any image is present
      if ~isnan(im(1))
	imageOk = 1;
	% Matlab doesn't do short-circuit evaluation
      elseif isempty(find(~isnan(im)))
	% all Nans
	imageOk = 0;
      else
	imageOk = 1;
      end

      % imagetime = starttime + (n-1)*resolution;
      imagetime = samt(n) + it(n)/2;
      
      if imageOk
	% remember tag because the 'high-level' graphics functions lose it 
	keepKeys = {'Tag' 'UserData'};
	keepVal = get(gh(x,y), keepKeys); 
	% ud = get(gh(x,y), 'UserData');  % remember UserData too

	surfaceimage(xMesh, yMesh, im, im, ...
                     'parent', gh(x,y), ...
                     'edgecolor', 'none');
	
	% shading interp;
	shading(gh(x,y), defaults.shading)
	view(2);
	% set(gh(x,y), keepKeys, keepVal);
	
	% label image
	switch char(defaults.imagelabelstyle)
	 case 'timestamp'
	  imageLabel = strftime(imagetime, defaults.imagelabeltimeformat);
	  visible = 'on';
	 
	 case 'number'
	  imageLabel = sprintf(defaults.imagelabelnumberformat, n);
	  visible = 'on';
	 
	 otherwise
	  if ~strcmp(char(defaults.imagelabelstyle), '')
	    warning('unknown image label style, not displaying');
	  end
	  imageLabel = ''; % don't label
	  visible = 'off';
	end
	if ~isempty(imageLabel)
	  text('Parent', gh(x,y), ...
	       'String', imageLabel, ...
	       'Units', 'normalized', ...
	       'Position', defaults.imagelabelposition, ...
	       'Color', defaults.imagelabelcolor, ...
	       'FontSize', defaults.imagelabelfontsize, ...
	       'HorizontalAlignment', ...
	       defaults.imagelabelhorizontalalignment, ...
	       'VerticalAlignment', defaults.imagelabelverticalalignment, ...
	       'Visible', defaults.imagelabelvisible, ...
	       'Tag', 'imagelabel');
	end
	
      else
	% blank axes ?
	disp('warning: blank image');
	set(gh(x,y), 'Visible', 'on', ...
	    'Color', 'white', ...
	    'XColor', 'white', ...
	    'YColor', 'white', ...
	    'Box', 'on', ...
	    'XTick', [], ...
	    'YTick', []);
	lims = get(gh(x,y), {'XLim', 'YLim'});
	text('Parent', gh(x,y), ...
	    'String', sprintf('Missing\ndata'), ...
	    'HorizontalAlignment', 'center', ...
	    'VerticalAlignment', 'middle', ...
	    'Position', [(lims{1}(2) - lims{1}(1))/2, ...
	      (lims{2}(2) - lims{2}(1))/2]);
	
	% axes(gh(x,y));
	% cla
      end
      set(gh(x,y), 'UserData', ...
		   {imagetime, imagetime+it(n), n});
		   % {imagetime, imagetime+resolution, n});

    else
      % out of samples, blank
      set(gh(x,y), 'Visible', 'off');
    end
    m = m + 1;             % advance to next axes
    n = n + defaults.step; % advance to next sample
  end
end

set(gh,'XLimMode', 'manual', ...
    'YLimMode', 'manual', ...
    'XLim', xlim, ...
    'yLim', ylim);

% set colour limits to all be same
climTmp = get(gh, 'CLim');
if iscell(climTmp)
  climTmp = [climTmp{:}];
end
% for backward compatibility where an empty matrix used for default
if isempty(defaults.clim)
  defaults.clim = [nan nan];
end

if isnan(defaults.clim(1))
  defaults.clim(1) = min(climTmp(:));
end
if isnan(defaults.clim(2))
  defaults.clim(2) = max(climTmp(:));
end
  
aspectRatio = miaaspectratio(xlim, ylim, defaults.units);
aspectRatio(3) = 1;

set(gh,'CLimMode', 'manual', ...
       'CLim', defaults.clim, ...
       'PlotBoxAspectRatio', aspectRatio);
  
if ~isempty(fh)
  % add colourbar
  cbh = makeplotfig('addcolorbar', fh, ...
      'shading', defaults.shading, ...
      'clim', defaults.clim, ...
      'xlim', defaults.clim, ...
      'title', datalabel(mia));
  
end


% a hack to get the paper size/aspect ratio correct for 8x8 images
if ~isempty(fh)
  set(fh, 'PaperType', 'a4', ...
      'PaperPositionMode', 'manual', ...
      'PaperUnits', 'centimeters');
  paperPos = [0 0 18 24];
  paperPos(1:2) = (get(fh, 'PaperSize') - paperPos(3:4)) * 0.5;
  set(fh ,'PaperPosition', paperPos, ...
      'Pointer', 'arrow');
end

% -------
if exist('heatermenu')
  % make tools menus visible
  toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
  if ~isempty(toolsMenuHandle)
    set(toolsMenuHandle, 'Visible', 'on');
    % load the heater menu with the same on/off times used for creating
    % the image plot(s)
    heatermenu('init', toolsMenuHandle, ...
	       'starttime', defaults.starttime, ...
	       'endtime', defaults.endtime, ...
	       'onofftimes', 'on', ...
	       'heaterevent', defaults.heaterevent);
  end
end
% -------

% if any imagelabels were added then make the menu item visible, and set
% the Checked property to the same state as the image label visibility
ilh = findobj(fh, 'Type', 'Text', 'Tag', 'imagelabel');
if ~isempty(ilh)
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'showimagelabels'), ...
      'Visible', 'on', ...
      'Checked', defaults.imagelabelvisible);
end

return;



% ===============================================================
% function [fh, gh] = localAutoPrint(mia, beams, defaults, varargin)
function [fh, gh] = localAutoPrint(mia, defaults, varargin)
fh = [];
gh = [];

defaults.autoprint = 0; % turn it off now to avomia recursive calls
if isempty(defaults.endtime)
  defaults.endtime = getendtime(mia);
end
% defaults.beams = beams;
if isempty(defaults.autoprintfilename)
  fstr = '%Y%m%d%H%M%S'
  defaults.autoprintfilename = [strftime(defaults.starttime, fstr) '-' ...
	strftime(defaults.endtime, fstr)];
end

switch defaults.scale
  case 'fixed'
    % data = getdata(mia, beams, []);
    mi = nan; % initialise running min and max with nan
    ma = nan;
    for n = 1:getimagesize(mia, 2)
      data = getimage(mia, n);
      mi = min(mi, min(data(:)));
      ma = max(ma, max(data(:)));
    end
    % choose min and max - not the cleverest method but will do for now
    % defaults.clim = [min(data(:)), max(data(:))];
    defaults.clim = [mi ma];
    
  case 'automatic'
    % no need to do anything
    
  otherwise
    error(['Unknown scale type was (' defaults.scale ')']);
end

% convert all the default name/values to a cell array
fn = fieldnames(defaults);
args = cell(1, 2*length(fn));
for n = 0:(length(fn)-1)
  args{(2*n)+1} = fn{n+1};
  args{(2*n)+2} = getfield(defaults, fn{n+1});
end
% if isempty(defaults.endtime)
%   defaults.endtime = getendtime(mia);
% end
if defaults.autoprintdevice(1) ~= '-';
  defaults.autoprintdevice = ['-' defaults.autoprintdevice];
end
if isempty(defaults.autoprintfilenameextension)
  switch defaults.autoprintdevice
    case {'-dps' '-dpsc' '-dps2' '-dpsc2'}
      ext = 'ps';
    case {'-deps' '-depsc' '-deps2' '-depsc2'}
      ext = 'eps';
    case '-dhpgl'
      ext = 'hpgl';
    case '-dtiff' 
      ext = 'tiff';
    case {'-dpcxmono' '-dpcx16' '-dpcx256' '-dpcx24b'}
      ext = 'pcx';
    case {'-dpbm' '-dpbmraw'}
      ext = 'pbm';
    case {'-dpgm' '-dpgmraw'}
      ext = 'pgm';
    case {'-dppm' '-dppmraw'}
      ext = 'ppm';
    otherwise
      ext = 'prn';
  end
  defaults.autoprintfilenameextension = ext;
end

imageStepTS = defaults.step * getresolution(mia); % timespan between images
plotDuration = prod(defaults.miniplots) * imageStepTS;

st = defaults.starttime;
et = defaults.endtime;
t1 = st;
failedtries = 0; % the number of times aligning failed
page = 1;

while t1 < et
  align = 0;
  if defaults.autoprintoverlap > 0 ...
	& failedtries <= defaults.autoprinttries ...
	& isequal(defaults.miniplots, [8 8]) ...
	& rem(defaults.starttime, imageStepTS) == timespan
    % attempt to be smart
    for n = 0:1:defaults.autoprintoverlap
      tTest = max(t1 - n*imageStepTS, st);
      if rem(tTest, 60*imageStepTS) == timespan
	% align to 60*imageStep
	align = 1;
      elseif rem(tTest, timespan(1, 'm')) == timespan
	% align to 1 minute boundary
	align = 1;
      end
      if align
	t1 = tTest;
	break;
      end
    end
  end
  if ~align
    failedtries = failedtries + 1;
  end
  
  t2 = min(t1 + plotDuration, et);
  [fh gh] = imageplot(mia, args{:}, varargin{:}, ...
      'starttime', t1, ...
      'endtime', t2);
  if defaults.autoprintappend
    print(fh, defaults.autoprintdevice, '-append', ...
	sprintf('%s.%s', defaults.autoprintfilename, ...
	defaults.autoprintfilenameextension));
  else
    print(fh, defaults.autoprintdevice, ...
	sprintf('%s_p%d.%s', defaults.autoprintfilename, page, ...
	defaults.autoprintfilenameextension));
  end
  close(fh);
  fh = [];
  t1 = t2;
  page = page + 1;
end

% have to return fh and gh since calling functions expect them, however they
% are too late to do anything useful with them. Since we have already called
% the derived version imageplot with autoprint=off for each page, the
% original imageplot() should have already done everything necessary


return; 










