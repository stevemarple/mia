function [r, units] = linearise_rio_kar_2(mia)
%LINEARISE_RIO_KAR_2  Linearise riometer data from rio_kar_2.
%
%   r = LINEARISE_RIO_KAR_2(mia)
%   r: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

[r, units] = linearise_lancsrio(mia);
