function [status,mesg,filename2,x] = mia2cdf(mia, filename, varargin)
%MIA2CDF  Convert a RIO_BASE object to a CDF file.
%
% f = mia2cdf(mia, filename)

isofstr = '%Y-%m-%d %H:%M:%S.%#';

% enable/disable debugging message in cdf_tools mex files
defaults.debug = 0;

% write out the skeleton object
defaults.skeleton = 0;
% column majority is more efficient for Matlab
defaults.majority = 'column';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

instrument = getinstrument(mia);
in_abbrev_uc = upper(getabbreviation(instrument));
in_sn = getserialnumber(instrument);
starttime = getstarttime(mia);
endtime = getendtime(mia);
[res resmesg] = getresolution(mia);

beams = getbeams(mia);
allbeams = info(instrument, 'allbeams');
bestresolution = info(instrument, 'bestresolution');
proc = getprocessing(mia);

if logical(defaults.skeleton)
  starttime = info(instrument, 'starttime')
  endtime = info(instrument, 'endtime');
  if ~isvalid(endtime)
    endtime = timestamp('9999-01-01');
  end
  beams = allbeams;
  proc = {''};
end


if isempty(proc)
  proc = {''};
end

% ga = global_attributes
data_version = 1;

ga.ADID_ref = 'NSSD0110'; % 'NSSD0241';
ga.Project = 'ISTP>International Solar-Terrestrial Physics';
ga.Source_name = 'MIA';
ga.Discipline = 'Space Physics>Ionospheric Science';

% For the Key parameter number use:
% 0: absorption
% 1: power
% 2: qdc
switch class(mia)
 case 'rio_abs'
  data_type_num = 1;
 case {'rio_power' 'rio_rawpower'}
  data_type_num = 2;
 case {'rio_qdc' 'rio_rawqdc' 'rio_qdc' 'rio_rawqdc'}
  data_type_num = 3
 otherwise
  error(sprintf('unknown class for Data_type number (was ''%s'')', ...
		class(mia)));
end

% Does the object contain all beams at highest resolution?
if isequal(beams, allbeams) & res == bestresolution
  data_type_fstr = 'H%d>High Resolution data';
else
  data_type_fstr = 'K%d>Key Parameter';
end

ga.Data_type = sprintf(data_type_fstr, data_type_num);

ga.Descriptor = sprintf('%s%d>Riometer (%s #%d)', ...
			     in_abbrev_uc, in_sn, in_abbrev_uc, in_sn);

ga.Data_version = num2str(data_version);
ga.PI_name = 'unknown';
ga.PI_affiliation = 'unknown';
ga.TEXT = 'no bibliography';

ga.Instrument_type = 'Ground-Based Magnetometers, Riometers, Sounders';
ga.Mission_group = 'Ground-Based Investigations';

ga.Logical_source = ['GLORIA' '_' ...
		    local_attrib_first_part(ga.Data_type) '_' ...
		    local_attrib_first_part(ga.Descriptor) ...
		   ];

ga.Logical_file_id = ...
    [ga.Logical_source '_' ...
     strftime(starttime, '%Y%m%d_') ...
     sprintf('V%02d', data_version)];

ga.Logical_source_description = [gettype(mia) ' from ' ...
		    getcode(instrument, 1)];

ga.Generated_by = sprintf('MIA (%s)', getcreator(mia));
ga.Generation_date = strftime(getcreatetime(mia), isofstr);
ga.MODS = sprintf('%s\n', proc{:});

if isempty(resmesg)
  ga.Time_resolution = char(res);
else
  resguess = guessresolution(mia);
  if isvalid(resguess)
    ga.Time_resolution = [char(guessresolution(mia)) ...
                        ' (not constant)'];
  else
    ga.Time_resolution = 'unknown';
  end
end


ga.GAIA_version = uint16(0);
ga.Instrument_class = class(instrument);
ga.Start_time = starttime;
ga.End_time = endtime;


% va = variable_attributes
% EPOCH
va.CATDESC.Epoch = 'Sample time';
va.DEPEND_0.Data = 'Epoch';
va.DICT_KEY.Epoch = 'time>Epoch';
va.DISPLAY_TYPE.Epoch = ' ';
va.FIELDNAM.Epoch = 'Time Line';
va.FILLVAL.Epoch = -1e31;
va.FORMAT.Epoch = 'F16.2';
va.MONOTON.Epoch = 'INCREASE';
va.SCALEMIN.Epoch = strftime(starttime, isofstr);
va.SCALEMAX.Epoch = strftime(endtime, isofstr);
va.UNITS.Epoch = 'ms';
va.VALIDMAX.Epoch = endtime;
va.VALIDMIN.Epoch = starttime;
va.VAR_TYPE.Epoch = 'support_data';


% DATA
va.CATDESC.Data = ['Riometer ' gettype(mia, 'l')];
va.DICT_KEY.Data = ' ';
va.DISPLAY_TYPE.Data = 'time_series';
va.FIELDNAM.Data = datalabel(mia);
va.FILLVAL.Data = -1e31;
va.FORMAT.Data = [num2str(numel(beams)) 'G13.6'];
va.UNITS.Data = getunits(mia);
if isempty(va.UNITS.Data)
  error('units not set');
end
if isa(mia, 'rio_abs')
  va.VALIDMAX.Data = 25;
  va.VALIDMIN.Data = -1;
end
va.VAR_TYPE.Data = 'data';


% BEAMS
va.CATDESC.Beams = 'Beam number';
va.DEPEND_1.Data = 'Beams';
va.DICT_KEY.Beams = 'label>beams';
va.DISPLAY_TYPE.Beams = ' ';
va.FIELDNAM.Beams = 'Beam number';
va.FILLVAL.Beams = int32(-2147483648);
va.FORMAT.Beams = 'I';
if numel(mia) == 1
  va.MONOTON.Beams = 'INCREASE';
end
va.UNITS.Beams = ' ';
va.VALIDMAX.Beams = int32(max(allbeams));
va.VALIDMIN.Beams = int32(min(allbeams));
va.VAR_TYPE.Beams = 'support_data';

% FREQUENCY
va.CATDESC.Frequency = 'Centre frequency';
va.FIELDNAM.Frequency = 'Frequency';
va.DICT_KEY.Frequency = 'frequency>';
va.UNITS.Frequency = 'Hz';
va.VAR_TYPE.Frequency = 'support_data';

if numel(beams) <= 1
  va.LABLAXIS.Epoch = {'Epoch'};
  va.LABLAXIS.Data = datalabel(mia);
  va.LABLAXIS.Beams = 'Beam number';
else
  va.LABL_PTR_1.Epoch = 'label_epoch';
  va.LABL_PTR_1.Data = 'label_data';
  va.LABL_PTR_1.Beams = 'label_beams';
  
  va.FIELDNAM.label_epoch = 'Label for Epoch';
  va.FIELDNAM.label_data = 'Label for Data';
  va.FIELDNAM.label_beams = 'Label for Beams';

  va.CATDESC.label_epoch = 'Label for Epoch';
  va.CATDESC.label_data = 'Label for Data';
  va.CATDESC.label_beams = 'Label for Beams';

  va.FORMAT.label_epoch ='A';
  va.FORMAT.label_data = 'A';
  va.FORMAT.label_beams = 'A';

  va.VAR_TYPE.label_epoch = 'metadata';
  va.VAR_TYPE.label_data = 'metadata';
  va.VAR_TYPE.label_beams = 'metadata';
end


data = getdata(mia);
data(isnan(data)) = va.FILLVAL.Data;


YYYYmmdd = strftime(starttime, '%Y%m%d');
if logical(defaults.skeleton)
  YYYYmmdd = '00000000';
  % epoch = repmat(timestamp, 0, 0);
  % data = {[]};
end


defaultfilename = lower([ga.Logical_source '_' ...
		    YYYYmmdd '_' ...
		    sprintf('V%02d', data_version)]);

if isempty(filename)
  filename = defaultfilename;
end



label_epoch = 'Epoch';
label_data = cell(1, numel(beams));
for n = 1:numel(beams)
  label_data{n} = sprintf('%s (beam %d)', gettype(mia, 'c'), beams(n));
end
label_data = gettype(mia, 'c');
label_beams = 'Beam number';

% Variable metadata
vars_md = [];

if logical(defaults.skeleton) 
  vars.Epoch = timestamp('cdfepoch', 0);
  vars.Data = data;
  vars.Beams = int32(allbeams);
else
  epoch = getsampletime(mia);
  vars.Epoch = epoch;
  vars.Data = data;
  vars.Beams = int32(beams);
end

if numel(beams) > 1
  vars.label_epoch = {label_epoch};
  vars.label_data = {label_data};
  vars.label_beams = {label_beams};
end

vars.Project = getfacility(instrument);
if isempty(vars.Project)
  vars.Project = 'GloRiA';
end
vars.Project = {vars.Project};

vars.Site_code = {upper(getabbreviation(instrument))};
vars.Device = {getcode(instrument)};
vars.Frequency = getfrequency(instrument);



% Set the padvalues from the FILLVAL variable attributes
fn = fieldnames(va.FILLVAL);
for n = 1:numel(fn)
  if isfield(vars_md, fn{n})
    tmp = getfield(vars_md, fn{n});
  else
    tmp = [];
  end
  tmp.padvalue = getfield(va.FILLVAL, fn{n});
  vars_md = setfield(vars_md, fn{n}, tmp);
end

variable_rename = {};
if isa(mia, 'rio_abs')
  variable_rename(end+(1:2)) = {'Data' 'Absorption'};
elseif isa(mia, 'rio_power') | isa(mia, 'rio_rawpower')
  variable_rename(end+(1:2)) = {'Data' 'Raw_data'};
end

[status mesg x] = cdf_write(filename, ...
                            'debug', defaults.debug, ...
                            'majority', defaults.majority, ...
                            'variables', vars, ...
                            'variables_metadata', vars_md, ...
                            'variable_attributes', va, ...
                            'global_attributes', ga, ...
                            'variable_rename', variable_rename);

filename2 = filename;


% ----------------------------------------------
% split an attribute to return only the part before a '>'
function r = local_attrib_first_part(s)

pos = find(s == '>');
if isempty(pos)
  error(sprintf('attribute missing ''>'' (was ''%s'')', s));
elseif length(pos) > 1
  error(sprintf('attribute badly formed (was ''%s'')', s));
elseif pos == 1 
  error(sprintf('attribute badly formed (was ''%s'')', s));
end

r = s(1:(pos-1));


