function [r, m] = datalabel2(mia, varargin)

r = gettype(mia, 'c');

freqStr = '';
in = getinstrument(mia);
freq = [];
if isa(in, 'riometer')
  freq = getfrequency(in);
end

units = getunits(mia);
% state frequency for absorption
if strcmp(units, 'dB')
  switch length(freq)
   case 0
    ;
   case 1
    freqStr = sprintf(' @ %s', printunits(freq(1), 'Hz', ...
					  'formatstr', '%g'));
   otherwise
    error('frequency should be scalar (or possibly empty)');
  end
end

if any(strcmp(units, {'dB' 'dBm' 'raw' 'ADC'}))
  m = 0;
else
  m = []; % choose automatically
end

if ~isempty(units)
  r = sprintf('%s (%%s%s)', r, freqStr);
end
