function r = getparameterindex(mia, beams)
%GETPARAMETERINDEX  Return the indices for the beams requested.

sz = prod(size(beams));
r = zeros(sz,1);
if isempty(mia.beams)
  return;
end
  
for n = 1:sz
  % where this beam number occurs in mia.beams 
  ind = find(mia.beams == beams(n));
  if ~isempty(ind)
    r(n) = ind(1); % just take first reference
  else
    r(n) = 0; % not present
  end
end


