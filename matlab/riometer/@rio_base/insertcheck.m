function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for RIO_BASE data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mia_base_insertcheck(a, b);


beamsa = getbeams(a);
beamsb = getbeams(b);
rs.beams = unique([beamsa(:); beamsb(:)]);

% now modify the row details for subsasgn to include all different beams
[tmp aa.subs{1}] = ismember(beamsa, rs.beams);
[tmp ba.subs{1}] = ismember(beamsb, rs.beams);

rs.datasize(1) = numel(rs.beams);

