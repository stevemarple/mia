function [r, units] = linearise_rio_kar_1(mia)
%LINEARISE_RIO_KAR_1  Linearise riometer data from rio_kar_1.
%
%   r = LINEARISE_RIO_KAR_1(mia)
%   r: linearised data
%   mia: RIO_RAWPOWER object
%
%   See also rio_rawpower/RIO_POWER.

[r, units] = linearise_lancsrio(mia);
