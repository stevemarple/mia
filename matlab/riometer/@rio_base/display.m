function varargout = display(mia, varargin)
%DISPLAY Display a RIO_BASE object.
%
%   DISPLAY(mia)
%   h = DISPLAY(mia, parent, pos1, pos2)
%
%   h: vectors of handles created
%   parent: handle of parent FIGURE
%   pos1: position vector for labels
%   pos2: position vector for information
%
%   Use the second form for displaying a MIA_BASE object into a window
%
%   See also MIA_BASE, DISP, FIGURE.

switch nargin
 case 1
  % mia_base_display(mia, varargin{:});
  disp(sprintf('\n%s = \n\n%s', inputname(1), char(mia)));
      
 case 4
  parent = varargin{1};
  tmppos1 = varargin{2};
  tmppos2 = varargin{3};
  
  bgColour = get(parent, 'Color');
  
  spacing = [5 5];
  handles = zeros(1,4);

  % data
  handles(1) = uicontrol(parent, 'Position', tmppos1, ...
			 'Style', 'text', ...
			 'String', 'Data:', ...
			 'HorizontalAlignment', 'right', ...
			 'BackgroundColor', bgColour, ...
			 'Enable', 'inactive');
  
  handles(2) = uicontrol(parent, 'Position', tmppos2, ...
			 'Style', 'text', ...
			 'String', matrixinfo(getdata(mia)), ...
			 'HorizontalAlignment', 'left', ...
			 'BackgroundColor', bgColour, ...
			 'Enable', 'on');
  
  tmppos = uiresize(handles(1:2), 'rl', 'bb');
  tmppos1 = tmppos(1,:) + [0 tmppos(1,4)+spacing(2) 0 0];
  tmppos2 = tmppos(2,:) + [0 tmppos(2,4)+spacing(2) 0 0];
  
  % beams
  handles(3) = uicontrol(parent, 'Position', tmppos1, ...
			 'Style', 'text', ...
			 'String', 'Beams:', ...
			 'HorizontalAlignment', 'right', ...
			 'BackgroundColor', bgColour, ...
			 'Enable', 'inactive');
  
  handles(4) = uicontrol(parent, 'Position', tmppos2, ...
			 'Style', 'text', ...
			 'String', printseries(mia.beams), ...
			 'HorizontalAlignment', 'left', ...
			 'BackgroundColor', bgColour, ...
			 'Enable', 'on');
  
  tmppos = uiresize(handles(3:4), 'rl', 'bb');
  tmppos1 = tmppos(1,:) + [0 tmppos(1,4)+spacing(2) 0 0];
  tmppos2 = tmppos(2,:) + [0 tmppos(2,4)+spacing(2) 0 0];
  
  
  varargout{1} = [handles display(mia.mia_base, parent, ...
				  tmppos1, tmppos2)]; 
  % (tmppos1 + [0 tmppos1(4)+spacing(2) 0 0]), ...
  % (tmppos2 + [0 tmppos2(4)+spacing(2) 0 0]))];
  ;
  
  
 otherwise
  error('Incorrect parameters'); 
end

