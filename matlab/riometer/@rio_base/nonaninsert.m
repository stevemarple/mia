function [r, aa, ar, ba, br, rs] = nonaninsert(a, b)
%NONANINSERT  Overload NONANINSERT for RIO_BASE.
%
% See mia_base/NONANINSERT.

[r, aa, ar, ba, br, rs] = mia_base_nonaninsert(a, b);

r = setbeams(r, rs.beams);
