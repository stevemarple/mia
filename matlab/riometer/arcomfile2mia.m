function [mia, FPGAinfo_r] = ...
    arcomfile2mia(rio, filename, cls, beams, want_calib_data, bestres, ...
		  complex_data)
%ARCOMFILE2MIA  Read a ARCOM format file and return as a MIA object

deprecated('please use arcom_file_to_mia');

orig_beams = beams;

% get the position where the beam occurs in the arcom data array
arcombeammap = info(rio, 'arcombeammap');

requested_indices = arcombeammap(beams(:));

cls_obj = feval(cls);
if isa(cls_obj, 'rio_rawpower_calib') | isa(cls_obj, 'rio_power_calib')
  want_calib_data = []; % get all values
  uses_calib_data = 1;
  switch info(rio, 'systemtype')
   case 'iris'
    % use middle imaging beam
    calibration_beams = info(rio, 'imagingbeams');
    calibration_beams = calibration_beams((numel(calibration_beams) + 1) ./ 2);

    beams = unique([beams calibration_beams]);
   
   case 'aries'
    % use all beams;
    calibration_beams = beams;
   otherwise
    error(sprintf('system unknown to %s (was ''%s'')', mfilename, ...
		  info(rio, 'systemtype')));
  end
else
  uses_calib_data = 0;
end

[AllBeams,FPGAHopfTime,FPGAinfo,postinteg] = ...
    read_arcom_file2(filename, rio, want_calib_data);

%==========================================================================
% Get required Data and append to previous
% data = repmat(NaN,length(beams),size(AllBeams,2) );

% for i=1:1:length(requested_indices)
%   data(i,:) = abs(AllBeams(requested_indices(i),:)); 
% end      

if logical(complex_data)
  % keep data as complex values
  data = AllBeams(requested_indices, :);
else
  data = abs(AllBeams(requested_indices, :));
end

if isempty(data)
  mia = [];
  return;
end

% calculate sample times
sampletime = timestamp(FPGAHopfTime.date) + bestres ./ 2;

if length(sampletime) ~= size(data, 2)
  whos
  error('number of timestamps does not match number of samples');
end

% For postintegrated data use the mean of start and (end+1s) times. Note
% that endtime is the start of the last sample, not the end of the last
% sample, hence we must add bestres
ispostintegrated = (postinteg.nestinglevel > 1);
unixepoch = timestamp([1970 1 1 0 0 0]);
bestres_s = gettotalseconds(bestres);
sampletime(ispostintegrated) = ...
    unixepoch + timespan(mean([postinteg.starttime(ispostintegrated);
		    (postinteg.endtime(ispostintegrated)+bestres_s)], 1), 's');

integrationtime = repmat(bestres, size(sampletime));
integrationtime(ispostintegrated) = ...
    timespan(postinteg.secondcounter(ispostintegrated), 's');

% put data into the correct order using the timestamps
[sampletime idx] = sort(sampletime);
data2 = data(:, idx);

% ensure the FPGAinfo returned is in the same order
% FPGAinfo_r = FPGAinfo(idx);
FPGAinfo_r = FPGAinfo;
fn = fieldnames(FPGAinfo_r);
for n = 1:length(fn)
  tmp = getfield(FPGAinfo_r, fn{n});
  tmp = tmp(idx);
  FPGAinfo_r = setfield(FPGAinfo_r, fn{n}, tmp);
end


% Set start and end times ...          
%        st = sampletime(1);
%        et = sampletime(end)+ timespan(1,'s');
st = min(sampletime - integrationtime./2);
et = max(sampletime + integrationtime./2);





% construct the MIA object, of the appropriate type. data matrix to be
% added later (may need to convert to dBm or delete calibration values)
mia = feval(cls, ...
	    'starttime', st, ...
	    'endtime', et, ...
	    'sampletime', sampletime, ...
	    'integrationtime', integrationtime, ...    
	    'instrument', rio, ...
	    'load', 0, ...
	    'data', [], ...
	    'units', 'raw',...
	    'processing', ['Created by ', mfilename],...
	    'beams', beams);

if isa(mia, 'rio_power')
  % need power in dBm. From comparison with Kilpisjarvi IRIS adjust the
  % power by an empirical value -199dB
  % mia = setdata(mia, (10 .* log10(data2)) - 199);
  data2 = (10 .* log10(data2)) + info(rio, 'dbmconversion', st);
  mia = setunits(mia, 'dBm');
end


extra_args = {};
% get the calibration data into a useful form
if uses_calib_data
  % noise_active = [FPGAinfo(:).NoiseActiveFlag];
  % calib_idx = find(noise_active);
  calib_idx = find(FPGAinfo.NoiseActiveFlag);
  
  if ~isempty(calib_idx)
    % copy the good calibration values
    calibrationdata = data2(getparameterindex(mia, calibration_beams), ...
			    calib_idx);
    if numel(calibration_beams) == 1 & numel(beams) > 1
      calibrationdata = repmat(calibrationdata, numel(beams), 1);
    end
    
    
    % set ALL calibration times in the data to be nans
    data2(:, calib_idx) = nan;
    
    calibrationtimestamps = sampletime(calib_idx);
    mia = setcalibrationdata(mia, calibrationdata, calibrationtimestamps);
  end
  
end

mia = adddataquality(mia, 'Uncalibrated');


mia = setdata(mia, data2);


if ~isequal(beams, orig_beams)
  % had to add extra beams to get the calibration information. Extract
  % only the request beams
  mia = extract(mia, 'beams', orig_beams);
end
