function r = info_rio_tjo_1_data
%INFO_RIO_TJO_1_DATA Return basic information about rio_tjo_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_tjo_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=tjo;serialnumber=1

r.limits = [];
r.limits.original_format = [0 4096];
r.limits.rio_power = [0 4096];
r.limits.rio_qdc = [0 4096];
r.limits.rio_rawpower = [0 4096];
r.limits.rio_rawqdc = [0 4096];
r.pixels.aacgm.x.max = 74.8;
r.pixels.aacgm.x.min = 71.6;
r.pixels.aacgm.x.step = 0.2;
r.pixels.aacgm.y.max = 68;
r.pixels.aacgm.y.min = 66.3;
r.pixels.aacgm.y.step = 0.1;
r.pixels.deg.x.max = -15.5;
r.pixels.deg.x.min = -18.7;
r.pixels.deg.x.step = 0.1;
r.pixels.deg.y.max = 66.9;
r.pixels.deg.y.min = 65.5;
r.pixels.deg.y.step = 0.1;
r.pixels.km.x.max = 80;
r.pixels.km.x.min = -80;
r.pixels.km.x.step = 8;
r.pixels.km.y.max = 80;
r.pixels.km.y.min = -80;
r.pixels.km.y.step = 8;
r.pixels.km_antenna.x.max = 80;
r.pixels.km_antenna.x.min = -80;
r.pixels.km_antenna.x.step = 8;
r.pixels.km_antenna.y.max = 80;
r.pixels.km_antenna.y.min = -80;
r.pixels.km_antenna.y.step = 8;
r.pixels.m.x.max = 80000;
r.pixels.m.x.min = -80000;
r.pixels.m.x.step = 8000;
r.pixels.m.y.max = 80000;
r.pixels.m.y.min = -80000;
r.pixels.m.y.step = 8000;
r.pixels.m_antenna.x.max = 80000;
r.pixels.m_antenna.x.min = -80000;
r.pixels.m_antenna.x.step = 8000;
r.pixels.m_antenna.y.max = 80000;
r.pixels.m_antenna.y.min = -80000;
r.pixels.m_antenna.y.step = 8000;
r.abbreviation = 'tjo';
r.antennaazimuth = 341.296;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.65;
r.azimuth = [-45 -35.5 -23.2 -8.1 8.1 -336.8 -324.5 -315 -54.5 -45 ...
    -31 -11.3 11.3 -329 -315 -305.5 -66.8 -59 -45 -18.3 18.3 -315 ...
    -301 -293.2 -81.9 -78.7 -71.7 -45 -315 -288.3 -281.3 -278.1 ...
    -98.1 -101.3 -108.3 -135 -225 -251.7 -258.7 -261.9 -113.2 -121 ...
    -135 -161.7 -198.3 -225 -239 -246.8 -125.5 -135 -149 -168.7 ...
    -191.3 -211 -225 -234.5 -135 -144.5 -156.8 -171.9 -188.1 -203.2 ...
    -215.5 -225];
r.badbeams = [1 2 7 8 9 16 49 56 57 58 63 64];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 11.169 ...
    11.169 11.169 11.169];
r.bibliography = 'art:314';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2010 09 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1 9];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 16;
r.ifrequency = 3e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 ...
    62 63 64];
r.latitude = 66.2;
r.location1 = 'Tjornes';
r.location1_ascii = '';
r.location2 = 'Iceland';
r.logo = '';
r.logurl = '';
r.longitude = -17.1;
r.modified = timestamp([2011 07 29 12 11 30.177229]);
r.name = '';
r.piid = 14;
r.qdcclass = 'rio_rawqdc';
r.qdcduration = timespan(28 , 'd');
r.qdcoffset = timespan(00, 'h', 00, 'm', 00, 's');
r.resolution = timespan(00, 'h', 00, 'm', 04, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ...
    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ...
    1 1 1 1 1 1 1 1 1 1];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = 'http://www.nipr.ac.jp/english/r_groups/t01_atmosphere_a.html';
r.url2 = {'http://www.nipr.ac.jp/english/r_groups/t01_atmosphere_a.html'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [72.1 55.8 47.1 42.8 42.8 47.1 55.8 72.1 55.8 42.8 34.1 ...
    29.3 29.3 34.1 42.8 55.8 47.1 34.1 24.1 17.7 17.7 24.1 34.1 ...
    47.1 42.8 29.3 17.7 7.8 7.8 17.7 29.3 42.8 42.8 29.3 17.7 7.8 ...
    7.8 17.7 29.3 42.8 47.1 34.1 24.1 17.7 17.7 24.1 34.1 47.1 55.8 ...
    42.8 34.1 29.3 29.3 34.1 42.8 55.8 72.1 55.8 47.1 42.8 42.8 ...
    47.1 55.8 72.1];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1 , 'd');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'niprriodata';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/original_format/%Y/%m/%y%m%d%H.TJO';
r.defaultfilename.original_format.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.original_format.default.resolution = timespan([], 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 21600];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'niprriodata';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/original_format/%Y/%m/%y%m%d%H.TJO';
r.defaultfilename.rio_power.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_power.default.resolution = timespan([], 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 21600];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = false;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan([], 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [64 900];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = false;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan([], 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [64 -1];
r.defaultfilename.rio_qdc_fft.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'niprriodata';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/original_format/%Y/%m/%y%m%d%H.TJO';
r.defaultfilename.rio_rawpower.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan([], 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 21600];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = false;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan([], 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [64 900];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.defaultfilename.rio_rawqdc_fft.default.archive = 'default';
r.defaultfilename.rio_rawqdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc_fft.default.failiffilemissing = false;
r.defaultfilename.rio_rawqdc_fft.default.format = 'mat';
r.defaultfilename.rio_rawqdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/tjo_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc_fft.default.loadfunction = '';
r.defaultfilename.rio_rawqdc_fft.default.resolution = timespan([], 's');
r.defaultfilename.rio_rawqdc_fft.default.savefunction = '';
r.defaultfilename.rio_rawqdc_fft.default.size = [64 -1];
r.defaultfilename.rio_rawqdc_fft.default.units = 'ADC';
r.institutions{1} = 'National Institute for Polar Research';
% end of function
