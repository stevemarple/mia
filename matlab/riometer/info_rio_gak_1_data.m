function r = info_rio_gak_1_data
%INFO_RIO_GAK_1_DATA Return basic information about rio_gak_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_gak_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=gak;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'gak';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 42;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 62.3917;
r.location1 = 'Gakona';
r.location1_ascii = '';
r.location2 = 'Alaska';
r.logo = '';
r.logurl = '';
r.longitude = -145.147;
r.modified = timestamp([2011 07 08 11 28 03.87778]);
r.name = '';
r.piid = 6;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = '2x2 5-element Yagi';
r.wbeams = 1;
r.wfrequency = 3e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'University of Maryland';
% end of function
