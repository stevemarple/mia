function r = rio_res_1
%RIO_RES_1  RIOMETER object for Resolute, Canada.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'res', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'NRCAN', ...
    'location', location('Resolute', 'Canada', ...
                         74.700000, -94.900000), ...
    'frequency', 3e+07);

