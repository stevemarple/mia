function mia = niprriodata_file_to_mia(filename, varargin)
%NIPRRIODATA_FILE_TO_MIA Convert NIPR riometer data file to MIA object.
%
%   mia = NIPRRIODATA_FILE_TO_MIA(filename)
%   mia = RIO_RAWPOWER object
%   filename: CHAR

% required options for normal import functions
defaults.mia = [];
defaults.cancelhandle = [];
defaults.starttime = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});


% these values are good for all current NIPR riometers
record_size = 1024;
default_frames_per_record = 8;
number_of_beams = 64;
bn = sqrt(number_of_beams);
beam_order = reshape(fliplr(reshape(1:number_of_beams, [bn bn])'), ...
			     [1 number_of_beams]);
sample_format = 'uint16';

% calculate default beams
if isempty(defaults.mia)
  beams = [];
else
  beams = getbeams(defaults.mia);
end
if isempty(beams)
  beams = 1:number_of_beams;
end

% get riometer details
rio = [];
if ~isempty(defaults.mia)
  rio = getinstrument(defaults.mia);
end


% preallocate timestamp and data matrix
fid = url_fopen(filename, 'r', 'ieee-le');
disp(sprintf('loading %s', filename));
fseek(fid, 0, 'eof');
file_size = ftell(fid);
fseek(fid, 0, 'bof');

t_s = repmat(nan, [1 floor(file_size ./ record_size) * ...
		   default_frames_per_record]);

% integration time stored in seconds whilst reading records
integ_s = repmat(nan, [1 numel(t_s)]);
data = repmat(nan, [numel(beams) numel(t_s)]);
t_idx = 1;

date = []; % remember date in the file
while 1
  cancelcheck(defaults.cancelhandle);

  [rt, rd, fpos, scan_rate] = ...
      localReadRecord(fid, filename, beam_order, ...
                      default_frames_per_record, sample_format);

  if isempty(rd)
    break; % no data to insert
  end
  
  if isempty(date)
    date = rt(1:3);
  elseif ~isequal(date, rt(1:3))
    error('date in file changed from %04d-%02d-%02d to %04d-%02d-%02d', ...
          date, rt(1:3));
  end
  
  last_scan_rate = scan_rate; % remember last valid scan rate
  % riometer still unknown, try to get from filename. Do it here so that
  % operating time can be taken into account
  if isempty(rio)
    rio_list = [rio_nipr rio_nal_1];
    [tmp1 tmp2 ext] = fileparts(filename);
    ext(1) = []; % remove leading '.'
    rio = rio_list(find(strcmp(lower(ext), getabbreviation(rio_list))));
    
    switch numel(rio)
     case 0
      error('riometer not specified and cannot deduce from filename');
     case 1
      ; % matched
     otherwise
      % oops! Maybe there are multiple riometers with the same abbreviation
      % (but different serial numbers). This is likely to happen when the
      % new Syowa riometer goes online. Could probably guess which is
      % correct from the operating times.
      
      % this warning can be deleted when code is fully tested
      warning('multiple matches when deducing riometer');
      
      % try to get correct riometer (untested)
      rio = rio(isoperational(rio, timestamp(rt)));
      
      if numel(rio) ~= 1
	 % still do not get exact match
	error('riometer not specified and cannot deduce from filename (#2)');
      end	
      
    end
  end

  sample_offsets_s = (scan_rate/2):scan_rate:(scan_rate*(default_frames_per_record));
  
  % calculate sample times for the records (as seconds of the day)
  rt_s = rt(4:6) *[3600; 60; 1];
  rst_s = rt_s + sample_offsets_s(1:size(rd,2));

  t_idx2 = t_idx:(t_idx + size(rd, 2) - 1);
      
  % insert appropriate beams into data matrix
  data(:, t_idx2) = rd(beams, :);
  
  % insert sample times into timestamp matrix, and integrationtime matrix
  t_s(t_idx2) = rst_s;
  integ_s(t_idx2) = scan_rate;
  
  % adjust the pointer into the timestamp matrix
  t_idx = t_idx + size(rd, 2);

  if feof(fid)
    break
  end
end

fclose(fid);

% trim any excess from data and sampletime matrices
data(:, t_idx:end) = [];
t_s(t_idx:end) = [];
integ_s(t_idx:end) = [];
t_numel = numel(t_s);

% ensure samples are unique and in order
[t_s t_order] = unique(t_s);
data = data(:, t_order);
integ_s = integ_s(t_order);

if ~isequal(numel(t_s), t_numel)
  disp(sprintf('%d sample(s) lost after ensuring only unique samples', ...
               t_numel - numel(t_s)));
end

t = timestamp([date 0 0 0]) + timespan(t_s, 's');
starttime = [];
if ~isempty(defaults.starttime)
  starttime = defaults.starttime;
elseif ~isempty(defaults.mia)
  starttime = getstarttime(defaults.mia);
end

if isempty(starttime) & ~isempty(t)
  starttime = t(1);
end

data = uint16(data);

if isa(defaults.mia, 'rio_qdc_base')
  cls = 'rio_rawqdc';
else
  cls = 'rio_rawpower';
end

mia = feval(cls, ...
	    'starttime', starttime, ...
	    'sampletime', t,  ...
	    'instrument', rio, ...
	    'data', data, ...
	    'units', 'ADC', ...
	    'beams', beams);


if ~isempty(t)
  res = guessresolution(mia);
  mia = setendtime(mia, t(end) + timespan(last_scan_rate / 2, 's'));
  mia = setintegrationtime(mia, timespan(integ_s, 's'));
end


if isa(defaults.mia, 'mia_base') ...
      & ~strcmp(class(mia), class(defaults.mia))
  mia = feval(class(defaults.mia), mia);
end
  
% ----------------------------------
function [rt, rd, fpos, scan_rate] = ...
      localReadRecord(fid, fname, beam_order, ...
                      default_frames_per_record, sample_format)
%LOCALREADRECORD  Read a record in the file
% 
%   rt: record time, seconds of day
%   rd: record data, DOUBLE
%   fpos: file position of start of record

rt = [];
rd = [];
fpos = ftell(fid);
scan_rate = nan;
% read the time
sz = [1 6];
[dv c] = fread(fid, sz, 'uint8');
if c ~= prod(sz)
  return;
end

dv_orig = dv;
if dv(1) < 80
  dv(1) = dv(1) + 2000;
else
  dv(1) = dv(1) + 1900;
end

if any(dv > [2099 12 31 23 59 59]) | any(dv < [1980 1 1 0 0 0])
  disp(sprintf(['bad timestamp : [%02d %02d %02d %02d %02d %02d]\n' ...
		'file name     : %s\n' ...
		'file position : %d\n' ...
		'block start   : %d'], ...
	       dv_orig(1:6), fname, ftell(fid), fpos));
else
  rt = dv;
end

% read the sampling interval (beam scan rate)
[scan_rate c] = fread(fid, 1, 'uint16');
if c ~= 1
  return;
end
scan_rate = scan_rate * 0.01; % convert from x10ms to seconds

% skip the next 7 bytes
fseek(fid, 7, 0);

% last header byte is number of frames in this record
frames = fread(fid, 1, 'uint8');
if frames == 0
  frames = default_frames_per_record;
end

% read all frames in one record
sz = [prod(size(beam_order)) frames];
[rd c] = fread(fid, sz, sample_format);
if c ~= prod(sz) & 0
  % rt = [];
  % rd = [];
  % return;
  rd(sz(1),sz(2)) = nan; % resize to the desired size
  rd((c+1):(end-1)) = nan; % set all other new data to nan
end

