function r = info_rio_sna_3_data
%INFO_RIO_SNA_3_DATA Return basic information about rio_sna_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_sna_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=sna;serialnumber=3

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'sna';
r.antennaazimuth = [];
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = 19;
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 110;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -71.67;
r.location1 = 'Vesleskarvet (SANAE IV)';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = -2.85;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 19;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 3;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = '';
r.url = '';
r.url2 = {};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 5.14e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Potchefstroom University for Christian Higher Education';
% end of function
