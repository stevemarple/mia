function r = sgoriorawdata_file_to_mia(filename, varargin)
%SGORIORAWDATA_FILE_TO_MIA  Convert SGO riometer raw data file to MIA object.
%
%   mia = SGORIODATA_FILE_TO_MIA(filename)
%   mia: RIO_RAWPOWER object
%   filename: CHAR
%
defaults.mia = [];
defaults.cancelhandle = [];
defaults.starttime = [];
defaults.archive = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object must be set');
end
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('instrument object must be set in mia object');
end


r = defaults.mia;

df = info(rio, 'defaultfilename', r, 'archive', defaults.archive);

if isempty(defaults.starttime)
  error('starttime must be set')
end

r = setstarttime(r, defaults.starttime);
et = defaults.starttime + df.duration;
r = setendtime(r, et);

r = setunits(r, df.units);

% ascii
tmp = url_load(filename);

data = tmp(:, 1)';
ymd = tmp(:, 2);
hms = tmp(:, 3);

day = rem(ymd, 100);
month = rem(fix(ymd / 100), 100);
year = fix(ymd / 10000);

second = rem(hms, 100);
minute = rem(fix(hms / 100), 100);
hour = fix(hms / 10000);

% Time are start of smaple, need middle
samt = (timestamp([year month day hour minute second]) + df.resolution/2)';
r = setdata(r, feval(df.dataclass, data));
r = setsampletime(r, samt);
r = setintegrationtime(r, repmat(df.resolution, size(samt)));

