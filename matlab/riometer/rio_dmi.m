function r = rio_dmi
%RIO_DMI  Return list of DMI riometers
%
%   r = RIO_DMI
%   r: vector of RIOMETER objects
%
% See also RIOMETER.

rios = instrumenttypeinfo(riometer,'aware');
r = rios(strcmp('DMI riometer chain', getfacility(rios)));


