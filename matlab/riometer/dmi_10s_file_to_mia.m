function r = dmi_10s_file_to_mia(filename, varargin)
%DMI_10S_FILE_TO_MIA  Convert DMI 10s data to MIA object.
%
%   mia = DMI_10S_FILE_TO_MIA(filename)
%   mia: RIO_(RAW)POWER object
%   filename: CHAR
%

defaults.mia = [];
defaults.cancelhandle = [];
defaults.starttime = [];
defaults.archive = '';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% calculate default beams
if isempty(defaults.mia)
  beams = [];
else
  beams = getbeams(defaults.mia);
end
if isempty(beams)
  beams = 1:number_of_beams;
end

map = fliplr(reshape(1:64,[8 8])');
if isempty(defaults.mia)
  error('mia object must be set');
end
rio = getinstrument(defaults.mia);
if isempty(rio)
  error('instrument object must be set in mia object');
end


r = defaults.mia;

df = info(rio, 'defaultfilename', r, 'archive', defaults.archive);

if isempty(defaults.starttime)
  error('starttime must be set')
end

r = setstarttime(r, defaults.starttime);
et = defaults.starttime + df.duration;
r = setendtime(r, et);
% r = setunits(r, df.units);
r = setunits(r, df.units);

% ascii
tmp = url_load(filename);

% Check size
sz = [numel(info(rio, 'allbeams')) df.duration/df.resolution];

data = repmat(nan, sz);
if ~isequal(size(tmp), [8 * sz(2), 8])
  warning(sprintf('incorrect datasize for %s: disregarding data', filename));
  return
else
  if strcmp(df.units, 'A')
    % Data is in units of 0.001 mA
    tmp = tmp * 0.001e-3;
  elseif strcmp(df.units, 'ADC')
    % do nothing
  else
    error(sprintf(['unknown data units (was ''' df.units ''')']));
  end

  for n = 0:(size(tmp, 1)-1)
    dt = fix(n/8) + 1; % time index
    mr = mod(n, 8) + 1; % map row
    b = map(mr, :);
    data(b, dt) = tmp(n+1, :)';
  end
end
data(data == 0) = nan;

r = setdata(r, data(beams, :));

samt = (defaults.starttime + df.resolution./2):df.resolution:et;
r = setsampletime(r, samt);
r = setintegrationtime(r, repmat(df.resolution, size(samt)));

