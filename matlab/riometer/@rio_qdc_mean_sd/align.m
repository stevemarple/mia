function [r, stddev] = align(mia, varargin)
%Overloaded version of ALIGN for rio_qdc_mean_sd and rio_rawqdc_mean_sd.
%
% [r, stddev] = ALIGN(...);
% r: power data
% stddev: aligned standard deviation
%
% See rio_qdc_base/ALIGN for more details.

% do normal align
r = rio_qdc_base_align(mia, varargin{:});

% now align the standard deviation
s = rio_qdc(mia);

s = setdata(s, getstddev(mia));

stddev = align(s, varargin{:});
