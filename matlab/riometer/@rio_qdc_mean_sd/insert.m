function [r,aa,ar,ba,br,rs] = insert(a, b)

[r,aa,ar,ba,br,rs] = rio_base_insert(a, b);

r.stddev = repmat(a.stddev(1,1), rs.datasize);

% copy std dev from a
r.stddev = feval(rs.subsasgn, r.stddev, aa, ...
		 feval(rs.subsref, a.stddev, ar));

% copy std dev from b
r.stddev = feval(rs.subsasgn, r.stddev, ba, ...
		 feval(rs.subsref, b.stddev, br));
