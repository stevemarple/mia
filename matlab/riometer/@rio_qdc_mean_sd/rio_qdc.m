function r = rio_qdc(mia)
%RIO_QDC Convert a RIO_QDC_MEAN_SD to a RIO_QDC.
%
%   r = RIO_QDC(mia)
%   r: RIO_QDC object(s)
%   mia: RIO_QDC_MEAN_SD object(s)
%
%   See also RIO_QDC_MEAN_SD.

r = repmat(rio_qdc, size(mia));

for n = 1:numel(mia)
  tmp = mia(n);
  r(n) = tmp.rio_qdc;
end

