function [fh, gh, ph] = plot(mia, varargin)
%PLOT  Plot data from a RIO_QDC_MEAN_SD or RIO_RAWQDC_MEAN_SD object.
%
% The 'zdata' option can be used to specify the ZData values for the mean
% QDC, and the +/- standard deviation QDCs. It can be given as either a
% NUMERIC or CELL array. If just one value is given it is used for both
% QDCs, otherwise the first value is used for the mean QDC and the second
% for the +/- standard deviation QDCs.
%
% See rio_base/PLOT and mia_base/PLOT for more details about the specific
% options available.
%
% See also rio_base/PLOT, mia_base/PLOT.

% sanity check
if ~isequal(getdatasize(mia), size(mia.stddev))
  error('QDC mean and standard deviation matrices are different sizes');
end

instrument = getinstrument(mia);
beams = getbeams(mia);

defaults.beams = info(instrument, 'preferredbeam', 'beams', beams);
defaults.zdata = [];
[defaults unvi] = interceptprop(varargin, defaults);

defaults.beams = sort(defaults.beams);

switch numel(defaults.zdata)
 case 0
  zdata = {[] []};
 case 1
  if iscell(defaults.zdata)
    zdata = {defaults.zdata{1} defaults.zdata{1}};
  else
    zdata = {defaults.zdata(1) defaults.zdata(1)};
  end
 otherwise
  if iscell(defaults.zdata)
    zdata = {defaults.zdata{1} defaults.zdata{2}};
  else
    zdata = {defaults.zdata(1) defaults.zdata(2)};
  end
end


[fh gh ph] = rio_qdc_base_plot(mia, varargin{unvi}, ...
                               'beams', defaults.beams, ...
			       'zdata', zdata{1});


bi = getparameterindex(mia, defaults.beams);
num = numel(defaults.beams);
ph2 = zeros(num, 3);
for n = 1:num
  p = get(ph(n));
  ph2(n, 1) = ph(n);
  if isempty(zdata{2})
    zd = [];
  else
    zd = repmat(zdata{2}, size(p.XData));
  end
  ph2(n, 2) = line('Parent', p.Parent, ...
		   'Color', p.Color, ...
		   'LineStyle', '-.', ...
		   'XData', p.XData, ...
		   'YData', p.YData + mia.stddev(bi(n), :), ...
		   'ZData', zd);
  ph2(n, 3) = line('Parent', p.Parent, ...
		   'Color', p.Color, ...
		   'LineStyle', '-.', ...
		   'XData', p.XData, ...
		   'YData', p.YData - mia.stddev(bi(n), :), ...
		   'ZData', zd);
  
end

ph = ph2;
