function mia = rio_qdc_mean_sd(varargin)
%RIO_QDC_MEAN_SD IRIS quiet day curve with standard deviation
%
%   mia = RIO_QDC_MEAN_SD
%   default constructor
%
%   mia = RIO_QDC_MEAN_SD(a)
%   a: rio_qdc type
%   Copy constructor
%
% 
% NB The behaviour of RIO_QDC_MEAN_SD and RIO_RAWQDC_MEAN_SD should be
% identical, except they have different parents (RIO_QDC or
% RIO_RAWQDC). Therefore the RIO_RAWQDC_MEAN_SD class directory is a
% symbolic link to RIO_QDC_MEAN_SD. Documention is written from the
% viewpoint of RIO_QDC_MEAN_SD, but applies to RIO_RAWQDC_MEAN_SD also (from
% this point on swap all references for RIO_QDC_MEAN_SD to
% RIO_RAWQDC_MEAN_SD).
%
% RIO_QDC_MEAN_SD is derived from RIO_QDC which means all the functions of
% RIO_QDC can be used with RIO_QDC_MEAN_SD. RIO_QDC_MEAN_SD contains all the
% data fields of RIO_QDC and also a standard deviation.
%

% This function may be called as @rio_qdc_mean_sd/rio_qdc_mean_sd.m or
% @rio_rawqdc_mean_sd/rio_rawqdc_mean_sd.m, depending on whether the real
% file or symbolic link is accessed

[tmp cls] = fileparts(mfilename);

% Unlike some other classes the parent is not a 'virtual class', so ensure
% it does not load data (or log) when it is created
if strcmp(cls, 'rio_qdc_mean_sd')
  parent = 'rio_qdc';
elseif strcmp(cls, 'rio_rawqdc_mean_sd')
  parent = 'rio_rawqdc';
else
  error(sprintf('unknown class (was ''%s'')', cls));
end
		 
% Make it easy to change the class definition at a later date
latestversion = 1;
mia.versionnumber = latestversion;

% mean value is stored in the parent class (since it is a QDC object of
% the appropriate type).

mia.stddev = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & strcmp(class(varargin{1}), parent)
  % construct from base object
  p = varargin{1};
  mia = class(mia, cls, p);
  
elseif nargin == 1 & (isa(varargin{1}, cls) | isstruct(varargin{1}))
  % copy constructor
  p = feval(parent);
  mia = class(mia, cls, p);

elseif nargin == 2 & strcmp(class(varargin{1}), parent)
  p = varargin{1};
  mia.stddev = varargin{2};
  mia = class(mia, cls, p);
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use parameter name/value interface
  % [mia unvi] = interceptprop(varargin, mia);

  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 0;
  defaults.time = [];
  defaults.beams = [];
  defaults.instrument = [];
  defaults.create = 0;
  defaults.createmethod = 'slidingmedian2qdc';
  [defaults unvi] = interceptprop(varargin, defaults);
  if isempty(defaults.instrument)
    error('instrument must be specified');
  end
  if isempty(defaults.beams)
    defaults.beams = info(defaults.instrument, 'allbeams');
  end
  mia = rmfield(defaults, {'load' 'loadoptions' 'log' 'time' 'create' ...
		    'beams', 'instrument', 'createmethod'});
    
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi}, 'load', 0);
  p = setbeams(p, defaults.beams);
  p = setinstrument(p, defaults.instrument);
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if logical(defaults.load)
    mia = loaddata(mia, defaults.time, ...
		   defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  
  elseif logical(defaults.create)
    switch char(defaults.createmethod)
     case 'fromqdcs'
      % return whole object, but save individually as with rio_qdc
      beams = getbeams(mia);
      for b = beams
	error('makefilenameandpath unknown')
	% [filename Path] = makefilenameandpath(defaults.starttime, ...
	%			      defaults.location, b);

	[status message] = mkdirhier(Path);
	if status
	  warning(message);
	end
	mia = setbeams(mia, b);
	% mia = makefromqdcs(mia);
	mia = makefromqdcs(mia, defaults);
	if ~isempty(defaults.filename)
	  filename = defaults.filename;
	end
	if ~isempty(defaults.path)
	  Path = defaults.path
	end
	save(mia);
      end
      
     otherwise
      error(sprintf('unknown createmethod (was ''%s'')', ...
		    char(defaults.create))); 
    end
  
  end

elseif nargin == 1 & iscell(varargin{1})
  % Create from a cell array of QDCs
  
  % dcadjust all relative to the first
  qdcs = varargin{1};
  dsz = getdatasize(qdcs{1});
  data = getdata(qdcs{1});
  data(end,end,numel(qdcs)) = nan; % extend matrix
  qdcdates{1} = isodate(getqdcperiodstarttime(qdcs{1}));
  for n = 2:numel(qdcs)
    if ~isequal(getdatasize(qdcs{n}), dsz)
      qdcs{1}
      qdcs{n}
      error('data size differs');
    end
    qdcs{n} = dcadjust(qdcs{n}, qdcs{1}, 'errorfunction', 'minimisestddev');
    data(:, :, n) = getdata(qdcs{n});
    qdcdates{n} = isodate(getqdcperiodstarttime(qdcs{n}));
  end
  % mia.stddev = repmat(nan, dsz);
  mia = feval(mfilename, qdcs{1});
  mia = setdata(mia, mean(data, 3));
  mia.stddev = std(data, 0, 3);
  mia = setqdcperiodstarttime(mia, timestamp('bad'));
  mia = setqdcperiodendtime(mia, timestamp('bad'));
  mia = setprocessing(mia, ['Created from QDCs for ' join(', ', qdcdates)]);
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% sanity check
if ~isequal(getdatasize(mia), size(mia.stddev))
  if isempty(mia.stddev)
    mia.stddev = repmat(nan, getdatasize(mia));
  else
    mia
    getdatasize(mia), size(mia.stddev)
    warning(['QDC mean and standard deviation matrices are different' ...
	   ' sizes: setting stddev to nans']);
    mia.stddev = repmat(nan, getdatasize(mia));
  end
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
