function r = char(mia, varargin)
%CHAR  Convert a RIO_QDC_MEAN_SD/RIO_RAWQDC_MEAN_SD object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end


r = sprintf(['%s' ...
	     'std. deviation    : %s\n'], ...
	    rio_qdc_base_char(mia, varargin{:}), ...
	    matrixinfo(mia.stddev));
    
