function r = gettype(rio, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(rio)
%   r: CHAR
%   mia: RIO_BASE object
%
%   See also RIO_QDC, RIO_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end

switch class(rio)
 case 'rio_qdc_mean_sd'
  switch mode
   case 'l'
    r = 'mean/std. dev. QDC power';
    
   case 'u'
    r = 'MEAN/STD. DEV. QDC POWER';
    
   case 'c'
    r = 'Mean/std. dev. QDC power';
    
   case 'C'
    r = 'Mean/Std. Dev. QDC Power';
    
   otherwise
    error('unknown mode');
  end

 case 'rio_rawqdc_mean_sd'
  switch mode
   case 'l'
    r = 'mean/std. dev. raw QDC power';
    
   case 'u'
    r = 'MEAN/STD. DEV. RAW QDC POWER';
    
   case 'c'
    r = 'Mean/std. dev. raw QDC power';
    
   case 'C'
    r = 'Mean/Std. Dev. Raw QDC Power';
    
   otherwise
    error('unknown mode');
  end

 otherwise
  error(sprintf('unknown input class (was ''%s'')', class(rio)));
end

  

