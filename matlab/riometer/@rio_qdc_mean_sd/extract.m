function  [r,sr,s] = extract(mia, varargin)
%EXTRACT EXTRACT overloaded for RIO_QDC_MEAN_SD object.
%
% See rio_qdc_base/EXTRACT, mia_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

% leave all intelligence to another class
[r sr s] = rio_base_extract(mia, varargin{:});

% use the susbsref name given by extract, this allows compatibility wiht
% matlab 5.1
r.stddev = feval(s.subsref, r.stddev, sr);

