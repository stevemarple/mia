function r = setstddev(mia, d, varargin)
%SETSTDDEV  Set the stddev matrix, or part of.
%
%   r = setstddev(mia, d)
%   r = setstddev(mia, d, row, column);
%   r: modified MIA_BASE object
%   d: stddev matrix (row x col)
%   mia: MIA_BASE object
%   row: row(s)
%   col: column(s)
%
%   Set the stddev matrix, or part of it. Eaxct meaning of stddev array is
%   dependent on stddev type; e.g. rows correspond to beams (riometers) or
%   components (magnetometers), but use GETBEAMINDEX/GETCOMPINDEX to map
%   beam numbers/components into rows. Columns correspond to samples. 
%
%   If row (or col) is zero, or empty, then all rows (or columns) of the
%   stddev matrix are set. row and col may be vectors.
%
%   For multi-dimensional arrays time index should be the last one. For
%   images use (X, Y, t)
%
%   See also MIA_BASE, GETBEAMINDEX.

if matlabversioncmp('>', '5.1')
  sa = 'subsasgn';
else
  sa = 'subsasg2';
end


r = mia;
if nargin == 2
  r.stddev = d;

elseif nargin == 3 & isstruct(varargin{1})
  r.stddev = feval(sa, r.stddev, varargin{1}, d);
  
% elseif nargin == 4
%   row = varargin{1};
%   col = varargin{2};
%   if isempty(row) | row == 0
%     % do for all beams
%     row = [1:size(mia.stddev,1)];
%   else
%     row = varargin{1};
%   end
%   if isempty(col) | col == 0
%     % do for all samples
%     col = [1:size(mia.stddev,2)];
%   end
%   r.stddev(row, col) = d;
%  
% else
%   error('incorrect parameters');
else
  for n = 1:length(varargin)
    if isequal(varargin{n}, 0)
      warning(['Please use '':'' to signify all elements of matching' ...
	       ' index']);
      varargin{n} = ':';
    end
  end
  s.type = '()';
  s.subs = varargin;
  
  r.stddev = feval(sa, r.stddev, s, d);
end



