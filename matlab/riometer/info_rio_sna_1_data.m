function r = info_rio_sna_1_data
%INFO_RIO_SNA_1_DATA Return basic information about rio_sna_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_sna_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=sna;serialnumber=1

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.limits.rio_rawqdc = [0 4096];
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'sna';
r.antennaazimuth = 0;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.5;
r.azimuth = [315 324.491 336.758 351.895 8.10516 23.2416 35.5095 45 ...
    305.509 315 328.958 348.713 11.2871 31.0424 45 54.4905 293.242 ...
    301.042 315 341.654 18.346 45 58.9576 66.7584 278.105 281.287 ...
    288.346 315 45 71.654 78.7129 81.8948 261.895 258.713 251.654 ...
    225 135 108.346 101.287 98.1052 246.758 238.958 225 198.346 ...
    161.654 135 121.042 113.242 234.491 225 211.042 191.287 168.713 ...
    148.958 135 125.509 225 215.509 203.242 188.105 171.895 156.758 ...
    144.491 135 0];
r.badbeams = [];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [13.9 12.8 12 11.5 11.5 12 12.8 13.9 12.8 11.5 10.9 ...
    10.6 10.6 10.9 11.5 12.8 12 10.9 10.3 10.1 10.1 10.3 10.9 12 ...
    11.5 10.6 10.1 10.1 10.1 10.1 10.6 11.5 11.5 10.6 10.1 10.1 ...
    10.1 10.1 10.6 11.6 12 10.9 10.3 10.1 10.1 10.3 10.9 12 12.8 ...
    11.5 10.9 10.6 10.6 10.9 11.5 12.8 13.9 12.8 12 11.5 11.5 12 ...
    12.8 13.9];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = 19;
r.defaultheight = 90000;
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 13;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60];
r.latitude = -71.67;
r.location1 = 'Vesleskarvet (SANAE IV)';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = -2.85;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 19;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [2.574 1.817 1.502 1.423 1.502 1.817 2.574 ...
    1.817 1.355 1.187 1.143 1.187 1.355 1.817 1.502 1.187 1.065 ...
    1.031 1.065 1.187 1.502 1.423 1.143 1.031 1 1.031 1.143 1.423 ...
    1.502 1.187 1.065 1.031 1.065 1.187 1.502 1.817 1.355 1.187 ...
    1.143 1.187 1.355 1.817 2.574 1.817 1.502 1.423 1.502 1.817 ...
    2.574];
r.starttime = timestamp([1997 04 01 00 00 00]);
r.systemtype = 'iris';
r.url = 'http://www.puk.ac.za/physics/Physics%20Web/Research/Riometers.htm';
r.url2 = {'http://www.puk.ac.za/physics/Physics%20Web/Research/Riometers.htm'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [60.1111 51.879 45.9699 42.7182 42.7182 45.9699 51.879 ...
    60.1111 51.879 41.4349 33.7226 29.2865 29.2865 33.7226 41.4349 ...
    51.879 45.9699 33.7226 24.0375 17.7216 17.7216 24.0375 33.7226 ...
    45.9699 42.7182 29.2865 17.7216 7.78988 7.78988 17.7216 29.2865 ...
    42.7182 42.7182 29.2865 17.7216 7.78988 7.78988 17.7216 29.2865 ...
    42.7182 45.9699 33.7226 24.0375 17.7216 17.7216 24.0375 33.7226 ...
    45.9699 51.879 41.4349 33.7226 29.2865 29.2865 33.7226 41.4349 ...
    51.879 60.1111 51.879 45.9699 42.7182 42.7182 45.9699 51.879 ...
    60.1111];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'int16';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'iris';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/sna_1/original_format/%Y/%m/%d/p_%y_%j.%H';
r.defaultfilename.original_format.default.loadfunction = '';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 3600];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.default.failiffilemissing = true;
r.defaultfilename.rio_power.default.format = 'mat';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/sna_1/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power.default.loadfunction = '';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 3600];
r.defaultfilename.rio_power.default.units = 'dBm';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/sna_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [64 3600];
r.defaultfilename.rio_qdc.default.units = 'dBm';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'int16';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.default.failiffilemissing = true;
r.defaultfilename.rio_rawpower.default.format = 'mat';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/sna_1/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_rawpower.default.loadfunction = '';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 3600];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/sna_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [64 3600];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.institutions{1} = 'Potchefstroom University for Christian Higher Education';
% end of function
