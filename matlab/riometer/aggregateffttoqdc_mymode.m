function r = aggregateffttoqdc_mymode(binned, varargin)


defaults.method = 'fixedprecision';
defaults.precision = [];
defaults.ignoreuniformdistribution = 1;

defaults.windowsize = 1;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% Find the mode
wsz = defaults.windowsize;
if rem(wsz, 2) == 0
  wsz = wsz - 1;
end
wsz = max(1, wsz); % have to use at least one sample

qdcsamples = size(binned, 2);
r = repmat(nan, 1, qdcsamples);

for n = 1:qdcsamples
  if wsz == 1
    idx = n;
  else
    idx = (n-(wsz-1)./2):(n+(wsz-1)./2);
    idx(idx <= 0) = idx(idx <= 0) + qdcsamples;
    idx(idx > qdcsamples) = idx(idx > qdcsamples) - qdcsamples;
  end

  d = binned(:, idx); % select all data within windowed region
  
  switch defaults.method
   case 'closest'
    d = sort(d(:)); % sort into ascending order
    [tmp m] = min(diff(d)); % find minimum difference between neighbours
    r(n) = mean(d(m:(m+1))); % take midpoint between min distance
   
   case 'fixedprecision'
    % round to a fixed precision value and take the mode
    if isempty(defaults.precision)
      % error('defaults.precision must be specified');
      % prec = mean(diff(sort(d)))/4;
      % prec = str2num(sprintf('%.1g', mean(diff(sort(d))) ));

      % Find mean of the differences between ordered set
      prec = nonanmean(diff(sort(d)));
      % Round to nearest 1 significant figure
      % prec = round2(prec, power(10, floor(log10(abs(prec)))));
    else
      prec = defaults.precision;
    end
    [mo mon] = mode(round2(d(:), prec));
    if mon == 1 & logical(defaults.ignoreuniformdistribution)
      % If the number of samples that have the modal value is only 1 then
      % either their is only one sample (unreliable) or the distribution is
      % uniform (also unreliable). Note that we don't bother checking for
      % the case that all unique values occur > 1 times, as this is
      % extremely unlikely.
      r(n) = nan;
    else
      % Could have multiple values for the mode, take their mean and use
      % that as the QDC value
      r(n) = mean(mo);
    end
    
   case 'overlappingbins'
    % Calculate the mode by binning data values into overlapping bins (ie
    % each sample can end up in > 1 bin). To avoid having many bins, most of
    % which may be empty, round all the data values to a fixed
    % precision. The unique values then give a central value for a
    % bin. However, there may be bins, whose central value are not in the
    % unique set but should still be calculated (eg a bin in between two
    % humps in the distribution). Consider these points as valid bins. It is
    % then easy to loop through each bin and find out how many samples occur
    % within its edges.
    
    % Round to a fixed precision value, then calculate how many times
    % each unique value occurs.
    [uniq count] = uniquewithcount(round2(d(:), defaults.precision));

    bin_centers = unique(repmat(uniq(:), 1, numel(bin_adjuster)) + ...
			 repmat(bin_adjuster, numel(uniq), 1));
    
    % Now sum up the number of values in the overlapping bins. 
    overlappingcount = zeros(size(bin_centers));
    for m = 1:numel(bin_centers)
      overlappingcount(m) ...
	  = sum(count(uniq > bin_centers(m) - binwidth_2 & ...
		      uniq < bin_centers(m) + binwidth_2));
    end
    
    % Find the highest number of counts, then choose the bin(s) with that
    % number of counts. As several bins may have the same, highest count
    % take the mean of them all
    bin_idx = find(overlappingcount == max(overlappingcount));
    
    r(n) = mean(bin_centers(bin_idx));
    
   otherwise
    error(sprintf('unknown mode method (was ''%s'')', ...
		  defaults.method));
  end
end
