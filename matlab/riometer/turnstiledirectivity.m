function r = turnstiledirectivity(theta, phi)
%TURNSTILEDIRECTIVITY Calculate the directivity of a crossed-dipole antenna.
%
%   r = TURNSTILEDIRECTIVITY(theta, phi);
%   r: complex directivity
%   theta: zenith angle (radians)
%   phi:   azimuth angle (radians)
%   
%   If theta and phi are matrices then they must be the same size.
%
%   See Detrick and Rosenburg, "A phased-array radiowave imager for
%   studies of cosmic noise absorption".
%
%   See also IRISDIRECTIVITY.

pi_2 = pi / 2;

sinTheta = sin(theta);
a = sinTheta .* cos(phi);
b = sinTheta .* sin(phi);


% c = power(0.5, -0.5)
% c = 1 / power(2, -0.5)

% (a-b) and (a+b) in range -sqrt(2) -> +sqrt(2)
c = power(2, -0.5);
cosAlpha = c .* (a + b);
cosGamma = c .* (a - b);

alpha = acos(cosAlpha);
gamma = acos(cosGamma);

r = cos(pi_2 .* cosAlpha) ./ sin(alpha) ...
    + i .* (cos(pi_2 .* cosGamma) ./ sin(gamma));

    

