function r = aggregateffttoqdc_modal(binned)

% Convert binned data to single value

qdcsamples = size(binned, 2);
r = zeros(1, qdcsamples);

for n = 1:qdcsamples
  d = binned(:, n);
  d2 = d(~isnan(d));
  try
    [bw dens xmesh] = kde(d2);
    [tmp idx] = max(dens);
    r(n) = xmesh(idx);
    disp(sprintf('kde succeeded for %d points', numel(d2)));
  catch
    % assignin('base', 'd', d);
    % d(~isnan(d))
    % figure;
    % plot(d(~isnan(d)))
    % rethrow
    disp(sprintf('kde failed for %d points', numel(d2)));
    r(n) = median(d2);
  end
end

