function r = rio_ram_2
%RIO_RAM_2  RIOMETER object for Ramfjordmoen, Norway.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'ram', ...
    'serialnumber', 2, ...
    'name', 'ARIES', ...
    'facility', '', ...
    'location', location('Ramfjordmoen', 'Norway', ...
                         69.644100, 19.490500), ...
    'frequency', 3.8235e+07);

