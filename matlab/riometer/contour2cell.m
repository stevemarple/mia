function r = contour2cell(c)
%CONTOUR2CELL Convert contour matrix into more accessible form
%
%   r = CONTOUR2CELL(c)
%   r: CELL array of STRUCTs representing the contours.
%   c: contour matrix returnd from CONTOURC.
%
% r is a CELL array of STRUCTs, where each STRUCT contains the fields
%    contour: the lvel of the contour line
%    x: the X values
%    y: the Y values
%
% Note that it is possible there may be more than one contour of a given
% value.
%
% See also CONTOURC.

r = {};
pos = 1;
rSz = 0;
while pos < size(c,2)
  rSz = rSz + 1;
  r{rSz}.contour = c(1,pos);
  points = c(2,pos);
  r{rSz}.x = c(1,(pos+1):(pos+points));
  r{rSz}.y = c(2,(pos+1):(pos+points));
  pos = pos + points + 1;
end
