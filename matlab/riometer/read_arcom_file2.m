function[AllBeams, FPGAHopfTime, FPGAinfo, postinteg] = read_arcom_file2(filename, varargin)
%READ_ARCOM_FILE
%
%   [data FPGA_Hopf_time FPGA_info] = READ_ARCOM_FILE(filename)
%   data: DOUBLE matrix of the data, complex and untrimmed
%   FPGA_Hopf_time: timestamp information
%   FPGA_info: information from the FPGA
%   postinteg: post-integration details


%       Type is the riometer data type
%       type = 1 (AIRIS)
%       type = 2 (ARIES)
%       type = 3 (power)

switch nargin
 case 1
  % don't pre-allocate array sizes
  rio = [];
  calibdata = [];
  
  case 2
  rio = varargin{1};
  calibdata = [];

 case 3
  rio = varargin{1};
  calibdata = varargin{2};
  
 otherwise
  error('incorrect parameters');
end

if ~isempty(rio)
  df = info(rio, 'defaultfilename', 'original_format');
  AllBeams = zeros(df.size);
  
  FPGAHopfTime.status = repmat(nan, 1, df.size(2));
  FPGAHopfTime.weekday = FPGAHopfTime.status;
  % date is a datevec, so store as column of datevecs
  FPGAHopfTime.date = cell(1, df.size(2));

  FPGAinfo.SamplesIntegrated = repmat(nan,  1, df.size(2));
  FPGAinfo.ADCconversions = FPGAinfo.SamplesIntegrated;
  FPGAinfo.NoiseModeSet = FPGAinfo.SamplesIntegrated;
  FPGAinfo.NoiseActiveFlag = FPGAinfo.SamplesIntegrated;
  FPGAinfo.NoiseOnCnt = FPGAinfo.SamplesIntegrated;
  FPGAinfo.NoiseOffCnt = FPGAinfo.SamplesIntegrated;
  FPGAinfo.Version = FPGAinfo.SamplesIntegrated;

  postinteg.postintegrator_version = repmat(nan, 1, df.size(2));
  postinteg.starttime = repmat(nan, 1, df.size(2));
  postinteg.endtime = repmat(nan, 1, df.size(2));
  postinteg.nestinglevel = repmat(nan, 1, df.size(2));
  postinteg.secondcounter = repmat(nan, 1, df.size(2));
end

% set size of variables
% switch type 
%     case 1
%         AllBeams = repmat(complex(NaN,NaN),[64 800]);
%     case 2
%         AllBeams = repmat(complex(NaN,NaN),[1024 800]);
%     case 3
%         AllBeams = repmat(complex(NaN,NaN),[64 800]);
%     otherwise
%       disp('Invalid Type Parameter') ; 
% end 

ValidPacketTypes = {'0011' '0012' };

% Do hex conversions outside loop for speed improvement                    
Magic_Dec = hex2dec('ABDE');

for n = 1:length(ValidPacketTypes)
  ValidPacketTypes_Dec(n) = hex2dec(ValidPacketTypes{n}); 
end

%--------------------------------------------------------------------------
% Open file
fid = url_fopen(filename, 'r');

% disp(['reading ' filename]);
fprintf(['reading ' filename ' ...']);
t_idx = 0; % time index
	    
%--------------------------------------------------------------------------
% Scan for magic and packet type
while ~feof(fid)
  Magic = fread(fid, 1, 'uint16');
  PacketType = fread(fid, 1, 'uint16');    

  if feof(fid)
    break;
  end
  
  MagicFlag = (Magic == Magic_Dec);
  PacketValid = any(PacketType == ValidPacketTypes_Dec);
  
  
  if ~MagicFlag
    ; % begin scan again (do nothing here)
     error('no magic');
    
  elseif ~PacketValid
    % wrong type of packet, skip over the rest of it
    PacketSize = fread(fid, 1, 'uint32'); % total packet size              
    fseek(fid,PacketSize - 8,'cof');
    % disp('wrong type of packet');
    
  else
    % valid packet of the correct type
    % disp('valid packet');
    
    [beams_real, beams_imag, SingleFPGAHopfTime, ...
     SingleFPGAinfo, single_postinteg] = LocalExtractFPGADataPacket(fid);
    
    % copy data if:
    % calibdata is empty (copy everything)
    % calibdata = 1 and it is calibration data
    % calibdata = 0 and it is not calibration data
    % SingleFPGAinfo.NoiseActiveFlag
    if isempty(calibdata) | ... 
	  (~calibdata & SingleFPGAinfo.NoiseActiveFlag == 0) | ...
      (calibdata & SingleFPGAinfo.NoiseActiveFlag ~= 0)
      t_idx = t_idx+1;
      AllBeams(:,t_idx)       = complex(beams_real,beams_imag);
      
      % copy FPGAHopfTime
      FPGAHopfTime.status(t_idx) = SingleFPGAHopfTime.status;
      FPGAHopfTime.weekday(t_idx) = SingleFPGAHopfTime.weekday;
      FPGAHopfTime.date{t_idx} = SingleFPGAHopfTime.date;
      
      % copy FPGAinfo
      FPGAinfo.SamplesIntegrated(t_idx) = SingleFPGAinfo.SamplesIntegrated;
      FPGAinfo.ADCconversions(t_idx) = SingleFPGAinfo.ADCconversions;
      FPGAinfo.NoiseModeSet(t_idx) = SingleFPGAinfo.NoiseModeSet;
      FPGAinfo.NoiseActiveFlag(t_idx) = SingleFPGAinfo.NoiseActiveFlag;
      FPGAinfo.NoiseOnCnt(t_idx) = SingleFPGAinfo.NoiseOnCnt;
      FPGAinfo.NoiseOffCnt(t_idx) = SingleFPGAinfo.NoiseOffCnt;
      FPGAinfo.Version(t_idx) = SingleFPGAinfo.Version;
      
      % copy postintegration details
      postinteg.postintegrator_version(t_idx) ...
	  = single_postinteg.postintegrator_version;
      postinteg.starttime(t_idx) = single_postinteg.starttime;
      postinteg.endtime(t_idx) = single_postinteg.endtime;
      postinteg.nestinglevel(t_idx) = single_postinteg.nestinglevel;
      postinteg.secondcounter(t_idx) = single_postinteg.secondcounter;
    end
    
  end
end

% trim to correct size
AllBeams = AllBeams(:,1:t_idx);
FPGAHopfTime = local_trim_to_size(FPGAHopfTime, t_idx);
FPGAinfo = local_trim_to_size(FPGAinfo, t_idx);
postinteg = local_trim_to_size(postinteg, t_idx);

fclose(fid);

fprintf(' done\n');
return;
       

%--------------------------------------------------------------------------
function[beams_real, beams_imag, SingleFPGAHopfTime, SingleFPGAinfo, postinteg] = LocalExtractFPGADataPacket(fid)
% Valid packet
%   disp('Valid Packet--in sub function')
PacketSize  = fread(fid, 1, 'uint32');% Total packet Size including headers
CheckSum    = fread(fid, 1, 'uint32');  % Not used. Set to 0xDEADBEEF
endpacket = 0;

packet_bytes_read = 12;

postinteg.postintegrator_version = 0;
postinteg.starttime = nan;
postinteg.endtime = 0;
postinteg.nestinglevel = 0;
postinteg.secondcounter = nan;

% while ~endpacket
while packet_bytes_read < PacketSize
  
  Descriptor = fread(fid, 1, 'uint8');  % Descriptor val
  D_size     = fread(fid, 1, 'uint16'); % Descriptor size
  
  if D_size <= 0
    disp(sprintf('warning: illegal descriptor size (%d) at file offset %d', ...
		 D_size, ftell(fid)));
  end
  
  packet_bytes_read = packet_bytes_read + D_size;
  
  %disp(strcat('Descriptor : ',dec2hex(Descriptor),'as dec' ,num2str(Descriptor) ,'file check',num2str(feof(fid))))
  
  % disp(sprintf('read_arcom_file: descriptor=0x%x', Descriptor));
  switch Descriptor
   case (10) % 0xA     ARIES
    % disp('read_arcom_file: ARIES descriptor');
    croppedID                 = fread(fid, 1, 'uint8');  % zero byte
    beams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    beams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
    switch croppedID
     case {111 112}
      % some beams are bad
      idx = [1:107 118:137 152:168 185:199 218:230 251:261 284:292 317:324 ...
	     349:355 382:387 414:419 446:451 478:483 510:515 542:547 ...
	     574:579 606:611 638:643 670:676 701:708 733:741 764:774 ...
	     795:807 826:840 857:873 888:907 918:1024];
      beams_real(idx) = nan;
      beams_imag(idx) = nan;
    end
    
   case 11 % 0xB     AIRIS
    % disp('read_arcom_file: AIRIS descriptor');
    dummybyte                 = fread(fid, 1, 'uint8');  % zero byte
    beams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    beams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
   
   case 7 % 0x7      Power
    dummybyte                 = fread(fid, 1, 'uint8');  % zero byte
    HACKbeams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    HACKbeams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
   
   case 8 % 0x8 HOPF Time
    dummybyte  = fread(fid, 1, 'uint8');  % zero byte
    SingleFPGAHopfTime.status     = fread(fid, 1, 'uint8');
    SingleFPGAHopfTime.weekday    = fread(fid, 1, 'uint8'); 
    tenhour    = fread(fid, 1, 'uint8');
    unithour   = fread(fid, 1, 'uint8');

    tenmin     = fread(fid, 1, 'uint8');
    unitmin    = fread(fid, 1, 'uint8');
    tensec     = fread(fid, 1, 'uint8');
    unitsec    = fread(fid, 1, 'uint8');

    tenday     = fread(fid, 1, 'uint8');
    unitday    = fread(fid, 1, 'uint8');
    tenmonth   = fread(fid, 1, 'uint8');
    unitmonth  = fread(fid, 1, 'uint8');
    tenyear    = fread(fid, 1, 'uint8');
    unityear   = fread(fid, 1, 'uint8');                            
    
    sec         =      ((tensec-'0')*10)  + (unitsec -'0');
    min         =      ((tenmin-'0')*10)  + (unitmin -'0');
    hour        =      ((tenhour-'0')*10) + (unithour -'0');
    day         =      ((tenday-'0')*10)  + (unitday -'0');    
    month       =      ((tenmonth-'0')*10)+ (unitmonth -'0');
    year        = 2000+((tenyear-'0')*10) + (unityear  -'0');
    
    SingleFPGAHopfTime.date = [year month day hour min sec];                            
    dummyread                 = fread(fid, 1,'uint16');
   
   case 9 % 0x9 INFO
    % disp('read_arcom_file: Info descriptor');
    dummybyte                        = fread(fid, 1, 'uint8');  % zero byte
    SingleFPGAinfo.SamplesIntegrated = fread(fid, 1,'uint32');
    SingleFPGAinfo.ADCconversions    = fread(fid, 1,'uint32');
    % SingleFPGAinfo.NoiseStatus1     = fread(fid, 1,'uint8 => double');
    % SingleFPGAinfo.NoiseStatus2     = fread(fid, 1,'uint8 => double');
    SingleFPGAinfo.NoiseModeSet      = fread(fid, 1,'ubit1');
    dummybits                        = fread(fid, 1,'ubit11');

    % NoiseActiveFlag is active low, so invert
    % SingleFPGAinfo.NoiseActiveFlag   = fread(fid, 1,'ubit1');
    SingleFPGAinfo.NoiseActiveFlag   = ~fread(fid, 1,'ubit1');
    
    dummybits                        = fread(fid, 1,'ubit3');
    SingleFPGAinfo.NoiseOnCnt        = fread(fid, 1,'uint16');                            
    SingleFPGAinfo.NoiseOffCnt       = fread(fid, 1,'uint32');
   
   case 16 %0x10 VERSION
    SingleFPGAinfo.Version           = fread(fid, 1, 'uint8');  % zero byte
    endpacket = 1;
    if D_size > 4
      % new versions of the version descriptor contain
      % additional information
      fseek(fid, D_size - 4, 'cof');
    end
       
   case 37 % 0x25
    postinteg.postintegrator_version = fread(fid, 1, 'uint8');
    % unix time_t (inclusive): beginning of current int. cycle 
    postinteg.starttime = fread(fid, 1, 'uint32'); 

    % unix time_t (inclusive): end of current int. cycle 
    postinteg.endtime = fread(fid, 1, 'uint32'); 
    
    % number of "postintegrator" runs that this data has seen:
    % 1=postintegrated directly from 1s data, 2=postintegrated from
    % postintegrated data that was postintegrated directly from 1s data, etc
    postinteg.nestinglevel = fread(fid, 1, 'uint16');    
    
    % reserved, keep alignment to 32-bit boundaries 
    fread(fid, 1, 'uint16');
   
    % number of source 1s packets that have been assimilated into this
    % post-integrated packet  
    postinteg.secondcounter = fread(fid, 1, 'uint32');
    
   otherwise
    disp(sprintf('warning: unknown packet descriptor (was 0x%x)', Descriptor));
    % return;
    % skip D_size - 3 bytes
    fseek(fid, D_size - 3, 'cof');
  end 

  if feof(fid)
    break;
  end

end

beams_real = beams_real ./ SingleFPGAinfo.SamplesIntegrated;
beams_imag = beams_imag ./ SingleFPGAinfo.SamplesIntegrated;


function r = local_trim_to_size(s, len)
% s: struct
% len: new length
fn = fieldnames(s);
r = s;
for n = 1:length(fn)
  tmp = getfield(r, fn{n});
  tmp = tmp(1:len);
  r = setfield(r, fn{n}, tmp);
end

