function r = info_rio_lyr_1_data
%INFO_RIO_LYR_1_DATA Return basic information about rio_lyr_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_lyr_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=lyr;serialnumber=1

r.limits = [];
r.limits.rio_qdc = [0 4096];
r.limits.rio_rawpower = [0 4096];
r.pixels.aacgm.x.max = 114.5;
r.pixels.aacgm.x.min = 108.7;
r.pixels.aacgm.x.step = 0.2;
r.pixels.aacgm.y.max = 76.8;
r.pixels.aacgm.y.min = 75.6;
r.pixels.aacgm.y.step = 0.05;
r.pixels.deg.x.max = 15.4;
r.pixels.deg.x.min = 8.2;
r.pixels.deg.x.step = 0.4;
r.pixels.deg.y.max = 79.64;
r.pixels.deg.y.min = 78.2;
r.pixels.deg.y.step = 0.08;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 12;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 12;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 12;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 12;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 12000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 12000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 12000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 12000;
r.abbreviation = 'lyr';
r.antennaazimuth = 0;
r.antennaphasingx = [-0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 ...
    0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 ...
    -0.875 -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 ...
    -0.625 -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 ...
    -0.375 -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 ...
    -0.125 0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 ...
    0.125 0.375 0.625 0.875 -0.875 -0.625 -0.375 -0.125 0.125 0.375 ...
    0.625 0.875];
r.antennaphasingy = [0.875 0.875 0.875 0.875 0.875 0.875 0.875 ...
    0.875 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.625 0.375 ...
    0.375 0.375 0.375 0.375 0.375 0.375 0.375 0.125 0.125 0.125 ...
    0.125 0.125 0.125 0.125 0.125 -0.125 -0.125 -0.125 -0.125 ...
    -0.125 -0.125 -0.125 -0.125 -0.375 -0.375 -0.375 -0.375 -0.375 ...
    -0.375 -0.375 -0.375 -0.625 -0.625 -0.625 -0.625 -0.625 -0.625 ...
    -0.625 -0.625 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 -0.875 ...
    -0.875];
r.antennas = [8 8];
r.antennaspacing = 0.5;
r.azimuth = [-45 -35.1 -23.4 -8.1 8.1 23.4 35.1 45 -54.9 -45 -30.6 ...
    -11.7 11.7 30.6 45 54.9 -66.6 -59.4 -45 -18.9 18.9 45 59.4 66.6 ...
    -81.9 -78.3 -71.1 -45 45 71.1 78.3 81.9 -98.1 -101.7 -108.9 ...
    -135 135 108.9 101.7 98.1 -113.4 -120.6 -135 -161.1 161.1 135 ...
    120.6 113.4 -125.1 -135 -149.4 -168.3 168.3 149.4 135 125.1 ...
    -135 -144.9 -156.6 -171.9 171.9 156.6 144.9 135];
r.badbeams = [1 8 57 64];
r.beamplanc = 8;
r.beamplanr = 8;
r.beamwidth = [13.7 14.6 16.5 16.2 16.2 16.5 14.6 13.7 14.6 16.4 ...
    15.2 14.4 14.4 15.2 16.4 14.6 16.5 15.2 13.9 13.3 13.3 13.9 ...
    15.2 16.5 16.2 14.4 13.3 12.9 12.9 13.3 14.4 16.2 16.2 14.4 ...
    13.3 12.9 12.9 13.3 14.4 16.4 16.5 15.2 13.9 13.3 13.3 13.9 ...
    15.2 16.5 14.6 16.4 15.2 14.4 14.4 15.2 16.4 14.6 13.7 14.6 ...
    16.5 16.2 16.2 16.5 14.6 13.7];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'DMI riometer chain';
r.facility_url = '';
r.facilityid = 6;
r.groupids = [1];
r.iantennatype = 'crossed-dipole';
r.ibeams = 64;
r.id = 7;
r.ifrequency = 3.82e+07;
r.imaging = true;
r.imagingbeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ...
    20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 ...
    41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 ...
    62 63 64];
r.latitude = 78.2;
r.location1 = 'Longyearbyen';
r.location1_ascii = '';
r.location2 = 'Svalbard';
r.logo = '';
r.logurl = '';
r.longitude = 15.82;
r.modified = timestamp([2015 07 29 15 39 36.766941]);
r.name = '';
r.piid = 4;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(4 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1.92751 1.58462 1.4179 1.3453 1.3453 1.4179 ...
    1.58462 1.92751 1.58462 1.31968 1.19495 1.14158 1.14158 1.19495 ...
    1.31968 1.58462 1.4179 1.19495 1.09195 1.04833 1.04833 1.09195 ...
    1.19495 1.4179 1.3453 1.14158 1.04833 1.00905 1.00905 1.04833 ...
    1.14158 1.3453 1.3453 1.14158 1.04833 1.00905 1.00905 1.04833 ...
    1.14158 1.3453 1.4179 1.19495 1.09195 1.04833 1.04833 1.09195 ...
    1.19495 1.4179 1.58462 1.31968 1.19495 1.14158 1.14158 1.19495 ...
    1.31968 1.58462 1.92751 1.58462 1.4179 1.3453 1.3453 1.4179 ...
    1.58462 1.92751];
r.starttime = timestamp([]);
r.systemtype = 'iris';
r.url = 'http://web.dmi.dk/fsweb/solar-terrestrial/ground_ionosph_obs/';
r.url2 = {'http://web.dmi.dk/fsweb/solar-terrestrial/ground_ionosph_obs/'};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [58.5 70.2 62.1 56.7 56.7 62.1 70.2 58.5 70.2 56.7 45 ...
    38.7 38.7 45 56.7 70.2 62.1 45 31.5 22.5 22.5 31.5 45 62.1 56.7 ...
    38.7 22.5 9.9 9.9 22.5 38.7 56.7 56.7 38.7 22.5 9.9 9.9 22.5 ...
    38.7 56.7 62.1 45 31.5 22.5 22.5 31.5 45 62.1 70.2 56.7 45 38.7 ...
    38.7 45 56.7 70.2 58.5 70.2 62.1 56.7 56.7 62.1 70.2 58.5];
r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1 , 'd');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'niprriodata';
r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/original_format/%Y/%m/%y%m%d%H.LYB';
r.defaultfilename.original_format.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.original_format.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [64 86400];
r.defaultfilename.original_format.default.units = 'ADC';
r.defaultfilename.rio_power.default.archive = 'default';
r.defaultfilename.rio_power.default.dataclass = 'double';
r.defaultfilename.rio_power.default.defaultarchive = true;
r.defaultfilename.rio_power.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_power.default.failiffilemissing = false;
r.defaultfilename.rio_power.default.format = 'niprriodata';
r.defaultfilename.rio_power.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/original_format/%Y/%m/%y%m%d%H.LYB';
r.defaultfilename.rio_power.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_power.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.default.savefunction = '';
r.defaultfilename.rio_power.default.size = [64 86400];
r.defaultfilename.rio_power.default.units = 'ADC';
r.defaultfilename.rio_power.Q10s.archive = '10s';
r.defaultfilename.rio_power.Q10s.dataclass = 'double';
r.defaultfilename.rio_power.Q10s.defaultarchive = false;
r.defaultfilename.rio_power.Q10s.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.Q10s.failiffilemissing = false;
r.defaultfilename.rio_power.Q10s.format = 'dmi_10s';
r.defaultfilename.rio_power.Q10s.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/10s/%Y/IR%yD%j/IR%y%m%d.%H';
r.defaultfilename.rio_power.Q10s.loadfunction = '';
r.defaultfilename.rio_power.Q10s.resolution = timespan(00, 'h', 00, 'm', 10, 's');
r.defaultfilename.rio_power.Q10s.savefunction = '';
r.defaultfilename.rio_power.Q10s.size = [64 360];
r.defaultfilename.rio_power.Q10s.units = 'ADC';
r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [1 86164];
r.defaultfilename.rio_qdc.default.units = 'ADC';
r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [1 86164];
r.defaultfilename.rio_qdc_fft.default.units = 'ADC';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'niprriodata';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/original_format/%Y/%m/%y%m%d%H.LYB';
r.defaultfilename.rio_rawpower.default.loadfunction = 'niprriodata_to_mia';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [64 86400];
r.defaultfilename.rio_rawpower.default.units = 'ADC';
r.defaultfilename.rio_rawpower.Q10s.archive = '10s';
r.defaultfilename.rio_rawpower.Q10s.dataclass = 'double';
r.defaultfilename.rio_rawpower.Q10s.defaultarchive = false;
r.defaultfilename.rio_rawpower.Q10s.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.Q10s.failiffilemissing = false;
r.defaultfilename.rio_rawpower.Q10s.format = 'dmi_10s';
r.defaultfilename.rio_rawpower.Q10s.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/10s/%Y/IR%yD%j/IR%y%m%d.%H';
r.defaultfilename.rio_rawpower.Q10s.loadfunction = '';
r.defaultfilename.rio_rawpower.Q10s.resolution = timespan(00, 'h', 00, 'm', 10, 's');
r.defaultfilename.rio_rawpower.Q10s.savefunction = '';
r.defaultfilename.rio_rawpower.Q10s.size = [64 360];
r.defaultfilename.rio_rawpower.Q10s.units = 'ADC';
r.defaultfilename.rio_rawqdc.default.archive = 'default';
r.defaultfilename.rio_rawqdc.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc.default.format = 'mat';
r.defaultfilename.rio_rawqdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc.default.loadfunction = '';
r.defaultfilename.rio_rawqdc.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc.default.savefunction = '';
r.defaultfilename.rio_rawqdc.default.size = [64 3600];
r.defaultfilename.rio_rawqdc.default.units = 'ADC';
r.defaultfilename.rio_rawqdc_fft.default.archive = 'default';
r.defaultfilename.rio_rawqdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_rawqdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_rawqdc_fft.default.duration = timespan(28 , 'd');
r.defaultfilename.rio_rawqdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_rawqdc_fft.default.format = 'mat';
r.defaultfilename.rio_rawqdc_fft.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/lyr_1/rio_rawqdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_rawqdc_fft.default.loadfunction = '';
r.defaultfilename.rio_rawqdc_fft.default.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawqdc_fft.default.savefunction = '';
r.defaultfilename.rio_rawqdc_fft.default.size = [64 3600];
r.defaultfilename.rio_rawqdc_fft.default.units = 'ADC';
r.institutions{1} = 'Danish Meteorological Institute';
% end of function
