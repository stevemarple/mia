function r = rio_qdc_base(mia)
%RIO_QDC_BASE  Extract the parent RIO_QDC_BASE object from a RIO_RAWQDC object.
%
%   r = RIO_QDC_BASE(rio)
%   r: RIO_QDC_BASE object
%   rio: RIO_QDC object
%
%   See also RIO_QDC_BASE, RIO_QDC.

r = mia.rio_qdc_base;

