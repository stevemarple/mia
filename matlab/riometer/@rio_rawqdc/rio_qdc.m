function r = rio_qdc(mia)
%RIO_QDC  Convert a RIO_RAWQDC object to RIO_QDC object.
%
%   r = RIO_QDC(mia)
%   r: RIO_QDC object
%   mia: RIO_RAWQDC object
%
%   See also RIO_RAWQDC, RIO_QDC.

% Have to linearise

% in = getinstrument(mia);
% abbrev = getabbreviation(in);
% func = ['linearise_' abbrev];
 
% if ~exist(func)
%   func = ['linearise_' lower(getfacility(in))];
% end

% if ~exist(func)
%   error(['Cannot linearise riometer data from ' abbrev]);
% else
%   [p units] = feval(func, mia);
% end

[p units] = linearise(mia);

r = rio_qdc(rio_qdc_base(mia));
r = setdata(r, p);
r = setunits(r, units);
