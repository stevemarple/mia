function r = gettype(mia, varargin)
%GETTYPE  Return riometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: RIO_RAWQDC object
%
%   See also RIO_RAWQDC, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'raw QDC power';
  
 case 'u'
  r = 'RAW QDC POWER';
  
 case 'c'
  r = 'Raw QDC power';
  
 case 'C'
  r = 'Raw QDC Power';
  
 otherwise
  error('unknown mode');
end

return

