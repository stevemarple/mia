function r = info_rio_lyc_1_data
%INFO_RIO_LYC_1_DATA Return basic information about rio_lyc_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_lyc_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=lyc;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'lyc';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 41;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 64.61;
r.location1 = 'Lycksele';
r.location1_ascii = '';
r.location2 = 'Sweden';
r.logo = '';
r.logurl = '';
r.longitude = 18.75;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 11;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([2004 12 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.irf.se/';
r.url2 = {'http://www.irf.se/'};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 3.82e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'IRE';
% end of function
