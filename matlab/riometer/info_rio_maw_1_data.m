function r = info_rio_maw_1_data
%INFO_RIO_MAW_1_DATA Return basic information about rio_maw_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_maw_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=maw;serialnumber=1

r.limits = [];
r.limits.rio_qdc = [-118 -104];
r.limits.rio_rawpower = [0 4096];
r.pixels = [];
r.abbreviation = 'maw';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [60];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = 30;
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 54;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = -67.6;
r.location1 = 'Mawson';
r.location1_ascii = '';
r.location2 = 'Antarctica';
r.logo = '';
r.logurl = '';
r.longitude = 62.87;
r.modified = timestamp([2011 07 08 10 46 13.845872]);
r.name = '';
r.piid = 22;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1];
r.starttime = timestamp([1989 12 08 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.ips.gov.au/Category/HF%20Systems/High%20Latitude%20and%20IPY/Polar%20Cap%20Absorption/Polar%20Cap%20Absorption.php';
r.url2 = {'http://www.ips.gov.au/Category/HF%20Systems/High%20Latitude%20and%20IPY/Polar%20Cap%20Absorption/Polar%20Cap%20Absorption.php' 'http://www.ips.gov.au/World_Data_Centre/1/8/1' 'http://www.ips.gov.au/World_Data_Centre/1/8' 'http://www.ips.gov.au/World_Data_Centre/2/8/7'};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = [];
r.widebeams = [1];
r.zenith = [0];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'mat';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/maw_1/rio_abs/%Y/%m/%Y%m%d.mat';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan([], 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [8640 1];
r.defaultfilename.rio_abs.default.units = 'dB';
r.institutions{1} = 'Australian Antarctic Division';
% end of function
