function [mia, qdc, tries] = rio_abs_with_qdc(varargin)
%RIO_ABS_WITH_QDC  Load RIO_ABS data, specifying the QDCs to use.
%
%   [mia, qdc, tries] = RIO_ABS_WITH_QDC(...)
%
%   mia: RIO_ABS object for the period/instrument requested
%   qdc: RIO_QDC object used to construct the RIO_ABS object
%   tries: the number of tries made to find the RIO_QDC object used (0
%   means the supplied QDC was used)
%
%   RIO_ABS_WITH_QDC accepts the parameter name/value pairs accepted by
%   RIO_ABS, and also the following parameters. Note that 'abs' and 'qdc'
%   are required to get the proper behaviour by this function.
%
%   'abs', RIO_ABS
%   The previous RIO_ABS object returned by this function (pass an empty
%   matrix for the first function call). This information is used to
%   determine if the previous RIO_QDC can be used for the period now
%   requested.
%
%   'qdc', RIO_QDC
%   The previous RIO_QDC object returned by this function (pass an empty
%   matrix for the first function call). If possible the previous RIO_QDC
%   will be used to construct the RIO_ABS object requested (thus saving
%   time loading the files).
%
%   'maxqdctries' DOUBLE
%   An integer limiting the number of tries for loading QDCs. If
%   maxqdctries > 1 then RIO_ABS_WITH_QDC will attept to load older QDCs
%   if the standard QDCs cannot be loaded. This feature is mainly
%   intended for loading recent data for which a QDC may not be
%   present. If the value returned for tries > 1 then the processing
%   string of the data will contain preliminary, and the data should not
%   be considered final. Default value is 1. An ERROR will result if
%   maxqdctries is exceeded.
%
%
% NOTE
%
% The 'loadoptions' parameter is intercepted and the value for
% 'failiffilemissing' is always set to 1 for the RIO_QDC object, and
% defaults to 0 for the RIO_ABS object, ie, a quiet day curve is always
% required but (unless overridden) data files are never required (missing
% data is indicated by NANs).
%
% See also RIO_ABS, RIO_QDC.

defaults.instrument = [];
defaults.qdc = [];
defaults.abs = [];
defaults.starttime = [];
defaults.endtime = [];
defaults.maxqdctries = 1;
defaults.beams = [];

defaults.log = 1;
defaults.loadoptions = {};
defaults.cancelhandle = [];

[defaults unvi] = interceptprop(varargin, defaults);
if isempty(defaults.instrument)
  error('instrument not specified');
end
if isempty(defaults.starttime)
  error('starttime not specified');
end
if isempty(defaults.starttime)
  error('endtime not specified');
end


qdcLoadOptions = {defaults.loadoptions{:}, 'failiffilemissing', 1};
absLoadOptions = {'failiffilemissing', 0, defaults.loadoptions{:}};

% only try loading from power + QDC if power data is available
if ~isempty(info(defaults.instrument, 'defaultfilename', 'rio_power'))

  qdcstarttime = calcqdcstarttime(defaults.instrument, defaults.starttime, ...
				  defaults.endtime);

  if ~isempty(defaults.abs) 
    % attempt to determine if current QDC can be used again.
    if calcqdcstarttime(defaults.abs) ~= qdcstarttime
      % find and load qdc
      defaults.qdc = [];
    end
  end

  % [filename interval] = info(defaults.instrument, 'defaultfilename', ...
  % 'rio_qdc', defaults.beams);
  s = info(defaults.instrument, 'defaultfilename', 'rio_qdc');
  
  tries = 0;
  if isempty(defaults.qdc)
    t = qdcstarttime;
    for tries = 1:defaults.maxqdctries
      % does the QDC exist for this time
      missing = 0;
      for n = 1:numel(defaults.beams)
	if ~url_exist(beamstrftime(t, defaults.beams(n), s.fstr))
	  missing = 1;
	  break;
	end
      end
      if ~missing
		% load QDC, don't log since we are really creating absorption
        % data
		try
		  defaults.qdc = rio_qdc('time', t, ...
								 'beams', defaults.beams, ...
								 'instrument', defaults.instrument, ...
								 'load', 1, ...
								 'log', 0, ...
								 'loadoptions', qdcLoadOptions);
		  break;
		catch
		end
      end
      t = t - s.duration;
    end
  end

  if isempty(defaults.qdc)
    % still empty, then cannot have found one within permitted number of
    % tries
    error('Cannot load suitable QDC');
  end
  
end


% set logging to be whatever was originally requested
mia = rio_abs('starttime', defaults.starttime, ...
	      'endtime', defaults.endtime, ...
	      'beams', defaults.beams, ...
	      'instrument', defaults.instrument, ...
	      'qdc', defaults.qdc, ...
	      'load', 1, ...
	      'log', defaults.log, ...
	      'loadoptions', absLoadOptions ,...
	      varargin{unvi});

% if the incorrect QDC was used set the dataquality flag to preliminary
if tries > 1
  mia = adddataquality(mia, 'Preliminary');
end

qdc = defaults.qdc;
