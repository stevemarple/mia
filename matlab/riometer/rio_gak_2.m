function r = rio_gak_2
%RIO_GAK_2  RIOMETER object for Gakona, Alaska.

% Automatically generated by makeinstrumentdatafunctions
r = riometer('abbreviation', 'gak', ...
    'serialnumber', 2, ...
    'name', 'HAARP', ...
    'facility', '', ...
    'location', location('Gakona', 'Alaska', ...
                         62.391700, -145.147000), ...
    'frequency', 3.86e+07);

