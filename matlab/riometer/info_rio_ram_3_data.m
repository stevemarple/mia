function r = info_rio_ram_3_data
%INFO_RIO_RAM_3_DATA Return basic information about rio_ram_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_ram_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=ram;serialnumber=3

r.limits = [];
r.limits.rio_power = [nan nan];
r.limits.rio_power_calib = [nan nan];
r.limits.rio_qdc = [nan nan];
r.limits.rio_rawpower = [nan nan];
r.pixels.deg.x.max = 22.4;
r.pixels.deg.x.min = 16.6;
r.pixels.deg.x.step = 0.1;
r.pixels.deg.y.max = 70.65;
r.pixels.deg.y.min = 68.5;
r.pixels.deg.y.step = 0.05;
r.pixels.km.x.max = 120;
r.pixels.km.x.min = -120;
r.pixels.km.x.step = 6;
r.pixels.km.y.max = 120;
r.pixels.km.y.min = -120;
r.pixels.km.y.step = 6;
r.pixels.km_antenna.x.max = 120;
r.pixels.km_antenna.x.min = -120;
r.pixels.km_antenna.x.step = 6;
r.pixels.km_antenna.y.max = 120;
r.pixels.km_antenna.y.min = -120;
r.pixels.km_antenna.y.step = 6;
r.pixels.m.x.max = 120000;
r.pixels.m.x.min = -120000;
r.pixels.m.x.step = 6000;
r.pixels.m.y.max = 120000;
r.pixels.m.y.min = -120000;
r.pixels.m.y.step = 6000;
r.pixels.m_antenna.x.max = 120000;
r.pixels.m_antenna.x.min = -120000;
r.pixels.m_antenna.x.step = 6000;
r.pixels.m_antenna.y.max = 120000;
r.pixels.m_antenna.y.min = -120000;
r.pixels.m_antenna.y.step = 6000;
r.abbreviation = 'ram';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [1 1];
r.antennaspacing = 0.5;
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = 1;
r.beamplanr = 1;
r.beamwidth = [];
r.bibliography = 'art:66,art:568,art:591';
r.comment = 'Joint Lancaster University and MPAe project.';
r.credits = '';
r.datarequestid = 1;
r.defaultheight = 90000;
r.endtime = timestamp([2008 08 27 14 31 32]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [1 18];
r.iantennatype = '';
r.ibeams = [];
r.id = 99;
r.ifrequency = 3.8235e+07;
r.imaging = false;
r.imagingbeams = [];
r.latitude = 69.6441;
r.location1 = 'Ramfjordmoen';
r.location1_ascii = '';
r.location2 = 'Norway';
r.logo = 'lancs_uni_crest';
r.logurl = '';
r.longitude = 19.4905;
r.modified = timestamp([2015 07 17 13 34 34.545008]);
r.name = 'ARIES widebeam';
r.piid = 18;
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = 'http://www.dcs.lancs.ac.uk/iono/rulesoftheroad';
r.serialnumber = 3;
r.standardobliquity = [nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    1.716 1.647 1.6 1.57 1.557 1.557 1.57 1.6 1.647 1.716 nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan 1.699 1.601 1.53 1.479 1.445 1.423 1.412 1.412 1.423 ...
    1.445 1.479 1.53 1.601 1.699 nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan 1.632 1.53 1.456 1.402 ...
    1.362 1.335 1.318 1.309 1.309 1.318 1.335 1.362 1.402 1.456 ...
    1.53 1.632 nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan 1.601 1.492 1.412 1.353 1.309 1.277 1.254 1.239 1.232 ...
    1.232 1.239 1.254 1.277 1.309 1.353 1.412 1.492 1.601 nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan 1.601 1.48 1.392 ...
    1.326 1.277 1.239 1.212 1.192 1.18 1.173 1.173 1.18 1.192 1.212 ...
    1.239 1.277 1.326 1.392 1.48 1.601 nan nan nan nan nan nan nan ...
    nan nan nan nan 1.632 1.492 1.392 1.318 1.262 1.218 1.186 1.161 ...
    1.144 1.133 1.128 1.128 1.133 1.144 1.161 1.186 1.218 1.262 ...
    1.318 1.392 1.492 1.632 nan nan nan nan nan nan nan nan nan ...
    1.699 1.53 1.412 1.326 1.262 1.212 1.173 1.144 1.122 1.107 ...
    1.096 1.091 1.091 1.096 1.107 1.122 1.144 1.173 1.212 1.262 ...
    1.326 1.412 1.53 1.699 nan nan nan nan nan nan nan nan 1.601 ...
    1.456 1.353 1.277 1.218 1.173 1.139 1.112 1.092 1.077 1.068 ...
    1.063 1.063 1.068 1.077 1.092 1.112 1.139 1.173 1.218 1.277 ...
    1.353 1.456 1.601 nan nan nan nan nan nan nan 1.716 1.53 1.402 ...
    1.309 1.239 1.186 1.144 1.112 1.087 1.068 1.054 1.045 1.041 ...
    1.041 1.045 1.054 1.068 1.087 1.112 1.144 1.186 1.239 1.309 ...
    1.402 1.53 1.716 nan nan nan nan nan nan 1.647 1.479 1.362 ...
    1.277 1.212 1.161 1.122 1.092 1.068 1.05 1.037 1.029 1.025 ...
    1.025 1.029 1.037 1.05 1.068 1.092 1.122 1.161 1.212 1.277 ...
    1.362 1.479 1.647 nan nan nan nan nan nan 1.6 1.445 1.335 1.254 ...
    1.192 1.144 1.107 1.077 1.054 1.037 1.025 1.016 1.013 1.013 ...
    1.016 1.025 1.037 1.054 1.077 1.107 1.144 1.192 1.254 1.335 ...
    1.445 1.6 nan nan nan nan nan nan 1.57 1.423 1.318 1.239 1.18 ...
    1.133 1.096 1.068 1.045 1.029 1.016 1.009 1.005 1.005 1.009 ...
    1.016 1.029 1.045 1.068 1.096 1.133 1.18 1.239 1.318 1.423 1.57 ...
    nan nan nan nan nan nan 1.557 1.412 1.309 1.232 1.173 1.128 ...
    1.091 1.063 1.041 1.025 1.013 1.005 1.001 1.001 1.005 1.013 ...
    1.025 1.041 1.063 1.091 1.128 1.173 1.232 1.309 1.412 1.557 nan ...
    nan nan nan nan nan 1.557 1.412 1.309 1.232 1.173 1.128 1.091 ...
    1.063 1.041 1.025 1.013 1.005 1.001 1.001 1.005 1.013 1.025 ...
    1.041 1.063 1.091 1.128 1.173 1.232 1.309 1.412 1.557 nan nan ...
    nan nan nan nan 1.57 1.423 1.318 1.239 1.18 1.133 1.096 1.068 ...
    1.045 1.029 1.016 1.009 1.005 1.005 1.009 1.016 1.029 1.045 ...
    1.068 1.096 1.133 1.18 1.239 1.318 1.423 1.57 nan nan nan nan ...
    nan nan 1.6 1.445 1.335 1.254 1.192 1.144 1.107 1.077 1.054 ...
    1.037 1.025 1.016 1.013 1.013 1.016 1.025 1.037 1.054 1.077 ...
    1.107 1.144 1.192 1.254 1.335 1.445 1.6 nan nan nan nan nan nan ...
    1.647 1.479 1.362 1.277 1.212 1.161 1.122 1.092 1.068 1.05 ...
    1.037 1.029 1.025 1.025 1.029 1.037 1.05 1.068 1.092 1.122 ...
    1.161 1.212 1.277 1.362 1.479 1.647 nan nan nan nan nan nan ...
    1.716 1.53 1.402 1.309 1.239 1.186 1.144 1.112 1.087 1.068 ...
    1.054 1.045 1.041 1.041 1.045 1.054 1.068 1.087 1.112 1.144 ...
    1.186 1.239 1.309 1.402 1.53 1.716 nan nan nan nan nan nan nan ...
    1.601 1.456 1.353 1.277 1.218 1.173 1.139 1.112 1.092 1.077 ...
    1.068 1.063 1.063 1.068 1.077 1.092 1.112 1.139 1.173 1.218 ...
    1.277 1.353 1.456 1.601 nan nan nan nan nan nan nan nan 1.699 ...
    1.53 1.412 1.326 1.262 1.212 1.173 1.144 1.122 1.107 1.096 ...
    1.091 1.091 1.096 1.107 1.122 1.144 1.173 1.212 1.262 1.326 ...
    1.412 1.53 1.699 nan nan nan nan nan nan nan nan nan 1.632 ...
    1.492 1.392 1.318 1.262 1.218 1.186 1.161 1.144 1.133 1.128 ...
    1.128 1.133 1.144 1.161 1.186 1.218 1.262 1.318 1.392 1.492 ...
    1.632 nan nan nan nan nan nan nan nan nan nan nan 1.601 1.48 ...
    1.392 1.326 1.277 1.239 1.212 1.192 1.18 1.173 1.173 1.18 1.192 ...
    1.212 1.239 1.277 1.326 1.392 1.48 1.601 nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan 1.601 1.492 1.412 1.353 1.309 ...
    1.277 1.254 1.239 1.232 1.232 1.239 1.254 1.277 1.309 1.353 ...
    1.412 1.492 1.601 nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan 1.632 1.53 1.456 1.402 1.362 1.335 1.318 1.309 ...
    1.309 1.318 1.335 1.362 1.402 1.456 1.53 1.632 nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan 1.699 1.601 ...
    1.53 1.479 1.445 1.423 1.412 1.412 1.423 1.445 1.479 1.53 1.601 ...
    1.699 nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan 1.716 1.647 1.6 1.57 1.557 1.557 1.57 ...
    1.6 1.647 1.716 nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan ...
    nan nan nan nan nan nan nan nan nan nan nan nan nan nan nan];
r.starttime = timestamp([2006 01 13 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.dcs.lancs.ac.uk/iono/aries/';
r.url2 = {'http://www.dcs.lancs.ac.uk/iono/aries/'};
r.wantennatype = 'crossed-dipole';
r.wbeams = 64;
r.wfrequency = [];
r.widebeams = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 ...
    21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 ...
    42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 ...
    63 64];
r.zenith = [1];
r.defaultfilename.original_format.original_format.archive = 'original_format';
r.defaultfilename.original_format.original_format.dataclass = 'double';
r.defaultfilename.original_format.original_format.defaultarchive = true;
r.defaultfilename.original_format.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.original_format.original_format.failiffilemissing = false;
r.defaultfilename.original_format.original_format.format = 'arcom';
r.defaultfilename.original_format.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/original_format/%Y/%m/%d_f/aries%Y%m%d%H_000_f.arcom';
r.defaultfilename.original_format.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.original_format.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.original_format.original_format.savefunction = '';
r.defaultfilename.original_format.original_format.size = [1 3600];
r.defaultfilename.original_format.original_format.units = 'raw';
r.defaultfilename.rio_power.mat.archive = 'mat';
r.defaultfilename.rio_power.mat.dataclass = 'double';
r.defaultfilename.rio_power.mat.defaultarchive = true;
r.defaultfilename.rio_power.mat.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.mat.failiffilemissing = false;
r.defaultfilename.rio_power.mat.format = 'mat';
r.defaultfilename.rio_power.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_power/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power.mat.loadfunction = '';
r.defaultfilename.rio_power.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.mat.savefunction = '';
r.defaultfilename.rio_power.mat.size = [1 3600];
r.defaultfilename.rio_power.mat.units = 'dBm';
r.defaultfilename.rio_power.original_format.archive = 'original_format';
r.defaultfilename.rio_power.original_format.dataclass = 'double';
r.defaultfilename.rio_power.original_format.defaultarchive = false;
r.defaultfilename.rio_power.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power.original_format.failiffilemissing = false;
r.defaultfilename.rio_power.original_format.format = 'arcom';
r.defaultfilename.rio_power.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/original_format/%Y/%m/%d_f/aries%Y%m%d%H_000_f.arcom';
r.defaultfilename.rio_power.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_power.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power.original_format.savefunction = '';
r.defaultfilename.rio_power.original_format.size = [64 3600];
r.defaultfilename.rio_power.original_format.units = 'dBm';
r.defaultfilename.rio_power_calib.mat.archive = 'mat';
r.defaultfilename.rio_power_calib.mat.dataclass = 'double';
r.defaultfilename.rio_power_calib.mat.defaultarchive = true;
r.defaultfilename.rio_power_calib.mat.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power_calib.mat.failiffilemissing = false;
r.defaultfilename.rio_power_calib.mat.format = 'mat';
r.defaultfilename.rio_power_calib.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_power/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_power_calib.mat.loadfunction = '';
r.defaultfilename.rio_power_calib.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power_calib.mat.savefunction = '';
r.defaultfilename.rio_power_calib.mat.size = [1 3600];
r.defaultfilename.rio_power_calib.mat.units = 'dBm';
r.defaultfilename.rio_power_calib.original_format.archive = 'original_format';
r.defaultfilename.rio_power_calib.original_format.dataclass = 'double';
r.defaultfilename.rio_power_calib.original_format.defaultarchive = false;
r.defaultfilename.rio_power_calib.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_power_calib.original_format.failiffilemissing = false;
r.defaultfilename.rio_power_calib.original_format.format = 'arcom';
r.defaultfilename.rio_power_calib.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/original_format/%Y/%m/%d_f/aries%Y%m%d%H_000_f.arcom';
r.defaultfilename.rio_power_calib.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_power_calib.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_power_calib.original_format.savefunction = '';
r.defaultfilename.rio_power_calib.original_format.size = [64 3600];
r.defaultfilename.rio_power_calib.original_format.units = 'dBm';
r.defaultfilename.rio_qdc.mat.archive = 'mat';
r.defaultfilename.rio_qdc.mat.dataclass = 'double';
r.defaultfilename.rio_qdc.mat.defaultarchive = true;
r.defaultfilename.rio_qdc.mat.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.mat.failiffilemissing = true;
r.defaultfilename.rio_qdc.mat.format = 'mat';
r.defaultfilename.rio_qdc.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.mat.loadfunction = '';
r.defaultfilename.rio_qdc.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc.mat.savefunction = '';
r.defaultfilename.rio_qdc.mat.size = [1 3600];
r.defaultfilename.rio_qdc.mat.units = 'dBm';
r.defaultfilename.rio_qdc_fft.mat.archive = 'mat';
r.defaultfilename.rio_qdc_fft.mat.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.mat.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.mat.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc_fft.mat.failiffilemissing = true;
r.defaultfilename.rio_qdc_fft.mat.format = 'mat';
r.defaultfilename.rio_qdc_fft.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.mat.loadfunction = '';
r.defaultfilename.rio_qdc_fft.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_qdc_fft.mat.savefunction = '';
r.defaultfilename.rio_qdc_fft.mat.size = [1 3600];
r.defaultfilename.rio_qdc_fft.mat.units = 'dBm';
r.defaultfilename.rio_rawpower.mat.archive = 'mat';
r.defaultfilename.rio_rawpower.mat.dataclass = 'double';
r.defaultfilename.rio_rawpower.mat.defaultarchive = true;
r.defaultfilename.rio_rawpower.mat.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.mat.failiffilemissing = false;
r.defaultfilename.rio_rawpower.mat.format = 'mat';
r.defaultfilename.rio_rawpower.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_rawpower.mat.loadfunction = '';
r.defaultfilename.rio_rawpower.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.mat.savefunction = '';
r.defaultfilename.rio_rawpower.mat.size = [1 3600];
r.defaultfilename.rio_rawpower.mat.units = 'raw';
r.defaultfilename.rio_rawpower.original_format.archive = 'original_format';
r.defaultfilename.rio_rawpower.original_format.dataclass = 'double';
r.defaultfilename.rio_rawpower.original_format.defaultarchive = false;
r.defaultfilename.rio_rawpower.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower.original_format.failiffilemissing = false;
r.defaultfilename.rio_rawpower.original_format.format = 'arcom';
r.defaultfilename.rio_rawpower.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/original_format/%Y/%m/%d_f/aries%Y%m%d%H_000_f.arcom';
r.defaultfilename.rio_rawpower.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_rawpower.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower.original_format.savefunction = '';
r.defaultfilename.rio_rawpower.original_format.size = [64 3600];
r.defaultfilename.rio_rawpower.original_format.units = 'raw';
r.defaultfilename.rio_rawpower_calib.mat.archive = 'mat';
r.defaultfilename.rio_rawpower_calib.mat.dataclass = 'double';
r.defaultfilename.rio_rawpower_calib.mat.defaultarchive = true;
r.defaultfilename.rio_rawpower_calib.mat.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower_calib.mat.failiffilemissing = false;
r.defaultfilename.rio_rawpower_calib.mat.format = 'mat';
r.defaultfilename.rio_rawpower_calib.mat.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/rio_rawpower/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.rio_rawpower_calib.mat.loadfunction = '';
r.defaultfilename.rio_rawpower_calib.mat.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower_calib.mat.savefunction = '';
r.defaultfilename.rio_rawpower_calib.mat.size = [1 3600];
r.defaultfilename.rio_rawpower_calib.mat.units = 'raw';
r.defaultfilename.rio_rawpower_calib.original_format.archive = 'original_format';
r.defaultfilename.rio_rawpower_calib.original_format.dataclass = 'double';
r.defaultfilename.rio_rawpower_calib.original_format.defaultarchive = false;
r.defaultfilename.rio_rawpower_calib.original_format.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.rio_rawpower_calib.original_format.failiffilemissing = false;
r.defaultfilename.rio_rawpower_calib.original_format.format = 'arcom';
r.defaultfilename.rio_rawpower_calib.original_format.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/ram_3/original_format/%Y/%m/%d_f/aries%Y%m%d%H_000_f.arcom';
r.defaultfilename.rio_rawpower_calib.original_format.loadfunction = 'arcom2mia';
r.defaultfilename.rio_rawpower_calib.original_format.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.defaultfilename.rio_rawpower_calib.original_format.savefunction = '';
r.defaultfilename.rio_rawpower_calib.original_format.size = [64 ...
    3600];
r.defaultfilename.rio_rawpower_calib.original_format.units = 'raw';
r.institutions{1} = 'Lancaster University';
% end of function
