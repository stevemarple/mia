function [r,varargout] = arcom_to_structs(varargin)
%ARCOM_TO_STRUCTS Read an ARCOM file into a CELL array of STRUCTs
%
%   [r c] = ARCOM_TO_STRUCTS(...)
%   r: CELL array of STRUCTs
%   c: array of packet types (numeric IDs)
%
% The default behaviour can be modified with the following name/value
% pairs:
%
%   'filename', CHAR
%   The name of the file to read. Required unless listpackets or
%   listdescriptors is given. The name is passed to URL_FOPEN, meaning
%   that URLs are may be given in addition to name of local files.
%
%   'listpackets', LOGICAL
%   List the names of the known packet types. The return value is a STRUCT
%   whose fieldnames are the names of the packets types, and the field
%   values are their numeric IDs.
%
%   'listdescriptors', LOGICAL
%   List the names of the known descriptors. The return value is a STRUCT
%   whose fieldnames are the names of the descriptors, and the field values
%   are their numeric IDs.
%
%   'progress', LOGICAL
%   Print a progress output for reading the file. Note that printing the
%   progress indicator occupies a significant amount of time compared to
%   reading the file. May be removed from future versions of
%   ARCOM_TO_STRUCTS.
%
%   'verbosity', NUMERIC
%   An integer verbosity level. Zero produces no output, one prints the name
%   of the file being opened, two also prints the name of the packet being
%   read and 3 also prints the name of descriptor being read.
%
%   'wanteddescriptors', NUMERIC
%   A list of the wanted descriptor IDs (see the listdescriptors
%   option). Only descriptors in the list are read (which requires that
%   the parent packet must be given in wantedpackets, if set). The
%   default is an empty list, which means read all descriptors.
%
%   'wantedpackets', NUMERIC
%   A list of the wanted packet IDs (see the listdescriptors option). Only
%   packets in the list are read. The default is an empty list, which means
%   read all packets.
%
% See also ARCOM_FILE_TO_MIA.

% packet_types.ARCOMPACKET_UNDEFINED   = 0; % 0x0000
packet_types.ARCOMPACKET_TEXT        = 1; % 0x0001
packet_types.ARCOMPACKET_GPS_NMEA    = 2; % 0x0002
packet_types.ARCOMPACKET_GPS_UBX     = 3; % 0x0003

% old TIMESTAMP packet
packet_types.ARCOMPACKET_reserved1   = 4; % 0x0004 

packet_types.ARCOMPACKET_TIMESTAMP   = 5; % 0x0005
packet_types.ARCOMPACKET_BASICDATA   = 6; % 0x0006

% unprocessed data straight from the DoDMA function (old)
packet_types.ARCOMPACKET_RAWFPGADATA = 7; % 0x0007  

% packetised [but otherwise unmodified] data 
% from the DoDMA function (new), used to be called RAWFPGAPACKET. 
% Renamed 2005-12-09 MGrill
% 2006-03-06 MGrill can now also contain nBIT_CPLX_DATA descriptors instead 
%                   of 0x0a/0x0b descriptors for original ARIES/AIRIS data
packet_types.ARCOMPACKET_FPGAPACKET   = 17; % 0x0011  

% current status of shared memory interface
packet_types.ARCOMPACKET_SHMEMSTATUS  = 18; % 0x0012

% containing 1-wire temperature sensor readings
packet_types.ARCOMPACKET_TEMPDATA     = 19; % 0x0013

% heating and ventilation information
packet_types.ARCOMPACKET_HVINFO       = 20; % 0x0014

% Raw ADC samples
packet_types.ARCOMPACKET_RAWRXSAMPLES = 21; % 0x0015

% Config for 4x4 system
packet_types.ARCOMPACKET_CONFIG_NXN   = 22 ; % 0x0016

% Raw input data for 4x4 system
packet_types.ARCOMPACKET_RAW_DATA_INPUT_NXN = 23; % 0x0017

% Control packet for arcomscheduler
packet_types.ARCOMPACKET_CONTROL = 24; % 0x0018

% the highest currently defined packet type + 1
ARCOMPACKET_MAX_TYPE = 25;

% descriptor_types.DESCRIPTOR_ANY               = 0; % 0x00
descriptor_types.DESCRIPTOR_UTC_TIME          = 1; % 0x01
descriptor_types.DESCRIPTOR_RAW_DATA_16BIT    = 2; % 0x02

%   // probably not used at all in newer versions
descriptor_types.DESCRIPTOR_CMPLX_DATA_64BIT  = 3; % 0x03

descriptor_types.DESCRIPTOR_STREAM_ID         = 4; % 0x04
descriptor_types.DESCRIPTOR_GPS_TIME          = 5; % 0x05

% 64 x (64bit real + 64bit imag) values
% 2 arms of 32 values
descriptor_types.DESCRIPTOR_64_CPLX_POW       = 7; % 0x07  

% HOPF time string
descriptor_types.DESCRIPTOR_HOPF_GPS_INFO     = 8; % 0x08  

% Nr of Samples, Noise words, etc
descriptor_types.DESCRIPTOR_RX_INFO           = 9; % 0x09

% ARIES data (1024 complex 64+64bit values)
% all real parts followed by all imaginary parts
descriptor_types.DESCRIPTOR_ARIES_CPLX_DATA   = 10; % 0x0a

% 0x0a and 0x0b are identical in terms of contents,
% the only difference is the overall size. 
% ARIES = 1024 complex values, AIRIS=64 complex values.
% (all real parts followed by all imaginary parts)

% AIRIS data (64 complex 64+64bit values)
% all real parts followed by all imaginary parts
descriptor_types.DESCRIPTOR_AIRIS_CPLX_DATA   = 11; % 0x0b

% version number of bitstream
descriptor_types.DESCRIPTOR_BITSTREAM_VERSION = 16; % 0x10,  

% ASCII name of shmeminterface (goes into SHMEMSTATUS packet)
descriptor_types.DESCRIPTOR_SHMEMNAME         = 17; % 0x11

% a 1:1 copy of the shmem ctl block (goes into SHMEMSTATUS packet)
descriptor_types.DESCRIPTOR_SHMEMCTLBLOCK     = 18; %0x12

% 0x20 - 0x23 are used by the packetcropper tool to convert 0x0a descriptors
% into "cropped" versions that take up less space.

descriptor_types.DESCRIPTOR_8BIT_CPLX_DATA    = 32; % 0x20
descriptor_types.DESCRIPTOR_16BIT_CPLX_DATA   = 33; % 0x21
descriptor_types.DESCRIPTOR_24BIT_CPLX_DATA   = 34; % 0x22
descriptor_types.DESCRIPTOR_32BIT_CPLX_DATA   = 35; % 0x23

descriptor_types.DESCRIPTOR_FFT_TAPER_INFO    = 36; % 0x24

% added to data packets by the postintegrator
descriptor_types.DESCRIPTOR_POST_INT_INFO     = 37; % 0x25

% DC offsets for all receiver channels
descriptor_types.DESCRIPTOR_RX_DC_OFFSETS     = 38; % 0x26

% unix timestamp
descriptor_types.DESCRIPTOR_UNIXTIME          = 39; % 0x27

% reading from a 1-wire temperature sensor
descriptor_types.DESCRIPTOR_TEMP_READING      = 40; % 0x28

% Heating and ventilation
% basic info about current state of HV control
descriptor_types.DESCRIPTOR_HV_CONTROL_INFO   = 41; % 0x29
% more detailed info about current HV controller state
descriptor_types.DESCRIPTOR_HV_DETAILS        = 42; % 0x2a

% The ID of the instrument that produced the data (not used?)
descriptor_types.DESCRIPTOR_INSTRUMENT        = 43; % 0x2b

% Precedes each CMPLX_DATA_16BIT
descriptor_types.DESCRIPTOR_RX_SAMPLES_INFO   = 44; % 0x2c

% Raw 16 bit sample data
descriptor_types.DESCRIPTOR_CMPLX_DATA_16BIT  = 45; % 0x2d

% Descriptor for data inside ARCOMPACKET_CONFIG_NXN
descriptor_types.DESCRIPTOR_CONFIG_NXN        = 46; % 0x2e

% Descriptor for data inside ARCOMPACKET_RAW_DATA_INPUT_NXN
descriptor_types.DESCRIPTOR_RAW_DATA_INPUT_NXN = 47; % 0x2f

% Descriptor for RX power for the 4x4 system. Payload size can vary to
% accomodate 8x8 system.
descriptor_types.DESCRIPTOR_RX_POWER           = 48; % 0x30

% Descriptor for info from the 4x4 system
descriptor_types.DESCRIPTOR_RX_INFO_NXN       = 49; % 0x31

% DC offsets for all receiver channels (new version)
descriptor_types.DESCRIPTOR_RX_DC_OFFSETS_NXN = 50; % 0x32

% Control descriptor for arcomscheduler
descriptor_types.DESCRIPTOR_CONTROL           = 51; % 0x33

% Descriptor for raw receiver samples
descriptor_types.DESCRIPTOR_RAWRXSAMPLES      = 52; % 0x34,

% Largest descriptor ID + 1
descriptor_types.DESCRIPTOR_MAX_TYPE          = 53; % 0x35


defaults.listpackets = 0;
defaults.listdescriptors = 0;
defaults.filename = '';
defaults.verbosity = 1;
defaults.progress = 0;

defaults.wantedpackets = [];
defaults.wanteddescriptors = [];
[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% generate list of known packet types
knownpackets_names = fieldnames(packet_types);
knownpackets_id = zeros(1, numel(knownpackets_names));  % all known IDs
for n = 1:length(knownpackets_id)
  val = getfield(packet_types, knownpackets_names{n});
  knownpackets_id(n) = val;
  packet_type_to_name{val} = knownpackets_names{n};
end

if isempty(defaults.wantedpackets)
  % defaults.wantedpackets = knownpackets_id;
else
  defaults.wantedpackets = defaults.wantedpackets(:)';

  % check wantedpackets are known to us
  unknown_packet_types = setdiff(defaults.wantedpackets, knownpackets_id);
  if ~isempty(unknown_packet_types)
    warning(sprintf('do not know of packet type 0x%02x\n', ...
		  unknown_packet_types))
  end
end

% generate list of known descriptor types
knowndescriptors_names = fieldnames(descriptor_types);
knowndescriptors_id = zeros(1, numel(knowndescriptors_names));
for n = 1:length(knowndescriptors_id)
  val = getfield(descriptor_types, knowndescriptors_names{n});
  knowndescriptors_id(n) = val;
  descriptor_type_to_name{val} = knowndescriptors_names{n};
end

if isempty(defaults.wanteddescriptors)
  % defaults.wanteddescriptors = knowndescriptors_id;
else
  defaults.wanteddescriptors = defaults.wanteddescriptors(:)';

  % check wanteddescriptors are known to us
  unknown_descriptor_types = setdiff(defaults.wanteddescriptors, knowndescriptors_id);
  if ~isempty(unknown_descriptor_types)
    warning(sprintf('do not know of descriptor type 0x%02x\n', ...
		  unknown_descriptor_types))
  end
end


if logical(defaults.listpackets)
  r = packet_types;
  varargout{1} = packet_type_to_name;
  return
elseif logical(defaults.listdescriptors)
  r = descriptor_types;
  varargout{1} = descriptor_type_to_name;
  return
elseif isempty(defaults.filename)
  error('filename must be set');
end


% To calculate an approximate number of packets to expect assume file is one
% hour long, is at 1s resolution, and each packet type occurs every second
if isempty(defaults.wantedpackets)
  r = cell(1, 3600*numel(knownpackets_id));
else
  r = cell(1, 3600*numel(defaults.wantedpackets));
end

r_types = zeros(size(r));

fid = url_fopen(defaults.filename, 'r', 'ieee-le');
fseek(fid, 0, 'eof');
filesize = ftell(fid);
fseek(fid, 0, 'bof');

if defaults.verbosity
  disp(['reading ' defaults.filename]);
end

magic_value = hex2dec('ABDE');

packet_number = 0; % number of packets in the file (first is 1)
packet_count = 0;  % number of packets in the output (first is 1)

offset = nan;
while ~feof(fid)
  last_offset = offset;
  offset = ftell(fid);
  magic = fread(fid, 1, 'uint16');
  packet_type = fread(fid, 1, 'uint16');
  if feof(fid)
    break;
  end

  if magic ~= magic_value
    warning(sprintf(['bad magic in %s, packet #%d offset %d\n', ...
		     'last packet was offset %d'], ...
		    defaults.filename, packet_number+1, offset, last_offset));
    break
  end

  if packet_type > numel(packet_type_to_name)
    packet_name = '';
  else
    packet_name = packet_type_to_name{packet_type};
  end
  if isempty(packet_name)
    packet_name = sprintf('ARCOMPACKET_UNKNOWN_0x%04x', packet_type);
  end
  
  
  packet_number = packet_number + 1;
  packet_size = fread(fid, 1, 'uint32'); % total packet size              
  
  if ~isempty(defaults.wantedpackets) && ~any(packet_type == defaults.wantedpackets)
    % discard this packet
    fseek(fid,  packet_size - 8,'cof');
    if defaults.verbosity 
      v = find(packet_type == knownpackets_id);
      if isempty(v)
        disp(sprintf('ignoring unknown packet type (0x%02x)', ...
                     packet_type));
      elseif defaults.verbosity >= 2
        disp(sprintf('ignoring packet type %s (0x%02x)', ...
                     knownpackets_names{v}, packet_type));
      end
    end
    
  elseif packet_type == packet_types.ARCOMPACKET_RAWFPGADATA
    % ARCOMPACKET_RAWFPGADATA packet does not contain descriptors, have a
    % special case for reading this packet
    p = [];
    p.packet_type = packet_type;
    p.packet_name = packet_name;
    p.file_offset = offset;
    packet_count = packet_count + 1;
    if defaults.verbosity >= 2
      v = find(packet_type == knownpackets_id);
      disp(sprintf('reading packet #%d %s (0x%02x) size=%d offset %d', ...
                   packet_number, knownpackets_names{v}, ...
		   packet_type, packet_size, offset));
    end
    p.data = fread(fid, (packet_size-8) / 4, 'uint32');
    
    
    % copy packet
    r{packet_count} = p;
    r_types(packet_count) = packet_type;
    p = [];
    if defaults.verbosity >= 3
      disp('--------');
    end

  else
    % packet is known and wanted, read it
    p = [];
    p.packet_type = packet_type;
    p.packet_name = packet_name;
    p.file_offset = offset;
    packet_count = packet_count + 1;
    if defaults.verbosity >= 2
      v = find(packet_type == knownpackets_id);
      disp(sprintf('reading packet #%d %s (0x%02x) size=%d offset %d', ...
                   packet_number, knownpackets_names{v}, ...
		   packet_type, packet_size, offset));
    end
    
    p.checksum = fread(fid, 1, 'uint32');  % Not used. Often set to 0xDEADBEEF
    packet_bytes_read = 12;
    
    while packet_bytes_read < packet_size
      descriptor_start_offset = ftell(fid);
      descriptor_type = fread(fid, 1, 'uint8');
      descriptor_size = fread(fid, 1, 'uint16');
      packet_bytes_read = packet_bytes_read + descriptor_size;
      
      if ~isempty(defaults.wanteddescriptors) && ~any(descriptor_type == defaults.wanteddescriptors)
        fseek(fid, descriptor_size - 3, 'cof');
        if defaults.verbosity >= 2
          v = find(descriptor_type == knowndescriptors_id);
          if isempty(v)
            disp(sprintf('ignoring unknown descriptor type (0x%02x)', ...
                         descriptor_type));
          else
            disp(sprintf('ignoring descriptor type %s (0x%02x)', ...
                         knowndescriptors_names{v}, descriptor_type));
          end
        end
        
      else
        d = [];
        d.descriptor_type = descriptor_type;
	
	if descriptor_type > numel(descriptor_type_to_name)
	  descriptor_name = '';
	else
	  descriptor_name = descriptor_type_to_name{descriptor_type};
	end
	if isempty(descriptor_name)
	  descriptor_name = sprintf('DESCRIPTOR_UNKNOWN_0x%02x', ...
				    descriptor_type);
	end
	
	if defaults.verbosity >= 3
          % v = find(descriptor_type == knowndescriptors_id);
          % disp(sprintf('reading descriptor %s (0x%02x)', ...
          %              knowndescriptors_names{v}, descriptor_type));
	  disp(sprintf('reading descriptor %s (0x%02x)', ...
                        descriptor_name, descriptor_type));
        end
	
	
        switch descriptor_type
         case descriptor_types.DESCRIPTOR_UTC_TIME
          d.year = fread(fid, 1, 'uint16');
          d.month = fread(fid, 1, 'uint8');
          d.day = fread(fid, 1, 'uint8');
          d.hours = fread(fid, 1, 'uint8');
          d.minutes = fread(fid, 1, 'uint8');
          d.seconds = fread(fid, 1, 'uint8');
          
         case descriptor_types.DESCRIPTOR_CMPLX_DATA_64BIT
          reserved = fread(fid, 1, 'uint8');
          d.data = fread(fid, (descriptor_size-4)/8, 'uint64');
          
         case descriptor_types.DESCRIPTOR_STREAM_ID
          d.streamid = fread(fid, 1, 'uint16');
	  
         case descriptor_types.DESCRIPTOR_GPS_TIME
          d.secondssinceepoch = fread(fid, 1, 'uint64');
          
         case descriptor_types.DESCRIPTOR_64_CPLX_POW
          reserved = fread(fid, 1, 'uint8');  
          sz = (descriptor_size-4)/(8*4);
          % d.data =  fread(fid, sz, 'int64');
	  d.x.real =  fread(fid, sz, 'int64');
	  d.x.imag =  fread(fid, sz, 'int64');
	  d.y.real =  fread(fid, sz, 'int64');
	  d.y.imag =  fread(fid, sz, 'int64');
          
         case descriptor_types.DESCRIPTOR_HOPF_GPS_INFO
          reserved1 = char(fread(fid, 1, 'uint8'));
          d.dow = char(fread(fid, 1, 'uint8'));
          d.status = char(fread(fid, 1, 'uint8'));
	  
	  hours = char(fread(fid, 2, 'char')');
	  mins = char(fread(fid, 2, 'char')');
	  secs = char(fread(fid, 2, 'char')');
	  days = char(fread(fid, 2, 'char')');
	  months = char(fread(fid, 2, 'char')');
	  years = char(fread(fid, 2, 'char')');	  
	  
	  % Combine YMDhms into a timestamp field. If any value not valid
          % ensure entire timestamp is NAN
	  tmp = double([years months days hours mins secs]');
	  if any(tmp < '0') | any(tmp > '9')
	    d.timestamp = [nan nan nan nan nan nan];
	    d 
	    tmp

	  else
	    tmp = tmp - '0';
	    d.timestamp = [2000 + [10 1] * tmp(1:2), ...
			   [10 1] * tmp(3:4), ...
			   [10 1] * tmp(5:6), ...
			   [10 1] * tmp(7:8), ...
			   [10 1] * tmp(9:10), ...
			   [10 1] * tmp(11:12)];
	  end
	  
          reserved2 = fread(fid, 1, 'uint8');
          reserved3 = fread(fid, 1, 'uint8');
         
         case descriptor_types.DESCRIPTOR_RX_INFO
          reserved1 = fread(fid, 1, 'uint8');
          d.nr_of_samples = fread(fid, 1, 'uint32');
          d.nr_of_adc_in_cycle = fread(fid, 1, 'uint32');
	  
	  % bit 12: 1=noise source off 0=noise source on

	  % bit 0:  noise mode: 0=always signal 1=alternate signal/noise
          % according to noise_word
	  % d.noise_status = fread(fid, 1, 'uint16');
	  
	  d.noise_mode_set = fread(fid, 1,'ubit1');
	  dummybits = fread(fid, 1,'ubit11');
	  
	  % NoiseActiveFlag is active low, so invert
	  d.noise_active_flag   = ~fread(fid, 1,'ubit1');
	  
	  dummybits = fread(fid, 1,'ubit3');
    
          d.noise_on_cnt = fread(fid, 1, 'uint16');
          d.noise_off_cnt = fread(fid, 1, 'uint32');
          
         case descriptor_types.DESCRIPTOR_ARIES_CPLX_DATA 
          reserved = fread(fid, 1, 'uint8');  
          sz = (descriptor_size-4)/16;
          d.real =  fread(fid, sz, 'int64');
          d.imag =  fread(fid, sz, 'int64');
                    
	 case descriptor_types.DESCRIPTOR_AIRIS_CPLX_DATA
          reserved = fread(fid, 1, 'uint8');  
          sz = (descriptor_size-4)/16;
          d.real =  fread(fid, sz, 'int64');
          d.imag =  fread(fid, sz, 'int64');
	 
	 % To go here
	 
	 case descriptor_types.DESCRIPTOR_BITSTREAM_VERSION
          d.version = fread(fid, 1, 'uint8');
	  if descriptor_size > 4
	    subdev_ver = fread(fid, 1, 'uint8');
	    dev_ver = fread(fid, 1, 'uint8');
	    minor_ver = fread(fid, 1, 'uint8');
	    main_ver = fread(fid, 1, 'uint8');
	  end
	  
         case descriptor_types.DESCRIPTOR_FFT_TAPER_INFO
	  reserved1 = fread(fid, 1, 'uint8');
	  % % bit24: overflow flag, bits 5...0: scale_sch
	  % d.fft_config = fread(fid, 1, 'uint32'); 
	  fft_config = fread(fid, 1, 'uint32');
	  d.overflow = bitget(fft_config, 25);
	  d.scaling_a = bitshift(bitand(48, fft_config), -4);
	  d.scaling_b = bitshift(bitand(12, fft_config), -2);
	  d.scaling_c = bitand(3, fft_config);
	  
	  % user-defined ID identifying this set of tapering coefficients
	  d.taper_id = fread(fid, 1, 'uint16');
	  reserved2 = fread(fid, 1, 'uint8');
	  
	  % bit0: pow_select
	  d.pow_select = fread(fid, 1, 'uint8');      
	  
         case descriptor_types.DESCRIPTOR_POST_INT_INFO
          d.postintegrator_version = fread(fid, 1, 'uint8');
          d.starttime = fread(fid, 1, 'uint32');
          d.endtime = fread(fid, 1, 'uint32');
          
          if d.postintegrator_version >= 1
            d.nestinglevel = fread(fid, 1, 'uint16');
            reserved = fread(fid, 1, 'uint16');
            d.secondcounter = fread(fid, 1, 'uint32');
          else
            d.nestinglevel = 0;
            d.secondcounter = [];
          end
          
	 case descriptor_types.DESCRIPTOR_RX_DC_OFFSETS
	  reserved = fread(fid, 1, 'uint8');  
	  sz = (descriptor_size - 4) / (4*4);
	  % d.offsets = fread(fid, sz, 'int32');
	  
	  % shift right 16 bits (bottom half is the fractional part)
	  d.offsets.x.real = fread(fid, sz, 'int32') / 65536;
	  d.offsets.x.imag = fread(fid, sz, 'int32') / 65536;
	  d.offsets.y.real = fread(fid, sz, 'int32') / 65536;
	  d.offsets.y.imag = fread(fid, sz, 'int32') / 65536;

	 case descriptor_types.DESCRIPTOR_UNIXTIME
	  reserved = fread(fid, 1, 'uint8'); 
	  d.unixtime = fread(fid, 1, 'uint32'); 
	  
	 case descriptor_types.DESCRIPTOR_TEMP_READING
	  d.sensorid = fread(fid, 1, 'uint8'); 
	  d.reading = fread(fid, 1, 'float32'); 
	  
	 case descriptor_types.DESCRIPTOR_HV_CONTROL_INFO
	  iobitmask = fread(fid, 1, 'uint8');
	  d.fan = logical(bitget(iobitmask, 1));
	  d.heater = logical(bitget(iobitmask, 2));
	  
	 case descriptor_types.DESCRIPTOR_HV_DETAILS
	  reserved = fread(fid, 1, 'uint8');
	  
	  % normalised process input value
	  d.process_input_n = fread(fid, 1, 'float32');

	  % normalised controller output value
	  d.controller_output_n = fread(fid, 1, 'float32');
	  
	  % resulting current heater duty cycle (0.0--1.0)
	  d.heater_duty_cycle = fread(fid, 1, 'float32');

	  % resulting current fan duty cycle (0.0--1.0)
	  d.fan_duty_cycle = fread(fid, 1, 'float32');
	  
	  % sub-second fraction of system time when this state became valid
	  d.tv_usec = fread(fid, 1, 'uint32');
	  
	  if descriptor_size >= 28
	    d.setpoint = fread(fid, 1, 'float32');
	  end
	 
	 case descriptor_types.DESCRIPTOR_RX_POWER  
	  reserved = fread(fid, 1, 'uint8');
	  len = (descriptor_size - 4) / (8 * 2);
	  d.real = fread(fid, len, 'int64');
	  d.imag = fread(fid, len, 'int64');
	  
	  
	 case descriptor_types.DESCRIPTOR_RX_INFO_NXN
	  reserved = fread(fid, 1, 'uint8');
	  d.taper_id = fread(fid, 1, 'uint32');
	  d.gain_id = fread(fid, 1, 'uint32');
	  d.nr_of_samples = fread(fid, 1, 'uint32');

	  % d.instrument_id = fread(fid, 1, 'uint32');
	  d.instrument.beamforming_type = fread(fid, 1, 'uint8');
	  d.instrument.size = fread(fid, 1, 'uint8');
	  d.instrument.serial_no = fread(fid, 1, 'uint16');
	  
	  d.power_stream_select = fread(fid, 1, 'ubit2');
	  fread(fid, 1, 'ubit14'); % discard 14 bits
	  
	  % bypass flags
	  d.bypass.offset = fread(fid, 1, 'ubit1');
	  d.bypass.gain = fread(fid, 1, 'ubit1');
	  d.bypass.tapering = fread(fid, 1, 'ubit1');
	  fread(fid, 1, 'ubit13'); % discard 13 bits
	  
	  % raw data
	  d.raw_data.channel_1 = fread(fid, 1, 'ubit4');
	  fread(fid, 1, 'ubit4'); % discard 4 bits
	  d.raw_data.channel_2 = fread(fid, 1, 'ubit4');
	  fread(fid, 1, 'ubit4'); % discard 4 bits
	  
	  d.raw_data.stream_select = fread(fid, 1, 'ubit2');
	  fread(fid, 1, 'ubit6'); % discard 6 bits

	  d.raw_data.src = fread(fid, 1, 'ubit1');
	  d.raw_data.enable = fread(fid, 1, 'ubit1');
	  fread(fid, 1, 'ubit6'); % discard 6 bits
	  
	  d.nr_raw_data_samples = fread(fid, 1, 'uint32');
	  d.noise_on_cnt = fread(fid, 1, 'uint32');
	  d.noise_off_cnt = fread(fid, 1, 'uint32');
	  
	  % Gain and noise control
	  d.noise_src = fread(fid, 1, 'ubit2'); % config value
	  d.rf_switch = fread(fid, 1, 'ubit2'); % config value
	  d.counter_start_state = fread(fid, 1, 'ubit1');
	  d.noise_level = fread(fid, 1, 'ubit1');
	  fread(fid, 1, 'ubit2');
	  d.rx_gain = fread(fid, 1, 'ubit3');
	  fread(fid, 1, 'ubit5');
	  
	  % calib noise src, 0=off, 1=on
	  d.calib_on = fread(fid, 1, 'ubit1');  % current status

	  % 0=RX to antenna, 1=RX to calib noise src
	  d.calib_switch = fread(fid, 1, 'ubit1'); % current status
	  fread(fid, 1, 'ubit14');
	  
	  % FFT scaling
	  d.fft_scaling_stage_1 = fread(fid, 1, 'uint16');
	  d.fft_scaling_stage_2 = fread(fid, 1, 'uint16');
	  d.nr_of_adc_in_cycle = fread(fid, 1, 'uint32');
	  	  
	 case descriptor_types.DESCRIPTOR_RX_DC_OFFSETS_NXN
	  reserved = fread(fid, 1, 'uint8');  
	  sz = [2 (descriptor_size - 4) / 4];
	  tmp = fread(fid, sz, 'int16');
	  d.offsets.real = tmp(1, :);
	  d.offsets.imag = tmp(2, :);
	  
	 case descriptor_types.DESCRIPTOR_RAWRXSAMPLES
	  reserved = fread(fid, 1, 'uint8');  
	  sz = [2 (descriptor_size - 4) / 4];
	  % first row is first channel, second row is 2nd channel
	  d.rawdata = fread(fid, sz, 'int16');
	  
         otherwise
	  if defaults.verbosity
	    fprintf(['no code to read descriptor %s ' ...
		     '(type=0x%02x) in packet %s\n'], ...
		    descriptor_name, descriptor_type, packet_name);
	  end
          % fseek(fid, descriptor_size - 3, 'cof');
	  d.unknown_payload = fread(fid, descriptor_size - 3, 'uint8');
        end

	if feof(fid)
	  warning('end of file reached');
	  % ignore last packet, it is incomplete
	  p = [];
	  packet_count = packet_count - 1;
	  packet_number = packet_number - 1;
	  break;
	end

	% check the descriptor has been read correctly
	if ftell(fid) - descriptor_start_offset ~= descriptor_size
	  v = find(descriptor_type == knowndescriptors_id);
	  ft = ftell(fid);
	  fclose(fid);
	  error(sprintf(['failed to read descriptor %s (0x%02x): ' ...
			 'size is %d but read %d bytes, ' ...
			 'descriptor start offset is %d'], ...
			knowndescriptors_names{v}, descriptor_type, ...
			descriptor_size, ...
			ft - descriptor_start_offset, ...
			descriptor_start_offset));
	end
	
		
	if isfield(p, descriptor_name)
	  % packet already had a descriptor with this name, so append
	  % to existing field
	  tmp = getfield(p, descriptor_name);
	  tmp(end+1) = d;
	  p = setfield(p, descriptor_name, tmp);
	else
	  % new descriptor for this packet, create a new field
	  p = setfield(p, descriptor_name, d);
	end

      end % end of descriptor reading
      
      
      
      d = [];
      
    end % end of reading current packet

    switch defaults.progress
     case 0
      ;
     case 1
      disp(sprintf('progress: %02.0f%%', 100 * ftell(fid) / filesize));
     otherwise
      fprintf('\rprogress: %02.0f%%', 100 * ftell(fid) / filesize);
    end

    if ~isempty(p)
      % copy packet
      r{packet_count} = p;
      r_types(packet_count) = packet_type;
      p = [];
    end

    if defaults.verbosity >= 3
      disp('--------');
    end

  end % end of reading packets
  
end

fclose(fid);

% trim extra elements remaining from the pre-allocation
r((packet_count+1):end) = []; 

if nargout > 1
  r_types((packet_count+1):end) = []; 
  varargout{1} = r_types;
end
