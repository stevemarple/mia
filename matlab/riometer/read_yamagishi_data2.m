function mia = read_yamagishi_data2(rio, varargin)

% interval to read data
defaults.starttime = [];
defaults.endtime = [];

defaults.beams = [];

% temporal resolution of the input file(s)
defaults.inputresolution = [];

% write data out at this resolution. If not set then it is assumed that
% the blocks are on boundaries of inputresolution, a warning will be
% produced if that is not the case
defaults.outputresolution = [];

% the position of each beam in the file. The data is sorted so that data
% is in order of ascending beam number
defaults.beamorder = [];

% a strftime format string to indicate how to load the data
defaults.filename = '';


[defaults unvi] = interceptprop(varargin, defaults);

isofstr = '%Y-%m-%d %H:%M:%S';

if isempty(defaults.starttime)
  error('starttime must be specified');
elseif isempty(defaults.endtime)
  error('endtime must be specified');
elseif isempty(defaults.inputresolution)
  error('inputresolution must be specified');
elseif isempty(defaults.beamorder)
  error('beamorder must be specified');
elseif ~isempty(defaults.outputresolution)
  if defaults.outputresolution > defaults.inputresolution
    error('output resolution cannot be lower than input resolution');
  end
  resFactor = defaults.inputresolution / defaults.outputresolution;
  if resFactor ~= floor(resFactor)
    error('input resolution must be a multiple of the output resolution');
  end
end

totalSamples = (defaults.endtime - defaults.starttime) / ...
    defaults.inputresolution;

[ib wb] = info(rio, 'beams');
allBeams = sort([ib wb]);
if isempty(defaults.beams)
  defaults.beams = allBeams;
end
defaults.beams = reshape(defaults.beams, [1 prod(size(defaults.beams))]);

% The data is organised as files of one day duration, but if errors occur
% then files for hours other than 0 may be present.

day = timespan(1, 'd');
% hour = timespan(1, 'h');

t0 = floor(defaults.starttime, day);
et = ceil(defaults.endtime, day);

res = defaults.outputresolution;
if isempty(res)
  res = defaults.inputresolution;
end

mia = rio_abs('starttime', defaults.starttime, ...
	      'endtime', defaults.endtime, ...
	      'resolution', res, ...
	      'beams', defaults.beams, ...
	      'instrument', rio, ...
	      'data', repmat(nan, length(defaults.beams), totalSamples), ...
	      'load', 0, ...
	      'log', 1);

obl = '';
oblFac = [];
mia = setobliquityfield(mia, obl, oblFac);

samplesPerDay = day / res;

while t0 < et
  tNextDay = t0 + day;
  miaDay = rio_abs('starttime', t0, ...
		   'endtime', tNextDay, ...
		   'resolution', res, ...
		   'beams', defaults.beams, ...
		   'instrument', rio, ...
		   'data', repmat(nan, length(defaults.beams), ...
				  samplesPerDay), ...
		   'load', 0, ...
		   'log', 0);
  miaDay = setobliquityfield(miaDay, obl, oblFac);

  for h = 0:23
    t = t0 + timespan(h, 'h');
    fname = strftime(t, defaults.filename)
    if ~isempty(dir(fname))
      % read the file
      
      [fid mesg] = fopen(fname, 'r', 'ieee-le'); % PC format
      if fid == -1
	error(sprintf('could not open %s: %s', fname, mesg));
      end
      % read records
      while ~feof(fid)
	[rt rd] = localReadRecord(fid, defaults);
	rt
	if isempty(rt)
	  break;
	end
	
	if isempty(defaults.outputresolution)
	  % check record starts on a boundary value
	  if floor(rt, defaults.inputresolution) ~= rt
	    warning(sprintf(['record for %s does not start on a %s ' ...
			   'boundary, moving to nearest boundary'], ...
			    strftime(rt, isofstr), ...
			  char(defaults.inputresolution)));
	    rt = round(rt, defaults.inputresolution);
	  end
	  
	elseif defaults.outputresolution ~= defaults.inputresolution
	  % increase number of samples (never decrease)
	  data = zeros(size(rd) .* [1 resFactor]);
	  for n = 0:(size(rd, 2)-1)
	    data(:, n*resFactor + [1:resFactor]) = rd(:, n+1);
	  end
	  rd = data;
	end
      
	etTmp = rt + res * size(rd, 2);
	miaTmp = rio_abs('starttime', rt, ...
			 'endtime', etTmp, ...
			 'resolution', res, ...
			 'beams', allBeams, ...
			 'instrument', rio, ...
			 'data', rd(defaults.beamorder(defaults.beams), :), ...
			 'load', 0, ...
			 'log', 0);
	miaTmp = setobliquityfield(miaTmp, obl, oblFac);
      
	if isempty(miaDay)
	  'INS 1'
	  miaDay = miaTmp;
	else
	  'INSERT miaDay miaTmp'
	  miaDay, miaTmp
	  miaDay = insert(miaDay, miaTmp)
	end
      end

      if etTmp >= t0 + day
	break; % don't need to look for any more files from this day
      end
    end
  end
  
  if isempty(miaDay)
    ; % do nothing
  elseif isempty(mia)
    'INS 2'
    mia = miaDay;
  else
    'INSERT mia miaDay'
    mia, miaDay
    mia = insert(mia, miaDay);
  end
  
  t0 = tNextDay;
end


% ----------------------------------
function [rt, rd] = localReadRecord(fid, defaults)

rt = [];
rd = [];

% read the time
sz = [1 6];
[dv c] = fread(fid, sz, 'uint8');
if c ~= prod(sz)
  return;
end


if dv(1) < 80
  dv(1) = dv(1) + 2000;
else
  dv(1) = dv(1) + 1900;
end
rt = timestamp(dv);

% skip the next 10 bytes
fseek(fid, 10, 0);

% read 8 frames
sz = [prod(size(defaults.beamorder)) 8];
[rd c] = fread(fid, sz, 'uint16');
if c ~= prod(sz)
  rt = [];
  rd = [];
  return;
end



