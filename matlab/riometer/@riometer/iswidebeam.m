function r = iswidebeam(rio)
%ISWIDEBEAM  Indicate if RIOMETER is a widebeam riometer.
%
%   r = ISWIDEBEAM(rio)
%   r: LOGICAL
%   rio: RIOMETER instrument(s)
%
% ISWIDEBEAM indicates if the riometer objects are imaging riometers. r is
% the same size as rio. Note that riometers can contain both widebeam and
% imaging arrays, thus ISWIDEBEAM and ISIMAGING are not the inverse of
% each other.
%
% See also RIOMETER, ISIMAGING.

num = numel(rio);
r = logical(size(rio));
for n = 1:num
  r(n) = ~isempty(info(rio(n), 'widebeams'));
  % s = info(rio(n), '_struct'); r(n) = ~isempty(s.wfrequency);
end
