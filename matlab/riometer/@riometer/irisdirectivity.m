function r = irisdirectivity(rio, theta, phi, beam, varargin)
%IRISDIRECTIVITY Calculate the directivity of the IRIS antenna.
%
%   r = IRISDIRECTIVITY(rio, theta, phi, beam, ...);
%   r: complex directivity
%   rio: RIOEMTER object
%   theta: zenith angle (radians)
%   phi:   azimuth angle (radians)
%   beam: single beam
%   
%   If theta and phi are matrices then they must be the same size. NB the
%   azimuth angle is measured in the trigonometric manner, not like
%   compass angles!
%
% The following parameter name/value pairs can be used to override the
% values found from the RIOMETER INFO.
%
%   'antennas', [1x2 DOUBLE]
%   Override the normal number of antennas for this riometer.
%
%   'antennaspacing', DOUBLE
%   Override the normal antenna spacing for this riometer.
%
%   'xphasing', DOUBLE
%   'yphasing, DOUBLE
%    Set the antenna phasing information.
%
%   See D. L. Detrick, T. J. Rosenburg. "A Phased-Array Radiowave Imager for
%   Studies of Cosmic Noise Absorption". Radio Science.  ISSN
%   0048-6604. 25(4) pp. 325-338. January 1990.
%
%   See also TURNSTILEDIRECTIVITY.

theta(theta == 0) = eps;
phi(phi == 0) = eps;

if length(beam) > 1
  r = cell(size(beam));
  [tmp mfname] = fileparts(mfilename);
  for n = 1:prod(size(beam))
    r{n} = feval(mfname, rio, theta, phi, beam(n), varargin{:});
  end
  return
end

d2r = pi / 180; % factor to multiply by to convert from degrees to radians

defaults.xphasing = []; % defer
defaults.yphasing = []; % defer
defaults.dipoletheta = [0.001:1:90] * d2r;
defaults.dipolephi = [0.001:1:360 0.001] * d2r;

defaults.antennaspacing = info(rio, 'antennaspacing');
defaults.antennas = info(rio, 'antennas');

[defaults unvi] = interceptprop(varargin, defaults);

[imagingBeams wideBeams] = info(rio, 'beams');

Nx = defaults.antennas(1);
Ny = defaults.antennas(2);


if 0
  % METHOD USED BY H. TAO (WRONG)
  % model by rotating dipole
  
  % Dipole is circularly-phased so integrate over all phi

  % create the theta and phi matrices ourselves. When performing the sum we
  % need to know along which dimension phi varies, since we have to
  % integrate over that dimension of the results matrix
  [uniqueTheta uInd1 uInd2] = unique(theta);
  [dipoleTheta dipolePhi] = meshgrid(uniqueTheta, defaults.dipolephi);
  % dipolePhi varies in 'y' direction

  % %%%%%%%%%%%
  if 0
    thetaSz = size(theta)
    defDipolePhiSz = size(defaults.dipolephi)
    uniqueThetaSx = size(uniqueTheta)
    dipoleThetaSz = size(dipoleTheta)
    dipolePhiSz = size(dipolePhi)
    ddsz = size(dipoledirectivity(dipoleTheta, dipolePhi))
    coefsz = size(sin(0.5 .* pi .* cos(dipoleTheta)))
  end
  % %%%%%%%%%%%
  dd = dipoledirectivity(dipoleTheta, dipolePhi) .* ...
      sin(0.5 .* pi .* cos(dipoleTheta));
  % each column of dd is at constant theta
  d0prelim = sum(dd, 1) ./ max(dd, [], 1);

  % create the directivity for all directions. Can ignore phi since we have
  % integrated over all phi.
  d0 = reshape(d0prelim(uInd2), size(theta));
  % %%% ddSz = size(dd)
  % %%% d0Sz = size(d0)
end

% model by turnstile antenna
d0 = turnstiledirectivity(theta, phi);

% if beam == (1 + length(defaults.xphasing) * length(defaults.yphasing))
if ismember(beam, wideBeams)
  % widebeam antenna
  % r = turnstiledirectivity(theta, phi);
  r = d0;
  return;
end

% suppress divide by zero warnings, but remember current warning state
warnstate = warning;
warning off MATLAB:divideByZero; 

[xphasing yphasing] = info(rio, 'antennaphasing', 'beams', beam);
if isempty(defaults.xphasing)
  defaults.xphasing = xphasing;
end
if isempty(defaults.yphasing)
  defaults.yphasing = yphasing;
end



sinTheta = sin(theta);


betaX = defaults.xphasing;
betaY = defaults.yphasing;

if 0
  % original equations by Detrick and Rosenburg
  psiX = pi * (sinTheta .* cos(phi) - betaX);
  psiY = pi * (sinTheta .* sin(phi) - betaY);
elseif 1
  % adaptions by SRM for antenna spacings different to 0.5 lambda
  psiX = pi * (2 * defaults.antennaspacing * sinTheta .* cos(phi) - betaX);
  psiY = pi * (2 * defaults.antennaspacing * sinTheta .* sin(phi) - betaY);
end

% get rid of divide by zero warnings
psiX(psiX == 0) = eps;

r = d0 .* ((1 - exp(i .* Nx .* psiX)) ./ (1 - exp(i .* psiX))) ...
    .*    ((1 - exp(i .* Ny .* psiY)) ./ (1 - exp(i .* psiY))) .* ...
    (1 - exp(i .* pi .* cos(theta)));

% restore previous warning state
warning(warnstate);
