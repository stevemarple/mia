function mia = niprqdc_to_mia(rio, filename)
%NIPRQDC_TO_MIA Convert NIPR format to mia QDC
%
%   mia = niprqdc_to_mia(rio, filename)
%

% Load as power data
p = niprriodata_file_to_mia(filename, ...
			    'mia', rio_rawpower('instrument', rio, ...
						'load', 0));
p = setdata(p, double(getdata(p)));


bestres = info(rio, 'bestresolution');



% Convert sample times to local mean sidereal time
samt = getsampletime(p);

lmst = ut2lmst(getsampletime(p), getgeolong(getlocation(rio)));

% Wrap any negative values into correct range
idx = find(lmst < timespan(0, 's'));
while ~isempty(idx)
  lmst(idx) = lmst(idx) - siderealday;
  idx = find(lmst < timespan(0, 's'));
end

% Wrap any larger values into correct range
idx = find(lmst > siderealday);
while ~isempty(idx)
  lmst(idx) = lmst(idx) - siderealday;
  idx = find(lmst > siderealday);
end

data = getdata(p);

% sort according to sidereal time
[samt2 idx] = sort(lmst);
data2 = data(:, idx);


mia = rio_rawqdc('instrument', rio, ...
		 'starttime', getstarttime(p), ...
		 'endtime', getendtime(p), ...
		 'sampletime', samt2, ...
		 'integrationtime', repmat(bestres, size(samt2)), ...
		 'data', data2, ...
		 'units', getunits(p), ...
		 'beams', getbeams(p));

mia = addprocessing(mia, sprintf('created from %s', filename));

df = info(rio, 'defaultfilename', class(mia));

qdcst = calcqdcstarttime(mia);
qdcet = qdcst + df.duration;
mia = setqdcperiodstarttime(mia, qdcst);
mia = setqdcperiodendtime(mia, qdcet);


mia = spaceregularly(mia, ...
		     'markmissingdata', false, ...
		     'method', 'cubic', ...
		     'resolution', bestres);

