function varargout = instrumenttypeinfo(in, info, varargin)
%INSTRUMENTTYPEINFO  Return information about the instrument type.
%
%   r = INSTRUMENTTYPEINFO(rio, 'supported')
%   r = INSTRUMENTTYPEINFO(rio, 'supported', 'rio_abs')
%   r = INSTRUMENTTYPEINFO(rio, 'supported', 'rio_power')
%   r = INSTRUMENTTYPEINFO(rio, 'supported', 'rio_rawpower')
%   r = INSTRUMENTTYPEINFO(rio, 'supported', 'rio_qdcpower')
%
%   rio: any riometer instrument
%   Return a list of supported riometers. If the optional data type is
%   supplied the list is restricted to supported riometers for that
%   datatype. Support for rio_image depends on the image type
%   (absorption, power etc).
%
%   See also RIOMETER, riometer/INFO.

switch char(info)
 case 'abbreviation'
  varargout{1} = 'rio';
  
 case 'aware'
  % indicate which instruments MIA is aware of, even if they are not
  % supported by MIA (useful for plotting)
  s = rio_instrumenttypeinfo_data;
  varargout{1} = s.aware;
  
 case 'imageclass'
  varargout{1} = 'rio_image';
  
 case 'supported'
  % mfilename(in, 'supported')
  % mfilename(in, 'supported', 'datatype', rio_abs)
  % mfilename(in, 'supported', 'datatype', rio_power)
  % mfilename(in, 'supported', 'datatype', rio_rawpower)
  % mfilename(in, 'supported', 'datatype', rio_qdcpower)
  
  % list supported instruments. Optionally take the 'datatype' for which
  % support is required
  
  defaults.datatype = 'any';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  s = rio_instrumenttypeinfo_data;
  if ~isfield(s.supported, defaults.datatype)
    error(sprintf('do not have information for data type ''%s''', ...
		  defaults.datatype));
  end
  varargout{1} = getfield(s.supported, defaults.datatype);
  
 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end
  
