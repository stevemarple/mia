function r = ariesgain(rio, theta, phi, beams, varargin)
%ARIESGAIN Calculate the effective gain of the ARIES system.
%
%   r = ARIESGAIN(rio, theta, phi, beams, ...);
%   r: complex directivity
%   rio: RIOMETER object
%   theta: zenith angle (radians)
%   phi:   azimuth angle (radians)
%   beam: single beam
%   
%   If theta and phi are matrices then they must be the same size. NB the
%   azimuth angle is measured in the trigonometric manner, not like
%   compass angles!
%
% The following parameter name/value pairs can be used to override the
% values found from the RIOMETER INFO.
%
%   'antennaspacing', DOUBLE
%   Override the normal antenna spacing for this riometer.
%
%   'xphasing', DOUBLE
%   'yphasing, DOUBLE
%    Set the antenna phasing information.
%
%   'taperid', NUMERIC or CHAR
%   If not empty sets the tapering values used. Defaults to no tapering
%   if empty or not given.
%
%   'wantcomplex', LOGICAL
%   Flag indicating if complex values should be returned. Defaults to
%   FALSE.
%
%   'xtapering', DOUBLE
%   'ytapering', DOUBLE
%   Sets the tapering values used on the X/Y antennas. Overrides any X/Y
%   taper values selected by the taperid option.
%
% ARIESGAIN calculates the gain of the ARIES system. Note that
% directivity of the system is not a meaningful measure.
%
%   See also IRISDIRECTIVITY.

theta(theta == 0) = eps;
phi(phi == 0) = eps;

d2r = pi / 180; % factor to multiply by to convert from degrees to radians

defaults.xphasing = []; % defer
defaults.yphasing = []; % defer

defaults.taperid = []; % defer
defaults.xtapering = []; % defer
defaults.ytapering = []; % defer

%defaults.dipoletheta = [0.001:1:90] * d2r;
% defaults.dipolephi = [0.001:1:360 0.001] * d2r;
defaults.antennas = info(rio, 'antennas');
defaults.antennaspacing = info(rio, 'antennaspacing');
% defaults.butlertype = 1; %1 = unmodified BM (no zenithal beam)
%                          %0 = modified BM (zenithal beam)

defaults.wantcomplex = true;

[defaults.xphasing defaults.yphasing] = ...
    info(rio, 'antennaphasing', 'beams', beams);

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% Tapering. Let taperid set basic tapering details. Individual x or y
% tapering can be overridden by setting xtapering or ytapering.
if isempty(defaults.taperid) & ...
      (isempty(defaults.xtapering) | isempty(defaults.ytapering))
  % need to default to some taperid
  % disp('defaulting to no tapering to get missing taper details');
  % defaults.taperid = 'B000';
  defaults.taperid = 'B201';
  fprintf('defaulting to taper ID ''%s'' to get missing taper details\n', ...
          defaults.taperid);
end

% Default to no tapering
xtapering = ones(1, defaults.antennas(1));
ytapering = ones(1, defaults.antennas(2));

if isempty(defaults.taperid)
  % Default to no tapering
  xtapering = ones(1, defaults.antennas(1));
  ytapering = ones(1, defaults.antennas(2));
else
  % Use tapering from the selected taperid option
  [xtapering ytapering] = info(rio, 'tapering', defaults.taperid);
end

if isempty(defaults.xtapering)
  defaults.xtapering = xtapering;
end
if isempty(defaults.ytapering)
  defaults.ytapering = ytapering;
end

if ~isreal(defaults.xtapering) | ~isreal(defaults.ytapering)
  % disp('converting complex tapering values to real numbers');
  defaults.xtapering = abs(defaults.xtapering);
  defaults.ytapering = abs(defaults.ytapering);
end

% Antenna shape
% X fan beam (aligned on X axis, controls X position)
Nx1 = defaults.antennas(1);
Ny1 = 1;

% Y fan beam (aligned on Y axis, controls Y position)
Nx2 = 1;
Ny2 = defaults.antennas(2);

% model turnstile antenna
d0 = turnstiledirectivity(theta, phi);


beamplan = info(rio, 'beamplan');

r = cell(size(beams));
for bn = 1:numel(beams)

  
  % As we need phasing for the FANBEAMS we note that this is different to
  % phasing for the pencil beams i.e. the info stored in the rio database
  % 

  % decode required beam to x-fan and y-fan
  [yfan xfan] = find(beamplan == beams(bn));
  
  % below produces x fan beams i.e. N-S running fan beams

  % betaX1 = defaults.xphasing(xfan)
  betaX1 = defaults.xphasing;
  betaY1 = 0;
  
  % below produces y fan beams i.e. W-E running fan beams
  betaX2 = 0;
  % betaY2 = defaults.yphasing(yfan)
  betaY2 = defaults.yphasing;
  

  Ax1 = defaults.xtapering; 
  Ay1 = ones(1, length(Ax1));
  
  Ay2 = defaults.ytapering; 
  Ax2 = ones(1, length(Ay2));


  %sinTheta = sin(theta);

  if 0
    % original equations by Detrick and Rosenburg
    psiX = pi * (sinTheta .* cos(phi) - betaX);
    psiY = pi * (sinTheta .* sin(phi) - betaY);
  elseif 0
    % adaptions by SRM for antenna spacings different to 0.5 lambda
    psiX = pi * (2 * defaults.antennaspacing * sinTheta .* cos(phi) - betaX);
    psiY = pi * (2 * defaults.antennaspacing * sinTheta .* sin(phi) - betaY);
    
  elseif 1
    
    %fanbeam x

    sincos = 2 .* defaults.antennaspacing .* sin(theta) .* cos(phi);
    sinsin = 2 .* defaults.antennaspacing .* sin(theta) .* sin(phi);
    % psiX = pi * (2 * defaults.antennaspacing * sin(theta(th_row,th_col)) * cos(phi(th_row,th_col)) - betaX);
    % psiY = pi * (2 * defaults.antennaspacing * sin(theta(th_row,th_col)) * sin(phi(th_row,th_col)) - betaY);
    psiX1 = pi .* (sincos - betaX1);
    psiY1 = pi .* (sinsin - betaY1);

    psiX2 = pi .* (sincos - betaX2);
    psiY2 = pi .* (sinsin - betaY2);

    tempX1 = zeros(size(theta));
    for n=1:1:Nx1                        
      tempX1 = tempX1 + Ax1(n).* exp(j .*(n-1) .*psiX1);
    end            
    
    tempY1 = zeros(size(theta));
    for n=1:1:Ny1           
      tempY1 = tempY1 + Ay1(n) .* exp(j .* (n-1) .* psiY1);
    end
    
    AF1 = tempX1 .* tempY1;
    
    tempX2 = zeros(size(theta));
    for n=1:1:Nx2                        
      tempX2 = tempX2 + Ax2(n) .* exp(j .* (n-1) .* psiX2);
    end            
    
    
    tempY2 = zeros(size(theta));
    for n=1:1:Ny2
      tempY2 = tempY2 + Ay2(n) .* exp(j .* (n-1) .* psiY2);
    end
    
    AF2 = tempX2 .* tempY2;     
  end  

  % get rid of divide by zero warnings
  %psiX1(psiX1 == 0) = eps;

  % r = d0 .* ((1 - exp(i .* Nx .* psiX)) ./ (1 - exp(i .* psiX))) ...
  %     .*    ((1 - exp(i .* Ny .* psiY)) ./ (1 - exp(i .* psiY))) .* ...
  %     (1 - exp(i .* pi .* cos(theta)));
  r1 = d0 .* AF1.*(1 - exp(i .* pi .* cos(theta)));
  r2 = d0 .* AF2.*(1 - exp(i .* pi .* cos(theta)));

  r{bn} = r1 .* conj(r2);
  if ~logical(defaults.wantcomplex)
    r{bn} = abs(r{bn});
  end
end

% for backwards compatibility
if numel(beams) == 1
  r = r{1};
end
