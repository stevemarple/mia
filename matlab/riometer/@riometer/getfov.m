function r = getfov(in)
%GETFOV  Return the field of view information
%
%   r = GETFOV(in)
%   r: CELL array of geograpic positions giving the fields of view
%   in: RIOMETER object(s)
%
%   GETFOV returns information about the field of view for all 'in'
%   instruments. The results are inside a CELL array of size 2 by N,
%   where N is the number of instruments in 'in'.

r = cell(2, prod(size(in)));

for n = 1:prod(size(in))
  [r{1, n} r{2, n}] = info(in(n), 'pixels', [], 'deg');
end
