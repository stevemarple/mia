function r = info_rio_nal_2(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

val = 1;

% there were 1.4e6 samples per second when this value was estimated 
dbmconversion = -199 + 10 * log10(1.4e6);

switch req
 case 'arcombeammap'
  % This indicates where in the arcom data array each beam is
  % located. The final line of the matrix are endfire beams. Access them
  % as beams 50-64.
  r.varargout{1} = [10 11 12 13 14 15 16 ... 
		    18 19 20 21 22 23 24 ...
		    26 27 28 29 30 31 32 ...
		    34 35 36 37 38 39 40 ...
		    42 43 44 45 46 47 48 ...
		    50 51 52 53 54 55 56 ... 
		    58 59 60 61 62 63 64 ...
		    1  2  3  4  5  6  7 8  9 17 25 33 41 49 57];
  
 
 case 'calibrationbeams'
  r.varargout{1} = 25;
  
 case 'calibrationsignalstrength'
  % This is the calibration signal strength in appropriate units.
  defaults.time = [];
  defaults.beams = [];
  defaults.units = '';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
    if isempty(defaults.beams)
    error('require beams');
  elseif isempty(defaults.time)
    error('require time');
  elseif ~isa(defaults.time, 'timestamp')
    error('time must be a timestamp')
  end
  
  r.varargout{1} = 182000;
  
  switch defaults.units
   case ''
    error('require units');
   case 'raw'
    ; % do nothing
   case 'dBm'
    mia = rio_rawpower('starttime', defaults.time, ...
                       'instrument', in, ...
		       'data',  r.varargout{1}, ...
		       'units', 'raw');
    mia2 = rio_power(mia);
    r.varargout{1} = getdata(mia2);
   otherwise
    error(sprintf('unknown units (was ''%s'')', defaults.units));
  end
  
 case 'calibrationwindow'
  % for calibration use the 3rd to last samples in the calibration sequence
  % r.varargout{1} = [3 inf];
  r.varargout{1} = [-inf inf];
  
 case 'commoncalibrationdata'
  r.varargout{1} = true;

 case 'dbmconversion'
  % This is the correction to get the dBm value of the uncalibrated data
  % approximately correct. See also 'calibrationsignalstrength'
  
  % varargin{1} is time (timestamp)
  if length(varargin) < 1
    error('require start time of file'); % but ignored at present
  end
  r.varargout{1} = dbmconversion;

 case 'defaultcalibrationmethod'
  r.varargout{1} = 'cubic';

 case 'includesequencenumber'
  % modify a strftime format specifier to include the sequence number
  if length(varargin) ~= 1
    error('require format specifier (only)');
  end
    [fstr_path fstr_file fstr_ext] = fileparts(varargin{1});
  fstr_file = strrep(fstr_file, '_000_f', '_%%03d_f');
  r.varargout{1} = fullfile(fstr_path, [fstr_file fstr_ext]);
    
 case 'tapering'
  taper = ones(1, 8);
  r.varargout{1} = taper;
  r.varargout{2} = taper;
 
 
 otherwise
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
