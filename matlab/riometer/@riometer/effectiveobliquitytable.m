function [r, az, zen] = effectiveobliquitytable(rio, varargin)
%EFFECTIVEOBLIQUITYTABLE  Create lookup table for effective obliquity.

defaults.beams = [];
defaults.height = info(rio, 'defaultheight');

defaults.geodesicmesh = []; % calculate default only if needed (slow)
defaults.azimuth = [];
defaults.zenith = [];

defaults.zenithalabsorption = [];
% [0.0001 0.01:0.01:0.1 0.15:0.05:0.5 ...
%	    0.6:0.1:5 5.5:0.5:30];

[defaults unvi] = interceptprop(varargin, defaults);

numOfBeams = prod(size(defaults.beams));

defaults.zenithalabsorption = ...
    reshape(defaults.zenithalabsorption, ...
	    1, prod(size(defaults.zenithalabsorption)));

absSz = size(defaults.zenithalabsorption);
% check absorption is matrix having same number of rows as beams (with
% rows ordered as beams

if prod(absSz) == 0
  error('zenithal absorption level (dB) not specified');
elseif any(defaults.zenithalabsorption <= 0)
  error('zenithal absorption values must be > 0 dB');
end


% get the directivity
[d az zen] = directivity(rio, ...
			     'geodesicmesh', defaults.geodesicmesh, ...
			     'zenith', defaults.zenith, ...
			     'azimuth', defaults.azimuth, ...
			     'beams', defaults.beams);

% obliq = 1 ./ cos(zen); % ignores curvature of ionosphere

% allow for curvature of the ionosphere
alpha = polarangleofray(zen, defaults.height);
obliq = 1 ./ cos(zen - alpha);

r.obliquity = zeros(numOfBeams, prod(absSz));
for n = 1:numOfBeams
  pd = power(d{n}, 2);
  % totalPower = sum(pd);
  totalPower = nonansum(pd);
  
  for m = 1:prod(absSz)
    % obliquity has to apply to absorption values in dB
    abs = power(10, -defaults.zenithalabsorption(m) .* obliq / 10);

    % absPower = sum(pd .* abs);
    absPower = nonansum(pd .* abs);
    
    % apparent absorption
    appAbs = 10 * log10(totalPower / absPower);
    
    % effective obliquity
    r.obliquity(n, m) = appAbs ./ defaults.zenithalabsorption(m);
  end
end

r.zenithalabsorption = defaults.zenithalabsorption;
% table maps true zenithal absorption to obliquity factor

r.apparentabsorption = zeros(size(r.obliquity));
for n = 1:numOfBeams
  r.apparentabsorption(n, :) = r.obliquity(n, :) .* r.zenithalabsorption;
end
r.beams = defaults.beams;
r.height = defaults.height;

% insert extra column at start so that small/negative values are not
% converted to nans. Make first column as negative as possible so that all
% negative absorption values have the same effective obliquity factor as the
% smallest positive absorption value would. Make no attempt to hide nans
% creeping in for very high absorption values - anything so high can
% justifiably be converted to nan - issues such as receiver noise
% temperature, antenna noise temperature and ratio of cosmic background
% temperature to mesosphere temperature become significant.

r.obliquity = r.obliquity(:, [1 1:end]);
r.apparentabsorption = [repmat(-realmax, numOfBeams, 1), ...
		    r.apparentabsorption];
r.zenithalabsorption = [-realmax r.zenithalabsorption];
