function r = gettype(in, varargin)
%GETTYPE  Return instrument type.
%
%   r = GETTYPE(in)
%   r: CHAR
%   in: RIOMETER object
%
%   See also MIA_INSTRUMENT_BASE, RIOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'riometer';
  
 case 'u'
  r = 'RIOMETER';
  
 case {'c' 'C'}
  r = 'Riometer';
 
 otherwise
  error(sprintf('unknown mode (was ''%s'')', mode));
end
return


