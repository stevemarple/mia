function r = ariesgain(rio, T, P, beams, varargin)
% ARIESGAIN  Calculate complex ARIES power gain
%
% G  =  ariesgain(T, P, beams, taperid)
%
% T, P are the zenith and azimuth angles of the look-directions for
% which the gain is wanted. T and P should be in radians and the
% azimuth is defined so that it increases anti-clockwise from east
% (i.e. not like a compass bearing). It is assumed that the ARIES
% antenna arrays are aligned geographically east-west and
% north-south.
%
% 'beams' is a vector of beam numbers for which the gain is wanted and
% 'taperid' is a string which describes the antenna tapering that
% should be applied.
%
% G is a matrix of complex power gains the same size as T and P.
%
% Note: it is possible that the phase of G may be incorrect if the
% cross-correlation multiplies the signals from the two fan beams
% differently than is assumed in this code. This should not affect
% the magnitude of G. 

N = 32; % number of antennas per arm
d = 0.5; % antenna spacing (wavelengths)

defaults.taperid = []; % defer
defaults.xtapering = []; % defer
defaults.ytapering = []; % defer
defaults.wantcomplex = true;

% By default return pencil beams, but allow xfan and yfan data to be
% returned also.
defaults.beamtype = 'pencil';

% Accept these options, but check to see the defaults have not changed,
% otherwise generate an error since the settings ae not honoured.
defaults.xphasing = [];
defaults.yphasing = [];
defaults.antennas = [];
defaults.antennaspacing = [];


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

% Check the options are accepted but ignore are either not set, or are set
% to the correct values for this riometer.
[xphasing yphasing] = info(rio, 'antennaphasing', 'beams', beams);
if ~isempty(defaults.xphasing) & ~isequal(defaults.xphasing, xphasing)
  error('xphasing overriden but this function does not honour the setting');
end
if ~isempty(defaults.yphasing) & ~isequal(defaults.yphasing, yphasing)
  error('yphasing overriden but this function does not honour the setting');
end
if ~isempty(defaults.antennas) ...
  & ~isequal(defaults.antennas, info(rio, 'antennas'))
  error('antennas overriden but this function does not honour the setting');
end
if ~isempty(defaults.antennaspacing) ...
  & ~isequal(defaults.antennaspacing, info(rio, 'antennaspacing'))
  error('antennaspacing overriden but this function does not honour the setting');
end

% Tapering. Let taperid set basic tapering details. Individual x or y
% tapering can be overridden by setting xtapering or ytapering.
if isempty(defaults.taperid) & ...
      (isempty(defaults.xtapering) | isempty(defaults.ytapering))
  % need to default to some taperid
  % disp('defaulting to no tapering to get missing taper details');
  % defaults.taperid = 'B000';
  defaults.taperid = 'B201';
  fprintf(['%s: defaulting to taper ID ''%s'' to get missing ' ...
	   'taper details\n'] , ...
	  mfilename, defaults.taperid);
end

% Default to no tapering
xtapering = ones(1, N);
ytapering = ones(1, N);

if isempty(defaults.taperid)
  % Default to no tapering
  xtapering = ones(1, defaults.antennas(1));
  ytapering = ones(1, defaults.antennas(2));
else
  % Use tapering from the selected taperid option
  [xtapering ytapering] = info(rio, 'tapering', defaults.taperid);
end

if isempty(defaults.xtapering)
  defaults.xtapering = xtapering;
end
if isempty(defaults.ytapering)
  defaults.ytapering = ytapering;
end

if ~isreal(defaults.xtapering) | ~isreal(defaults.ytapering)
  % disp('converting complex tapering values to real numbers');
  defaults.xtapering = abs(defaults.xtapering);
  defaults.ytapering = abs(defaults.ytapering);
end




ref_x = 15; % position of intersection antenna on X arm
ref_y = 15; % position of intersection antenna on Y arm


% determine fan beams for desired pencil beam

bp = info(rio_ram_2, 'beamplan');

for bn = 1:numel(beams)
  beam = beams(bn);

  [ny,nx] = find(bp == beam);

  % directivity of each antenna

  d0 = turnstiledirectivity(T,P); 

  % account for the ground image when antenna is lambda/4 above
  % ground

  d0 = d0.*(1-exp(i*pi*cos(T)));

  % steering phases - these determine the pointing direction of each
  % fan beam and ensure we get two beam positions either side of zenith, not
  % a zenithal beam.

  a = (1:N)-(N+1)/2;
  spx = 2*pi*a*(nx-(N+1)/2)/N;
  spy = -2*pi*a*(ny-(N+1)/2)/N;

  % components of wave vector in x,y plane

  kx = 2*pi*sin(T).*cos(P);
  ky = 2*pi*sin(T).*sin(P);

  % directivity of x-arm fan beam

  zx = zeros(size(T));
  for n = 1:N
    phi = (n-ref_x)*d*kx - spx(n);
    zx = zx+defaults.xtapering(n)*exp(-i*phi);
  end
  zx = zx.*d0;

  % directivity of y-arm fan beam

  zy = zeros(size(T));
  for n = 1:N
    phi = (n-ref_y)*d*ky - spy(n);
    zy = zy+defaults.ytapering(n)*exp(-i*phi);
  end
  zy = zy.*d0;

  % gain of pencil beam

  G = zx.*conj(zy); % are zx and zy the right way round?

  switch defaults.beamtype
   case {'' 'pencil'}
    r{bn} = G;
   case 'xfan'
    r{bn} = zx;
   case 'yfan'
    r{bn} = zy;
   otherwise
    error(sprintf('unknown beam type (was ''%s'')', defaults.beamtype));
  end
  
  if ~defaults.wantcomplex
    r{bn} = abs(r{bn});
  end
end


% for backwards compatibility
if numel(beams) == 1
  r = r{1};
end

