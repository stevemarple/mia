function r = info2(in, vout, s, req, varargin)
%INFO2 Information function for RIOMETER objects
%
%   [...] = INFO(in, request, ...)
%   in: MIA_INSTRUMENT_BASE object
%   request: information request
%
% This function documents only the additional requests for RIOMETER
% instruments and the documentation should be read in conjunction with
% mia_instrument_base/INFO. Note that INFO2 should not be called
% directly.
%
% The following additional requests are recognised:
%
%   r = INFO(in, 'allbeams');
%   r: numbers of all beams
%   All beam numbers.
%
%   [xphasing yphasing] = INFO(in, 'antennaphasing', ...);
%   xphasing: phasing information in X coordinate
%   yphasing: phasing information in Y coordinate
%   Unless the additional parameter name/value pair "'beams', value" is
%   given then the phasing details for all imaging beams are given.
%
%   r = INFO(in, 'antennaazimuth');
%   r: azimuth of antenna array (compass degrees)
%
%   r = INFO(in, 'antennas');
%   r: number of antennas (rows, columns)
%
%   r = INFO(in, 'antennaspacing');
%   r: spacing between antennas (in wavelengths)
%
%   [itype wtype] = INFO(in, 'antennatype');
%   itype: imaging beams antenna type
%   wtype: widebeams antenna type
%   Possible antenna types include 'crossed-dipole' and 'la jolla'. An
%   empty string is returned if the antenna type is not known.
%
%   r = INFO(in, 'antennaspacing');
%   r: spacing between antennas (in wavelengths)
%
%   r = INFO(in, 'azimuth');
%   r = INFO(in, 'azimuth', beams);
%   r: azimuth of the beams (for all beams if not beam numbers are not
%   specified). Angle is in compass degrees and includes any effect of
%   the antenna array pointing direction (see 'antennaazimuth' request).
%
%   r = INFO(in, 'badbeams');
%   r: beam numbers of bad beams (i.e., beams which should not normally
%   be used, such as imaging beams with high sidelobe levels, or beams
%   which suffer from interference)
%
%   r = INFO(in, 'beamindex', beams);
%   r: order of the beams in the data
%
%   [x, y] = INFO(in, 'beamlocations', ...);
%   x, y: coordinates of the beam locations in the units specified
%   The following parameter name value pairs are accepted:
%       'beams', DOUBLE
%       Beam numbers to use, defaults to all beams if not specified
%
%       'units', CHAR
%       coordinate system units, such as 'deg', 'km', 'km_antenna' or
%       'aacgm'.
% 
%       'height', DOUBLE
%       projection height in metres. Also used for magnetic coordinate
%       systems.
%   Unknown parameters are passed onto riometer/CALCBEAMLOCATIONS.
%
%   r = INFO(in, 'beamplan');
%   r: schematic layout of the imaging beams.
%
%   [ibeams wbeams bbeams] = INFO(in, 'beams');
%   ibeams: imaging beam numbers
%   wbeams: widebeam numbers
%   bbeams: beam numbers of "bad" beams, which should not be used normally
%
%   r = INFO(in, 'defaultheight');
%   r: default height (in metres) used when projecting beams onto the
%   ionosphere.
%
%   r = INFO(in, 'beamwidth', ...);
%   r: nominal beam width (angle between opposite -3dB points)
%   The following name/value pairs are recognised:
%       'beams', DOUBLE
%       Numbers of the beams to use, defaults to all beams
%      
%       'units', CHAR
%       Units for the beam width angle, defaults to degrees ('deg')
%
%   [x y] = INFO(in, 'fov', ...);
%   x, y: coordinates of the field of view. See the 'pixels' information
%   request for more information about additional parameters.
%
%   r = INFO(in, 'goodbeams');
%   r: number of beams which are not 'bad'. See 'badbeams' for more
%   information.
%
%   r = INFO(in, 'imagingbeams');
%   r: beam numbers for the imaging beams (empty if none)
%
%   [x, y] = INFO(in, 'pixels', ...);
%   x, y: coordinates of the pixel locations in the units specified
%   The following parameter name value pairs are accepted:
%       'beams', DOUBLE
%       Beam numbers to use. Currently ignored but someday it may be
%       possible to calculate a sensible perimter from a given set of
%       beams.
%
%       'units', CHAR
%       coordinate system units, such as 'deg', 'km', 'km_antenna' or
%       'aacgm'.
% 
%       'height', DOUBLE
%       Currently ignored, but may be used in future. Defaults to the
%       defaultheight value.
%
%   r = INFO(in, 'preferredbeam', ...);
%   r: preferred beam number. Use to decide upon a default beam for
%   plotting, etc. Additional parameter name/value pairs may be
%   specified:
%       'beams', DOUBLE
%       Beam numbers indicating from which beams the preferred beam
%       should be chosen. Defaults to all beams.
%
%       'time', DOUBLE
%       Normally ignored, but may be used to select an alternative value
%       for when certain hardware is know to be broken/faulty.
%
%
%   r = INFO(in, 'qdcclass');
%   r: class used to store QDCs (e.g., 'rio_qdc', or 'rio_rawqdc')
%
%   r = INFO(in, 'qdcduration');
%   r: standard duration of QDCs (TIMESPAN)
%
%   r = INFO(in, 'qdcoffset');
%   r: offset used when calculating QDCs (TIMESPAN)
%
%   r = INFO(in, 'qdcstarttime', t);
%   r: QDC start time (TIMESTAMP)
%   t: a time within the QDC priod (TIMESTAMP)
% 
%   r = INFO(in, 'standardobliquity');
%   r = INFO(in, 'standardobliquity', beams);
%   r: standard obliquity value for the beams (or for all beams if not
%   beam numbers are not specified).
%
%   r = INFO(in, 'systemtype');
%   r: system type, e.g., 'iris', 'aries', 'widebeam'
%
%   r = INFO(in, 'widebeams');
%   r: beam numbers for the wide beams (empty if none)
%
%   r = INFO(in, 'zenith');
%   r = INFO(in, 'zenith', beams);
%   r: zenith angle of the beams (for all beams if not beam numbers are not
%   specified). Result is in degrees.


r = vout;
r.validrequest = 1; % assume valid

abbrev_sn = getcode(in, 0);
allbeams = union(s.imagingbeams, s.widebeams);

switch req
 case 'antennaphasing'
  defaults.beams = s.imagingbeams;
  [defaults unvi] = interceptprop(varargin, defaults);
  bi = info(in, 'beamindex', defaults.beams);
  r.varargout{1} = s.antennaphasingx(bi);
  r.varargout{2} = s.antennaphasingy(bi);

  
 case 'antennatype'
  r.varargout{1} = {s.iantennatype s.wantennatype};
  
 case 'beamindex'
  % normal situation is first beam is #1
  r.varargout{1} = varargin{1};

  % these requests are all returned verbatim from the data structure
 case {'antennaazimuth' 'antennas' 'antennaspacing' ...
       'systemtype' 'qdcclass' 'qdcduration' 'qdcoffset' ...
      'imagingbeams' 'widebeams' 'badbeams'}
  r.varargout{1} = getfield(s, req);
  
 case 'defaultheight'
  if ~isempty(s.defaultheight)
    r.varargout{1} = s.defaultheight;
  else
    r.varargout{1} = 90e3; % default to 90km
  end
  
  % these requests all have one value per beam, with an optional
  % parameter being the beam number to limit which values are returned
 case {'azimuth' 'zenith' 'standardobliquity'}
  r.varargout{1} = getfield(s, req);
  if length(varargin)
    r.varargout{1} = r.varargout{1}(info(in, 'beamindex', varargin{1}));
  end
  % in the database azimuth is relative to pointing direction
  if strcmp(req, 'azimuth')
    r.varargout{1} = r.varargout{1} + s.antennaazimuth;
  end
  
 case 'beamlocations'
  defaults.units = [];
  defaults.beams = allbeams;
  defaults.height = s.defaultheight;
  defaults.antennaazimuth = [];
  [defaults unvi] = interceptprop(varargin, defaults);

  if isempty(defaults.units)
    error('units must be set');
  elseif strcmp(defaults.units, 'beamplan')
    bp = info(in, 'beamplan');
    tmp1 = zeros(1, numel(defaults.beams));
    tmp2 = zeros(1, numel(defaults.beams));
    for n = 1:numel(defaults.beams)
      [row col] = find(bp == defaults.beams(n));
      switch numel(col)
       case 0
	% not found - wide beam?
	tmp1(n) = nan;
	tmp2(n) = nan;
       case 1
	tmp1(n) = col;
	tmp2(n) = size(bp, 1) + 1 - row;
       otherwise
	error(sprintf('beam %d occurs multiple times in the beam plan', ...
		      defaults.beams(n)));
      end
    end
    
  else
    
    if isempty(defaults.antennaazimuth)
      defaults.antennaazimuth = s.antennaazimuth;
    end
    bi = info(in, 'beamindex', defaults.beams);
    % in the database azimuth is relative to pointing direction
    [tmp1 tmp2] = ...
	calcbeamlocations(in, ...
			  'beams', defaults.beams, ...
			  'azimuth', s.azimuth(bi)+defaults.antennaazimuth, ...
			  'zenith', s.zenith(bi), ...
			  'height', defaults.height, ...
			  'units', defaults.units, ...
			  varargin{unvi});
  end
  if length(r.varargout) > 1
    r.varargout{1} = tmp1;
    r.varargout{2} = tmp2;
  else
    r.varargout{1} = [tmp1;tmp2];
  end
  
 case 'fov'
  [r.varargout{1}, r.varargout{2}] = calcfov(in, varargin{:});
  
 case 'pixels'
  % Accept a beams parameter. Someday may be able to select a set of
  % pixels based on which beams are actually used.
  defaults.beams = [];
  defaults.units = '';
  defaults.height = s.defaultheight; % accept but ignore for now
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  if isempty(defaults.units)
    error('units must be set');
  elseif strcmp(defaults.units, 'beamplan')
    bp = info(in, 'beamplan');
    r.varargout{1} = 1:size(bp, 2);
    r.varargout{2} = 1:size(bp, 1);
    return
  end
    
  knownSystems = {'deg' 'aacgm' 'cgm' 'km' 'm' 'km_antenna' 'm_antenna'};
  
  
  if ~isfield(s.pixels, defaults.units)
    if ~any(strcmp(defaults.units, knownSystems))
      error(sprintf('unknown coordinate system (was ''%s'')', ...
		    defaults.units));
    else
      error(sprintf(['do not have default pixel values for ''%s''' ...
		     ' for riometer %s'], defaults.units, abbrev_sn));
    end
  end
  tmp = getfield(s.pixels, defaults.units);
  r.varargout{1} = tmp.x.min:tmp.x.step:tmp.x.max;
  r.varargout{2} = tmp.y.min:tmp.y.step:tmp.y.max;
  
 case 'preferredbeam'
  defaults.beams = [s.widebeams s.imagingbeams];
  
  % ignore a time option here but accept as may be used by a function
  % specific to one riometer
  defaults.time = []; 
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});

  % use the first widebeam if one is present
  tmp = intersect(s.widebeams, defaults.beams);
  if ~isempty(tmp)
    r.varargout{1} = tmp(1);
    return;
  end
  
  % otherwise use most vertical beam
  if ~isempty(s.zenith)
    [tmp1 tmp2] = min(info(in, 'zenith', defaults.beams));
    r.varargout{1} = defaults.beams(tmp2);
  else
     % don't have zenith information
     if numel(s.beamplanc) == 1 & numel(s.beamplanr) & ~isempty(s.ibeams)
       % have beamplan, so calculate the beam nearest the centre of the array
       beamplan = info(in, 'beamplan');
       [row col] = find(ismember(beamplan, defaults.beams));
       % find minimum distance (min dist is also min dist^2)
       dist = (row - (s.beamplanr/2)).^2 + (col - (s.beamplanc/2)).^2;
       [tmp idx] = min(dist);
       r.varargout{1} = beamplan(row(idx), col(idx));
     else
       % use first beam
       r.varargout{1} = defaults.beams(1);
     end
  end
  
 case 'beamplan'
  r.varargout{1} = reshape(s.imagingbeams, [s.beamplanr s.beamplanc])';
  
 case 'beams'
  % for backwards compatibility
  r.varargout{1} = s.imagingbeams;
  r.varargout{2} = s.widebeams;
  r.varargout{3} = s.badbeams;
  
 case 'allbeams'
  r.varargout{1} = union(s.imagingbeams, s.widebeams);
  
 case 'goodbeams'
  r.varargout{1} = setdiff(union(s.imagingbeams, s.widebeams), s.badbeams);
  
 case 'qdcstarttime'
  % with method below it is not clear what floor should round to. It creates
  % different values between the old and new timestamp classes.
  
  %r.varargout{1} = floor(varargin{1} - s.qdcoffset, s.qdcduration) ...
  %   + s.qdcoffset;
  
  os = fix(s.qdcoffset ./ timespan(1, 'd'));
  days = fix(s.qdcduration ./ timespan(1, 'd'));
  dn = (floor(fix(datenum(varargin{1}) - days - os) / days) * days) ...
       + days + os;
  
  r.varargout{1} = timestamp(datevec(dn));

  
 case 'beamwidth'
  defaults.beams = allbeams;
  defaults.units = 'deg';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  bi = info(in, 'beamindex', defaults.beams);
  switch defaults.units
   case 'deg'
    r.varargout{1} = s.beamwidth(bi);
   
   otherwise
    error(['unknown unit (was ' units ')']);
  end

 otherwise
  r.validrequest = vout.validrequest; % put back to original setting
end
  
