function r = info_rio_ram_4(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

val = 1;
switch req
 case 'arcombeammap' % ------------------------
  % This indicates where in the arcom data array each beam is
  % located
  r.varargout{1} = 1:32;
    
 case 'calibrationsignalstrength' % ------------------------
  % This is the calibration signal strength in dBm
  % varargin{1} is time (timestamp)
  % varargin{2} is beams
  if length(varargin) < 2
    error('require start time of file and beams');
  end
  load info_rio_ram_2_calibrationsignalstrength;
  r.varargout{1} = calibrationsignalstrength.data(varargin{2});

 case 'calibrationwindow' % ------------------------
  % for calibration use the 3rd to last samples in the calibration sequence
  r.varargout{1} = [3 inf];

 case 'dbmconversion' % ------------------------
  % varargin{1} is time (timestamp)
  if length(varargin) < 1
    error('require start time of file')
  end

  r.varargout{1} = -205 + 10 * log10(1.4e6);
 
 case 'includesequencenumber' % ------------------------
  % modify a strftime format specifier to include the sequence number
  if length(varargin) ~= 1
    error('require format specifier (only)');
  end
    [fstr_path fstr_file fstr_ext] = fileparts(varargin{1});
  fstr_file = strrep(fstr_file, '_000_f', '_%%03d_f');
  r.varargout{1} = fullfile(fstr_path, [fstr_file fstr_ext]);
   
 case 'tapering' % ------------------------
  % refer to rio_ram_2 for tapering details
  [r.varargout{1:2}] = info(rio_ram_2, 'tapering', varargin{:});
 
 case 'defaultfilename' % ------------------------
  cls = varargin{1};
  if ~ischar(varargin{1})
    cls = class(varargin{1});
  end

  % modify standard names if the archive option was used
  defaults.archive = '';
  [defaults unvi] = interceptprop(varargin(2:end), defaults);
  switch defaults.archive
   case ''
    ; % do nothing
    
   case 'summary'
    switch cls
     case {'original_format' 'rio_rawpower'}
      r.varargout{1}.fstr = ...
          ['http://www.dcs.lancs.ac.uk/iono/miadata/riometer/' ...
           'ram_2/original_format.summary/aries_summary_%Y-%m-%d.arcom'];
      r.varargout{1}.duration = timespan(1, 'd');
      % don't need a special load function since the files are one per
      % day, with no open/closed suffixes etc
      r.varargout{1}.loadfunction = '';
     
     case {'rio_power' 'rio_power_calib'}
      r.varargout{1}.fstr = strrep(r.varargout{1}.fstr, ...
				   'rio_power', ...
				   'rio_power.summary');
      
     case {'rio_qdc' 'rio_qdc_fft'}
      r.varargout{1}.fstr = strrep(r.varargout{1}.fstr, ...
				   'rio_qdc', ...
				   'rio_qdc.summary');
      r.varargout{1}.fstr
     otherwise
      ; % summary archive not supported for this class, ignore
    end
    
   otherwise
    error(sprintf('unknown archive (was ''%s'')', defaults.archive));
   
  end
 
  
 otherwise % ------------------------
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
