function [b,d] = beamswithscintillation(rio, varargin)
%BEAMSWITHSCINTILLATION Determine beams affected by scintillation.
%
%   [r d] = BEAMSWITHSCINTILLATION(rio, ...)
%   r: vector of beam affected by scintillation
%   d: STRUCT of options used
%   rio: RIOMETER object
%
% BEAMSWITHSCINTILLATION determines which imaging beams are likely to be
% significantly affected by scintillation. For Northern hemisphere
% riometers the default source is cassiopeia_a, and for Southern
% hemisphere riometers sagittarius_astar.
%
% By default only beams for which the star passes through the -3dB edge
% are considered to be affected. The beams, resolution, sources, method
% and threshold can all be adjusted if considered necessary.
%
% See also SCINTILLATION_CALCULATOR.

loc = getlocation(rio);
defaults.beams = info(rio, 'imagingbeams');
defaults.resolution = timespan(10, 'm');

if getgeolat(loc) >= 0
  defaults.sources = {'cassiopeia_a'};
else
  defaults.sources = {'sagittarius_astar'};
end

defaults.method = 'antennagain';
defaults.threshold = -3;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi});

% Any day will do.
st = timestamp('2000-01-01');
et = timestamp('2000-01-02');

samt = st:defaults.resolution:et;

% meth = {'method', 'angulardistance', 'threshold', []};
% meth = {'method', 'antennagain', 'threshold', -3};

mask = scintillation_calculator(rio, ...
                                'time', samt, ...
                                'beams', defaults.beams, ...
                                'sources', defaults.sources, ...
				'method', defaults.method, ...
				'threshold', defaults.threshold);


b = defaults.beams(sum(mask,2) > 1); % beams affected by scintillation
d = defaults;
