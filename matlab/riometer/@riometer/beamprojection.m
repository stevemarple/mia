function [x, y] = beamprojection(rio, varargin)
%BEAMPROJECTION  Calculate a RIOMETER beam projection.
%
%   [x, y] = BEAMPROJECTION(rio, ...)
%   x: x coordinates, CELL vector corresponding to beam numbers
%   y: y coordinates, CELL vector corresponding to beam numbers
%   rio: RIOMETER object
%
%  The beam projection is calculated for the riometer's own default height
%  (from the INFO 'defaultheight' request), and is based on a curved
%  ionosphere. Where possible the projection is made using the antenna
%  radiation pattern. If that is not possible then the beam parameters
%  (width, azimuth and zenith) are used to project a circular beam onto the
%  ionosphere. The default behaviour may be modified with the following
%  name/value pairs:
%
%     'beams', DOUBLE
%     A vector of the riometer beam numbers for which the projections
%     should be calculated.
%
%     'height', DOUBLE
%     The height (in metres) for which the projection should be
%     calculated.
%
%     'contourlevel', DOUBLE (dB)
%     The contour level, in decibels, for which the contours should be
%     calculated. The default is -3dB.
%
%     'units', CHAR
%     The coordinate system to use. Options are 'deg', 'aacgm', 'cgm'(?),
%     'm', 'm_antenna'.
%
%     'points', DOUBLE
%     When the antenna radiation pattern is not available points determines
%     the number of points on the beam outline which are projected
%     upwards onto the ionosphere.
%
% See also PLOTBEAMPROJECTION.

[imagingBeams wideBeams] = info(rio, 'beams');
defaults.beams = [imagingBeams wideBeams];
defaults.points = 80; % number of points on circle
defaults.height = info(rio, 'defaultheight');
defaults.contourlevel = -3;
defaults.units = 'deg';

[defaults unvi] = interceptprop(varargin, defaults);

numOfBeams = prod(size(defaults.beams));


ibeamsPos = find(ismember(defaults.beams, imagingBeams));
ibeams = defaults.beams(ibeamsPos);

wbeamsPos = find(ismember(defaults.beams, wideBeams));
wbeams = defaults.beams(wbeamsPos);

x = cell(1, numOfBeams);
y = cell(1, numOfBeams);

if ~isempty(ibeams)
  if ~isempty(info(rio, 'antennaphasing'));
    % have phasing information for this riometer, calculate beam pattern
    % from theory, see IRISDIRECTIVITY.
    [tmpx tmpy] = irisbeamprojection(rio, ...
				     'height', defaults.height, ...
				     'units', defaults.units, ...
				     'beams', ibeams, ...
				     'contourlevel', defaults.contourlevel,...
				     varargin{unvi});
  else
    [tmpx tmpy] = basicbeamprojection(rio, ...
				      'height', defaults.height, ...
				      'units', defaults.units, ...
				      'beams', ibeams, ...
				      'points', defaults.points, ...
				      varargin{unvi});
  end
  if iscell(tmpx)
    [x{ibeamsPos}] = deal(tmpx{:});
    [y{ibeamsPos}] = deal(tmpy{:});
  else
    x{ibeamsPos} = tmpx;
    y{ibeamsPos} = tmpy;
  end
end

if ~isempty(wbeams)
  for n = 1:numel(wbeamsPos)
    b = defaults.beams(wbeamsPos(n));
    [tmpx tmpy] = basicbeamprojection(rio, ...
				      'height', defaults.height, ...
				      'units', defaults.units, ...
				      'beams', b, ...
				      'points', defaults.points);
    if iscell(tmpx)
      [x{wbeamsPos(n)}] = deal(tmpx{:});
      [y{wbeamsPos(n)}] = deal(tmpy{:});
    else
      x{wbeamsPos(n)} = tmpx;
      y{wbeamsPos(n)} = tmpy;
    end  
  end
end


