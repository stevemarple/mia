function [r1,r2] = calcqdcstarttime(in, st, varargin)
%CALCQDCSTARTTIME  Calculate the start time of the required QDC.
%
%   [r1,r2] = CALCQDCSTARTTIME(in, st, et)
%   r1: start time of required RIO_QDC object (TIMESTAMP)
%   r2: end time of required RIO_QDC object (TIMESTAMP)
%   in: RIOMETER instrument
%   st: start time (TIMESTAMP)
%   et: end time (TIMESTAMP)
%
%   See also RIO_QDC, TIMESTAMP.

if length(varargin)
  r1 = info(in, 'qdcstarttime', mean([st varargin{1}]));
else
  r1 = info(in, 'qdcstarttime', st);
end

r2 = r1 + info(in, 'qdcduration');

