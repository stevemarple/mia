function r = getimageclassname(in)
%GETIMAGECLASSNAME  Return class name(s) of image types for instrument.
%
%   r = GETIMAGECLASSNAME(in)
%   r: CELL array of class names of associated image classes (empty if
%   none)
%   in: instrument object of type RIOMETER.
%
%   See also MIA_INSTRUMENT_BASE.

r = {'rio_image'}

