function r = getdefaultlineargs(in)
%GETDEFAULTLINEARGS  Return the default LINE parameters.
%
%   r = GETDEFAULTLINEARGS(in)
%   r: CELL array of parameters
%   in: MIA_INSTRUMENT_BASE object
%
%   See also mia_instrument_base/PLOT.

color = 'y';
if getgeolat(getlocation(in(1))) < 0
  % probably plotting on white not green
  color = 'r';
end

r = {'Color', color, ...
     'LineStyle', 'none', ...
     'Marker', '+'};
