function [mask,val,t,stardata,defaults] = negative_sidelobe_calculator(rio, varargin)
%NEGATIVE_SIDELOBE_CALCULATOR  Predict when sources enter negative sidelobes.
%
%   [mask val t stardata] = NEGATIVE_SIDELOBE_CALCULATOR(rio, ...)
%   mask: LOGICAL mask indicating when sources are in negative sidelobes
%   val: complex antenna gain values
%   t: vector of TIMESTAMP objects
%   stardata: STRUCT containing details of radio sources selected
%   rio: RIOMETER object
%   ...: additional parameter name/value pairs required, see below
%
% NEGATIVE_SIDELOBE_CALCULATOR calculates when the selected radio sources
% pass through negative sidelobes. NEGATIVE_SIDELOBE_CALCULATOR obtains its
% radio source details from SCINTILLATION_CALCULATOR.
%
% The default behaviour can be modified by the following name/value
% pairs:
%
%   'time', TIMESTAMP
%   TIMESTAMP vector detailing when the calculation(s) should be
%   made. Required unless starttime/endtime/resolution supplied.
%
%   'starttime', TIMESTAMP
%   'endtime', TIMESTAMP
%   'resolution', TIMESPAN
%   Scalar indicating the times for which the calculations should be made.
%
%   'beams', DOUBLE (vector)
%   The riometer beams for which calculations should be made. Defaults to
%   all beams if omitted or empty matrix given.
%
%   'threshold', DOUBLE (scalar or vector)
%   The threshold value used when calculating the edge of the sidelobes,
%   in dB relative to gain at beam centre.
%
%   'sources', CELL
%   Select radio sources used in the calculation. Note that if multiple
%   sources are selected the same threshold is used for each source. The
%   CELL array should contains either CHARs, which are the shorthand names
%   of known sources (from listsources), [1x2] numeric data (right ascension
%   and declination, in degrees), or STRUCTs of the same form returned by
%   listsources).
%
% See also MIA_FILTER_REMOVE_SCINTILLATION.

if ~strcmp(info(rio, 'systemtype'), 'aries')
  error('Negative sidelobes exist only with Mills cross imaging riometers');
end

% multipliers to convert degrees to radians, and back
d2r = (pi/180);
r2d = (180/pi);

mask = logical([]);


% for timestamped data
defaults.time = [];

% for starttime/endtime and resolution data
defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];

defaults.beams = [];
loc = getlocation(rio);
if getgeolat(loc) >= 0
  defaults.sources = {'cassiopeia_a' 'cygnus_a'};
else
  defaults.sources = {'sagittarius_a'};
end
defaults.threshold = -20


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

numsources = numel(defaults.sources);

if isempty(defaults.beams)
  defaults.beams = info(rio, 'goodbeams');
end
numbeams = numel(defaults.beams);

ser_empty = [isempty(defaults.starttime) ...
	     isempty(defaults.endtime) ...
	     isempty(defaults.resolution)];
if isempty(defaults.time)
  if any(ser_empty)
    error('startime, endtime AND resolution required, unless time given');
  end
  t = defaults.starttime:defaults.resolution:defaults.endtime;
  
  if t(end) == defaults.endtime
    % t(end) = [];
    t = t(1:(end-1));
  end
  
else
  if any(~ser_empty)
    error('cannot define startime, endtime, or resolution if time given');
  end
  t = defaults.time;
end

loc = getlocation(rio);
lon_rad = getgeolong(loc) * d2r;
lat_rad = getgeolat(loc) * d2r;

if isa(t, 'timespan')
  % Convert sidereal time to a timestamp
  t = lmst2ut(t, loc, timestamp('2000-01-01'));
end

jd = juliandate(t)';

sourcedata = []
for n = 1:numsources
  if isa(defaults.sources{n}, 'char')
    if ~isfield(sources, defaults.sources{n})
      error(sprintf('source ''%s'' is unknown', defaults.sources{n}));
    end
    sd = getfield(sources, defaults.sources{n});
    ra_dec = [sd.rightascension sd.declination];
    sourcedata(n).name = defaults.sources{n};
    sourcedata(n).propername = sd.propername;

  elseif isstruct(defaults.sources{n})
    % insert new source data into known sources
    
    % check all the correct fields exist
    fn = fieldnames(sources)
    tmp = [];
    for m = 1:length(fn)
      if ~isfield(defaults.sources{n}, fn{m})
	error(sprintf('source data missing field ''%s''', fn{m}));
      end
      tmp = setfield(tmp, fn{m}, getfield(defaults.sources{n}, fn{m}));
    end
    sources = setfield(sources, tmp.name, tmp);
    
  else
    error(sprintf('cannot process source data of class ''%s''', ...
		  class(defaults.sources{n})));
  end
  ra_dec_rad = ra_dec * d2r;
  az_alt_rad = horiz_coo(ra_dec_rad, jd, [lon_rad lat_rad], 'h');

  sourcedata(n).rightascension = ra_dec(1);
  sourcedata(n).declination = ra_dec(2);
  sourcedata(n).azimuth = az_alt_rad(:,1) .* r2d; 
  sourcedata(n).altitude = az_alt_rad(:,2) .* r2d; 
end
sourcedata = reshape(sourcedata, size(defaults.sources));
