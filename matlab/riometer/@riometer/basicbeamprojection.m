function [xIono, yIono] = basicbeamprojection(rio, varargin)
%PLOTBEAMPROJECTION 
% plot the projection of the beams at a height h. If h is not given then a 
% default height of 90km is used.

xIono = [];
yIono = [];

Re = earthradius; % radius of the Earth (approx 6378000m)
pi_2 = pi/2; 

[imagingBeams wideBeams] = info(rio, 'beams');
defaults.beams = [imagingBeams wideBeams];

defaults.points = 80;  % number of points used on cone of beam
defaults.height = info(rio, 'defaultheight');
defaults.units = 'm';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

numOfBeams = prod(size(defaults.beams));

loc = getlocation(rio);
lat0 = getgeolat(loc);
long0 = getgeolong(loc);

zenith = info(rio, 'zenith', defaults.beams);
azimuth = info(rio, 'azimuth', defaults.beams);
beamwidth = info(rio, 'beamwidth', 'beams', defaults.beams);

% convert azimuth from compass degrees to graph degrees
azimuth = rem(-azimuth + 90, 360);

%  convert to radians
zenith = zenith * 2 * pi / 360;
azimuth = azimuth * 2 * pi / 360;
beamwidth = beamwidth * 2 * pi / 360;

xIono = zeros(defaults.points, numOfBeams);
yIono = zeros(defaults.points, numOfBeams);

% circumscribe a circle around the beam centre (at unit distance from 
% origin) and convert it to new azimuth and zenith angles. 
r(1,:) = tan(beamwidth(1,:)/2); % beam radius
for bn = 1:prod(size(defaults.beams))
  b = defaults.beams(bn);
  [x,y,z] = sph2cart(azimuth(bn), (pi_2-zenith(1,bn)), 1);
  for i = 1:defaults.points+1
    alpha = ((2 * pi * (i-1)) / defaults.points);
    deltax = -r*sin(alpha)*sin(azimuth(bn)) - ...
	r*cos(alpha)*cos(zenith(1,bn))*cos(azimuth(bn));
    deltay =  r*sin(alpha)*cos(azimuth(bn)) - ...
	r*cos(alpha)*cos(zenith(1,bn))*sin(azimuth(bn));
    deltaz =  r * sin(zenith(1,bn)) * cos(alpha);
    [beamEdgeAzimuth, beamEdgeElevation] = ...
	cart2sph(x+deltax, y+deltay, z+deltaz);
    
    for jn =  1:prod(size(defaults.beams))
      j = defaults.beams(jn);
      d(1,jn) = Re * polarangleofray(pi_2 - beamEdgeElevation(1,jn), ...
	  defaults.height); 
    end
    xIono(i,bn) = d(1,bn) * cos(beamEdgeAzimuth(bn));
    yIono(i,bn) = d(1,bn) * sin(beamEdgeAzimuth(bn));
  end
end


switch defaults.units
  case 'km'
    % already in m
    xIono = xIono / 1e3;
    yIono = yIono / 1e3;
    
  case 'deg'
    % check whether xy2longlat wants distances at height h=0 or h=height
    [xIono yIono] = xy2longlat(xIono, yIono, defaults.height, loc);
    
  otherwise
    error(['unknown unit (was ' defaults.units ')']);
end


