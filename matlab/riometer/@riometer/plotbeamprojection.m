function h = plotbeamprojection(rio, ah, varargin)
%PLOTBEAMPROJECTION  Plot riometer beam projection
%
%   h = PLOTBEAMPROJECTION(rio, ah, ...)
%
%   h: handles of graphic objects
%   rio: RIOMETER object
%   ah: AXES handle
%
%   The following parameter name/value pairs are also accepted:
%
%     'style', ['patch' | 'line' | 'transpatch']
%     Determine if the beam outlines should be LINEs, PATCHes or fake
%     transparent patches (see GPC_TRANSPATCH/GPC_M_TRANSPATCH).
%
%     'ismmap', LOGICAL
%     Indicate if the AXES was drawn with M_MAP, and thus whether the M_MAP
%     equivalents of the drawing functions should be used. In normal use
%     PLOTBEAMPROJECTION is able to correctly identify this case with
%     ISMMAP.
%
%     'patchargs', { }
%     CELL aray of parameters passed to the PATCH, M_PATCH,
%     GPC_TRANSPATCH, GPC_M_TRANSPATCH function.
%
%     'lineargs', { }
%     CELL aray of parameters passed to the line function.
%
%     'gpcalpha', DOUBLE value, 0 <= d <= 1
%     The transparency value for GPC_TRANSPATCH and GPC_M_TRANSPATCH.
%
%     'units', ['deg' | 'aacgm' | 'm' | 'km']
%     Units used for plotting the beam projection. 
%
%   Any unknown parameters are passed onto BEAMPROJECTION, this includes
%   beams, points, height, and contourlevel.
%
% See also BEAMPROJECTION, DIRECTIVITY, GPC


if length(rio) ~= 1
  h = cell(size(rio));
  for n = 1:prod(size(rio))
    h{n} = feval(mfilename, rio(n), ah, varargin{:});
  end
  return;
end

% below this point rio is a scalar

defaults.style = 'patch'; % patch | line
defaults.ismmap = []; % defer
defaults.patchargs = {}; % defer
defaults.lineargs = {'Color', 'k', ...
		    'LineStyle', '-', ...
		    'Marker', 'none'};
defaults.units = 'deg';
defaults.gpcalpha = 0.8;

[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.ismmap)
  defaults.ismmap = ismmap(ah);
end

if isempty(defaults.patchargs)
  if defaults.ismmap
    defaults.patchargs = {'EdgeColor', 'y', ...
		    'FaceColor', 'none'};
  else
    defaults.patchargs = {'EdgeColor', 'b', ...
		    'FaceColor', 'none'};
  end
end

if 0
%%%%%%%%%%%%%%%%%%%%% DEBUG
fname = fullfile(mia_personaldir, 'beamproj.mat');
if 0
  [x y] = beamprojection(rio, 'units', defaults.units, varargin{unvi});
  disp(['saving beam projection to ' fname]);
  save(fname, 'x', 'y');
else
  disp(['loading beam projection from' fname]);
  load(fname);
end
%%%%%%%%%%%%%%%%%%%%% DEBUG (end)
else
  [x y] = beamprojection(rio, 'units', defaults.units, varargin{unvi});
  Nmax = numel(x);
  h = [];
end


switch defaults.style
 case 'patch'
  % if a scalar value for cdata is given in patchargs then make it the
  % same size as XData
  cdata = [];
  cdataSz = [1 1];
  cdataIdx = find(strcmpi('cdata', {defaults.patchargs{1:2:end}}));
  if ~isempty(cdataIdx)
    cdata = defaults.patchargs{2 * cdataIdx(end)};
  end

  for n = 1:Nmax
    if ~iscell(x{n})
      x1 = {x{n}};
      y1 = {y{n}};
    else
      x1 = x{n};
      y1 = y{n};
    end
    for m = 1:numel(x1)
      tmpx = x1{m};
      tmpy = y1{m};
      if defaults.ismmap
	[tmpx tmpy] = m_ll2xy(tmpx, tmpy);
      end
      if length(cdata) == 1
	% only repmat cdata if cdata is scalar
	cdataSz = size(tmpx); 
      end
      h(end+1) = patch('Parent', ah, ...
		       'XData', tmpx, ...
		       'YData', tmpy, ...
		       defaults.patchargs{:}, ...
		       'CData', repmat(cdata, cdataSz));
    end
  end

 case 'transpatch'
  for n = 1:Nmax
    if defaults.ismmap
      % use m_map aware version, so that background patches are plotted on
      % the clipped background, not the axes. Create GPC polygons with
      % coordinates mapped from degrees to appropriate values
      h = gpc_m_transpatch(gpc_m_map(x{n}, y{n}), ah, defaults.gpcalpha, ...
			   defaults.patchargs{:});
    else
      h = gpc_transpatch(gpc(x{n}, y{n}), ah, defaults.gpcalpha, ...
			 defaults.patchargs{:});
    end 
  end
  
 case 'line' 
  % unlike patches lines must be explicitly closed back to the start
  for n = 1:Nmax
    if ~iscell(x{n})
      x1 = {x{n}};
      y1 = {y{n}};
    else
      x1 = x{n};
      y1 = y{n};
    end
    for m = 1:numel(x1)
      if defaults.ismmap
		[tmpx tmpy] = m_ll2xy(x1{m}, y1{m});
      else
		tmpx = x1{m};
		tmpy = y1{m};
      end
      h(end+1) = line(tmpx([1:end 1]), tmpy([1:end 1]), ...
		      defaults.lineargs{:});
    end
  end
  
 otherwise
  error('Unknown style');
end

