function varargout = calcobliquity(rio, varargin)
%CALCOBLIQUITY  Calculate obliquity factors for a RIOMETER.
%
%   [r1, r2] = CALCOBLIQUITY(rio, ...)
%   r1: obliquity factors (but see below for exception)
%   r2: CHAR suitable to use for ADDPROCESSING comments
%   rio: RIOMETER object (scalar)
%
%   Calculate obliquity factors for a riometer. r may be a column vector,
%   with one element per beam number. For effective obliquity factors r
%   is a matrix, where the number of rows is equal to the number of
%   beams, and the number of columns is equal to the number of columns in
%   the absorption matrix.
%
%   The following parameter name/value pairs may be used to modify the
%   standard behaviour.
%
%     'beams', DOUBLE vector
%     The beams for which obliquity factors should be calculated.
%
%     'style', CHAR
%     Select the style of obliquity factors calculated. Valid options are
%     'none' (no obliquity factors, i.e., obliquity factors = 1),
%     'simple' (constant obliquity factors based upon the zenith angle of
%     the beam centre) and 'effective'. Effective obliquity factors are a
%     function of the true zenithal absorption. Since only apparent
%     absorption values are recorded a lookup table is used to find the
%     obliquity factor for the apparent absorption. To speed up execution
%     CALCOBLIQUITY attempts to load pre-calculated lookup tables. If found,
%     and the relevant details match (height, zenithalabsorption etc)
%     then the pre-calculated lookup table is used, otheriwse a new table
%     is calculated. See below for a method to cache lookup tables.
%
%     'height', [1x1 DOUBLE]
%     The assumed height (in metres) at which the thin absorbing layer is
%     assumed to occur. Height is used only by the 'simple' and
%     'effective' styles. Default height is taken from riometer/INFO for
%     the appropriate riometer.
%
%     'absorption', DOUBLE matrix
%     The apparent absorption values for which effective obliquity
%     factors should be calculated. The number of rows must match the
%     number of beams. Used only for effective obliquity factors.
%
%     'interpmethod', CHAR
%     The name of the method used by interp1 when performaing the table
%     lookup for effective obliquity factors. Default is 'linear'.
%
%     'zenithalabsorption', DOUBLE row vector
%     The zenithal absorption values (in dB) to use when constructing the
%     lookup table. It is not normally necessary to specify this. Used
%     only the effective obliquity factors.
%
%   A number of pseudo-styles may also be specified:
%
%     'style', 'list'
%     Returns the valid styles which may be specified. r1 contains the style
%     names and r2 contains 'pretty' strings suitable for printing into GUI
%     controls. Intended for use by NEWRIODATA, and similar GUI functions.
%
%     'style', 'saveeffective'
%     Save the effective obliquity lookup table. Intended for administrator
%     use, but advanced users can take advantage of this feature if
%     necessary. Only one table can be stored per instrument, so it is
%     sensible to store values for the default absorption height. The file
%     is saved to the standard system location (but loaded from the first
%     instance found in the matlab search PATH, so it is possible for users
%     to have a customised lookup table). A WARNING message is printed if
%     SAVE failed. CALCOBLIQUITY returns the newly created lookup table. If
%     saved manually it should be stored in a variable called
%     'obliquity_table', the default filename is available as a third return
%     value.

if length(rio) ~= 1
  error('riometer must be a scalar');
end

d2r = pi/180;


defaults.beams = [];
% defaults.style = 'simple'; % simple | effective
defaults.style = ''; 

defaults.height = [];

defaults.interpmethod = 'linear';
defaults.absorption = [];
% true zenithal absorption values used when calculating the effective
% obliquity table 
defaults.zenithalabsorption =  [0.0001 0.01:0.01:0.1 0.15:0.05:0.5 ...
		    0.6:0.1:5 5.5:0.5:30];
[defaults unvi] = interceptprop(varargin, defaults);




if isempty(defaults.height)
  defaults.height = info(rio, 'defaultheight');
end
h = defaults.height;

numOfBeams = prod(size(defaults.beams));


fname = sprintf('rio_obliquity_%s_%d.mat', getabbreviation(rio), ...
		getserialnumber(rio));

switch defaults.style
  % ---------------------------------------
 case ''
  [tmp mfname] = fileparts(mfilename);
  styles = feval(mfname, rio, 'style', 'list');
  error(sprintf('style not specified, valid styles are %sand ''%s''.', ...
		sprintf('''%s'', ', styles{1:(end-1)}), ...
		styles{end}));

  % ---------------------------------------
 case 'none'
  % no-brainer as to why we were asked to do this, but support it anyway
  varargout{1} = ones(numOfBeams, 1);
  varargout{2} = sprintf('obliquity: %s', defaults.style);
  
  % ---------------------------------------
 case 'standard'
   varargout{1} = info(rio, 'standardobliquity', defaults.beams);
   varargout{2} = sprintf('obliquity: %s', defaults.style);
   
  % ---------------------------------------
 case 'simple'
  % r = info(rio, 'obliquity', 'beams', beams);
  r = zeros(numOfBeams, 1);
  
  z = reshape(info(rio, 'zenith', defaults.beams) * d2r, ...
	      [numOfBeams 1]);
  alpha = polarangleofray(z, defaults.height);
  r = 1 ./ cos(z-alpha); % with curvature of ionosphere

  varargout{1} = r;
  varargout{2} = sprintf('obliquity: %s (height = %.3g km)', ...
			 defaults.style, defaults.height/1e3);
    
  % ---------------------------------------
 case 'effective'
  
  % check the absorption data size is correct
  if size(defaults.absorption, 1) ~= numOfBeams
    error(['number of rows in absorption matrix must match number of ' ...
	   'beams']);
  end
  obliquity_table = [];
  if exist(fname, 'file') 
    load(fname);
    % now have obliquity_table loaded, check the various fields are
    % appropriate and we can use the pre-calculated values. Remember that
    % EFFECTIVEOBLIQUITYTABLE inserts a column for negative absorption
    % values, so don't include that when comparing zenithalabsorption
    % values. See comments at end of EFFECTIVEOBLIQUITYTABLE for reasons
    % why it is done.    
    if ~isequal(obliquity_table.zenithalabsorption(2:end), ...
		defaults.zenithalabsorption) | ...
	  ~isequal(defaults.height, obliquity_table.height) | ...
	  ~isempty(unvi)
      % reqd parameters are not all the same, or geodesicmesh or
      % azimuth/zenith specified
      obliquity_table = []; % recalculate
    end
  end
  if isempty(obliquity_table)
    disp('creating effective obliquity lookup table');
    obliquity_table = ...
	effectiveobliquitytable(rio, ...
				'beams', defaults.beams, ...
				'height', defaults.height, ...
				'zenithalabsorption', ...
				defaults.zenithalabsorption, ...
				varargin{unvi});
  end
  
  r = zeros(size(defaults.absorption));
  % do table lookup
  for n = 1:numOfBeams
    bi = find(defaults.beams(n) == obliquity_table.beams);
    r(n, :) = interp1(obliquity_table.apparentabsorption(bi, :), ...
		      obliquity_table.obliquity(bi, :), ...
		      defaults.absorption(n, :), ...
		      defaults.interpmethod);
  end

  varargout{1} = r;
  varargout{2} = sprintf('obliquity: %s (height = %.3g km)', ...
			 defaults.style, defaults.height/1e3);
  
  % ---------------------------------------
 case 'saveeffective'
  [imagingBeams wideBeams] = info(rio, 'beams');

  obliquity_table = ...
	effectiveobliquitytable(rio, ...
				'beams', union(imagingBeams, wideBeams), ...
				'height', defaults.height, ...
				'zenithalabsorption', ...
				defaults.zenithalabsorption, ...
				varargin{unvi});
  fname2 = fullfile(mia_basedir, 'riometer', fname);
  disp(sprintf('saving to %s', fname));
  cmd = 'save(fname2, ''obliquity_table'');';
  errcmd = 'warning(''cannot save, do you have admin access?'')';
  eval(cmd, errcmd);
  varargout{1} = obliquity_table;
  varargout{2} = fname;
  
  % ---------------------------------------
 case 'list'
  % for GUI functions it is useful to report which styles are valid
  varargout{1} = {'simple', 'effective', 'none'};
  varargout{2} = {'Simple', 'Effective', 'None'};
  
  
  % ---------------------------------------
 otherwise
  error(sprintf('unknown style (was %s)', defaults.style));
  
end


