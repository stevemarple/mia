function r = info_rio_ram_3(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

val = 1;
switch req
 case 'arcombeammap' % ------------------------
  % This indicates where in the arcom data array each beam is
  % located
  r.varargout{1} = 1:64;
    
 case 'calibrationsignalstrength' % ------------------------
  % This is the calibration signal strength in dBm
  % varargin{1} is time (timestamp)
  % varargin{2} is beams
  % varargin{3} is units
  if length(varargin) < 3
    error('require start time of file, beams and units');
  end
  load info_rio_ram_2_calibrationsignalstrength;
  disp(sprintf('fix %s: calibrationsignalstrength is guessed!', mfilename));
  calibrationsignalstrength.data = 2.1e5;
  r.varargout{1} = repmat(2.1e5, size(varargin{2}));
  switch varargin{3}
   case 'raw'
    ; % do nothing
   case 'dBm'
    r.varargout{1} = local_converttodbm(in, r.varargout{1}, varargin{1});
   otherwise
    error(sprintf('units must be raw or dBm (was ''%s'')', varargin{3}));
  end
  
 case 'calibrationwindow' % ------------------------
  % for calibration use the 3rd to last samples in the calibration sequence
  r.varargout{1} = [3 inf];

 case 'converttodbm' % ------------------------
  % convert some data values to dbm
  % varargin{1} is data
  % varargin{2} is time (timestamp)
  
  if length(varargin) ~= 2
    error('require start time of file')
  end
  r.varargout{1} = local_converttodbm(in, varargin{:});
  
 case 'dbmconversion' % ------------------------
  % varargin{1} is time (timestamp)
  if length(varargin) < 1
    error('require start time of file')
  end

  r.varargout{1} = -205 + 10 * log10(1.4e6);
 
 case 'defaultcalibrationmethod'
  r.varargout{1} = 'none';

 case 'includesequencenumber' % ------------------------
  % modify a strftime format specifier to include the sequence number
  if length(varargin) ~= 1
    error('require format specifier (only)');
  end
    [fstr_path fstr_file fstr_ext] = fileparts(varargin{1});
  fstr_file = strrep(fstr_file, '_000_f', '_%%03d_f');
  r.varargout{1} = fullfile(fstr_path, [fstr_file fstr_ext]);
   
 case 'preferredbeam'
    defaults.beams = [s.widebeams s.imagingbeams];
  
  % ignore a time option here but accept as may be used by a function
  % specific to one riometer
  defaults.time = []; 
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});

  if any(defaults.beams == 15) | isempty(defaults.beams)
    % use X15 if present
    r.varargout{1} = 15;
  elseif any(defaults.beams == 47) 
    % use Y15 if present
    r.varargout{1} = 47;
  else
    r.varargout{1} = defaults.beams(1);
  end
    
 case 'tapering' % ------------------------
  % refer to rio_ram_2 for tapering details
  [r.varargout{1:2}] = info(rio_ram_2, 'tapering', varargin{:});
  
 
 
 otherwise % ------------------------
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;

function r = local_converttodbm(in, data, t)
data(data == 0) = nan; % avoid taking logs of zero
r = 10 .* log10(data) + info(in, 'dbmconversion', t);

