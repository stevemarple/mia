function r = char(in, varargin)
%CHAR  Convert a RIOMETER object into a CHAR representation.
%
%   r = CHAR(in)
%   in: RIOMETER object
%   r: CHAR representation.
%
%   See also mia_instrument_base/CHAR.

% NB Make all objects derived from mia_instrument_base print a trailing
% newline character 

if length(in) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_instrument_base_char(in);
  return;
end

if length(varargin)
  r = mia_instrument_base_char(in, varargin{:});
  return
end


freq_str = '';
for n = 1:prod(size(in.frequency))
  if n > 1
    space = ' ';
  else
    space = '';
  end
  if isnan(in.frequency(n))
    tmp = '?';
  else
    tmp = printunits(in.frequency(n), 'Hz');
  end
  freq_str = [freq_str space tmp];
end

r = sprintf(['%s' ...
	     'frequency         : %s\n'], ...
 	    mia_instrument_base_char(in), freq_str);
%	    char(in.mia_instrument_base), freq_str);




