function r = info_rio_mai_2(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

val = 1;

% there were 1.4e6 samples per second when this value was estimated 
% dbmconversion = -199 + 10 * log10(1.4e6);
dbmconversion = -213 + 10 * log10(1.4e6);

commoncalibrationdata = false;

switch req
 case 'arcombeammap'
  % This indicates where in the arcom data array each beam is located. The
  % widebeam values are appended onto the imaging beam values in
  % arcom_file_to_mia.
  r.varargout{1} = [16     4     8    12 ...
		    13     1     5     9 ...
		    14     2     6    10 ...
		    15     3     7    11 ...
		    17:32];
   
 case 'calibrationbeams'
  % r.varargout{1} = [6 7 10 11];
  % r.varargout{1} = 17:32; % average all widebeam values
  r.varargout{1} = cell(1, 32);
  r.varargout{1}(1:16) = {[17:32]};
  for n = 17:32
    r.varargout{1}{n} = n;
  end
  
  
 case 'calibrationsignalstrength'
  % This is the calibration signal strength in appropriate units.
  defaults.time = [];
  defaults.beams = [];
  defaults.units = '';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  if isempty(defaults.beams)
    error('require beams');
  elseif isempty(defaults.time)
    error('require time');
  elseif ~isa(defaults.time, 'timestamp')
    error('time must be a timestamp')
  end
  
  
  if defaults.time >= timestamp([2010 02 10 18 0 0])
    % Example of how to update the calibration signal strength at a later
    % date.
    r.varargout{1} = 3350000; % approximate!
  else
    % Calculated 2010-02-11 by Steve Marple using data from 2010-02-04 to
    % 2010-02-10. Calibration signal strength was set to high.
    r.varargout{1} = 5120216;
  end
  
  if ~commoncalibrationdata
    % Need to replicate for each beam
    r.varargout{1} = repmat(r.varargout{1}, numel(defaults.beams), 1);
  end
  
  switch defaults.units
   case ''
    error('require units');
   case 'raw'
    ; % do nothing
   case 'dBm'
    mia = rio_rawpower('starttime', defaults.time, ...
                       'instrument', in, ...
		       'data',  r.varargout{1}, ...
		       'units', 'raw');
    mia2 = rio_power(mia);
    r.varargout{1} = getdata(mia2);
   otherwise
    error(sprintf('unknown units (was ''%s'')', defaults.units));
  end
  
 case 'calibrationwindow'
  % for calibration use the 3rd to last samples in the calibration sequence
  % r.varargout{1} = [3 inf];
  r.varargout{1} = [-inf inf];
  
 case 'commoncalibrationdata'
  r.varargout{1} = commoncalibrationdata;

 case 'dbmconversion'
  % This is the correction to get the dBm value of the uncalibrated data
  % approximately correct. See also 'calibrationsignalstrength'
  
  % varargin{1} is time (timestamp)
  if length(varargin) < 1
    error('require start time of file'); % but ignored at present
  end
  r.varargout{1} = dbmconversion;

 case 'defaultcalibrationmethod'
  r.varargout{1} = 'cubic';

 case 'includesequencenumber'
  % modify a strftime format specifier to include the sequence number
  if length(varargin) ~= 1
    error('require format specifier (only)');
  end
    [fstr_path fstr_file fstr_ext] = fileparts(varargin{1});
  fstr_file = strrep(fstr_file, '_000_f', '_%%03d_f');
  r.varargout{1} = fullfile(fstr_path, [fstr_file fstr_ext]);
    
 case 'tapering'
  taper = ones(1, 8);
  r.varargout{1} = taper;
  r.varargout{2} = taper;
 
 case 'preferredbeam'
  defaults.time = [];
  defaults.beams = union(s.imagingbeams, s.widebeams);
  [defaults unvi] = interceptprop(varargin, defaults);
  
 otherwise
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
