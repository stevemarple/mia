function [r,az,zen,fname] = antennagain(rio, varargin)
%ANTENNAGAIN Calculate the gain of a RIOMETER antenna.
%
%   [d az zen fn] = ANTENNAGAIN(rio, ...)
%
%   rio: RIOMETER object
%   d: gain matrix (CELL matrix) in linear units (not dB)
%   az: azimuth matrix (radians)
%   zen: zenith matrix (radians)
%   fn: filename of standard gain matrix (for admin use)
%
% Calculate the antenna gain of a riometer antenna system. ANTENNAGAIN
% automatically calls the appropriate directivity function (e.g.,
% IRISDIRECTIVITY) based on the 'systemtype' option in riometer/INFO. The
% gain matrix is a CELL matrix, where the number of elements corresponds to
% the number of beams specified. Each element is the same size as the
% azimuth/zenith matrices. Directivity is not a valid concept for ARIES
% systems, ANTENNAGAIN uses ARIESGAIN in this case.
%
% The following parameter name/value pairs are accepted:
%
%   'azimuth', [DOUBLE]
%   'zenith', [DOUBLE]
%   The azimuth/zenith angles for which the gain should be
%   calculated. The matrices must be the same size.
%
%   'geodesicmesh', [TRIANGLE]
%   A matrix of TRIANGLE objects which define a geodesic mesh. This is an
%   alternative method of supplying the azimuth/zenith angles. The vertices
%   of the triangles are used as the azimuth and zenith values for the
%   computation of antenna gain. This allow a mesh which is more
%   uniform than by specifying an azimuth/zenith 'grid'.
%    
%   'beams', [DOUBLE]
%   The beam number for which the gain should be calculated.   
%
%   'loadifpossible', LOGICAL
%   A boolean to indicate if the gain matrix should be loaded if
%   possible, rather than calculated. Defaults to 1, and should not be
%   needed except by riometer/MAKEGAIN.
%
%   'verbose', LOGICAL
%   Flag to turn off verbose message. If enabled prints beam number being
%   processed. Default is 0.
%
% The azimuth and zenith angles used for the calculation are returned as
% additional parameters, this is particularly useful when the azimuth and
% zenith angles are not specified. Specifying azimuth/zenith angles will
% prevent the pre-computed antenna gain values from being loaded.
%
% See also riometer/INFO, MAKEANTENNAGAIN.

if length(rio) ~= 1
  error('rio must be scalar');
end


[imagingBeams wideBeams] = info(rio, 'beams');
antennatype = info(rio, 'antennatype');
systemtype =  info(rio, 'systemtype');


% method of specifying azimuth and zenith
defaults.geodesicmesh = []; 


defaults.azimuth = []; % radians
defaults.zenith = []; % radians
defaults.units = 'radians';

defaults.beams = [];

defaults.loadifpossible = 1;
defaults.verbose = 0;

defaults.taperid = [];

[defaults unvi] = interceptprop(varargin, defaults);
% warnignoredparameters(varargin{unvi(1:2:end)});

d2r = pi / 180;
r2d = 180 / pi;
switch defaults.units
 case {'deg' 'degrees'}
  defaults.azimuth = defaults.azimuth * d2r;
  defaults.zenith = defaults.zenith * d2r;
 case {'rad' 'radians'}
  ; % do nothing
 otherwise
  error(sprintf('unknown units (was''%s'')', defaults.units));
end

bsz = size(defaults.beams);
tri = [];
fname = sprintf('rio_antennagain_%s_%d.mat', getabbreviation(rio), ...
		getserialnumber(rio));

if ~isempty(defaults.geodesicmesh)
  % use geodesic mesh to calculate
  tri = defaults.geodesicmesh;
  [az zen] = localGetAzZen(tri);

elseif isequal(defaults.azimuth, defaults.zenith, [])
  % both empty, use default settings
  if exist(fname, 'file') & logical(defaults.loadifpossible)
    disp(sprintf('loading %s', fname));
    load(fname);
    % now have r, az, zen, and beams variables loaded
    
    % check for missing beams
    miss = setdiff(defaults.beams, beams);
    if ~isempty(miss)
      error(sprintf('beam #%d not present\n', miss));
    end

    % need beam index, but can't use rio_base/getbeamindex because the
    % object is a riometer, not something derived from rio_base
    [tmp bi] = intersect(beams, defaults.beams); 

    r = reshape({r{bi}}, bsz);
    
    if any(strcmp(defaults.units, {'deg', 'degrees'}))
      az = az * r2d;
      zen = zen * r2d;
    end
    return;
  else
    if logical(defaults.loadifpossible)
      warning(sprintf(['%s not found, creating gain details ' ...
		       'instead of loading them (please wait)'], fname));
    end
    tri = geodesicmesh('polyhedron', 'icosahedron', 'recursiondepth', 5);
    [az zen] = localGetAzZen(tri);
  end
  
  
elseif isequal(size(defaults.azimuth), size(defaults.zenith))
  az = defaults.azimuth;
  zen = defaults.zenith;
else
  error('azimuth and zenith matrices are not equal sizes');
end

r = cell(bsz);

for bn = 1:prod(bsz)
  beam = defaults.beams(bn);
  if defaults.verbose
    disp(sprintf('processing beam %d', beam));
  end
  
  if ~isempty(intersect(beam, imagingBeams))
    % ----- imaging beam -----
    switch systemtype
     case 'iris'
      % r = irisgain(rio, zen, az, beam, varargin{:});
      r{bn} = abs(irisdirectivity(rio, zen, az, beam, varargin{:}));
      r{bn} = power(r{bn}, 2);
      
     case 'aries'
      r{bn} = ariesgain(rio, zen, az, beam, ...
			'taperid', defaults.taperid, ...
			varargin{unvi});
  
     otherwise
      db = dbstack;
      mf = db(1).name
      error(sprintf('unknown system, type (was %s). Please fix %s', ...
		    systemtype, mf));
      
    end  
    
  elseif ~isempty(intersect(beam, wideBeams))
    % ----- wide beam -----
    if isempty(info(rio, 'zenith'))
      warning('zenith info missing, assuming widebeam is vertical');
      
    elseif info(rio, 'zenith', beam) ~= 0
      error(sprintf(['widebeam not vertical, cannot calculate (beam #' ...
		     ' %d)'], beam));
    end
    
    switch antennatype{2}
     case 'crossed-dipole'
      % r = turnstilegain(zen, az);
      r{bn} = crosseddipoledirectivity(zen, az);
      r{bn} = power(r{bn}, 2);
      
     case 'la jolla'
      r{bn} = lajolladirectivity(zen, az);
      r{bn} = power(r{bn}, 2);
      
     otherwise
      db = dbstack;
      mf = db(1).name
      error(sprintf('unknown antenna type (was %s). Please fix %s', ...
		    antennatype{2}, mf));
    end
    
  else
    % ----- ooops -----
    error(sprintf('beam #%d is not an imaging beam or a widebeam', ...
		  beam));
  end
end

if any(strcmp(defaults.units, {'deg', 'degrees'}))
  az = az * r2d;
  zen = zen * r2d;
end


% -------------------------------------
function [az, zen] = localGetAzZen(tri)

% take only values above the horizon!
[x y z] = vertices(tri);

x(x == 0) = eps;
y(y == 0) = eps;
mask = (z > 0);

[az elev] = cart2sph(x(mask), y(mask), z(mask));
zen = pi/2 - elev; % convert elevation to zenith angle

