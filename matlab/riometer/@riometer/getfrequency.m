function r = getfrequency(in)
%GETFREQUENCY  Return the RIOMETER operating frequency.
%
%   r = GETFREQUENCY(in)
%   r: operating frequency (Hz)
%   in: RIOMETER object
%
%   See also RIOMETER.

r = zeros(size(in));
for n = 1:numel(in)
  % matlab 5.1 does not like () and . subscripts simultanteously
  tmp = in(n);
  if isempty(tmp.frequency)
    r(n) = nan;
  else
    r(n) = tmp.frequency;
  end
end

 
