function r = calculatebeamparameters(rio, varargin)
%CALCULATEBEAMPARAMETERS Calculate riometer beam parameters
%
%   r = CALCULATEBEAMPARAMETERS(rio, ...)
%   r: STRUCT with azimuth, zenith and beams
%   rio: RIOMETER object
%
% CALCULATEBEAMPARAMETERS calculates the beam details for a given riometer
% and returns values suitable for use by the INFO function or for insertion
% into the riometers database. Azimuths are returned as 'compass' degrees
% (ie 0 is North and 90 is East). The following name/value pairs may be used
% to modify the default behaviour:
%
%   'antennaazimuth', DOUBLE
%   The pointing direction for the antenna array. The default value is
%   taken from INFO(rio, 'antennaazimuth'). Note that the direction
%   should be given as a compass angle in degrees (ie 0 is North and 90
%   is East).
%
%   'beams', [1xn DOUBLE]
%   By default parameters for all imaging beams are calculated.
%
% Any unknown name/value pairs are passed directly to
% riometer/DIRECTIVITY, thereby allowing the default azimuth and zenith
% angle to be modified. See riometer/DIRECTIVITY for more details.
%
% See also mia_instrument_base/INFO, riometer/DIRECTIVITY.

if numel(rio) ~= 1
  error('riometer object must be scalar');
end

d2r = pi/180;
r2d = 180/pi;

[ib wb] = info(rio, 'beams');


% Riometer parameters
defaults.beams = [ib wb];
defaults.antennaazimuth = info(rio, 'antennaazimuth');

% step sizes in degrees
defaults.azimuthstep = 0.1; 
defaults.zenithstep = defaults.azimuthstep;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

r.beams = defaults.beams;
r.azimuth = zeros(size(r.beams));
r.zenith = zeros(size(r.beams));

% az1, zen1 are 1D, az2, zen2 are 2D
az1 = [0:defaults.azimuthstep:(360-defaults.azimuthstep)] * d2r;
zen1 = [0:defaults.zenithstep:90]' * d2r;
az2 = repmat(az1(:)', numel(zen1),1 );
zen2 = repmat(zen1(:), 1, numel(az1));



[d az zen] = directivity(rio, ...
			 'loadifpossible', 0, ...
			 'verbose', 1, ...
			 'beams', defaults.beams, ...
			 'azimuth', az2, ...
			 'zenith', zen2);

% Return azimuth and zenith matrices used by the directivity function
r.directivityazimuth = az;
r.directivityzenith = zen;

% Convert to column vectors
% az = az(:);
% zen = zen(:);

for n = 1:numel(defaults.beams)
  % d0 = abs(d{n}(:));
  d0 = abs(d{n});
  [d0max idx] = max(d0(:));
  % convert from trig angles to compass angles, and then allow for
  % pointing direction of the antenna
  r.azimuth_rad(n) = az(idx);
  r.zenith_rad(n) = zen(idx);

  r.directivity{n} = d0;
  % calculate -3dB contour
  gain = power(d0, 2);
  maxGain = max(gain(:));
  gainDB = 10 * log10(gain ./ maxGain);  % dB

  beamedge = contour2cell(contourc(az1, zen1, gainDB, [-3 -3]));
  
  
  % For each location on the contour find the angle between that location
  % and the beam centre
  for m = numel(beamedge)

    % For this contour take the mean value of the angles; double the width
    % (for side-to-side) and convert to degrees
    widths = mean(local_angle_between(r.azimuth_rad(n), r.zenith_rad(n), ...
				     beamedge{m}.x, beamedge{m}.y)) ...
	     .* 2 .* r2d;
    
    % disp(sprintf('MIN = %f, MAX=%f', min(widths), max(widths)));
    bw(m) = mean(widths);
  end
  % Take the smallest of the mean angles between the beam centre and the
  % contour edge (others will be side lobes), multiply by 2 to get width
  % from side-to-side and convert to degrees
  r.beamwidth(n) = min(bw);
end

% Adjust for antenna pointing direction
r.azimuth_rad = r.azimuth_rad - (defaults.antennaazimuth * d2r);

% convert radians version to 'compass' (not 'trigonometric') degrees
r.azimuth = -(r.azimuth_rad * r2d) + (90 + defaults.antennaazimuth);

% Wrap azimuth into +/- 180 degrees
idx2 = (r.azimuth > 180);
r.azimuth(idx2) =  r.azimuth(idx2) - 360;
idx3 = (r.azimuth <= -180);
r.azimuth(idx3) =  r.azimuth(idx3) + 360;



r.zenith = r.zenith_rad * r2d;

function r = local_angle_between(azA, zenA, azB, zenB)

% Convert from spherical to cartesian coordinates
[xA, yA, zA] = sph2cart(azA, pi/2 - zenA, 1);
[xB, yB, zB] = sph2cart(azB, pi/2 - zenB, 1);

% Calculate dot product
dp = (xA .* xB) + (yA .* yB) + (zA .* zB);

% Since all vectors lie on the unit cicle (ie magnitude = 1) the
% dotproduct is cos(theta)
r = acos(dp);

