function in = riometer(varargin)
%RIOMETER  Constructor for RIOMETER class.
%
%   r = RIOMETER
%
%   See also MIA_INSTRUMENT_BASE.

cls = 'riometer';

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

% in.frequency = []; % 
in.frequency = nan; % 

if nargin == 0
  % default constructor
  ib = mia_instrument_base;
  in = class(in, cls, ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  in = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = mia_instrument_base(varargin{unvi});
  in = class(in, cls, ib);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;
