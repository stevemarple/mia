function h = plotfov(in, ah, varargin)
%PLOTFOV  Plot a RIOMETER's field of view.
%
%   Overloaded version for RIOMETER. See mia_instrument_base/PLOTFOV.

defaults.units = 'm_antenna';
[defaults unvi] = interceptprop(varargin, defaults);
h = mia_instrument_base_plotfov(in, ah, varargin{unvi}, ...
				'units', defaults.units);
