function r = info_rio_kil_1(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

allbeams = union(s.imagingbeams, s.widebeams);

val = 1;
switch req
 case 'preferredbeam'
  defaults.time = [];
  defaults.beams = allbeams;
  [defaults unvi] = interceptprop(varargin, defaults);
  
  if ~isempty(defaults.time) & r.varargout{1} == 50
    % during this period the widebeam riometer failed, so use the most
    % zenithal beam instead
    if defaults.time >= timestamp([2003 4 10 0 0 0]) & ...
	  defaults.time < timestamp([2099 1 1 0 0 0])
      % call info again, but this time exclude beam 50 from the possible
      % beams
      r.varargout{1} = info(in, req, varargin{:}, ...
			  'beams', setdiff(defaults.beams, 50));
      disp(sprintf(['preferred beam is #%d as widebeam riometer ' ...
		    'was not functioning'], r.varargout{1}));
    end
  end
  
 otherwise
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
