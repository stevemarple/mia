function r = norstar2mia(rio, filename)
%NORSTAR2MIA  Convert Norstar ASCII data files to RIO_ABS objects

[t abs volts] = textread(filename, '%f %f %f', 'commentstyle','shell')
