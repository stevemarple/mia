function varargout = scintillation_calculator(rio, varargin)
%SCINTILLATION_CALCULATOR  Predict when scintillation might occur.
%
%   [mask val t sourcedata] = SCINTILLATION_CALCULATOR(rio, ...)
%
%   mask: LOGICAL mask indicating when scintillation might occur
%   val: angular distance or antenna gain values
%   t: vector of TIMESTAMP objects
%   sourcedata: STRUCT containing details of radio sources selected
%   rio: RIOMETER object
%   ...: additional parameter name/value pairs required, see below
%
%
%   sourcedata = SCINTILLATION_CALCULATOR(rio, 'listsources')
%   sourcedata: STRUCT containing details of known radio sources
%   rio: any riometer object (even a blank one)
%   If no return argument is given the known sources are printed.
%
%   methods = SCINTILLATION_CALCULATOR(rio, 'listmethods')
%   methods: CELL array listing the valid methods
%   rio: any riometer object (even a blank one)
%
% The default behaviour can be modified by the following name/value
% pairs:
%
%   'time', TIMESTAMP
%   TIMESTAMP vector detailing when the calculation(s) should be
%   made. Required unless starttime/endtime/resolution supplied.
%
%   'starttime', TIMESTAMP
%   'endtime', TIMESTAMP
%   'resolution', TIMESPAN
%   Scalar indicating the times for which the calculations should be made.
%
%   'beams', DOUBLE (vector)
%   The riometer beams for which calculations should be made. Defaults to
%   all beams if omitted or empty matrix given.
%
%   'method', CHAR
%   Method of operation. Valid arguments are 'angulardistance',
%   'antennagain', 'antennagainmainlobe' and
%   'negativesidelobe'. 'angulardistance' is the angular distance between a
%   radio source and the beam centre.  'antennagain' is the antenna gain
%   relative to gain in the beam pointing direction.  'antennagainmainlobe
%   is similar to 'antennagain' but the source must also pass through the
%   main lobe only, determined by the source passing within the -3dB gain
%   contour. 'negativesidelobe' only applied to Mills cross riometers and
%   determines when the source(s) pass through negative sidelobes. See also
%   'threshold'.
%
%   'threshold', DOUBLE (scalar or vector)
%   The threshold value used when predicting scintillation. For
%   method=angulardistance it is the angular distance in degrees (default is
%   half of the riometer beamwidth). For method=antennagain is the antenna
%   gain relative to the maximum antenna gain in dB (default is -3dB). If
%   the threshold is a scalar that value is used for all beams, if a vector
%   it must be the same length as the number of beams, and each value is
%   applied to the corresponding beam.
%
%   'sources', CELL
%   Select radio sources used in the calculation. Note that if multiple
%   sources are selected the same threshold is used for each source. The
%   CELL array should contains either CHARs, which are the shorthand names
%   of known sources (from listsources), [1x2] numeric data (right ascension
%   and declination, in degrees), or STRUCTs of the same form returned by
%   listsources).
%
%   'print', LOGICAL
%   Print scintillation details, or not. Defaults to false when NARGOUT=0
%   and true otherwise.
%
%   'plot', LOGICAL
%   For debugging purposes plot the startracks.
%
% See also MIA_FILTER_REMOVE_SCINTILLATION.


% multipliers to convert degrees to radians, and back
d2r = (pi/180);
r2d = (180/pi);
pi_2 = pi / 2;

mask = logical([]);

% known radio sources (right ascension, declination [degrees])

% http://www.seds.org/~spider/spider/Vars/casA.html
% sources.cassiopeia_a = [ra2deg(23, 23.4, 0), degms2deg(58, 50, 0)];
sources.cassiopeia_a.name = 'cassiopeia_a';
sources.cassiopeia_a.propername = 'Cassiopeia A';
sources.cassiopeia_a.rightascension = ra2deg(23, 23.4, 0);
sources.cassiopeia_a.declination = degms2deg(58, 50, 0);


% http://www.klima-luft.de/steinicke/Artikel/cyga/cyga_e.htm
sources.cygnus_a.name = 'cygnus_a';
sources.cygnus_a.propername = 'Cygnus A';
sources.cygnus_a.rightascension = ra2deg(19, 59, 28.3);
sources.cygnus_a.declination = degms2deg(40, 44, 2);

% http://en.wikipedia.org/wiki/Sagittarius_A%2A
sources.sagittarius_astar.name = 'sagittarius_astar';
sources.sagittarius_astar.propername = 'Sagittarius A*';
sources.sagittarius_astar.rightascension = ra2deg(17, 45, 40.045);
sources.sagittarius_astar.declination = -29.00775;

% http://www.absoluteastronomy.com/stars/polaris.htm
sources.polaris.name = 'polaris';
sources.polaris.propername = 'Polaris';
sources.polaris.rightascension = ra2deg(2, 31.812, 0);
sources.polaris.declination = degms2deg(89, 15.850, 0);

validmethods = {'angulardistance' 'antennagain' 'antennagainmainlobe' ...
		'negativesidelobe'};
% for timestamped data
defaults.time = [];

% for starttime/endtime and resolution data
defaults.starttime = [];
defaults.endtime = [];
defaults.resolution = [];

defaults.beams = [];
defaults.taperid = [];

defaults.sources = {};


defaults.method = ''; 
defaults.mode = ''; % method was called mode
defaults.threshold = [];

defaults.print = (nargout == 0);

% For debugging purposes.
defaults.plot = 0;

% list the known sources
if nargin > 1
  if strcmp(varargin{1}, 'listsources')
    fn = sort(fieldnames(sources));
    maxlength = 0;
    for n = 1:numel(fn)
      maxlength = max(length(fn{n}), maxlength);
    end
    
    if nargout
      varargout{1} = sources;
    else
      % No return argument, so print
      for n = 1:numel(fn)
	sd = getfield(sources, fn{n});
	disp(sprintf('%*s: %g, %g', maxlength+2, ...
		     fn{n}, sd.rightascension, sd.declination));
      end
    end
    
    return
  elseif strcmp(varargin{1}, 'listmethods')
    varargout{1} = validmethods;
    return
  end 
end


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~isempty(defaults.mode)
  warning('mode is now called method');
  if isempty(defaults.method)
    defaults.method = defaults.mode;
    defaults.mode = '';
  else
    error('cannot set both mode and method');
  end
end

if isempty(defaults.sources)
  % error('no sources specified');
  defaults.sources = {'cassiopeia_a', 'cygnus_a', 'sagittarius_astar'};
end

numsources = numel(defaults.sources);

if isempty(defaults.beams)
  defaults.beams = info(rio, 'allbeams');
end

numbeams = numel(defaults.beams);

ser_empty = [isempty(defaults.starttime) ...
	     isempty(defaults.endtime) ...
	     isempty(defaults.resolution)];


if isempty(defaults.time)
  if any(ser_empty)
    error('startime, endtime AND resolution required, unless time given');
  end
  usetime = 0;
  t = defaults.starttime:defaults.resolution:defaults.endtime;
  
  if t(end) == defaults.endtime
    % t(end) = [];
    t = t(1:(end-1));
  end
  
else
  if any(~ser_empty)
    error('cannot define startime, endtime, or resolution if time given');
  end
  t = defaults.time;
  usetime = 1;
end

if ~isempty(defaults.method) 
  switch numel(defaults.threshold)
   case 0
    ; %  set later
    
   case 1
    defaults.threshold = repmat(defaults.threshold, numbeams, 1);
    
   case numbeams
    ;
    
   otherwise
    error(['threshold must be a scalar or a vector equal to the number of' ...
	   ' beams']); 
  end
end

% lmst = ut2lmst(t, getlocation(rio));
loc = getlocation(rio);
lon_rad = getgeolong(loc) * d2r;
lat_rad = getgeolat(loc) * d2r;

if isa(t, 'timespan')
  % Convert sidereal time to a timestamp
  t = lmst2ut(t, loc, timestamp('2000-01-01'));
end
numt = numel(t);

jd = juliandate(t)';

sourcedata = [];

for n = 1:numsources
  if isnumeric(defaults.sources{n})
    if ~isequal(size(defaults.sources{n}), [1 2])
      error('numeric source data incorrect size');
    end
    ra_dec = defaults.sources{n};
    sourcedata(n).name = '';
    sourcedata(n).propername = sprintf('Unknown (%.1f, %.1f)', ...
				       defaults.sources{n});
    
  elseif isa(defaults.sources{n}, 'char')
    if ~isfield(sources, defaults.sources{n})
      error(sprintf('source ''%s'' is unknown', defaults.sources{n}));
    end
    sd = getfield(sources, defaults.sources{n});
    ra_dec = [sd.rightascension sd.declination];
    sourcedata(n).name = defaults.sources{n};
    sourcedata(n).propername = sd.propername;

  elseif isstruct(defaults.sources{n})
    % insert new source data into known sources
    
    % check all the correct fields exist
    fn = fieldnames(sources)
    tmp = [];
    for m = 1:length(fn)
      if ~isfield(defaults.sources{n}, fn{m})
	error(sprintf('source data missing field ''%s''', fn{m}));
      end
      tmp = setfield(tmp, fn{m}, getfield(defaults.sources{n}, fn{m}));
    end
    sources = setfield(sources, tmp.name, tmp);
    
  else
    error(sprintf('cannot process source data of class ''%s''', ...
		  class(defaults.sources{n})));
  end

  ra_dec_rad = ra_dec * d2r;
  az_alt_rad = horiz_coo(ra_dec_rad, jd, [lon_rad lat_rad], 'h');

  sourcedata(n).rightascension = ra_dec(1);
  sourcedata(n).declination = ra_dec(2);
  sourcedata(n).azimuth = az_alt_rad(:,1) .* r2d; 
  sourcedata(n).altitude = az_alt_rad(:,2) .* r2d; 
end

sourcedata = reshape(sourcedata, size(defaults.sources));

% plot the star tracks for debugging purposes
if defaults.plot
  [fh ah] = makeplotfig('init', 'title', 'Star tracks');
  colororder = get(gca, 'ColorOrder');
  legend_str = cell(1, numsources);
  for n = 1:numsources
    cn = 1 + rem(n-1, size(colororder, 1)-1);
    plot(sourcedata(n).azimuth, sourcedata(n).altitude, ...
	 'Color', colororder(cn,:));
    legend_str{n} = sourcedata(n).propername;
  end
  
  % rotate colors
  colororder = colororder([2:end 1], :);
  set(gca, 'ColorOrder', colororder);
  lh = legend(legend_str{:});
  set(findall(lh, 'Type', 'text'), 'Interpreter', 'none');
end


mask = logical(zeros(numbeams, numt));

beam_az = info(rio, 'azimuth', defaults.beams) ...
          - info(rio, 'antennaazimuth');
beam_zen = info(rio, 'zenith', defaults.beams);

val = repmat(nan, numbeams, numel(sourcedata(n).azimuth));
mask = logical(zeros(size(val)));

if strcmp(defaults.method, 'directivity')
  warning('directivity method is now called antennagain');
  defaults.method = 'antennagain';
end

switch defaults.method
 case 'angulardistance'
  % beam_az_rad = info(rio, 'azimuth', defaults.beams) * d2r;
  % beam_alt_rad = (90 - info(rio, 'zenith', defaults.beams)) * d2r;
  beam_az_rad = beam_az * d2r;
  beam_alt_rad = (90 - beam_zen) * d2r;
  
  if isempty(defaults.threshold)
    defaults.threshold = info(rio, 'beamwidth', 'beams', defaults.beams);
  end
  for bn = 1:numbeams
    for sn = 1:numsources
      tmp = r2d * sphere_dist(beam_az_rad(bn), beam_alt_rad(bn), ...
			      sourcedata(sn).azimuth * d2r, ...
			      sourcedata(sn).altitude * d2r)';
      % ignore source when it is below the horizon
      tmp(sourcedata(sn).altitude < 0) = nan;
      
      val(bn,:) = min(val(bn,:), tmp);
    end
    mask(bn, :) = (val(bn,:) < defaults.threshold(bn));
  end
  
 case 'antennagain'
  if isempty(defaults.threshold)
    defaults.threshold = repmat(-3, numbeams, 1);
  end
  
  % Find the maximum gain for each beam
  gn_max = cell(1, numbeams);
  for bn = 1:numbeams
    tmp = antennagain(rio, ...
                      'azimuth', (90 - beam_az(bn)) * d2r, ...
                      'zenith', beam_zen(bn) * d2r, ...
                      'beams', defaults.beams(bn), ...
                      'taperid', defaults.taperid);
    gn_max{bn} = abs(tmp{1}); % ensure gain is not complex
    
    % disp(sprintf('#%d  %f  %.1f,%.1f', defaults.beams(bn), ...
    %          	 gn_max{bn}, beam_az(bn), beam_zen(bn)));
  end
  
  for sn = 1:numsources
    gn = antennagain(rio, ...
		     'azimuth',d2r * (90 - sourcedata(sn).azimuth), ...
		     'zenith', d2r * (90 - sourcedata(sn).altitude), ...
		     'taperid', defaults.taperid, ...
		     'beams', defaults.beams);
    for bn = 1:numbeams
      % ensure gain is not complex
      gn{bn} = abs(gn{bn});
      
      % ignore source when it is below the horizon
      gn{bn}(sourcedata(sn).altitude < 0) = nan;
      
      % convert to dB relative to beam pointing direction
      gn{bn} = 10*log10(gn{bn} ./ gn_max{bn});
      
      val(bn,:) = max(val(bn,:), gn{bn}');
    end
  end
  
  for bn = 1:numbeams
    % Each beam can have its own threshold
    mask(bn, :) = (val(bn,:) > defaults.threshold(bn));
  end
  
 case 'antennagainmainlobe'
  % Get the details for the requested threshold using the antennagain
  % method. This might include crossings into sidelobes.
  defaults2 = defaults;
  defaults2.method = 'antennagain';
  prop = makeprop(defaults2);
  [mask_ag val_ag t sourcedata] = feval(mfilename, rio, prop{:});

  % Get the details for crossing into the 'main' lobe. This could be
  % based on angular distance, (ensures that grating lobes are excluded,
  % although assumes beams are circular). Alternatively could use -3dB
  % level (works around non-circular beams but ignores potential grating
  % lobes).
  if 0
    defaults2.method = 'angulardistance';
    defaults2.threshold = []; ; % use default threshold (0.5 * beamwidth)
  else
    defaults2.method = 'antennagain';
    defaults2.threshold = -3;
  end
  prop = makeprop(defaults2);
  mask_ml = feval(mfilename, rio, prop{:}); % main lobe mask
  mask = logical(zeros(size(mask_ag))); % Final mask
  val = val_ag;
  for n = 1:numbeams
    mask(n, :) = 0;
    % Does the star pass into the main lobe?
    if any(mask_ml(n, :))
      % Yes, somewhere, work out which group(s) of ones in mask_ag is(are)
      % due to the main lobe.
      [tmp seriesdata] = printseries(find(mask_ag(n, :)));

      for m = 1:size(seriesdata, 2)
	% For the current run of ones find out if any corresponding values
        % in the main lobe mask are true.
	cols = seriesdata(1, m):seriesdata(2, m):seriesdata(3, m);
	if any(mask_ml(n, cols))
	  % Masks intersect so for this beam crossing use the mask values
          % from mask_ag.
	  mask(n, cols) = true;
	end
      end
    end
  end
  
 case 'negativesidelobe'
  % Negative sidelobes only really make sense with ARIES systems, However,
  % don't restrict use to just those systems.
  if isempty(defaults.threshold)
    defaults.threshold = -20;
  end

  for sn = 1:numel(sourcedata)
    sd = sourcedata(sn);
    disp(sprintf('Source = %s', sd.propername));
    ra_dec = [sd.rightascension sd.declination];
    
    ra_dec_rad = ra_dec * d2r;
    az_alt_rad = horiz_coo(ra_dec_rad, jd, [lon_rad lat_rad], 'h');
    
    % Convert to azimuth (as compass angle, not trig angle) and zenith
    az_rad = pi_2 - az_alt_rad(:,1);
    zen_rad = pi_2 - az_alt_rad(:,2); 
    
    gain = zeros(numbeams, numt);

    for bn = 1:numbeams
      beam = defaults.beams(bn);
      disp(sprintf('Beam %d', beam));

      % Get details times when source is inside the main lobe
      sc_mask = feval(mfilename, rio, ...
		      'time', t, ...
		      'beams', beam, ...
		      'sources', {sd.name}, ...
		      'method', 'antennagainmainlobe', ...
		      'taperid', defaults.taperid, ...
		      'threshold', defaults.threshold(bn));
      
      % [g0dB g0] = antenna_beam_gain(rio, beam)
      
      tmp = ...
	  antennagain(rio, ...
		      'azimuth', pi_2 - (d2r * info(rio, 'azimuth', beam)), ...
		      'zenith', d2r * info(rio, 'zenith', beam), ...
		      'beams', beam, ...
		      'taperid', defaults.taperid);
      g0 = tmp{1};
      g0Ang = angle(g0);

      % Compute location of radio star
      tmp = antennagain(rio, ...
			'azimuth', az_rad, ...
			'zenith', zen_rad, ...
			'beams', beam, ...
			'taperid', defaults.taperid);
      g = tmp{1};
      
      
      % Rotate the component of gain which is opposite to the gain at the
      % beam centre to lie along the positive X axis. This is a
      % rotation of pi-g0Ang. Then take the real component.
      gOpposite = real(g .* exp(i .* (pi - g0Ang)));
      
      % Some values might be negative, convert to smallest positive
      % integer
      gOpposite = max(gOpposite, eps);
      % Take dB value relative to gain along in beam pointing direction
      gdB = 10 * log10(gOpposite ./ abs(g0));
      mask_bn = (gdB > defaults.threshold(bn)) & ~sc_mask';
      mask(bn, :) = mask(bn, :) | mask_bn';

      % gain(bn, :) = g';
      gain(bn, :) = gdB';
    end
    val = setfield(val, sd.name, gain);

  end
  
 case 'negativesidelobe2'
  % Negative sidelobes only really make sense with ARIES systems, However,
  % don't restrict use to just those systems.
  if isempty(defaults.threshold)
    defaults.threshold = -20;
  end

  for sn = 1:numel(sourcedata)
    sd = sourcedata(sn);
    disp(sprintf('Source = %s', sd.propername));
    ra_dec = [sd.rightascension sd.declination];
    
    ra_dec_rad = ra_dec * d2r;
    az_alt_rad = horiz_coo(ra_dec_rad, jd, [lon_rad lat_rad], 'h');
    
    % Convert to azimuth (as compass angle, not trig angle) and zenith
    az_rad = pi_2 - az_alt_rad(:,1);
    zen_rad = pi_2 - az_alt_rad(:,2); 
    
    negsl = zeros(numbeams, numt);

    for bn = 1:numbeams
      beam = defaults.beams(bn);
      disp(sprintf('Beam %d', beam));

      % Get details times when source is inside the main lobe
      sc_mask = feval(mfilename, rio, ...
		      'time', t, ...
		      'beams', beam, ...
		      'sources', {sd.name}, ...
		      'method', 'antennagainmainlobe', ...
		      'taperid', defaults.taperid, ...
		      'threshold', defaults.threshold(bn));
      
      % [g0dB g0] = antenna_beam_gain(rio, beam)
      
      tmp = ...
	  antennagain(rio, ...
		      'azimuth', pi_2 - (d2r * info(rio, 'azimuth', beam)), ...
		      'zenith', d2r * info(rio, 'zenith', beam), ...
		      'beams', beam, ...
		      'taperid', defaults.taperid);
      g0 = tmp{1};
      g0Ang = angle(g0);

      % Compute location of radio star
      tmp = antennagain(rio, ...
			'azimuth', az_rad, ...
			'zenith', zen_rad, ...
			'beams', beam, ...
			'taperid', defaults.taperid);
      g = tmp{1};
      
      % Calculate the power for the pointing direction only, and for when
      % the radio source contribution is included.
      % p0 = power(real(g0), 2) + power(imag(g0), 2);
      % p = power(real(g0 + g), 2) + power(imag(g0 + g), 2);
      p0 = real(g0) + imag(g0);
      p = real(g0 + g) + imag(g0 + g);
      
      
      % Calculate the relative difference in power. Positive values mean
      % that negative sidelobes are present.
      relp = (p0 - p) ./ p0;
      mask_bn = (relp > power(10, defaults.threshold(bn)/10)) & ~sc_mask';
      mask(bn, :) = mask(bn, :) | mask_bn';
      
      negsl(bn, :) = relp';
    end
    val = setfield(val, sd.name, negsl);
    
  end
  
  % ---------------------------
 otherwise
  error(sprintf('unknown method (was ''%s'')', defaults.method));
end


if nargout
  varargout{1} = mask;
end

if nargout >= 2
  varargout{2} = val;
end
if nargout >= 3
  varargout{3} = t;
end
if nargout >= 4
  varargout{4} = sourcedata;
end


if defaults.print
  switch defaults.method
   case 'negativesidelobe'
    s = 'Beams with source(s) in negative sidelobe'; 
   otherwise
    s = 'Beams with scintillation';
  end
  
  %    'YYYY-MM-DD hh:mm:ss  Beams with scintillation
  scint_idx = find(any(mask, 1)); % scintillation time index
  disp(sprintf('        time         %s', s));
  for n = 1:numel(scint_idx)
    beams_idx = find(mask(:, scint_idx(n)));
    disp(sprintf('%s %s', ...
		 strftime(t(scint_idx(n)), '%Y-%m-%d %H:%M:%S'), ...
		 printseries(defaults.beams(beams_idx))));
  end
end


function deg = ra2deg(hours, minutes, seconds)
deg = ((((seconds / 60) + minutes) / 60) + hours) * 15;

function r = degms2deg(deg, minutes, seconds)
r = deg + (((seconds/60) + minutes) / 60);
