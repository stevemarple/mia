function [x, y] = calcbeamlocations(rio, varargin)
%CALCBEAMLOCATIONS  Calculate the beam locations for a riometer.
%
%   [x, y] = CALCULATE(rio, ...)
%
%   

defaults.beams = [];

% defer these settings to avoid excessive function calling
defaults.azimuth = [];
defaults.height = []; % projection height, in metres
defaults.zenith = [];
defaults.units = '';

% map up (or down) field line from absorption height ('height') to the
% 'fieldlineheight'. units must be deg or aacgm
defaults.fieldlineheight = [];


[defaults unvi] = interceptprop(varargin, defaults);

% sanity check
if ~isequal(size(defaults.beams), size(defaults.azimuth)) | ...
      ~isequal(size(defaults.beams), size(defaults.zenith))
  error(['size of beam matrix must be equal to azimuth and zenith' ...
	 ' matrices']);
end
% handle the fields we deferred from giving default settings. Now the
% function calls are made only if needed
deferred_fields = {'azimuth', 'zenith'}; % 
for n = 1:length(deferred_fields)
  f = deferred_fields{n};
  if isempty(getfield(defaults, f))
    defaults = setfield(defaults, f, info(rio, f));
  end
end
if isempty(defaults.height)
  defaults.height = info(rio, 'defaultheight');
end

pi_180 = pi / 180;
theta = defaults.zenith * pi_180;
Re = earthradius;
ReH = Re + defaults.height;

x = zeros(size(defaults.zenith));
y = zeros(size(defaults.zenith));

% From Detrick and Rosenburg, "A Phased-array Radiowave Imager for Studies
% of Cosmic Noise Absorption", Radio Science, Vol 25, No. 4, pp 325-328,
% July-August 1990

deltaTheta = sqrt(ReH.^2 - (Re * sin(theta)).^2) - Re * cos(theta);
  
% nearlyDTheta is the angle subtended at the Earth's centre (in radians)
% between the beam and the zenith of the riometer station. dTheta isn't an
% angle but will stick to terminology used in Detrick and Rosenburg.
nearlyDTheta = acos(...
    (ReH.^2 + Re.^2 - deltaTheta.^2) / ...
    (2 * Re * ReH));
dTheta = ReH * nearlyDTheta;
% azimuth angles are w.r.t to North, map to trigonmetric angle.
defaults.azimuth = 90 - defaults.azimuth;

% calculate x and y in metres based on a Cartesian coordinate system where
% the riometer system is at (0,0) and the Y axis points north 
xm = dTheta .* cos(defaults.azimuth * pi_180);
ym = dTheta .* sin(defaults.azimuth * pi_180);    

switch defaults.units
 case ''
  error('units must be given');
  
 case 'm'
  x = xm;
  y = ym;
  
 case 'km'
  x = xm * 1e-3;
  y = ym * 1e-3;

 case {'m_antenna' 'km_antenna'}
  % calculate x and y in km based on a Cartesian coordinate system where the
  % riometer system is at (0,0) and the Y axis is aligned in the direction
  % the antenna points.
  
  % the azimuth angle below is based on trigonmetric angle, hence subtract
  defaults.azimuth = defaults.azimuth + info(rio, 'antennaazimuth');
  
  x = dTheta .* cos(defaults.azimuth * pi_180);
  y = dTheta .* sin(defaults.azimuth * pi_180);    
  
 if strcmp(defaults.units, 'km_antenna')
   x = x * 1e-3;
   y = y * 1e-3;
 end
   
 case 'deg'
  % map to longitude and latitude
  [x y] = ...
      xy2longlat(xm, ym, defaults.height, getlocation(rio));
  
 case 'aacgm'
  % map to longitude and latitude
  [x y] = ...
      xy2longlat(xm, ym, defaults.height, getlocation(rio));
  % map from geocentric longitude and latitude to AACGM
  tmp = geocgm('direction', 1, ...
	       'height', defaults.height, ...
	       'latitude', y, 'longitude', x, ...
	       varargin{unvi});
  x = tmp.cgm_longitude;
  y = tmp.cgm_latitude;
 otherwise
  error(sprintf('unknown unit (was %s)', defaults.units));
end

if ~isempty(defaults.fieldlineheight)
  switch defaults.units
   case 'deg'
    % map down to ground (sfp = start footprint)
    x(1)
    y(1)
    % 'height', defaults.height, ...
    [tmp1 tmp2 sfp] = ...
	geocgm(varargin{unvi}, ...
	       'direction', 'geo2cgm', ...
	       'height', defaults.height, ...
	       'latitude', y, ...
	       'longitude', x);
    tmp1.cgm_latitude(1)
    tmp1.cgm_longitude(1)
    sfp.cgm_latitude(1)
    sfp.cgm_longitude(1)
    
    % now map back up to fieldlineheight
    tmp = geocgm(varargin{unvi}, ...
		 'direction', 'cgm2geo', ...
		 'height', defaults.fieldlineheight, ...
		 'latitude', sfp.cgm_latitude, ...
		 'longitude', sfp.cgm_longitude)
    x = tmp.geocentric_longitude;
    y = tmp.geocentric_latitude;
    x(1)
    y(1)

   case 'aacgm'
    % map down to ground (sfp = start footprint)
    [tmp1 tmp2 sfp] = ...
	geocgm(varargin{unvi}, ...
	       'direction', 'cgm2geo', ...
	       'height', 0, ...
	       'latitude', y, ...
	       'longitude', x);
    
    % now map back up to fieldlineheight
    tmp = geocgm(varargin{unvi}, ...
		 'direction', 'geo2cgm', ...
		 'height', defaults.fieldlineheight, ...
		 'latitude', sfp.geocentric_latitude, ...
		 'longitude', sfp.geocentric_longitude);
    x = tmp.cgm_longitude;
    y = tmp.cgm_latitude;
   
   otherwise
    error(['field line mapping can only be carried out when units are ' ...
	   '''deg'' or ''aacgm''']);
  end
end
  
