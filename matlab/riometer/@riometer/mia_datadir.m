function r = mia_datadir(rio)
%MIA_DATADIR  Return the data base directory for a RIOMETER object.
%
%   r = MIA_DATADIR(rio)
%   r: base directory (CHAR)
%   rio: RIOMETER object
% 
%   See also mia_instrument_base/INFO.

r = fullfile(mia_datadir, 'riometer', ...
	     sprintf('%s_%d', getabbreviation(rio), getserialnumber(rio)));
