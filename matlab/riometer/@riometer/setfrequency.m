function r = setfrequency(in, f)
%SETFREQUENCY  Set the RIOMETER operating frequency.
%
%   r = SETFREQUENCY(in, f)
%   r: modified RIOMETER object(s)
%   in: RIOMETER object(s)
%   f: new operating frequency (Hz) (scalar or matrix same size as "in")
%
%   See also RIOMETER, GETFREQUENCY.

if prod(size(in)) == 1 | isempty(f)
  % default constructor has frequency empty, but can't see why it should
  % be needed here. Allow anyway. Also allow the frequency to be set to a
  % vector (maybe for different beams?) for the case of "in" being
  % scalar.
  r = in;
  for n = 1:prod(size(in))
    r(n).frequency = f;
  end

elseif ~isequal(size(in), size(f))
  error('in and f must be same size');

else
  r = in;
  for n = 1:prod(size(in))
    r(n).frequency = f(n);
  end
end
