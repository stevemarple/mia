function r = standardobliquityfilename(rio)
%STANDARDOBLIQUITYFILENAME  Return the filename for std obliquity data.
%
%   r = STANDARDOBLIQUITYFILENAME(rio)

% private function, don't bother with non scalar input problems

r = sprintf('rio_std_obliquity_%s_%d.mat', getabbreviation(rio), ...
	    getserialnumber(rio));
