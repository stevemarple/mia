function [Xout, Yout] = irisbeamprojection(rio, varargin);

d2r = pi / 180; % factor to multiply by to convert from degrees to radians
r2d = 180 / pi; % factor to multiply by to convert from radians to degrees
Re = earthradius; % take a local copy for speed (metres)

defaults.beams = info(rio, 'beams');
defaults.height = info(rio, 'defaultheight');  % metres
defaults.contourlevel = -3; % dB
 % units for the X and Y dimensions. Other valid option is: 'deg'
defaults.units = 'm';
defaults.eliminatesidelobes = 0;
defaults.eliminategratinglobes = 0;

% Used when merging multiple contours to find matching start/end points
defaults.mergingcontourtolerance = 1e3 * eps;

systemtype = lower(info(rio, 'systemtype'));
if strcmp(systemtype, 'aries')
  % ariesdirectivity accepts tapering information, so allow that to be pass
  % on if specified
  defaults.taperid = [];
  defaults.xtapering = [];
  defaults.ytapering = [];
  defaults.beamtype = '';
end

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

numOfBeams = prod(size(defaults.beams));
% irisdirectivity wants trig-style azimuth
thetaV = [0.0001:1:90] * pi / 180;
phiV = [0.001:1:360 0.001] * pi / 180;
thetaV = unwrap(thetaV);
phiV = unwrap(phiV);

% values
[theta phi] = meshgrid(thetaV, phiV);


XV = thetaV .* r2d;
YV = phiV .* r2d;

X = theta .* r2d;
Y = phi .* r2d;


if length(defaults.contourlevel) == 1
  defaults.contourlevel = [defaults.contourlevel defaults.contourlevel];
end

Xout = cell(1,numOfBeams);
Yout = cell(1,numOfBeams);

for n = 1:numOfBeams
  switch systemtype
   case 'aries'
    gain = abs(ariesgain(rio, theta, phi, defaults.beams(n), ...
			 'taperid', defaults.taperid, ...
			 'xtapering', defaults.xtapering, ...
			 'ytapering', defaults.ytapering, ...
                         'beamtype', defaults.beamtype));
    
   case 'iris'
    d0 = abs(irisdirectivity(rio, theta, phi, defaults.beams(n)));
    gain = power(d0, 2);
   
   case 'widebeam'
    error(['cannot use ' mfilename ' on widebeam system']);
    
   otherwise
    error('unknown system type');
  end
  
  

  maxGain = max(gain(:));
  gainDB = 10 * log10(gain ./ maxGain);  % dB
  
  switch char(defaults.units)
    case {'deg' 'm' 'km'}
      % Adjust pointing direction.
      antennaAzRad = d2r * (360 - info(rio, 'antennaazimuth'));
    case {'km_antenna' 'm_antenna'}
      % Based on a km grid, centred on riometer and the Y axis points in the
      % same direction as the antenna array, so don't adjust for antenna
      % arrya pointing direction.
      antennaAzRad = 0;
      
    otherwise
	error(sprintf('Unknown units (was %s)', char(defaults.units)));
    end
      
  c = contourc(thetaV, phiV, gainDB, defaults.contourlevel);
  cc = contour2cell(c);
  if length(cc) > 1 
    % preallocate for speed
    tmpx = cell(1, length(cc));
    tmpy = cell(1, length(cc));
  end

  for m = 1:length(cc)
    % process each contour
    % convert zenith angle to angle at Earth's centre allowing for
    % curvature of the ionosphere
    polarangle = polarangleofray(cc{m}.x, defaults.height);
    r = Re * polarangle; % radial distance on Earth
    x = r .* cos(cc{m}.y + antennaAzRad);
    y = r .* sin(cc{m}.y + antennaAzRad);
    switch char(defaults.units)
      case 'deg'
	% map from km to latitude, longitude
	[x y] = xy2longlat(x, y, 0, getlocation(rio));
     case {'m' 'm_antenna'}
     
     case {'km' 'km_antenna'}
      x = x / 1e3;
      y = y / 1e3;
      
      otherwise
	error(sprintf('Unknown units (was %s)', char(defaults.units)));
    end
    
    if length(cc) > 1
      tmpx{m} = x;
      tmpy{m} = y;
    else
      tmpx = x;
      tmpy = y;
    end

    Xout{n} = tmpx;
    Yout{n} = tmpy;
  end

  % try to resolve multiple contours, check for common start/ends and
  % turn into one contour if possible
  if iscell(Xout{n}) & length(Xout{n}) == 2
    % only attempt simple cases for now
    tmpx = Xout{n};
    tmpy = Yout{n};

    tmpxR = tmpx;
    tmpyR = tmpy;

    if defaults.mergingcontourtolerance ~= 0
      for z = 1:numel(tmpxR)
	tmpxR{z} = round2(tmpxR{z}, defaults.mergingcontourtolerance);
	tmpyR{z} = round2(tmpyR{z}, defaults.mergingcontourtolerance);
      end
    end
    
    if tmpxR{1}(1) == tmpxR{2}(1) & tmpyR{1}(1) == tmpyR{2}(1)
      % common head
      Xout{n} = [fliplr(tmpx{1}) tmpx{2}(2:end)];
      Yout{n} = [fliplr(tmpy{1}) tmpy{2}(2:end)];
    elseif tmpxR{1}(end) == tmpxR{2}(end) & tmpyR{1}(end) == tmpyR{2}(end)
      % common tail
      Xout{n} = [tmpx{1}(1:(end-1)) fliplr(tmpx{2})];
      Yout{n} = [tmpy{1}(1:(end-1)) fliplr(tmpy{2})];
    elseif tmpxR{1}(end) == tmpxR{2}(1) & tmpyR{1}(end) == tmpyR{2}(1)
      Xout{n} = [tmpx{1} tmpx{2}(2:end)];
      Yout{n} = [tmpy{1} tmpy{2}(2:end)];
    elseif tmpxR{1}(1) == tmpxR{2}(end) & tmpyR{1}(1) == tmpyR{2}(end)
      Xout{n} = [tmpx{2} tmpx{1}(2:end)];
      Yout{n} = [tmpy{2} tmpy{1}(2:end)];
    end
    
  end
  
  if iscell(Xout{n}) & logical(defaults.eliminategratinglobes)
    % Anything which has multiple contours is deleted
    Xout{n} = [];
    Yout{n} = [];
  end
  
  if iscell(Xout{n}) & logical(defaults.eliminatesidelobes)
    % Find the highest value
    [row col] = find(gain == max(gain(:)));
    
    if length(row) > 1
      warning('more than one maxima found!');
      row = row(1);
      col = col(1);
    end
    
    % Really want to know if the contour encloses the maximum. (How)
    % Instead take the mean of the contour points and pretend that is its
    % centre. Find the contour whose centre is closest to the maximum.
    dist = zeros(size(Xout{n})); 
    for k = 1:prod(size(Xout{n}))
      dist(k) = sqrt(power(mean(Xout{n}{k}) - X(row,col), 2) + ...
		     power(mean(Yout{n}{k}) - Y(row,col), 2));
    end
    [tmp closest] = min(dist(:));
    Xout{n} = Xout{n}{closest};
    Yout{n} = Yout{n}{closest};
  end
  
end

