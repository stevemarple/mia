function mia = readniprfile(rio, varargin)
%READNIPRFILE  Read a riometer datafile in NIPR format
%
%


% To save _huge_ amounts of execution time this function does not make
% full use of the time classes, nor does it use INSERT. Calculations
% are made as seconds in the day, which is why it is important to
% check that the record belongs to the current day. (Normally the
% files do not cross day boundaries.) 

% This is a low-level function 

defaults.filename = '';

% to check which records are valid it is necessary to know the
% correct starttime of the file. If not passed this is
% calculated from the first record in the file (which may be
% contain the wrong time).
defaults.filestarttime = [];

% can limit which records are included
defaults.starttime = [];
defaults.endtime = [];

defaults.beams = [];
defaults.class = '';
defaults.units = 'ADC';
% temporal resolution of the input file(s)
defaults.inputresolution = [];

% write data out at this resolution. If not set then it is assumed that
% the blocks are on boundaries of inputresolution, a warning will be
% produced if that is not the case
defaults.outputresolution = [];

% optionally limit the data used to within set times


% a strftime format string to indicate how to load the data
defaults.limits = [0 4095];
defaults.sample = 'uint16';

defaults.defaultfilename = [];

% for debugging it is useful to see how many records failed to start
% on a time which is a multiple of the record duration
defaults.showboundarystats = 0;


% It is possible that some data for a given day is stored in the previous
% day's data file. This happens when the last record in a file crosses a day
% boundary (or an hour boundary for the new Syowa riometer). To solve this
% problem the previous file must be read. It is not necessary to read the
% entire file, much time can be saved by reading only the last record
defaults.lastrecordonly = 0;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.filename)
  error('filename not given');
end
if isempty(defaults.beams)
  % error('beam numbers must be specifed');
  defaults.beams = info(rio, 'allbeams');
end

% these values are good for all current NIPR riometers
framesperrecord = 8;
numberofbeams = 64;
bn = sqrt(numberofbeams);
beamorder = reshape(fliplr(reshape(1:numberofbeams, [bn bn])'), ...
			     [1 numberofbeams]);




isofstr = '%Y-%m-%d %H:%M:%S'; % for printing dates/times
resFactor = 1; % input resolution to output resolution


if isempty(defaults.class)
  error('class must be specified');
else
  isaqdc = isa(feval(defaults.class), 'rio_qdc_base');
end
if isempty(defaults.inputresolution)
  error('inputresolution must be specified');
end

if ~isempty(defaults.outputresolution)
  if defaults.outputresolution > defaults.inputresolution
    error('output resolution cannot be lower than input resolution');
  end
  resFactor = defaults.inputresolution / defaults.outputresolution;
  if resFactor ~= floor(resFactor)
    error('input resolution must be a multiple of the output resolution');
  end
end


defaults.beams = reshape(defaults.beams, [1 prod(size(defaults.beams))]);


res = defaults.outputresolution;
if isempty(res)
  res = defaults.inputresolution;
end
resSecs = gettotalseconds(res);

inputresolutionSecs = gettotalseconds(defaults.inputresolution);

% open the file and find out the start and end times
[fid mesg] = fopen(defaults.filename, 'r', 'ieee-le'); % PC format
if fid == -1
  error(sprintf('could not open %s: %s', defaults.filename, mesg));
end

% read first record, get record size and then file size
rt1 = localReadRecord(fid, defaults.filename, beamorder, framesperrecord, ...
		      defaults);
if isempty(rt1)
  error(['corrupted file: ', defaults.filename]);
end

recordSz = ftell(fid);
fseek(fid, 0, 'eof');
fileSz = ftell(fid);

% seek to the last record
lastRecPos = floor(fileSz/recordSz) * recordSz;
if lastRecPos == fileSz
  lastRecPos = lastRecPos - recordSz;
elseif 0 & lastRecPos > fileSz - recordSz
  % last record is incomplete, use penultimate record
  lastRecPos = lastRecPos - recordSz;
end
fseek(fid, lastRecPos, 'bof');
rt2 = localReadRecord(fid, defaults.filename, beamorder, framesperrecord, ...
		      defaults);


if logical(defaults.lastrecordonly)
  % reading the last record only is not the normal situation. To
  % ensure we get the same behaviour as for reading the whole file
  % just skip to the start of the last record and read it again,
  % even though we have read it once (it won't take long and this
  % way guarantees the same behaviour).
  fseek(fid, lastRecPos, 'bof');
  st = timestamp(rt2);
else
  % read the file from the beginning
  frewind(fid);
  st = timestamp(rt1);
end

% fstDatevec = datevec(st);

et = timestamp(rt2) + framesperrecord * defaults.inputresolution;
etdate = getdate(et);

if isempty(defaults.defaultfilename)
  defaults.defaultfilename = info(rio, 'defaultfilename', defaults.class);
end

if ~isempty(defaults.filestarttime)
  filestarttime = floor(defaults.filestarttime, ...
			defaults.defaultfilename.duration);
  fstDatevec = datevec(filestarttime);
elseif getdate(st) ~= etdate & (et - etdate) > timespan(0, 's') & ~isaqdc
  st
  et
  rt2
  framesperrecord
  defaults.inputresolution
  error(sprintf('file %s crosses midnight (%s/%s), refusing to process', ...
		defaults.filename, ...
		strftime(st, '%Y-%m-%d'), strftime(et, '%Y-%m-%d')));
elseif isaqdc
  % hack for QDC files (don't use the duration part of defaultfilename)
  filestarttime = floor(st, timespan(1, 'd'));
  fstDatevec = datevec(filestarttime);
else
  filestarttime = floor(st, defaults.defaultfilename.duration);
  fstDatevec = datevec(filestarttime);
end
fstS = fstDatevec(4:6) * [3600; 60; 1]; % time of day in seconds


% wish to add only data between start and end times (if set). Would
% normally do this with the time classes, but they are two slow to
% use this frequently. Therefore use seconds of the day, taking
% care to set this only if the start/end date matches the file date
lim1 = 0;     % inclusive, use >=
lim2 = 86400; % exclusive, use <
if isa(defaults.starttime, 'timestamp') & isa(defaults.endtime, 'timestamp')
  if getdate(defaults.starttime) == getdate(filestarttime)
    lim1 = gettotalseconds(gettimeofday(defaults.starttime));
  elseif defaults.starttime > filestarttime
    lim1 = 86400;
  end
  if getdate(defaults.endtime) == getdate(filestarttime)
    lim2 = gettotalseconds(gettimeofday(defaults.endtime));
  elseif (defaults.endtime) < filestarttime
    lim2 = 0;
  end
  if lim2 > lim1
    data = repmat(nan, length(defaults.beams), ...
		  (lim2 - lim1) / resSecs);
  else
    data = [];
  end
  st = defaults.starttime;
  et = defaults.endtime;
elseif isa(defaults.starttime, 'timestamp') | ...
      isa(defaults.endtime, 'timestamp')
  error('both start and endtime must be specified');
else
  data = repmat(nan, length(defaults.beams), (et-st) / res);
end

mia = feval(defaults.class, ...
	    'starttime', st, ...
	    'endtime', et, ...
	    'resolution', res, ...
	    'beams', defaults.beams, ...
	    'instrument', rio, ...
	    'units', defaults.units, ...
	    'load', 0, ...
	    'log', 1);






obl = '';
oblFac = [];
if strcmp(defaults.class, 'rio_abs')
  mia = setobliquityfield(mia, obl, oblFac);
end



% count proportion of records not on boundary
boundaryCounter = [0 0]; 
			 
% read records
while ~feof(fid)
  [rt rd rpos] = localReadRecord(fid, defaults.filename, beamorder, ...
				 framesperrecord, defaults);
  if isempty(rt)
    break;
  elseif ~isequal(fstDatevec(1:3), rt(1:3))
    % sanity check, since we are not using insert to add data
    fstDatevec
    rt
    warning(sprintf(['record beginning %s does not belong day %s\n' ...
		     'file name: %s\n' ...
		     'file position: %d\n' ...
		     'YMD: %d %d %d'], ...
		    strftime(timestamp(rt), isofstr), ...
		    strftime(st, isofstr), ...
		    defaults.filename, ...
		    rpos, ...
		    rt(1:3)));
  else
    
    if isempty(defaults.outputresolution) | resFactor == 1
      % check record starts on a boundary value
      rts = rt(4:6) * [3600; 60; 1];
      if rem(rts, inputresolutionSecs)
	if 0
	  warning(sprintf(['record for %s does not start on a %s ' ...
			   'boundary, moving to nearest boundary'], ...
			  strftime(timestamp(rt), isofstr), ...
			  char(defaults.inputresolution)));
	end
	rts = (round(rts / inputresolutionSecs)) * ...
	      inputresolutionSecs;
	boundaryCounter = boundaryCounter + [1 1];
      else
	boundaryCounter = boundaryCounter + [0 1];
      end
    elseif resFactor > 1
      % increase number of samples (never decrease)
      rd2 = zeros(size(rd) .* [1 resFactor]);
      for n = 0:(size(rd, 2)-1)
	rd2(:, n*resFactor + [1:resFactor]) = rd(:, n+1);
      end
      rd = rd2;
    end
    
    % the times of the samples, in seconds of day
    
    sampleS = rts:resSecs:(((size(rd, 2)-1) * resSecs) + rts);
    % reorder all samples according to beam number;
    tmp(beamorder, :) = rd(:, :);
    
    % the index of the samples to use
    idx = find(sampleS >= lim1 & sampleS < lim2);
    
    % the time of the samples in use
    sampleS2 = sampleS(idx); 

    % data(:, 1 + (sampleS2-fstS/resSecs)) = tmp(defaults.beams, idx);
    if ~isempty(idx)
      data(:, 1 + ((sampleS2-lim1)/resSecs)) = ...
	  tmp(defaults.beams, idx);
    end
    if 0 
      % offset = rts / resSecs;
      offset = (rts-fstS) / resSecs;
      idx1 = offset + 1;
      idx2 = offset + framesperrecord*resFactor;
      % data(beamorder(defaults.beams), idx1:idx2) = rd(:, :);
      tmp(beamorder, :) = rd(:, :);
      % data(:, idx1:idx2) = tmp(defaults.beams, :);
      idx = idx1:idx2;
      idxf = find((idx >= lim1 & idx < lim2))
      data(:, idx(idxf)) = tmp(defaults.beams, idxf);
    end
    clear tmp;
  end
end

fclose(fid);
fid = [];

data(data <= defaults.limits(1)) = nan;
data(data >= defaults.limits(2)) = nan;

if strcmp(defaults.class, 'rio_rawpower')
  % store raw data in same class to save space
  data = feval(defaults.sample, data);
end

mia = setdata(mia, data);

if strcmp(defaults.class, 'rio_abs')
  mia = setobliquityfield(mia, obl, oblFac);
end


if logical(defaults.showboundarystats)
  disp(sprintf('proportion of records not on boundary: %d/%d (%.1f%%)', ...
	       boundaryCounter, 100*boundaryCounter(1)/ ...
	       boundaryCounter(2)));
end

% ----------------------------------
function [rt, rd, fpos] = localReadRecord(fid, fname, beamorder, ...
					  framesperrecord, defaults)
%LOCALREADRECORD  Read a record in the file
% 
%   rt: record time, [1x6 DOUBLE] (see DATEVEC)
%   rd: record data, DOUBLE
%   fpos: file position of start of record

rt = [];
rd = [];
fpos = ftell(fid);
% read the time
sz = [1 6];
[dv c] = fread(fid, sz, 'uint8');
if c ~= prod(sz)
  return;
end


if any(dv > [99 12 31 23 59 59])
  % NB two-digit year at this point
  disp(sprintf(['bad timestamp: [%d %d %d %d %d %d]\n' ...
		'file name: %s\n' ...
		'file position: %d\n' ...
		'block start: %d'], ...
	       dv(1:6), fname, ftell(fid), fpos));
  return; % year not valid, discard block
elseif dv(1) < 80
  dv(1) = dv(1) + 2000;
else
  dv(1) = dv(1) + 1900;
end
rt = dv;

disp(sprintf('%04d-%02d-%02d  %02d:%02d:%02d   % 5d', dv, ...
	     dv(4:6) * [3600; 60 ;1]));

% skip the next 10 bytes
fseek(fid, 10, 0);

% read all frames in one record
sz = [prod(size(beamorder)) framesperrecord];
[rd c] = fread(fid, sz, defaults.sample);
if c ~= prod(sz) & 0
  % rt = [];
  % rd = [];
  % return;
  rd(sz(1),sz(2)) = nan; % resize to the desired size
  rd((c+1):(end-1)) = nan; % set all other new data to nan
end
