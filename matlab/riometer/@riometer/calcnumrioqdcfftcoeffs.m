function r = calcnumrioqdcfftcoeffs(rio, varargin)
%CALCNUMRIOQDCFFTCOEFFS Calculate number of coefficients for RIO_QDC_FFT.
%
%   r = CALCNUMRIOQDCFFTCOEFFS(rio);
%   r = CALCNUMRIOQDCFFTCOEFFS(rio, ...);
%   r: number of coefficients required
%   rio: RIOMETER object
%   beams: beam numbers
%
% CALCNUMRIOQDCFFTCOEFFS calculates the number of coefficients which should
% be used for a RIO_QDC_FFT object, based on the riometer beams widths,
% pointing directions and its latitude. The idea is that QDCs for beams
% pointing in the plane of the ecliptic require more coefficients than ones
% pointing parallel to the Earth's axis of rotation. Similarly, QDCs from
% narrow beams record more detail in the background cosmic radiation and
% thus require more coefficients for QDCs from wider beams.
%
% The following parameter name/value pairs are recognised:
%
%   'beams', vector
%   Return the default number of coefficients for only the named beams. 
%   If not specified values for all beams are returned.
%
%   'resolution', [1x1 TIMESPAN]
%   Temporal resolution of the data. If not given it is assumed to be
%   equal to that returned by INFO(rio, 'bestresolution').
%
%
% See also RIO_QDC_FFT.



defaults.beams = info(rio, 'allbeams');
defaults.resolution = info(rio, 'bestresolution');

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi});


t = timestamp('now');
jd = juliandate(t);

bw = info(rio, 'beamwidth', 'beams', defaults.beams);
az = info(rio, 'azimuth', defaults.beams);
ze = info(rio, 'zenith', defaults.beams);

% r = (360 ./ bw);

loc = getlocation(rio);
lat = getgeolat(loc);
lon = getgeolong(loc);
d2r = pi / 180;

% equatorial lon/lat
eqcoord = horiz_coo([az(:) (90-ze(:))]*d2r, jd, [lon lat]*d2r, 'e');

% r = (360 ./ bw(:))
% r = (360 ./ bw(:)) .* cos(eqcoord(:,2))

% r = ceil((360 ./ bw(:)) .* cos(eqcoord(:,2))) + 5;
  
resSecs = gettotalseconds(defaults.resolution);

r = ceil((360 ./ bw(:)) .* cos(eqcoord(:,2)) ./ resSecs);

riocode = sprintf('%s_%d', getabbreviation(rio), getserialnumber(rio));

% local adjustments
switch riocode
 case 'kil_1'
  r = r+5;
 
 case 'zhs_1'
  r = r+8;

 otherwise
  r = r+5;
end


