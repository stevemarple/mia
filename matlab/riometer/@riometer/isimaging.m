function r = isimaging(rio)
%ISIMAGING  Indicate if RIOMETER is an imaging riometer.
%
%   r = ISIMAGING(rio)
%   r: LOGICAL
%   rio: RIOMETER instrument(s)
%
% ISIMAGING indicates if the riometer objects are imaging riometers. r is
% the same size as rio. Note that riometers can contain both widebeam and
% imaging arrays, thus ISIMAGING and ISWIDEBEAM are not the inverse of each
% other.
%
% See also RIOMETER, ISWIDEBEAM.

num = numel(rio);
r = repmat(false, size(rio));
for n = 1:num
  r(n) = ~isempty(info(rio(n), 'imagingbeams'));
  % s = info(rio(n), '_struct'); r(n) = ~isempty(s.ifrequency);
end
