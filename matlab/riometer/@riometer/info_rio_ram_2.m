function r = info_rio_ram_2(in, vout, s, req, varargin)
% varargout = va; % copy temporary output to output
r = vout;

val = 1;
switch req
 case 'arcombeammap' % ------------------------
  % This indicates where in the arcom data array each beam is
  % located
  n = 32;
  r.varargout{1} = reshape(flipud(reshape(1:(n*n),n,n)')', 1, n*n);
    
 case 'calibrationbeams'
  r.varargout{1} = [496 497 528 529];
  
 case 'calibrationsignalstrength' % ------------------------
  % This is the calibration signal strength in appropriate units
  defaults.time = [];
  defaults.beams = [];
  defaults.units = '';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  if isempty(defaults.beams)
    error('require beams');
  elseif isempty(defaults.time)
    error('require time');
  end
  
  load info_rio_ram_2_calibrationsignalstrength;
  r.varargout{1} = calibrationsignalstrength.data(defaults.beams);
  switch defaults.units
   case ''
    error('require units');
   case 'raw'
    ; % do nothing
   case 'dBm'
    mia = rio_rawpower('starttime', defaults.time, ...
                       'instrument', in, ...
		       'data',  r.varargout{1}, ...
		       'units', 'raw');
    mia2 = rio_power(mia);
    r.varargout{1} = getdata(mia2);
   otherwise
    error(sprintf('unknown units was ''%s''', defaults.units));
  end
  
 case 'calibrationwindow' % ------------------------
  % for calibration use the 3rd to last samples in the calibration sequence
  r.varargout{1} = [3 inf];
 
 case 'commoncalibrationdata'
  r.varargout{1} = true;

 case 'dbmconversion' % ------------------------
  % varargin{1} is time (timestamp)
  if length(varargin) < 1
    error('require start time of file')
  end
  % there were 1.4e6 samples per second when this value was estimated
  r.varargout{1} = -205 + 10 * log10(1.4e6);
 
 case 'defaultcalibrationmethod'
  r.varargout{1} = 'uncalibrated';

 case 'includesequencenumber' % ------------------------
  % modify a strftime format specifier to include the sequence number
  if length(varargin) ~= 1
    error('require format specifier (only)');
  end
    [fstr_path fstr_file fstr_ext] = fileparts(varargin{1});
  fstr_file = strrep(fstr_file, '_000_f', '_%%03d_f');
  r.varargout{1} = fullfile(fstr_path, [fstr_file fstr_ext]);
   
 case 'tapering' % ------------------------
  % info(rio, 'tapering', taperid)
  if numel(varargin) ~= 1
    error('tapering option requires taperid parameter');
  end
  
  taperid = varargin{1};
  if ischar(taperid)
    taperid = hex2dec(taperid); % allow use of hex values
  elseif numel(taperid) ~= 1
    error('tapering option requires a scalar taperid parameter');
  end
  

  switch taperid
   case 45056
    % taperid = 0xB000, no tapering
       xtaper = ones(1, 32);
       ytaper = xtaper;
  
   case 45057
    % taperid = 0xB001, X and Y arms have identical tapering
    xtaper = [-100 -301 -498 -690 -876 -1053 -1220 -1375 ...
              -1517 -1645 -1757 -1851 -1928 -1987 -2026 -2046 ...
              -2046 -2026 -1987 -1928 -1851 -1757 -1645 -1517 ...
              -1375 -1220 -1053 -876 -690 -498 -301 -100] + ...
             i * [-2046 -2026 -1987 -1928 -1851 -1757 -1645 -1517 ...
                  -1375 -1220 -1053 -876 -690 -498 -301 -100 ...
                  100 301 498 690 876 1053 1220 1375 ...
                  1517 1645 1757 1851 1928 1987 2026 2046];
    xtaper = xtaper ./ 2047; % defined as 12 bit values
    ytaper = xtaper;
    
   case 45328
    % taperid = 0xB110, X and Y arms have identical tapering
    xtaper = [-96 -182 -87 216 448 172 -553 -925 ...
              -235 1028 1419 207 -1486 -1737 -82 1759 ...
              1759 -82 -1737 -1486 207 1419 1028 -235 ...
              -925 -553 172 448 216 -87 -182 -96] + ...
             i * [133 -23 -225 -254 95 584 551 -278 ...
                  -1132 -868 556 1674 1059 -851 -2008 -1048 ...
                  1048 2008 851 -1059 -1674 -556 868 1132 ...
                  278 -551 -584 -95 254 225 23 -133];
    xtaper = xtaper ./ 2047; % defined as 12 bit values
    ytaper = xtaper;

   case 45329
    % taperid = 0xB111, X and Y arms have identical tapering
    xtaper = [-96 -182 -87 216 448 172 -553 -925 ...
              -235 1028 1419 207 -1486 -1737 -82 1759 ...
              1759 -82 -1737 -1486 207 1419 1028 -235 ...
              -925 -553 172 448 216 -87 -182 -96] + ...
             i * [-133 23 225 254 -95 -584 -551 278 ...
                  1132 868 -556 -1674 -1059 851 2008 1048 ...
                  -1048 -2008 -851 1059 1674 556 -868 -1132 ...
                  -278 551 584 95 -254 -225 -23 133];
    xtaper = xtaper ./ 2047; % defined as 12 bit values
    ytaper = xtaper;
    
   case 45568
    % taperid = 0xB200, X and Y arms have identical tapering
    xtaper = [61 205 205 369 512 655 922 1024 ...
              1147 1290:164:1618 1823 1823 1823 2047 ...
              2047 1823 1823 1823 1618 1454 1290 1147 ...
              1024 922 655 512 369 205 205 61];
    xtaper = xtaper ./ 2047; % defined as 12 bit values
    ytaper = xtaper;
    
   case 45569
    % taperid = 0xB201, X and Y arms have identical tapering
    xtaper = [-3 -30 -50 -124 -219 -337 -549 -688 ...
              -850 -1036 -1247 -1463 -1716 -1768 -1803 -2046 ...
              -2046 -1803 -1768 -1716 -1463 -1247 -1036 -850 ...
              -688 -549 -337 -219 -124 -50 -30 -3] + ...
             i * [-61 -203 -199 -347 -463 -562 -740 -759 ...
                  -770 -769 -748 -692 -614 -443 -267 -100 ...
                  100 267 443 614 692 748 769 770 ...
                  759 740 562 463 347 199 203 61];
    xtaper = xtaper ./ 2047; % defined as 12 bit values
    ytaper = xtaper;
             
   otherwise
    error(sprintf('unknown taper id (was %d [0x%04X])', taperid, ...
                  taperid));
  end

  r.varargout{1} = xtaper;
  r.varargout{2} = ytaper;
 
  
 otherwise % ------------------------
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
