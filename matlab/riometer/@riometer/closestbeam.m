function r = closestbeam(rio, x, y, varargin)
%CLOSESTBEAM Return the closest beam.
%
%

if numel(x) ~= 1
  error('x must be scalar');
end
if numel(y) ~= 1
  error('y must be scalar');
end

defaults.units = '';
defaults.beams = []; % all beams
defaults.height = [];

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.units)
  error('units must be set');
end

if isempty(defaults.beams)
  defaults.beams = info(rio, 'allbeams');
end


[beamlocX beamlocY] = info(rio, 'beamlocations', ...
			   'beams', defaults.beams, ...
			   'units', defaults.units, ...
			   'height', defaults.height);

switch defaults.units
 case {'m' 'km'}
  % no need to sqrt, just find smallest dist^2
  dist = power(beamlocX - x, 2) + power(beamlocY - y, 2);
  
 case {'deg' 'aacgm'}
  dist = zeros(size(defaults.beams));
  for n = 1:numel(defaults.beams)
    dist(n) = m_lldist([y beamlocY(n)], [x beamlocX(n)]);
  end
  
 otherwise
  error(sprintf('unknown units (was ''%s'')', defaults.units));
end

[tmp idx] = min(dist);

r = defaults.beams(idx);
