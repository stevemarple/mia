function r = info_rio_kar_2_data
%INFO_RIO_KAR_2_DATA Return basic information about rio_kar_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_kar_2. To override
% information given in this file see the instructions in INFO.
%

r.limits = [];
r.limits.rio_qdc = [nan nan];
r.limits.rio_rawpower = [nan nan];
r.limits.rio_rawqdc = [nan nan];
r.pixels = [];
r.abbreviation = 'kar';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [1 1];
r.antennaspacing = 0.5;
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = 1;
r.beamplanr = 1;
r.beamwidth = [60];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = 90000;
r.endtime = timestamp([2080 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = nan;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 65.708105;
r.location1 = 'Karholl';
r.location1_ascii = 'Karholl';
r.location2 = 'Iceland';
r.logo = 'pric_logo';
r.logurl = '';
r.longitude = -17.369623;
r.modified = timestamp([2018 11 18 09 58 00]);
r.name = 'Karholl WB';
r.piid = [];
r.qdcclass = 'rio_qdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(7 , 'd');
r.resolution = timespan(00, 'h', 00, 'm', 01, 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [];
r.starttime = timestamp([2018 09 27 00 00 00]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = 'crossed-dipole';
r.wbeams = 1;
r.wfrequency = 38.235e6;
r.widebeams = [1];
r.zenith = [0];


r.defaultfilename.original_format.default.archive = 'default';
r.defaultfilename.original_format.default.dataclass = 'double';
r.defaultfilename.original_format.default.defaultarchive = true;
r.defaultfilename.original_format.default.duration = timespan(1, 'd');
r.defaultfilename.original_format.default.failiffilemissing = false;
r.defaultfilename.original_format.default.format = 'lancsrio';
% r.defaultfilename.original_format.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/kil_1/original_format/%Y/%m/%d/p_%y_%j.%H';
r.defaultfilename.original_format.default.fstr = '/data/riometer/kar_2/original_format/%Y/%m/kar2_%Y%m%d.txt';
r.defaultfilename.original_format.default.loadfunction = '';
r.defaultfilename.original_format.default.resolution = timespan(1, 's');
r.defaultfilename.original_format.default.savefunction = '';
r.defaultfilename.original_format.default.size = [1 86400];
r.defaultfilename.original_format.default.units = 'V';


r.defaultfilename.rio_rawpower.default = r.defaultfilename.original_format.default;
r.defaultfilename.rio_rawpower.default.failiffilemissing = true;


r.defaultfilename.rio_power.default = r.defaultfilename.original_format.default;
r.defaultfilename.rio_power.default.failiffilemissing = true;
r.defaultfilename.rio_power.default.units = 'dBm';


% r.defaultfilename.rio_abs.Q1s.archive = '1s';
% r.defaultfilename.rio_abs.Q1s.dataclass = 'double';
% r.defaultfilename.rio_abs.Q1s.defaultarchive = false;
% r.defaultfilename.rio_abs.Q1s.duration = timespan(01, 'h', 00, 'm', 00, 's');
% r.defaultfilename.rio_abs.Q1s.failiffilemissing = true;
% r.defaultfilename.rio_abs.Q1s.format = 'mat';
% r.defaultfilename.rio_abs.Q1s.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/kil_1/rio_abs/1s/%Y/%m/%d/%Y%m%d%H.mat';
% r.defaultfilename.rio_abs.Q1s.loadfunction = '';
% r.defaultfilename.rio_abs.Q1s.resolution = timespan(00, 'h', 00, 'm', 01, 's');
% r.defaultfilename.rio_abs.Q1s.savefunction = '';
% r.defaultfilename.rio_abs.Q1s.size = [50 3600];
% r.defaultfilename.rio_abs.Q1s.units = 'dB';

r.defaultfilename.rio_qdc.default.archive = 'default';
r.defaultfilename.rio_qdc.default.dataclass = 'double';
r.defaultfilename.rio_qdc.default.defaultarchive = true;
r.defaultfilename.rio_qdc.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc.default.failiffilemissing = true;
r.defaultfilename.rio_qdc.default.format = 'mat';
% r.defaultfilename.rio_qdc.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/kil_1/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.fstr = '/data/riometer/kar_2/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc.default.loadfunction = '';
r.defaultfilename.rio_qdc.default.resolution = timespan(1, 's');
r.defaultfilename.rio_qdc.default.savefunction = '';
r.defaultfilename.rio_qdc.default.size = [1 86164];
r.defaultfilename.rio_qdc.default.units = 'dBm';


r.defaultfilename.rio_qdc_fft.default.archive = 'default';
r.defaultfilename.rio_qdc_fft.default.dataclass = 'double';
r.defaultfilename.rio_qdc_fft.default.defaultarchive = true;
r.defaultfilename.rio_qdc_fft.default.duration = timespan(14 , 'd');
r.defaultfilename.rio_qdc_fft.default.failiffilemissing = true;
r.defaultfilename.rio_qdc_fft.default.format = 'mat';
r.defaultfilename.rio_qdc_fft.default.fstr = '/data/riometer/kar_2/rio_qdc/%Y/%Y%m%d/%Y%m%db%Q.mat';
r.defaultfilename.rio_qdc_fft.default.loadfunction = '';
r.defaultfilename.rio_qdc_fft.default.resolution = timespan(1, 's');
r.defaultfilename.rio_qdc_fft.default.savefunction = '';
r.defaultfilename.rio_qdc_fft.default.size = [1 -1];
r.defaultfilename.rio_qdc_fft.default.units = 'dBm';
% r.defaultfilename.rio_qdc_mean_sd.default.archive = 'default';
% r.defaultfilename.rio_qdc_mean_sd.default.dataclass = 'double';
% r.defaultfilename.rio_qdc_mean_sd.default.defaultarchive = true;
% r.defaultfilename.rio_qdc_mean_sd.default.duration = timespan(00, 'h', 00, 'm', 00, 's');
% r.defaultfilename.rio_qdc_mean_sd.default.failiffilemissing = true;
% r.defaultfilename.rio_qdc_mean_sd.default.format = 'mat';
% r.defaultfilename.rio_qdc_mean_sd.default.fstr = ['/data/riometer/kar_2/rio_qdc_mean_sd/%Y/b%Q.mat';
% r.defaultfilename.rio_qdc_mean_sd.default.loadfunction = '';
% r.defaultfilename.rio_qdc_mean_sd.default.resolution = timespan(1, 's');
% r.defaultfilename.rio_qdc_mean_sd.default.savefunction = '';
% r.defaultfilename.rio_qdc_mean_sd.default.size = [1 86400];
% r.defaultfilename.rio_qdc_mean_sd.default.units = 'dBm';


r.institutions = {'Polar research Insitute of China', 'The Icelandic Centre for Research'};
% end of function
