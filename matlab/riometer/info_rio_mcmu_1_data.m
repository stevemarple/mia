function r = info_rio_mcmu_1_data
%INFO_RIO_MCMU_1_DATA Return basic information about rio_mcmu_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_mcmu_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=mcmu;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'mcmu';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [0];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [60];
r.bibliography = 'art:750';
r.comment = 'Formerly CANOPUS, now NORSTAR.';
r.credits = 'NORSTAR riometer data (formerly CANOPUS) is supplied courtesy of the University of Calgary.';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = 'NORSTAR';
r.facility_url = 'http://cgsm.ca/norstar';
r.facilityid = 1;
r.groupids = [1];
r.iantennatype = '';
r.ibeams = [];
r.id = 27;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 56.66;
r.location1 = 'Fort McMurray';
r.location1_ascii = '';
r.location2 = 'Canada';
r.logo = 'uni_calgary_logo';
r.logurl = '';
r.longitude = -111.21;
r.modified = timestamp([2013 12 16 15 01 21.688661]);
r.name = '';
r.piid = 5;
r.qdcclass = 'rio_rawqdc';
r.qdcduration = timespan(14 , 'd');
r.qdcoffset = timespan(00, 'h', 00, 'm', 00, 's');
r.resolution = timespan(00, 'h', 00, 'm', 05, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [1];
r.starttime = timestamp([1989 01 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 3e+07;
r.widebeams = [1];
r.zenith = [0];
r.defaultfilename.rio_abs.default.archive = 'default';
r.defaultfilename.rio_abs.default.dataclass = 'double';
r.defaultfilename.rio_abs.default.defaultarchive = true;
r.defaultfilename.rio_abs.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.default.failiffilemissing = false;
r.defaultfilename.rio_abs.default.format = 'miapy_h5';
r.defaultfilename.rio_abs.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/norstar/%Y/%m/%d/mcmu_%Y-%m-%d.h5';
r.defaultfilename.rio_abs.default.loadfunction = '';
r.defaultfilename.rio_abs.default.resolution = timespan(00, 'h', 00, 'm', 05, 's');
r.defaultfilename.rio_abs.default.savefunction = '';
r.defaultfilename.rio_abs.default.size = [1 17280];
r.defaultfilename.rio_abs.default.units = 'dB';
r.defaultfilename.rio_abs.FTP.archive = 'FTP';
r.defaultfilename.rio_abs.FTP.dataclass = 'double';
r.defaultfilename.rio_abs.FTP.defaultarchive = false;
r.defaultfilename.rio_abs.FTP.duration = timespan(1 , 'd');
r.defaultfilename.rio_abs.FTP.failiffilemissing = false;
r.defaultfilename.rio_abs.FTP.format = 'canopus';
r.defaultfilename.rio_abs.FTP.fstr = '@data_location_rio_canopus';
r.defaultfilename.rio_abs.FTP.loadfunction = '';
r.defaultfilename.rio_abs.FTP.resolution = timespan([], 's');
r.defaultfilename.rio_abs.FTP.savefunction = '';
r.defaultfilename.rio_abs.FTP.size = [1 17280];
r.defaultfilename.rio_abs.FTP.units = 'dB';
r.defaultfilename.rio_rawpower.default.archive = 'default';
r.defaultfilename.rio_rawpower.default.dataclass = 'double';
r.defaultfilename.rio_rawpower.default.defaultarchive = true;
r.defaultfilename.rio_rawpower.default.duration = timespan(1 , 'd');
r.defaultfilename.rio_rawpower.default.failiffilemissing = false;
r.defaultfilename.rio_rawpower.default.format = 'miapy_h5';
r.defaultfilename.rio_rawpower.default.fstr = 'http://spears.lancs.ac.uk/miadata/riometer/norstar/%Y/%m/%d/mcmu_%Y-%m-%d.h5';
r.defaultfilename.rio_rawpower.default.loadfunction = '';
r.defaultfilename.rio_rawpower.default.resolution = timespan(00, 'h', 00, 'm', 05, 's');
r.defaultfilename.rio_rawpower.default.savefunction = '';
r.defaultfilename.rio_rawpower.default.size = [1 17280];
r.defaultfilename.rio_rawpower.default.units = 'V';
r.institutions{1} = 'University of Calgary';
% end of function
