function r = info_rio_kir_2_data
%INFO_RIO_KIR_2_DATA Return basic information about rio_kir_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_kir_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=kir;serialnumber=2

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'kir';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([2020 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 91;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 67.84;
r.location1 = 'Kiruna';
r.location1_ascii = '';
r.location2 = 'Sweden';
r.logo = '';
r.logurl = '';
r.longitude = 20.42;
r.modified = timestamp([2011 07 08 09 57 02.217817]);
r.name = '';
r.piid = 11;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.standardobliquity = [];
r.starttime = timestamp([2003 01 01 00 00 00]);
r.systemtype = 'widebeam';
r.url = 'http://www.irf.se/';
r.url2 = {'http://www.irf.se/'};
r.wantennatype = 'la jolla';
r.wbeams = 1;
r.wfrequency = 3.82e+07;
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'IRE';
% end of function
