function r = info_rio_thl_1_data
%INFO_RIO_THL_1_DATA Return basic information about rio_thl_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about rio_thl_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=riometer;abbreviation=thl;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'thl';
r.antennaazimuth = 0;
r.antennaphasingx = [];
r.antennaphasingy = [];
r.antennas = [];
r.antennaspacing = [];
r.azimuth = [];
r.badbeams = [];
r.beamplanc = [];
r.beamplanr = [];
r.beamwidth = [];
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'DMI riometer chain';
r.facility_url = '';
r.facilityid = 6;
r.groupids = [];
r.iantennatype = '';
r.ibeams = [];
r.id = 49;
r.ifrequency = [];
r.imaging = false;
r.imagingbeams = [];
r.latitude = 77.5;
r.location1 = 'Thule';
r.location1_ascii = '';
r.location2 = 'Greenland';
r.logo = '';
r.logurl = '';
r.longitude = -69.2;
r.modified = timestamp([2009 05 11 11 53 12.664619]);
r.name = '';
r.piid = 4;
r.qdcclass = '';
r.qdcduration = timespan([], 's');
r.qdcoffset = timespan([], 's');
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.standardobliquity = [];
r.starttime = timestamp([]);
r.systemtype = 'widebeam';
r.url = '';
r.url2 = {};
r.wantennatype = '';
r.wbeams = [];
r.wfrequency = [];
r.widebeams = [];
r.zenith = [];
r.defaultfilename = [];
r.institutions{1} = 'Danish Meteorological Institute';
% end of function
