function r = canusefigures
% CANUSEFIGURES Check if figures can be displayed
%
%   r = CANUSEFIGURES
%   r: LOGICAL
%
% CANUSEFIGURES indicates if FIGUREs can be displayed; always true for
% Windows but not always true for other systems.

if ispc
  r = true;
else
  r = ~strcmp(get(0,'TerminalProtocol'), 'none');
end
