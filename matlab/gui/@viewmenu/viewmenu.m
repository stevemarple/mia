function h = viewmenu(varargin)
%VIEWMENU Create a viewmenu object, or add another window to an existing one.
%
%   h = VIEWMENU(newfh);
%   h = VIEWMENU(fh, newfh);
%
%   fh: FIGURE handle to an object managed a VIEWMENU object.
%   newfh: handle to a FIGURE which should become managed by a VIEWMENU
%   object.
%
%   If the figure already has a menu with the tag 'viewmenutop' then that
%   viewmenu is used. By this method an invisible UIMENU can act as a
%   place holder for where the VIEWMENU should be inserted.
%
%   See also IRISMENU, FIGURE, UIMENU.

% FUNCTIONS
% topLevelMenu  Common code for a top-level menu
% subMenu       Common code for a sub-menu

% The class data is stored in a struct field of a FIGURE UserData area. It
% is possible that the figure will be closed, and destroy the class
% information. To prevent such data loss the figure's DestroyFcn callback
% should ensure that the figure is removed from the list of available
% figures, and move the class data if necessary. The callback on the figure
% is automatically set to do this. Changes to the figure's DestroyFcn should
% append or prepend calls to other functions, or else the replacement
% function should arrange to call viewmenucb(vm, 'close'). The callbacks
% which handle moving data and closing windows should be set
% {'Interruptible', 'no'} to prevent race conditions.


% member fields
vm.container = [];       % handle to figure whos UserData contains the class
vm.figureHandles = [];   % figures managed by this class instance
vm.menuHandles = [];     % the handles of the top-level UIMENUs
% vm.submenuHandles = [];  % the handles of the sub-menu objects

switch nargin
  case 0
    % not sure this is a useful case - but is normally needed for loading
    % from disk. Return a viewdata object, since there is no handle.
    h = class(vm, 'viewmenu');
    return;
    
  case 1
    % vm.container = varargin{1};
    vm.figureHandles = varargin{1};
    % create a top-level menu
    newMenuHandle = topLevelMenu(varargin{1});
    vm.menuHandles = newMenuHandle;
    
    % make the container object the toplevel uimenu
    vm.container = newMenuHandle;
    
    % create a submenu item which will select the initial window
    subMenu(newMenuHandle, varargin{1});
    
    vm = class(vm, 'viewmenu');
    
    update(vm);

    % make sure we are notified if the window dies
    appendcallback(varargin{1}, 'DeleteFcn', ...
    'viewmenucb(getviewmenu(gcbf),''deletewin'')'); 


  case 2
    % args are parent window (any relevant one), and new window.
    % get class data
    vm = getviewmenu(varargin{1});
    
    % In the new figure create a top-level menu
    newMenuHandle = topLevelMenu(varargin{2});
    vm.menuHandles = [vm.menuHandles newMenuHandle];
    
    % Store where the class data is kept
    set(newMenuHandle, 'UserData', vm.container);
    
    % on new figure create submenus to all to all existing figures
    for h = vm.figureHandles
      subMenu(newMenuHandle, h);
    end
    
    % add new figure to list
    vm.figureHandles = [vm.figureHandles varargin{2}];
    
    % create a new submenu control, store the figure handle in its
    % UserData and set the callback to go to the figure by itself. Do
    % this for all menuHandles
    for h = vm.menuHandles
      subMenu(h, varargin{2});
    end
    
      
    % % update the UserData area of the figure to include the handle to the
    %     % container
    %     newFigureUD = get(varargin{2}, 'UserData');
    %     newFigureUD.viewmenu = vm.container;
    %     set(varargin{2}, 'UserData', newFigureUD);
    
        
    % update the class object
    update(vm);
    % make sure we are notified if the window dies
    appendcallback(varargin{2}, 'DeleteFcn', ...
    'viewmenucb(getviewmenu(gcbf),''deletewin'')'); 
     
  otherwise
    error('Incorrect number of parameters');
    
end


function h = topLevelMenu(parent)
% check if a top-level menu exists. If so then set the properties
% correctly and return the handle.
h = findobj(parent, 'Tag', 'viewmenutop');
if length(h) == 0
  % none exist
  h = uimenu(parent, 'Label', '&Views', ...
      'Callback', 'viewmenucb(getviewmenu(gcbf),''top'')', ...
      'Interruptible', 'off', ...
      'Tag', 'viewmenutop');
else
  if length(h) > 1
    warning(['figure ' num2str(parent) ' has multiple viewmenus']);
    h = h(1); % take first
  end
  set(h, 'Label', '&Views', ...
      'Callback', 'viewmenucb(getviewmenu(gcbf),''top'')', ...
      'Interruptible', 'off', ...
      'Visible', 'on');
end  

filemenuh = findobj(parent, 'Type', 'uimenu', 'Tag', 'file');
if ~isempty(filemenuh)
  uimenu(filemenuh, 'Label', 'Close dataset', ...
      'Callback',  'viewmenucb(getviewmenu(gcbf),''closedataset'')', ...
      'Interruptible', 'off', ...
      'Separator', 'off', ...
      'Tag', 'viewclosedataset');
end
return;
    

function h = subMenu(parent, figHandle)
h = uimenu(parent, ...
    'UserData', figHandle, ...
    'Callback', 'figure(get(gcbo, ''UserData''))', ...
    'Interruptible', 'off', ...
    'Tag', 'viewmenusub');
% does matlab 5.1 have a Position field? If not then better return right
% now
return;
% swap last two entries so that close data set is always at the bottom
kids = get(parent, 'Children');
nkids = length(kids);
if nkids >= 2
  penult = findobj(kids, 'flat', 'Position', nkids-1);
  last = findobj(kids, 'flat', 'Position', nkids);
  set([penult; last], {'Position'}, {nkids; nkids-1});
end

return;







