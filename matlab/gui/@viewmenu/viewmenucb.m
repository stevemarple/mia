function viewmenucb(varargin)
%VIEWMENUCB Callback function for VIEWMENU class.
%
% VIEWMENUCB(vm, ...)
% vm: VIEWMENU object
%
% See also VIEWMENU.

vm = varargin{1};

if ~isa(vm, 'viewmenu')
  error('first argument should be a viewmenu object');
end

switch varargin{2}
  case 'top'
    % A top level menu is selected. Update the labels on the submenus.
    for h = get(gcbo, 'Children')' % vm.submenuHandles
      if ~strcmp(get(h, 'Tag'), 'viewclosedataset')
	targetFigHandle = get(h, 'UserData');
	set(h, 'Label', get(targetFigHandle, 'Name'));
      end
    end
   
  case 'deletewin'
    illWindowHandle = gcbf; % the window which will soon die
    
    if length(vm.figureHandles) == 1
      % only one window and it is going, no need to move data
      return;
    
    elseif illWindowHandle == get(vm.container, 'Parent')
      % the figure whose Viewmenu stores the class data is going to die
      vm.figureHandles = setxor(vm.figureHandles, illWindowHandle);
      

      % remove the affected menu handle from the list
      vm.menuHandles = setxor(vm.menuHandles, vm.container);
      oldContainer = vm.container;
      
      % choose another top-level uimenu object to be the container
      vm.container = vm.menuHandles(1);
      
      % remove any affected submenus from the windows which will stay
      h = findobj(vm.menuHandles, 'UserData', illWindowHandle);
      delete(h);
      
      % set the UserData in the non-container objects to point to the new
      % container 
      set(vm.menuHandles(2:prod(size(vm.menuHandles))), ...
	  'UserData', vm.container);
      
      % update to reflect all the changes. This will update
      % vm(menuHandles(1)) - the container
      update(vm);
      
    else
      % remove the figure from our list
      vm.figureHandles = setxor(vm.figureHandles, illWindowHandle); 
      
      % remove the soon-to-die top-level uimenu from our list
      h = findobj(vm.menuHandles, 'flat', 'Parent', illWindowHandle);
      delete(h);
      vm.menuHandles = setxor(vm.menuHandles, h);
    
      % remove redundant submenus. Search the remaining toplevel menu
      % handles, so that their children (the submenus) are searched for
      % UserData which is the same as the handle of the window which is
      % about to die.
      h = findobj(vm.menuHandles, 'UserData', illWindowHandle);
      delete(h);
      
      update(vm);
    end
    
    return;
    % code below effectively commented out
    
    
    % find the top-level menu handle in the affected window and delete it.
    % This will delete the children and make searching for the other
    % affected children quicker.
    h = findobj(vm.menuHandles, 'flat', 'Parent', illWindowHandle);
    delete(h);
    vm.menuHandles = setxor(vm.menuHandles, h);
    
    % Search the remaining toplevel menu handles, so that their children
    % (the submenus) are searched for UserData which is the same as the
    % handle of the window which is about to die.
    h = findobj(vm.menuHandles, 'UserData', illWindowHandle);
    delete(h);
    
    % remove the ill window from our list
    vm.figureHandles = setxor(vm.figureHandles, illWindowHandle);
    
    if vm.container ~= illWindowHandle
      % record the changes
      update(vm);
    elseif ~isempty(vm.figureHandles)
      % there are still windows remaining, shift the data quickly
      % before this window goes! 
      vm.container = vm.figureHandles(1);
      
      % fix the UserData areas to point to the new container
      for n = 2:prod(size(vm.figureHandles))
	ud = get(vm.figureHandles(n), 'UserData');
	ud.viewmenu = vm.container;
	set(vm.figureHandles(n), 'UserData', ud);
      end
      update(vm);
    end

  case 'closedataset'
    vmsh = findobj(gcbf, 'Type', 'uimenu', 'Tag', 'viewmenusub')';
    if length(vmsh) > 1
      button = questdlg('Close data set?', 'Close data set?', ...
	  'Ok','Cancel','Cancel');
      if strcmp(button, 'Cancel');
	return;
      end
    end
    targetFigHandles = get(vmsh, 'UserData');
    if iscell(targetFigHandles)
      close([targetFigHandles{:}]);
    else
      close(targetFigHandles);
    end
    return;
    
  otherwise
    error('unknown action');
    
end

