function r = getfigurehandles(vm)
%GETFIGUREHANDLES  Find the figures managed by a VIEWMENU object.
%
%   r = GETFIGUREHANDLES(vm)
%   r: vector of FIGURE handles
%   vm: VIEWMENU object
%
%   See also VIEWMENU.

r = vm.figureHandles;
