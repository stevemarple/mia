function update(vm)
%UPDATE  Update the class object stored in the UserData area.
%
%   UPDATE(vm)
%   vm: VIEWMENU object
%
%   The viewmenu object contains the handle of a graphics UserData area
%   where the original object is stored. UPDATE modifies the UserData
%   copy to be the same as the object passed to this function. This is a
%   necessary workaround as Matlab does not implement pointers.
%
%   See also VIEWMENU.

set(vm.container, 'UserData', vm);

