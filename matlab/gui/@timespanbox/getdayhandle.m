function h = getdayhandle(tsb)
% GETDAYHANDLE Get the handle of the day NUMBERBOX (if present, else [])

h = tsb.dayhandle;
