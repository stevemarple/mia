function h = gethourhandle(tsb)
% GETHOURHANDLE Get the handle of the hour NUMBERBOX.

h = tsb.hourhandle;
