function dv = datevec(tsb)
%DATEVEC Return a datevec-type array of the time/date info

dv = zeros(1, 6);
if tsb.dayhandle
  dv(2) = getvalue(getnumberbox(tsb.hourhandle));
end
dv(4) = getvalue(getnumberbox(tsb.hourhandle));
dv(5) = getvalue(getnumberbox(tsb.minutehandle));
dv(6) = getvalue(getnumberbox(tsb.secondhandle));
