function ts = gettimespan(tsb)
% GETTIMESPAN Get a TIMESPAN object from a TIMESPANBOX
% ts = gettimespan(tsb)

dh = getdayhandle(tsb);
if dh
  ts = timespan(getvalue(getnumberbox(dh)), 'd', ...
      getvalue(getnumberbox(gethourhandle(tsb))), 'h', ...
      getvalue(getnumberbox(getminutehandle(tsb))), 'm', ...    
      getvalue(getnumberbox(getsecondhandle(tsb))), 's');
else
  ts = timespan(getvalue(getnumberbox(gethourhandle(tsb))), 'h', ...
      getvalue(getnumberbox(getminutehandle(tsb))), 'm', ...    
      getvalue(getnumberbox(getsecondhandle(tsb))), 's');
end



