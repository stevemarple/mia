function h = gethandle(tsb)
%GETHANDLE Return the handle.

deprecated('use gethandles');

if isempty(tsb.dayhandle)
  h = [tsb.hourhandle tsb.minutehandle tsb.secondhandle];
else
  h = [tsb.dayhandle tsb.hourhandle tsb.minutehandle tsb.secondhandle];
end
