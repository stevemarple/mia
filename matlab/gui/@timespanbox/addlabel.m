function varargout = addlabel(tsb, spacing, label)
%ADDLABEL  Add a label to a TIMESPANBOX.
%
%   h = ADDLABEL(tsb, pos, label)
%   [h pos] = ADDLABEL(..)
%   h: handle to text object
%   pos: position vector of label
%   tsb: TIMESPANBOX object
%   pos: position vector
%   label: text to label with
%
%   The label is added right-aligned to the left of the TIMESPANBOX, with
%   the given spacing. The default figure font and size are used.
%
%   See also TIMESPANBOX.

if ~isempty(tsb.dayhandle)
  prop = get(tsb.dayhandle);
else
  prop = get(tsb.hourhandle);
end

bgColour = get(prop.Parent, 'Color');

varargout{1} = uicontrol(prop.Parent, ...
    'Position', ...
    [prop.Position(1)-spacing-1 prop.Position(2) 1 prop.Position(4)], ... 
    'Style', 'text', ...
    'HorizontalAlignment', 'right', ...
    'String', label, ...
    'Enable', 'inactive', ...
    'BackgroundColor', bgColour);
pos = uiresize(varargout{1}, 'r', 'b');

if nargout > 1
  varargout{2} = pos;
end



