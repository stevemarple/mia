function handle = timespanbox(parent, pos, varargin)
%TIMESPANBOX  Create a TIMESPANBOX, return a handle to the uicontrol.
%
%   h = TIMESPANBOX(parent, pos);
%   h = TIMESPANBOX(parent, pos, daywidth, nearest)

% Unlike normal class constructors this one returns a handle to a graphics
% object, the actual class data is stored in the user data area. This is
% because the callbacks really need access to a reference, rather than an
% old static copy of what was in the datebox. By using one of the handles,
% and storing the class object in the UserData area of that handle then we
% effectively have a reference.

% Although the day uicontrol is displayed first, it is optional so the
% data is stored in the UserData area of hourhandle instead.

% member fields
tsb.dayhandle = []; % empty if the day entry box not used.
tsb.hourhandle = []; % object stored in userdata of hourhandle NUMBERBOX
tsb.minutehandle =[];
tsb.secondhandle = [];
tsb.colonhandles = [];
tsb.nearest = [];
tsb.userdata = [];

switch nargin
  case 3
    daywidth = varargin{1};

  case 4
    daywidth = varargin{1};
    if isa(varargin{2}, 'timespan') & varargin{2} > timespan
      tsb.nearest = varargin{2};
    else
      error('''nearest'' must be a TIMESPAN > 0s');
    end
  otherwise
    daywidth = [];
end

tsb = class(tsb, 'timespanbox');

[tsb.dayhandle tsb.hourhandle tsb.minutehandle ...
      tsb.secondhandle tsb.colonhandles] = ...
    maketimespanbox(tsb, parent, pos, daywidth);  

set(tsb.colonhandles, 'UserData', tsb.hourhandle);
% these are not normal uicontrols - UserData area already in use
setuserdata(getnumberbox(tsb.hourhandle), tsb);
setuserdata(getnumberbox(tsb.minutehandle), tsb.hourhandle);
setuserdata(getnumberbox(tsb.secondhandle), tsb.hourhandle);

% numberbox already has callback so append one
appendcallback(tsb.hourhandle, ...
    'timespanboxcb(getuserdata(getnumberbox(gcbo)));'); 

appendcallback(tsb.minutehandle, ...
    'timespanboxcb(getuserdata(getnumberbox(gcbo)));'); 

appendcallback(tsb.secondhandle, ...
    'timespanboxcb(getuserdata(getnumberbox(gcbo)));'); 

if tsb.dayhandle
  setuserdata(getnumberbox(tsb.dayhandle), tsb.hourhandle);
  appendcallback(tsb.dayhandle, ...
    'timespanboxcb(getuserdata(getnumberbox(gcbo)));'); 
end




% update and initialise
settimespan(tsb,timespan);
update(tsb);

handle = double(tsb.hourhandle);


