function settimespan(tsb, ts)
% SETTIMESPAN Set the TIMESPAN displayed in a TIMESPANBOX

if ~isempty(tsb.nearest)
  ts = round(ts, tsb.nearest);
end  
[d h m s] = getdhms(ts);

if tsb.dayhandle;
  setvalue(getnumberbox(tsb.dayhandle), d);
end
setvalue(getnumberbox(tsb.hourhandle), h);
setvalue(getnumberbox(tsb.minutehandle), m);
setvalue(getnumberbox(tsb.secondhandle), s);

