function setuserdata(tsb, data)
%SETUSERDATA Set the user data area.

tsb.userdata = data;
update(tsb);

