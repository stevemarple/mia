function h = getminutehandle(tsb)
% GETMINUTEHANDLE Get the handle of the minute NUMBERBOX.

h = tsb.minutehandle;
