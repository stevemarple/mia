function h = gethandles(tsb)
%GETHANDLE  Return the uicontrol handles used in a TIMESPANBOX.
%
%   h = GETHANDLE(tb)
%   h: column vector of handles to UICONTROL objects
%   tb: TIMESPANBOX object
%
%   See also TIMESPANBOX.

if isempty(tsb.dayhandle)
  h = [tsb.hourhandle; tsb.minutehandle; tsb.secondhandle];
else
  h = [tsb.dayhandle; tsb.hourhandle; tsb.minutehandle; tsb.secondhandle];
end
