function appendcallback(tsb, varargin)
%APPENDCALLBACK  Append a callback to a TIMESPANBOX.
%
%   APPENDCALLBACK(tsb, s)
%   tsb: TIMESPANBOX
%   s: callback string
%
%   APPENDCALLBACK appends a user defined callback to the UICONTROL
%   objects used by a TIMESPANBOX. After the TIMESPANBOX has been adjust
%   the callback is called.
%
%   See also TIMESPANBOX, APPENDCALLBACK.

h = [tsb.dayhandle tsb.hourhandle tsb.minutehandle tsb.secondhandle];

appendcallback(h, varargin{:});
