function data = getuserdata(tsb)
%GETUSERDATA Get the user data area.

data = tsb.userdata;

