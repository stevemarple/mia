function varargout = setposition(tsb, pos)
%SETPOSITION Set the position of a TIMESPANBOX.
%
%   setposition(tsb, pos)
%   tsb: TIMESPANBOX
%   pos: new position (width and height ignored)

% arrange so that first handle is left most and 2nd right most to make
% calculating newPos simpler
if isempty(tsb.dayhandle)
  handles = [tsb.hourhandle tsb.secondhandle tsb.minutehandle ...
	tsb.colonhandles]; 
else
  handles = [tsb.dayhandle tsb.secondhandle tsb.hourhandle ...
	tsb.minutehandle tsb.secondhandle tsb.colonhandles]; 
end

p = get(handles, 'Position');

% move all by the same amount
offset = [pos(1)-p{1}(1), pos(2)-p{1}(2), 0, 0];

for n = 1:length(p)
  p{n} = p{n} + offset;
end

set(handles, {'Position'}, p);

if nargout == 1
  % simple enough to do ourselves as all UICONTROLs (excluding text) are
  % the same type. Don't bother calling BOUNDINGBOX.
  varargout{1} = [p{1}(1:2), p{4}(1) + p{4}(3) - p{1}(1), p{1}(4)];
end

