function p = getposition(tsb)
%GETPOSITION Return the position matrix for a TIMESPANBOX object.
%
%   See also BOUNDINGBOX.

if tsb.dayhandle
  % day entry box exists
  p = boundingbox(tsb.dayhandle, tsb.hourhandle, tsb.minutehandle, ...
      tsb.secondhandle);
else
  p = boundingbox(tsb.hourhandle, tsb.minutehandle, tsb.secondhandle);
end
