function h = getsecondhandle(tsb)
% GETSECONDHANDLE Get the handle of the second NUMBERBOX

h = tsb.secondhandle;
