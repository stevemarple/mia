function setnearest(tsb, ts)
%SETNEAREST  Set the TIMESPAN boundary to which a TIMESPANBOX should round.
%
%   SETNEAREST(tsb, ts)
%   tsb: TIMESPANBOX
%   ts: TIMESPAN (> 0s) or [] to turn off rounding
%
%   See also TIMESPANBOX, TIMESPAN.

tsb.nearest = ts;
update(tsb);
