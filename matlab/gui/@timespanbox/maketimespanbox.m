function [dh, hh, mh, sh, ch] = maketimespanbox(tsb, parent, pos, daywidth)
%MAKETIMESPANBOX Function to hold all the calls to uicontrol
%
%

bgcolour = get(parent, 'Color');

% day
if daywidth
  dayPos = [pos(1) pos(2) 1 1];
  dh = numberbox(parent, dayPos, ...
      '%d', 'n=max(0,n);', 0); 
  dayPos = uiresize(dh, 'l', 'b', '99999');

  ch = zeros(1, 3); % colon handles
  cPos = zeros(3, 4);
  ch(3) = uicontrol(parent, 'Style', 'text', ... 
      'Position', [dayPos(1)+dayPos(3), pos(2), 1, dayPos(4)], ...
      'String', ',', 'HorizontalAlignment', 'center', ...
      'Enable', 'inactive', ...
      'BackgroundColor', bgcolour);
  cPos(3,:) = uiresize(ch(3), 'l', 'm');
  
  hourPos = [cPos(3,1)+cPos(3,3), pos(2), 1, 1];
else
  dh = [];
  ch = zeros(1, 2); % colon handles
  cPos = zeros(2, 4);
  hourPos = [pos(1) pos(2) 1 1];
end

% hour
hh = numberbox(parent, hourPos, '%02d', 'n=max(0,n);', 0); 
hourPos = uiresize(hh, 'l', 'b', '999');

% h:m
ch(2) = uicontrol(parent, 'Style', 'text', 'Position', ...
    [hourPos(1)+hourPos(3), pos(2), 1, hourPos(4)], ...
    'String', ':', 'HorizontalAlignment', 'center', ...
    'Enable', 'inactive', ...
    'BackgroundColor', bgcolour); 

cPos(2,:) = uiresize(ch(2), 'l', 'm', ':');

% minute
minutePos = [cPos(2,1)+cPos(2,3) pos(2) 1 1];
mh = numberbox(parent, minutePos, '%02d', 'n=max(0,n);', 0); 
minutePos = uiresize(mh, 'l', 'b', '999');

% m:s
ch(1) = uicontrol(parent, 'Style', 'text', 'Position', ...
    [minutePos(1)+minutePos(3), pos(2), 1, minutePos(4)], ...
    'String', ':', 'HorizontalAlignment', 'center', ...
    'Enable', 'inactive', ...
    'BackgroundColor', bgcolour); 
cPos(1,:) = uiresize(ch(1), 'l', 'm', ':');

% second
secondPos = [cPos(1,1)+cPos(1,3) pos(2) 1, 1];
sh = numberbox(parent, secondPos, '%02g', 'n=max(0,n);', 0); 
secondPos = uiresize(sh, 'l', 'b', '999');


if matlabversioncmp('>=', '5.2')
  % add tooltips
  set(dh, 'TooltipString', 'days');
  set(hh, 'TooltipString', 'hours');
  set(mh, 'TooltipString', 'minutes');
  set(sh, 'TooltipString', 'seconds');
end
  

