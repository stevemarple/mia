function timespanboxcb(tsb)
% TIMESPANBOXCB Callback function for TIMESPANBOX class
% May be called with either a TIMESPANBOX object, or the handle to a
% UICONTROL whose userdata contains a TIMESPANBOX object

if strcmp(class(tsb), 'double')
  tsb = getuserdata(getnumberbox(tsb));
end

% get the current values
ts = gettimespan(tsb);

% now write back, mapping 90s to 1m 30s etc
settimespan(tsb, ts);






