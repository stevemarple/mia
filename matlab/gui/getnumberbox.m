function nb = getnumberbox(handle)
%GETNUMBERBOX  Return a NUMBERBOX object from a uicontrol handle.
%
%   nb = GETNUMBERBOX(h)
%   nb: NUMBERBOX object
%   h: the handle associated with a NUMBERBOX object
%
%   The handle associated with a NUMBERBOX object is the one returned by
%   the constructor.
%
%   See also NUMBERBOX.

if isa(handle, 'numberbox')
  % already is a nb!
  warning('this already is a numberbox');
  nb = handle;
  return;

elseif ishandle(handle)
  nb = get(handle, 'UserData');
  if ~strcmp(class(nb), 'numberbox')
    error(['handle is not associated with a NUMBERBOX object, or' ...
	  ' UserData area corrupted (was class ' class(nb) ')']);
  end
  
else
  error('Not a valid handle');
end
    




