function moveuicontrols(h, pos, rel)
%MOVEUICONTROLS  Move UICONTROL objects, with relative or absolute position.
%
%   MOVEUICONTROLS(h, pos, rel);
%   h: CELL or DOUBLE array of handles
%   pos: CELL array, or [n x 4] DOUBLE array of position vector(s)
%   rel: 'a' for aboslute position, 'r' for relative position
%
%   If h is a vector then the number of positions given must be either
%   equal to prod(size(h)), or 1.

nmax = prod(size(h));

for n = 1:nmax
  if iscell(h)
    h1 = h{n};
  else
    h1 = h(n,:);
  end
  if size(pos,1) == 1
    pos1 = pos;
  elseif iscell(pos)
    pos1 = pos{n};
  else
    pos1 = pos(n,:);
  end
    
  curPos = get(h1, 'Position');
  if rel == 'a'
    % absolute
    set(h1, 'Position', [pos1(1:2) curPos(3:4)]);
  else
    % relative
    pos1 = curPos + [pos1(1:2) 0 0];
    set(h1, 'Position', pos1);
  end
  
  
end
