function varargout = newriodata(action, varargin)
%NEWRIODATA Make a menu for creating RIO_BASE objects.
%
%   NEWRIODATA
%   Create and initialise (same as NEWRIODATA('init')). 
%
%   NEWRIODATA(action)
%
%   Perform spcified action (used by callbacks).
%
%   NEWRIODATA('init', 'rio_abs')
%   NEWRIODATA('init', 'rio_power')
%   Make for creating RIO_ABS and RIO_POWER objects respectively.

% The checking of the data could happen after any of the items on this menu
% have been altered, by calling this function as part of the uiobjects'
% callbacks. I think that will be too slow, so perform the checking only
% when ok is pressed. It also gives the user a chance to correct any
% mistakes without being 'shouted at'. Give an error dialog box explaining
% the error, and hopefully with a button for help.
%
% For creating absorption data give the option of supplying a QDC. If a QDC
% is selected based on starting time don't load it there and then, just
% store the time (as if it were the QDC). Otherwise the QDC must be updated
% when various parameters (beams, instruments etc) are modified. It also has
% the advantage of loading the QDC (slow) after all the interactive stuff
% has been done.

badColor = [0.8 0 0];
day = timespan(1, 'd');

if nargin == 0
  action = 'init';
end

switch action
  % --------------------------------------------------------------
 case 'init'
  % valid datatypes, maybe add 'rio_qdc' later
  defaults.datatypes = {'rio_abs' 'rio_power' 'rio_rawpower' 'rio_qdc'}; 
  defaults.datatype = 'rio_abs'; % initial datatype
  
  defaults.qdc = [];  % QDC to use for absorption when in manual QDC mode
  [defaults unvi] = interceptprop(varargin, defaults);

  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text


  fh = figure('Name', ['New riometer data'], ...
	      'NumberTitle','off', ...
	      'HandleVisibility', 'callback', ...
	      'Visible', 'off', ...
	      'MenuBar', 'None', ...
	      'Resize', 'off');
  
  % grab userdata area in case some figure create function has started
  % accessing it as a struct
  
  ud = get(fh, 'UserData');
  % instruments could change according to the datatype selected, so store in
  % userdata area, but remember to change contents when changing
  % datatype, also change the instrument menu to suit
  ud.instruments = instrumenttypeinfo(riometer, 'supported', ...
				      'datatype', defaults.datatype);
  
  ud.instruments = sort(ud.instruments, ...
			{'locationname', 'serialnumber', 'latitude'});

  % current chosen instrument
  ud.instrument = preferredinstrument('riometer');
  if ~any(ud.instrument == ud.instruments)
    % does not exist in list
    ud.instrument = ud.instruments(1); 
  end
    
  
  ud.datatype = defaults.datatype; % currently selected datatype

  ud.qdc = defaults.qdc;  % current QDC
  ud.defaults = rmfield(defaults, 'qdc'); % could be too big to keep twice
  

  % Add standard menus
  helpData = {['New riometer data'], ...
	      ['miahelp(''/' defaults.datatype '/creating.html'')'], ...
	      'help_newdata'};
  miamenu(fh, 'help', helpData);
  
  % Add standard toolbar
  toolbarh = miatoolbar('init', ...
			'parent', fh);

  bgColor = get(fh, 'Color');

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'Resolution method:  ');
  textPos = uiresize(textH, 'l', 'b');

  delete(textH);
  
  % allocate space for the handles and positions
  genH     = zeros(21,1);   % handles to uicontrols in general frame
  genPos   = zeros(21,4);   % positions  "                    "

  % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the button widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  % frameWidth = 3.9 * textPos(3);
  frameWidth = 5 * textPos(3);
  
  lines = 12;
  listboxlines = 5;
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+(lines+1)*textPos(4)+...
       ((lines+1+listboxlines)*lineSpacing)]; 
  
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', bgColor, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'General' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'New riometer data', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');

  % 'Data type' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Data type: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');
  
  % x coord of RH column in figure
  textPos(3);
  rhColDiff = spacing(1) + 30;
  rhCol = genPos(1,1) + textPos(3) + rhColDiff;
  datatypeStr = cell(size(ud.defaults.datatypes));
  for n = 1:length(datatypeStr)
    datatypeStr{n} = gettype(feval(ud.defaults.datatypes{n}), 'c');
  end
  
  genPos(4,:) = [rhCol genPos(3,2) 1 1];
  genH(4) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'HorizontalAlignment', 'left', ...
		      'Value', find(strcmp(defaults.datatype, ...
					   ud.defaults.datatypes)), ...
		      'String', datatypeStr, ...
		      'Position', genPos(4,:), ...
		      'Callback', [mfilename '(''datatype'');'], ...
		      'Tag', 'datatypemenu');
  genPos(4,:) = uiresize(genH(4), 'l', 'b');

  genPos(5,:) = genPos(3,:) - [0 genPos(3,4)+lineSpacing 0 0];
  genH(5) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Instrument: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(5,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');

  % genPos(6,:) = [rhCol genPos(5,2) 1 1];
  genPos(6,:) = [rhCol genPos(5,2)+genPos(5,4) 1 1];

  % make a list of instruments containing all the  instruments we could
  % print in the popup menu. Use this list when calling uiresize. Will do
  % updateinstruments later to get the right items in the list

  tmpInstruments = instrumenttypeinfo(riometer, 'supported', ...
				      'datatype', ud.defaults.datatype);
  tmpInstruments = sort(tmpInstruments, ...
			{'locationname', 'serialnumber', 'latitude'});
  instrumentsStr = localInstruments2Str(tmpInstruments);
  
  instrumentsStrLen = zeros(size(instrumentsStr));
  for n = 1:length(instrumentsStr)
    instrumentsStrLen = length(instrumentsStr{n});
  end
  
  % genH(6) = uicontrol(fh, 'Style', 'popupmenu', ...
  genH(6) = uicontrol(fh, 'Style', 'listbox', ...
		      'HorizontalAlignment', 'left', ...
		      'String', instrumentsStr, ...
		      'Value', find(ud.instrument == ud.instruments), ...
		      'Position', genPos(6,:), ...
		      'Callback', [mfilename '(''instrument'');'], ...
		      'Tag', 'instrumentmenu');
  ud.instrumentmenu = genH(6);
  
  if strcmp(get(genH(6), 'Style'), 'popupmenu')
    genPos(6,:) = uiresize(genH(6), 'l', 'b');
  else
    % resize, allowing upto listboxlines entries in list, and strlength of
    % 25Ms
    genPos(6,:) = uiresize(genH(6), 'l', 't', repmat('M', listboxlines, 25));
  end
  genPos(5,2) = genPos(6,2);
  set(genH(5), 'Position', genPos(5,:));
  
  genPos(7,:) = [rhCol 10 0 0];
  
  % create start/end box and find size of it
  ud.steth = stetbox(fh, genPos(7,:));
  sb = getstetbox(ud.steth);

  appendcallback(getstetbox(ud.steth), ...
		 [mfilename '(''updatesamples'');']);
  
  sbPos = getposition(sb);
  genPos(7,:) = [rhCol, genPos(6,2)-sbPos(4)-lineSpacing, 0, 0];
  setposition(sb, genPos(7,:));
  
  
  % addlabel(sb, rhColDiff, ...
  %  not sure why this is, sort out later
  addlabel(sb, rhColDiff - spacing(1), ...
	   {'Start time: ', 'End time: ', 'Duration: '});
  sbPos = getposition(sb);

  
  [imagingBeams wideBeams badBeams] = info(ud.instrument, 'beams');

  genPos(8,:) = [genPos(3,1), genPos(7,2)-lineSpacing-textPos(4), ...
		 genPos(3,3:4)];

  genPos(9,:) = [rhCol genPos(8,2:4)];
  genH(9) = timespanbox(fh, genPos(9,:), 5, ...
			info(ud.instrument, 'bestresolution'));
  genH(8) = addlabel(gettimespanbox(genH(9)), rhColDiff-10, 'Resolution: ');
  ud.resh = genH(9);
  appendcallback(gettimespanbox(ud.resh), ...
		 [mfilename '(''updateresolution'');']);
		 % [mfilename '(''updatesamples'');']);
  
  genPos(20,:) = [genPos(3,1), genPos(8,2)-lineSpacing-textPos(4), ...
		  genPos(3,3:4)];
  
  % resolution method
  genH(20) = uicontrol(fh, 'Position', genPos(20,:), ...
		       'Style', 'text', ...
		       'HorizontalAlignment', 'right', ...
		       'String', 'Resolution method: ', ...
		       'Enable', 'inactive', ...
		       'BackgroundColor', bgColor);
  ud.resdirection = 'off';
  ud.decreaseresmethods.mean = 'Mean';
  ud.decreaseresmethods.nonanmean = 'No NaN mean';
  ud.decreaseresmethods.median = 'Median';
  ud.decreaseresmethods.nonanmedian = 'No NaN median';
  ud.increaseresmethods.interp = 'Interpolate (''interp'')';
  ud.increaseresmethods.interp = 'Linear interpolation (''interp/linear'')';
  ud.increaseresmethods.interp = 'Spline interpolation (''interp/spline'')';
  ud.increaseresmethods.interp = 'Cubic interpolation (''interp/cubic'')';  
  ud.increaseresmethods.interp = 'Nearest neighbour interpolation (''interp/nearest'')';  
  
  genPos(21,:) = [rhCol genPos(20,2:4)];
  genH(21) = uicontrol(fh, 'Position', genPos(21,:), ...
		       'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'left', ...
		       'String', ' ', ...
		       'Visible', 'off', ...
		       'Enable', 'on', ...
		       'Value', 1, ...
		       'UserData', '', ...
		       'BackgroundColor', bgColor);
  genPos(21,:) = uiresize(genH(21), 'l', 'b', '9999999999');
  ud.resmethodh = genH(21);
  ud.resdirection = [];
  
  % genPos(10,:) = [genPos(3,1), genPos(8,2)-lineSpacing-textPos(4), ...
  %                 genPos(3,3:4)];
  genPos(10,:) = [genPos(3,1), genPos(20,2)-lineSpacing-textPos(4), ...
		  genPos(20,3:4)];
  
  genH(10) = uicontrol(fh, 'Position', genPos(10,:), ...
		      'Style', 'text', ...
		      'HorizontalAlignment', 'right', ...
		      'String', 'No. of samples: ', ...
		      'Enable', 'inactive', ...
		      'BackgroundColor', bgColor);
  
  genPos(11,:) = [rhCol genPos(10,2:4)];
  genH(11) = uicontrol(fh, 'Position', genPos(11,:), ...
		      'Style', 'text', ...
		      'HorizontalAlignment', 'left', ...
		      'String', '', ...
		      'Enable', 'on', ...
		      'BackgroundColor', bgColor);
  genPos(11,:) = uiresize(genH(11), 'l', 'b', '9999999999');
  ud.sampleh = genH(11);
  
  genPos(12,:) = [genPos(3,1), genPos(10,2)-lineSpacing-textPos(4), ...
		  genPos(3,3:4)];
  genH(12) = uicontrol(fh, 'Position', genPos(12,:), ...
		      'Style', 'text', ...
		      'HorizontalAlignment', 'right', ...
		      'String', 'Beams: ', ...
		      'Enable', 'inactive', ...
		      'BackgroundColor', bgColor);

  genPos(13,1) = genPos(4,1);
  genPos(13,2:4) = genPos(12,2:4);
  % ud.beamh = beambox(fh, genPos(13,:), imagingBeams, wideBeams, ...
  % 		     [' for new ']); 

  genH(13) = beambox('parent', fh, ...
		     'position', genPos(13,:), ...
		     'imaging', imagingBeams, ...
		     'imagingallow', imagingBeams, ...
		     'widebeam', wideBeams, ...
		     'widebeamallow', wideBeams, ...
		     'badbeams', badBeams, ...
   		     'title', [' for new ' ...
		    gettype(feval(ud.datatype), 'l')]); 
  
  ud.beamh  = genH(13);
  genPos(13,:) = getposition(getbeambox(ud.beamh));
  
  
  genPos(14,:) = [genPos(3,1), genPos(12,2)-lineSpacing-textPos(4), ...
		  genPos(3,3:4)];
  genH(14) = uicontrol(fh, 'Position', genPos(14,:), ...
		       'Style', 'text', ...
		       'HorizontalAlignment', 'right', ...
		       'String', 'QDC: ', ...
		       'Enable', 'inactive', ...
		       'BackgroundColor', bgColor);
  ud.qdctexth = genH(14);
  
  genPos(15,:) = [rhCol genPos(14,2) 1 1];
  genH(15) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'left', ...
		       'Value', 1, ...
		       'String', ...
		       {'Auto', 'Manual (by date)', 'Manual (from file)'}, ... 
		       'Position', genPos(15,:), ...
		       'Callback', [mfilename '(''qdc'');'], ...
		       'Tag', 'qdctype');
  genPos(15,:) = uiresize(genH(15), 'l', 'b');
  ud.qdctypeh = genH(15);
  
  genPos(16,1) = genPos(15,1) + genPos(15,3) + spacing(1);
  genPos(16,2:4) = genPos(15,2:4);
  genH(16) = uicontrol(fh, 'Style', 'pushbutton', ...
		       'HorizontalAlignment', 'center', ...
		       'Value', 1, ...
		       'String', 'Select', ... 
		       'Position', genPos(16,:), ...
		       'Callback', [mfilename '(''qdcselect'');'], ...
		       'Tag', 'qdcselect');
  ud.qdcselecth = genH(16);
  genPos(16,:) = uiresize(genH(16), 'l', 'b');

  genPos(17,1) = genPos(16,1) + genPos(16,3) + spacing(1);
  genPos(17,2:4) = genPos(15,2:4);
  genH(17) = uicontrol(fh, 'Style', 'pushbutton', ...
		       'HorizontalAlignment', 'center', ...
		       'Value', 1, ...
		       'String', 'Details', ... 
		       'Position', genPos(17,:), ...
		       'Callback', [mfilename '(''qdcdetails'');'], ...
		       'Tag', 'qdcdetails');
  genPos(17,:) = uiresize(genH(17), 'l', 'b');
  ud.qdcdetailsh = genH(17);
    
  
  genPos(18,:) =  [genPos(3,1), genPos(14,2)-lineSpacing-textPos(4), ...
		   genPos(3,3:4)];

  genH(18) = uicontrol(fh, 'Position', genPos(18,:), ...
		       'Style', 'text', ...
		       'HorizontalAlignment', 'right', ...
		       'String', 'Obliquity factor(s): ', ...
		       'Enable', 'inactive', ...
		       'BackgroundColor', bgColor);
  ud.obliquitytexth = genH(18);
  
  [tmpUd tmpStr] = calcobliquity(ud.instrument, 'style', 'list');
  genPos(19,:) = [rhCol genPos(18,2) 1 1];
  genH(19) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'left', ...
		       'Value', 1, ...
		       'String', tmpStr, ...
		       'UserData', tmpUd, ...
		       'Position', genPos(19,:), ...
		       'Callback',  [mfilename '(''obliquity'');'], ...
		       'Tag', 'obliquity');
  genPos(19,:) = uiresize(genH(19), 'l', 'b');
  ud.obliquityh = genH(19);
  
  
  okPos = [10 10 1 1];
  [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'');'], ...
			   [mfilename '(''cancel'');']);
  
  
  set(fh, 'UserData', ud);
  % get instruments printed correctly
  feval(mfilename, 'updateinstruments', fh, ud.instrumentmenu);
  
  % ensure QDC controls are correctly set
  feval(mfilename, 'qdcupdate', fh);
  
  set(fh, 'Visible', 'on');
  % --------------------------------------------------------------
 case 'ok'
  % perform the checks now
  fh = gcbf;
  ud = get(fh, 'UserData');
  t = gettime(getstetbox(ud.steth));
  resolution = gettimespan(gettimespanbox(ud.resh));
  if resolution == timespan(0, 's')
    resolution = timespan('bad');
  end
  
  beams = getbeams(getbeambox(ud.beamh));
  optimes = info(ud.instrument, 'operatingtimes');
  
  if isempty(t{1}) | isempty(t{2})
    errordlg('Please set start and end times to valid dates', ...
	     'Error', 'modal');
  elseif t{3} <= timespan(0, 's')
    errordlg('Duration must be a non-zero value', 'Error', ...
	     'modal');
  elseif resolution < timespan(0, 's')
    errordlg('Resolution cannot be negative', 'Error', 'modal');
  elseif isvalid(resolution) & fix(t{3}./resolution) ~= (t{3}./resolution)
    fix(t{3}/resolution)
    t{3}/resolution
    errordlg(['The duration should be exactly divisible by the ' ...
	      'resolution'], 'Error', 'modal');
  elseif isempty(beams)
    errordlg('Please select at least one beam', 'Error', ...
	     'modal');
  elseif isvalid(optimes(1)) & t{1} < optimes(1)
    errordlg(['Instrument did not start operation until ' ...
	      char(optimes(1))]);
  elseif isvalid(optimes(2)) & t{2} > optimes(2)
    errordlg(['Instrument ceased operation on ' ...
	      char(optimes(2))]);
  else
    % passed all checks
    samples = (t{2}-t{1}) ./ resolution;
    if samples > 5 * 86400 
      if strcmp('No', ...
		questdlg({'You have selected a large amount of data, ', ...
			  'are you sure you want to continue?'}, ...
			 'Warning', 'Yes', 'No', 'No'))
	return;
      end
    elseif t{2}-t{1} > timespan(10, 'd') 
      if strcmp('No', ...
		questdlg({'You have selected a long duration, ', ...
			  'are you sure you want to continue?'}, ...
			 'Warning', 'Yes', 'No', 'No'))
	return;
      end
    end

    
    pointer = get(fh, 'Pointer');
    set(fh, 'Pointer', 'watch');
    buildTime = timestamp('now');      
    
    % error command, whatever build method used
    errcmd = ['[lasterrstr, lasterrid] = lasterr,' ...
	      'errordlg({''Loading data failed:'',' ...
	      'sprintf(''%s'', lasterrstr)}, '...
	      '''Error'', ''modal'');' ...
	      'set(fh, ''Pointer'', pointer);' ...
	      'return;'];

    if 1
      args = {'load', 1, ...
	      'instrument', ud.instrument, ...
	      'starttime', t{1}, ...
	      'endtime', t{2}, ...
	      'beams', beams};
      
      if strcmp(ud.datatype, 'rio_abs') 
	% special features for absorption
	% choose QDC
	val = get(ud.qdctypeh, 'Value');
	if val ~= 1 
	  if isempty(ud.qdc)
	    error('QDC not set');
	  elseif isa(ud.qdc, 'rio_qdc')
	    args{end+1} = 'qdc';
	    args{end+1} = ud.qdc;
	  elseif isa(ud.qdc, 'timestamp')
	    % load qdc
	    args{end+1} = 'qdc';
	    args{end+1} = rio_qdc('instrument', ud.instrument, ...
				  'load', 1, ...
				  'beams', beams, ...
				  'time', ud.qdc);
	  else
	    error('unknown data');
	  end
	end
	% obliquity
	args{end+1} = 'obliquity';
	args{end+1} = get(ud.obliquityh, 'UserData'); % cell array
	args{end} = args{end}{get(ud.obliquityh, 'Value')};
      end
      resmethodProp = get(ud.resmethodh); % get all params
      resmethod = resmethodProp.UserData{resmethodProp.Value};
      cancelH = findobj(fh, 'Type', 'uicontrol', ...
			'Style', 'pushbutton', ...
			'Tag', 'cancel');
      % rio = feval(ud.datatype, args{:}, 'cancelhandle', cancelH);
      rio = [];
      if ~isvalid(resolution)
	resolution = [];
      end
      cmd = ['rio = feval(ud.datatype, args{:}, ' ...
	     '''loadoptions'', {''cancelhandle'', cancelH, ' ...
	     '''resolution'', resolution,' ...
	     '''resolutionmethod'',resmethod});'];
      eval(cmd, errcmd);
            
    else
      error('set up this way to use QDC');
      rio = [];
      cmd = ['rio = feval(ud.datatype, ' ...
	     '''load'', 1, ' ...
	     '''instrument'', ud.instrument, ' ...
	     '''starttime'', t{1}, ' ...
	     '''endtime'', t{2}, ' ...
	     '''resolution'', resolution, ' ...
	     '''beams'', beams);'];
      set(fh, 'Pointer', pointer), % error('stop')
      lasterr('');
      eval(cmd, errcmd);
    end
    if isempty(rio)
      return;
    end
    if ~ishandle(fh)
      return; % cancel pressed
    end;

    % the amount of time it took to make
    buildTime = timestamp('now') - buildTime; 
    fstr = '%Y%m%d%H%M%S';
    
    % preset filename to something useful
    rio = setfilename(rio, strftime(t{1}, fstr));
    
    miashow('init', rio, 'buildtime', buildTime);
    set(fh, 'Pointer', pointer);
    % close(fh);
  end
  

  % --------------------------------------------------------------
 case 'cancel'
  close(gcbf);

  % --------------------------------------------------------------
 case 'datatype'
  % mfilename('datatype')
  % mfilename('datatype', fh, menuh)
  if nargin > 2
    fh = varargin{1};
    menuh = varargin{2};
  else
    fh = gcbf;
    menuh = gcbo;
  end
  
  ud = get(fh, 'UserData');
  ud.datatype = ud.defaults.datatypes{get(menuh, 'Value')};
  type = gettype(feval(ud.datatype), 'l');
  settitle(getbeambox(ud.beamh), [' for new ' type]);
  set(fh, 'UserData', ud, ...
	  'Name', sprintf('New riometer data [%s]', type));
  
  states = {'off', 'on'};
  set([ud.qdctypeh; ud.qdcselecth; ud.qdcdetailsh; ud.qdctexth; ...
      ud.obliquityh; ud.obliquitytexth], ...
      'Visible', states{strcmp(ud.datatype, 'rio_abs') + 1});
  
  feval(mfilename, 'updateinstruments', fh);
  
  % --------------------------------------------------------------
 case 'instrument'
  if nargin > 2
    fh = varargin{1};
    menuh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  else
    fh = gcbf;
    menuh = gcbo;
  end
  ud = get(fh, 'UserData');
  % clear currently selected beams when changing station
  n = get(menuh, 'Value');
  lastInstrument = ud.instrument;
  ud.instrument = ud.instruments(n);
  if ud.instrument == lastInstrument
    return; % no actual change
  end
  
  set(fh, 'UserData', ud);

  [imagingBeams wideBeams badBeams] = info(ud.instrument, 'beams');
  beams = ([imagingBeams(:)' wideBeams(:)']);
  if length(beams) > 1
    beams = []; % no default if many beams
  end
    
  setbeams(getbeambox(ud.beamh), ...
	   'beams', beams, ...
	   'imaging', imagingBeams, ...
	   'imagingallow', imagingBeams, ...
	   'widebeam', wideBeams, ...
	   'widebeamallow', wideBeams, ...
	   'badbeams', badBeams);
  setnearest(gettimespanbox(ud.resh), info(ud.instrument, 'bestresolution'));
  
  % --------------------------------------------------------------
 case 'updateinstruments'
  % mfilename('updateinstruments')
  % mfilename('updateinstruments', fh, menuh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  % menuh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  
  ud = get(fh, 'UserData');
  n = get(ud.instrumentmenu, 'Value');
  
  ud.instruments = instrumenttypeinfo(riometer, 'supported', ...
				      'datatype', ud.datatype);
  ud.instruments = sort(ud.instruments, ...
			{'locationname', 'serialnumber', 'latitude'});
  
  % check if selected instrument is in the new list
  n = find(ud.instrument == ud.instruments);
  if isempty(n)
    disp(sprintf(['\rPreviously selected instrument not available ' ...
		 'for this datatype\a']));
    % take preferred instrument if possible
    ud.instrument = instrumenttypeinfo(riometer, 'preferred');
    n = find(ud.instrument == ud.instruments);
    if isempty(n)
      % does not exist in list
      n = 1;
      ud.instrument = ud.instruments(1); 
    end
    set(ud.instrumentmenu, 'Value', n(1));
  else
    ud.instrument = ud.instruments(n(1)); % take first match
    set(ud.instrumentmenu, 'Value', n(1));
  end

  st = info(ud.instrument, 'starttime');
  set(fh, 'UserData', ud); % save settings
  feval(mfilename, 'updatetimes', fh, st, st + day);
  ud = get(fh, 'UserData');
  
  instrumentsStr = localInstruments2Str(ud.instruments);
  imh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  set(imh, 'String', instrumentsStr);
  set(fh, 'UserData', ud);

  % --------------------------------------------------------------
 case 'updatetimes'
  % mfilename('updatedate', fh, st, et)
  % mfilename('updatedate', st, et)
    if nargin >= 3
    fh = varargin{1};
	st = varargin{2};
	et = varargin{3};
  else
    fh = gcbf;
	st = varargin{1};
	et = varargin{2};
  end

   ud = get(fh, 'UserData');
   seb = getstetbox(ud.steth);
   settime(seb, {st, et, et - st});
   set(fh, 'UserData', ud); % save settings
   
  % --------------------------------------------------------------
 case 'updatesamples'
  % mfilename('updateinstruments')
  % mfilename('updateinstruments', fh, menuh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  
  res = gettimespan(gettimespanbox(ud.resh));
  t = gettime(getstetbox(ud.steth));

  s = 'Error';
  col = badColor;
  if isempty(t{3})
    ; % error
  elseif t{3} == timespan(0, 's') 
    ;
  elseif res == timespan(0, 's')
    s = 'Unknown';
    col = 'k';
  else
    samples = t{3} / res;
    if isinf(samples) | isnan(samples)
      ;
    elseif floor(samples) <= samples + eps & floor(samples) >= samples - eps
      s = sprintf('%d', samples);
      col = 'k';
    else
      s = sprintf('%g', samples);
    end
  end
  set(ud.sampleh, 'String', s, 'ForegroundColor', col);
  
  
  % --------------------------------------------------------------
 case 'updateresolution'
  % mfilename('updateinstruments')
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  res = gettimespan(gettimespanbox(ud.resh));
  bestres = info(ud.instrument, 'bestresolution');

  if isempty(res) | isequal(res, timespan)
    resdirection = 'off';
  
  elseif res == bestres
    resdirection = 'same';
  else
    if res > bestres
      % decrease resolution of the recorded data
      resdirection = 'dec';
      tmp = ud.decreaseresmethods;
    else
      resdirection = 'inc';
      tmp = ud.increaseresmethods;
    end
  end
  
  if strcmp(ud.resdirection, resdirection)
    % same as before, do nothing
    return
  end
  switch resdirection
   case 'off'
    set(ud.resmethodh, ...
	'Visible', 'off', ...
	'Value', 1, ...
	'UserData', {''});

   case 'same'
    set(ud.resmethodh, ...
	'Style', 'text', ...
	'Visible', 'on', ...
	'Value', 1, ...
	'String', '(native resolution)', ...
	'Enable', 'inactive', ...
	'UserData', {''});
    
   case {'inc' 'dec'}
    mud = fieldnames(tmp); % menu userdata
    str = cell(size(mud));
    for n = 1:length(mud)
      str{n} = getfield(tmp, mud{n});
    end
    set(ud.resmethodh, ...
	'Style', 'popupmenu', ...
	'Visible', 'on', ...
	'String', str, ...
	'Enable', 'on', ...
	'Value', 1, ...
	'UserData', mud);
   otherwise
    error(sprintf('unknown resolution method state (was ''%s'')', ...
		  resdirection));
  end
  uiresize(ud.resmethodh, 'l', 'b');
  ud.resdirection = resdirection; % save direction
  set(fh, 'UserData', ud);
  % now update the number of samples
  feval(mfilename, 'updatesamples', fh);
  
  % --------------------------------------------------------------
 case 'qdc'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  
  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  if val == 1
    % auto
    set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
  else
    % some kind of manual
    set([ud.qdcselecth], 'Enable', 'on');
    if isempty(ud.qdc)
      set(ud.qdcdetailsh, 'Enable', 'off');
    else
      set(ud.qdcdetailsh, 'Enable', 'on');
    end
  end
  % --------------------------------------------------------------
 case 'qdcselect'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end

  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  switch val
   case 1
    
   case 2
    % manual, by date 
    if isempty(ud.qdc)
      t = gettime(getstetbox(ud.steth));
      t = t{1};
      if ~isa(t, 'timestamp')
	t = timestamp('today'); % timebox might be bad
      end
    elseif isa(ud.qdc, 'rio_qdc')
      t = getstarttime(ud.qdc);
    elseif isa(ud.qdc, 'timestamp')
      t = ud.qdc;
    else
      error('unknown data');
    end
    
    [t button] = uigettimestamp('init', ...
				'timestamp', t, ...
				'name', 'Select QDC');
    if strcmp(button, 'cancel')
      feval(mfilename, 'qdcupdate', fh);
      return; % no changes made
    end
    ud.qdc = t;

   case 3
    % manual, from file
    errordlg('Not implemented', 'Error', 'modal');
    return;
    
    [filename Path] = uigetfile('*.mat', 'Select QDC');
    if isequal(filename, 0)
      % cancel pressed, so no changes
      return;
    end
    pathfile = fullfile(Path, filename);
    
   otherwise
    error('Unknown QDC method');
  end
  set(fh, 'UserData', ud); % save settings
  feval(mfilename, 'qdcupdate', fh); % get buttons set correctly
  % --------------------------------------------------------------
 
 case 'qdcdetails'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  if isempty(ud.qdc)
    disp('no QDC to display'); % this should not happen
  elseif isa(ud.qdc, 'rio_qdc')
    disp(sprintf('\r%s', char(ud.qdc)));
  elseif isa(ud.qdc, 'timestamp')
    disp(sprintf('\rUse QDC for %s', char(ud.qdc)));
  else
    error('unknown data');
  end
  
  % --------------------------------------------------------------
 case 'qdcupdate'
  % mfilename('qdc')
  % mfilename('qdc', fh, menuh)

  % update QDC controls, based upon their current settings, and the qdc
  % object in the userdata area
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  if val == 1
    % auto
    set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
  else
    % some kind of manual setting
    % set(ud.qdcselecth, 'Enable', 'on');
    if isempty(ud.qdc)
      % set(ud.qdcdetailsh, 'Enable', 'off');
      set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
      set(ud.qdctypeh, 'Value', 1); % flick back to auto
    else
      set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'on');
    end
  end
  
  % --------------------------------------------------------------
 case 'obliquity'
  % mfilename('obliquity')
  cbo = gcbo;
  obl = get(cbo, 'UserData'); % cell array
  obl = obl{get(cbo, 'Value')};
  switch obl
   case 'none'
    % disp(sprintf(['\r\n\aObliquity: none\nnot including obliquity ' ...
    % 'factors is an advanced option. Use with extreme ' ...
    % 'caution']));
    warndlg(['Not incuding obliquity factors is an advanced option. ' ...
	     'Use with extreme caution'], 'Warning', 'modal');
    
   case 'simple'
    ; % do nothing
    disp(sprintf(['\r\nObliquity: simple\nstandard obliquity factors, ' ...
		  ' based upon zenith angle of beam centre. ' ...
		  'Includes correction for ionospheric curvature']));
   
   case 'effective'
    disp(sprintf(['\r\nObliquity: effective\nmost accurate but slowest. ' ...
		  'Of most use for events with high absorption values. ' ...
		  'Requires antenna directivity to be known (not true ' ...
		  'for all supported riometers. Includes correction ' ...
		  'for ionospheric curvature']));
    
   otherwise
    db = dbstack;
    mf = db(1).name
    error(sprintf('unknown obliquity style, (was %s). Please fix %s', ...
		  obl, mf));
  end
    
  % --------------------------------------------------------------
  
 otherwise
  error(sprintf('unknown action (was %s)', char(action)));
end


% --------------------------------------------------------------
function r = localInstruments2Str(in)
r = cell(size(in));
for n = 1:length(in)
  nm = getname(in(n));
  abbrev = getabbreviation(in(n));
  if isempty(nm)
    r{n} = sprintf('%s (%s) #%d', getname(getlocation(in(n))), abbrev, ...
		   getserialnumber(in(n)));
  else
    r{n} = sprintf('%s (%s) [%s] #%d', getname(getlocation(in(n))), ...
		   abbrev, nm, getserialnumber(in(n)));
  end
  r{n} = char(r{n}); % convert any internatiolisation chars to real chars
end
