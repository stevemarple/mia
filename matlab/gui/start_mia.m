function varargout = start_mia(varargin)
%START_MIA Initial window for MIA.


if nargin > 0
  action = varargin{1};
else
  action = 'focus';
end

switch action
  
  case 'figuredelete'
    % a figure is about to be deleted
    fh = localGetFigHandle;
    if strcmp(get(fh, 'Visible'), 'on')
      return; % main window exists and is visible
    end
    
    % find out if there is a visible toolkit figure
    visWin = 0;
    vmtopH = findall(0, 'Type', 'uimenu', 'Tag', 'viewmenutop');
    tkfH = get(vmtopH,'Parent');
    cbo = gcbo;
    if ~isempty(tkfH)
      if iscell(tkfH)
	tkfH = [tkfH{:}];
      end
      for n = 1:length(tkfH)
	if tkfH(n) ~= cbo & ~isequal(tkfH(n), fh) & ...
	      strcmp(get(tkfH(n), 'Visible'), 'on');
	  visWin = 1;
	  break;
	end
      end
    end
    if ~visWin
      % no more (visible) toolkit windows - create a new one
      feval(mfilename, 'focus');
    end
    
    
  case 'focus'
    fh = localNewWindow;
    figure(fh); % this also makes it visible, if it wasn't
  
  otherwise 
    error('unknown action');
end


if nargout > 0;
  varargout{1} = fh;
end


% --------------------------------------------------------------
function fh = localGetFigHandle
% check to see if a window exists already
fh = findall(0, 'Type', 'figure', 'Tag', 'mia');
if ~isempty(fh)
  fh = fh(1);
end

% --------------------------------------------------------------
function fh = localNewWindow
% need to create a new figure (or return its handle if already created)

fh = localGetFigHandle;
if ~isempty(fh)
  return;
end

screenSize = get(0, 'ScreenSize');
figPos = [1 1 200 1];
figPos(1:2) = [fix((screenSize(3)-figPos(3)) / 2) ...
      screenSize(4)-figPos(4)-100];

if matlabversioncmp('<', '5.3')
  menuBar = 'figure';
else
  menuBar = 'none';
end
menuBar

fh = figure('Visible', 'off', ...
	    'DeleteFcn', '', ...
	    'IntegerHandle', 'off', ...
	    'HandleVisibility', 'callback', ...
	    'MenuBar', menuBar, ...
	    'Name', sprintf('MIA v%.1f', miaversion), ...
	    'NumberTitle', 'off', ...
	    'Position', figPos, ...
	    'Resize', 'off', ...
	    'Tag', 'mia');

% delete existing menus
delete(findall(fh, 'Type', 'uimenu'));

if matlabversioncmp('<', '5.3')
  % add toolkit ones
  miamenu(fh);
end

uitbH = findall(fh, 'Type', 'uitoolbar');
% uiptH = findall(fh, 'Type', 'uipushtool');
% newData = findobj(uiptH, 'flat', 'TooltipString', 'New Figure');

% delete all children of the toolbar and start again
delete(uitbH);

miatoolbar('init', ...
	   'parent', fh, ...
	   'miasave', 'off');

set(fh, 'Visible', 'on', 'Menu', menuBar);




