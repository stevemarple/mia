function r = getbeambox(h)
% GETBEAMBOX  Return a BEAMBOX object from a handle of BEAMBOX uicontrol
% Note that this only returns a copy of the current object, and the
% 'live' version is normally kept on a UserData area of a uicontrol
% object.

% It is done this way because a) matlab doesn't have references and b)
% the handles are doubles, not a class which can be extended.

% search all handles given, but don't return more than one object
if length(h) ~= 1
  cmd = sprintf('b=%s(h(n));', mfilename);
  for n = 1:length(h)
    eval(cmd, 'b=[];');
    if ~isempty(b)
      if isempty(r)
	r = b;
      else
	error('multiple beam boxes found');
      end
    end
  end
end

if isa(h, 'beambox')
  % already is a r!
  r = h;
  return;
elseif ishandle(h)
  p = get(h);
  
  switch p.Type
   case 'uicontrol'
    if strcmp(p.Tag, 'mapbutton')
      r = get(h, 'UserData');
      
    else
      % probably the series box, or the OK button from the map
      % window. Look in UserData for a valid handle
      h2 = get(h, 'UserData');
      if ishandle(h2)
	r = get(h2, 'UserData');
	if isa(r, 'beambox')
	  return;
	end
      end
      error('unknown uicontrol');
    end
    
   case 'figure'
    h2 = findall(h, 'Type', 'uicontrol', 'Tag', 'mapbutton');
    switch length(h2)
     case 0
      error('cannot find a beam box in this figure');
     case 1
      r = get(h2, 'UserData');
     otherwise
      error('multiple beam box objects found');
    end

   otherwise
    error('need uicontrol or figure handle');
  end
    
%   r = get(h, 'UserData');
%   if strcmp(class(r), 'beambox');
%     % h was handle of map button
%     return;
%   else
%     % h must be handle of series edit box
%     % therefore now have handle of map button
%     r = get(r, 'UserData');
%     return;
%   end
  
else
  error('not a UI handle');
end
