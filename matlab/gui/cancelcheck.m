function cancelcheck(h)
%CANCELCHECK  Check if the cancel button has been pressed.
%
%   CANCELCHECK(h)
%   h: handle of the cancel button or other object which is DELETEd when
%   cancel is pressed
%
%   CANCELCHECK checks if the handle is valid. If so it assumes cancel has
%   not been pressed. If the handle is not valid ERROR is called. This
%   function also calls PAUSE(0) to allow GUI callbacks to be executed. If h
%   is an emptry matrix CANCELCHECK returns without error.
%
%   See also ISHANDLE, PAUSE.

% allow interrupts from GUI callbacks to be executed
pause(0); 

if isempty(h)
  % disp('empty handle');
  % error('empty handle'); % for debugging
  return;
end
if ~ishandle(h)
  error('Cancel pressed')
end

