function varargout = imagelabel(varargin)
%IMAGELABEL  Label a MIA_IMAGE_BASE IMAGEPLOT with suitable labels
%
%   IMAGELABEL(...)
%
% The behaviour of this function is controlled with the following
% parameter name/value pairs:
%
%   'action', ['label' | 'init' | 'gethandles']
%    Indicate what action the function should perform. 'init' is for
%    initialising FIGUREs to add appropriate UIMENU options. 'label' will
%    add the labels determined by the 'style' parameter. Other actions
%    are used internally in response to UIMENU selections.
%
%   'style', CHAR
%   The labelling style to be used. Choose 'none' to have no label, or
%   'number' to show the sequential image number. Additional types are
%   available depending upon the type of data plotted, and may be listed
%   by typing "getimagelabel(mia, 'list')" in the console window (where
%   "mia" is the data object). See GETIMAGELABEL for more information.
%
%   'figure', HANDLE
%   Handle to the FIGURE whos image plots should be labelled.
%
%   'position', [1x4 DOUBLE]
%   Position vector of the label in the axes
%
%   'timeformat', CHAR
%   The STRFTIME format string used for printing TIMESPAN objects. Default is
%   '%H:%M:%S'.
%
%   'numberformat', CHAR
%   The SPRINTF format specifier used for printing numbers. Default is '%d'.
%
%   'color', COLORSPEC
%   The color used for the labels.
%
%   'fontsize', DOUBLE
%   Size of the label font.
%
%   'verticalalignment', CHAR
%   'horizontalalignment', CHAR
%   The alignment of the labels. See TEXT for more details.
%
%   'visible', ['on' | 'off']
%   Whether labels are displayed initially.
%
% See also MIA_IMAGE_BASE, IMAGEPLOT, TEXT.

% this function expects to find userdata for the plot axes to be a struct
% containing the following fields: mia, imagenumber. Blank images do not
% contain these fields, but this function does test for the existence of
% a field before using it.

% this is a useful function which could sensibly be called from the
% command line. Therefore make the default action to be to label, and
% have action selected by a parameter name/value pair.

% if the figure in question already has imagelabel stuff then get the
% defaults from its userdata area. This means we need to inspect the
% parameter list to see if figure is specified
tmp.figure = [];
tmp = interceptprop(varargin, tmp);
if isempty(tmp.figure)
  fh = gcbf;
  if isempty(fh)
    fh = get(0, 'CurrentFigure'); % gca could create figure here!
  end
  if isempty(fh)
    % still empty?!
    error('figure handle not specified and cannot guess it');
  end
else
  fh = tmp.figure;
end
imageLabelMenuH = findobj(fh, ...
			  'Tag', 'imagelabels', ...
			  'Type', 'uimenu');
% read userdata before adding to it, it might contain information
% created by the uiobject's CreateFcn
if isempty(imageLabelMenuH)
  error('Cannot find image label menu object');
end
ud = get(imageLabelMenuH, 'UserData');

defaults.action = 'label'; % 'init' initialise the figure menus

% call getimagelabel(mia, 'list') for a valid list (depends on class)
defaults.style = 'none'; 
defaults.position = [0.05 0.05 1]; % relative position in axes
defaults.timeformat = '%H:%M:%S';
defaults.numberformat = '%d';
defaults.color = 'w';
defaults.backgroundcolor = 'none';
defaults.fontsize = 10;
defaults.verticalalignment = 'bottom';
defaults.horizontalalignment = 'left';
defaults.visible = 'on'; % inital condition, change via menu
defaults.figure = fh; 

% stuff used for initialisation
defaults.image = [];

% now see if figure has defaults stored in it. If so use whatever is
% found in it
if isfield(ud, 'defaults')
  if isstruct(ud.defaults)
    fn = fieldnames(ud.defaults);
  else
    fn = {};
  end
  for n = 1:length(fn)
    defaults = setfield(defaults, fn{n}, getfield(ud.defaults, fn{n}));
  end
end

[defaults unvi] = interceptprop(varargin, defaults);

ah = findall(fh, 'Type', 'axes', 'Tag', 'plot');

switch defaults.action
 case 'init'
  % add stuff to menus
  if ~isa(defaults.image, 'mia_image_base')
    error('image must be a type derived from mia_image_base');
  end

  defaults.action = 'label'; % subsequent calls should be for label
  % defaults.image is too large to store in UserData (not needed anyhow)
  ud.defaults = localRemoveUnneededFields(defaults);
  
  set(imageLabelMenuH, ...
      'Visible', 'on', ...
      'UserData', ud);
  
  [style name] = getimagelabel(defaults.image, 'list');
  % this function supports two others, prepend to lists
  style = {'none' 'number' style{:}};
  name = {'None' 'Image number' name{:}};
  for n = 1:length(style)
    uimenu('Parent', imageLabelMenuH, ...
	   'Label', name{n}, ...
	   'Callback', sprintf('%s(''style'',''%s'');', ...
			       mfilename, style{n}), ...
	   'Tag', sprintf('imagelabel_%s', style{n}));
  end
  uimenu('Parent', imageLabelMenuH, ...
	 'Label', 'Label color...', ...
	 'Callback', [mfilename '(''action'',''setcolor'');'], ...
	 'Separator', 'on', ...
	 'Tag', 'imagelabel_color');
  uimenu('Parent', imageLabelMenuH, ...
	 'Label', 'Label font...', ...
	 'Callback', [mfilename '(''action'',''setfont'');'], ...
	 'Tag', 'imagelabel_color');

  set(imageLabelMenuH, 'UserData', ud);
  
  % now label with the initial label style
  feval(mfilename, 'figure', fh);
  
 case 'label'
  % actually label the plot(s)

  % unset checked on menu items
  mh = get(imageLabelMenuH, 'Children');
  set(mh, 'Checked', 'off');
  % the one to turn on is probably gcbo, but cannot be sure. Also it is
  % possible to call imagelabel from the command line, so this approach
  % still updates the menu items
  styleH = findall(mh, ...
		   'Type', 'uimenu', ...
		   'Tag', sprintf('imagelabel_%s', defaults.style));
  set(styleH, 'Checked', 'on');
  
  % delete old image labels
  if ~isempty(ah)
    % delete(findall(ah, 'Type', 'text', 'Tag', 'imagelabel'));
  end
  n = 0;
  r = zeros(size(ah));
  % iterate through input list
  for h = ah(:)'
    aud = get(h, 'UserData'); % axis user data
    labelString = '';
    switch defaults.style
     case 'none'
      labelString = '';
     case 'number'
      if isfield(aud, 'imagenumber')
	labelString = sprintf(defaults.numberformat, aud.imagenumber);
      else
	disp('imagenumber field missing');
      end
     otherwise
      if isfield(aud, 'mia')
	labelString = getimagelabel(aud.mia, defaults, varargin{unvi});
      end
    end
    lh = findall(h, 'Type', 'text', 'Tag', 'imagelabel');
    if length(lh) > 1
      delete(lh); % more than one label? delete and recreate!
      lh = [];
    end
    % if ~isempty(labelString)
    if isempty(lh)
      n = n + 1;
      r(n) = text('Parent', h, ...
		  'String', labelString, ...
		  'Units', 'normalized', ...
		  'Position', defaults.position, ...
		  'Color', defaults.color, ...
		  'BackgroundColor', defaults.backgroundcolor, ...
		  'FontSize', defaults.fontsize, ...
		  'HorizontalAlignment', defaults.horizontalalignment, ...
		  'VerticalAlignment', defaults.verticalalignment, ...
		  'Visible', defaults.visible, ...
		  'Tag', 'imagelabel');
      % for some reason the string is not shown above surfaces and patches
      % unless units is data, but want normalized units when positioning the
      % object, so set units to data afterwards. It seems this is fixed in
      % matlab v5.3, keep for backwards compatibility with v5.1
      set(r(n), 'Units', 'data');
    else
      n = n + 1;
      r(n) = lh;
      set(lh, 'String', labelString);
    end
  end

  %   % update the UserData area to reflect the latest defaults, it could be
  %   % that the function has been called from the command line and the
  %   % timeformat has been changed.
  %   ud.defaults = localRemoveUnneededFields(defaults);
  %   set(imageLabelMenuH, 'UserData', ud);
  varargout{1} = r(1:n);
  
  % ------------------------------------------------------------------------
 case 'gethandles'
  varargout{1} = findall(ah, 'Type', 'text', 'Tag', 'imagelabel'); 
  
  % ------------------------------------------------------------------------
 case 'setcolor'
  lh = findall(ah, 'Type', 'text', 'Tag', 'imagelabel');  
  if length(lh)
    c = get(lh(1), 'Color');
  else
    c = text2rgb(defaults.color);
  end
  col = uisetcolor(c, 'Select image label color');
  set(lh, 'Color', col);
  defaults.color = col;
  
  % ------------------------------------------------------------------------
 case 'setfont'
  lh = findall(ah, 'Type', 'text', 'Tag', 'imagelabel');  
  if isempty(lh)
    disp('Cannot find any image labels whose font should be set');
    varargout{1} = [];
    return
  end
  uisetfont(lh(1), 'Select image label font');
  fontPN = {'FontAngle' 'FontName' 'fontSize' 'FontWeight'};
  fontPV = get(lh(1), fontPN);
  set(lh, fontPN, fontPV);
  
 otherwise 
  error(sprintf('unknown action (was %s)', defaults.action));
end


% update the UserData area to reflect the latest defaults, it could be
% that the function has been called from the command line and the
% timeformat has been changed.
defaults.action = 'label';
ud.defaults = localRemoveUnneededFields(defaults);
set(imageLabelMenuH, 'UserData', ud);



function r = localRemoveUnneededFields(defaults)
r = defaults;
% these fields are not needed (or wanted) after initialisation.
fn = {'image' 'figure'};
for n = 1:length(fn)
  if isfield(r, fn{n})
    r = rmfield(r, fn{n});
  end
end
