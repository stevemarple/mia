function varargout = uigettimestamp(action, varargin)
%UIGETTIMESTAMP  Modal dialog box to return a TIMESTAMP.
%
%   [t b] = UIGETTIMESTAMP('init', ...)
%   t: TIMESTAMP object
%   b: button pressed ['ok | 'cancel']
%
%   UIGETTIMESTAMP is a dialog box to obtain a TIMESTAMP. The standard
%   behaviour can be modified with the following name/value pairs:
%
%     'timestamp', TIMESTAMP
%     The intial time in the form
%
%     'name', CHAR
%      The dialog window name. Default is 'Select time'.
%
%     'frametitle', CHAR
%     The title string printed in the frame. The default is to use the
%     same string as the window title.
%  
%     'label', CHAR
%     The label used in the form. Default is 'Time: '.
%
%   Closing the window is equivalent to clicking cancel.
%
%   See also TIMEBOX, TIMESTAMP.

if nargin == 0
  action = 'init';
end



switch action
  % --------------------------------------------------------------
 case 'init';

  defaults.timestamp = timestamp('today');
  defaults.name = 'Select time';
  defaults.frametitle = [];
  defaults.label = 'Time: ';
  [defaults unvi] = interceptprop(varargin, defaults);

  if ~isa(defaults.timestamp, 'timestamp') ...
	| prod(size(defaults.timestamp)) ~= 1
    error('timestamp not valid');
  end

  if isnumeric(defaults.frametitle)
    defaults.frametitle = defaults.name;
  end
  
  % create the figure then add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text

  fh = figure('Name', defaults.name, ...
	      'Visible', 'off', ...
	      'IntegerHandle', 'off', ...
	      'MenuBar', 'none', ...
	      'NumberTitle', 'off', ...
	      'WindowStyle', 'modal', ...
	      'ButtonDownFcn', '' , ...
	      'CloseRequestFcn', [mfilename '(''cancel'');' ]);
  ud = get(fh, 'UserData');
  ud.defaults = defaults;
  
  ud.return = {}; % return value
  bgColor = get(fh, 'Color');

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', [defaults.label ' ']);
  textPos = uiresize(textH, 'l', 'b');
  % frameColor = get(textH, 'BackgroundColor');
  frameColor = get(fh, 'Color');

  delete(textH);
  
  % allocate space for the handles and positions
  genH     = zeros(4,1);   % handles to uicontrols in general frame
  genPos   = zeros(4,4);   % positions  "                    "

    % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the buton widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  frameWidth = 3.9 * textPos(3);
  
  lines = 1;
  
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+(lines+1)*textPos(4)+lines*lineSpacing]; 
  
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', bgColor, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'General' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', defaults.frametitle, ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');

  % 'Data type' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', defaults.label, ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');
  
  % x coord of RH column in figure
  textPos(3);
  rhColDiff = spacing(1) + 30;
  rhCol = genPos(1,1) + textPos(3) + rhColDiff;
  
  genPos(4,:) = [rhCol genPos(3,2) 1 1];
  genH(4) = timebox(fh, genPos(4,:));
  ud.timeh = genH(4);
  tb = gettimebox(ud.timeh);
  settime(tb, defaults.timestamp);

  okPos = [10 10 1 1];
  [ud.okh ud.cancelh] = okcancel('init', fh, okPos, ...
				 [mfilename '(''ok'');'], ...
				 [mfilename '(''cancel'');']);

  % reposition uicontrols (except ok/cancel) neatly within frame
  h = [genH(2:3); gethandles(gettimebox(ud.timeh))];
  bb = boundingbox(h);
  err = zeros(1, 4);
  err(1:2) = bb(1:2) - genPos(1,1:2) - spacing; 
  for h2 = reshape(h, 1, prod(size(h)));
    set(h2, 'Position', get(h2, 'Position') + err);
  end
  
  % set frame TR corner
  genPos(1,:) = get(genH(1), 'Position');
  genPos(1, 3:4) = bb(3:4) + 2*frameSpacing;
  set(genH(1), 'Position', genPos(1,:));
  
  % set frame title position to have same x values as frame, so it will be
  % centered 
  genPos(2,:) = get(genH(2), 'Position');
  genPos(2, [1 3]) = genPos(1, [1 3]);
  set(genH(2), 'Position', genPos(2,:));
  genPos(2,:) = uiresize(genH(2), 'c', 'b');
  
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3:4)-figWH)./2  figWH]);

  okcancel('resize', fh, ud.okh, ud.cancelh);
  set(fh, 'UserData', ud, ...
	  'Visible', 'on', ...
	  'Resize', 'off');
  waitfor(ud.okh); % wait for the ok button to be deleted
  % resumed
  ud = get(fh, 'UserData'); % reload modified userdata
  varargout{1} = ud.return{1};
  varargout{2} = ud.return{2};
  delete(fh);
  return;

  % --------------------------------------------------------------
 case 'ok'
  fh = gcbf;
  ud = get(fh, 'UserData');
  t = gettime(gettimebox(ud.timeh));
  if ~isa(t, 'timestamp')
    errordlg('Time not valid', 'Error', 'modal');
    return;
  end
  ud.return{1} = t;
  ud.return{2} = 'ok';

  set(fh, 'UserData', ud);
  delete(ud.okh); % activate the waitfor
  
  % --------------------------------------------------------------
 case 'cancel';
  % cancel button pressed (also used for window close request)
  fh = gcbf;
  ud = get(fh, 'UserData');
  ud.return{1} = ud.defaults.timestamp; % use initial value
  ud.return{2} = 'cancel';
  set(fh, 'UserData', ud);
  delete(ud.okh); % activate the waitfor

  % --------------------------------------------------------------
 otherwise
  error(sprintf('unknown action (was %s)', char(action)));
end
