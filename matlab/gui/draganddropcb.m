function draganddropcb(action, varargin)

if nargin == 0
  action = 'init';
  varargin{1} = gcf;
end

switch action
  % ---------------------------------------------
 case 'init'
  if nargin == 1
    fh = gcf;
  else
    fh = varargin{1};
  end
  fcn = get(fh, 'WindowButtonUpFcn');
  s = sprintf('%s(''drop'');', mfilename);
  if isempty(fcn)
    fcn = s;
  elseif fcn(end) == ';'
    fcn = [fcn s];
  else
    fcn = [fcn ';' s];
  end
  set(fh, 'WindowButtonUpFcn', fcn);

  % ---------------------------------------------
 case 'drop'
  % get the target figure handle. This can change when the mouse moves so
  % ask ASAP
  pointerPosition = localGetPositionInPixels(0, 'PointerLocation');
  tfh = get(0, 'PointerWindow'); 
  fh = gcbf;
  if tfh == 0
    return; % can't drag and drop to non-matlab window
  elseif ~strcmp(get(fh, 'SelectionType'), 'normal')
    return; % not normal drag
  end
  sah = get(fh, 'CurrentObject');
  if isempty(sah)
    return  % no source axes
  end

  sahType = get(sah, 'Type');
  switch sahType
   case {'root' 'figure' 'uicontrol' 'uimenu' 'uicontextmenu'}
    return; % not dragging from an axes
   case 'axes'
    ;
   otherwise
    % something inside an axes
    sah = get(sah, 'Parent');
  end
  % check that we should copy this axes
  if strcmp(get(sah, 'HandleVisibility'), 'off')
    % if we make an exact copy of this axes then we have no way to add
    % new data to the plot later. HandleVisiblity=off means no one should
    % be playing with the original, so we guess no-one should make a copy
    % either 
    return;
  end
  
  % find out which axis we should drop into
  % HandleVisibility=on needed otherwise axes can't become current axis
  tah = findobj(tfh, 'Type', 'axes', ...
		'HandleVisibility', 'on');
  if isempty(tah)
    return; % no suitable axes
  end
  tfhPosition = localGetPositionInPixels(tfh);
  tahPosition = localGetPositionInPixels(tah);
  if iscell(tahPosition)
    tahPosition = cell2mat(tahPosition);
  end
  
  % bouding boxes of axes, [x1 y1 x2 y2]
  tahBoundingBox = [tahPosition(:, 1:2), ...
		    tahPosition(:, 1:2) + tahPosition(:, 3:4)];
  
  % find in which axes the mouse was released
  pointerPositionInWindow = pointerPosition - tfhPosition(1:2);
  % axes is the one where tmp(n, :) == [-1 -1 +1 +1], meaning mouse
  % position is between x and y limits
  tmp = sign(tahBoundingBox - repmat(pointerPositionInWindow, ...
				     length(tah), 2));
  comp = tmp(:,1) == -1 & tmp(:,2) == -1 & ...
	 tmp(:,3) == +1 & tmp(:,4) == +1;
  match = find(comp);
  if isempty(match)
    return; % none found
  end
  
  % for each axes check if it is supposed to receive drag and drop. If
  % not remove from list. See if any are left
  tahUd = get(tah, 'UserData');
  
  % generalise for mutliple axes
  if length(tah) == 1
    tahUd = {tahUd};
  end
  
  for n = 1:length(match)
    if isstruct(tahUd{match(n)})
      ud = tahUd{match(n)};
      if isfield(ud, 'receivedraganddrop')
	if ud.receivedraganddrop == 0
	  % remember to delete this axis from the list of possibles
	  match(n) = 0; 
	end
      end
    end
  end
  % ignore those where receivedraganddrop is off
  match(match == 0) = []; 
  
  switch length(match)
   case 0
    return; % no possibles left
    
   case 1
    tahUd = tahUd{match};
    tah = tah(match);
    
   otherwise
    disp(['multiple axes found for this drag and drop location, not' ...
	  ' proceeding']);
    return;
  end

  if tah == sah
    return; % don't drag and drop to same axes
  end
  % check if source axes is supposed to provide data
  sahUd = get(sah, 'UserData');
  if isfield(sahUd, 'providedraganddrop')
    if sahUd.providedraganddrop == 0
      return;
    end
  end

  
  % check if we are supposed to drop data into this axes
  receivedraganddrop = nan; % not known
  % ud = get(tah, 'UserData');
  if isstruct(tahUd)
    if isfield(tahUd, 'receivedraganddrop')
      receivedraganddrop = tahUd.receivedraganddrop;
    end
  end
  switch receivedraganddrop
   case 0
    return; % should already have been caught
   case 1
    ;
   otherwise
    button = questdlg(['Are you sure you want to drop ' ...
		       'a new figure into this window?'], 'Warning', ...
		      'Yes', 'No', 'Help', 'No');
    switch(button)
     case 'Yes'
      ;
     case 'No'
      return;
     case 'Help'
      disp(['If you select ''Yes'' then the current contents of this axis' ...
	    ' will be replaced']);
      return;
     otherwise
      error(sprintf('unknown button (was ''%s'')', button));
    end
  end
  
  % sah = get(fh, 'CurrentAxes'); % source axis handle
  if 1
    prop = get(sah);
    prop = rmfield(prop, ...
		   {'BeingDeleted', 'CurrentPoint', 'Children', ...
		    'Parent', 'Units', 'Position', ...
		    'Title', 'Type', 'XLabel', 'YLabel', 'ZLabel'});
    delete(allchild(tah));
    copyobj(get(sah, 'Children'), tah);
    if isstruct(prop.UserData)
      if isfield(prop.UserData, 'receivedraganddrop')
	% allow drag and drop to be performed again on the copy, but
        % under same terms as this time (ie directly or with dialogbox)
	prop.UserData.receivedraganddrop = receivedraganddrop;
      end
    end
    set(tah, prop);
  else
    % seems to preserve everything, but handle changes which breaks some
    % things
    prop = get(tah);
    if isstruct(prop.UserData)
      if isfield(prop.UserData, 'receivedraganddrop')
	% allow drag and drop to be performed again on the copy, but
        % under same terms as this time (ie directly or with dialogbox)
	prop.UserData.receivedraganddrop = receivedraganddrop;
      end
    end
    delete(tah);
    tah = copyobj(sah, tfh);
    set(tah, 'Units', prop.Units);
    set(tah, 'Position', prop.Position, 'UserData', prop.UserData);
  end
  
  % ---------------------------------------------
 otherwise 
  error(sprintf('unknown action (was ''%s'')', action));
end


function r = localGetPositionInPixels(h, varargin)
r = [];
if nargin > 1
  paramName = varargin{1};
else
  paramName = 'Position';
end
units = get(h, 'Units');
if ischar(units)
  units = {units};
end

getCmd = sprintf('set(h, ''Units'', ''pixels'');r = get(h, ''%s'');', ...
		 paramName);
resetCmd = 'set(h, {''Units''}, units);';
% tr/catch handling not in matlab v5.1, so use eval instead
eval([getCmd resetCmd], resetCmd);

