function db = getdatebox(handle)
%GETDATEBOX  Return a DATEBOX object from a handle of DATEBOX UICONTROL.
%
%   db = GETDATEBOX(h)
%   db: DATEBOX object
%   h: UICONTOL handle associated with a DATEBOX object
% 
%   Note that this only returns a copy of the current object, and the 'live'
%   version is normally kept on a UserData area of a UICONTROL object.
%
%   It is done this way because a) matlab doesn't have references and b) the
%   handles are doubles, not a class which can be extended.
%
%   See also DATEBOX.
if ishandle(handle)
  handle = double(handle);
end

switch class(handle)
  case 'datebox'
    % already is a db!
    warning('this already is a datebox');
    db = handle;
    return;

  case 'double'
    if ishandle(handle)
      db = get(handle, 'UserData');
      % if strcmp(class(db), 'datebox')
      if isa(db, 'datebox')
	return;
      else
	db = getdatebox(db);
	return;
      end
    else
      error('Not a valid handle');
    end
    
  otherwise
    % some other class which is storing our datebox
    db = getuserdata(handle);
    if isa(db, 'datebox')
      return;
    else
      db = getdatebox(db);
    end
    return;
end



