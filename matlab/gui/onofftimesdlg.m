function varargout = onofftimesdlg(action, varargin)

switch action  
  % --------------------------------------------------------------
  % mfilename('init')
 case 'init'

  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text

  fh = figure('Name', 'Heater on/off times', ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'HandleVisibility', 'callback', ...
	      'WindowStyle', 'modal', ...
	      'MenuBar', 'none');
  
  bgColour = get(fh, 'Color');
  
  defaults.color = 'r';
  [defaults unvi] = interceptprop(varargin, defaults);

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'Colour:  ');
  textPos = uiresize(textH, 'l', 'b');
  frameColour = get(fh, 'Color');
  delete(textH);
  
  % in case anything else uses UserData, or window is later created in
  % a different way load UserData
  ud = get(fh, 'UserData');
  ud.fh = fh;
  
  
  % allocate space for the handles and positions. Call them gen in
  % case we choose to add other frames
  genH     = zeros(19,1);   % handles to uicontrols in general frame
  genPos   = zeros(19,4);   % positions  "                    "
  
  
  % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the buton widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  frameWidth = 3.9 * textPos(3);

  linesOfText = 4;
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+linesOfText*textPos(4)+...
       (linesOfText-1)*lineSpacing]; 
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);

  % -----------------------------
  % General frame
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', frameColour, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'Configure on/off markers'
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Configure on/off markers', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % -----------------------------
  % 'Style' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Style: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;

  % style menu
  genPos(4,:) = [rhCol genPos(3,2) 1 1]; % set W & H in uiresize()
  genH(4) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'String', {'line', 'lines', 'bars', 'patch'}, ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(4,:));
  genPos(4,:) = uiresize(genH(4), 'l', 'b', 'patch ');
  ud.styleH = genH(4);
  
  % -----------------------------
  % 'Color'
  genPos(5,:) = [genPos(3,1), genPos(3,2)-textPos(4)-lineSpacing, ...
		 textPos(3) textPos(4)];
  genH(5) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Color: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(5,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');

  % auto/manual
  genPos(8,:) = [rhCol genPos(5,2) 1 1]; % set W & H in uiresize()
  genH(8) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'String', {'Auto'; 'Manual'},  ...
		      'HorizontalAlignment', 'center', ...
		      'Callback', [mfilename '(''auto_manual'')'], ...
		      'Position', genPos(8,:));
  genPos(8,:) = uiresize(genH(8), 'l', 'b');
  
  % edit button
  % set W & H in uiresize()
  genPos(6,:) = [rhCol genPos(8,2)-textPos(4)-lineSpacing, 1 1]; 
  genH(6) = uicontrol(fh, 'Style', 'pushbutton', ...
		      'String', 'Edit', ...
		      'HorizontalAlignment', 'center', ...
		      'Callback', [mfilename '(''color'')'], ...
		      'Position', genPos(6,:));
  genPos(6,:) = uiresize(genH(6), 'l', 'b', 'Edit');
  
  genPos(7, 1) = genPos(6,1) + genPos(6,3) + spacing(1);
  genPos(7, [2 4]) = genPos(6,[2 4]);
  genPos(7, 3) = genPos(7, 4);
  genH(7) = uicontrol(fh, 'Style', 'pushbutton', ...
		      'BackgroundColor', defaults.color, ...
		      'HorizontalAlignment', 'center', ...
		      'Callback', [mfilename '(''color'')'], ...
		      'Position', genPos(7,:));

  ud.editH = genH(6);
  ud.colorH = genH(7);
  ud.menuH = genH(8);
  
  % make frame the right size. Bottom left of a bounding box is the
  % bottom left of the frame.
  bb = boundingbox(get(fh, 'Children'));
  genPos(1,3:4) = bb(3:4) + spacing;
  figPos(3:4) = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  set([genH(1) fh], {'Position'}, {genPos(1,:); figPos});
  
  okPos = [10 10 1 1];
  [ud.oh ud.ch] = okcancel('init', fh, okPos, [mfilename '(''ok'')'], ...
			   [mfilename '(''cancel'')']);

  set(fh, 'UserData', ud, ...
	  'Visible', 'on', ...
	  'Resize', 'off');
  % initialise control states
  feval(mfilename, 'auto_manual', ud.menuH, fh);
  
  waitfor(ud.oh); % wait for the ok button to be deleted

  % resumed, other callbacks will have modified UserData by now (or may
  % have) so reload
  if ~ishandle(ud.fh) 
    % window killed (no uf.fh)
    varargout{1} = [];
  else
    ud = get(fh, 'UserData');
    if ~ishandle(ud.ch)
      % cancel pressed (no ud.ch)
      varargout{1} = [];
      % delete(ud.fh);
      close(fh);
    else
      % ok pressed
      menuStr = get(ud.styleH, 'String');
      if get(ud.menuH, 'Value') == 1
	colorArgs = {};  % auto, use default color
      else
	colorArgs = {'color', get(ud.colorH, 'BackgroundColor')}; % manual
      end
      varargout{1} = {'style', menuStr{get(ud.styleH, 'Value')}, ...
		      colorArgs{:}};
      close(fh);
    end
  end
  return;

  
  % --------------------------------------------------------------
 case 'ok'
  % mfilename('ok')
  ud = get(gcbf, 'UserData');
  % everything ok, just activate the previous waitfor by deleting ok
  % handle
  delete(ud.oh);
  
  % --------------------------------------------------------------
 case 'cancel'
  % mfilename('cancel')
  ud = get(gcbf, 'UserData');
  
  if ishandle(ud.oh) & ishandle(ud.ch)
    % signal cancel pressed by deleting the cancel button, then
    % activate the previous waitfor by deleting ok handle. Delete the
    % objects in 2 calls, since this guarantees the cancel button will
    % be deleted before the WAITFOR can resume
    delete(ud.ch);
    delete(ud.oh);
  else
    % (l)user pressed CTRL-C
    delete(ud.fh);
  end
  
  % --------------------------------------------------------------
 case 'color'
  if length(varargin)
    cbo = varargin{1};
    % fh = getparentfigure(cbo);
    fh = varargin{2};
  else
    [cbo, fh] = gcbo;
  end
  
  % fh = gcbf;
  ud = get(fh, 'UserData');
  set(ud.colorH, 'BackgroundColor', ...
		 uisetcolor(get(ud.colorH, 'BackgroundColor'), ...
			    'Select ON colour'));
  set(ud.menuH, 'Value', 2);  % set to manual mode
  % --------------------------------------------------------------
 case 'auto_manual'
  
  if length(varargin)
    cbo = varargin{1};
    % fh = getparentfigure(cbo);
    fh = varargin{2};
  else
    [cbo, fh] = gcbo;
  end

  ud = get(fh, 'UserData');
  value = get(ud.menuH, 'Value');
  switch value
   case 1
    % auto
    set(ud.colorH, 'Enable', 'off', 'Visible', 'off');
    set(ud.editH, 'Enable', 'off');
    
   case 2
    % manual
    set(ud.colorH, 'Enable', 'on', 'Visible', 'on');
    set(ud.editH, 'Enable', 'on');
    
   otherwise
    error('Unknown menu setting');
  end
  return;
  
  % --------------------------------------------------------------
 otherwise
  error('unknown action');
end


