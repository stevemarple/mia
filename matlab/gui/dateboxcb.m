function dateboxcb(db, box)
%DATEBOXCB  Callback function for DATEBOX class.
%
%   DATEBOX(h, box)
%   DATEBOX(db, box)
%
%   h: handle associated with a DATEBOX
%   db: DATEBOX object
%   box: name of the box which initiated the callback
%
%   This function is used internally by DATEBOX.


% May be called with either a DATEBOX object, or the handle to a
% UICONTROL whose userdata contains a DATEBOX object

if strcmp(class(db), 'double')
  db = getdatebox(db);
end

if strcmp(box, 'yday') | ...
      (strcmp(box, 'year') & strcmp(getprevious(db),'yday'))
  % update time from yday and year
  t = getdate(db, 'doy');
  % if isempty(t)
  if ~isvalid(t)
    % bad date
    set(db, 'ForegroundColor', 'red');
  else
    % update
    set(db, 'ForegroundColor', 'black');
    setdate(db, t);
  end

else
  % update from day of month, month and year
  t = getdate(db);
  % if isempty(t)
  if ~isvalid(t)
    % bad date
    set(db, 'ForegroundColor', 'red');
  else
    % update
    set(db, 'ForegroundColor', 'black');
    setdate(db, t);
  end
end


% remember what was adjusted last - ignore year box
if ~strcmp(box, 'year')
  setprevious(db, box);
end

dbs = struct(db);

if ~isempty(dbs.callback)  
  eval(dbs.callback);
end
