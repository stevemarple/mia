function appendcallback(handle, varargin)
%APPENDCALLBACK Add another callback function.
%
%   APPENDCALLBACK(h, newcallback);
%   APPENDCALLBACK(h, callbackFcn, newcallback);
%
%   h: handle to object which should be modified
%   newcallback: String containing the callback
%   callbackFcn: optionally choose a callback function, instead of using
%   the default 'Callback'
%
%   Callbacks can be assigned to multiple handles in one call. If
%   different callbacks are to be assigned to different handles
%   newcallback (and if appropriate callbackFcn) should be CELL
%   matrices. If the same newcallback/callbackFcn is to be applied to all
%   handles it may be either a 1x1 CELL matrix or a string.
%
%   See also SET, beambox/APPENDCALLBACK.

szh = size(handle);
switch nargin
  case 2
    callbackFunctionName = repmat({'Callback'}, szh);
    if ischar(varargin{1})
      newcallback = repmat({varargin{1}}, szh);
    else
      newcallback = varargin{1};
    end
      
  case 3
    if ischar(varargin{1})
      callbackFunctionName = repmat({varargin{1}}, szh);
    else
      callbackFunctionName = varargin{1};
    end
    if ischar(varargin{2})
      newcallback = repmat({varargin{2}}, szh);
    else
      newcallback = varargin{2};
    end
    
  otherwise
    error('Incorrect number of parameters');
end


cbs = get(handle, callbackFunctionName);
l = length(handle);
for n = 1:l
  c = cbs{n};
  
  % add trailing semicolon if needed
  if isempty(c)
    set(handle(n), callbackFunctionName, newcallback);
    
  else
    if c(end) ~= ';'
      set(handle(n), callbackFunctionName{n}, [c ';' newcallback{n}]);
    else
      set(handle(n), callbackFunctionName{n}, [c newcallback{n}]);
    end
  end
end




