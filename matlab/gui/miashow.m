function varargout = miashow(action, varargin)
%MIASHOW Show information about an MIA_BASE derived object.
%
%   fh = MIASHOW('init', mia);
%   
%   fh: figure handle
%   mia: MIA_BASE or derived object
%
%   Actions other than 'init' are used by the callbacks.

% This function uses UserData in the UIMENU objects and also the
% figure. The figure UserData area might be shared, so store data in a
% struct, providingthe field names don't clash the UserData area can be
% shared  successfully with a little cooperation. This means that before
% saving to UserData it should be loaded, and each field set one by one
% (usually only necessary to set the one which we wish to update). Beware
% of loading UserData (ready to save) and then calling another function
% which modifies it before the data is saved! 

if isa(action, 'mia_base')
  % make calling function easier for command line use
  varargin{1} = action;
  action = 'init';
end


switch action
  % --------------------------------------------------------------
 case 'init'
  % mfilename('init', mia, ...)
  defaults.buildtime = timespan;
  defaults.heater_data = [];
  [defaults unvi] = interceptprop({varargin{2:length(varargin)}}, ...
				  defaults); 

  % check the second argument is derived from mia_base
  if ~isa(varargin{1}, 'mia_base')
    error('Data must be derieved from mia_base');
  end
  
  figName = gettype(varargin{1},'c');
  
  % create figure using non-reuseable non-integer handles
  % this is important for windows which call irisedit - since it has to
  % put the data back into the window
  fh = figure('Name', figName, ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'HandleVisibility', 'on', ...
	      'IntegerHandle', 'off', ...
	      'MenuBar', 'None');

  helpData = {'Data view', ...
	      sprintf('miahelp(''%s/dataview.html'');', ...
		      gettype(varargin{1})), ...
	      'help_dataview'}; 
  miamenu(fh, 'help', helpData);
  
  % Add standard toolbar
  toolbarh = miatoolbar('init', ...
			'parent', fh);
  set(findobj(toolbarh, 'Type', 'uipushtool', 'Tag', 'miasave'), ...
      'ClickedCallback', [mfilename '(''save'');']);
  
  % store some useful information in a struct in the UserData area -
  % but only after fetching it first (see notes above).
  ud = get(fh, 'UserData');
  ud.mia = varargin{1}; % the data passed to be plotted/viewed etc

  % amount of time (a TIMESPAN) it took to make
  ud.buildTime = defaults.buildtime;

  % allow only one edit function to be active at once since they request
  % data in this window to be updated
  ud.editors = []; % figure handle to last editor started

  % get the data class to print information about itself into a box,
  % get the handles of the objects it created and trust the function to
  % set the parameters of the objects sensibly!
  % pos = [10 50 0 0]    
  display(ud.mia, fh, [99 20  1 1], [120 20 1 1]);
  set(findobj(fh, 'Type', 'uicontrol', 'Style', 'pushbutton', ...
	      'Tag', 'processing'), 'Callback', [mfilename '(''showproc'');']);
  
  spacing = [20 20];

  % kids = get(fh, 'Children');
  % kids = findobj(fh, 'Children');
  % kidsPos = get(kids, 'Position');
  % figBB = boundingbox(kids);
  
  % find Child uicontrols only
  uiH = findobj(fh, 'Type', 'uicontrol');
  uiPos = get(uiH, 'Position');
  figBB = boundingbox(uiPos);
  
  screenSize = get(0, 'ScreenSize');
  figPos = get(fh, 'Position');
  newFigPos = [figPos(1:2) figBB(3:4) + 2*spacing];

  % approx height, in pixels, of surrounding frame (no way to find out
  % true value) 
  windowBar = 80; 
  if newFigPos(2)+newFigPos(4) > screenSize(4) - windowBar
    % adjust so it does not stick off the top of the screen
    newFigPos(2) = screenSize(4) - newFigPos(4) - windowBar;
  end
  % work out how much to translate child objects by. bottom left after
  % translation should be (spacing).
  trans = [(spacing - figBB(1:2)) 0 0];
  % add trans to uiH and set positions (but its a cell array)
  for n = 1:length(uiPos)
    if length(uiPos{n}) == 4
      % a real position vector - not a uimenu position scalar
      uiPos{n} = uiPos{n} + trans;
    end
  end
  set(uiH, {'Position'}, uiPos);
  
  % update the state information and position. Now that UserData exists
  % in the figure add a close request function. (Closereq needs
  % UserData to work)
  set(fh, 'Position', newFigPos, ...
	  'UserData', ud, ...
	  'CloseRequestFcn', [mfilename '(''closereq'');']);
  %-------------------
  % Customise the menus
  % File
  saveMenuH = findobj(fh, 'Tag', 'file_save');
  set(saveMenuH, 'Visible', 'on', ...
		 'Callback', [mfilename '(''save'',''mat'');']);
  exportMenuH = findobj(fh, 'Tag', 'file_export');
  set(exportMenuH, 'Visible', 'on');
  uimenu(exportMenuH, 'Label', 'ASCII format...', ...
	 'Callback', [mfilename '(''export'',''ascii'');'], ...
	 'Tag', 'export_ascii');    

  % trap the close item - if the data took a long to time to build ask
  % if it is really finished with
  set(findobj('Tag', 'file_close'), ...
      'Callback', [mfilename '(''closereq'');']);
  
  if isa(ud.mia, 'rio_base')
    editMenuH = findobj(fh, 'Type', 'uimenu', 'Tag', 'edit');
    uimenu(editMenuH, 'Label', 'Remove scintillation', ...
	   'Callback', [mfilename '(''remscint'');']);

    if exist('irisedit') 
      set(editMenuH, 'Visible', 'on');
      uimenu(editMenuH, 'Label', 'Data editor', ...
	     'Callback', [mfilename '(''edit'');']);
    end
  end
  
  if isa(ud.mia, 'mia_image_base')
    % for imagedata there is an automated image plot routine
    filePrintH = findobj(fh, 'Type', 'uimenu', 'Tag', 'file_print');
    set(filePrintH, 'Visible', 'on', ...
		    'Callback', [mfilename '(''imageprint'');']);
  end
  % -------
  % Tools
  toolsMenuHandle = findobj(fh, 'Tag', 'tools');
  set(toolsMenuHandle, 'Visible', 'on');
  
  % for now there is no way of doing line plots on imagedata. There
  % ought to be, but it will need a new dialog box since the options
  % will have to be rather different

  if ~isa(ud.mia, 'mia_image_base')
    uimenu(toolsMenuHandle, 'Label', 'Line plot', ...
	   'Tag', 'lineplot', ...
	   'Callback', [mfilename '(''lineplot'');']);
  end
  if isa(ud.mia, 'mia_image_base')
    uimenu(toolsMenuHandle, 'Label', 'Image plot', ...
	   'Tag', 'imageplot', ...
	   'Callback', [mfilename '(''image'');']);
  end
  
  % no need to have option to create images if the data IS images
  if ~isa(ud.mia, 'mia_image_base')
    uimenu(toolsMenuHandle, 'Label', 'Image data', ...
	   'Tag', 'imagedata', ...
	   'Callback', [mfilename '(''imagedata'');']);
  else
    % on the other hand keograms have to be created from images
    uimenu(toolsMenuHandle, 'Label', 'Keogram', ...
	   'Tag', 'keogram', ...
	   'Callback', [mfilename '(''keogram'');']);

    % animated GIF movies
    uimenu(toolsMenuHandle, 'Label', 'Movie (GIF)', ...
	   'Tag', 'gifmovie', ...
	   'Callback', [mfilename '(''gifmovie'');']);
    
    % geographic map
    uimenu(toolsMenuHandle, 'Label', 'Overlay map', ...
	   'Tag', 'overlaymap', ...
	   'Callback', [mfilename '(''overlaymap'');']);
  end

  if isa(ud.mia, 'rio_power')
    uimenu(toolsMenuHandle, 'Label', 'S4 plot', ...
	   'Tag', 's4plot', ...
	   'Callback', [mfilename '(''s4plot'');']);
  end

  if exist('heatermenu')
    heatermenuHandle = ...
	heatermenu('init', toolsMenuHandle, ...
		   'details', 'on', ...
		   'starttime', getstarttime(ud.mia), ...
		   'endtime', getendtime(ud.mia), ...
		   'heater_data', defaults.heater_data);
    set(heatermenuHandle, 'Separator', 'on');
  end

  
  % Add a view menu, after writing back to UserData, else the class
  % data from VIEWMENU will be overwritten
  viewmenu(fh);
  set(fh, 'Visible', 'on');
  varargout{1} = fh;
  return;
  
  % --------------------------------------------------------------
 case 'showproc'
  fh = gcbf;
  ud = get(fh, 'UserData');
  proc = getprocessing(ud.mia);
  disp(' ');
  disp('Processing: ');
  procsize = prod(size(proc));
  if procsize
    for n = 1:procsize
      disp(proc{n});
    end
  else
    disp('(none)');
  end
  % errordlg('test')
  
  % --------------------------------------------------------------
 case 'edit'
  fh = gcbf;
  ud = get(fh, 'UserData');
  if ishandle(ud.editors)
    errordlg(sprintf(['Dataset can have only one active' ...
		      ' editor.\nPlease close data window titled ' ...
		      '"%s" before proceeding.'], ...
		     get(ud.editors, 'Name')), 'Error', 'modal');
    return;
  else
    ud.editors = irisedit('init', ud.mia, ...
			  'fighandle', fh, ...
			  'updatefcn', {mfilename, 'updatemia', fh});
    set(fh, 'UserData', ud);
    viewmenu(fh, ud.editors);
  end
  
  % --------------------------------------------------------------
 case 'remscint'
  fh = gcbf;
  ud = get(fh, 'UserData');
  if ishandle(ud.editors)
    errordlg(sprintf(['Dataset can have only one active' ...
		      ' editor.\nPlease close data window titled ' ...
		      '"%s" before proceeding.'], ...
		     get(ud.editors, 'Name')), 'Error', 'modal');
    return;
  else
    ud.editors = remscintdlg('init', ...
			     'fighandle', fh, ...
			     'updatefcn', {mfilename, 'updatemia', fh}, ...
			     'getdatafcn', {mfilename, 'getmia', fh});
    set(fh, 'UserData', ud);
  end
  
  % --------------------------------------------------------------
 case 'getmia'
  % mfilename('updatemia', fh, newmia);
  ud = get(varargin{1}, 'UserData');
  varargout{1} = ud.mia;

  % --------------------------------------------------------------
 case 'updatemia'
  % mfilename('updatemia', fh, newmia);
  fh = varargin{1};
  ud = get(fh, 'UserData');
  ud.mia = varargin{2};
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
 case 'lineplot'
  % Plot button pressed
  fh = gcbf;  % this figure
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  ud = get(fh, 'UserData');
  % find heater event (if one exists)
  heaterLoadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  if ~isempty(heaterLoadH)
    hlUd = get(heaterLoadH, 'UserData');
  else
    hlUd.heater_data = [];
  end

  % plotdlg('init', ud.mia, fh, {mfilename, 'getmia', fh}, ...
  %  'heater_data', hlUd.he);    
  if isa(ud.mia, 'rio_base')
    newFh = rioplotdlg('init', ...
		       'data', ud.mia, ...
		       'getdatafcn', {mfilename, 'getmia', fh}, ...
		       'heater_data', hlUd.heater_data);
  elseif isa(ud.mia, 'mia_image_base')
    errordlg('Line plots for images are not yet implemented', 'Error', ...
	     'modal');
    return;
  elseif isa(ud.mia, 'mag_data')
    newFh = plot(ud.mia);
  else
    errordlg('Uknown data type', 'Error', 'modal');
    return;
  end
    
  set(fh, 'Pointer', pointer);
  viewmenu(fh, newFh);
  
  return;

  % --------------------------------------------------------------
 case 'maxlineplot'
  % Plot button pressed
  fh = gcbf;  % this figure
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  ud = get(fh, 'UserData');
  maxlineplotdlg('init', ud.mia, fh);    
  set(fh, 'Pointer', pointer);

  return;
  
  % --------------------------------------------------------------
 case 'image'
  fh = gcbf;
  ud = get(fh, 'UserData');
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  % find heater event (if one exists)
  heaterLoadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  if ~isempty(heaterLoadH)
    hlUd = get(heaterLoadH, 'UserData');
  else
    hlUd.heater_data = [];
  end
  mia_imageplot_dlg('init', ud.mia, 'viewmenulink', fh, ...
		    'heater_data', hlUd.heater_data);
  set(fh, 'Pointer', pointer);
  
  % --------------------------------------------------------------
 case 'imagedata'
  fh = gcbf;
  ud = get(fh, 'UserData');
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  
  % find heater event (if one exists)
  heaterLoadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  if ~isempty(heaterLoadH)
    hlUd = get(heaterLoadH, 'UserData');
  else
    hlUd.heater_data = [];
  end
  
  if isa(ud.mia, 'rio_base')
    rio_imagedlg('init', ...
		 'data', ud.mia, ...
		 'getdatafcn', {mfilename, 'getmia', fh}, ...
		 'heater_data', hlUd.heater_data);
  else
    errordlg('Unknown data type', 'Error', 'modal');
  end
  set(fh, 'Pointer', pointer);

  % --------------------------------------------------------------
 case 'keogram'
  disp('***** Keograms aren''t fully tested...  *****');
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');

  ud = get(fh, 'UserData');
  % [newFigureH gh] = keogram(ud.mia);
  % 
  % % create a new View menu object to view it with
  % viewmenu(fh, newFigureH);

  mia_keogram_dlg('init', ud.mia, 'parentfh', fh);
  set(fh, 'Pointer', pointer);
  return;
  
  % --------------------------------------------------------------
 case 'gifmovie'
  % mfilename('gifmovie');
  % mfilename('gifmovie', fh);
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  disp(['***** Movie will have fixed size and delay until I write' ...
	' the forms window *****']);
  
  ud = get(fh, 'UserData');
  [filename] = getfilename(ud.mia);
  if isempty(filename)
    filename = 'untitled';
    Path = '.';
  else
    % remove any extension from the filename
    [Path filename ext] = fileparts(filename);
  end
  [filename Path] = uiputfile(fullfile(Path, [filename '.gif']), ...
			      'Save animated GIF movie');
  movie(ud.mia, 'filename', filename);
  set(fh, 'Pointer', pointer);
  return;
  
  % --------------------------------------------------------------
 case 'overlaymap'
  fh = gcbf;
  ud = get(fh, 'UserData');
  nmax = getdatasize(ud.mia, 3);
  n = 0;
  while n < 1 | n > nmax
    n = inputdlg(sprintf('sample number (min = 1, max = %d)', nmax), ...
		 'Enter sample number', 1, {'1'});
    if isempty(n)
      % cancel pressed
      return;
    else
      % n = atoi(n{1});
      n = fix(str2num(n{1}));
    end
  end
  disp(['User should be given the option to overlay into an '...
	'existing axes. Do this with ' ...
	'"waitfor(0,''CurrentFigure''),get(0,''CurrentFigure'')"' ...
	'Then get the current axes from the current figure (if' ...
	' exists)']);
  
  
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  % newFh = map(ud.mia, 'time', n);
  % viewmenu(fh, newFh);
  [newFh ah] = scand;
  map(ud.mia, ah, 'time', n);
  set(fh, 'Pointer', pointer);
  return;
  
  % --------------------------------------------------------------
 case 's4plot'
  % Plot button pressed
  fh = gcbf;  % this figure
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  ud = get(fh, 'UserData');

  % find heater event (if one exists)
  heaterLoadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  if ~isempty(heaterLoadH)
    hlUd = get(heaterLoadH, 'UserData');
  else
    hlUd.heater_data = [];
  end
  
  newFh = rio_s4plotdlg('init', 'data', ud.mia, ...
			'getdatafcn', {mfilename, 'getmia', fh}, ...
			'heater_data', hlUd.heater_data);    
  set(fh, 'Pointer', pointer);
  viewmenu(fh, newFh);
  
  return;

  % --------------------------------------------------------------
 case 'closereq'
  % Request to close the window
  fh = gcbf;
  if isempty(fh)
    fh = gcf; % close called from command-line, so no callback
  end
  ud = get(fh, 'UserData');
  if ud.buildTime > timespan(1,'m')
    button = questdlg(['This data took ' ...
		       char(floor(ud.buildTime, timespan(1,'s')),'c') ...
		       ' to make, are you sure you have finished with it?'], ...
		      'Really close?', 'Yes', 'No', 'Help', 'No');
    if strcmp(button, 'Yes')
      delete(fh);
    elseif strcmp(button, 'Help')
      miahelp([getname(getfiletype(ud.mia)) '/dataview.html#close']);
    end
    
  else
    % close without asking
    delete(fh);
  end
  
  % --------------------------------------------------------------
 case 'save'
  % miashow('save'):   save as matlab class
  fh = gcbf;
  ud = get(fh, 'UserData');
  
  
  [initPath initFile initExt] = fileparts(getfilename(ud.mia));
  if isempty(initFile)
    initFile = '*.mat';
  end
  if isempty(initPath)
    [filename Path] = uiputfile([initFile initExt], ...
				['Save ' gettype(ud.mia, 'l')]);
  else
    % uiputfile always wants to save in current directory, so make the
    % preferred directory the current one.
    cwd = cd;
    cd(initPath);
    [filename Path] = uiputfile([initFile initExt], ...
				['Save ' gettype(ud.mia, 'l')]);
    cd(cwd);
  end
  if isequal(filename, 0)
    disp(sprintf('\rNot saved'));
    return;
  elseif isempty(filename) | isempty(Path) 
    errordlg('Please choose a valid filename', ...
	     'Error', 'modal');
    return;
  else 
    pathfile = fullfile(Path, filename);
    ud.mia = setfilename(ud.mia, pathfile);
    mia = ud.mia;
    save(pathfile, 'mia');
  end
  disp(sprintf('Data saved to %s', pathfile));
			      
  % saved - so allow window to be closed without querying if ok
  ud.buildTime = timespan(0,'s');

  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
  % export
  case 'export'
  fh = gcbf;
  ud = get(fh, 'UserData');
  if nargin ~= 2
    error('incorrect parameters');
  end

    saveFormat = varargin{1};
  
  switch saveFormat
   case 'ascii'
    % exportasciidlg('init',ud.mia);
    [file Path] = uiputfile('*', ['Export ' gettype(ud.mia, 'l') ...
		    ' as ASCII']); 
    if isequal(file, 0) | isequal(Path, 0)
      disp('not saved');
    else
      f = fullfile(Path, file);
      ascii(ud.mia, f);
    end
   otherwise
    error(['unknown format (was ''' saveFormat ''')']);
    
    
  end
  

  % --------------------------------------------------------------
  % print
 case 'print'
  printdlg(gcbf);

  % --------------------------------------------------------------
  % imageprint
 case 'imageprint'
  cbf = gcbf;
  ud = get(cbf, 'UserData');
  if matlabversioncmp('>', '6.1')
    Path = uigetdir(pwd, 'Select directory to save files into');
    if isempty(Path)
      return;
    end
  else
    Path = pwd;
  end
  dev = inputdlg('Print device type (eg ''psc2'')', ...
		 'Select printer device type', 1, {'psc2'});
  if isempty(dev)
    return;
  end
  dev = dev{1};
  if ~(length(dev) > 2 & strcmp(dev(1:2), '-d'))
    dev = ['-d' dev];
  end
  
  append = questdlg('Append images into one file?', 'Append images', ...
		    'Yes', 'No', 'Yes');
  if strcmp(append, 'Yes')
    append = 1;
  else
    append = 0;
  end
  
  
  clim = [nan nan];
  if isa(ud.mia, 'rio_image')
    oc = getoriginalclass(ud.mia);
    if isa(feval(oc{1}), 'rio_abs')
      clim(1) = 0;
    end
  end
  imageplot(ud.mia, ...
	    'visible', 'off', ...
	    'clim', clim, ...
	    'autoprint', 1, ...
	    'autoprintappend', append, ...
	    'autoprintdevice', dev, ...
	    'autoprintpath', Path);
  
  
  % --------------------------------------------------------------
 otherwise
  error('unknown action');
  
end












