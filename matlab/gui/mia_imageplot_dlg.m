function varargout = mia_imageplot_dlg(action, varargin)
%MIA_IMAGEPLOT_DLG  Dialogue box for creating an image plot.
%
%   fh = MIA_IMAGEPLOT_DLG('init', mia, ...)
%   fh = MIA_IMAGEPLOT_DLG('init', mia, 'windowstyle', 'normal', ...)
%
%   options = MIA_IMAGEPLOT_DLG('init', mia, 'windowstyle', 'modal', ...)
%
%   fh: figure handle of dialog box
%   options: CELL array of options to use when calling IMAGEPLOT
%   mia: MIA_IMAGE_BASE object
%   
%   The default behaviour is to create a non-modal window, and return the
%   handle of the window, but if windowstyle is set to 'modal' then an
%   options CELL array is returned when the window is closed. An empty CELL
%   array indicates that cancel was pressed.
%
%   a number of parameter name/value pairs may be specified to modify the
%   default behaviour:
%
%     'miniplots', [1x2 DOUBLE]
%     Set the inital number of sub plots (x, y)
%
%     'startsample', [1x1 DOUBLE]
%     Set the initial starting sample
%
%     'endsample', [1x1 DOUBLE]
%     Set the initial end sample
%
%     'step', [1x1 DOUBLE]
%     Set the initial step size
%
%     'heaterevent' [HEATER_DATA]
%     Set the initial heater event (if heatermenu created)
%
%     'windowstyle' ['normal' | 'modal']
%     The type of window style and behaviour to use. See above for details.
%
%
%   See also IMAGEPLOT.

switch action
  % --------------------------------------------------------------
 case 'init'
  % mfilename('init', mia, ...);
  mia = varargin{1};  % any data derived from mia
  if ~isa(mia, 'mia_image_base')
    error('mia must be derived from mia_image_base');
  end
  dataSize = getdatasize(mia);

  defaults.miniplots = [8 8]; % NB this is (x y)
  defaults.startsample = 1;
  defaults.endsample = min(defaults.startsample - 1 + ...
			   prod(defaults.miniplots), dataSize(3));
  % define a string to use for the numberbox processing to
  % automatically limit the input
  defaults.startProcStr = sprintf('n=round(min(max(1,n),%d));', ...
				  dataSize(3));
  defaults.step = 1;
  
  % attempt to be a little smarter with the limits
  defaults.limits = autolimit(getlimits(mia));
  [defaults.xpixelpos, defaults.ypixelpos, defaults.pixelunits] = ...
      getpixels(mia);
  
  % the initial heaterevent (if heatermenu created)
  defaults.heaterevent = [];
  defaults.viewmenulink = gcbf;
  defaults.windowstyle = 'normal';
  [defaults unvi] = interceptprop({varargin{2:end}}, defaults); 

  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text
  figName =  gettype(mia, 'c');

  fh = figure('Name', figName, ...
	      'NumberTitle', 'off', ...
	      'Visible', 'off', ...
	      'HandleVisibility', 'callback', ...
	      'WindowStyle', defaults.windowstyle, ...
	      'MenuBar', 'None');
  
  % Add standard menus
  helpData = {'Creating images', ...
	      'miahelp(''/images/creating.html'')', 'help_images'};
  miamenu(fh, 'help', helpData);
  
  % add a view menu linking back to the parent window. This gives us a
  % way to link together any children created by this dialog, with
  % those created by the parent window
  if isempty(defaults.viewmenulink)
    viewmenu(fh); % no parent to link to, create a new family of views
  else
    % link to existing family of views
    viewmenu(defaults.viewmenulink, fh);
  end

  bgColour = get(fh, 'Color');

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'Interpolation method:  ');
  textPos = uiresize(textH, 'l', 'b');
  % frameColour = get(textH, 'BackgroundColor');
  frameColour = get(fh, 'Color');

  delete(textH);
  
  % allocate space for the handles and positions
  genH     = zeros(19,1);   % handles to uicontrols in general frame
  genPos   = zeros(19,4);   % positions  "                    "

  % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the buton widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  % frameWidth = 3.9 * textPos(3);
  frameWidth = 3 * textPos(3);
  
  lines = 6;
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+(lines+1)*textPos(4)+lines*lineSpacing]; 
  
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
  % -----------------------------
  % General frame
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', frameColour, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'General' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  % genPos(2,:) = [0 0 30 30];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Image plot', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');

  % 'Plots' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Plots: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  % rowbox = 4, 'rows' = 5, colbox = 6, 'columns' = 7
  genH(4) = numberbox(fh, [0 0 1 1], '%d', ...
		      'n=round(min(max(1,n),30));', defaults.miniplots(2));
  set(genH(4), 'Tag', 'rowbox');
  appendcallback(genH(4), [mfilename '(''miniplots'',''r'');']);
  
  % set to longest possible string (i.e. plural)
  genH(5) =  uicontrol(fh, 'Style', 'text', ...
		       'String', ' rows, ', ...
		       'HorizontalAlignment', 'left', ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive', ...
		       'Tag', 'rows_text');
  
  genH(6) = numberbox(fh, [0 0 1 1], '%d', ...
		      'n=round(min(max(1,n),30));', defaults.miniplots(1));
  set(genH(6), 'Tag', 'colbox');
  appendcallback(genH(6), [mfilename '(''miniplots'',''c'');']);
  
  % use longest possible string
  genH(7) = uicontrol(fh, 'Style', 'text', ...
		      'String', ' columns.', ...
		      'HorizontalAlignment', 'left', ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive', ...
		      'Tag', 'columns_text');
  
  genPos([4:7],:) = uiresize(genH([4:7]), 'l', 'b', {'999'; []; '999'; []}); 

  % x coord of RH column in figure
  textPos(3);
  rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;
  

  % correct the left,bottom parts of the position vectors
  genPos(4:7,2) = genPos(3,2);
  genPos(4,1) = rhCol;
  genPos(5,1) = genPos(4,1) + genPos(4,3);
  genPos(6,1) = genPos(5,1) + genPos(5,3);
  genPos(7,1) = genPos(6,1) + genPos(6,3);
  % move all together
  set(genH(4:7), {'Position'}, num2cell(genPos(4:7,:),2));
  
  % 'Start sample' = 8, end box = 9, printed time = 10
  
  genPos(8,:) = [genPos(3,1), genPos(3,2)-textPos(4)-lineSpacing, ...
		 textPos(3) textPos(4)];
  genH(8) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Start sample: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(8,:), ...
		      'BackgroundColor', frameColour, ...
		      'Enable', 'inactive');
  
  genPos(9,:) = [rhCol genPos(8,2) 1 1];
  genH(9) =  numberbox(fh, genPos(9,:), '%d', ...
		       defaults.startProcStr, defaults.startsample);
  genPos(9,:) = uiresize(genH(9), 'l', 'b', '99999');
  set(genH(9), 'Tag', 'startsample');
  appendcallback(genH(9), [mfilename '(''samples'',''s'');']);

  % unknown since the time is later printed into it - make it as wide
  % as possible
  genPos(10,:) = [rhCol+genPos(9,3)+spacing(1) genPos(8,2) 0 textPos(4)];
  genPos(10,3) = (genPos(1,1) + genPos(1,3) - spacing(1)) - genPos(10,1);
  % s = char(getstarttime(mia) + (defaults.startsample - 1) ... 
  %	   * getresolution(mia));
  s = char(getsamplestarttime(mia, defaults.startsample));
  genH(10) = uicontrol(fh, 'Style', 'text', ...
		       'String', s, ...
		       'Position', genPos(10,:), ...
		       'BackgroundColor', frameColour, ...
		       'HorizontalAlignment', 'left', ...
		       'Enable', 'on', ...
		       'Tag', 'starttimetext');
  
  % 'End sample' = 11, end box = 12, printed time = 13
  % 'step' = 14, box = 15, printed timespan = 16
  genPos(11:13,:) = genPos(8:10,:);
  genPos(14:16,:) = genPos(8:10,:);
  genPos(11:13,2) = genPos(8:10,2) - textPos(4) - lineSpacing;
  genPos(14:16,2) = genPos(11:13,2) - textPos(4) - lineSpacing;

  genH(11) = uicontrol(fh, 'Style', 'text', ...
		       'String', 'End sample: ', ...
		       'HorizontalAlignment', 'right', ...
		       'Position', genPos(11,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');

  genH(12) = numberbox(fh, genPos(12,:), '%d', ...
		       defaults.startProcStr, defaults.endsample);
  set(genH(12), 'Tag', 'endsample');
  appendcallback(genH(12), [mfilename '(''samples'',''e'');']);

  % s = char(getstarttime(mia) + defaults.endsample *
  % getresolution(mia));
  s = char(getsampleendtime(mia, defaults.endsample));
  
  genH(13) = uicontrol(fh, 'Style', 'text', ...
		       'String', s, ...
		       'Position', genPos(13,:), ...
		       'BackgroundColor', frameColour, ...
		       'HorizontalAlignment', 'left', ...
		       'Enable', 'on', ...
		       'Tag', 'endtimetext');

  genH(14) = uicontrol(fh, 'Style', 'text', ...
		       'String', 'Step: ', ...
		       'HorizontalAlignment', 'right', ...
		       'Position', genPos(14,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');
  genH(15) = numberbox(fh, genPos(15,:), '%d', ...	 
		       'n=round(max(1,n));', defaults.step);
  % 'n=round(min(max(1,n),100));', defaults.step);
  set(genH(15), 'Tag', 'samplestep');
  appendcallback(genH(15), [mfilename '(''samples'',''u'');']);

  tmpRes = getresolution(mia);
  if all(tmpRes == tmpRes(1))
    s = char(tmpRes(1) * defaults.step, 'c'); % compact form
  else
    s = '';
  end
  
  genH(16) = uicontrol(fh, 'Style', 'text', ...
		       'String', s, ...
		       'Position', genPos(16,:), ...
		       'BackgroundColor', frameColour, ...
		       'HorizontalAlignment', 'left', ...
		       'Enable', 'on', ...
		       'Tag', 'steptext');
  
  % 'Limit' = 17, menu = 18, box = 19
  genPos(17:19,:) = genPos(14:16,:);
  genPos(17:19,2) = genPos(14,2) - textPos(4) - lineSpacing;
  genH(17) = uicontrol(fh, 'Style', 'text', ...
		       'String', 'Limit: ', ...
		       'HorizontalAlignment', 'right', ...
		       'Position', genPos(17,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');
  genH(18) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'center', ...
		       'String', {'Automatic' 'Fixed'}, ...
		       'Position', genPos(18,:), ...
		       'Callback', [mfilename '(''limits'');'], ...
		       'Tag', 'limitsmenu');
  genPos(18,:) = uiresize(genH(18), 'l','b');
  
  % add box, correct for x position now it is known
  genPos(19,1) = genPos(18,1) + genPos(18,3) + spacing(1);
  genH(19) = numberbox(fh, genPos(19,:), '%g', ...
		       'n=max(0,n);', defaults.limits(2));
  uiresize(genH(19), 'l', 'b', '999.9');
  set(genH(19), 'Tag', 'limitsbox', 'Enable', 'off');
  
  
  genPos(20:21,:) = genPos(17:18,:);
  genPos(20:21,2) = genPos(17,2) - textPos(4) - lineSpacing;
  genH(20) = uicontrol(fh, 'Style', 'text', ...
		       'String', 'Method: ', ...
		       'HorizontalAlignment', 'right', ...
		       'Position', genPos(20,:), ...
		       'BackgroundColor', frameColour, ...
		       'Enable', 'inactive');
  
  % if any new names are added remember to update localGetOptions
  methodNames = {'Automatic', 'Image', 'Surface', 'Surfaceimage'};
  genH(21) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'center', ...
		       'String', methodNames, ...
		       'Position', genPos(21,:), ...
		       'Callback', [mfilename '(''method'');'], ...
		       'Tag', 'methodmenu');
  genPos(21,:) = uiresize(genH(18), 'l','b');
  
  
  okPos = [10 10 1 1];
  [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'');'], ...
			   [mfilename '(''cancel'');']);
  

  % store some information in UserData are. Put as a struct so it could
  % be shared later
  ud = get(fh, 'UserData');
  ud.mia = mia;
  ud.defaults = defaults;
  ud.modaloptions = {}; % 
  set(fh, 'UserData', ud);
  

  % get the rows/columns text printed corretly
  feval(mfilename, 'miniplots', 'u', fh);

  set(fh, 'Visible', 'on');
  
  switch defaults.windowstyle
   case 'normal'
    % return the figure handle, 'OK' will call imageplot
    varargout{1} = fh;
    return;
    
   case 'modal'
    % don't return until either OK or cancel has been pressed,
    waitfor(okH);
    if ishandle(fh)
      ud = get(fh, 'UserData');
      varargout{1} = ud.modaloptions;
      close(fh);
    else
      varargout{1} = {};
    end
    
   otherwise
    error(sprintf('unknown window style (was %s)', defaults.windowstyle));
  end

  
  % --------------------------------------------------------------
 case 'limits'
  % mfilename('limits'): called by menu
  switch nargin
   case 1
    ;
   otherwise
    error('incorrect parameters');
  end
  fh = gcbf;
  lmh = gcbo;
  lbh = findobj(fh, 'Tag', 'limitsbox');
  % find out which way it has been set. 
  switch get(lmh, 'Value')
   case 1
    % automatic, turn off numberbox
    set(lbh, 'Enable', 'off');
   case 2
    % fixed, turn on numberbox
    set(lbh, 'Enable', 'on');
   otherwise
    error('unknown menu setting');
  end
  
  % --------------------------------------------------------------
 case 'samples'
  % mfilename('samples','s'): start sample changed
  % mfilename('samples','e'): end sample changed
  % mfilename('samples','u'): something else changed, do update
  % If not in callback add the figure handle as an extra parameter 

  % check if the start or end samples need changing. The proc string
  % passed to NUMBERBOX at creation time should ensure they are always
  % in the range 1 <= n <= numOfSamples
  if nargin == 2
    fh = gcbf; % figure handle
  else 
    fh = varargin{2};
  end
  ud = get(fh, 'UserData');
  starttime = getstarttime(ud.mia);
  endtime = getendtime(ud.mia); 
  resolution = getresolution(ud.mia);
  numOfSamples = getdatasize(ud.mia, 3);
  stth = findobj(fh, 'Tag', 'starttimetext');
  etth = findobj(fh, 'Tag', 'endtimetext');
  rowBox = getnumberbox(findobj(fh, 'Tag', 'rowbox'));
  colBox = getnumberbox(findobj(fh, 'Tag', 'colbox'));
  rows = getvalue(rowBox);
  cols = getvalue(colBox);
  step = getvalue(getnumberbox(findobj(fh, 'Tag', 'samplestep')));
  miniplots = rows * cols;
  switch varargin{1}
   case 's'
    % called by start sample box
    sbh = gcbo;
    ebh = findobj(fh, 'Tag', 'endsample');
   case 'e'
    % called by end sample box
    sbh = findobj(fh, 'Tag', 'startsample');
    ebh = gcbo;
   otherwise 
    % called by neither
    sbh = findobj(fh, 'Tag', 'startsample');
    ebh = findobj(fh, 'Tag', 'endsample');
  end
  sb  = getnumberbox(sbh); % start box
  eb  = getnumberbox(ebh); % end box
  st  = getvalue(sb);
  et  = getvalue(eb);

  % update step time
  stepth = findobj(fh, 'Tag', 'steptext');
  %%% set(stepth, 'String', char(sum(getresolution(ud.mia, st+step-1)), 'c'));
  [res mesg] = getresolution(ud.mia);
  if isempty(mesg)
    resstr = char(res * step);
  else
    resstr = '?';
  end
  set(stepth, 'String', resstr);
  
  
  % now do the update
  switch varargin{1}
   case {'s', 'u'}
    % see if we need to alter end sample
    et = min(numOfSamples, st + miniplots*step); 
    samplesUsed = et - st + 1; % this many samples requested
    
    % max plottable round to a value which is visible with given step size
    samplesUsed = min(samplesUsed, miniplots*step); 
    samplesUsed = floortoval(samplesUsed, step); 
    et = st + samplesUsed - step;
    if et < st
      et = st;
    end
    
   case 'e'
    st = max(1, et - miniplots*step);
    samplesUsed = et - st + 1; % this many samples requested
    
    % max plottable round to a value which is visible with given step size
    samplesUsed = min(samplesUsed, miniplots*step); 
    samplesUsed = floortoval(samplesUsed, step);
    st = et - samplesUsed + step;
    if st < 0
      st = min(mod(st, step), et)
    end
    if st == 0
      st = step;
    end
    
   otherwise
    error('unknown uicontrol'); % oops
  end
  setvalue(sb, st);
  setvalue(eb, et);

  % set(stth, 'String', char(starttime + ((st-1) * resolution)));
  set(stth, 'String', char(getsamplestarttime(ud.mia, st)));
  % set(etth, 'String', char(starttime + (et * resolution)));
  set(etth, 'String', char(getsampleendtime(ud.mia, et)));
  
  % --------------------------------------------------------------
 case 'miniplots'
  % mfilename('miniplots', 'r'); % rows
  % mfilename('miniplots', 'c'); % columns
  % mfilename('miniplots', 'u', fh); % general update
  fh = gcbf;
  if varargin{1} == 'u'
    fh = varargin{2};
  else
    fh = gcbf;
  end
  
  if varargin{1} == 'r' | varargin{1} == 'u'
    % update row  text
    rows = getvalue(getnumberbox(findobj(fh, 'Tag', ...
					 'rowbox')));
    if rows > 1
      str = ' rows, ';
    else
      str = ' row, ';
    end
    set(findobj(fh, 'Tag', 'rows_text'), 'String', str);
  end
  
  if varargin{1} == 'c' | varargin{1} == 'u'
    % update column text
    cols = getvalue(getnumberbox(findobj(fh, 'Tag', ...
					 'colbox')));
    if cols > 1
      str = ' columns.';
    else
      str = ' column. ';
    end
    set(findobj(fh, 'Tag', 'columns_text'), 'String', str);
  end
  
  % now update the samples
  feval(mfilename, 'samples', 'u', fh);
  
  % --------------------------------------------------------------
 case 'grid'
  % mfilename('grid', 'x');
  % mfilename('grid', 'y');
  fh = gcbf;
  h = gcbo;
  s = get(h, 'String'); 
  n = unique(mystr2num(s)); % also sorts
  set(h, 'String', printseries(n));

  % --------------------------------------------------------------
 case 'advdefaults'
  switch nargin 
   case 1
    % callback, gcbf better be valid!
    fh = gcbf;
   case 2
    % not callback
    fh = varargin{1};
   otherwise
    error('incorrect parameters');
  end
  ud = get(fh, 'UserData');
  % set units
  unitsh = findobj(fh, 'Tag', 'unitsmenu'); 
  setpopupmenu(unitsh, ud.defaults.units);
  
  % set x grid (pixel) positions
  set(findobj(fh, 'Tag', 'xgridpoints'), 'String', ...
		    printseries(ud.defaults.xpixelpos));

  % set y grid (pixel) positions
  set(findobj(fh, 'Tag', 'ygridpoints'), 'String', ...
		    printseries(ud.defaults.ypixelpos));
  
  % set mapping height
  setvalue(getbeambox(findobj(fh, 'Tag', 'mapheight')), ...
	   ud.defaults.height);
  
  % set interpolation method
  interpmethodh = findobj(fh, 'Tag', 'interpmethod');
  setpopupmenu(interpmethodh, ud.defaults.interpmethod);

  % --------------------------------------------------------------
 case 'method'
  ;
  
  % --------------------------------------------------------------
 case 'ok'
  fh = gcbf;
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  ud = get(fh, 'UserData');
  
  options = localGetOptions(fh);
  ws = get(fh, 'WindowStyle');
  switch ws
   case 'normal'
    % create child figure
    [fhc ghc] = imageplot(ud.mia, options{:});
    viewmenu(fh, fhc);
    % reset pointer on this figure, and set childs pointer to be arrow
    set([fh fhc], {'Pointer'}, {pointer; 'arrow'});
    close(fh);
    
   case 'modal'
    ud.modaloptions = options;
    set(fh, 'UserData', ud);
    delete(findall(fh, 'Type', 'uicontrol', 'Tag', 'ok'));
    
   otherwise
    error(sprintf('unknown window style (was %s)', ws));
  end

  % --------------------------------------------------------------
 case 'cancel'
  % varargout = cell(1,nargout);
  fh = gcbf;
  ud = get(fh, 'UserData');
  ud.modaloptions = {};
  set(fh, 'UserData', ud);
  close(fh);
  
  
  % --------------------------------------------------------------
 otherwise
  error('unknown action');
  
end


% --------------------------------------------------------------
function r = floortoval(n, m)
% floor, rounding n down to a multiple of m. E.g. if m is 2 r is always
% odd, and if m is 10 r always ends in a zero.
r = n - rem(n, m);

% --------------------------------------------------------------
function r = mystr2num(s)
t = s;
[tmp tmpIndex] = setdiff(t, '0123456789:-.'); % keep 0-9:
t(tmpIndex) = ' ';
if ~strcmp(s, t) 
  disp(sprintf('\a')); % string was changed - make an annoying beep
end
r = str2num(['[' t ']']);

% --------------------------------------------------------------
function r = localGetOptions(fh)
%LOCALGETOPTIONS  Get options to use when calling IMAGEPLOT
r = {};

  ud = get(fh, 'UserData');
  % general options
  starttime = getstarttime(ud.mia);
  endtime = getendtime(ud.mia); 
  resolution = getresolution(ud.mia);
  rows = getvalue(getnumberbox(findobj(fh, 'Tag', 'rowbox')));
  cols = getvalue(getnumberbox(findobj(fh, 'Tag', 'colbox')));
  st = getvalue(getnumberbox(findobj(fh, 'Tag', 'startsample')));
  et = getvalue(getnumberbox(findobj(fh, 'Tag', 'endsample')));
  step = getvalue(getnumberbox(findobj(fh, 'Tag', 'samplestep')));
  
  % samples need not be regular
  % plotstarttime = getimagetime(ud.mia, st);
  % plotendtime = getimagetime(ud.mia, et) + getresolution(ud.mia, et);
  plotstarttime = getsamplestarttime(ud.mia, st);
  plotendtime = getsampleendtime(ud.mia, et);
    
  if get(findobj(fh, 'Tag', 'limitsmenu'), 'Value') == 2
    % fixed scaling
    limits = getvalue(getnumberbox(findobj(fh, 'Tag', 'limitsbox')));
    limits = [0 limits];
  else
    % tell class to calculate it itself
    limits = [];
  end

  methodData = get(findobj(fh, 'Tag', 'methodmenu'));
  if isempty(methodData)
    method = '';
  else
    method = lower(methodData.String{methodData.Value});
    if strcmp(method, 'automatic')
      method = '';
    end
  end
  
r = {'clim', limits, ...
     'miniplots', [cols rows], ...
     'starttime', plotstarttime, ...
     'endtime', plotendtime, ...
     'step', step, ...
     'heaterevent', ud.defaults.heaterevent, ...
     'method', method};
     
