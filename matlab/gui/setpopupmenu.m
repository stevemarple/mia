function setpopupmenu(h, str)
%SETPOPUPMENU  Set a popupmenu to show the given string.
%
%   SETPOPUPMENU(h, s)
%   h: handle to a popupmenu UICONTROL
%   s: string to select
%
%   The strings in the popupmenu must be in a CELL array. s must be
%   string in the String cell array.
%
%   See also POPUPSTR.

list = get(h, 'String');
if ~iscell(list)
  error('Menu strings must be in a cell array');
end
found = 0;
for n = 1:prod(size(list))
  if strcmp(list{n}, str)
    set(h, 'Value', n);
    found = n;
    break;
  end
end
if ~found
  warning(['Could not set menubar to ' str]);
end


