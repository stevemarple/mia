function mialoaddlg(action, varargin)
%MIALOADDLG  Load data derived from MIA_BASE into MIA.
%
%

switch action
  % -----------------------------------------------------
 case 'init'
  
  % get the file to open
  [filename Path] = uigetfile('*.mat')
  if ~ischar(filename)
    % cancel pressed or some other error, quit quietly
    return;
  end
  
  pathfile = fullfile(Path, filename);
  w = whos('-file', pathfile)
  s = load(pathfile);
  fn = fieldnames(s);
  for n = 1:length(s)
    f = getfield(s, fn{n});
    if isa(f, 'mia_base')
      for m = 1:prod(size(f))
	miashow('init', f(m));
      end
    else
      disp(sprintf('%s is not a MIA data object', fn{n}));
    end
  end
  
  % -----------------------------------------------------
 otherwise
   error('unknown action');
end
