function pos = uiresize(h, ha, va, varargin)
%UIRESIZE  Resize uicontrols to correct size after reading Extent property.
%
%   pos = uiresize(h, horizAlign, vertAlign);
%   pos = uiresize(h, horizAlign, vertAlign, str);
%   pos: new position vector
%   h: handle to a uicontrol object
%   ha: horizontal alignment, 't' (top), 'm' (middle), 'b' (bottom)
%   va: vertical alignment, 'l' (left), 'c' (centre), 'r' (right)
%   str: cell array of string to use for setting width / height
%   (if h is scalar a char array may be supplied)
%
%   UIRESIZE reads the position and size properties of UICONTROL
%   onjects. Width and height adjustments are made to ensure the text fits
%   within the boundary of the control. For some UICONTROL objects (static
%   text, edit boxes etc.) a vertical offset is also added. This allows
%   buttons placed adjacent to text or other buttons to look centred
%   vertically. For left and bottom alignment the width and height parts of
%   the position vector are ignored. For other alignments they are used to
%   calculate the original centre/middle or right/top edge. Note that the
%   alignments are used to calculate the position vector, they do not
%   affect any settings of HorizontalAlignment.
%
%   See also UICONTROL.

enlargeOnly = 0;
switch nargin
 case 3
  str = cell(size(h));
  
 case 4
  str = varargin{1};
  if ischar(str)
    str = {str};
  end

 case 5
  str = varargin{1};
  if ischar(str)
    str = {str};
  end
  enlargeOnly = varargin{2};
  
 otherwise
  error('incorrect parameters');
end

szh = size(h);
% mess about because 'if' doesn't do well with vectors
% if ~prod(szh == size(ha))
if ~isequal(szh, size(ha))
  if prod(size(ha)) == 1
    ha = repmat(ha, szh);
  else
    error('number of handles and horizontal alignment symbols must match');
  end
end

% if ~prod(szh == size(va))
if ~isequal(szh, size(va))
  if prod(size(va)) == 1
    va = repmat(va, szh);
  else
    error(['sizes of number of handles and vertical alignment symbols' ...
	   ' must match']);  
  end
end

% if (isa(str, 'cell') & ~prod(szh == size(str))) | ...
%       (ischar(str) &  ~prod(szh == [1 1]))
if (isa(str, 'cell') & ~isequal(szh, size(str))) | ...
    (ischar(str) &  ~isequal(szh, [1 1]))
  error('number of handles and strings must match');
end


prop = get(h); % get all properties as a struct

numOfControls = prod(size(h));
pos = zeros(numOfControls, 4);
for n = 1:numOfControls
  
  if ~strcmp(prop(n).Type, 'uicontrol')
    warning('Can only resize uicontrol objects');
    pos(n,:) = prop(n).Position;
    break;
  end

  % if a string for sizing was supplied set it now
  if ~isempty(str{n})
    set(h(n), 'String', str{n});
    prop(n).Extent = get(h(n), 'Extent');
  end
  
  pos(n,:) = prop(n).Position;

  switch prop(n).Style
    
   case 'checkbox'
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + ...
	[0 0 prop(n).Extent(4)+4 6];
    
   case 'edit'
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + [0 2 6 2];

   case 'frame'
    warning('sorry, not yet implemented');
    
   case 'listbox'
    % warning('sorry, not yet implemented');
    % pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + [0 0 6 6]
    if 0 
      pos(n,:) = [prop(n).Position(1:2) 0 0] + ...
	  ((prop(n).Extent + [0 0 0 -6]) .* [1 1 1 length(prop(n).String)]) ...
	  + [0 0 26 6];
      % prop(n).Extent + [0 0 26 26];
    else
      if isempty(str{n})
	tmp = prop(n).String; % use current values
      else
	tmp = str{n};
      end
      if iscell(tmp)
	entries = prod(size(tmp));
      elseif ischar(tmp)
	entries = size(tmp, 1);
      else
	error(sprintf('unknown type for string (was %s)', class(tmp)));
      end
      pos(n,:) = [prop(n).Position(1:2) 0 0] + ...
	  ((prop(n).Extent + [0 0 0 -5]) .* [1 1 1 entries]) ...
	  + [0 0 26 6];
    end
    
   case 'popupmenu'
    if isempty(str{n}) | 1 % set above!
      if iscell(prop(n).String)
	set(h(n), 'Value', 1);
	prop(n).Extent = [0 0 0 0]; % initialise to find largest extents
	for m = 1:prod(size(prop(n).String))
	  set(h(n), 'String', prop(n).String{m});
	  prop(n).Extent = max(prop(n).Extent, get(h(n), 'Extent'));
	end
	set(h(n), 'String', prop(n).String, 'Value', prop(n).Value);
      else
	set(h(n), 'String', maxstring(prop(n).String));
	% update extent
	prop(n).Extent = get(h(n), 'Extent');
	set(h(n), 'String', prop(n).String);
      end
    else
      % be lazy/efficient
      warning(['uiresize for popupmenu and user supplied strings not' ...
	       ' implemented'])
    end
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + ...
	[0 0 prop(n).Extent(4)+4 6];
    % [0 0 30 2];
    
   case 'pushbutton'
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + [0 0 6 6];
    
   case 'radiobutton'
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent + [0 0 22 6];
    
   case 'slider'
    warning('sorry, not yet implemented');
    
   case 'text'
    pos(n,:) = [prop(n).Position(1:2) 0 0] + prop(n).Extent;
    
   case 'togglebutton'
    warning('sorry, not yet implemented');
  end

  % if a string for sizing was used revert back to old string
  if ~isempty(str{n})
    set(h(n), 'String', prop(n).String);
  end

  switch ha(n)
   case 'l'
    ; % already done

   case 'c'
    % keep centre about where was centre
    oldCentre = prop(n).Position(1) + prop(n).Position(3) / 2;
    pos(n,1) = oldCentre - pos(n,3) / 2;
    
   case 'r'
    % right-hand edge in same place
    oldRHEdge = prop(n).Position(1) + prop(n).Position(3);
    pos(n,1) = oldRHEdge - pos(n,3);
    
   otherwise
    error('unknown alignment symbol');
  end

  switch va(n)
   case 'b'
    ; % already done

   case 'm'
    % keep middle about where was middle
    oldMiddle = prop(n).Position(2) + prop(n).Position(4) / 2;
    pos(n,2) = oldMiddle - pos(n,4) / 2;
    
   case 't'
    % top edge in same place
    oldTEdge = prop(n).Position(2) + prop(n).Position(4);
    pos(n,2) = oldTEdge - pos(n,4);
    
   otherwise
    error('unknown alignment symbol');
  end

  if enlargeOnly
    % use bounding box of old and new sizes. The position can only grow
    % (if any direction)
    pos(n,:) = boundingbox(prop(n).Position, pos(n,:));
  end

  set(h(n), 'Position', pos(n,:));
  
end
return;


function s = maxstring(str)
% Given a cell or char array, or 'string1|string2|string3' list, as used
% by popupmenus and listboxes, return the longest string.
s = '';

if isa(str, 'cell')
  nelem = prod(size(str));
  maxLen = length(str{1});
  s = str{1};
  for n = 2:nelem
    len = length(str{n});
    if len > maxLen
      maxLen = len;
      s = str{n};
    end
  end
  return;
  
elseif ischar(str)
  sz = size(str); 
  if sz(1) > 1
    % padded string matrix, no pretty way to find string length
    warning([mfilename ' prefers to uicontrols where multiple strings' ...
	     ' are in cell arrays']); 
    % so do I - this section isn't tested :)

    s = deblank(str(1,:)); % keep current best in s
    maxLen = length(s);
    if maxLen == sz(2);
      return; % cannot be beaten
    end
    for r = 2:sz(1)
      dbstr = deblank(str(r,:));
      len = length(dbstr);
      if len == sz(2)
	s = dbstr;
	return; % cannot be beaten
      elseif len > maxLen
	maxLen = len;
	s = dbstr;
      end
    end
    return;
    
  else
    % could be plain string or 'string1|string2'
    bars = find(str == '|');
    if isempty(bars)
      % plain string
      s = str;
      return;
    else
      % yuck - 'string1|string2'
      warning([mfilename ' prefers to uicontrols where multiple strings' ...
	       ' are in cell arrays']); 
      % so do I - this section isn't tested :)
      barNum = length(bars);
      bars = [0 bars]; % now looks like str = '|string1|string2'
      maxLen = sz(2) - bars(barNum);  % length of last string
      s = str(barNum:sz(2));
      for n = 1:barNum
	len = bars(n+1) - bars(n) - 1;
	if len > maxLen
	  maxLen = len;
	  s = str(bars(n)+1:bars(n+1)-1);
	end
      end
      return;
    end
  end
else 
  % oops!
  error('incorrect type for string');
end

