function tsb = gettimespanbox(handle)
%GETTIMESPANBOX  Return a TIMESPANBOX object from a UICONTROL handle.
%
%   tsb = GETTIMESPANBOX(h)
%   tsb: TIMESPANBOX object
%   h: handle associated with a TIMESPANBOX object
%
%   See also TIMESPANBOX.

switch class(handle)
 case 'timespanbox'
  % already is a tsb!
  % warning('this already is a timespanbox');
  tsb = handle;
  return;

 case 'double'
  if ishandle(handle)
    tsb = get(handle, 'UserData');
    if strcmp(class(tsb), 'timespanbox')
      return;
    else
      tsb = gettimespanbox(tsb);
    end
    
  else
    error('Not a valid handle');
  end
  
 otherwise
  tsb = gettimespanbox(getuserdata(handle));
  return;
end

