function setprevious(seb, p)
% GETPREVIOUS Get the previous box which was modified.
%
% Used to calculate the starttime, endtime or duration; whichever wasn't
% set/adjusted in the last two operations. If two consecutive adjustments
% are made to the same part (starttime, endtime or duration) then don't
% record the second one, just a change. The check for this is made in
% stetboxcb, since that already has the information needed to check for
% that, and it saves a function call in the callback process.

seb.previous = p;
update(seb);
