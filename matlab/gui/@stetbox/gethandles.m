function [h, h2] = gethandles(seb)
%GETHANDLE Return the handles of objects used in the STETBOX.

% h = [seb.starthandle seb.endhandle seb.durationhandle];

tmp1 = gethandles(gettimebox(seb.starthandle));
tmp2 = gethandles(gettimebox(seb.endhandle));
tmp3 = gethandles(gettimespanbox(seb.durationhandle));
h = [tmp1(:); tmp2(:);tmp3(:)];
