function s = getprevious(seb)
% GETPREVIOUS Get the previous box which was modified.

% Used to calculate the starttime, endtime or duration; whichever wasn't
% set/adjusted in the last two operations. If two consecutive adjustments
% are made to the same part (starttime, endtime or duration) then don't
% record the second one, just a change.

s = seb.previous;
