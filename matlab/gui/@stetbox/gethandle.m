function [h, h2] = gethandle(seb)
%GETHANDLE Return the handles of objects used in the STETBOX.

deprecated('use gethandles');


h = [seb.starthandle seb.endhandle seb.durationhandle];

tmp1 = gethandle(gettimebox(seb.starthandle));
tmp2 = gethandle(gettimebox(seb.endhandle));
tmp3 = gethandle(gettimespanbox(seb.durationhandle));
h = [tmp1(:); tmp2(:);tmp3(:)];
