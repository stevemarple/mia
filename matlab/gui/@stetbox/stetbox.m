function handle = stetbox(parent, pos)
%STETBOX Make set of boxes for entering start time/end time/duration.
% 
%   Return handle to first graphics object drawn.
%
%   h= STETBOX(parent, pos)
%
%   See also TIMEBOX, DATEBOX, TIMESPANBOX.
%
% Unlike normal class constructors this one returns a handle to a graphics
% object, the actual class data is stored in the user data area. This is
% because the callbacks really need access to a reference, rather than an
% old static copy of what was in the datebox. By using one of the handles,
% and storing the class object in the UserData area of that handle then we
% effectively have a reference.

% member fields
seb.starthandle = [];
seb.endhandle = [];
seb.durationhandle = [];
seb.previous = [1 2]; % 1 = starttime, 2 = endtime, 4 = duration
seb.userdata = [];

seb = class(seb, 'stetbox');

[seb.starthandle seb.endhandle seb.durationhandle] = makestetbox(seb, ...
    parent, pos);

% Keep only one copy of db, but the callbacks on ALL uicontrols must be
% able to find it - reference it by storing the handle to the uicontrol
% whose UserData contains the relevant db.

setuserdata(gettimebox(seb.starthandle), seb);
setuserdata(gettimebox(seb.endhandle), seb.starthandle);
setuserdata(gettimespanbox(seb.durationhandle), seb.starthandle);

% the boxes already have callbacks so add another
tmp = gettimebox(seb.starthandle);

appendcallback(gethandles(tmp), ...
    'stetboxcb(getstetbox(gcbo), 1);');

tmp = gettimebox(seb.endhandle);
appendcallback(gethandles(tmp), ...
    'stetboxcb(getstetbox(gcbo), 2);');

tmp = gettimespanbox(seb.durationhandle);
appendcallback(gethandles(tmp), ...
    'stetboxcb(getstetbox(gcbo), 4);');

% update and initialise
update(seb);
handle = seb.starthandle;



