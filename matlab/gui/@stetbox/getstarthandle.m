function h = getstarthandle(seb)
%GETSTARTHANDLE Return the handle to the start TIMEBOX.

h = seb.starthandle;
