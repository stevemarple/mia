function t = gettime(seb)
%GETTIME Return the times and duration held in a STETBOX.
%
% t = gettime(seb);

t = {gettime(gettimebox(seb.starthandle)), ...
      gettime(gettimebox(seb.endhandle)), ...
      gettimespan(gettimespanbox(seb.durationhandle))};
