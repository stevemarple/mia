function p = getposition(seb)
%GETPOSITION Return the position matrix for a STETBOX object.
%
%   See also BOUNDINGBOX.

p = boundingbox({getposition(getdatebox(seb.starthandle)), ...
      getposition(getdatebox(seb.endhandle)), ...
      getposition(gettimespanbox(seb.durationhandle))});
