function [stH, etH, durH] = makestetbox(seb, parent, pos)
%MAKESEBOX

spacing = [8 8]; % x,y

% make all items in order we want the tab key to access them.
% 1: start
% 2: end 
% 3: dur
stH = timebox(parent, [0 0 1 1]);
etH = timebox(parent, [0 0 1 1]);
durH = timespanbox(parent, [0 0 1 1], 5, timespan(1,'s'));

% get the boxes
stBox = gettimebox(stH);
etBox = gettimebox(etH);
durBox = gettimespanbox(durH);

% get positions
stPos = getposition(stBox);
etPos = stPos; % assume st and end boxes are the same W and H
durPos = getposition(durBox);

% calculate new positions
durPos(1:2) = [pos(1) + stPos(3) - durPos(3), pos(2)];
etPos(1:2) = [pos(1), pos(2) + spacing(2) + durPos(4)];
stPos(1:2) = [pos(1), etPos(2) + spacing(2) + etPos(4)];

% re-position to correct locations
setposition(stBox, stPos);
setposition(etBox, etPos);
setposition(durBox, durPos);
