function r = setposition(seb, pos)
%SETPOSITION  Set the position of a STETBOX.
%
%   newPos = SETPOSITION(tb, pos)
%   newPos: new position vector
%   sb: STETBOX
%   pos: new position (width and height ignored)

h = gethandles(seb);
oldPos = get(h, 'Position');
if ~iscell(oldPos)
  oldPos = {oldPos};
end

bb = boundingbox(h);


err = [bb(1:2) - pos(1:2) 0 0];
r = bb - err;

b = gettimebox(seb.starthandle);
setposition(b, getposition(b) - err);
b = gettimebox(seb.endhandle);
setposition(b, getposition(b) - err);
b = gettimespanbox(seb.durationhandle);
setposition(b, getposition(b) - err);

