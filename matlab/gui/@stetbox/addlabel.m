function varargout = addlabel(seb, spacing, label)
%ADDLABEL  Add a label to a STETBOX.
%
%   h = ADDLABEL(sb, pos, label)
%   [h pos] = ADDLABEL(..)
%   h: 3x1 cell MATRIX of handles to text objects
%   sb: STETBOX object
%   spacing: spacing between label and box (scalar)
%   label: CELL matrix of text to label with
%
%   The labels are added right-aligned to the left of the STETBOX, with the
%   given spacing. The default figure font and size are used.
%
%   See also STETBOX.

% add the label ourself to avoid too many low level function
% calls. Anyway the timespanbox is not aligned with the other boxes.
startTB = gettimebox(seb.starthandle);
endTB = gettimebox(seb.endhandle);
durationTSB = gettimespanbox(seb.durationhandle);

% get bounding box (position) for the whole sub-box
numBoxes = 3;
boxPos = zeros(numBoxes,4);
boxPos(1,:) = getposition(startTB);
boxPos(2,:) = getposition(endTB);
boxPos(3,:) = getposition(durationTSB);

prop = get(seb.starthandle); % general properties
bgColour = get(prop.Parent, 'Color');

left = min(boxPos(:,1));
h = zeros(1,numBoxes);
for n = 1:numBoxes
  h(n) = uicontrol(prop.Parent, ...
      'Position', [left - spacing - 1, boxPos(n,2), 1, boxPos(n,4)], ...
      'Style', 'text', ...
      'HorizontalAlignment', 'right', ...
      'String', label{n}, ...
      'Enable', 'inactive', ...
      'BackgroundColor', bgColour);
end

pos = uiresize(h, repmat('r', 1, numBoxes), repmat('b', 1, numBoxes));

varargout{1} = h;
if nargout > 1
  varargout{2} = pos;
end
