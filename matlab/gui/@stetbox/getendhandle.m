function h = getendhandle(seb)
%GETENDHANDLE Return the handle to the end TIMEBOX.

h = seb.endhandle;
