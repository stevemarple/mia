function appendcallback(seb, varargin)
%APPENDCALLBACK  Append a callback to a STETBOX.
%
%   APPENDCALLBACK(seb, s)
%   seb: STETBOX
%   s: callback string
%
%   APPENDCALLBACK appends a user defined callback to the UICONTROL
%   objects used by a STETBOX. After the STETBOX has been adjust
%   the callback is called.
%
%   See also STETBOX, APPENDCALLBACK.

appendcallback(gettimebox(seb.starthandle), varargin{:});
appendcallback(gettimebox(seb.endhandle), varargin{:});
appendcallback(gettimespanbox(seb.durationhandle), varargin{:});

