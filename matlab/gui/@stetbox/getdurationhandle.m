function h = getdurationhandle(seb)
%GETDURATIONHANDLE Return the handle to the duration TIMESPANBOX.

h = seb.durationhandle;
