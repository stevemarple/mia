function settime(seb, t)
%SETTIME Set the times and duration held in a STETBOX.
%
% settime(seb, t);

settime(gettimebox(seb.starthandle), t{1});
settime(gettimebox(seb.endhandle), t{2});
settimespan(gettimespanbox(seb.durationhandle), t{3});




