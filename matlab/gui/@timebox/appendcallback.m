function appendcallback(tb, varargin)
%APPENDCALLBACK  Append a callback to a TIMEBOX.
%
%   APPENDCALLBACK(tb, s)
%   tb: TIMEBOX
%   s: callback string
%
%   APPENDCALLBACK appends a user defined callback to the UICONTROL
%   objects used by a TIMEBOX. After the TIMEBOX has been adjust
%   the callback is called.
%
%   See also TIMEBOX, APPENDCALLBACK.

appendcallback(getdatebox(tb.dateboxhandle), varargin{:});
appendcallback(gettimespanbox(tb.timespanhandle), varargin{:});

