function h = gethandle(tb)
%GETHANDLE  Return the uicontrol handles used in a TIMEBOX.
%
%   h = GETHANDLE(tb)
%   h: column vector of handles to UICONTROL objects
%   tb: TIMEBOX object
%
%   See also TIMEBOX.

deprecated('use gethandles');

h = [ gethandle(getdatebox(tb.dateboxhandle)) ...
      gethandle(gettimespanbox(tb.timespanhandle))]; 

