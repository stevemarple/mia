function dv = datevec(tb)
%DATEVEC  Return a DATEVEC-type array of the time/date info in a TIMEBOX.
%
%   dv = DATEVEC(tb)
%   dv: [1x6] vector of [year month day hour minute second]
%   tb: TIMEBOX object
%
%   See also DATEVEC.

tsb = gettimespanbox(tb.timespanhandle);
db = getdatebox(tb.dateboxhandle);

dv = datevec(tsb) + datevec(db);
