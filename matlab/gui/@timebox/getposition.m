function p = getposition(tb)
%GETPOSITION  Return the position matrix for a TIMEBOX object.
%
%   p = GETPOSITION(tb)
%   p: position vector [left bottom width height]
%   tb: TIMEBOX object
%
%   See also BOUNDINGBOX.

p = boundingbox(getposition(getdatebox(tb.dateboxhandle)), ...
    getposition(gettimespanbox(tb.timespanhandle)));

