function varargout = setposition(tb, pos)
%SETPOSITION  Set the position of a TIMEBOX.
%
%   newPos = SETPOSITION(tb, pos)
%   newPos: new position vector
%   tb: TIMEBOX
%   pos: new position (width and height ignored)

db = getdatebox(tb.dateboxhandle);
tsb = gettimespanbox(tb.timespanhandle);

dbPos = setposition(db, pos); % keep new position for its width
spacing = 20; % should be same value as in timebox/timebox.m


% only force TIMESPANBOX to calculate the new position if we need it
if nargout == 0
  setposition(tsb, [dbPos(1)+dbPos(3)+spacing(1), pos(2) 0, 0]);
else
  tsbPos = setposition(tsb, [dbPos(1)+dbPos(3)+spacing(1), pos(2) 0, 0]);
  varargout{1} = boundingbox(dbPos, tsbPos);
end
