function data = getuserdata(tb)
%GETUSERDATA  Get data from internal user data area from a TIMEBOX.
%
%   d = GETUSERDATA(tb)
%   d: data previously set with timebox/SETUSERDATA.
%   tb: TIMEBOX object
%
%   See also TIMEBOX, timebox/SETUSERDATA.

data = tb.userdata;

