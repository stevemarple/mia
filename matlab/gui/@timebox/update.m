function update(tb)
%UPDATE  Update the class object stored in the UserData area.
%
%   See also TIMEBOX.

% probably not a user function. This is needed to work around Matlab's
% lack of references.

setuserdata(getdatebox(tb.dateboxhandle), tb);

