function t = gettime(tb)
%GETTIME  Return a TIMESTAMP object of the time/date info.
%
%   t = GETTIME(tb)
%   t: TIMESTAMP object representing details in TIMEBOX.
%   tb: TIMEBOX object
%
%   See also TIMEBOX, TIME.

% to save on a function call insert the workings of datevec

tsb = gettimespanbox(tb.timespanhandle);
db = getdatebox(tb.dateboxhandle);

t=timestamp(datevec(tsb) + datevec(db));
