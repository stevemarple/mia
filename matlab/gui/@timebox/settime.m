function settime(tb, t)
%SETTIME  Set the time held in a TIMEBOX.
%
%   SETTIME(tb, t)
%   tb: TIMEBOX object
%   t: TIME object
%
%   See also TIMEBOX, TIME, timebox/GETTIME.

ts = t - getdate(t);
setdate(getdatebox(tb.dateboxhandle), t);
settimespan(gettimespanbox(tb.timespanhandle), ts);
