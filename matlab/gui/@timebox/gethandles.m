function h = gethandles(tb)
%GETHANDLES  Return the uicontrol handles used in a TIMEBOX.
%
%   h = GETHANDLES(tb)
%   h: vector of handles to UICONTROL objects
%   tb: TIMEBOX object
%
%   See also TIMEBOX.

h = [ gethandles(getdatebox(tb.dateboxhandle));
      gethandles(gettimespanbox(tb.timespanhandle))]; 

