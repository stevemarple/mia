function setuserdata(tb, data)
%SETUSERDATA  Set the internal user data area of a TIMEBOX.
%
%   SGETUSERDATA(tb, d)
%   tb: TIMEBOX object
%   d: data to be set (any type)
%
%   See also TIMEBOX, timebox/GETUSERDATA.

tb.userdata = data;
update(tb);

