function handle = timebox(parent, pos, varargin)
%TIMEBOX  Create a TIMEBOX for entering time and date.

% member fields
tb.dateboxhandle = []; % class object stored in userdata of databox class
tb.timespanhandle = [];
tb.userdata = [];

tb = class(tb, 'timebox');

% insert the boxes
spacing = 20; % should be same value as in timebox/setposition.m
tb.dateboxhandle = datebox(parent, pos);
dbpos = getposition(getdatebox(tb.dateboxhandle));
    

tb.timespanhandle = timespanbox(parent, ...
    [dbpos(1)+dbpos(3)+spacing(1), dbpos(2), 10, dbpos(4)], ...
    [], timespan(1,'s'));
tsb = gettimespanbox(tb.timespanhandle);

% set numberboxes to do sensible processing for hours, minutes, seconds
% ### DEBUG ### This should be an option on the timespanbox
setprocessing(getnumberbox(gethourhandle(tsb)), ...
    'n=round(min(max(0,n),23));');
setprocessing(getnumberbox(getminutehandle(tsb)), ...
    'n=round(min(max(0,n),59));');
setprocessing(getnumberbox(getsecondhandle(tsb)), ...
    'n=round(min(max(0,n),59));');



% store the way back where tb is kept
setuserdata(getdatebox(tb.dateboxhandle), tb);
setuserdata(gettimespanbox(tb.timespanhandle), tb.dateboxhandle);


% update and initialise?

update(tb);
handle = double(tb.dateboxhandle);


