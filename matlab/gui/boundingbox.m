function p = boundingbox(varargin)
%BOUNDINGBOX  Calculate position property of a boundingbox.
%
%   pos = BOUNDINGBOX(...)
%   pos: [1x4] DOUBLE vector, [left bottom width height]
%
%   The bounding box is a box which just encloses the objects. The objects
%   may be specified as single handles, column matrices of handles, position
%   vectors (1 x 4 matrix) or cell arrays containing any of the above.

if nargin == 0
  p = [];
  return;
end

left = nan;
right = nan;
bottom = nan;
top = nan;


for n = 1:nargin
  h = varargin{n};
  
  % Fix for >= R2015a. Cast objects to numeric values.
  if ishandle(h)
    h = double(h);
  end
  
  if iscell(h)
    % split up the cell with a recursive call
    p = boundingbox(h{:});
  
  elseif ~isa(h, 'double')
    varargin{:}
    error('Not a double or cell matrix');
    
  elseif length(h) == 1
    % assume it is a handle (don't bother with checking - it's an error
    % if it isn't
    p = localGetPosition(h);
  
  elseif size(h, 2) == 1
    % column vector of handles
    % p = get(h, 'Position');
    p = localGetPosition(h);
    % menu position is a scalar indicating order
    type = get(h, 'Type');
    % might need to fix here for uicontextmenu
    notMenu = ~strcmp(type, 'uimenu');
    p = cat(1,p{notMenu});
    
  elseif size(h) == [1 4]
    % position vector
    p = h;
    
  else 
    error(['Only handles, column matrices of handles, position vectors' ...
	  ' or cell arrays of these types are acceptable']);
    
  end
  left = min(left, min(p(:,1)));
  right = max(right, max(p(:,1) + p(:,3)));
  bottom = min(bottom, min(p(:,2)));
  top = max(top, max(p(:,2) + p(:,4)));
  
end

p = [left bottom (right-left) (top-bottom)];
return



% givena single handle get the Position data (or equivalent)
function p = localGetPosition(h)
if numel(h) ~= 1
  p = cell(size(h));
  for n = 1:numel(h)
    p{n} = localGetPosition(h(n));
  end
  return
end

g = get(h);
if isfield(g, 'Position')
  p = get(h, 'Position');
elseif isfield(g, 'XData')
  if isempty(g.XData) | ~isequal(size(g.XData), size(g.YData))
    p = [nan nan nan nan];
      else
	p = [min(g.XData) min(g.YData) max(g.XData) max(g.YData)];
	p(3:4) = p(3:4) - p(1:2);
  end
else
  error(sprintf('cannot handle objects of type %s', g.Type));
end

