function h = mypatch(Xin, Yin, Zin, Cin)
h = [];

xx = localExtendMatrix(Xin);
yy = localExtendMatrix(Yin);

method = 'linear'
switch method
 case 'own'
  zz = localExtendMatrix(Zin);
  cc = localExtendMatrix(Cin);
 
 case {'linear' 'cubic' 'nearest', 'v4', 'spline'}
  
  zz = griddata(Xin, Yin, Zin, xx, yy, method);
  cc = griddata(Xin, Yin, Cin, xx, yy, method);

 otherwise
  if exist(method)
    zz = feval(method, Xin, Yin, Zin, xx, yy);
    cc = feval(method, Xin, Yin, Cin, xx, yy);
  else
    error(sprintf('Cannot find method ''%s''', method));
  end
end

nans = any(isnan(zz(:)))
nans = any(isnan(Zin(:)))
if 1
  figure
  plot3(Xin, Yin, repmat(200, size(Xin)), 'ko', ...
       xx, yy, repmat(200, size(xx)),'b+')
  view(2);
  hold on
end
len = prod(size(Xin)) + prod(size(xx));
xdata = zeros(3, len);
ydata = zeros(3, len);
zdata = zeros(3, len);
cdata = zeros(3, len);

rows = size(Xin, 1);
cols = size(Xin, 2);
for c = 0:(cols-1)
  for r = 0:(rows-1)
    R = r + 1;
    C = c + 1;
    R2 = r + 2;
    C2 = c + 2;
    n = 4 * ((c * rows) + r);
    xdata(:, n+1) = [Xin(R,C); xx(R, C); xx(R, C2)];
    xdata(:, n+2) = [Xin(R,C); xx(R, C); xx(R2, C)];
    xdata(:, n+3) = [Xin(R,C); xx(R2, C); xx(R2, C2)];
    xdata(:, n+4) = [Xin(R,C); xx(R, C2); xx(R2, C2)];

    ydata(:, n+1) = [Yin(R,C); yy(R, C); yy(R, C2)];
    ydata(:, n+2) = [Yin(R,C); yy(R, C); yy(R2, C)];
    ydata(:, n+3) = [Yin(R,C); yy(R2, C); yy(R2, C2)];
    ydata(:, n+4) = [Yin(R,C); yy(R, C2); yy(R2, C2)];

    zdata(:, n+1) = [Zin(R,C); zz(R, C); zz(R, C2)];
    zdata(:, n+2) = [Zin(R,C); zz(R, C); zz(R2, C)];
    zdata(:, n+3) = [Zin(R,C); zz(R2, C); zz(R2, C2)];
    zdata(:, n+4) = [Zin(R,C); zz(R, C2); zz(R2, C2)];

    cdata(:, n+1) = [Cin(R,C); cc(R, C); cc(R, C2)];
    cdata(:, n+2) = [Cin(R,C); cc(R, C); cc(R2, C)];
    cdata(:, n+3) = [Cin(R,C); cc(R2, C); cc(R2, C2)];
    cdata(:, n+4) = [Cin(R,C); cc(R, C2); cc(R2, C2)];

  end
end

h = patch('XData', xdata, ...
	  'YData', ydata, ...
	  'ZData', zdata, ...
	  'CData', cdata);
shading flat
% points1 = prod(size(x));
% points2 = prod(size(xx));
% vertices = ([x(:), y(:), z(:); xx(:), yy(:), zz(:)]);
% % faces = [50 1 2; 50 8 9; 50 1 8; 50 2 9];
% faces = [1 50 51; 1 59 60; 1 50 59; 1 51 60];

% data = [z(:); zz(:)];

% % facevertexcdata = data(faces(:));
% facevertexcdata = repmat(nan, size(vertices));
% facevertexcdata = data;
% cdata = data;
% matrixinfo(vertices)

% figure
% h = patch('Parent', gca, ...
% 	  'EdgeColor', 'k', ...
% 	  'Faces', faces, ...
% 	  'FaceVertexCData', facevertexcdata, ...
% 	  'Vertices', vertices);

% -----------------
function ea = localExtendMatrix(a)
ea = zeros(size(a) + [1 1]);

% interpolate in the middle of existing data
for r = 1:(size(a, 1)-1)
  for c = 1:(size(a, 2)-1)
    ea(r+1, c+1) = mean(reshape(a(r+[0 1], c+[0 1]), [1 4]));
  end
end

% extrapolate beyond the border of existing data (but not corners just
% yet)

% ea(2:(end-1), 1) = mean([a(1:(end-1), 1), a(2:end, 1)],2); % left
ea(2:(end-1), 1) = -ea(2:(end-1),2) + ...
    2 * mean([a(1:(end-1), 1), a(2:end, 1)],2);

ea(2:(end-1), end) = -ea(2:(end-1), end-1) + ...
    2 * mean([a(1:(end-1), end), a(2:end, end)],2); % right

ea(1, 2:(end-1)) = -ea(2,2:(end-1)) + ...
    2 * mean([a(1, 1:(end-1)); a(1, 2:end, 1)], 1); % top

ea(end, 2:(end-1)) = -ea(end-1, 2:(end-1)) + ...
    2 * mean([a(end, 1:(end-1)); a(end, 2:end)], 1); % bottom

% extrapolate for the corners

ea(1,1) = sum([a(1,1) ea(2,2)] .* [2 -1]);
ea(1,end) = sum([a(1,end) ea(2,(end-1))] .* [2 -1]);
ea(end,1) = sum([a(end,1) ea((end-1),2)] .* [2 -1]);
ea(end,end) = sum([a(end,end) ea(end-1,end-1)] .* [2 -1]);
