function setuserdata(nb, data)
%SETUSERDATA Set an internal user data area in a NUMBERBOX.
%
%   SETUSERDATA(nb, d)
%   nb: NUMBERBOX object
%   d: user specified data (any type)
%
%   See also NUMBERBOX, numberbox/GETUSERDATA.

nb.UserData = data;
update(nb);


