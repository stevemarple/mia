function varargout = get(nb, varargin)
%GET  Get object properties for all uicontrol objects in NUMBERBOX.
%
%   GET(nb, ...)
%
%   numberbox/GET works like the builtin GET (see graphics/GET), but
%   instead of being passed a handle a NUMBERBOX object is passed
%   instead. GET is called on all uicontrol objects used in a NUMBERBOX.
%
%   See also graphics/GET.

if strcmp(class(nb), 'double')
  varargout = builtin('get', nb, varargin{:});
  return;
end

varargout{1} = get(nb.handle, varargin{:});

