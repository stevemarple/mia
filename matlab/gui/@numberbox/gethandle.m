function h = gethandle(nb)
%GETHANDLE Return the handle used by a NUMBERBOX object.
%
%   h = GETHANDLE(nb);
%   h: uicontrol handle
%
%   See also NUMBERBOX.

h = nb.handle
