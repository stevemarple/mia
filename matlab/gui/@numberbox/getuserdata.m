function data = getuserdata(nb)
%GETUSERDATA  Get data from internal user data area of a NUMBERBOX.
%
%   data = GETUSERDATA(nb);
%   data: any data previously stored with numberbox/SETUSERDATA.
%
%   See also NUMBERBOX, numberbox/SETUSERDATA.

data = nb.UserData;


