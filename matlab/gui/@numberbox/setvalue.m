function setvalue(nb, value)
%SETVALUE  Set the value of a NUMBERBOX, and update the display to match.
%
%   SETVALUE(nb, v)
%   nb: NUMBERBOX object
%   v: the value to be set
%
%   SETVALUE also updates the class object stored in the UserData area.
%
%   NB whatever value given is used - even a float in a box which can only
%   normally display integers. To clear the box set value to be an empty
%   matrix.
%
%   See also NUMBERBOX, numberbox/GETVALUE.

nb.value = value;
set(nb.handle, 'String', sprintf(nb.formatstr, double(nb.value)));

% put all the changes made to this class object back into the class
% object stored in the UserData area
update(nb);
