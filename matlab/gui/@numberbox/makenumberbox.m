function handle = makenumberbox(nb, parent, pos)
%MAKENUMBERBOX Function to hold all the calls to uicontrol
%
%   This function is used internally by the NUMBERBOX constructor.

handle = uicontrol(parent, 'Style', 'edit', 'Position', pos, ...
    'HorizontalAlignment', 'right');

if ~isnan(nb.value)
  setvalue(nb, nb.value); % lazy way to redraw
end
