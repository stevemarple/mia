function setprocessing(nb, str)
%SETPROCESSING  Set the processing function for a NUMBERBOX object.
%
%   SETPROCESSING(nb, f)
%   nb: NUMBERBOX object
%   f: function to be called after the user modifies the contents of the
%      NUMBERBOX (CHAR)
%
%   See also NUMBERBOX, numberbox/GETPROCESSING.

nb.processing = str;
update(nb); % update copy held in uicontrol UserData
return;

