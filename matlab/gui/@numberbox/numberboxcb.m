function numberboxcb(nb)
%NUMBERBOXCB  Callback for the NUMBERBOX class.
%
%   This function should not be called by user routines.

% nb.value = atof(get(nb.handle, 'String'));
nb.value = str2num(lower(get(nb.handle, 'String')));
if isempty(nb.value)
  nb.value = nan;
else
  nb.value = nb.value(1);
end
% run users processing
n = nb.value;  % for users to access the value
if ~isempty(nb.processing)
  if ischar(nb.processing)
    eval(nb.processing);
  else
    eval(nb.processing{1}, nb.processing{2});
  end
end

% set the value and update the class object in the userdata area.
setvalue(nb, n);

% nb.value = n;

% set(nb.handle, 'String', sprintf(nb.formatstr, nb.value));

% % put all the changes made to this class object back into the class
% % object stored in the UserData area
% update(nb);


