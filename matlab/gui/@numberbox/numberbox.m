function handle = numberbox(parent, pos, fstr, proc, varargin)
%NUMBERBOX  Create an edit box for entering numbers.
%
%   h = NUMBERBOX(parent, pos, fstr, proc);
%   h = NUMBERBOX(parent, pos, fstr, proc, value);
%
%   h: handle to first UICONTROL object
%   parent: handle of parent  figure
%   pos: position vector
%   fstr: format string to use when printing value into box.
%   proc: processing string
%   value: optional initial value.
%
%   If post-processing is desired, 'proc' should be a string to be
%   evaluated, and should contain a function which alters the value of 'n'
%   i.e. proc='n=round(n);' to use for an integer NUMBERBOX, or
%   'n=round(min(max(0,n),59));' to use as an entry routine suitable for the
%   minutes part of a TIME. If proc is a cell, it should contain two strings
%   for evaluation as EVAL(try, catch).
%
%   See also numberbox/SETUSERDATA, TIMEBOX, TIMESPANBOX, STETBOX,
%   UICONTROL, EVAL.

% member fields
nb.handle = [];
if ~isempty(proc) & ~ischar(proc) & ~iscell(proc)
  error('processing must be a char or cell array if not an empty matrix');
end
nb.processing = proc;
if nargin > 4
  nb.value = varargin{1}(1);
else
  nb.value = [];
end
nb.formatstr = fstr;
nb.UserData = [];
nb = class(nb, 'numberbox');

nb.handle = double(makenumberbox(nb, parent, pos));

% set callback to operate on key presses
if ~isempty(nb.value)
  set(nb.handle, 'Callback', 'numberboxcb(get(gcbo, ''UserData''));', ...
      'String', sprintf(nb.formatstr, double(nb.value)));
else
  set(nb.handle, 'Callback', 'numberboxcb(get(gcbo, ''UserData''));');
end


% store into the uicontrol
set(nb.handle, 'UserData', nb);

handle=nb.handle;



