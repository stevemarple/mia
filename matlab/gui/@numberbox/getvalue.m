function value = getvalue(nb)
%GETVALUE  Return the value from a NUMBERBOX.
%
%   r = GETVALUE(nb)
%   r: value entered within the NUMBERBOX
%   nb: NUMBERBOX object
%
%   See also NUMBERBOX, numberbox/SETVALUE.

value = nb.value;


