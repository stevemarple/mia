function varargout = heatermenu(action, varargin)

switch action
  % mfilename('init', menuParentH, ...)
 case 'init'
  
  % no initial heater event
  % default start/end times if no others can be found
  defaults.heater_data = []; 
  defaults.starttime = [];
  defaults.endtime = [];
  
  % options to decide whether or not to show certain menus
  defaults.load = 'on';
  defaults.details = 'on';
  defaults.onofftimes = 'off';
  defaults.s4plot = 'off';
  
  if nargin > 2
    [defaults unvi] = interceptprop({varargin{2:length(varargin)}}, ...
				    defaults); 
  end
  
  h = uimenu(varargin{1}, 'Label', 'Heating', ...
	     'Separator', 'on', ...
	     'Callback',  [mfilename '(''heatermenu'');'], ...
	     'Tag', 'heater');
  
  loadH = uimenu(h, 'Label', 'Load heater event', ...
		 'Visible', defaults.load, ...
		 'Tag', 'heater_load');
  hldUd = get(loadH, 'UserData');
  hldUd.heater_data = defaults.heater_data; % initial heater event
  set(loadH, 'UserData', hldUd);
  
  % load up UserData in case another functions has got to it first
  % (assume if so data is a struct) 
  ud = get(h, 'UserData'); 
  ud.starttime = defaults.starttime;
  ud.endtime = defaults.endtime;
  set(h, 'UserData', ud);

  uimenu(h, 'Label', 'Event details', ...
	 'Visible', defaults.details, ...
	 'Enable', 'off', ...
	 'Callback',  [mfilename '(''details'');'], ...
	 'Tag', 'heater_details');

  onOffH = uimenu(h, 'Label', 'On/off times...', ...
		  'Separator', 'on', ...
		  'Visible', defaults.onofftimes, ...
		  'Enable', 'off', ...
		  'Callback',  [mfilename '(''onofftimes'');'], ...
		  'Tag', 'heater_onoff');
  
  uimenu(h, 'Label', 'S4 plot', ...
	 'Visible', defaults.s4plot, ...
	 'Tag', 'heater_s4plot');
  
  % Interruptible=off added because autoload can take a few seconds to run
  uimenu(loadH, 'Label', 'Autoload', ...
	 'Callback', [mfilename '(''autoload'');'], ...
	 'Interruptible', 'off', ...
	 'Tag', 'heater_load_auto');

  uimenu(loadH, 'Label', 'Select...', ...
	 'Callback', [mfilename '(''manualload'');'], ...
	 'Tag', 'heater_load_manual');
  
  varargout{1} = h;

  
  % ----------------------------------------  
 case 'heatermenu'
  % findout if the figure contains any plots which are labelled with
  % timetick
  switch length(varargin)
   case 0
    fh = gcbf;
    cbo = gcbo;
   case 2
    fh = varargin{1};
    cbo = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  ah = findobj(fh, 'Type', 'axes');
  xlh = get(ah, 'XLabel');
  if iscell(xlh)
    xlh = cell2mat(xlh);
  end
  loadH = findobj(cbo, 'Tag', 'heater_load');
  ud = get(loadH, 'UserData');
  if isstruct(ud) 
    fn = fieldnames(ud);
    if ~isempty(find(strcmp(fn, 'heater_data'))) & ~isempty(ud.heater_data) 
      % found field ud,he and it is not empty
      heater_dataExists = 1;
    else
      heater_dataExists = 0;
    end
    % test to see if any XLabels contain time information
    xlhProp = get(xlh);
    for n = 1:length(xlhProp)
      if ~isempty(xlhProp(n).UserData) & iscell(xlhProp(n).UserData) ...
	    & isequal(size(xlhProp(n).UserData), [1 4]) ...
	    & isa(xlhProp(n).UserData{1}, 'timestamp') ...
	    & isa(xlhProp(n).UserData{2}, 'timestamp') ...
	    & isa(xlhProp(n).UserData{3}, 'timespan') 
	% found an axis whose XLabel contains time information
	if heater_dataExists
	  set(get(cbo, 'Children'), 'Enable', 'on');
	else
	  set(get(cbo, 'Children'), 'Enable', 'off');
	  set(loadH, 'Enable', 'on');
	end
	return;
      end
    end
  end

  %  not found
  cboUd = get(cbo, 'UserData');
  if isa(cboUd.starttime, 'timestamp') & isa(cboUd.endtime, 'timestamp')
    % default to this start/endtime, instead of using one off of a axes
    if heater_dataExists
      set(get(cbo, 'Children'), 'Enable', 'on');
    else
      set(get(cbo, 'Children'), 'Enable', 'off');
      set(loadH, 'Enable', 'on');
    end
  else
    % no time information anywhere, so no point in having the menu
    % options enabled
    set(get(cbo, 'Children'), 'Enable', 'off');
  end
  
  % ----------------------------------------  
 case 'autoload'
  switch length(varargin)
   case 0
    cbo = gcbo;
    fh = gcbf;
   case 2
    fh = varargin{1};
    cbo = varargin{2};
   otherwise 
    error('incorrect parameters');
  end
  ph = get(cbo, 'Parent'); % handle of parent, ie 'Load heater event'
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  loadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  ud = get(loadH, 'UserData'); 
  ud.heater_data = []; % erase any existing one (but don't save back yet)

  % ah = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  ah = findobj(fh, 'Type', 'axes');
  xlh = get(ah, 'XLabel');
  if iscell(xlh)
    xlh = cell2mat(xlh);
  end
  
  % ### DEBUG ### This portion of code uses the start/end times from
  % the first axes it finds. If a figure has multiple axes with
  % different start/end times then it may not do quite what it
  % should... 
  found = 0;
  xlhProp = get(xlh);
  for n = 1:length(xlhProp)
    if ~isempty(xlhProp(n).UserData) & iscell(xlhProp(n).UserData) ...
	  & isequal(size(xlhProp(n).UserData), [1 4]) ...
	  & isa(xlhProp(n).UserData{1}, 'timestamp') ...
	  & isa(xlhProp(n).UserData{2}, 'timestamp') ...
	  & isa(xlhProp(n).UserData{3}, 'timespan') 
      found = 1;
      ud.heater_data = heater_data('starttime', xlhProp(n).UserData{1}, ...
				   'endtime', xlhProp(n).UserData{2}, ...
				   'instrument', eiscat_heater);
      break;
    end
  end
  
  if ~found
    % check whether the heater menu was created with default start/end
    % times and use those if so.
    heatingH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater');
    heaterUd = get(heatingH, 'UserData');
    if ~isempty(heaterUd) & isa(heaterUd.starttime, 'timestamp') ...
	  & isa(heaterUd.endtime, 'timestamp')
      ud.heater_data = heater_data('starttime', heaterUd.starttime, ...
				   'endtime', heaterUd.endtime, ...
				   'instrument', eiscat_heater);
    end
  end
  
  if isempty(getontimes(ud.heater_data))
    % errordlg('No heater data for this time', 'Error', ...
    % 	     'modal');
    warndlg('No heater data for this time', 'Warning');
    set(fh, 'Pointer', pointer);
    return;
  elseif length(ud.heater_data) > 1
    % multiple matches, select one event
    ud.heater_data = heater_datadlg('init', ud.heater_data);
  end
  
  % enable all heating items
  set(get(get(ph, 'Parent'), 'Children'), 'Enable', 'on');
  
  % change names on this one
  set(ph, 'Label', 'New heater event', 'UserData', ud);
  set(fh, 'Pointer', pointer);
  % ----------------------------------------  
 case 'manualload'
  switch length(varargin)
  case 0
    fh = gcbf;
    cbo = gcbo;
   case 2
    fh = varargin{1};
    cbo = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  ph = get(cbo, 'Parent'); % handle of parent, ie 'Load heater event'
  loadH = findobj(fh, 'Type', 'uimenu', 'Tag', 'heater_load');
  ud = get(loadH, 'UserData'); 
  ud.heater_data = []; % erase any existing one (but don't save back yet)
  [file path] = uigetfile('*.mat', 'Select heater event');
  pathfile = fullfile(path, file);
  s = whos('-file', pathfile);
  if ~isempty(find(strcmp({s.name}, 'he')))
    % heater event file
    load(pathfile, 'he');
    
    % for matlab 5.1: ensure any version changes are fixed
    ud.heater_data = heater_data(he); 
    
    set(loadH, 'UserData', ud);
    
    % change names on this one
    set(ph, 'Label', 'New heater event', 'UserData', ud);

  else
    errordlg('Not a heater event file', 'Error', 'modal');	    
    return;
  end
  
  % ----------------------------------------  
 case 'onofftimes'
  switch length(varargin)
   case 0
    fh = gcbf;
    cbo = gcbo;
   case 2
    fh = varargin{1};
    cbo = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  ph = get(cbo, 'Parent'); % handle of parent, ie 'Heating'
  loadH = findobj(ph, 'Type', 'uimenu', 'Tag', 'heater_load');
  ud = get(loadH, 'UserData');
  
  ah = findobj(fh, 'Type', 'axes', 'Tag', 'plot');
  % onofftimes(ud.heater_data, ah, 'color', uisetcolor([1 0 0], 'Select ON colour'));
  onofftimesArgs = onofftimesdlg('init');
  % empty means cancel was pressed
  if ~isempty(onofftimesArgs)
    onofftimes(ud.heater_data, ah, onofftimesArgs{:});
  end
  
  % ----------------------------------------  
 case 'details'
   switch length(varargin)
   case 0
    fh = gcbf;
    cbo = gcbo;
   case 2
    fh = varargin{1};
    cbo = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  ph = get(cbo, 'Parent'); % handle of parent, ie 'Heating'
  loadH = findobj(ph, 'Type', 'uimenu', 'Tag', 'heater_load');
  ud = get(loadH, 'UserData');

  disp(sprintf('\rHeater data details:\n\n%s\n', char((ud.heater_data))));

  
  % ----------------------------------------  
 otherwise
  error('unknown action');
  
end


