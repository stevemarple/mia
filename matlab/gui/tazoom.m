function varargout = tazoom(varargin)
%TAZOOM Time-axis zoom, for zooming multiple axes which have common times
%
% TAZOOM(h, action)
% h: AXES and/or FIGURE handles
% action: the zoom action to perform [on | xon | restore | out]
%
% TAZOOM is intended to replace ZOOM for linked zooming on axes which
% show data over a common time period. It is assumed that the X limits on
% the axes are the same.
%
% TAZOOM must first be activated by calling with action set to 'on' or
% 'xon'. The handles must be a matrix of valid handles to either AXES or
% FIGUREs containing AXES. When FIGURE handles are passed only child AXES
% whose HandleVisibility property is set to 'on' are included.
%
% The 'on' action enables zooming in both X and Y dimensions, 'xon'
% limits zooming to only the X axis. 'out' returns the axes limits to
% their original values but does not deactivate TAZOOM. 'restore'
% restores all AXES settings to their original values (including
% callbacks) and deactivates TAZOOM.
%
% TAZOOM cooperates with TIMETICK2 to relabel the axis with appropriate
% time labels.
%
% The default behaviour can be modified with the following name/value
% pairs:
%
%   'crop', LOGICAL
%   Flag to indicate if TAZOOM should crop the selected zoom area with
%   the original X/Y limits. Default is 1, which prevents zooming outside
%   of the original viewing area.
%
%   'ymargin', scalar DOUBLE
%   Amount of blank space shown above and below the extreme limits within
%   the selected viewing area, expressed as a ratio. Ignored when
%   activated with 'xon'.
%
%   'honouryselection', LOGICAL
%   Flag to indicate if TAZOOM should zoom the active axis to the Y limits
%   selected with the rubberband box (see RBBOX), or based upon the data
%   present (as is performed with the remaining axes). Default is 1.
%
%
% See also ZOOM, TIMETICK2.

if nargin == 0
  error('incorrect parameters');
elseif isnumeric(varargin{1})
  eval('get(0,''DefaultAxesHitTest'');hitTest={''HitTest'',''on''};', ...
       'hitTest={};');
  
  h1 = varargin{1}(:); % h is always a column vector
  h = [];
  for n = 1:length(h1)
    t = get(h1(n), 'Type');
    switch t
     case 'axes'
      h(end+1) = h1(n);
     case 'figure'
      % h = [h(:); findobj(get(h1(n), 'Children'), 'Type', 'axes')];

      % find all axes for which zoom is probably sensible
      h = [h(:);
	   findall(h1(n), ...
		   hitTest{:}, ...
		   'Type', 'axes', ...
		   'Visible', 'on', ...
		   'HandleVisibility', 'on'); 
	   findall(h1(n), ...
		   hitTest{:}, ...
		   'Type', 'axes', ...
		   'Visible', 'on', ...
		   'HandleVisibility', 'callback'); ];
     otherwise
      error('handles must be figure or axes');
    end
  end
  
  action = lower(varargin{2});
  varargin = {varargin{3:end}};
else
  h = gca;
  action = lower(varargin{1});
  varargin = {varargin{2:end}};
end



switch action
  % ---------------------------------------------------------------
 case {'on' 'xon'}
  if isempty(h)
    error('need axis handles');
  end
  % copy properties
  ud = localGetUserData(h);
  if isempty(ud)
    % initialise
    ud.h = h;
    ud.p = get(h);
    % do not allow other callbacks to interrupt any running of this
    % function as it may mean that the user data changes unexepectedly
    set(h, 'Interruptible', 'off'); % does this immediately!
    if matlabversioncmp('>=', '5.2')
      set(h, 'UIContextMenu', []);
    end
    
    ud.on = action; % remember whether to zoom just x
    ud.xlimhistory = zeros(0, 2);
    ud.ylimhistory = zeros(length(h), 2, 0);
    ud.defaults.crop = 1;
    ud.defaults.ymargin = 0.1;
    ud.defaults.honouryselection = 1;
    
    [ud.defaults unvi] = interceptprop(varargin, ud.defaults);
    warnignoredparameters(varargin{unvi});
  
    ud.defaults.crop = logical(ud.defaults.crop);
  else
    % initialisation has been done, but return some settings to the same
    % values as used for initialisation since action=off can alter these
    set(h, 'Interruptible', 'off');
    if matlabversioncmp('>=', '5.2')
      set(h, 'UIContextMenu', []);
    end
  end
  ud.status = action;
  localUpdate(ud);
  set(h, 'ButtonDownFcn', sprintf('%s(''buttondown'');', mfilename));

  % ---------------------------------------------------------------
 case 'out'
  ud = localGetUserData(h);
  if isempty(ud)
    error('cannot find original axes settings');
  end
  for n = 1:length(ud.h)
    set(ud.h(n), ...
	'XLim', ud.p(n).XLim, ...
	'YLim', ud.p(n).YLim, ...
    	'ZLim', ud.p(n).ZLim, ...
	'CLim', ud.p(n).CLim);
    
  end
  ud.xlimhistory = zeros(0, 2);
  ud.ylimhistory = zeros(length(h), 2, 0);
  localUpdateTimeLabel(ud.h);
  localUpdate(ud);
  
  % ---------------------------------------------------------------
 case 'restore'
  % put the axes properties back to how they were
  ud = localGetUserData(h);
  if isempty(ud)
    disp('nothing to restore')
    return; % nothing to put back
  end
  
  fn = fieldnames(ud.p(1));
  for n = 1:length(fn)
    % In future may not be sure what values are read-only. Try each field
    % one by one, instead an eval in case it goes wrong.
    for m = 1:length(ud.h)
      eval('set(ud.h(m), fn{n}, getfield(ud.p(m), fn{n}));', ...
	   ';');
      % 'disp([fn{n} '': '' lasterr]);');
    end
  end
  localCleanup(ud);

  % ---------------------------------------------------------------
 case 'off'
  % restore ButtonDownFcn and UIContextMenu to their original values
  ud = localGetUserData(h);
  if isempty(ud)
    return
  end

  for n = 1:length(ud.h)
    if matlabversioncmp('>=', '5.2')
      set(ud.h(n), ...
	  'ButtonDownFcn', ud.p(n).ButtonDownFcn, ...
	  'UIContextMenu', ud.p(n).UIContextMenu);
    else
      set(ud.h(n), 'ButtonDownFcn', ud.p(n).ButtonDownFcn);
    end
  end
  ud.status = action;
  localUpdate(ud);
  
  % ---------------------------------------------------------------
 case 'buttondown'
  ud = localGetUserData(h);
  if isempty(ud)
    return; % odd, no data for the current axis
  end

  hlen = length(ud.h);
  
  cbo = gcbo; % axes object
  cbf = get(cbo, 'Parent');

  selType = get(cbf, 'SelectionType');
  switch selType
   case 'alt'
    % zoom out to previous setting
    if ~isempty(ud.xlimhistory)
      set(ud.h, 'XLim', ud.xlimhistory(end, :));
      localUpdateTimeLabel(ud.h);
      
      for n = 1:hlen
	set(ud.h(n), 'YLim', ud.ylimhistory(n, :, end));
      end
      ud.xlimhistory(end, :) = [];
      ud.ylimhistory(:, :, end) = [];
      localUpdate(ud);
    end
    return;
    
    % case 'extend'
   case 'open'
    % zoom all the way out
    if ~isempty(ud.xlimhistory)
      % set(ud.h, {'XLim'}, {ud.p.XLim}', {'YLim'}, {ud.p.YLim}');
      set(ud.h, {'XLim'}, {ud.p.XLim}');
      localUpdateTimeLabel(ud.h);
      
      for n = 1:hlen
	set(ud.h(n), 'YLim', ud.p(n).YLim);
      end
      ud.xlimhistory = zeros(0, 2);
      ud.ylimhistory = zeros(hlen, 2, 0);
      localUpdate(ud);
    end
    return;
  end
  
  rect = rbbox; % uses the figure units
  if isequal(rect(3:4), [0 0])
    return; % just a point, no mouse drag
  end
  
  % for the callback axes work out what these points mean in terms of data
  % values
  ap = get(cbo); % get the latest setting
  figUnits = get(cbf, 'Units');
  set(cbo, 'Units', figUnits);
  pos = get(cbo, 'Position');
  set(cbo, 'Units', ap.Units);
  
  % turn rect position vector into a pair of coords
  x = [rect(1) sum(rect([1 3]))];
  y = [rect(2) sum(rect([2 4]))];
  xd = interp1e([pos(1) sum(pos([1 3]))], ap.XLim, x')';
  yd = interp1e([pos(2) sum(pos([2 4]))], ap.YLim, y')';

  if length(xd) ~= 2 & numel(xd) ~= 2 ~all(isfinite(xd(:)))
    error('bad length');
  end

  if ud.defaults.crop
    xd = [max(xd(1), ap.XLim(1)) min(xd(2), ap.XLim(2))];
  end
  
  
  % remember what the limits were
  hist = size(ud.xlimhistory, 1) + 1;
  ud.xlimhistory(hist, :) = ap.XLim;
  for n = 1:hlen
    ud.ylimhistory(n, :, hist) = get(ud.h(n), 'YLim');
  end
  
  set(ud.h, 'XLim', xd);
  localUpdateTimeLabel(ud.h);
  
  if ~strcmp(ud.on, 'xon')
    % compute Y limits
    yd2 = zeros(hlen, 2);
    for n = 1:hlen
      currentYLim = get(ud.h(n), 'YLim');
      if cbo == ud.h(n) & ud.defaults.honouryselection
	% selected axis, use Y limits indicated
	yd2(n,:) = yd;
      else
	ylim1 = [];
	ylim2 = [];
	ch = get(ud.h(n), 'Children');
	for m = 1:length(ch)
	  cp = get(ch(m));
	  switch cp.Type
	   case 'line'
	    % work out if any line data exists between the limits
	    idx = find(cp.XData >= xd(1) & cp.XData <= xd(2));
	    if ~isempty(idx)
	      % line fragment between limits, take the extreme Y values
	      ylim1(end+1) = min(cp.YData(idx));
	      ylim2(end+1) = max(cp.YData(idx));
	      
	      % interpolate at the ends to find the Y values at the
              % limits
	      if idx(1) > 1
		ylim1(end+1) = interp1(cp.XData([-1 0] + idx(1)), ...
				       cp.YData([-1 0] + idx(1)), ...
				       xd(1));
	      end
	      if idx(end) < length(cp.XData)
		ylim2(end+1) = interp1(cp.XData([0 1] + idx(end)), ...
				       cp.YData([0 1] + idx(end)), ...
				       xd(1));
	      end
	    end
	    
	   otherwise
	    ; % ignore any other objects
	  end
	  
	end
	if isempty(ylim1)
	  yd2(n,:) = currentYLim;
	else
	  yd2(n,:) = [min(ylim1) max(ylim2)];
	  % allow for blank space at 
	  yd2(n,:) = yd2(n,:) + ...
	      diff(yd2(n,:)) .* ud.defaults.ymargin .* [-1 +1];
	end
      end
      
      if ud.defaults.crop
	yd2(n, :) = [max(yd2(n,1), ud.p(n).YLim(1)) ...
		     min(yd2(n,2), ud.p(n).YLim(2))];
      end
      
      if all(isfinite(yd2(n,:))) & yd2(n,2) > yd2(n,1)
	set(ud.h(n), 'YLim', yd2(n,:));
      end
    end
  end
  
  localUpdate(ud);
 
  % ---------------------------------------------------------------
 case 'status'
  % return the zoom status
  ud = localGetUserData(h);
  if isempty(ud)
    varargout{1} = '';
  else
    varargout{1} = ud.status;
  end

  % ---------------------------------------------------------------
 case 'reposition'
  % move along the X axes to be centred on some other point
  ud = localGetUserData(h);
  
  currentXLim = get(h(1), 'XLim');
  c = mean(currentXLim);
  
  newXLim = currentXLim + (varargin{1} - c);
  if ud.defaults.crop
    if newXLim(1) < ud.p(1).XLim(1)
      newXLim = newXLim + (ud.p(1).XLim(1) - newXLim(1));
    end
    if newXLim(2) > ud.p(1).XLim(2)
      newXLim = newXLim - (newXLim(2) - ud.p(1).XLim(2));
    end
  end
  
  localSetXLim(ud, newXLim);
  localUpdate(ud);

  
  % ---------------------------------------------------------------
 otherwise
  error('unknown action');
end


% ---------------------------------------------------------------
function localUpdate(ud);
% choose a handle
for h = ud.h(:)'
  initialUserData = get(h, 'UserData');
  ud2 = setfield(initialUserData, 'tazoom', ud);
  set(h, 'UserData', ud2);
end

% ---------------------------------------------------------------
function localCleanup(ud);
% choose a handle
for h = ud.h(:)'
  ud2 = get(h, 'UserData');
  if isfield(ud2, 'tazoom')
    ud2 = rmfield(ud2, 'tazoom');
  end
  set(h, 'UserData', ud2);
end

% ---------------------------------------------------------------
function r = localGetUserData(h);
% choose a handle
h = min(h);
s = get(h, 'UserData');
if isfield(s, 'tazoom')
  r = getfield(s, 'tazoom');
else
  r = [];
end

% ---------------------------------------------------------------
% Find all axes with ticktime2 labels
function localUpdateTimeLabel(h)
timetick2(h, 'updateonly', true);

% ---------------------------------------------------------------
function localSetXLim(ud, xd)
set(ud.h, 'XLim', xd);
localUpdateTimeLabel(ud.h);
hlen = length(ud.h);
cbo = gcbo;

if ~strcmp(ud.on, 'xon')
  % compute Y limits
  yd2 = zeros(hlen, 2);
  for n = 1:hlen
    currentYLim = get(ud.h(n), 'YLim');
    if isequal(cbo, ud.h(n)) & ud.defaults.honouryselection
      % selected axis, use Y limits indicated
      yd2(n,:) = yd;
    else
      ylim1 = [];
      ylim2 = [];
      ch = get(ud.h(n), 'Children');
      for m = 1:length(ch)
	cp = get(ch(m));
	switch cp.Type
	 case 'line'
	  % work out if any line data exists between the limits
	  idx = find(cp.XData >= xd(1) & cp.XData <= xd(2));
	  if ~isempty(idx)
	    % line fragment between limits, take the extreme Y values
	    ylim1(end+1) = min(cp.YData(idx));
	    ylim2(end+1) = max(cp.YData(idx));
	    
	    % interpolate at the ends to find the Y values at the
	    % limits
	    if idx(1) > 1
	      ylim1(end+1) = interp1(cp.XData([-1 0] + idx(1)), ...
				     cp.YData([-1 0] + idx(1)), ...
				     xd(1));
	    end
	    if idx(end) < length(cp.XData)
	      ylim2(end+1) = interp1(cp.XData([0 1] + idx(end)), ...
				     cp.YData([0 1] + idx(end)), ...
				     xd(1));
	    end
	  end
	  
	 otherwise
	  ; % ignore any other objects
	end
	
      end
      if isempty(ylim1)
	yd2(n,:) = currentYLim;
      else
	yd2(n,:) = [min(ylim1) max(ylim2)];
	% allow for blank space at 
	yd2(n,:) = yd2(n,:) + ...
	    diff(yd2(n,:)) .* ud.defaults.ymargin .* [-1 +1];
      end
    end
    
    if ud.defaults.crop
      yd2(n, :) = [max(yd2(n,1), ud.p(n).YLim(1)) ...
		   min(yd2(n,2), ud.p(n).YLim(2))];
    end
    
    set(ud.h(n), 'YLim', yd2(n,:));
  end
end
