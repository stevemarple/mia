function varargout = pnm2png(pnmfile, pngfile)
%PNG2GIF Convert a PNG format image to GIF format.
%
%   PNG2GIF(pngfile, giffile);
%   pngfile: filename of PNG image
%   giffile: new filename for GIF image
%

if isunix
  [status message] = unix(['pnmtopng ' pnmfile ' > ' pngfile]);
  
else
  error(['Do not know how to convert PNMs to PNG. Please add for ' ...
	 computer ' to ' mfilename]);
end

if nargout >= 1
  varargout{1} = status;
end

if nargout >= 2
  varargout{2} = message;
end

