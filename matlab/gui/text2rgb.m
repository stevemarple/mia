function varargout = text2rgb(varargin)
%TEXT2RGB Hack to convert textual color names to RGB.
%
%   rgb = TEXT2RGB(str)
%   {rgb1 rgb2 ... rgbn = TEXT2RGB(str1, str2, strn)
%   rgb: RGB color matrix, [1 x 3] where n is the number of parameters.
%   str: textual representation
%
%   Convert a textual color name (e.g. 'w') to its RGB equivalent
%   (e.g. [1 1 1]). This function is needed because some functions
%   require an RGB value and will not accept a textual equivalent
%   (e.g. PATCH). Multiple strings may be supplied,
%
%   See also RGB2HSV, HSV2RGB, PATCH.


if nargin ~= 1 & nargout ~= nargin
  % allow single input to not have an output (useful for debugging)
  error('number of inputs must equal number of outputs');
end

varargout = cell(1, nargout);
fh = figure('Visible', 'off');
eval('varargout = localText2Rgb(fh, varargin{:});', ...
     'delete(fh);error(lasterr)');
delete(fh);
return


function r = localText2Rgb(fh, varargin)
r = cell(size(varargin));
for n = 1:length(varargin)
  set(fh, 'Color', varargin{n});
  r{n} = get(fh, 'Color');
end
