function [cdata, hotspot] = skullcursor
%SKULLCURSOR  Custom "skull and cross-bones" cursor
%
%   [cdata hotspot] = SKULLCURSOR
%
%   See also FIGURE.

% cdata = [
%   2.0 1.0 2.0 nan nan 2.0 1.0 1.0 1.0 1.0 2.0 nan nan nan nan nan;
%   1.0 1.0 1.0 nan 2.0 1.0 1.0 1.0 1.0 1.0 1.0 2.0 nan nan nan nan;
%   2.0 1.0 2.0 2.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 2.0 nan nan nan;
%   nan nan 2.0 1.0 1.0 2.0 2.0 1.0 1.0 2.0 2.0 1.0 1.0 2.0 nan nan;
%   nan nan 2.0 1.0 1.0 2.0 2.0 1.0 1.0 2.0 2.0 1.0 1.0 2.0 nan nan;
%   nan nan 2.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 2.0 nan nan;
%   nan nan nan 2.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 2.0 nan nan nan;
%   2.0 nan nan nan 2.0 1.0 1.0 1.0 1.0 1.0 1.0 2.0 nan nan 1.0 2.0;
%   1.0 2.0 nan nan nan 2.0 1.0 1.0 1.0 1.0 2.0 nan nan nan 2.0 1.0;
%   1.0 2.0 2.0 nan nan 2.0 1.0 1.0 1.0 1.0 2.0 nan nan 2.0 1.0 nan;
%   nan 1.0 1.0 2.0 2.0 nan 2.0 1.0 1.0 2.0 nan nan 2.0 1.0 1.0 nan;
%   nan nan 1.0 1.0 1.0 1.0 nan nan nan nan 2.0 1.0 1.0 nan 2.0 2.0;
%   nan nan nan nan nan 1.0 1.0 2.0 1.0 1.0 1.0 1.0 nan nan nan nan;
%   nan nan nan 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan nan nan;
%   1.0 1.0 1.0 1.0 2.0 2.0 2.0 2.0 2.0 2.0 1.0 1.0 1.0 nan nan 1.0;
%   1.0 2.0 2.0 2.0 2.0 nan nan nan nan 2.0 2.0 2.0 2.0 1.0 nan nan;];
hotspot = [2 2];

% This was grabbed from xfig, and so will come under the GNU General Public
% License
cdata = [
  2.0 1.0 2.0 nan nan nan 1.0 1.0 1.0 1.0 nan nan nan nan nan nan;
  1.0 1.0 1.0 nan nan 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan nan nan;
  2.0 1.0 2.0 nan 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan nan;
  nan nan nan 1.0 1.0 2.0 2.0 1.0 1.0 2.0 2.0 1.0 1.0 nan nan nan;
  nan nan nan 1.0 1.0 2.0 2.0 1.0 1.0 2.0 2.0 1.0 1.0 nan nan nan;
  nan nan nan nan 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan nan;
  nan nan nan nan nan 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan nan nan;
  nan nan nan nan nan nan 1.0 1.0 1.0 1.0 nan nan nan nan nan nan;
  nan 1.0 nan nan nan nan 1.0 1.0 1.0 1.0 nan nan nan nan 1.0 nan;
  nan 1.0 nan nan nan nan 1.0 1.0 1.0 1.0 nan nan nan nan 1.0 1.0;
  nan nan 1.0 nan nan nan nan 1.0 1.0 nan nan nan nan 1.0 nan nan;
  nan nan nan 1.0 1.0 1.0 nan nan nan nan 1.0 1.0 1.0 nan nan nan;
  nan nan nan nan nan nan 1.0 1.0 1.0 1.0 nan nan nan nan nan nan;
  nan nan nan nan 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 nan nan nan 1.0;
  nan 1.0 1.0 1.0 1.0 nan nan nan nan nan nan 1.0 1.0 1.0 1.0 1.0;
  nan 1.0 nan nan nan nan nan nan nan nan nan nan nan nan 1.0 nan];

