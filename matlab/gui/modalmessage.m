function modalmessage(message, title)
%MODALMESSAGE Display message in modal dialog box, else to the command window.
%
%   MODALMESSAGE(message, windowtitle)
%   message: the message (CHAR)
%   title: window title of the modal dialog box
%
% MODALMESSAGE displays a message in a modal dialog box if FIGUREs can be
% displayed, other it is written to the command window.  title is used
% only for the FIGURE window title. 

% Can figures be shown?
if canusefigures;
  waitfor(msgbox(message, title, 'help', 'modal'));
else
  disp(message);
end
