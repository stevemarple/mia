function h = surfaceimage(x, y, z, c, varargin)
%SURFACEIMAGE  An improved SURFACE and IMAGE function.
%
%   h = SURFACEIMAGE(x, y, z, c, ...);
%   h: PATCH handle
%   x: X data
%   y: Y data
%   z: Z data
%   c: C data
%
% SURFACEIMAGE is modeled on the built-in SURFACE and IMAGE functions and
% can be used in a similar way. It is implemented using PATCH. It can be
% considered as an improvement to SURFACE as switching between flat and
% interp(olated) SHADING does not cause the data to 'jump' North-East or
% South-West. It can also extend the data to form pixels around the data
% points, similar to IMAGE. However, unlike IMAGE there is no requirement
% for the inter-pixel spacing to remain constant, hence it can also be
% considered as am improvement to IMAGE.
%
% The following parameter name/value pairs are recognised:
%
%   'parent', AXES handle
%   The AXES in which the PATCH should be placed. If missing the current
%   axes (see GCA) is used.
%
%   'extend', LOGICAL (default = 1)
%   Extend the PATCH outside of the natural edge to form pixels around
%   the data points. The default is to extend the PATCH/
%
%   'extrapolate', LOGICAL (default = 0)
%   Use extrapolation when calculating the Z and C values for the points
%   outside of the natural edge. The default is to not extrapolate,
%   instead using the value of the nearest data point (or the mean of the
%   nearest two data points).
%
% Any unrecognised name/value pairs are passed to the PATCH function.
%
% NOTES
%
% The flexibility of this function is entirely due to one special feature of
% PATCH; when FaceColor is 'flat' each face color is determined by the first
% vertex. By splitting each pixel into a 8 triangles where each triangle's
% first vertex is located on pixel center it is possible to create a
% graphics object which can be correctly displayed with both flat and
% interp(olated) SHADING. 
%
% In all cases X and Y data must be monotonic. Rectilinear grids
% distorted by pincushion or barrel distortion satisfy this criteria.
%
% See also IMAGE, PATCH, SURFACE, SHADING.

defaults.extend = 1;
defaults.extrapolate = 0;
defaults.parent = [];
[defaults unvi] = interceptprop(varargin, defaults);

% check input parameters
d = {x y z c};
for n = 1:length(d);
  if ndims(d{n}) ~= 2
    error('matrices must have only 2 dimensions');
  elseif any(size(d{n}) < 2)
    matrixinfo(d{n})
    error('inputs must be matrices, not vectors or scalars');
  end
end
clear d;

if any(isnan(x(:)))
  error('X input must not contain NaNs');
elseif any(isnan(y(:)))
  error('Y input must not contain NaNs');
end

% local_check_monotonic(x, 'X');
% local_check_monotonic(y, 'Y');

if isempty(defaults.parent)
  % cannot use as default since this could unnecessarily create an axis
  defaults.parent = gca; 
end

% Always extend the grid for the X and Y values. Let the Z and C values
% be user-configurable
x2 = local_make_grid(x, 1);
y2 = local_make_grid(y, 1);
z2 = local_make_grid(z, defaults.extrapolate);
c2 = local_make_grid(c, defaults.extrapolate);



% Having made our special gridcreate the vertices array
vertices = [x2(:), y2(:), z2(:)];

% For a given element in X, in which row in vertices will be positioned?
idx = reshape(1:numel(x2), size(x2));

% Every original data point will be surrounded by 8 triangles
faces = zeros(numel(x) * 8, 3);

% Work through the original data points in the new x2, y2 matrices and
% connect up the triangles
facenum = 1;
for rn = 2:2:size(x2, 1)
  for cn = 2:2:size(x2, 2)
    od = idx(rn, cn); % index of original data point
    % find vertex numbers around original point
    vn = reshape(idx((rn-1):(rn+1), (cn-1):(cn+1)), 1, 9);
    % vn1    vn4    vn7
    % vn2   vn5/od  vn8
    % vn3    vn6    vn9

    % connect them up. Start every face from the original data point,
    % ensuring that the original value will be used when
    % shading=flat. Work clockwise from 12 o'clock.
    faces(facenum, :)   = [od vn(4) vn(7)];
    faces(facenum+1, :) = [od vn(7) vn(8)];
    faces(facenum+2, :) = [od vn(8) vn(9)];
    faces(facenum+3, :) = [od vn(9) vn(6)];
    faces(facenum+4, :) = [od vn(6) vn(3)];
    faces(facenum+5, :) = [od vn(3) vn(2)];
    faces(facenum+6, :) = [od vn(2) vn(1)];
    faces(facenum+7, :) = [od vn(1) vn(4)];
    
    facenum = facenum + 8;
  end
end

if ~logical(defaults.extend)
  % remove any faces which extend outside of the original data, ie find
  % which rows of faces contain references to elements on the outside of
  % idx
  idx_del = unique([idx(1,:) idx(end,:) idx(:,1)' idx(:,end)']);

  % no need to check column 1, it only contains references to the
  % original data points
  faces(ismember(faces(:,2),idx_del),:) = [];
  faces(ismember(faces(:,3),idx_del),:) = [];
end

h = patch(varargin{unvi}, ...
          'Parent', defaults.parent, ...
	  'Vertices', vertices, ...
	  'EdgeColor', 'none', ...
	  'FaceVertexCData', c2(:), ...
	  'FaceColor', 'flat', ...
	  'EdgeColor', 'none', ...
	  'CDataMapping', 'scaled', ...
	  'Faces', faces);



function x2 = local_make_grid(x, extrapolate)
% Make a grid looking like:

%
% .   .   .   .   .   .  .  .  .
%
% .   o   +   o   +   o  +  o  .
%
% .   +   *   +   *   +  *  +  .
%
% .   o   +   o   +   o  +  o  .
%
% .   .   .   .   .   .  .  .  .
%
%
% Where o = original data point, 
%       + = interpolated point between two original points
%       * = interpolated point between four original points
%       . = extrapolated
%

% Extrapolate or copy

sz = size(x);
sz2 = 2 * sz + 1;
x2 = zeros(sz2);


% insert original data points
x2(2:2:end,2:2:end) = x;

% interpolate along rows
x2(2:2:end, 3:2:(end-2)) = x(:, 1:(end-1)) + 0.5*diff(x, 1, 2);

% interpolate along columns. The new points between 4 original data
% points become the mean of all 4 values.
x2(3:2:(end-2), :) = x2(2:2:(end-3), :) + 0.5*diff(x2(2:2:end, :), 1, 1);


if logical(extrapolate)
  % extrapolate for the top row
  x2(1,:) = x2(2,:) - diff(x2(2:3,:), 1, 1);
  
  % extrapolate for the bottom row
  x2(end,:) = x2(end-1,:) + diff(x2((end-2):(end-1),:), 1, 1);
  
  % extrapolate for the left column
  x2(:,1) = x2(:,2) - diff(x2(:,2:3), 1, 2);
  
  % extrapolate for the right column
  x2(:,end) = x2(:,end-1) + diff(x2(:, (end-2):(end-1)), 1, 2);
  
  % extrapolate top left corner
  x2(1,1) = x2(2,2) - diff([x2(2,2) x2(3,3)]);
  
  % extrapolate top right corner
  x2(1,end) = x2(2,end-1) + diff([x2(3,end-2) x2(2,end-1)]);
  
  % extrapolate bottom left corner
  x2(end,1) = x2(end-1,2) - diff([x2(end-1,2) x2(end-2,3)]);
  
  % extrapolate bottom right corner
  x2(end,end) = x2(end-1,end-1) - diff([x2(end-1,end-1) x2(end-2,end-2)]);
else
  % copy into edges
  x2(1,:) = x2(2, :);        % top row
  x2(end,:) = x2(end-1, :);  % bottom row
  x2(:, 1) = x2(:, 2);       % left column
  x2(:, end) = x2(:, end-1); % right column
  
  % copy into corners
  x2(1, 1) = x2(1, 2);           % top left
  x2(1, end) = x2(1, end-1);     % top right
  x2(end, 1) = x2(end, 2);       % bottom left
  x2(end, end) = x2(end, end-1); % bottom right
end

function local_check_monotonic(a, name)
% Check all rows are monotonic
for n = 1:size(a, 1);
  sgn = sign(diff(a(n, :)));
  if ~all(sgn(1) == sgn)
    disp(sprintf('row %d in %s is not monotonic', n, name));
  end
end

% Check all columns are monotonic
for n = 1:size(a, 2)
  sgn = sign(diff(a(:, n)));
  if ~all(sgn(1) == sgn)
    disp(sprintf('column %d in %s is not monotonic', n, name));
  end
end


