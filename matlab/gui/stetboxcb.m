function stetboxcb(seb, box)
%STETBOXCB  Callback for STETBOX class.
%
%   This function is used internally by STETBOX.
%
%   See also STETBOX.

% box code:
% 1 = starttime
% 2 = endtime
% 4 = duration

lastbox = getprevious(seb);
t = [];
% calc is the starttime/endtime/duration to be calculated.
if box == lastbox(1)
  calc = box + lastbox(2);
else
  calc = box + lastbox(1);
end
switch calc
  case 1
    ;
    
  case 2
    ;
    
  case 3
    % starttime and endtime set, calculate duration
    t = gettime(seb);
    if ~isempty(t{1}) & ~isempty(t{2})
      t{3} = t{2} - t{1};
    end
    
  case 4
    ;
    
  case 5
    % starttime and duration set, calculate endtime
    t = gettime(seb);
    if ~isempty(t{1}) & ~isempty(t{2})
      t{2} = t{1} + t{3};
    end
  case 6
    % endtime and duration set, calculate starttime
    t = gettime(seb);
    if ~isempty(t{1}) & ~isempty(t{2})
      t{1} = t{2} - t{3};
    end
end

if ~isempty(t{1}) & ~isempty(t{2})
  % ### DEBUG ###
  % this might not give the best behavior
  if t{1} > t{2}
    if box == 1
      t{2} = t{1};
      t{3} = timespan; % zero
    elseif box == 2
      t{1} = t{2};
      t{3} = timespan; % zero	  
    end
  end
  % ### END OF TEST ###
  settime(seb, t);
end

% if the box was different to before, remember it!
if box ~= lastbox(1)
  setprevious(seb, [box lastbox(1)])
end



