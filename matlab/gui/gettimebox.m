function tb = gettimebox(handle)
%GETTIMEBOX  Return a TIMEBOX object from a UICONTROL handle.
%
%   tb = GETTIMEBOX(h)
%   tb: TIMEBOX object
%   h: handle associated with a TIMEBOX object
%
%   See also TIMEBOX.

switch class(handle)
 case 'timebox'
  % already is a tb!
  % warning('this already is a timebox');
  tb = handle;
  return;

 case 'double'
  if ishandle(handle)
    tb = get(handle, 'UserData');
    if strcmp(class(tb), 'timebox')
      return;
    else
      tb = gettimebox(tb);
      return;
    end
  else
    error('Not a valid handle');
  end	
  
 otherwise
  % some other class which is storing our timebox
  tb = gettimebox(getuserdata(handle));
  return;
end



