function varargout = mia_keogram_dlg(action, varargin)

switch action
  % --------------------------------------------------------------
  case 'init'
    % mfilename('init', mia, ...)
    mia = varargin{1};
    if ~isa(mia, 'mia_image_base')
        error('object must be derived from mia_image_base');
    end

    fh = figure('Name', [gettype(mia, 'c') ' keogram'], ...
        'NumberTitle', 'off', ...
	'Visible', 'off', ...
	'HandleVisibility', 'callback', ...
	'IntegerHandle', 'off', ...
	'MenuBar', 'None');

    % Add standard menus
    helpData = {'Creating keograms', ...
	  'miahelp(''/keograms/creating.html'')', 'help_images'};
    miamenu(fh, 'help', helpData);

    ud = get(fh, 'UserData'); % load form figure - some other function
    % might already be using it
    
    % use a struct to keep all our default parameters together. Also
    % gives a way to override them easily using INTERCEPTPROP if we wish
    % to later
    defaults.parentfh = [];
    defaults.xpixelpos = [];
    defaults.ypixelpos = [];

    [defaults unvi] = interceptprop({varargin{2:length(varargin)}}, ...
	defaults); 

    % add a view menu linking back to the parent window. This gives us a
    % way to link together any children created by this dialog, with
    % those created by the parent window
    if isempty(defaults.parentfh)
      viewmenu(fh); % no parent to link to, create a new family of views
    else
      % link to existing family of views
      viewmenu(defaults.parentfh, fh);      
    end

    if isempty(defaults.xpixelpos) & isempty(defaults.ypixelpos)
      % assume N-S keogram across image centre
      [defaults.xpixelpos defaults.ypixelpos] = getpixels(mia);
      defaults.xpixelpos = ...
	  defaults.xpixelpos(ceil(length(defaults.xpixelpos)/2));
    end
    
    
    % add all the controls
    spacing = [10 10]; % x,y
    frameSpacing = [10 10]; % figure to frame spacing
    lineSpacing = 8;  % vertical spacing between lines of text
    bgColour = get(fh, 'Color');
    
    % need to find suitable width and height for all the text objects in
    % the LH column. Do this by creating a test object and finding out
    % what its position is after uiresize. The text used should be the
    % longest. Strictly speaking that might depend on font, kerning etc
    % but we will ignore that. Add an extra space if you have problems :)
    textH = uicontrol(fh, 'Style', 'text', ...
	'Position', [0 0 1 1], ...
	'String', 'X grid points:  ');
    textPos = uiresize(textH, 'l', 'b');
    frameColour = get(fh, 'Color');
    delete(textH);
 
    % allocate space for the handles and positions
    genH     = zeros(8,1);   % handles to uicontrols in general frame
    genPos   = zeros(8,4);   % positions  "                    "

    genPos(1,:) = [frameSpacing 50 0] + ...
	[0 50 0 2*spacing(2)+4*textPos(4)+3*lineSpacing]; 

    % -----------------------------
    % General frame
    % frame  = 1
    genH(1) = uicontrol(fh, 'Style', 'frame', ...
	'BackgroundColor', frameColour, ...
	'Position', genPos(1,:), ...
	'Tag', 'generalframe');
    
    % 'Keogram' = 2
    genPos(2,:) = [genPos(1,1)+spacing(1), ...
	  genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
	  genPos(1,3)-2*spacing(2), textPos(4)];
    genH(2) = uicontrol(fh, 'Style', 'text', ...
	'String', 'Keogram', ...
	'FontWeight', 'bold', ...
	'HorizontalAlignment', 'center', ...
	'Position', genPos(2,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');

    
    % x coord of RH column in figure
    rhCol = genPos(1,1) + spacing(1) + textPos(3) + 30;

    % Units = 7
    genPos(7,:) = [genPos(1,1)+spacing(1), ...
	  genPos(2,2)-lineSpacing- textPos(4), ...
	  textPos(3), textPos(4)];
    genH(7) = uicontrol(fh, 'Style', 'text', ...
	'String', 'Units: ', ...
	'HorizontalAlignment', 'right', ...
	'Position', genPos(7,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');
    
    % units (deg/km)
    units = getpixelunits(mia);
    if strcmp(units, 'deg')
      units = 'Degrees';
    end
    genPos(8,:) =  [rhCol genPos(7,2) textPos(3:4)];
    genH(8) = uicontrol(fh, 'Style', 'text', ...
	'String', units, ...
	'HorizontalAlignment', 'left', ...
	'Position', genPos(8,:), ...
	'BackgroundColor', frameColour, ...	
	'Enable', 'on');    
    
    % 'X grid points' = 3
    genPos(3,:) = genPos(7,:) - [0 textPos(4)+lineSpacing 0 0];
    genH(3) = uicontrol(fh, 'Style', 'text', ...
	'String', 'X grid points: ', ...
	'HorizontalAlignment', 'right', ...
	'Position', genPos(3,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');
 

    % X grid points box
    genPos(4,:) = [rhCol genPos(3,2) textPos(3:4)];
    genH(4) = uicontrol(fh, 'Style', 'edit', ...
	'HorizontalAlignment', 'center', ...
	'Position', genPos(4,:), ...
	'Callback', [mfilename '(''grid'');'], ...
	'String', printseries(defaults.xpixelpos), ...
	'Tag', 'xgridpoints');
    
    % 'Y grid points'
    genPos(5,:) = genPos(3,:) - [0 textPos(4)+lineSpacing 0 0];
    genH(5) = uicontrol(fh, 'Style', 'text', ...
	'String', 'Y grid points: ', ...
	'HorizontalAlignment', 'right', ...
	'Position', genPos(5,:), ...
	'BackgroundColor', frameColour, ...
	'Enable', 'inactive');
    
    % Y grid points box
    genPos(6,:) = [rhCol genPos(5,2) textPos(3:4)];
    genH(6) = uicontrol(fh, 'Style', 'edit', ...
	'HorizontalAlignment', 'center', ...
	'Position', genPos(6,:), ...
	'Callback', [mfilename '(''grid'');'], ...
	'String', printseries(defaults.ypixelpos), ...
	'Tag', 'ygridpoints');
    
    genPos([4 6],:) = uiresize(genH([4 6]), 'l', 'b', ...
	{'12345678901234567890'; '12345678901234567890'});  

    bb = boundingbox(genPos(3, :), genPos(4, :), genPos(5, :), ...
	genPos(6, :));
    
    % calculate frame width
    genPos(1,3) = bb(1) + bb(3) + frameSpacing(1); 
    % centre 'Keogram'						   
    genPos(2,[1 3]) = genPos(1,[1 3]) + [spacing(1) -2*spacing(1)];
    figPos = get(fh, 'Position');
    figPos(3) = genPos(1,1) + genPos(1,3) + frameSpacing(1);
    figPos(4) = genPos(1,2) + genPos(1,4) + frameSpacing(2);
    set([genH(1:2);fh], {'Position'}, {genPos(1,:); genPos(2,:); ...
	  figPos});
    
    
    okPos = [10 10 1 1];
    [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'');'], ...
	[mfilename '(''cancel'');']);

    
    
    ud.mia = mia;
    ud.defaults = defaults;
    if ~isempty(unvi)
      ud.unvi = varargin{1+unvi};
    else
      ud.unvi = {};
    end
    set(fh, 'UserData', ud, 'Visible', 'on');

    
    % --------------------------------------------------------------
  case 'grid'
    % mfilename('grid', 'x');
    % mfilename('grid', 'y');
    fh = gcbf;
    h = gcbo;
    s = get(h, 'String'); 
    n = unique(mystr2num(s)); % also sorts
    set(h, 'String', printseries(n));

    % --------------------------------------------------------------
  case 'ok'
    fh = gcbf;
    pointer = get(fh, 'Pointer');
    set(fh, 'Pointer', 'watch');
    ud = get(fh, 'UserData');
    
    % get and check the grid points
    xpixelpos = str2num(get(findobj(fh, 'Tag', 'xgridpoints'), ...
	'String')); 
    ypixelpos = str2num(get(findobj(fh, 'Tag', 'ygridpoints'), ...
	'String'));
    
    xPixLen = length(xpixelpos);
    yPixLen = length(ypixelpos);

    if xPixLen == 0
      errordlg('An X coordinate must be given.', 'Error', ...
	  'modal');
      set(fh, 'Pointer', pointer);
      return;
      
    elseif yPixLen == 0
      errordlg('A Y coordinate must be given.', 'Error', ...
	  'modal');
      set(fh, 'Pointer', pointer);
      return;
      
    elseif xPixLen ~= yPixLen & xPixLen ~= 1 & yPixLen ~= 1
      errordlg(['If X and Y coordinates are vectors they must ' ...
	    'be the same length.'], 'Error', 'modal');
      set(fh, 'Pointer', pointer);
      return;
    end
    
    disp(['xpixelpos: ' printseries(xpixelpos)]);
    disp(['ypixelpos: ' printseries(ypixelpos)]);
    newfh = keogram(ud.mia, ud.unvi{:}, 'xpixelpos', xpixelpos, ...
	'ypixelpos', ypixelpos);
    viewmenu(fh, newfh); % link to existing data
    % set(fh, 'Pointer', pointer);
    close(fh);
    
    % --------------------------------------------------------------
  case 'cancel'
    varargout = cell(1,nargout);
    close(gcbf);
    
    
    % --------------------------------------------------------------
  otherwise
    error('unknown action');
    
end


% --------------------------------------------------------------
function r = mystr2num(s)
t = s;
[tmp tmpIndex] = setdiff(t, '0123456789:-.'); % keep 0-9:
t(tmpIndex) = ' ';
if ~strcmp(s, t) 
  disp(sprintf('\a')); % string was changed - make an annoying beep
end
r = str2num(['[' t ']']);
