function surfacecallback(h)

ud = get(h, 'UserData');
if ~isempty(ud)
  error(sprintf('Cannot use %s, UserData not empty', mfilename));
end
% If we endlessly calculate new positions then we have problems when nans
% are present. Return to original values to avoid 'nan pollution'
ud.originalCData = get(h, 'CData');
ud.originalXData = get(h, 'XData');
ud.originalYData = get(h, 'YData');
ud.originalZData = get(h, 'ZData');

while 1
  waitfor(h, 'FaceColor');
  if ~ishandle(h)
    return;
  end
  s = get(h)
  
  if strcmp(s.FaceColor, 'flat') & ...
	strcmp(s.UserData.previousFaceColor, 'interp')
    % extend CData, XData, YData and ZData matrices
    cdata = s.CData;
    cdata(end+1,:) = cdata(end,:);
    cdata(:,end+1) = cdata(:,end);
    
    xdata = localExtendMatrix(s.XData)
    
    xdata(end+1) = sum(s.XData((end-1):end) .* [-1 2])
    ydata = s.YData;
    ydata(end+1) = sum(s.YData((end-1):end) .* [-1 2])
    zdata = s.ZData;
    zdata(end+1) = sum(s.ZData((end-1):end) .* [-1 2])
    
    zdata = s.CData;
    zdata(end+1,:) = cdata(end,:);
    zdata(:,end+1) = cdata(:,end);
    % set(h, 'CData', cdata, 'XData', xdata, 'YData', ydata, 'ZData', zdata);
    
  elseif strcmp(s.FaceColor, 'interp') & ...
	strcmp(s.UserData.previousFaceColor, 'flat')
    if 0
      % shorten CData, XData, YData and ZData matrices
      cdata = s.CData(1:(end-1), 1:(end-1));
      xdata = s.XData(1:(end-1));
      ydata = s.YData(1:(end-1));
      zdata = s.ZData(1:(end-1), 1:(end-1));
      set(h, 'CData', cdata, 'XData', xdata, 'YData', ydata, 'ZData', ...
	     zdata);
    end
    set(h, 'CData', ud.originalCData, ...
	   'XData', ud.originalXData, ...
	   'YData', ud.originalYData, ...
	   'ZData', ud.originalZData);
  else
    % a color or 'none'. Make no changes.
  end
  ud.previousFaceColor = s.FaceColor;
  set(h, 'UserData', ud);
  clear s; % save memory while waiting
end

% -----------------
function ea = localExtendMatrix(a)
ea = zeros(size(a) + [1 1]);

for r = 1:size(a, 1)
  for c = 1:size(a, 2)
    ea(r+1, c+1) = mean(reshape(a(r+[0 1], c+[0 1]), [1 4]));
  end
end
    
 
