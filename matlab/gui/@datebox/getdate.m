function t = getdate(db, varargin)
%GETDATE  Return the date from a DATEBOX object.
%
%   t = GETDATE(db)
%   t: TIMESTAMP object (hours, minutes and seconds not set)
%   db: DATEBOX object
%
%   See also DATEBOX, datebox/SETDATE.

% By default the date is calculated from day of month, month and year as
% that is more efficient, and the two methods are usually the
% same. However, the callback function may require the date generated
% from the day of year and year, so allow the option of doing so.

switch nargin
 case 1
  t = timestamp(datevec(db));
  
 case 2
  % for internal use. Don't publish this interface.
  if strcmp(varargin{1}, 'doy')
    t = timestamp(getvalue(getnumberbox(db.yearhandle)), ...
	     getvalue(getnumberbox(db.ydayhandle)), 0, 0, 0);
  else
    t = timestamp(datevec(db));
  end
  
 otherwise
  error('Incorrect parameters');
end



