function h = gethandle(db)
%GETHANDLE  Return the handles of uicontrols used by a DATEBOX object.
%
%   h = GETHANDLE(db)
%   h: vector of handles used in DATEBOX
%   db: DATEBOX object
%
%   See also DATEBOX.

deprecated('use gethandles');

h = [db.mdayhandle db.monthhandle db.yearhandle db.ydayhandle];
