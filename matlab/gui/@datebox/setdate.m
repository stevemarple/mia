function setdate(db, t)
%SETDATE  Set the date displayed in a DATEBOX.
%
%   SETDATE(db, t)
%   db: DATEBOX object
%   t: TIMESTAMP object (hours, minutes and seconds ignored)
%
%   See also DATEBOX, datebox/GETDATE.

if isempty(t)
  % have to set to a date - so use today
  t = time;
end
dv = datevec(t);

% set(db.mdayhandle, 'Value', dv(3));
setvalue(getnumberbox(db.mdayhandle), dv(3));
set(db.monthhandle, 'Value', dv(2));
setvalue(getnumberbox(db.yearhandle), dv(1));
setvalue(getnumberbox(db.ydayhandle), getdayofyear(t));

  
