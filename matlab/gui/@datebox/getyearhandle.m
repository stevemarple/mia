function h = getyearhandle(db)
%GETYEARHANDLE  Return the handle of the year uicontrol.
%
%   h = GETYEARHANDLE(db)
%   h: UICONTROL handle
%   db: DATEBOX object
%
%   See also DATEBOX.

h = db.yearhandle;
