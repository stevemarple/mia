function appendcallback(db, varargin)
%APPENDCALLBACK  Append a callback to a DATEBOX.
%
%   APPENDCALLBACK(db, s)
%   db: DATEBOX
%   s: callback string
%
%   APPENDCALLBACK appends a user defined callback to the UICONTROL
%   objects used by a DATEBOX. After the DATEBOX has been adjust
%   the callback is called.
%
%   See also DATEBOX, APPENDCALLBACK.

h = [db.mdayhandle db.monthhandle db.yearhandle db.ydayhandle];
appendcallback(h, varargin{:});
