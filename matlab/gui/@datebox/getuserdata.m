function data = getuserdata(db)
%GETUSERDATA  Return data from the internal user data area of a DATEBOX.
%
%   d = GETUSERDATA(db)
%   d: data previously set with datebox/SETUSERDATA
%   db: DATEBOX object
%
%   See also DATEBOX, datebox/SETUSERDATA.

data = db.userdata;

