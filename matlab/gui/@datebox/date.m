function t = date(db)
%DATE  Return the date from a DATEBOX object.
%
%   t = DATE(db)
%   t: TIME object (hours, minutes and seconds not set)
%   db: DATEBOX object
%
%   See also datebox/GETDATE.

deprecated('use getdate instead');
t = time(datevec(db));
