function dv = datevec(db)
%DATEVEC  Return a DATEVEC style array of the time/date info.
%
%   dv = DATEVEC(db)
%   dv: DATEVEC style vector, [yyyy mm dd HH MM SS]
%   db: DATEBOX object
%
%   See also DATEBOX, timefun/DATEVEC, TIME, TIMESPAN.

dv = zeros(1, 6);
dv(1) = getvalue(getnumberbox(db.yearhandle));
dv(2) = get(db.monthhandle, 'Value');
% dv(3) = get(db.mdayhandle, 'Value');
dv(3) = getvalue(getnumberbox(db.mdayhandle));
