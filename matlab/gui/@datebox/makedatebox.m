function [mdayHandle, monthHandle, yearHandle, ydayHandle] = makedatebox(db, parent, pos)
%MAKEDATEBOX  DATEBOX internal function to draw a new DATEBOX object.
%
%   This function is used internally by DATEBOX.

spacing = [0 5]; % X,Y

% year
yearPos = [pos(1) pos(2) 1 1];
yearHandle = numberbox(parent, yearPos, '%g', ...
    'n=round(min(max(1900,n),2100));', 1994); 
yearPos = uiresize(yearHandle, 'l', 'b', '99999');


% month
monthPos = [yearPos(1)+yearPos(3)+spacing(1), pos(2) 1 1];

months = {'January', 'February', 'March', 'April', 'May', 'June', ...
	  'July', 'August', 'September', 'October', 'November', 'December'};
monthHandle = double(uicontrol(parent, 'Style', 'popupmenu', ...
			       'Position', monthPos, ...
			       'HorizontalAlignment', 'center', ...
			       'String', months));

monthPos = uiresize(monthHandle, 'l', 'b');

mdayPos = [monthPos(1)+monthPos(3)+spacing(1), pos(2), 1, 1];
mdayHandle = numberbox(parent, mdayPos, '%g', ...
    'n=round(min(max(1,n),31));', 1); 
mdayPos = uiresize(mdayHandle, 'l', 'b', '999');

% yday
ydayPos = [mdayPos(1)+mdayPos(3)+spacing(1)+10, ... % extra spacing here
      pos(2), 1 1];
ydayHandle = numberbox(parent, ydayPos, '%g', ...
    'n=round(min(max(1,n),366));', 1);  
ydayPos = uiresize(ydayHandle, 'l', 'b', '9999');


if matlabversioncmp('>=', '5.2')
  % add tooltips
  set([mdayHandle monthHandle yearHandle ydayHandle], ...
      {'TooltipString'}, {'day of month'; 'month'; 'year'; 'day of year'});
end
  









