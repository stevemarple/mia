function s = getprevious(db)
%GETPREVIOUS  Return the handle of the previous box which was modified.
%
%   This function is used internally by DATEBOX.
%
% This function is used so that if yday was previously set, and then year
% changed (especially to/from a leap year) then the day of year and year is
% used to set the date. Otherwise the day of year changes due to leap day.

s = db.previous;
