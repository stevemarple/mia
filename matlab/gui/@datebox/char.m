function s = char(db)
%CHAR  Convert a DATEBOX object to a CHAR representation.
%
%   r = CHAR(db)
%   r: CHAR representation of datebox
%   db: DATEBOX object
%
%   See also DATEBOX.

s = char(getdate(db));

