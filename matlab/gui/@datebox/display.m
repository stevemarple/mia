function display(db)
%DISPLAY  Display a datebox object.

disp(' ');
disp([inputname(1),' = ' char(db)]);
disp(' ');
