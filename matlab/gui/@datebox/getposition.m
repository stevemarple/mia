function p = getposition(db)
%GETPOSITION  Return the position matrix for a DATEBOX object.
% 
%   p = GETPOSITION(db)
%   p: position vector
%   db: DATEBOX object
%
%   See also BOUNDINGBOX.
p = boundingbox(db.mdayhandle, db.monthhandle, db.yearhandle, ...
    db.ydayhandle); 
