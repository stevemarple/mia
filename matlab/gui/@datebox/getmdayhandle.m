function h = getmdayhandle(db)
%GETMDAYHANDLE  Return the handle of the mday (day of month) uicontrol.
%
%   h = GETMDAYHANDLE(db)
%   h: UICONTROL handle
%   db: DATEBOX object
%
%   See also DATEBOX.

h = db.mdayhandle;
