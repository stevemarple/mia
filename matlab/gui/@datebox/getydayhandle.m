function h = getydayhandle(db)
%GETYDAYHANDLE  Return the handle of the yday (day of year) uicontrol.
%
%   h = GETYDAYHANDLE(db)
%   h: UICONTROL handle
%   db: DATEBOX object
%
%   See also DATEBOX.

h = db.ydayhandle;
