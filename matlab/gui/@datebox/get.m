function varargout = get(db, varargin)
%GET  Get object properties for all uicontrol objects in DATEBOX.
%
%   r = GET(db, ...)
%   r: object properties
%   db: DATEBOX object
%
%   datebox/GET calls the builtin GET function on all UICONTROL objects
%   under its control. See graphics/GET for more details.

varargout = cell(size(varargin));

if strcmp(class(db), 'double')
  varargout = builtin('get', db, varargin{:});
  return;
else
  varargout{:} = get([db.mdayhandle db.monthhandle db.yearhandle ...
	db.ydayhandle], varargin{:});
end

% varargout{1} = get(db.mdayhandle, varargin{:});
% varargout{2} = get(db.monthhandle, varargin{:});
% varargout{3} = get(db.yearhandle, varargin{:});
% varargout{4} = get(db.ydayhandle, varargin{:});





