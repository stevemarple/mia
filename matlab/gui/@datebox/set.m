function set(db, varargin)
%SET  Set object properties for all uicontrol objects in DATEBOX.
%
%   SET(db, ...)
%   db: DATEBOX object
%
%   SET UICONTROL properties for all uicontrol objects in a DATEBOX. This
%   function should be used by care as it is possible to break the
%   DATEBOX behaviour. It is useful for setting the DATEBOX to be
%   visible/invisible, enabled/off etc.
%
%   See also DATEBOX, datebox/GET.

if strcmp(class(db), 'double')
  builtin('set', db, varargin{:});
  return;
end

set(db.mdayhandle, varargin{:});
set(db.monthhandle, varargin{:});
set(db.yearhandle, varargin{:});
set(db.ydayhandle, varargin{:});


