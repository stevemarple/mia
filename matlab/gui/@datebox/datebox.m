function handle = datebox(varargin)
%DATEBOX Create an edit box for entering dates.
%   Return handle to first graphics object drawn by datebox
%
%   h = DATEBOX(parent, pos);

% Unlike normal class constructors this one returns a handle to a graphics
% object, the actual class data is stored in the user data area. This is
% because the callbacks really need access to a reference, rather than an
% old static copy of what was in the datebox. By using one of the handles,
% and storing the class object in the UserData area of that handle then we
% effectively have a reference.


% member fields
db.mdayhandle = []; % store class in the user area of mday uicontrol
db.monthhandle =[];
db.yearhandle = [];
db.ydayhandle = [];
db.userdata = [];
db.previous = [];
db.callback = '';


if nargin == 2 & ishandle(varargin{1})
  db = class(db, 'datebox');

  [db.mdayhandle db.monthhandle db.yearhandle db.ydayhandle] ...
      = makedatebox(db, varargin{1:2});
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  defaults = db;
  defaults.parent = [];
  defaults.position = [];
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  db = rmfield(defaults, {'parent', 'position'});
  db = class(db, 'datebox');
  [db.mdayhandle db.monthhandle db.yearhandle db.ydayhandle] ...
      = makedatebox(db, defaults.parent, defaults.position);

else
  error('incorrect parameters');
end
  
  


% Keep only one copy of db, but the callbacks on ALL uicontrols must be
% able to find it - reference it by storing the handle to the uicontrol
% whose UserData contains the relevant db.


% set db.mdayhandle in update call later
set(db.monthhandle, 'UserData', db.mdayhandle);
% these are not normal uicontrols - UserData area already in use
setuserdata(getnumberbox(db.yearhandle), db.mdayhandle);
setuserdata(getnumberbox(db.ydayhandle), db.mdayhandle);

% add some callbacks so that the date can be updated after a field has
% changed
set(db.monthhandle, 'Callback', ...
    'dateboxcb(getdatebox(gcbo),''month'');');

% numberbox already has callback so append one
appendcallback([db.mdayhandle db.yearhandle db.ydayhandle], ...
    { 'dateboxcb(getdatebox(gcbo),''mday'');' ...
      'dateboxcb(getdatebox(gcbo),''year'');' ...
      'dateboxcb(getdatebox(gcbo),''yday'');'});
  
update(db);

% run callback to initialise everything
dateboxcb(db, 'yday');
handle = double(db.mdayhandle);








