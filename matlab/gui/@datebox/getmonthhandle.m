function h = getmonthhandle(db)
%GETMONTHHANDLE  Return the handle of the month uicontrol.
%
%   h = GETMDAYHANDLE(db)
%   h: UICONTROL handle
%   db: DATEBOX object
%
%   See also DATEBOX.

h = db.monthhandle;
