function varargout = setposition(db, pos)
%SETPOSITION Set the position of a DATEBOX.
%
%   newPos = setposition(db, pos)
%   newPos: new position of DATEBOX
%   db: TIMEBOX
%   pos: new position (width and height ignored)
%
%   See also DATEBOX, datebox/GETPOSITION.

handles = [db.yearhandle db.monthhandle db.mdayhandle db.ydayhandle];
positions = get(handles, 'Position');
trans = pos(1:2) - positions{1}(1:2); % translate by this much

for n = 1:length(handles)
  positions{n}(1:2) = positions{n}(1:2) + trans;
end

set(handles, {'Position'}, positions);

if nargout == 1
  varargout{1} = boundingbox(positions{:});
end
