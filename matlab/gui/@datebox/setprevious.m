function setprevious(db, s)
%SETPREVIOUS  Set the previous box inside a DATEBOX which was modified.
%
%  This function is used internally by DATEBOX.
%
%   See also datebox/GETPREVIOUS.

db.previous = s;
update(db);


