function setuserdata(db, data)
%SETUSERDATA  Set the internal user data area of a DATEBOX.
%
%   SETUSERDATA(db, d)
%   db: DATEBOX object
%   d: user data (any type)
%
%   See also DATEBOX, datebox/GETUSERDATA.

db.userdata = data;
update(db);

