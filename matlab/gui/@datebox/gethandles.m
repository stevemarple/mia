function h = gethandles(db)
%GETHANDLE  Return the handles of uicontrols used by a DATEBOX object.
%
%   h = GETHANDLES(db)
%   h: vector of handles used in DATEBOX
%   db: DATEBOX object
%
%   See also DATEBOX.

h = [db.mdayhandle; db.monthhandle; db.yearhandle; db.ydayhandle];
