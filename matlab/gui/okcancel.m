function varargout = okcancel(action, varargin)
%OKCANCEL  Function to create and manage Ok / Cancel buttons.
%
%   [oh ch] = OKCANCEL('init', fh, pos, okCb, cancelCb)
%
%   fh: figure handle
%   pos: position vector
%   oh: Ok button handle
%   ch: Cancel button handle
%   okCb: Callback string for ok button
%   cancelCb: Callback string for cancel button
%   Create Ok and Cancel buttons, assigning callbacks.
%
%   OKCANCEL('resize', fh);
%   OKCANCEL('resize', fh, oh, ch);
%
%   fh:figure handle
%   oh: ok button handle
%   ch: cancel button handle
%   Resize the ok and cancel buttons on the figure. If only the figure
%   handle is supplied it is assumed only one set of ok/cancel buttons exist
%   the figure.  The button width and height are automatically adjusted for
%   font size/name using UIRESIZE.
%
%   By calling the resize action inside a ResizeFcn callback on the
%   figure window the buttons will always be placed centrally (the x
%   offset part of the position vector is ignored).
%
%   See also UICONTROL, UIRESIZE.

% For speed it is assumed that the figure might use any units but that
% the Ok and Cancel buttons always use the same units as each other.

spacing = [8 5]; % x,y
switch action
  case 'init'
    fh = varargin{1};
    pos = varargin{2};
    okCb = varargin{3};
    cancelCb = varargin{4};
    % textWidth = inline('w*6+12'); % nominal width of one character
    if nargin > 5
      % spacing = varargin{5};
      warning('spacing option will be removed');
    end

    % initially place exactly where indicated
    okPos = pos;
    cancelPos = pos + [spacing(1)+pos(3) 0 0 0];
    oh = uicontrol(fh, 'Position', okPos, ...  
      	'Visible', 'off', ...
      	'String', 'OK', ...
      	'Tag', 'ok', ...
      	'Callback', okCb);
    
    ch = uicontrol(fh, 'Position', cancelPos, ...
      	'Visible', 'off', ...
      	'String', 'Cancel', ...
      	'Tag', 'cancel', ...
      	'Callback', cancelCb);

    % cancel is longer than ok so resize only that button
    cancelPos = uiresize(ch, 'l', 'b');
    okPos = [okPos(1)+okPos(3)-cancelPos(3) cancelPos(2) ...
	  cancelPos(3) cancelPos(4)];
	  
    if matlabversioncmp('>=', '5.2')
      set([oh; ch], {'TooltipString'}, ...
	  {'accept details'; 'cancel and close window'});
    end
    
    % move to centre
    figUnits = get(fh, 'Units');
    if strcmp(figUnits, 'pixels')
      figPos = get(fh, 'Position');
    else
      set(fh, 'Units', 'pixels');
      figPos = get(fh, 'Position');
      set(fh, 'Units', figUnits);
    end      

    newCancelPos1 = (figPos(3) + spacing(1)) / 2;
    translate = newCancelPos1 - cancelPos(1); % amount to shift to right
    
    okPos(1) = okPos(1) + translate;
    cancelPos(1) = cancelPos(1) + translate;
    set(oh, 'Position', okPos, ...
	'Visible', 'on');
    set(ch, 'Position', cancelPos, ...
	'Visible', 'on');

    varargout{1} = oh;
    switch nargout
      case {0, 1}
	;
	
      case 2
      varargout{2} = ch;
    
    otherwise
      error('incorrect number of output parameters');
    end
    
  case 'resize'
    % okcancel('resize', fh); % lazy way
    % okcancel('resize', fh, okh, cancelh); % faster way
    switch nargin
      case 2
	fh = varargin{1};
	oh = findobj(fh, 'Tag', 'ok');
	ch = findobj(fh, 'Tag', 'cancel');
      
      case 4
	fh = varargin{1};
	oh = varargin{2};
	ch = varargin{3};
	
      otherwise
	error('incorrect parameters');
    end
    
    okPos = get(oh, 'Position');
    cancelPos = get(ch, 'Position');
    % spacing = cancelPos(1) - okPos(1) - okPos(3);
    
    figUnits = get(fh, 'Units');
    okUnits = get(oh, 'Units');
    if strcmp(figUnits, okUnits)
      figPos = get(fh, 'Position');
    else
      set(fh, 'Units', okUnits);
      figPos = get(fh, 'Position');
      set(fh, 'Units', figUnits);
    end      
    
    newCancelPos1 = (figPos(3) + spacing(1)) / 2;
    translate = newCancelPos1 - cancelPos(1); % amount to shift to right
    
    okPos(1) = okPos(1) + translate;
    cancelPos(1) = cancelPos(1) + translate;
    set([oh; ch], {'Position'}, {okPos; cancelPos});
	
  otherwise
    error('unknown action');
    
end

