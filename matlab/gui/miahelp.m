function miahelp(url)
%MIAHELP  View help in default web browser.
%
%   MIAHELP(url)
%   url: any standard URL.
%
%   See also DOCOPT.

[status host] = unix('/bin/uname -n');
host = sscanf(host, '%s');  % get rid of whitespace, newlines etc

if strcmp(host, 'mango') | strcmp(host, 'egcomms-05')
  prefix = 'http://localhost/mia';
else
  prefix = 'http://www.dcs.lancs.ac.uk/iono/mia';
end

if url(1) ~= '/'
  fullUrl = [prefix '/' url];
else
  fullUrl = [prefix url];
end

status = web(fullUrl);

switch status
  case 0
    % ok
    
  case 1
    errordlg(['Browser could not be found. Please configure. See' ...
	  ' command window for more information'], 'Error', 'modal');
    help docopt;
    
  case 2
    errordlg(['Cannot launch browser.'], 'Error', 'modal');
    help docopt;
   
  otherwise
    ; % unknown status
end




