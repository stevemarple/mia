function varargout = miaedit(action, varargin)
%MIAEDIT  Edit an MIA data file.
%
%
%   Valid actions are: init
%
%

pointer.normal = 'arrow';
pointer.add = 'fullcrosshair';
pointer.move = 'fleur';
pointer.delete = 'custom'; % skull and cross bones!

if ~ischar(action)
  error('action must be a char');
end

switch action
  % --------------------------------------------------------------
 case 'init'
  % miaedit('init', curMia)
  % miaedit('init', curMia, name/value pairs)
  
  if nargin < 2 | rem(nargin, 2) == 1
    error('invalid parameters');
  end
  
  curMia = varargin{1};
  if ~isa(curMia, 'mia_base') | isa(curMia, 'mia_image_base')
    error('Invalid data type');
  end

  [defaults.title defaults.windowtitle] = maketitle(curMia, ...
						    'customstring', ...
						    'MIA Data Editor');
  defaults.windowtitle = ['MIA edit: ' defaults.windowtitle];
  defaults.reference = [];
  defaults.referencecolor = 'g';
  % handle of the function which called us. If set (and updatefcn
  % also set) then the data is put back into that window, as long as it
  % exists. The calling window should have been created with
  % IntegerHandle set to off.
  defaults.fighandle = [];
  
  % parameter list for feval. The new data is tagged onto the end  
  defaults.updatefcn = {}; 
  
  defaults.windowstyle = 'normal';
  defaults.filters = {};
  defaults.configurefilters = 1;
  [defaults unvi] = interceptprop({varargin{2:end}}, defaults);
  
  defaults.configurefilters = logical(defaults.configurefilters);

  instrument = getinstrument(curMia);
  instrumentCls = class(instrument);
  switch instrumentCls
   case 'riometer'
    curBeams  = getbeams(curMia);
    [curImagingBeams curWideBeams] = info(instrument, 'beams');
    parameterName = 'beam';
    
   case 'magnetometer'
    curComponents = getcomponents(curMia);
    parameterName = 'component';    
    
   otherwise
    error(sprintf('unknown instrument type (was ''%s'')', instrumentCls));
  end
  
  % for RH placement of controls
  % don't put this is ud just yet, we can only load ud after the
  % figure has been created. This is needed to ceate the figure. This
  % is not quite a position vector, it gives the spacing on [L B T R],
  % in normalized units.

  % buttons on RHS
  % plotAxesSpacing = [0.3 0.1 0.1 0.1]; 
  % buttons on LHS
  plotAxesSpacing = [0.1 0.1 0.3 0.1]; 
  [fh gh] = makeplotfig('init', ...
			'miniplots', [1 1], ...
			'spacing', plotAxesSpacing, ...
			'integerhandle', 'off', ...
			'visible', 'off', ...
			'title', defaults.title, ...
			'WindowStyle', defaults.windowstyle);
  makeplotfig('title', fh);
  fhData = get(fh);
  if matlabversioncmp('>=', '5.2')
    uicontextmenu = get(gh, 'UIContextMenu');
    delete(uicontextmenu);
  end
  % set callbacks for zoom UIMENU functions to use our the same ones as
  % for the uicontrol popupmenu
  zoomOnMenuH = findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_on');
  set(zoomOnMenuH, 'Callback', [mfilename '(''zoom'',''on'');']);
  
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_off'), ...
      'Callback', [mfilename '(''zoom'',''off'');']);
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_in'), ...
      'Callback', [mfilename '(''zoom'',''in'');']);
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_out'), ...
      'Callback', [mfilename '(''zoom'',''out'');']);
  set(findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_limits'), ...
      'Callback', [mfilename '(''zoom'',''limits'');']);
  
  
  % load UserData from figure just in case something else is also using it
  ud = fhData.UserData;
  ud.instrumentCls = instrumentCls;
  ud.parameterName = parameterName;
  ud.curMia = curMia;
  ud.originalMia = curMia;
  ud.refMia = defaults.reference;
  defaults.reference = []; % delete to save memory
  ud.refMiaH = [];
  ud.defaults = defaults;
  
  ud.spacing = [10 10]; % x,y
  ud.frameSpacing = [10 10]; % figure to frame spacing
  ud.lineSpacing = 8;  % vertical spacing between lines of text
  ud.plotAxesSpacing = plotAxesSpacing;
  switch instrumentCls
    case 'riometer'
     ud.beam = min(curBeams);
     ud.curBeams = sort(curBeams);

     [tmp1 tmp2 ud.curMiaH] = plot(ud.curMia, 'plotaxes', gh(1), ...
				   'beams', ud.beam);
     ud.curMiaH = ud.curMiaH(1); % real line is the first handle
     ud.tmpMiaH = [];
  
     if ~isempty(ud.refMia)
       np = get(gh(1), 'NextPlot');
       set(gh(1), 'NextPlot', 'add')
       [tmp1 tmp2 ud.refMiaH] = plot(ud.refMia, 'plotaxes', gh(1), ...
				     'beams', ud.beam, ...
				     'color', ud.defaults.referencecolor);
       set(gh(1),'NextPlot', np)
     end
     
   case 'magnetometer'
    ud.component = curComponents{1};
    ud.curComponents = curComponents;
    
    [tmp1 tmp2 ud.curMiaH] = plot(ud.curMia, 'plotaxes', gh(1), ...
				   'components', ud.component);
    ud.tmpMiaH = [];

    if ~isempty(ud.refMia)
       np = get(gh(1), 'NextPlot');
       set(gh(1), 'NextPlot', 'add')
       [tmp1 tmp2 ud.refMiaH] = plot(ud.refMia, 'plotaxes', gh(1), ...
				     'components', ud.component, ...
				     'color', ud.defaults.referencecolor);
       set(gh(1),'NextPlot', np)
    end
    
  end
  
  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems
  % :)
  
  longestStr = 'Delete points';
  longestStrLen = length(longestStr);

  if isempty(ud.defaults.filters)
    ud.defaults.filters = mia_filters(ud.curMia);
  elseif ~iscell(ud.defaults.filters)
    ud.defaults.filters = {ud.defaults.filters};
  end

  % find the names of the filters used, see if their names are longer 
  for n = 1:length(ud.defaults.filters)
    if ischar(ud.defaults.filters{n}) ...
	  | isa(ud.defaults.filters{n}, 'function_handle')
      % convert function name to filter
      ud.defaults.filters{n} = feval(ud.defaults.filters{n});
    end
    if ~isa(ud.defaults.filters{n}, 'mia_filter_base')
      error('not a MIA filter object');
    end
    filterDesc{n} = getname(ud.defaults.filters{n});
    if length(filterDesc{n}) > longestStrLen;
      longestStrLen = length(filterDesc{n});
      longestStr = filterDesc{n};
    end
  end
  
  longestStr = [longestStr ' '];
  textH(1) = uicontrol(fh, 'Style', 'text', ...
		       'Position', [0 0 1 1], ...
		       'String', longestStr);
  % 'String', 'Mapping height:  ');
  textPos = uiresize(textH(1), 'l', 't');
  delete(textH(1));

  % allocate space for the handles and positions
  uiH   = zeros(14, 1);
  uiPos = zeros(14, 4);
  
  % 
  % 
  % uiH is vector of handles of UI objects, plus the frame and axes
  % they 'live' in
  % uiH(1) = axes('Parent', fh, ...
  % 'Position', uiPos(1,:), ...
  % 'Color', 'none');
  
  % add points
  uiPos(1,:) = [-10 -10 1 1]; % hide 
  uiH(1) = uicontrol(fh, 'Style', 'radiobutton', ...
		     'Position', uiPos(1,:), ...
		     'String', 'Add points', ...
		     'HorizontalAlignment', 'left', ...
		     'Callback', [mfilename '(''keypress'',''a'');'], ...
		     'Tag', 'addbutton');
  % resize, using the longest string
  uiPos(1,:) = uiresize(uiH(1), 'l', 't', longestStr);
  if matlabversioncmp('>=', '5.2')
    set(uiH(1), 'TooltipString', 'Add new point to line');
  end
  
  % move points
  uiPos(2,:) = uiPos(1,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(2) = uicontrol(fh, 'Style', 'radiobutton', ...
		     'Position', uiPos(2,:), ...
		     'String', 'Move points', ...
		     'HorizontalAlignment', 'left', ...
		     'Enable', 'off', ...
		     'Callback', [mfilename '(''keypress'',''m'');'], ...
		     'Tag', 'movebutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(2), 'TooltipString', 'Move point on line');
  end
  
  % delete points
  uiPos(3,:) = uiPos(2,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(3) = uicontrol(fh, 'Style', 'radiobutton', ...
		     'Position', uiPos(3,:), ...
		     'String', 'Delete points', ...
		     'HorizontalAlignment', 'left', ...
		     'Enable', 'off', ...
		     'Callback', [mfilename '(''keypress'',''d'');'], ...
		     'Tag', 'deletebutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(3), 'TooltipString', 'Delete point on line');
  end
  
  zoomStr = {'Zoom on' 'Zoom off' 'Zoom in' 'Zoom out' 'Zoom limits'};
  uiPos(10,:) = uiPos(3,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(10) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'Position', uiPos(10,:), ...
		      'String', zoomStr, ...
		      'HorizontalAlignment', 'center', ...
		      'Enable', 'on', ...
		      'Callback', [mfilename '(''zoom'',''?'');'], ...
		      'Tag', 'zoommenu');
  if matlabversioncmp('>=', '5.2')
    set(uiH(10), 'TooltipString', 'Zoom options');
  end

  
  % undo button
  uiPos(4,:) = uiPos(10,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(4) = uicontrol(fh, 'Style', 'pushbutton', ...
		     'Position', uiPos(4,:), ...
		     'String', 'Undo', ...
		     'HorizontalAlignment', 'center', ...
		     'Enable', 'off', ...
		     'Callback', [mfilename '(''undo'');'], ...
		     'Tag', 'undobutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(4), 'TooltipString', 'Undo last change');
  end

  % redo button
  uiPos(9,:) = uiPos(4,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  % uiPos(9,:) = uiPos(4,:) - [0 2*(uiPos(1,4)+ud.lineSpacing) 0 0]; 
  uiH(9) = uicontrol(fh, 'Style', 'pushbutton', ...
		     'Position', uiPos(9,:), ...
		     'String', 'Redo', ...
		     'HorizontalAlignment', 'center', ...
		     'Enable', 'off', ...
		     'Callback', [mfilename '(''redo'');'], ...
		     'Tag', 'redobutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(9), 'TooltipString', 'Redo last undo');
  end

  % select filter
  uiPos(11, :) = uiPos(9,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(11) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'Position', uiPos(11,:), ...
		      'String', filterDesc, ...
		      'HorizontalAlignment', 'center', ...
		      'Enable', 'on', ...
		      'Callback', [mfilename '(''selfilter'');'], ...
		      'Tag', 'selfilter');
  if matlabversioncmp('>=', '5.2')
    set(uiH(11), 'TooltipString', 'Select filter method');
  end
  ud.filterMenuH = uiH(11);
  
  % configure filter
  uiPos(12, :) = uiPos(11,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(12) = uicontrol(fh, 'Style', 'pushbutton', ...
		      'Position', uiPos(12,:), ...
		      'String', 'Configure', ...
		      'HorizontalAlignment', 'center', ...
		      'Callback', [mfilename '(''configfilter'');'], ...
		      'Tag', 'configfilter');
  if matlabversioncmp('>=', '5.2')
    set(uiH(12), 'TooltipString', 'Configure selected filter');
  end
  if ~ud.defaults.configurefilters
    set(uiH(12), 'Enable', 'off');
  end

  % accept button
  uiPos(5,:) = uiPos(12,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(5) = uicontrol(fh, 'Style', 'pushbutton', ...
		     'Position', uiPos(5,:), ...
		     'String', 'Filter', ...
		     'HorizontalAlignment', 'center', ...
		     'Callback', [mfilename '(''accept'');'], ...
		     'Tag', 'acceptbutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(5), 'TooltipString', 'Filter data');
  end

  % previous beam
  uiPos(6,:) = uiPos(5,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(6) = uicontrol(fh, 'Style', 'pushbutton', ...
		     'Position', uiPos(6,:), ...
		     'String', ['Prev. ' parameterName] , ...
		     'HorizontalAlignment', 'center', ...
		     'Callback', [mfilename '(''previous'');'], ...
		     'Tag', 'previousbutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(6), 'TooltipString', ['Edit previous ' parameterName]);
  end

  
  % next beam
  uiPos(7,:) = uiPos(6,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(7) = uicontrol(fh, ...
		     'Style', 'pushbutton', ...
		     'Position', uiPos(7,:), ...
		     'String', ['Next ' parameterName], ...
		     'HorizontalAlignment', 'center', ...
		     'Callback', [mfilename '(''next'');'], ...
		     'Tag', 'nextbutton');
  if matlabversioncmp('>=', '5.2')
    set(uiH(7), 'TooltipString', ['Edit next ' parameterName]);
  end
  
  switch instrumentCls
    case 'riometer'
     % beam box
     uiPos(8,:) = uiPos(7,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
     uiH(8) = beambox('parent', fh, ...
		      'position', uiPos(8,:), ...
		      'imaging', curImagingBeams, ...
		      'widebeam', curWideBeams, ...
		      'title', ' to edit', ...
		      'imagingallow', ...
		      intersect(curImagingBeams, curBeams), ...
		      'widebeamallow', ...
		      intersect(curWideBeams, curBeams), ...
		      'limitedbeams', -1);
     
     bbox = getbeambox(uiH(8));
     resizeseriesbox(bbox, sprintf('%d ', max(curBeams)));
     appendcallback(bbox, [mfilename '(''beams'');']);
     
   case 'magnetometer'
    
  end
  
  % ok
  uiPos(13,:) = uiPos(8,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(13) = uicontrol(fh, ...
		      'Style', 'pushbutton', ...
		      'Position', uiPos(13,:), ...
		      'String', 'OK', ...
		      'HorizontalAlignment', 'center', ...
		      'Callback', sprintf('%s(''ok'',''%s'');', ...
					  mfilename, ...
					  defaults.windowstyle), ...
		      'Tag', 'ok');
  if matlabversioncmp('>=', '5.2')
    set(uiH(13), 'TooltipString', 'OK');
  end

  % cancel
  uiPos(14,:) = uiPos(13,:) - [0 uiPos(1,4)+ud.lineSpacing 0 0]; 
  uiH(14) = uicontrol(fh, ...
		     'Style', 'pushbutton', ...
		     'Position', uiPos(14,:), ...
		     'String', 'Cancel', ...
		     'HorizontalAlignment', 'center', ...
		      'Callback', sprintf('%s(''cancel'',''%s'');', ...
					  mfilename, ...
					  defaults.windowstyle), ...
		     'Tag', 'cancel');
  if matlabversioncmp('>=', '5.2')
    set(uiH(14), 'TooltipString', 'Cancel');
  end

  
  
  set(findobj(fh,  'Type', 'uimenu', 'Tag', 'file_exit'), ...
      'Callback', [mfilename '(''exit'');'], ...
      'Visible', 'on');
  
  % find edit menu, make visible
  editMenuH = findobj(fh, 'Type', 'uimenu', 'Tag', 'edit');
  ud.undomenu = uimenu(editMenuH, 'Label', '&Undo', ...
		       'Enable', 'off', ...
		       'Callback', [mfilename '(''undo'');'], ...
		       'Tag', 'edit_undo');
  ud.redomenu = uimenu(editMenuH, 'Label', '&Redo', ...
		       'Callback', '', ...
		       'Tag', 'edit_redo');
  ud.selUndoMenu = uimenu(editMenuH, 'Label', 'Selective undo...', ...
			  'Separator', 'on', ...
			  'Callback', [mfilename '(''sel'',''undo'');'], ...
			  'Tag', 'edit_selundo');
  ud.selUndoMenuKids = [];
  ud.selRedoMenu = uimenu(editMenuH, 'Label', 'Selective redo...', ...
			  'Callback', [mfilename '(''sel'',''redo'');'], ...
			  'Tag', 'edit_selredo');
  ud.selRedoMenuKids = [];
  set(editMenuH, 'Visible', 'on', ...
		 'Callback', [mfilename '(''draweditmenus'');']);

  % set up callback functions
  lineH = [ud.curMiaH]; % refMiaH
  set(fh, 'KeyPressFcn', [mfilename '(''keypress'');'], ...
	  'ResizeFcn', [mfilename '(''uireposition'');'], ...
	  'Pointer', pointer.normal, ...
	  'Name', defaults.windowtitle);

  if ~any(strcmp(defaults.windowstyle, {'normal' 'modal'}))
    error(sprintf('Unknown window mode (was ''%s'')', ...
		  defaults.windowstyle));
  end
  set(fh, 'CloseRequestFcn', [mfilename '(''close'');']);
  
  set(lineH, 'ButtonDownFcn', [mfilename '(''marker'');']);
  % varargout{1} = fh;
  % varargout{2} = gh;
  ud.gh = gh;
  ud.markers = 0;
  ud.state = 'a'; % add points to tmpMia
  ud.addbutton = uiH(1);
  ud.movebutton = uiH(2);
  ud.deletebutton = uiH(3);
  ud.zoompopupmenu = uiH(10);
  ud.undobutton = uiH(4);
  ud.redobutton = uiH(9);
  ud.acceptbutton = uiH(5);
  ud.prevbutton = uiH(6);
  ud.nextbutton = uiH(7);
  ud.beamboxH = uiH(8);
  ud.lastMarkerMoved = []; % number of last marker moved

  % undo data has format
  % {beam x1 x2 oldYdata}, with a row for each accepted change. First
  % change is first row, last change last row.
  ud.undoStack = {};
  ud.redoStack = {};
  ud.zoomState = 'off';
  [PointerShapeCData PointerShapeHotSpot] = skullcursor;
  set(fh, 'UserData', ud, ...
	  'PointerShapeCData', PointerShapeCData, ...
	  'PointerShapeHotSpot', PointerShapeHotSpot);
  % update mode buttons to show true status
  feval(mfilename, 'keypress', ud.state, fh);

  switch instrumentCls
   case 'riometer'
    % update beam selection buttons
    feval(mfilename, 'beams', fh, ud.beam);
   case 'magnetometer'
    % update beam selection buttons
    feval(mfilename, 'components', fh, ud.component);
  end
  
  % reposition UI controls
  feval(mfilename, 'uireposition', fh);
  
  
  
  set(fh, 'Visible', 'on');
  

  switch lower(defaults.windowstyle)
   case 'normal'
    % return figure and axes handles, run in 'background mode'
    varargout{1} = fh;
    varargout{2} = gh;
    
   case 'modal'
    % stupid hack for matlab 5.3 & 6. Need to call again for some unknown
    % reason. 
    feval(mfilename, 'uireposition', fh);
    if isempty(zoomOnMenuH) 
      error('Cannot find zoom on menu');
    end
    waitfor(zoomOnMenuH); 
    ud = get(fh, 'UserData'); 

    % will have been changed by the callbacks, so reload
    varargout{1} = ud.curMia;
    
    if nargout > 1
      % indicate if changed
      warning('editted value may be wrong if data contains NaNs');
      varargout{2} = isequal(ud.curMia, ud.originalMia);
    end
    delete(fh);
			      
   otherwise
    % how did it get this far without crashing? Maybe Mathworks added
    % another option?
    error('Unknown window mode');
  end
  
  % --------------------------------------------------------------    
 case 'uireposition'
  % disp(action);
  % miaedit('uireposition')
  % miaedit('uireposition', fh)
  % reposition UI controls
  if nargin == 1
    fh = gcbf;
  else
    fh = varargin{1};
  end
  fhData = get(fh);
  % ud = get(fh, 'UserData');
  ud = fhData.UserData;
  % bbh = gethandles(getbeambox(ud.beamboxH)); % get handles of both objects
  
  % find position of header
  hiddenHandlesState = get(0, 'ShowHiddenHandles');
  set(0,'ShowHiddenHandles', 'on');
  headerH = findobj(fh, 'Type', 'axes', 'Tag', 'header');
  set(0,'ShowHiddenHandles', hiddenHandlesState);
  headerData = get(headerH); 
  
  % find position of plot axes
  plotData = get(ud.gh(1));
  
  % bbox = boundingbox([ud.addbutton bbh]');
  h = findobj(fh, 'Type', 'uicontrol');
  bbox = boundingbox(h);
  
  % find minimum height of figure 
  minFigHeight = bbox(4) + 2*ud.lineSpacing; % uicontrols plus spacing
  
  % assume top of header is at top of figure
  if strcmp(headerData.Units, 'normalized')
    % trickier,
    minFigHeight = minFigHeight / (1 - headerData.Position(4));
  else
    minFigHeight = minFigHeight + headerData.Position(4);
  end
  if fhData.Position(4) < minFigHeight
    % increase figure size, bring bottom of window down (not top up)
    fhData.Position(2) = fhData.Position(2) - ...
	(minFigHeight - fhData.Position(4));
    fhData.Position(4) = minFigHeight;
    yerror = ud.lineSpacing - bbox(2);
  else
    yerror = ((fhData.Position(4) - minFigHeight) / 2) - bbox(2) + ...
	     ud.lineSpacing;
  end
  % is figure wide enough?
  minFigWidth = bbox(3) * 2; % ok as a rule of thumb
  if fhData.Position(3) < minFigWidth
    % increase width about centre
    fhData.Position(1) = fhData.Position(1) - (minFigWidth - ...
					       fhData.Position(3))/2; 
    fhData.Position(3) = minFigWidth;
  end
  xerror = fhData.Position(3) - (bbox(1)+bbox(3) + ud.lineSpacing);
  % should plot window change width?
  if strcmp(plotData.Units, 'normalized')
    plotData.Position(3) = ((bbox(1) - ud.lineSpacing + xerror) / ...
			    fhData.Position(3)) - plotData.Position(1); 
  else
    plotData.Position(3) = bbox(1) - ud.lineSpacing + xerror - ...
	plotData.Position(1); 
  end
  set(fh, 'Position', fhData.Position);
  set(ud.gh, 'Position', plotData.Position);
  moveuicontrols(h, [xerror yerror], 'r');
  
  % --------------------------------------------------------------
 case 'marker'
  % disp(action);
  fh = gcbf;
  ca = gca; % callback axes
  ud = get(fh, 'UserData');
  selectionType = get(fh, 'SelectionType');
  % if strcmp(selectionType, 'alt');
  % don't do anything for a right click
  % return;
  % end
  newPoint = get(ca, 'CurrentPoint');
  newPoint = newPoint(1, 1:2);
  
  if ~isempty(ud.tmpMiaH)
    tmpX = get(ud.tmpMiaH, 'XData');
    tmpY = get(ud.tmpMiaH, 'YData');
    if newPoint(1) <= tmpX(1) | newPoint(1) >= tmpX(length(tmpX))
      fixToLine = 1;
    else
      fixToLine = 0;
    end
  else
    fixToLine = 1;
    tmpX = [];
    tmpY = [];
  end
  curX = get(ud.curMiaH, 'XData');
  curY = get(ud.curMiaH, 'YData');
  
  % this assumes XData is monotonically increasing
  if strcmp(selectionType, 'extend') | strcmp(selectionType, 'alt')
    % find point in XData closest to selected X
    newPoint = nearestPoint(curX, curY, newPoint, 'xonline');
  elseif fixToLine | ud.markers < 2 
    % find point in XData closest to selected X
    newPoint = nearestPoint(curX, curY, newPoint, 'xyonline');
  else
    % x value should be valid
    % newPoint = nearestPoint(tmpX, tmpY, newPoint, 'xy');
    newPoint = nearestPoint(curX, curY, newPoint, 'x');
  end
  
  switch ud.state
   case 'a'
    % add a marker
    switch (ud.markers)
     case 0
      ud.tmpMiaH = line('Parent', ca, ...
			'XData', newPoint(1), ...
			'YData', newPoint(2), ...
			'Marker', '+', ...
			'Color', 'r', ...
			'ButtonDownFcn', [mfilename '(''marker'');']);

      % disable prev/next beam, beambox and undo
      if strcmp(ud.instrumentCls, 'riometer')
	h = gethandles(getbeambox(ud.beamboxH));
      else
	h = [];
      end
      h = [h; 
	   ud.prevbutton; ud.nextbutton;
	   ud.undobutton; ud.undomenu; ud.selUndoMenu];
      set(h, 'Enable', 'off');
      
      
	   
      % enable move and delete buttons
      set([ud.movebutton ud.deletebutton], 'Enable', 'on');
     otherwise
      if isempty(find(tmpX == newPoint(1)))
	[tmpX sortIndex] = sort([tmpX newPoint(1)]);
	tmpY = [tmpY newPoint(2)];
	tmpY = tmpY(sortIndex);
	set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
      else
	% point already used
	% disp('point already used');
	return;
      end
    end
    ud.markers = ud.markers + 1;
   case 'd'
    % delete nearest marker
    [tmpPoint bestTmpXindex] = nearestPoint(tmpX, tmpY, newPoint, 'xy');
    tmpXlen = length(tmpX);
    if ud.markers == 0
      return ; % nothing to delete
    elseif ud.markers == 1
      feval(mfilename, 'deletetmpline', fh);
      return; % avoid updating later with old information
      
    elseif bestTmpXindex == 1
      % first point
      tmpX = tmpX(2:tmpXlen);
      tmpY = tmpY(2:tmpXlen);
      % set new end marker to be on curMia
      adjPoint = nearestPoint(curX, curY, [tmpX(1) tmpY(1)], 'xonline');
      tmpX(1) = adjPoint(1);
      tmpY(1) = adjPoint(2);
      
      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
      ud.markers = ud.markers - 1;
    elseif bestTmpXindex == tmpXlen
      % last point
      tmpXlen = tmpXlen - 1; % update length
      tmpX = tmpX(1:tmpXlen);
      tmpY = tmpY(1:tmpXlen); % tmpX and tmpY are now tmpXlen long
			      % set new end marker to be on curMia
			      adjPoint = nearestPoint(curX, curY, ...
						      [tmpX(tmpXlen) tmpY(tmpXlen)], 'xonline');
			      tmpX(tmpXlen) = adjPoint(1);
			      tmpY(tmpXlen) = adjPoint(2);
			      
			      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
			      ud.markers = ud.markers - 1;
    else
      tmpX = tmpX([1:(bestTmpXindex-1) (bestTmpXindex+1):tmpXlen]);
      tmpY = tmpY([1:(bestTmpXindex-1) (bestTmpXindex+1):tmpXlen]);
      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
      ud.markers = ud.markers - 1;
    end
   case 'm'
    % move point
    fh = gcbf;
    ud = get(fh, 'UserData');
    set(fh, 'WindowButtonMotionFcn', [mfilename '(''movemarker'');']);
    set(fh, 'WindowButtonUpFcn', [mfilename '(''movemarkerup'');']);
    
   otherwise
    error('unknown ud.state');
  end
  % disp(sprintf('ud.markers = %d', ud.markers));
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
 case 'keypress'
  % disp(action);
  % miaedit('keypress')
  % miaedit('keypress', key)
  % miaedit('keypress', key, fh)
  if nargin > 2
    fh = varargin{2};
  else
    fh = gcbf;
  end
  ca = gca; % callback axes
  ud = get(fh, 'UserData');
  if nargin > 1
    curChar = varargin{1};
  else
    curChar = lower(get(fh, 'CurrentCharacter'));
    % curChar = get(fh, 'CurrentCharacter');
    % double(curChar)
    % curChar = lower(curChar);
  end
  if ud.markers == 0
    set([ud.movebutton ud.deletebutton], 'Enable', 'off');
    curChar = 'a';
  else
    set([ud.movebutton ud.deletebutton], 'Enable', 'on');
  end
  switch curChar
   case 'a'
    ud.state = curChar;
    set(ud.addbutton, 'Value', 1);
    set([ud.movebutton ud.deletebutton], 'Value', 0);
   case 'm'
    % only switch into move state if number of markers is non-zero
    ud.state = curChar;
    set(ud.movebutton, 'Value', 1);
    set([ud.addbutton ud.deletebutton], 'Value', 0);

   case 'd'
    % only switch into delete state if number of markers is non-zero
    ud.state = curChar;
    set(ud.deletebutton, 'Value', 1);
    set([ud.addbutton ud.movebutton], 'Value', 0);
    
   case 'z'
    if strcmp(ud.zoomState, 'on')
      feval(mfilename, 'zoom', 'off', fh);
      % update pointer without requiring mouse movement
      feval(mfilename, 'alterpointer', fh);
    else
      feval(mfilename, 'zoom', 'on', fh);
    end
    % no change in state, don't save and don't fall through to zoom
    % again 
    return;
    
   case 'g'
    if strcmp(get(ud.gh, 'XGrid'), 'on')
      set(ud.gh, 'XGrid', 'off', 'YGrid', 'off', 'ZGrid', 'off');
    else
      set(ud.gh, 'XGrid', 'on', 'YGrid', 'on', 'ZGrid', 'on');
    end
    return;
    
   case ''
    % do nothing, not even beep (might be SHIFT, CTRL or ALT)
   otherwise
    disp(sprintf('\a'));
    return;
  end
  set(fh, 'UserData', ud);
  % turn off zoom, set pointer
  feval(mfilename, 'zoom', 'off', fh);
  % update pointer without requiring mouse movement
  feval(mfilename, 'alterpointer', fh);

  % --------------------------------------------------------------    
 case 'undo'
  % disp(action);
  fh = gcbf;
  ud = get(fh, 'UserData');
  undoRow = size(ud.undoStack,1);
  undoBeam = ud.undoStack{undoRow, 1};
  ud.curMia = setdata(ud.curMia, ...
		      getbeamindex(ud.curMia,ud.undoStack{undoRow,1}), ...
		      ud.undoStack{undoRow,2}:ud.undoStack{undoRow,3}, ...
		      ud.undoStack{undoRow,4});
  tmpProc = getprocessing(ud.curMia);
  % % remove the processing comment!
  % ud.curMia = setprocessing(ud.curMia, {tmpProc{1:(end-1)}});
  
  if undoRow == 1
    ud.undoStack = {};
    set([ud.undobutton ud.undomenu ud.selUndoMenu], 'Enable', 'off');
  else
    ud.undoStack = reshape({ud.undoStack{1:(undoRow-1), 1:4}}, ...
			   undoRow-1, 4);
  end
  set(fh, 'UserData', ud);
  % switch to correct beam
  feval(mfilename, 'beams', fh, undoBeam); % this changes UserData
  
  % reload updated UserData
  % ud = get(fh, 'UserData');
  
  
  % --------------------------------------------------------------    
 case 'accept'
  % disp(action);
  fh = gcbf;
  ud = get(fh, 'UserData');
  if ud.markers == 0
    return;
  elseif ud.markers == 1
    feval(mfilename, 'deletetmpline', fh);
    return;
  end
  
  pointer = get(fh, 'Pointer');
  set(fh, 'Pointer', 'watch');
  % YData may not represent true data vlaues, it may have been scaled
  [tmp mult] = datalabel2(ud.curMia, 'parameterindex', 1);
  if isempty(mult)
    mult = 1;
  else
    mult = 10^mult;
  end
  tmpX = get(ud.tmpMiaH, 'XData');
  tmpY = get(ud.tmpMiaH, 'YData') .* mult;
  curX = get(ud.curMiaH, 'XData');
  curY = get(ud.curMiaH, 'YData') .* mult;
  tmpXlen = length(tmpX);
  
  
  % next valid X value to right of leftmost marker
  x1 = find(curX == tmpX(1)) + 1;
  % next valid X value to left of rightmost marker
  x2 = find(curX == tmpX(tmpXlen)) - 1;

  % test if end points are adjacent (easy - no interpolation needed)
  if x1 == x2 + 1
    feval(mfilename, 'deletetmpline', fh);
    set(fh, 'Pointer', pointer);
    return;
  end
  
  param = getfield(ud, ud.parameterName);
  % add to undo information first
  if isempty(ud.undoStack)
    ud.undoStack = {param x1 x2 curY(x1:x2)};
  else
    undoRows = size(ud.undoStack,1);
    ud.undoStack{undoRows + 1, 1} = param;
    ud.undoStack{undoRows + 1, 2} = x1;
    ud.undoStack{undoRows + 1, 3} = x2;
    ud.undoStack{undoRows + 1, 4} = curY(x1:x2);
  end
  set([ud.undobutton ud.undomenu ud.selUndoMenu], 'Enable', 'on');

  if 0
    % do interpolation
    interpmethod = 'linear';
    curY(x1:x2) = interp1(tmpX, tmpY, curX(x1:x2), interpmethod);
    set(ud.curMiaH, 'YData', curY);
    ud.curMia = setdata(ud.curMia, getbeamindex(ud.curMia, param), ...
				   x1:x2, curY(x1:x2));
  else
    % do filtering (new method)
    filNum = get(ud.filterMenuH, 'Value');
    res = getresolution(ud.curMia);
    if isa(ud.curMia, 'rio_qdc_base')
      % work in timespans if a QDC
      miaSt = timespan(0, 's');
      miaEt = floor(siderealday, res);
    else
      miaSt = getstarttime(ud.curMia);
      miaEt = getendtime(ud.curMia);
    end
    residue = getresidue(ud.defaults.filters{filNum}, ud.curMia);
    st = max(miaSt, miaSt + res * (x1 - residue(1)));
    et = min(miaEt, miaSt + res * (x2 + residue(2)));

    if maxconstraints(ud.defaults.filters{filNum}) > 0
      % remove any old constraints
      ud.defaults.filters{filNum} = clearconstraints(ud.defaults.filters{filNum});
      % add the new constraints
      for constrNum = 1:tmpXlen
	% addconstraints needs the X value as a time
	% ud.defaults.filters{filNum} = ...
	%    addconstraints(ud.defaults.filters{filNum}, st + ...
	%		   ((tmpX(constrNum) - curX(1)) * res), ...
	%		   tmpY(constrNum));

        ud.defaults.filters{filNum} = ...
	    addconstraints(ud.defaults.filters{filNum}, ...
                           timespan('cdfepoch', tmpX(constrNum)), ...
			   tmpY(constrNum));

      end
      % error('Filtering for interp not implemented yet');
    end
    ud.curMia
    param
    st, et
    % global FILTER; FILTER = ud.defaults.filters{filNum}
    % global DATA; DATA = filteredMia;

    % filteredMia = extract(ud.curMia, param, st, et);
    filteredMia = extract(ud.curMia, ...
			  'parameters', param, ...
			  'starttime', st, ...
			  'endtime', et);
    filteredMia = filter(ud.defaults.filters{filNum}, filteredMia);
    ud.curMia = insert(ud.curMia, filteredMia);

    ud.curMia = addprocessing(ud.curMia, ...
			      getprocessing(ud.defaults.filters{filNum}, ud.curMia));

  end
  set(fh, 'UserData', ud);
  feval(mfilename, 'deletetmpline', fh);
  
  feval(mfilename, [ud.parameterName 's'], fh, param);  % refresh plot

  set(fh, 'Pointer', pointer);
  return;
  
  % --------------------------------------------------------------    
 case 'previous'
  % disp(action);
  fh = gcbf;
  ud = get(fh, 'UserData');
  beam = ud.curBeams(find(ud.curBeams == ud.beam) - 1);
  feval(mfilename, 'beams', fh, beam);
  
  % --------------------------------------------------------------    
 case 'next'
  % disp(action);
  fh = gcbf;
  ud = get(fh, 'UserData');
  beam = ud.curBeams(find(ud.curBeams == ud.beam) + 1);
  feval(mfilename, 'beams', fh, beam);

  % --------------------------------------------------------------    
 case 'beams'
  % disp(action);
  % miaedit('beams');
  % miaedit('beams', fh);    
  % miaedit('beams', fh, newbeam);    
  if nargin > 1
    fh = varargin{1};
  else
    fh = getparentfigure(getbeambox(gcbo));
  end
  ud = get(fh, 'UserData');
  bbox = getbeambox(ud.beamboxH);
  if nargin > 2
    beam = varargin{2};
  else
    beam = getbeams(bbox);
  end
  
  if isempty(beam)
    % no beam selected
    disp(sprintf('\a'));
    % setbeams(bbox, ud.beam);
    setbeams(bbox, 'beams', ud.beam);
    return;
  elseif length(beam) > 1
    % multiple beams selected
    errordlg('Please select only one beam', 'Error', ...
	     'modal');
    % setbeams(bbox, ud.beam);
    setbeams(bbox, 'beams', ud.beam);
    return;
  else 
    % change beam
    if ud.beam == beam
      % same beam, so preserve x/y limits
      xlim = get(ud.gh(1), 'XLim');
      ylim = get(ud.gh(1), 'YLim');
    end

    [tmp1 tmp2 ud.curMiaH] = plot(ud.curMia, 'plotaxes', ud.gh(1), ...
				  'beams', ud.beam);
    ud.curMiaH = ud.curMiaH(1); % real line is the first handle
    if ~isempty(ud.refMia)
      np = get(ud.gh(1), 'NextPlot');
      set(ud.gh(1), 'NextPlot', 'add')
      [tmp1 tmp2 ud.refMiaH] = plot(ud.refMia, 'plotaxes', ud.gh(1), ...
				    'beams', ud.beam, ...
				    'color', ud.defaults.referencecolor);
      set(ud.gh(1),'NextPlot', np)
    end

    set(findobj(fh, 'Type', 'text','Tag', 'title'), ...
	'String', maketitle(ud.curMia, 'customstring', 'MIA Data Editor', ...
			    'beams', ud.beam)); 
    
    set(ud.curMiaH, 'ButtonDownFcn', [mfilename '(''marker'');']);
    % switch to addpoints mode
    set(fh, 'UserData', ud); % NB update NOW!
    feval(mfilename, 'keypress', 'a', fh);
    ud = get(fh, 'UserData'); % re-read!
    if ud.beam == beam
      % same beam, so preserve x/y limits
      set(ud.gh(1), 'XLim', xlim);
      set(ud.gh(1), 'YLim', ylim);
    else
      ud.beam = beam;
      % if nargin > 2
      %   setbeams(bbox, ud.beam); % update beambox for when prev/next used
      % end
    end
    % setbeams(bbox, ud.beam); % update beambox for when prev/next used
    setbeams(bbox, 'beams', ud.beam); % update beambox for when prev/next used


    % check setting of prev/next beam buttons
    if ud.beam == ud.curBeams(1)
      set(ud.prevbutton, 'Enable', 'off');
    else
      set(ud.prevbutton, 'Enable', 'on');
    end
    if ud.beam == ud.curBeams(end)      
      set(ud.nextbutton, 'Enable', 'off');
    else
      set(ud.nextbutton, 'Enable', 'on');
    end
    
    set(fh, 'UserData', ud);
  end

  % --------------------------------------------------------------    
 case 'components'
  if nargin > 1
    fh = varargin{1};
    component = varargin{2};
    ud = get(fh, 'UserData');
  else
    fh = gcbf;
    ud = get(fh, 'UserData');
    component = ud.component;
  end
  
  if ud.component == component
    % same component, so preserve x/y limits
    xlim = get(ud.gh(1), 'XLim');
    ylim = get(ud.gh(1), 'YLim');
  end
  
  ZZZ = ud.curMia
  delete(findall(ud.gh(1), 'Type', 'line'));
  [tmp1 tmp2 ud.curMiaH] = plot(ud.curMia, 'plotaxes', ud.gh(1), ...
				'components', ud.component);
  if ~isempty(ud.refMia)
    np = get(ud.gh(1), 'NextPlot');
    set(ud.gh(1), 'NextPlot', 'add')
    [tmp1 tmp2 ud.refMiaH] = plot(ud.refMia, 'plotaxes', ud.gh(1), ...
				  'components', ud.component, ...
				  'color', ud.defaults.referencecolor);
    set(ud.gh(1),'NextPlot', np)
  end

  % update title
  set(findobj(fh, 'Type', 'text','Tag', 'title'), ...
      'String', maketitle(ud.curMia, 'customstring', 'MIA Data Editor', ...
			  'component', ud.component)); 

  set(ud.curMiaH, 'ButtonDownFcn', [mfilename '(''marker'');']);
  % switch to addpoints mode
  set(fh, 'UserData', ud); % NB update NOW!
  feval(mfilename, 'keypress', 'a', fh);

  component = ud.component;
  
  ud = get(fh, 'UserData'); % re-read!
  if ud.component == component
    % same component, so preserve x/y limits
    set(ud.gh(1), 'XLim', xlim);
    set(ud.gh(1), 'YLim', ylim);
  else
    ud.component = component;
  end
  
  
  % check setting of prev/next buttons
  if ud.component == ud.curComponents{1}
    set(ud.prevbutton, 'Enable', 'off');
  else
    set(ud.prevbutton, 'Enable', 'on');
  end
  if ud.component == ud.curComponents{end}
    set(ud.nextbutton, 'Enable', 'off');
  else
    set(ud.nextbutton, 'Enable', 'on');
  end
  
  set(fh, 'UserData', ud);



  % --------------------------------------------------------------    
 case 'movemarker'
  % disp(action);
  fh = gcbf;
  ud = get(fh, 'UserData');

  curX = get(ud.curMiaH, 'XData');
  curY = get(ud.curMiaH, 'YData');
  tmpX = get(ud.tmpMiaH, 'XData');
  tmpY = get(ud.tmpMiaH, 'YData');

  point = get(gca, 'CurrentPoint');
  point = point(1,1:2);
  % find nearest marker
  tmpXlen = length(tmpX);
  [tmpPoint bestIndex] = nearestPoint(tmpX, tmpY, point, 'x');
  if isempty(ud.lastMarkerMoved)
    % first call to this function since a marker was selected, remember
    % marker
    ud.lastMarkerMoved = bestIndex;
    set(fh, 'UserData', ud);
    % set([ud.tmpMiaH ud.curMiaH ud.refMiaH], 'EraseMode', 'xor');
    set([ud.tmpMiaH], 'EraseMode', 'xor');	  
    set([ud.curMiaH(:); ud.refMiaH(:)], 'EraseMode', 'none');
  else
    bestIndex = ud.lastMarkerMoved;
  end
  
  if bestIndex == 1
    % first point
    [curPoint bestCurIndex] = nearestPoint(curX, curY, point, ...
					   'xyonline'); 
    if ud.markers == 1
      tmpX(1) = curPoint(1);
      tmpY(1) = curPoint(2);
      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
    elseif curPoint(1) < tmpX(2) & curPoint(1) >= curX(1)
      % only update when left of marker #2
      tmpX(1) = curPoint(1);
      tmpY(1) = curPoint(2);
      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
    end
  elseif bestIndex == tmpXlen
    % last point
    [curPoint bestCurIndex] = nearestPoint(curX, curY, point, ...
					   'xyonline'); 
    if curPoint(1) > tmpX(tmpXlen-1) & curPoint(1) <= curX(end)
      tmpX(tmpXlen) = curPoint(1);
      tmpY(tmpXlen) = curPoint(2);
      set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);      
    end
  else
    % other point
    selectionType = get(fh, 'SelectionType');
    if strcmp(selectionType, 'normal')
      [curPoint bestCurIndex] = nearestPoint(curX, curY, point, ...
					     'x'); 
    else
      [curPoint bestCurIndex] = nearestPoint(curX, curY, point, ...
					     'xyonline');
    end

    % set X coordinate
    if curPoint(1) <= tmpX(ud.lastMarkerMoved-1)
      % left of left marker, set to point just to right of left marker 
      % nextBestCurIndex is index into curX so that marker is not moved
      % over or left of marker to left
      onLineCurIndex = find(curX == tmpX(ud.lastMarkerMoved-1))+1;
      tmpX(ud.lastMarkerMoved) = curX(onLineCurIndex);
      % curX(find(curX == tmpX(ud.lastMarkerMoved-1))+1);
    elseif curPoint(1) >= tmpX(ud.lastMarkerMoved+1) 
      % right of right
      % marker, set to point just to left of right marker.
      % onLineCurIndex is index into curX so that marker is not moved
      % over or left of marker to left
      onLineCurIndex = find(curX == tmpX(ud.lastMarkerMoved+1))-1;
      tmpX(ud.lastMarkerMoved) = ...
	  curX(onLineCurIndex);
    else
      % between markers
      tmpX(ud.lastMarkerMoved) = curPoint(1);
      onLineCurIndex = bestCurIndex; 
    end
    % set Y coordinate. If supposed to be on line remember to use
    % X coord  of marker, not of mouse!
    if strcmp(selectionType, 'normal')
      tmpY(ud.lastMarkerMoved) = tmpPoint(2);
    else
      tmpY(ud.lastMarkerMoved) = curY(onLineCurIndex);	
    end	
    set(ud.tmpMiaH, 'XData', tmpX, 'YData', tmpY);
  end
  

  
  % --------------------------------------------------------------    
 case 'movemarkerup'
  % disp(action);
  % button up after movemarker
  fh = gcbf;
  ud = get(fh, 'UserData');
  ud.lastMarkerMoved = []; % allow a different marker to be moved
  set(fh, 'WindowButtonUpFcn', '', ...
	  'WindowButtonMotionFcn', [mfilename '(''alterpointer'');'], ...  
	  'UserData', ud);
  % set([ud.tmpMiaH ud.curMiaH ud.refMiaH], 'EraseMode', 'normal');
  set([ud.tmpMiaH(:); ud.curMiaH(:); ud.refMiaH(:)], 'EraseMode', 'normal');

  % --------------------------------------------------------------    
 case 'deletetmpline'
  % disp(action);
  % miaedit('deletetmpline', fh);
  % delete the temporary line and update buttons, number of markers
  % etc. This function loads the userdata from fh, so remember to save
  % any changes into UserData before calling this function!    
  fh = varargin{1};
  ud = get(fh, 'UserData');
  delete(ud.tmpMiaH);
  ud.tmpMiaH = [];
  ud.markers = 0;
  ud.state = 'a'; % switch to add markers state
  
  switch ud.instrumentCls
   case 'riometer'
    % enable beambox
    set(gethandles(getbeambox(ud.beamboxH)), 'Enable', 'on');
    
    % check before setting prev/next beam buttons
    if ud.beam ~= ud.curBeams(1)
      set(ud.prevbutton, 'Enable', 'on');
    end
    if ud.beam ~= ud.curBeams(end)
      set(ud.nextbutton, 'Enable', 'on');
    end
    
   case 'magnetometer'
    % check before setting prev/next beam buttons
    if ~isequal(ud.component, ud.curComponents{1})
      set(ud.prevbutton, 'Enable', 'on');
    end
    if ~isequal(ud.component, ud.curComponents{end})
      set(ud.nextbutton, 'Enable', 'on');
    end
 
  end

  if ~isempty(ud.undoStack)
    set([ud.undobutton ud.undomenu ud.selUndoMenu], 'Enable', 'on');
  end
  set(fh, 'UserData', ud);
  
  % update mode buttons to show true status
  feval(mfilename, 'keypress', 'a', fh);
  return;
  
  % --------------------------------------------------------------    
 case 'sel'
  % disp(action);
  % miaedit('sel', 'undo')
  % miaedit('sel', 'redo')
  % stack is the undo or redo stack
  
  fh = gcbf;
  cbo = gcbo;
  ud = get(fh, 'UserData');
  if strcmp(varargin{1}, 'undo')
    stack = ud.undoStack;
  else
    stack = ud.redoStack;
  end
  
  % draw selection options as a menu
  
  
  % --------------------------------------------------------------    
 case 'dosel'
  % disp(action);
  % miaedit('dosel', 'undo', event);
  % miaedit('dosel', 'redo', event);

  % Do selective undo/redo. Consider undo. Find the event n on the stack
  % and put its undo information back into the curMia object, at the right
  % place. This, however, may undo later so changes. First check that any
  % of the modified data was not subsequently modified. If it was don't
  % put it back (it undoes later changes). Instead, modify the undo
  % information of the later event with the undo data of the earlier
  % event. Then if the second event is also undone the data prior to
  % the first event is restored. 
  
  fh = gcbf;
  ud = get(fh, 'UserData');
  if strcmp(varargin{1}, 'undo')
    stack = ud.undoStack;
  else
    stack = ud.redoStack;
  end
  stackLen = size(stack, 1);
  event = varargin{2};
  beam = stack{event,1};
  % times for which data was modified
  eventX1 = stack{event,2};
  eventX2 = stack{event,3};
  eventXData = eventX1:eventX2;
  eventYData = stack{beam,4};
  for n = (event+1):stackLen
    if stack{n,1} == beam
      % beams match, further checks needed
      tmpX1 = stack{n,2};
      tmpX2 = stack{n,3};
      tmpXData = tmpX1:tmpX2;
      commonXtmp = intersect(eventXData, tmpXData);
      if ~isempty(commonXtmp)
	% update undo data of later event
	tmpYData = stack{n,4}
	tmpYData(commonXtmp -tmpX1 + 1) = ...
	    eventYData(commonXtmp - eventX1 + 1);
	% put back into stack
	stack{n, 4} = tmpYData;
	
	% remove the common times from eventXData
	eventXData = setdiff(eventXData, commonXtmp);
      end
    end
  end
  % Any times which were not modified after event are in eventXData,
  % update the original data and replot.
  if ~isempty(eventXData)
    ud.curMia = setdata(ud.curMia, getbeamindex(ud.curMia,beam), ...
				   eventXData, eventYData(eventXData - eventX1 + 1));
  end
  
  % update stack and put stack back into UserData
  stack = reshape({stack{[1:(event-1) (event+1):stackLen], ...
		    1:4}}, stackLen-1, 4);
  if strcmp(varargin{1}, 'undo')
    ud.undoStack = stack;
    if isempty(stack)
      set([ud.undobutton ud.undomenu ud.selUndoMenu], 'Enable', 'off');
    end
  else
    ud.redoStack = stack;
    if isempty(stack)
      set([ud.redobutton ud.redomenu ud.selUndoMenu], 'Enable', 'off');
    end
  end
  set(fh, 'UserData', ud);
  % switch to correct beam
  feval(mfilename, 'beams', fh, beam); % this changes UserData
  
  return;
  % --------------------------------------------------------------
 case 'draweditmenus'
  % disp(action);
  fh = gcbf;
  cbo = gcbo;
  % set(cbo, 'Visible', 'off');
  ud = get(fh, 'UserData');
  delete([ud.selUndoMenuKids; ud.selRedoMenuKids]);
  ud.selUndoMenuKids = ...
      drawEditMenus(ud.selUndoMenu, ud.undoStack, 'undo', ud.curMia);
  if isempty(ud.selUndoMenuKids)
    set(ud.selUndoMenu, 'Enable', 'off');
  else
    set(ud.selUndoMenu, 'Enable', 'on');
  end
  ud.selRedoMenuKids = ...
      drawEditMenus(ud.selRedoMenu, ud.redoStack, 'redo', ud.curMia);
  if isempty(ud.selUndoMenuKids)
    set(ud.selRedoMenu, 'Enable', 'off');
  else
    set(ud.selRedoMenu, 'Enable', 'on');
  end
  % set(cbo, 'Visible', 'on');
  % 
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------    
 case 'zoom'
  % disp(action);
  % miaedit('zoom', zoomaction);
  % miaedit('zoom', zoomaction, fh);
  % This action DOES modify the figure's UserData. This action does not
  % immediately alter the pointer, but sets the WindowButtonMotionFcn
  % callback to do so when the mouse moves. This is enough for the
  % menus and buttons. When the zoom status is changed by a keypress
  % the pointer ought to be changed manually with the 'alterpointer'
  % action. 
  zoomaction = varargin{1};
  if nargin == 2
    fh = gcbf;
  else
    fh = varargin{2};
  end
  ud = get(fh, 'UserData');
  axes(ud.gh(1));
  if strcmp(zoomaction, '?')
    % find out (called from popupmenu)
    zoomaction = popupstr(gcbo); % something like 'Zoom in'
    zoomaction = zoomaction(6:end)
  end
  switch zoomaction
   case 'on'
    setpopupmenu(ud.zoompopupmenu, ['Zoom ' zoomaction]);
    ud.zoomState = zoomaction;
    set(fh, 'UserData', ud, 'Pointer', pointer.normal, ...
	    'WindowButtonMotionFcn', '');
    zoom(fh, zoomaction);
    % disable callbacks on lines and axis
    % set([ud.tmpMiaH ud.curMiaH ud.refMiaH ud.gh], 'ButtonDownFcn',
    % ''); 
    set([ud.tmpMiaH(:); ud.curMiaH(:); ud.refMiaH(:); ud.gh(:)], ...
	'ButtonDownFcn', ''); 
    return;
    
   case 'off'
    setpopupmenu(ud.zoompopupmenu, ['Zoom ' zoomaction]);
    zoom(fh, zoomaction);
    ud.zoomState = zoomaction;

    % update and return pointer to proper value via movement callback
    set(fh, 'UserData', ud, ...
	    'WindowButtonMotionFcn', [mfilename '(''alterpointer'');']);
    
    % enable callbacks on lines and axis
    % set([ud.tmpMiaH ud.curMiaH ud.refMiaH ud.gh], ...
    % ...
    set([ud.tmpMiaH(:); ud.curMiaH(:); ud.refMiaH(:); ud.gh(:)], ...
	'ButtonDownFcn', [mfilename '(''marker'');']); 
    return;
    
   case 'in'
    setpopupmenu(ud.zoompopupmenu, ['Zoom ' ud.zoomState]);
    figure(fh);
    zoom(2);
    return;

   case 'out'
    setpopupmenu(ud.zoompopupmenu, ['Zoom ' ud.zoomState]);
    figure(fh);
    zoom(0.5);
    return;

   case 'limits'
    setpopupmenu(ud.zoompopupmenu, ['Zoom ' ud.zoomState]);
    figure(fh);
    zoom(fh, 'out');
    return;
    
   otherwise
    warning(['Unknown zoom action (was ' zoomaction ')']);
    return;
  end
  % --------------------------------------------------------------    
 case 'alterpointer'
  % disp(action);
  % miaedit('alterpointer');
  % miaedit('alterpointer', fh);
  if nargin == 1
    fh = gcbf;
  else
    fh = varargin{1};
  end
  ud = get(fh, 'UserData');
  point = get(ud.gh(1), 'CurrentPoint');
  ghData = get(ud.gh(1));
  if ghData.CurrentPoint(1,1) < ghData.XLim(1) ...
	| ghData.CurrentPoint(1,1) > ghData.XLim(2) ...
	| ghData.CurrentPoint(1,2) < ghData.YLim(1) ...
	| ghData.CurrentPoint(1,2) > ghData.YLim(2) 
    % outside of axes
    set(fh, 'Pointer', pointer.normal);
  elseif ud.state(1) == 'a'
    set(fh, 'Pointer', pointer.add);
  elseif ud.state(1) == 'm' 
    set(fh, 'Pointer', pointer.move);
  else
    % delete mode or an error, skull and cross-bones sounds appropriate
    % for either! 
    set(fh, 'Pointer', pointer.delete);
  end
  
  % --------------------------------------------------------------
 case 'selfilter'
  fh = gcbf;
  ud = get(fh, 'UserData');

  % --------------------------------------------------------------
 case 'configfilter'
  fh = gcbf;
  ud = get(fh, 'UserData');
  filNum = get(ud.filterMenuH, 'Value');
  ud.defaults.filters{filNum} = configure(ud.defaults.filters{filNum}, ud.curMia);
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
 case 'closeOLD'
  % disp(action);
  % miaedit('close');
  % miaedit('close', fh);
  if nargin == 1
    % fh = gcbf;
    shh = get(0, 'ShowHiddenHandles');
    set(0, 'ShowHiddenHandles', 'on');
    fh = get(0, 'CurrentFigure');
    set(0, 'ShowHiddenHandles', shh);
  else
    fh = varargin{1};
  end
  ud = get(fh, 'UserData');
  st = getstarttime(ud.curMia);
  res = getresolution(ud.curMia);
  
  % Would be nice to compress edits so that time edits which are
  % subsets of bigger edits are not listed. Don't worry for now.
  for n = 1:size(ud.undoStack, 1)
    x1t = st + (ud.undoStack{n, 2} - 1)*res;
    x2t = st + (ud.undoStack{n, 3} - 1)*res;
    ud.curMia = ...
	addprocessing(ud.curMia, ...
		      sprintf('edited beam % 2d, %s', ud.undoStack{n,1}, ...
			      dateprintf(x1t, x2t)));
  end
  
  if isequal(ishandle(ud.defaults.fighandle), 1)
    % update in calling figure
    feval(ud.defaults.updatefcn{:}, ud.curMia);
  else
    % existing window must have been closed - create a new one
    newfh = miashow('init', ud.curMia);
  end
  delete(fh);
  % delete(get(0,'CurrentFigure'));
  
  % --------------------------------------------------------------
 case {'ok' 'cancel'}
  % return the modified data (okmodal) or the original data (cancelmodal)
  % miaedit(ok|cancel, normal|modal);
  % miaedit(ok|cancel, normal|modal, fh);
  switch nargin
   case 2
    fh = gcbf;
   case 3
    fh = varargin{2};
   otherwise
    error('incorrect parameters');
  end
  wm = varargin{1};

  if strcmp(action, 'cancel')
    % revert original data
    ud = get(fh, 'UserData');
    ud.curMia = ud.originalMia;
    % clear editting history so that it isn't copied as processing
    % actions
    ud.undoStack = {}; 
    set(fh, 'UserData', ud);
  end
  
  switch wm
   case 'normal'
    delete(fh);
    
   case 'modal'
    % destroy the zoom on menu
    zoomOnMenuH = findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_on');
    if isempty(zoomOnMenuH) 
      warning('Cannot find zoom on menu');
    end
    delete(zoomOnMenuH);
    % let the waitfor complete, and let that close the window

   otherwise
    error('unknown window mode');
  end
  
  
  % --------------------------------------------------------------
 case 'modalcloseOLD'
  % miaedit('modalclose');
  % miaedit('modalclose', fh);
  if nargin == 1
    % fh = gcbf;
    shh = get(0, 'ShowHiddenHandles');
    set(0, 'ShowHiddenHandles', 'on');
    fh = get(0, 'CurrentFigure');
    set(0, 'ShowHiddenHandles', shh);
  else
    fh = varargin{1};
  end
  % destroy the zoom on menu
  zoomOnMenuH = findobj(fh, 'Type', 'uimenu', 'Tag', 'zoom_on');
  if isempty(zoomOnMenuH) 
    warning('Cannot find zoom on menu');
  end
  delete(zoomOnMenuH);
  % let the waitfor complete, and let that close the window
 
  
  % --------------------------------------------------------------
 case 'close'
  % miaedit('close');
  % miaedit('close', fh);
  if nargin == 1
    % fh = gcbf;
    shh = get(0, 'ShowHiddenHandles');
    set(0, 'ShowHiddenHandles', 'on');
    fh = get(0, 'CurrentFigure');
    set(0, 'ShowHiddenHandles', shh);
  else
    fh = varargin{1};
  end

  ud = get(fh, 'UserData');
  if isequal(ud.curMia, ud.originalMia)
    % no changes made, so just return
    feval(mfilename, 'ok', ud.defaults.windowstyle, fh);
    return
  end
  
  opt = {'Save', 'Don''t save', 'Cancel'};
  button = questdlg('The data has changed. Do you to save the changes?', ...
		    'Confirm changes', ...
		    opt{[1:3 3]})
  bn = find(strcmp(button, opt));
  if bn == 1
    % ok
    feval(mfilename, 'ok', ud.defaults.windowstyle, fh);
    return
  elseif bn == 2
    feval(mfilename, 'cancel', ud.defaults.windowstyle, fh);
    return
  end
  
  % cancel pressed, do nothing
  return

  % --------------------------------------------------------------
 case 'exit'
  % disp(action);
  % miaedit('exit');
  % miaedit('exit', fh);
  if nargin == 1
    fh = gcbf;
  else
    fh = varargin{1};
  end
  delete(fh);
  
  % --------------------------------------------------------------           
 case 'save'
  % disp(action);
  
  % --------------------------------------------------------------    
  % --------------------------------------------------------------    
 otherwise
  error(['unknown action (was ''' action ''')']);
  
end
return

% --------------------------------------------------------------    
% --------------------------------------------------------------    
function varargout = nearestPoint(xData, yData, point, metric)
%NEARESTPOINT Find the nearest point on the line.
%
%   newPoint = nearestPoint(xData, yData, point, metric)
%   [newPoint bestXindex] = nearestPoint(xData, yData, point, metric)
%
%   The line is defined by (xData, yData). Point and newPoint are [2x1]
%   matrices. metric should be one of 'x' or 'xy'. 'x' signifies use nearest
%   X value, 'xy' means nearest point in Euclidean space.

newPoint = [];
bestIndex = [];
if ~isempty(xData)
  switch metric
    % nearest x point, y is preserved
   case 'x'
    difference = abs(xData - point(1));
    bestIndex = find(difference == min(difference));
    bestIndex = bestIndex(1);
    newPoint = [xData(bestIndex) point(2)];
    
    % nearest x point, y is on the line
   case 'xonline'
    difference = abs(xData - point(1));
    bestIndex = find(difference == min(difference));
    bestIndex = bestIndex(1);
    newPoint = [xData(bestIndex) yData(bestIndex)];
    
    % absolute nearest point (ie in Euclidean space)
   case 'xy'
    difference = power(xData - point(1),2) + ...
	power(yData - point(2),2);      
    bestIndex = find(difference == min(difference));
    bestIndex = bestIndex(1);
    newPoint = [xData(bestIndex) yData(bestIndex)];

    % nearest point (ie in Euclidean space), but with restriction must be
    % on line
   case 'xyonline'
    difference = power(xData - point(1),2) + ...
	power(yData - point(2),2);      
    bestIndex = find(difference == min(difference));
    bestIndex = bestIndex(1);
    newPoint = [xData(bestIndex) yData(bestIndex)];

   otherwise
    error('unknown metric');
  end
  
end
if nargout >= 1
  varargout{1} = newPoint;
end
if nargout >= 2
  varargout{2} = bestIndex;
end
return

% --------------------------------------------------------------    
function r = drawEditMenus(menuH, stack, undoRedo, mia)
% r: handles of menu objects created
% menuH: handle of menu to attach list to
% stack the undo or redo stack
% undoRedo: 'undo' or 'redo', used for contstructing the Callback
% function 
r = zeros(size(stack, 1), 1);
for n = 1:size(stack, 1)
  st = getstarttime(mia) + ...
       (stack{n,2}-1)*getresolution(mia);
  et = getstarttime(mia) + ...
       stack{n,3}*getresolution(mia); 
  dateStr = dateprintf(st, et);
  r(n) = uimenu(menuH, ...
		'Label', sprintf('beam %d: %s', stack{n,1}, dateStr), ...
		'Callback', sprintf('%s(''dosel'',''%s'',%d)', ...
				    mfilename, undoRedo, n));
end
return


% --------------------------------------------------------------    
function r = localExtractFromQdc(mia, beam, st, et)

p = localCopyQdcParameters(mia)
% times to extract
et2 = p.starttime + et;
st2 = p.starttime + st;
p.sampletime = p.starttime + p.sampletime;
pl = makeprop(p);
if isa(mia, 'rio_rawqdc')
  r = rio_rawpower(pl{:});
elseif isa(mia, 'rio_qdc')
  r = rio_power(pl{:});
else
  error('incorrect parameters');
end

% r = extract(r, beam, st2, et2);
r = extract(r, 'beams', beam, 'starttime', st2, 'endtime', et2);
return

% --------------------------------------------------------------    
function r = localInsertIntoQdc(mia, mia2)
% copy all data samples from mia2 into corresponding data samples of mia
sampletime = getsampletime(mia);
sampletime2 = getsampletime(mia2) - getstarttime(mia);

% find the sample times in mia2 which are in mia, and where they are
[tf loc] = ismember(sampletime2, sampletime);
r = mia;
r = setdata(r, getdata(mia2, ':', find(tf)), ':', loc);
assignin('base', 'Qr', r);
return

% --------------------------------------------------------------    
function r = localCopyQdcParameters(mia)
f = {'starttime' 'endtime' 'instrument', ...
     'sampletime' 'integrationtime'  ...
     'units' 'data' 'beams' 'processing'};
r = [];
for n = 1:length(f)
  r = setfield(r, f{n}, feval(['get' f{n}], mia));
end
r.load = 0;
r.log = 0;

return











