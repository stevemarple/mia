function seb = getstetbox(handle)
%GETSTETBOX  Return a STETBOX (starttime/endtime box) from a handle.
%
%   seb = GETSTETBOX(h)
%   seb: STETBOX object
%   h: handle associated with a STETBOX
%
%   See also STETBOX.

if ishandle(handle)
  handle = double(handle);
end

switch class(handle)
 case 'stetbox'
  % already is a seb!
  % warning('this already is a stetbox');
  seb = handle;
  return;

 case 'double'
  if ishandle(handle)
    seb = get(handle, 'UserData');
    if strcmp(class(seb), 'stetbox')
      return;
    else
      seb = getstetbox(seb);
      return;
    end
  else
    handle
    error('Not a valid handle');
  end
  
 otherwise
  % some other class which is storing our stetbox
  % disp(sprintf('handle class %s', class(handle)));
  seb = getstetbox(getuserdata(handle));
  return;
end


