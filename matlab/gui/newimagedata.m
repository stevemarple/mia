function varargout = newimagedata(action, varargin)
%NEWIMAGEDATA Make a menu for creating RIO_BASE objects.
%
%   NEWIMAGEDATA
%   Create and initialise (same as NEWIMAGEDATA('init')). 
%
%   NEWIMAGEDATA(action)
%
%   Perform spcified action (used by callbacks).
%
%   NEWIMAGEDATA('init', 'rio_abs')
%   NEWIMAGEDATA('init', 'rio_power')
%   Make for creating RIO_ABS and RIO_POWER objects respectively.

% The checking of the data could happen after any of the items on this menu
% have been altered, by calling this function as part of the uiobjects'
% callbacks. I think that will be too slow, so perform the checking only
% when ok is pressed. It also gives the user a chance to correct any
% mistakes without being 'shouted at'. Give an error dialog box explaining
% the error, and hopefully with a button for help.
%

badColor = [0.8 0 0];

if nargin == 0
  action = 'init';
end


switch action
  % --------------------------------------------------------------
 case 'init'
  % valid datatypes
  defaults.instrumenttypes = mia_image_instruments;
  defaults.instrumenttype = defaults.instrumenttypes{1};
  
  defaults.qdc = [];  % QDC to use for absorption when in manual QDC mode
  [defaults unvi] = interceptprop(varargin, defaults);

  % ensure default type is in list!
  if ~ismember(defaults.instrumenttype, defaults.instrumenttypes)
    defaults.instrumenttype = defaults.instrumenttypes{1};
  end
  
  % create the figure and add all the controls
  spacing = [10 10]; % x,y
  frameSpacing = [10 10]; % figure to frame spacing
  lineSpacing = 8;  % vertical spacing between lines of text


  fh = figure('Name', ['New image data'], ...
	      'NumberTitle','off', ...
	      'HandleVisibility', 'callback', ...
	      'Visible', 'off', ...
	      'MenuBar', 'None', ...
	      'Resize', 'off');
  
  % grab userdata area in case some figure create function has started
  % accessing it as a struct
  
  ud = get(fh, 'UserData');
  % instruments will change according to the type selected, so store in
  % userdata area, but remember to change contents when changing
  % instrument type, also change the instrument menu to suit
  ud.instrumenttype = defaults.instrumenttype;
  ud.instruments = instrumenttypeinfo(feval(defaults.instrumenttype), ... 
				      'supported');
  
  ud.instrument = ud.instruments(1); % current chosen instrument
  % ud.datatype = defaults.datatype; % currently selected datatype

  ud.qdc = defaults.qdc;  % current QDC
  ud.defaults = rmfield(defaults, 'qdc'); % could be too big to keep twice
  

  % Add standard menus
  % helpData = {['New riometer data'], ...
  %	      'help_newdata'};
  % miamenu(fh, 'help', helpData);
  
  % Add standard toolbar
  toolbarh = miatoolbar('init', ...
			'parent', fh);

  bgColor = get(fh, 'Color');

  % need to find suitable width and height for all the text objects in
  % the LH column. Do this by creating a test object and finding out
  % what its position is after uiresize. The text used should be the
  % longest. Strictly speaking that might depend on font, kerning etc
  % but we will ignore that. Add an extra space if you have problems :)
  textH = uicontrol(fh, 'Style', 'text', ...
		    'Position', [0 0 1 1], ...
		    'String', 'Interpolation method:  ');
  textPos = uiresize(textH, 'l', 'b');

  delete(textH);
  
  % allocate space for the handles and positions
  genH     = zeros(19,1);   % handles to uicontrols in general frame
  genPos   = zeros(19,4);   % positions  "                    "

  % have to guess something for framewidth. Use a rule-of-thumb. Since
  % all the buton widths are related to text size having the frame
  % width to be a constant factor times larger than textPos(3) seems
  % ok.
  % frameWidth = 3.9 * textPos(3);
  frameWidth = 5 * textPos(3);
  
  lines = 10;
  genPos(1,:) = [frameSpacing frameWidth 0] + ...
      [0 50 0 2*spacing(2)+(lines+1)*textPos(4)+lines*lineSpacing]; 
  
  % resize figure to right size, keep top right in same place
  figWH = genPos(1,1:2) + genPos(1,3:4) + frameSpacing;
  figPos = get(fh, 'Position');
  screenSize = get(0, 'ScreenSize');
  set(fh, 'Position', [(screenSize(3)-figWH(1))/2 100 figWH]);
  
  % frame  = 1
  genH(1) = uicontrol(fh, 'Style', 'frame', ...
		      'BackgroundColor', bgColor, ...
		      'Position', genPos(1,:), ...
		      'Tag', 'generalframe');
  
  % 'General' = 2
  genPos(2,:) = [genPos(1,1)+spacing(1), ...
		 genPos(1,2)+genPos(1,4)-spacing(1)-textPos(4), ...
		 genPos(1,3)-2*spacing(2), textPos(4)];
  genH(2) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'New image data', ...
		      'FontWeight', 'bold', ...
		      'HorizontalAlignment', 'center', ...
		      'Position', genPos(2,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');

  % 'Instrument type' = 3
  genPos(3,:) = [genPos(1,1)+spacing(1), ...
		 genPos(2,2)-lineSpacing- textPos(4), ...
		 textPos(3), textPos(4)];
  genH(3) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Instrument type: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(3,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');
  
  % x coord of RH column in figure
  textPos(3);
  rhColDiff = spacing(1) + 30;
  rhCol = genPos(1,1) + textPos(3) + rhColDiff;
  instrumentStr = cell(size(ud.defaults.instrumenttypes));
  for n = 1:length(instrumentStr)
    instrumentStr{n} = gettype(feval(ud.defaults.instrumenttypes{n}), 'c');
  end
  
  genPos(4,:) = [rhCol genPos(3,2) 1 1];
  genH(4) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'HorizontalAlignment', 'left', ...
		      'Value', find(strcmp(defaults.instrumenttype, ...
					   ud.defaults.instrumenttypes)), ...
		      'String', instrumentStr, ...
		      'Position', genPos(4,:), ...
		      'Callback', [mfilename '(''instrumenttype'');'], ...
		      'Tag', 'instrumenttypemenu');
  genPos(4,:) = uiresize(genH(4), 'l', 'b');

  genPos(5,:) = genPos(3,:) - [0 genPos(3,4)+lineSpacing 0 0];
  genH(5) = uicontrol(fh, 'Style', 'text', ...
		      'String', 'Instrument: ', ...
		      'HorizontalAlignment', 'right', ...
		      'Position', genPos(5,:), ...
		      'BackgroundColor', bgColor, ...
		      'Enable', 'inactive');

  genPos(6,:) = [rhCol genPos(5,2) 1 1];

  % make a list of instruments containing all the  instruments we could
  % print in the popup menu. Use this list when calling uiresize. Will do
  % updateinstruments later to get the right items in the list

  tmpInstruments =  instrumenttypeinfo(feval(defaults.instrumenttype), ... 
				      'supported');
  % warning('fix me'); % generalise above to work for all instrument types
  
  instrumentsStr = localInstruments2Str(tmpInstruments);
    
  genH(6) = uicontrol(fh, 'Style', 'popupmenu', ...
		      'HorizontalAlignment', 'left', ...
		      'String', instrumentsStr, ...
		      'Value', find(ud.instrument == ud.instruments), ...
		      'Position', genPos(6,:), ...
		      'Callback', [mfilename '(''instrument'');'], ...
		      'Tag', 'instrumentmenu');
  ud.instrumentmenu = genH(6);
  
  genPos(6,:) = uiresize(genH(6), 'l', 'b');
  
  genPos(7,:) = [rhCol 10 0 0];
  
  % create start/end box and find size of it
  ud.steth = stetbox(fh, genPos(7,:));
  sb = getstetbox(ud.steth);

  appendcallback(getstetbox(ud.steth), ...
		 [mfilename '(''updatesamples'');']);
  
  sbPos = getposition(sb);
  genPos(7,:) = [rhCol, genPos(6,2)-sbPos(4)-lineSpacing, 0, 0];
  
  setposition(sb, genPos(7,:));
  
  % addlabel(sb, rhColDiff, ...
  %  not sure why this is, sort out later
  addlabel(sb, rhColDiff - spacing(1), ...
	   {'Start time: ', 'End time: ', 'Duration: '});
  sbPos = getposition(sb);

  
  genPos(8,:) = [genPos(3,1), genPos(7,2)-lineSpacing-textPos(4), ...
		 genPos(3,3:4)];

  genPos(9,:) = [rhCol genPos(8,2:4)];
  genH(9) = timespanbox(fh, genPos(9,:), 5, ...
			timespan(1,'s'));
			% info(ud.instrument, 'bestresolution'));
  genH(8) = addlabel(gettimespanbox(genH(9)), rhColDiff-10, 'Resolution: ');
  ud.resh = genH(9);
  appendcallback(gettimespanbox(ud.resh), ...
		 [mfilename '(''updatesamples'');']);
  
  genPos(10,:) = [genPos(3,1), genPos(8,2)-lineSpacing-textPos(4), ...
		  genPos(3,3:4)];
  
  genH(10) = uicontrol(fh, 'Position', genPos(10,:), ...
		      'Style', 'text', ...
		      'HorizontalAlignment', 'right', ...
		      'String', 'No. of samples: ', ...
		      'Enable', 'inactive', ...
		      'BackgroundColor', bgColor);
  
  genPos(11,:) = [rhCol genPos(10,2:4)];
  genH(11) = uicontrol(fh, 'Position', genPos(11,:), ...
		      'Style', 'text', ...
		      'HorizontalAlignment', 'left', ...
		      'String', '', ...
		      'Enable', 'on', ...
		      'BackgroundColor', bgColor);
  genPos(11,:) = uiresize(genH(11), 'l', 'b', '9999999999');
  ud.sampleh = genH(11);
  
  genPos(14,:) = [genPos(3,1), genPos(10,2)-lineSpacing-textPos(4), ...
		  genPos(3,3:4)];
  genH(14) = uicontrol(fh, 'Position', genPos(14,:), ...
		       'Style', 'text', ...
		       'HorizontalAlignment', 'right', ...
		       'String', 'QDC: ', ...
		       'Enable', 'inactive', ...
		       'BackgroundColor', bgColor);
  ud.qdctexth = genH(14);
  
  genPos(15,:) = [rhCol genPos(14,2) 1 1];
  genH(15) = uicontrol(fh, 'Style', 'popupmenu', ...
		       'HorizontalAlignment', 'left', ...
		       'Value', 1, ...
		       'String', ...
		       {'Auto', 'Manual (by date)', 'Manual (from file)'}, ... 
		       'Position', genPos(15,:), ...
		       'Callback', [mfilename '(''qdc'');'], ...
		       'Tag', 'qdctype');
  genPos(15,:) = uiresize(genH(15), 'l', 'b');
  ud.qdctypeh = genH(15);
  
  genPos(16,1) = genPos(15,1) + genPos(15,3) + spacing(1);
  genPos(16,2:4) = genPos(15,2:4);
  genH(16) = uicontrol(fh, 'Style', 'pushbutton', ...
		       'HorizontalAlignment', 'center', ...
		       'Value', 1, ...
		       'String', 'Select', ... 
		       'Position', genPos(16,:), ...
		       'Callback', [mfilename '(''qdcselect'');'], ...
		       'Tag', 'qdcselect');
  ud.qdcselecth = genH(16);
  genPos(16,:) = uiresize(genH(16), 'l', 'b');

  genPos(17,1) = genPos(16,1) + genPos(16,3) + spacing(1);
  genPos(17,2:4) = genPos(15,2:4);
  genH(17) = uicontrol(fh, 'Style', 'pushbutton', ...
		       'HorizontalAlignment', 'center', ...
		       'Value', 1, ...
		       'String', 'Details', ... 
		       'Position', genPos(17,:), ...
		       'Callback', [mfilename '(''qdcdetails'');'], ...
		       'Tag', 'qdcdetails');
  genPos(17,:) = uiresize(genH(17), 'l', 'b');
  ud.qdcdetailsh = genH(17);
    
  okPos = [10 10 1 1];
  [okH cancelH] = okcancel('init', fh, okPos, [mfilename '(''ok'');'], ...
			   [mfilename '(''cancel'');']);
  
  
  set(fh, 'UserData', ud);
  % get instruments printed correctly
  feval(mfilename, 'updateinstruments', fh, ud.instrumentmenu);
  
  % ensure QDC controls are correctly set
  feval(mfilename, 'qdcupdate', fh);
  
  set(fh, 'Visible', 'on');
  % --------------------------------------------------------------
 case 'ok'
  % perform the checks now
  fh = gcbf;
  ud = get(fh, 'UserData');
  t = gettime(getstetbox(ud.steth));
  resolution = gettimespan(gettimespanbox(ud.resh));

  if isempty(t{1}) | isempty(t{2})
    errordlg('Please set start and end times to valid dates', ...
	     'Error', 'modal');
  elseif t{3} <= timespan
    errordlg('Duration must be a non-zero value', 'Error', ...
	     'modal');
  elseif resolution <= timespan(0, 's')
    errordlg('Resolution must be a non-zero value', 'Error', ...
	     'modal');
  elseif fix(t{3}/resolution) ~= (t{3}/resolution)
    fix(t{3}/resolution)
    t{3}/resolution
    errordlg(['The duration should be exactly divisible by the ' ...
	      'resolution'], 'Error', 'modal');
  else
    pointer = get(fh, 'Pointer');
    set(fh, 'Pointer', 'watch');
    buildTime = timestamp('now');      
    if 1
      args = {'load', 1, ...
	      'instrument', ud.instrument, ...
	      'starttime', t{1}, ...
	      'endtime', t{2}, ...
	      'resolution', resolution};

      
      if 0 & val ~= 1 & strcmp(ud.datatype, 'rio_abs') 
	if isempty(ud.qdc)
	  error('QDC not set');
	elseif isa(ud.qdc, 'rio_qdc')
	  args{end+1} = 'qdc';
	  args{end+1} = ud.qdc;
	elseif isa(ud.qdc, 'timestamp')
	  % load qdc
	  args{end+1} = 'qdc';
	  args{end+1} = rio_qdc('instrument', ud.instrument, ...
				'load', 1, ...
				'time', ud.qdc);
	else
	  error('unknown data');
	end
      end
      cancelH = findobj(fh, 'Type', 'uicontrol', ...
			'Style', 'pushbutton', ...
			'Tag', 'cancel')
      args{:}
      rio = feval(instrumenttypeinfo(feval(ud.instrumenttype), ...
				     'imageclass'), ...
		  args{:}, 'cancelhandle', cancelH);
    else
      error('set up this way to use QDC');
      rio = [];
      cmd = ['rio = feval(ud.datatype, ' ...
	     '''load'', 1, ' ...
	     '''instrument'', ud.instrument, ' ...
	     '''starttime'', t{1}, ' ...
	     '''endtime'', t{2}, ' ...
	     '''resolution'', resolution);'];
      errcmd = ['errordlg({''Loading data failed:'',' ...
		'sprintf(''%s'', lasterr)}, '...
		'''Error'', ''modal'');' ...
		'set(fh, ''Pointer'', pointer);' ...
		'return;'];
      set(fh, 'Pointer', pointer), % error('stop')
      lasterr('');
      eval(cmd, errcmd);
      if isempty(rio)
	return;
      end
    end
    if ~ishandle(fh)
      return; % cancel pressed
    end;

    % the amount of time it took to make
    buildTime = timestamp('now') - buildTime;
    fstr = '%Y%m%d%H%M%S';
    
    % preset filename to something useful
    rio = setfilename(rio, strftime(t{1}, fstr));
    
    miashow('init', rio, 'buildtime', buildTime);
    set(fh, 'Pointer', pointer);
    % close(fh);
  end
  

  % --------------------------------------------------------------
 case 'cancel'
  close(gcbf);

  % --------------------------------------------------------------
 case 'instrumenttype'
  % mfilename('instrumenttype')
  % mfilename('instrumenttype', fh, menuh)
  if nargin > 2
    fh = varargin{1};
    menuh = varargin{2};
  else
    fh = gcbf;
    menuh = gcbo;
  end
  
  ud = get(fh, 'UserData');
  ud.instrumenttype = ud.defaults.instrumenttypes{get(menuh, 'Value')};
  ud.instruments = instrumenttypeinfo(feval(ud.instrumenttype), ... 
				      'supported');
  type = gettype(feval(ud.instrumenttype), 'l');
  set(fh, 'UserData', ud, ...
	  'Name', sprintf('New %s data [images]', type));
  
  states = {'off', 'on'};
  
  feval(mfilename, 'updateinstruments', fh);
  
  % --------------------------------------------------------------
 case 'instrument'
  if nargin > 2
    fh = varargin{1};
    menuh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  else
    fh = gcbf;
    menuh = gcbo;
  end
  ud = get(fh, 'UserData');

  n = get(menuh, 'Value');
  lastInstrument = ud.instrument;
  ud.instrument = ud.instruments(n);
  if ud.instrument == lastInstrument
    return; % no actual change
  end
  
  set(fh, 'UserData', ud);

  setnearest(gettimespanbox(ud.resh), info(ud.instrument, 'bestresolution'));
  
  % --------------------------------------------------------------
 case 'updateinstruments'
  % mfilename('updateinstruments')
  % mfilename('updateinstruments', fh, menuh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  % menuh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  
  ud = get(fh, 'UserData');
  n = get(ud.instrumentmenu, 'Value');
  
  ud.instruments = instrumenttypeinfo(feval(ud.instrumenttype), ... 
				      'supported');
  % check if selected instrument is in the new list
  n = find(ud.instrument == ud.instruments);
  
  if isempty(n)
    disp(sprintf(['\rPreviously selected instrument not available ' ...
		 'for this datatype']));
    ud.instrument = ud.instruments(1);
    set(ud.instrumentmenu, 'Value', 1);
  else
    ud.instrument = ud.instruments(n(1)); % take first match
    set(ud.instrumentmenu, 'Value', n(1));
  end
  
  instrumentsStr = localInstruments2Str(ud.instruments);
  imh = findobj(fh, 'Type', 'uicontrol', 'Tag', 'instrumentmenu');
  set(imh, 'String', instrumentsStr);
  uiresize(imh, 'l', 'b'); % ensure new instrument list displays ok
  set(fh, 'UserData', ud);
  
  % --------------------------------------------------------------
 case 'updatesamples'
  % mfilename('updateinstruments')
  % mfilename('updateinstruments', fh, menuh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  
  res = gettimespan(gettimespanbox(ud.resh));
  t = gettime(getstetbox(ud.steth));

  s = 'Error';
  col = badColor;
  if isempty(t{3})
    ; % error
  elseif t{3} == timespan | res == timespan
    ;
  else
    samples = t{3} / res;
    if isinf(samples) | isnan(samples)
      ;
    elseif floor(samples) <= samples + eps & floor(samples) >= samples - eps
      s = sprintf('%d', samples);
      col = 'k';
    else
      s = sprintf('%g', samples);
    end
  end
  set(ud.sampleh, 'String', s, 'ForegroundColor', col);
  
  
  % --------------------------------------------------------------
 case 'qdc'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  
  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  if val == 1
    % auto
    set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
  else
    % some kind of manual
    set([ud.qdcselecth], 'Enable', 'on');
    if isempty(ud.qdc)
      set(ud.qdcdetailsh, 'Enable', 'off');
    else
      set(ud.qdcdetailsh, 'Enable', 'on');
    end
  end
  % --------------------------------------------------------------
 case 'qdcselect'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end

  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  switch val
   case 1
    
   case 2
    % manual, by date 
    if isempty(ud.qdc)
      t = gettime(getstetbox(ud.steth));
      t = t{1};
      if ~isa(t, 'timestamp')
	t = timestamp('today'); % timebox might be bad
      end
    elseif isa(ud.qdc, 'rio_qdc')
      t = getstarttime(ud.qdc);
    elseif isa(ud.qdc, 'timestamp')
      t = ud.qdc;
    else
      error('unknown data');
    end
    
    [t button] = uigettimestamp('init', ...
				 'timestamp', t, ...
				 'name', 'Select QDC');
    if strcmp(button, 'cancel')
      feval(mfilename, 'qdcupdate', fh);
      return; % no changes made
    end
    ud.qdc = t;

   case 3
    % manual, from file
    errordlg('Not implemented', 'Error', 'modal');
    return;
    
    [filename Path] = uigetfile('*.mat', 'Select QDC');
    if isequal(filename, 0)
      % cancel pressed, so no changes
      return;
    end
    pathfile = fullfile(Path, filename);
    
   otherwise
    error('Unknown QDC method');
  end
  set(fh, 'UserData', ud); % save settings
  feval(mfilename, 'qdcupdate', fh); % get buttons set correctly
  % --------------------------------------------------------------
 
 case 'qdcdetails'
  % mfilename('qdc')
  % mfilename('qdc', fh)
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  if isempty(ud.qdc)
    disp('no QDC to display'); % this should not happen
  elseif isa(ud.qdc, 'rio_qdc')
    disp(sprintf('\r%s', char(ud.qdc)));
  elseif isa(ud.qdc, 'timestamp')
    disp(sprintf('\rUse QDC for %s', char(ud.qdc)));
  else
    error('unknown data');
  end
  
  % --------------------------------------------------------------
 case 'qdcupdate'
  % mfilename('qdc')
  % mfilename('qdc', fh, menuh)

  % update QDC controls, based upon their current settings, and the qdc
  % object in the userdata area
  if nargin > 1
    fh = varargin{1};
  else
    fh = gcbf;
  end
  ud = get(fh, 'UserData');
  val = get(ud.qdctypeh, 'Value');
  if val == 1
    % auto
    set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
  else
    % some kind of manual setting
    % set(ud.qdcselecth, 'Enable', 'on');
    if isempty(ud.qdc)
      % set(ud.qdcdetailsh, 'Enable', 'off');
      set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'off');
      set(ud.qdctypeh, 'Value', 1); % flick back to auto
    else
      set([ud.qdcselecth; ud.qdcdetailsh], 'Enable', 'on');
    end
  end
  
  % --------------------------------------------------------------
  
 otherwise
  error(sprintf('unknown action (was %s)', char(action)));
end


% --------------------------------------------------------------
function r = localInstruments2Str(in)
r = cell(size(in));
for n = 1:length(in)
  nm = getname(in(n));
  abbrev = getabbreviation(in(n));
  if isempty(nm)
    r{n} = sprintf('%s (%s)', getname(getlocation(in(n))), abbrev);
  else
    r{n} = sprintf('%s (%s) [%s]', getname(getlocation(in(n))), ...
		   abbrev, nm);
  end
end
