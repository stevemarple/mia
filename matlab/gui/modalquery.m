function r = modalquery(prompt, title, errorMessage)
%MODALQUERY Modal query dialog box which won't accept empty answers.
%
%   r = MODALQUERY(prompt, title, errorMessage)
%   r: user's response
%   prompt: Prompt which appears in the window, or on the command line
%   title: window title
%   errorMessage: message which appears if the user' response is empty
%
% MODALQUERY is a modal query dialog box which won't accept empty
% answers. If FIGUREs cannot be displayed the command line is used
% instead. title is used only for the FIGURE window title.
%
% See also CANUSEFIGURES, MODALMESSAGE.


% Can figures be shown?
useFigures = canusefigures;

r = '';
while isempty(r)
  if useFigures
    r = inputdlg(prompt, title, 1);
  else
    r = input([prompt ' '], 's');
  end
  if iscell(r)
    if ~isempty(r)
      r = r{1};
    end
  end
  if isempty(r) | all(isspace(r))
    if useFigures
      h = errordlg(errorMessage, 'Error', 'modal');
      waitfor(h);
    else
      fprintf('%s\n', errorMessage);
    end
  end
end

