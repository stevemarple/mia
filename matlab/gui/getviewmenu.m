function vm = getviewmenu(h)
%GETVIEWMENU  Return a VIEWMENU object using a handle to any managed FIGURE.
%
%   vm = GETVIEWMENU(h);
%
%   vm: VIEWMENU object
%   h: FIGURE handle
%
%   See also VIEWMENU, FIGURE.

% if an error occurs at this point you haven't passed a valid handle,
% no point in checking - it'll only waste time since it is an error anyway!
type = get(h, 'Type');

switch type
  case 'figure'
    vmh = findobj(h, 'Type', 'uimenu', 'Tag', 'viewmenutop');
    if isempty(vmh)
      % user might have closed it
      warning('Cannot find a VIEWMENU object associated with this figure');
      vm = [];
    end
    vm = getVmFromVmHandle(vmh);

  case 'uimenu'
    if ~isempty(findobj(h, 'flat', 'Tag', 'viewmenutop'))
      % handle is a toplevel viewmenu object
      vm = getVmFromVmHandle(h);
      
    else
      % oh dear, this is going to be long-winded - shouldn't have to do
      % this ever but it is easy to write and makes the function robust.
      % Recurse upwards until something suitable is found.
      warning(['handle ought to be a figure or top-level viewmenu' ...
	    ' handle, please wait...']);
      vm = getviewmenu(get(h, 'Parent')); % recurse upwards until
    end
    
  case 'root'
    % someone is being very silly...
    error('Cannot calculate figure from root object!');
    
  otherwise
    % recurse upwards
    warning(['handle ought to be a figure or top-level viewmenu' ...
	  ' handle, please wait...']);
    vm = getviewmenu(get(h, 'Parent'));
end

return;
% ========================

ud = get(h, 'UserData');

if isa(ud.viewmenu, 'double')
  % found a handle to the container object
  ud = get(ud.viewmenu, 'UserData');
  vm = ud.viewmenu;
  return;
  
elseif isa(ud.viewmenu, 'viewmenu')
  % found the class object
  vm = ud.viewmenu;
  return;
  
else
  error(['Handle is not associated with a viewmenu object or UserData' ...
	' area corrupted']);
end


% ========================

% given a handle to a top-level viewmenu uimenu return the class object
function vm = getVmFromVmHandle(vmh)
ud = get(vmh, 'UserData');
if isa(ud, 'double')
  % found a handle to the container object
  vm = get(ud, 'UserData');
  
elseif isa(ud, 'viewmenu')
  % found the object itself
  vm = ud;
  
else
  error('UserData area of viewmenu corrupted');
end  
return;
