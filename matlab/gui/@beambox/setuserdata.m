function setuserdata(bb, data)
%SETUSERDATA Set an internal user data area.
%
%   SETUSERDATA(bb, data)
%   bb: BEAMBOX object
%   data: any data
%
%   This function is a wrapper to store data somewhere inside the BEAMBOX
%   class. It will be stored in one of the UserData areas of a UICONTROL
%   object managed by BEAMBOX.

bb.userdata = data;
update(bb);

