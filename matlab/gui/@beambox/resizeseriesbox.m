function resizeseriesbox(bb, str)
%RESIZESERIESBOX Resize the series uicontrol in a BEAMBOX
%
%   RESIZESERIESBOX(bb)
%   bb: BEAMBOX object
%   str: the string which is to just fit inside the series box
%
%   See also UIRESIZE, BEAMBOX.

uiresize(bb.serieshandle, 'l', 'm', str);

