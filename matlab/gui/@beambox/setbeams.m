function setbeams(bb, varargin)
%SETBEAMS  Set the beams selected in a BEAMBOX.
%
%   SETBEAMS(bb, beams);
%   
%   See also BEAMBOX.

beams = nan;
imaging = nan;
widebeam = nan;
imagingallow = nan;
widebeamallow = nan;

if rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  defaults.beams = bb.beams;
  defaults.imaging = bb.imaging;
  defaults.widebeam = bb.widebeam;
  defaults.imagingallow = bb.imagingallow;
  defaults.widebeamallow = bb.widebeamallow;
  defaults.badbeams = bb.badbeams;
  
  [defaults unvi] = interceptprop(varargin, defaults);
  if length(unvi)
    warning(['The following parameter(s) have been ignored:', ...
	     sprintf(' %s', varargin{unvi(1:2:end)})]);
  end
  if any(~ismember(defaults.beams, [defaults.imaging defaults.widebeam]))
     error('attempted to set beams which do not exist');
  end
  
  bb.beams = defaults.beams;
  bb.imaging = defaults.imaging;
  bb.imagingallow = defaults.imagingallow;
  bb.widebeam = defaults.widebeam;
  bb.widebeamallow = defaults.widebeamallow;
  bb.badbeams = defaults.badbeams;
  
else
  warning('Please use parameter/name value interface');
  
  if nargin == 2
    beams = varargin{1};

  elseif nargin == 4
    beams = varargin{1};
    imaging = varargin{2};
    widebeam = varargin{3};
    imagingallow = imaging;
    widebeamallow = widebeam;
    
  elseif nargin == 6
    beams = varargin{1};
    imaging = varargin{2};
    widebeam = varargin{3};
    imagingallow = varargin{4};
    widebeamallow = varargin{5};
    
  else
    error('incorrect parameters');
  end
  
  % a simple error check
  if isequal(localisnan(beams), 0) & ...
	find(~ismember(beams, [bb.imaging bb.widebeam]))
    error('beams do not exist');
  end
  
  if ~localisnan(beams)
    bb.beams = beams;
  end
  if ~localisnan(imaging)
    bb.imaging = imaging;
  end
  if ~localisnan(widebeam)
    bb.widebeam = widebeam;
  end
  if ~localisnan(imagingallow)
    bb.imagingallow = imagingallow;
  end
  if ~localisnan(widebeamallow)
    bb.widebeamallow = widebeamallow;
  end
end

bb.selectionorder = repmat(nan, 1, abs(bb.limitedbeams));
bb.selectionorder(1:min(abs(bb.limitedbeams), ...
    prod(size(beams)))) = ...
    beams(1:min(abs(bb.limitedbeams), prod(size(beams))));

set(bb.serieshandle, 'String', printseries(bb.beams));
update(bb);
r = bb;
return

% ---------------------
function r = localisnan(a)
if isempty(a)
  r = 0;
else
  r = isnan(a);
end
return
