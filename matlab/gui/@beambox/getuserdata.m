function data = getuserdata(bb)
%GETUSERDATA Get data from internal user data area.
%
%   d = GETUSERDATA(bb)
%   d: data
%   bb: BEAMBOX object

data = bb.userdata;
