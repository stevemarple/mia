function appendcallback(bb, cbs)
%APPENDCALLBACK Add another callback function.
%
%   APPENDCALLBACK(bb, cb);
%
%   bb: BEAMBOX object
%   cbs: callback string to EVAL
%   callbackFcn: optionally choose a callback function, instead of using
%   the default 'Callback'
%
%   See also EVAL.

if ~ischar(cbs)
  error('callback string is not a string');
else
  bb.callback = cbs;
  % make sure everything associated with this beambox knows what to do
  update(bb);
end
