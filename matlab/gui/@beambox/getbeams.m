function b = getbeams(bb)
%GETBEAMS Get the beams selected from a BEAMBOX.
%
% See also BEAMBOX.

b = bb.beams;
