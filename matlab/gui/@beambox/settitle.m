function settitle(bb, title)
%SETTITLE  Set the title of the pop-up window used by BEAMBOX.
%
%   SETTITLE(bb, title);
%   bb: BEAMBOX object
%   title: tite string got be appended to "Select beams"
%
%   See also BEAMBOX.

bb.title = title;
update(bb);
