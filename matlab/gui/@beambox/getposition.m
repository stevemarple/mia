function p = getposition(bb)
%GETPOSITION Return the position matrix for a BEAMBOX object.
%
% See also BOUNDINGBOX

p = boundingbox(bb.maphandle, bb.serieshandle);
