function h = beambox(parent,pos,varargin)
%BEAMBOX Make an box for entering beams.
%
%   h = BEAMBOX(parent, pos, image, wide)
%   h = BEAMBOX(parent, pos, image, wide, title)
%   h = BEAMBOX(parent, pos, image, wide, title, imageallow, wideallow)
%   h = BEAMBOX(parent, pos, image, wide, title, ...
%               imageallow, wideallow, PARAM_NAME, PARAM_VALUE ...)
%   h: handle to uicontrol
%   parent: handle to parent FIGURE
%   pos: position vector
%   image: imaging beam numbers
%   wide: widebeam number(s)
%   title: title string to append after 'Select beams'
%   imageallow: imaging beam numbers which may be selected
%   wideallow: widebeam number(s) which may be selected
%
%   If imageallow and wideallow are given, the values should be members of
%   the sets image and wide, errors will result if they are not!
%
%   Additional parameters may be specified by means of the parameter
%   name/value method. Value parameters are:
%
%      'beamlimit', n 
%      Limit selection to a maximum of n beams only. If n is negative then
%      once -n beams have been selected none can be unselected. In both
%      cases, attempting to select > abs(n) will unselect the earliest
%      selected beam.
%
%   If you wish to have a callback run after the beams have changed it is
%   not possible to simply append a callback to all the BEAMBOX
%   uicontrols - pressing the map button brings up a new figure whose
%   controls will not have the callback. Instead use the member function
%   APPENDCALLBACK.
%
%   See also GETBEAMBOX, APPENDCALLBACK.
%
%
%   This is a generic class which will work for any number of imaging beams
%   (though it currently only knows how to draw the map for a square
%   number). It will also work for any number of widebeams. The beams are
%   drawn in the map left to right, top row to bottom row. The number
%   given to each beam is taken from the corresponding place in the
%   imagingbeam/widebeam matrix. They do not have to be numbered 1->49!
%   2:2:98 also works!

%
% $Log$

% make this generic so it can easily be used for systems with different
% numbers of beams
% member fields
bb.imaging = [];   % beam numbers of the imaging beams
bb.widebeam = [];  % beam number(s) of the wide beam(s)
bb.imagingallow = []; % if set, 
bb.widebeamallow = [];
bb.badbeams = []; % beams deemed to be bad (shown as red)
bb.maphandle = []; % class object stored in user data of 'map' button
bb.serieshandle = [];
bb.beams = [];     % selected beams
bb.title = [];
bb.userdata = [];  % give any encapsulating classes somewhere to store data 
bb.callback = [];  % if set a callback string to execute after updating
bb.defaults.limitedbeams = 0;

% if nargin > 7 & 0

if nargin > 7 & 0
  [bb.defaults unvi] = interceptprop({varargin{6:length(varargin)}}, ...
				     bb.defaults); 
end
% the beams

if mod(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % parameter name/value interface
  [bb unvi] = interceptprop(varargin, bb);
  if length(unvi)
    warning(['The following parameter(s) have been ignored:', ...
	     sprintf(' %s', varargin{unvi(1:2:end)})]);
  end

else
  switch nargin
   case 3
    bb.imaging = varargin{1};
    
   case 4
    bb.imaging = varargin{1};
    bb.widebeam = varargin{2};
    bb.imagingallow = bb.imaging;
    bb.widebeamallow = bb.widebeam;
   case 5
    bb.imaging = varargin{1};
    bb.widebeam = varargin{2};
    bb.imagingallow = bb.imaging;
    bb.widebeamallow = bb.widebeam;
    bb.title = varargin{3};
    
   case 7
    bb.imaging = varargin{1};
    bb.widebeam = varargin{2};
    bb.title = varargin{3};
    bb.imagingallow = varargin{4};
    bb.widebeamallow = varargin{5};
    
   otherwise
    if nargin < 3 | mod(nargin, 2) == 0
      error('incorrect parameters');
    end
    bb.imaging = varargin{1};
    bb.widebeam = varargin{2};
    bb.title = varargin{3};
    bb.imagingallow = varargin{4};
    bb.widebeamallow = varargin{5};
  end
end

if bb.defaults.limitedbeams ~= 0
  bb.selectionorder = repmat(nan, 1, abs(bb.defaults.limitedbeams));
else
  bb.selectionorder = [];
end

if isempty([bb.imaging bb.widebeam])
  error('No beams!');
end
bb = class(bb, 'beambox');

[bb.maphandle bb.serieshandle] = makebeambox(bb, defaults.parent, pos);

% store the data once, and store the path back to the data for the other
% controls 
set(bb.maphandle, 'UserData', bb);
set(bb.serieshandle, 'UserData', bb.maphandle);

% set the callbacks
set(bb.maphandle, 'Callback', 'beamboxcb(getbeambox(gcbo),''map'');');
set(bb.serieshandle, 'Callback', 'beamboxcb(getbeambox(gcbo),''series'');');


h = bb.maphandle;








