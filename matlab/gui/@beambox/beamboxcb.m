function beamboxcb(bb, box)
%BEAMBOXCB Callback function for BEAMBOX class.
%
% BEAMBOXCB(bb, box)
% bb: BEAMBOX object
% box: uicontrol making the callback, 1 = map, 2 = series

% override the mfilename command - since it returns beambox/beamboxcb -
% which is then evaulated as a divide operation... GRRRR
mfilename = 'beamboxcb';

badColor = [0.8 0 0];

if isa(box, 'char')
  switch box
    % -------------------------------------------------------------
    case 'map'
      % create and call map selecting window 
      fh = figure('Name', ['Select beams' bb.title], ...
	  'NumberTitle', 'off', ...
	  'ResizeFcn', ['okcancel(''resize'',gcbf);'], ...
	  'Visible', 'off', ...
	  'WindowStyle', 'modal');

      spacing = [5 5]; % x,y (general spacing between buttons
      figSpacing = [20 20]; % spacing to edge of figure
      ud = []; % user data to go into figure

      bgcolour = get(fh, 'Color');
      
      
      % add the buttons for the imaging beams
      % need a frame round beams, but leave space for ok/cancel buttons
      imageFrameBL = [spacing(1) 65 0 0];
      % offset for where the buttons are
      % imagePosOffset = [10 50 0 0]; 
      imagePosOffset = imageFrameBL + [figSpacing 0 0];
      
      % placed 
      numImageBeams = prod(size(bb.imaging));
      rows = floor(sqrt(numImageBeams)); % use for this cols too
      % since it is sq. 
      if rows*rows ~= numImageBeams
	error('Don''t know how to draw a map for a non-square array');
      end
      
      % 99 for 49 beams, 9999 for 1024 beams, etc
      longestString = repmat('9', 1, ...
	  1+floor(log10(max([bb.imaging bb.widebeam]))));

      % create one button and uiresize it to find the correct size
      testHandle = uicontrol(fh, 'Position', [0 0 60 20], ...
	  'Style', 'checkbox', ...
	  'HorizontalAlignment', 'right', ...
	  'UserData', bb.maphandle);
      testPos = uiresize(testHandle, 'l', 'b', longestString);
      delete(testHandle);

      % first place a frame around where the imaging beams will go
      framePos = zeros(1, 4);
      if ~isempty(bb.imaging)
	framePos = imagePosOffset + ...
	    [-spacing (rows*(testPos(3)+spacing(1))+spacing(1)) ...
	      (rows*(testPos(4)+spacing(2))+spacing(2))];
	ud.framehandle = uicontrol(fh, 'Position', framePos, 'Style', ...
	    'frame', 'UserData', bb.maphandle);
      else
	ud.framehandle = [];
      end
      
      ud.imagehandles = zeros(1, numImageBeams);
      % ud.imagehandles(:) = copyobj(testHandle, repmat(fh, 1, numImageBeams));
      % move them to the right places
      if bb.limitedbeams ~= 0
	limitedBeamsCallback = ...
	'beamboxcb(getbeambox(gcbo),''beambuttons'');';
      else
	limitedBeamsCallback = '';
      end
      imagePos = ([(testPos(3:4) + spacing(1:2)) testPos(3:4)] ...
	      .*[0 0 1 1]) + imagePosOffset;
      for r = rows:-1:1
	for c = 1:rows 
	  n = (rows-r)*rows + c;
	  imagePos = ([(testPos(3:4) + spacing(1:2)) testPos(3:4)] ...
	      .*[c-1 r-1 1 1]) + imagePosOffset;
	  ud.imagehandles(n) = uicontrol(fh, 'Position', imagePos, ...
	  'Style', 'checkbox', ...
	  'HorizontalAlignment', 'right', ...
	  'String', [int2str(bb.imaging(n)) ' '], ...
	  'Value', ismember(bb.imaging(n), bb.beams), ...
	  'Callback', limitedBeamsCallback, ...
	  'UserData', bb.maphandle);
	  % set(ud.imagehandles(n), 'Position', imagePos, ...
	  %     'String', int2str(bb.imaging(n)), ...
	  %     'Value', ismember(bb.imaging(n), bb.beams));
	end
      end

      % restrict the active buttons to only those which are allowed
      % offbeams = setxor(bb.imaging, bb.imagingallow);
      % for n = offbeams
      %  set(ud.imagehandles(find(bb.imaging == n)), 'Enable', 'off'); 
      % end
      set(ud.imagehandles(~ismember(bb.imaging, bb.imagingallow)), ...
	  'Enable', 'off'); 


      % add the widebeam button(s)
      numWideBeams = prod(size(bb.widebeam));
      ud.widehandles = zeros(1,numWideBeams);
      wbPos = imagePos; % will be left as this if no widebeams
      for n = 1:numWideBeams
	wbPos = [(testPos(3:4) + spacing(1:2)) testPos(3:4)] .* ...
	    [rows numWideBeams-n 1 1] + [spacing(1) 0 0 0] + ...
	    imagePosOffset;
	ud.widehandles(n) = uicontrol(fh, 'Position', wbPos, ...
	    'Style', 'checkbox', ...
	    'HorizontalAlignment', 'right', ...
	    'String', [int2str(bb.widebeam(n)) ' '], ...
	    'UserData', bb.maphandle, ...
	    'Callback', limitedBeamsCallback, ...
	    'Value', ismember(bb.widebeam(n), bb.beams));    
      end
      % restrict the active buttons to only those which are allowed
      % offbeams = setxor(bb.widebeam, bb.widebeamallow);
      % for n = offbeams
      % set(ud.widehandles(find(bb.widebeam == n)), 'Enable', 'off'); 
      % end
      set(ud.widehandles(~ismember(bb.widebeam, bb.widebeamallow)), ...
	  'Enable', 'off'); 

      % set bad beams to red
      set(ud.imagehandles(ismember(bb.imaging, bb.badbeams)), ...
	  'ForegroundColor', badColor);
      ismember(bb.widebeam, bb.badbeams)
      set(ud.widehandles(logical(ismember(bb.widebeam, bb.badbeams))), ...
	  'ForegroundColor', badColor);
      
      
      % add some buttons to select all / clear all / toggle
      boundbox = boundingbox([ud.imagehandles(:); ud.widehandles(:)]);
      selectGoodPos = [wbPos(1)+testPos(3)+spacing(1), ...
	    boundbox(2)+boundbox(4), 1, 1];
	    % framePos(2) + framePos(4) - spacing(2) - 1, 1, 1];
      
      ud.selectgoodbeamshandle = uicontrol(fh, 'Style', 'pushbutton', ...
	  'Position', selectGoodPos, ...
	  'String', 'Select good beams', ...
	  'UserData', bb.maphandle, ...
	  'Callback', [mfilename '(getbeambox(gcbo),''selectgood'');']); 
      selectGoodPos = uiresize(ud.selectgoodbeamshandle, 'l', 't');
      
      selectAllPos = selectGoodPos - [0 selectGoodPos(4)+spacing(2) 0 0];
      ud.selectallhandle = uicontrol(fh, 'Style', 'pushbutton', ...
	  'Position', selectAllPos, ...
	  'String', 'Select all', ...
	  'UserData', bb.maphandle, ...
	  'Callback', [mfilename '(getbeambox(gcbo),''selectall'');']); 
      % selectAllPos = uiresize(ud.selectallhandle, 'l', 't');
      % 'Select all' is longest text so use its width and height for the
      % other buttons below it
      
      clearAllPos = selectAllPos - [0 selectAllPos(4)+spacing(2) 0 0];
      ud.clearallhandle = uicontrol(fh, 'Style', 'pushbutton', ...
	  'Position', clearAllPos, ...
	  'String', 'Clear all', ...
	  'UserData', bb.maphandle, ...
	  'Callback', [mfilename '(getbeambox(gcbo),''clearall'');']);
      
      togglePos = clearAllPos - [0 clearAllPos(4)+spacing(2) 0 0];
      ud.togglehandle = uicontrol(fh, 'Style', 'pushbutton', ...
	  'Position', togglePos, ...
	  'String', 'Toggle', ...
	  'UserData', bb.maphandle, ...
	  'Callback', [mfilename '(getbeambox(gcbo),''toggle'');']);

      if matlabversioncmp('>=', '5.2')
	% add tooltips
	set(ud.selectallhandle, 'TooltipString', ...
	    'select all available beams');
	set(ud.clearallhandle, 'TooltipString', ...
	    'clear the current selection');
	set(ud.togglehandle, 'TooltipString', ...
	    'invert the current selection');
	set(ud.framehandle, 'TooltipString', ...
	    'Imaging beams');
      end

      % have to make adjustments if rows of imaging beams < 3
      if rows < 3 & length(bb.widebeam) < 3
	% imagebox = boundingbox(ud.imagehandles(:));
	% widebox = boundingbox(ud.widehandles(:));
	err = boundbox(2) - togglePos(2);
	if err > 0
	  % too low
	  selectAllPos(2) = selectAllPos(2) + err;
	  clearAllPos(2) = clearAllPos(2) + err;
	  togglePos(2) = togglePos(2) + err;
	  set([ud.selectallhandle ud.clearallhandle  ud.togglehandle], ...
	      {'Position'}, {selectAllPos; clearAllPos; togglePos});
	end
      end

      % add ok/cancel buttons last so they are last when tabbing through
      % options in figure
      [okh cancelh] = okcancel('init', fh, [figSpacing 1 1], ...
	  [mfilename '(getbeambox(gcbo),''ok'')'], ...
	  [mfilename '(getbeambox(gcbo),''cancel'')']);
      set([okh cancelh], 'UserData', bb.maphandle);
      
      figPos = get(fh, 'Position');
      figWH = [selectAllPos(1)+selectAllPos(3) ...
	       max([framePos(2)+framePos(4), ...
		    selectAllPos(2)+selectAllPos(4), ...
		    boundbox(2)+boundbox(4)])] ...
	      + figSpacing;
      
      set(fh, 'UserData', ud, ...
	      'Position', [figPos(1:2) figWH]);
      okcancel('resize', fh); % place in centre of figure
      set(fh, 'visible', 'on', 'Resize', 'off');
      return;
      
      % -------------------------------------------------------------
    case 'series'
      s = get(bb.serieshandle, 'string');
      % replace non-numeric chars by space
      t = s;
      [tmp tmpIndex] = setdiff(t, '0123456789:-'); % keep 0-9:
      t(tmpIndex) = ' ';
      if ~strcmp(s, t) 
	disp(sprintf('\a')); % string was changed - make an annoying beep
      end
      % use 'e' to denote an error condition
      m = eval(['[' t ']'], '''e'''); 
      n = intersect(m, [bb.imagingallow bb.widebeamallow]); 
      if ~strcmp(n, 'e') % ~isempty(n)
	% did anything get thrown away?
	if ~isempty(setxor(m, n))
	  disp(sprintf('\a')); % yes - make an annoying beep
	end
	bb.beams = n;
	% rationalise and print back into string
	update(bb);
      else
	% % not an error
	disp(sprintf('\a'));
	% bb.beams = n;
	% update(bb);
      end
      set(bb.serieshandle, 'string', printseries(bb.beams));
      
      % evaluate users callback if set now we are done
      if ~isempty(bb.callback)
	eval(bb.callback);
      end
      
      % -------------------------------------------------------------
    case 'ok'
      fh = gcbf;
      pointer = get(fh, 'Pointer');
      set(fh, 'Pointer', 'watch');
      
      ud = get(fh, 'userdata');
      ivals = get(ud.imagehandles, 'value');
      if isa(ivals, 'double')
	ivals = {ivals};
      end
      wbvals = get(ud.widehandles, 'value');
      if isa(wbvals, 'double')
	wbvals = {wbvals};
      end
      bb.beams = [bb.imaging(find([ivals{:}])) ...
	    bb.widebeam(find([wbvals{:}]))];
      	
      if abs(bb.limitedbeams) > 1
	plural = 's';
      else
	plural = '';
      end
      if bb.limitedbeams < 0 & ...
	    length(bb.beams) ~= -bb.limitedbeams
	errordlg(sprintf('Please select exactly %d beam%s', ...
	    -bb.limitedbeams, plural), 'Error', ...
	    'modal');
	set(fh, 'Pointer', pointer);
	return;
      elseif bb.limitedbeams > 0 & ...
	    length(bb.beams) > -bb.limitedbeams 
	errordlg(sprintf('Please select no more than %d beam%s', ...
	    -bb.limitedbeams, plural), 'Error', ...
	    'modal');
	set(fh, 'Pointer', pointer);
	return;
      end
      update(bb);
      % print the newly selected beams into the series box
      set(bb.serieshandle, 'string', printseries(bb.beams));
      
      set(fh, 'Visible', 'off');
      % evaluate users callback if set now we are done
      if ~isempty(bb.callback)
	% % make gcbo available so that "getbeambox(gcbo)" works
	% gcbo = bb.maphandle; 
	eval(bb.callback);
      end

      % close AFTER running users callback since class user might wish to
      % access  the BEAMBOX UserData area. "getuserdata(getbeambox(gcbo)))"
      % only works if the current callback object has not been destroyed!
      
      close(fh);

      % -------------------------------------------------------------    
    case 'cancel'
      close(gcbf);
      
      % -------------------------------------------------------------
    case 'selectgood'
      % only select good beams which may be selected!
      fh = gcbf;
      ud = get(fh, 'userdata');
      % don't modify anything other than allowed beams
      onbeams = setdiff(bb.imagingallow, bb.badbeams);
      offbeams = intersect(bb.imagingallow, bb.badbeams);
      for n = onbeams
	set(ud.imagehandles(find(bb.imaging == n)), 'value', 1);
      end
      for n = offbeams
	set(ud.imagehandles(find(bb.imaging == n)), 'value', 0);
      end

      onbeams = setdiff(bb.widebeamallow, bb.badbeams);
      offbeams = intersect(bb.widebeamallow, bb.badbeams);
      for n = onbeams
	set(ud.widehandles(find(bb.widebeam == n)), 'value', 1);
      end
      for n = offbeams
	set(ud.widehandles(find(bb.widebeam == n)), 'value', 0);
      end
      
      % -------------------------------------------------------------
    case 'selectall'
      % only select beams which may be selected!
      fh = gcbf;
      ud = get(fh, 'userdata');
      onbeams = intersect(bb.imaging, bb.imagingallow);
      for n = onbeams
	set(ud.imagehandles(find(bb.imaging == n)), 'value', 1);
      end
      onbeams = intersect(bb.widebeam, bb.widebeamallow);
      for n = onbeams
	set(ud.widehandles(find(bb.widebeam == n)), 'value', 1);
      end
      
      
      % -------------------------------------------------------------
    case 'clearall'
      % can clear all - even those which are allowed to be selected
      fh = gcbf;
      ud = get(fh, 'userdata');
      set([ud.imagehandles ud.widehandles], 'value', 0);
      
      % -------------------------------------------------------------
    case 'toggle'
      % toggle only beams which may be selected
      fh = gcbf;
      ud = get(fh, 'userdata');
      togglebeams = intersect(bb.imaging, bb.imagingallow);
      for n = togglebeams
	h = ud.imagehandles(find(bb.imaging == n));
	set(h, 'value', ~get(h, 'value'));
      end
      togglebeams = intersect(bb.widebeam, bb.widebeamallow);
      for n = togglebeams
	h = ud.widehandles(find(bb.widebeam == n));
	set(h, 'value', ~get(h, 'value'));
      end
      % -------------------------------------------------------------
    case 'beambuttons'
      fh = gcbf;
      cbo = gcbo;
      if get(cbo, 'Value') == 0;
	% deselecting a beam
	return;
      end

      % check number selected matches 
      % ======
      if 0 
	selectedH = findobj(fh, 'Type', 'uicontrol', ...
	    'Style', 'checkbox', 'Value', 1);
	selectedBeams = get(selectedH, 'String'); % might be cell array
	if iscell(selectedBeams)
	  selectedBeams = atoi(selectedBeams{:});
	else
	  selectedBeams = atoi(selectedBeams);
	end
      end
      % ======
      ud = get(fh, 'userdata');
      ivals = get(ud.imagehandles, 'value');
      if isa(ivals, 'double')
	ivals = {ivals};
      end
      wbvals = get(ud.widehandles, 'value');
      if isa(wbvals, 'double')
	wbvals = {wbvals};
      end
      selectedBeams = [bb.imaging(find([ivals{:}])) ...
	    bb.widebeam(find([wbvals{:}]))];
      
      % update selection order to remove beams which aren't selected
      tmp = bb.selectionorder(find(ismember(bb.selectionorder, ...
	  selectedBeams)));
      bb.selectionorder = repmat(nan, 1, abs(bb.limitedbeams));
      bb.selectionorder(1:length(tmp)) = tmp;

      earliestSelectedBeam = ...
	  bb.selectionorder(abs(bb.limitedbeams));
      if ~isnan(earliestSelectedBeam);
	% unselect beam
	oldButtonH = findobj(fh, 'Type', 'uicontrol', ...
	    'Style', 'checkbox', ...
	    'String', [int2str(earliestSelectedBeam) ' ']);
	set(oldButtonH, 'Value', 0);
      end
      % curBeam = atoi(get(cbo, 'String'));
      curBeam = fix(str2num(get(cbo, 'String')));
      bb.selectionorder = [curBeam ...
	    bb.selectionorder(1:(abs(bb.limitedbeams)-1))];
      update(bb); % store back selection order
      % -------------------------------------------------------------
    otherwise
      error('unknown uicontrol');
  end
  
else
  error(sprintf('invalid parameter (was class %s)', class(handle)));
end

