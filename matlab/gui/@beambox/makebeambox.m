function [mapHandle, seriesHandle] = makebeambox(bb, parent, pos)
%MAKEBEAMBOX Place the UICONTROLs for a BEAMBOX object.

mapHandle = [];
seriesHandle = [];

spacing = [10 5]; % X,Y

mapPos = [pos(1) pos(2) 1 1]; % + buttonSpacing;
mapHandle = double(uicontrol(parent, 'Position', mapPos, ...
			     'Style', 'pushbutton', ...
			     'String', 'Map', ...
			     'Interruptible', 'off', ...
			     'BusyAction', 'cancel', ...
			     'Tag', 'mapbutton')); 
mapPos = uiresize(mapHandle, 'l', 'b');

seriesPos = [mapPos(1)+mapPos(3)+spacing(1) pos(2) 1 1];
seriesHandle = double(uicontrol(parent, 'Position', seriesPos, ...
				'Style', 'edit', ...
				'String', printseries(bb.beams), ...
				'Tag', 'seriesbox'));
seriesPos = uiresize(seriesHandle, 'l', 'b', '1234567890123456');

if matlabversioncmp('>=', '5.2')
  % add tooltips
  set(mapHandle, 'TooltipString', 'Press to choose beams graphically');
  set(seriesHandle, 'TooltipString', 'Beams shown as a Matlab matrix');
end
