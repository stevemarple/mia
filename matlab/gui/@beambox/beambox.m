function h = beambox(varargin)
%BEAMBOX Make an box for entering beams.
%
%   h = BEAMBOX(parent, pos, image, wide)
%   h = BEAMBOX(parent, pos, image, wide, title)
%   h = BEAMBOX(parent, pos, image, wide, title, imageallow, wideallow)
%   h = BEAMBOX(parent, pos, image, wide, title, ...
%               imageallow, wideallow, PARAM_NAME, PARAM_VALUE ...)
%   h: handle to uicontrol
%   parent: handle to parent FIGURE
%   pos: position vector
%   image: imaging beam numbers
%   wide: widebeam number(s)
%   title: title string to append after 'Select beams'
%   imageallow: imaging beam numbers which may be selected
%   wideallow: widebeam number(s) which may be selected
%
%   If imageallow and wideallow are given, the values should be members of
%   the sets image and wide, errors will result if they are not!
%
%   Additional parameters may be specified by means of the parameter
%   name/value method. Value parameters are:
%
%      'limitedbeams', n 
%      Limit selection to a maximum of n beams only. If n is negative then
%      once -n beams have been selected none can be unselected. In both
%      cases, attempting to select > abs(n) will unselect the earliest
%      selected beam.
%
%   If you wish to have a callback run after the beams have changed it is
%   not possible to simply append a callback to all the BEAMBOX
%   uicontrols - pressing the map button brings up a new figure whose
%   controls will not have the callback. Instead use the member function
%   APPENDCALLBACK.
%
%   See also GETBEAMBOX, APPENDCALLBACK.
%
%
%   This is a generic class which will work for any number of imaging beams
%   (though it currently only knows how to draw the map for a square
%   number). It will also work for any number of widebeams. The beams are
%   drawn in the map left to right, top row to bottom row. The number
%   given to each beam is taken from the corresponding place in the
%   imagingbeam/widebeam matrix. They do not have to be numbered 1->49!
%   2:2:98 also works!

cls = 'beambox';

% don't bother with a version number because objects of this class should
% not be saved.

% make this generic so it can easily be used for systems with different
% numbers of beams
% member fields
bb.imaging = [];   % beam numbers of the imaging beams
bb.widebeam = [];  % beam number(s) of the wide beam(s)
bb.imagingallow = []; % if set, 
bb.widebeamallow = [];
bb.badbeams = []; % beams deemed to be bad (shown as red)
bb.maphandle = []; % class object stored in user data of 'map' button
bb.serieshandle = [];
bb.beams = [];     % selected beams
bb.title = [];
bb.userdata = [];  % give any encapsulating classes somewhere to store data 
bb.callback = [];  % if set a callback string to execute after updating
bb.limitedbeams = 0;
bb.selectionorder = [];

defaults.position = [];
defaults.parent = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  default.position = [1 1 1 1];
  defaults.parent = gcf
  bb = class(bb, cls);
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor 
  bb = varargin{1};
  % not sure this is a valid action for the implmentation. All the same
  % allow the object to be duplicated exactly (matlab may need it for
  % loading functions or something)
  return; 
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface

  defaults2 = bb; % copy the class member fields
  fn = fieldnames(defaults);
  % copy all other options
  for n = 1:length(fn)
    defaults2 = setfield(defaults2, fn{n}, getfield(defaults, fn{n}));
  end
  
  [bb2 unvi] = interceptprop(varargin, defaults2);
  % defaults should not contain any extraneous fields
  defaults = rmfield(bb2, fieldnames(bb));
		 
  % want position to be specified, hence the check later
  bb = rmfield(bb2, fn);
  if length(unvi)
    warning(['The following parameter(s) have been ignored:', ...
	     sprintf(' %s', varargin{unvi(1:2:end)})]);
  end
  
  bb = class(bb, cls);

else
  error('incorrect parameters');
end

if bb.limitedbeams ~= 0
  bb.selectionorder = repmat(nan, 1, abs(bb.limitedbeams));
  % copy the most recent beams into the selection order box
  n = min(abs(bb.limitedbeams), numel(bb.beams));
  bb.selectionorder(1:n) = bb.beams(1:n);
else
  bb.selectionorder = [];
end

if isempty([bb.imaging bb.widebeam])
  error('No beams!');
end
if isempty(defaults.position)
  error('position not specified');
end
if isempty(defaults.parent)
  error('parent not specified')
elseif length(defaults.parent) ~= 1 
  error('parent must be a scalar')
end
[bb.maphandle bb.serieshandle] = makebeambox(bb, defaults.parent, ...
					     defaults.position);

% store the data once, and store the path back to the data for the other
% controls 
set(bb.maphandle, 'UserData', bb);
set(bb.serieshandle, 'UserData', bb.maphandle);

% set the callbacks
set(bb.maphandle, 'Callback', 'beamboxcb(getbeambox(gcbo),''map'');');
set(bb.serieshandle, 'Callback', 'beamboxcb(getbeambox(gcbo),''series'');');


h = bb.maphandle;








