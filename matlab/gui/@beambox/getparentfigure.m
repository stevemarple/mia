function h = getparentfigure(bb)
%GETPARENTFIGURE  Return the parent figure of a BEAMBOX object
%
%   h = GETPARENTFIGURE(bb)
%   h: handle of figure in which BEAMBOX is placed
%   bb: BEAMBOX object
%
%   When the Map button is pressed the BEAMBOX object creates a new
%   window. If a callback has been set with the APPENDCALLBACK function
%   then the callback figure (as returned by GCBF) is the map window
%   figure, not the original figure in which the BEAMBOX object was
%   placed. This function provides a means by which the parent figure can
%   always be accessed, use as
%
%   fh = getparentfigure(getbeambox(gcbo));
%
%   See also BEAMBOX, GCBF, GCBO, APPENDCALLBACK.

h = get(bb.maphandle, 'Parent');
