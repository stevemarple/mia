function h = gethandles(bb)
%GETHANDLES  Return the handles used by a BEAMBOX object
%
%   h = GETHANDLES(bb)
%   h: handles (row matrix)
%   bb: BEAMBOX object
%
%   Return the handles of UICONTROL objects used by a BEAMBOX. *Warning*,
%   it is easy to break the encapsulation, this function should be used
%   with care, only for tasks such as enabling/disabling the uicontrols.
%
%   See also BEAMBOX, UICONTROL.

h = [bb.maphandle; bb.serieshandle];

