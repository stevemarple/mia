function[AllBeams, FPGAHopfTime, FPGAinfo] = read_arcom_file(filename, varargin)
%READ_ARCOM_FILE
%
%   [data FPGA_Hopf_time FPGA_info] = READ_ARCOM_FILE(filename)
%   data: DOUBLE matrix of the data, complex and untrimmed
%   FPGA_Hopf_time: timestamp information
%   FPGA_info: information from the FPGA


%       Type is the riometer data type
%       type = 1 (AIRIS)
%       type = 2 (ARIES)
%       type = 3 (power)

deprecated('Please use read_arcom_file2');

switch nargin
 case 1
  % don't pre-allocate array sizes
  rio = [];
  calibdata = [];
  
  case 2
  rio = varargin{1};
  calibdata = [];

 case 3
  rio = varargin{1};
  calibdata = varargin{2};
  
 otherwise
  error('incorrect parameters');
end

if ~isempty(rio)
  df = info(rio, 'defaultfilename', 'original_format');
  AllBeams = zeros(df.size);
  FPGAHopfTime = repmat(struct('status', [], ...
			       'weekday', [], ...
			       'date', []), 1, df.size(2));
  FPGAinfo = repmat(struct('SamplesIntegrated', [], ...
			   'ADCconversions', [], ...
			   'NoiseModeSet', [], ...
			   'NoiseActiveFlag', [], ...
			   'NoiseOnCnt', [], ...
			   'NoiseOffCnt', [], ...
			   'Version', []), 1, df.size(2));
end

% set size of variables
% switch type 
%     case 1
%         AllBeams = repmat(complex(NaN,NaN),[64 800]);
%     case 2
%         AllBeams = repmat(complex(NaN,NaN),[1024 800]);
%     case 3
%         AllBeams = repmat(complex(NaN,NaN),[64 800]);
%     otherwise
%       disp('Invalid Type Parameter') ; 
% end 

ValidPacketTypes = {'0011' '0012' };

% Do hex conversions outside loop for speed improvement                    
Magic_Dec = hex2dec('ABDE');

for n = 1:length(ValidPacketTypes)
  ValidPacketTypes_Dec(n) = hex2dec(ValidPacketTypes{n}); 
end

%--------------------------------------------------------------------------
% Open file
[fid mesg] = url_fopen(filename, 'r');
if (fid == -1)
  error(sprintf('cannot open %s: %s', filename, mesg));
end 

% disp('NFS debug'); eval('toc',''); tic 
disp(['loading ' filename]);
bindex = 0; % beam index
	    
%--------------------------------------------------------------------------
% Scan for magic and packet type
while ~feof(fid)
  Magic = fread(fid, 1, 'uint16');
  PacketType = fread(fid, 1, 'uint16');    
  
  if feof(fid)
    break;
  end
  
  MagicFlag = (Magic == Magic_Dec);
  PacketValid = any(PacketType == ValidPacketTypes_Dec);
  
  
  if ~MagicFlag
    ; % begin scan again (do nothing here)
    % disp('no magic');
    
  elseif ~PacketValid
    % wrong type of packet, skip over the rest of it
    PacketSize = fread(fid, 1, 'uint32'); % total packet size              
    fseek(fid,PacketSize - 8,'cof');
    % disp('wrong type of packet');
    
  else
    % valid packet of the correct type
    % disp('valid packet');
    
    [beams_real, beams_imag, SingleFPGAHopfTime, SingleFPGAinfo] = LocalExtractFPGADataPacket(fid);
    
    % copy data if:
    % calibdata is empty (copy everything)
    % calibdata = 1 and it is calibration data
    % calibdata = 0 and it is not calibration data
    % SingleFPGAinfo.NoiseActiveFlag
    if isempty(calibdata) | ... 
	  (~calibdata & SingleFPGAinfo.NoiseActiveFlag == 0) | ...
      (calibdata & SingleFPGAinfo.NoiseActiveFlag ~= 0)
      bindex = bindex+1;
      AllBeams(:,bindex)       = complex(beams_real,beams_imag);
      FPGAHopfTime(:,bindex)   = SingleFPGAHopfTime;          
      FPGAinfo(:,bindex)       = SingleFPGAinfo;          
    end
    
  end
end

% trim to correct size
if size(AllBeams, 2) ~= bindex
  AllBeams = AllBeams(:,1:bindex);
  FPGAHopfTime = FPGAHopfTime(1:bindex);
  FPGAinfo = FPGAinfo(1:bindex);
end

fclose(fid);
return;
       

%--------------------------------------------------------------------------
function[beams_real, beams_imag, SingleFPGAHopfTime, SingleFPGAinfo] = LocalExtractFPGADataPacket(fid)
% Valid packet
%   disp('Valid Packet--in sub function')
PacketSize  = fread(fid, 1, 'uint32');% Total packet Size including headers
CheckSum    = fread(fid, 1, 'uint32');  % Not used. Set to 0xDEADBEEF
endpacket = 0;

packet_bytes_read = 12;

% while ~endpacket
while packet_bytes_read < PacketSize
  
  Descriptor = fread(fid, 1, 'uint8');  % Descriptor val
  D_size     = fread(fid, 1, 'uint16'); % Descriptor size
  
  packet_bytes_read = packet_bytes_read + D_size;
  
  %disp(strcat('Descriptor : ',dec2hex(Descriptor),'as dec' ,num2str(Descriptor) ,'file check',num2str(feof(fid))))
  
  % disp(sprintf('read_arcom_file: descriptor=0x%x', Descriptor));
  switch Descriptor
   case (10) % 0xA     ARIES
    % disp('read_arcom_file: ARIES descriptor');
    croppedID                 = fread(fid, 1, 'uint8');  % zero byte
    beams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    beams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
    switch croppedID
     case {111 112}
      % some beams are bad
      idx = [1:107 118:137 152:168 185:199 218:230 251:261 284:292 317:324 ...
	     349:355 382:387 414:419 446:451 478:483 510:515 542:547 ...
	     574:579 606:611 638:643 670:676 701:708 733:741 764:774 ...
	     795:807 826:840 857:873 888:907 918:1024];
      beams_real(idx) = nan;
      beams_imag(idx) = nan;
    end
    
   case (11) % 0xB     AIRIS
    % disp('read_arcom_file: AIRIS descriptor');
    dummybyte                 = fread(fid, 1, 'uint8');  % zero byte
    beams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    beams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
   
   case (7) % 0x7      Power
    dummybyte                 = fread(fid, 1, 'uint8');  % zero byte
    beams_real                = fread(fid, (D_size/4 -1)/4, 'int64');
    beams_imag                = fread(fid, (D_size/4 -1)/4, 'int64');
   
   case (8) % 0x8 HOPF Time
    dummybyte  = fread(fid, 1, 'uint8');  % zero byte
    SingleFPGAHopfTime.status     = fread(fid, 1, 'uint8');
    SingleFPGAHopfTime.weekday    = fread(fid, 1, 'uint8'); 
    tenhour    = fread(fid, 1, 'uint8');
    unithour   = fread(fid, 1, 'uint8');

    tenmin     = fread(fid, 1, 'uint8');
    unitmin    = fread(fid, 1, 'uint8');
    tensec     = fread(fid, 1, 'uint8');
    unitsec    = fread(fid, 1, 'uint8');

    tenday     = fread(fid, 1, 'uint8');
    unitday    = fread(fid, 1, 'uint8');
    tenmonth   = fread(fid, 1, 'uint8');
    unitmonth  = fread(fid, 1, 'uint8');
    tenyear    = fread(fid, 1, 'uint8');
    unityear   = fread(fid, 1, 'uint8');                            
    
    sec         =      ((tensec-'0')*10)  + (unitsec -'0');
    min         =      ((tenmin-'0')*10)  + (unitmin -'0');
    hour        =      ((tenhour-'0')*10) + (unithour -'0');
    day         =      ((tenday-'0')*10)  + (unitday -'0');    
    month       =      ((tenmonth-'0')*10)+ (unitmonth -'0');
    year        = 2000+((tenyear-'0')*10) + (unityear  -'0');
    
    SingleFPGAHopfTime.date = [year month day hour min sec];                            
    dummyread                 = fread(fid, 1,'uint16');
   
   case (9) % 0x9 INFO
    % disp('read_arcom_file: Info descriptor');
    dummybyte                        = fread(fid, 1, 'uint8');  % zero byte
    SingleFPGAinfo.SamplesIntegrated = fread(fid, 1,'uint32');
    SingleFPGAinfo.ADCconversions    = fread(fid, 1,'uint32');
    % SingleFPGAinfo.NoiseStatus1     = fread(fid, 1,'uint8 => double');
    % SingleFPGAinfo.NoiseStatus2     = fread(fid, 1,'uint8 => double');
    SingleFPGAinfo.NoiseModeSet      = fread(fid, 1,'ubit1');
    dummybits                        = fread(fid, 1,'ubit11');

    % NoiseActiveFlag is active low, so invert
    % SingleFPGAinfo.NoiseActiveFlag   = fread(fid, 1,'ubit1');
    SingleFPGAinfo.NoiseActiveFlag   = ~fread(fid, 1,'ubit1');
    
    dummybits                        = fread(fid, 1,'ubit3');
    SingleFPGAinfo.NoiseOnCnt        = fread(fid, 1,'uint16');                            
    SingleFPGAinfo.NoiseOffCnt       = fread(fid, 1,'uint32');
   
   case (16) %0x10 VERSION
    SingleFPGAinfo.Version           = fread(fid, 1, 'uint8');  % zero byte
    endpacket = 1;
   
   otherwise
    disp(sprintf('error: unknown packet descriptor (was 0x%x)', Descriptor));
    % return;
    % skip D_size - 3 bytes
    fseek(fid, D_size - 3, 'cof');
  end 
  
end

% assertion check
% if packet_bytes_read ~= PacketSize
%   disp('not read all bytes from the packet!');
% end

beams_real = beams_real ./ SingleFPGAinfo.SamplesIntegrated;
beams_imag = beams_imag ./ SingleFPGAinfo.SamplesIntegrated;


