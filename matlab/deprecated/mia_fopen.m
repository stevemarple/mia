function [fid, mesg] = mia_fopen(varargin)
%MIA_FOPEN  Convenient replacement for FOPEN which performs error checking.
%
% fid = MIA_FOPEN(...)
% Equivalent to FOPEN but check for errors. If an error opening the file
% occurs then ERROR is called with an appropriate erro message.
%
% [fid, mesg] = MIA_FOPEN(...)
% When the mesg return value is requested MIA_FOPEN is entirely equivalent
% to FOPEN, and no error checking is performed.
%
% See FOPEN.

deprecated('Please use url_fopen');

[fid mesg] = fopen(varargin{:});
if fid == -1 & nargout < 2
  error(sprintf('Could not open %s: %s', varargin{1}, mesg));
end
