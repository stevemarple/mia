function r = mia_tempdir
%MIA_TEMPDIR  Get the name of the user's personal temporary directory

deprecated('Please use tempdir');

r = getenv('MIA_TEMPDIR');
if isempty(r)
  r = tempdir;
end
