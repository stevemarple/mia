function h = surface2(varargin)
%SURFACE2 Deprecated alternative to SURFACE. See SURFACEIMAGE.

deprecated('Please use surfaceimage');
h = surfaceimage(varargin{:});
