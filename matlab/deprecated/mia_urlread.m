function [s,status,mesg] = mia_urlread(url)
%MIA_URLREAD  MIA-equivalent of Matlab's URLREAD.
%
% [s status, mesg] = MIA_URLREAD(url);

deprecated('Please use url_read');
[s,status,mesg] = url_read(url);

