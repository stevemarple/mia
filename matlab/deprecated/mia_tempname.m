function r = mia_tempname(varargin)
%MIA_TEMPNAME  Get a temporary filename.
%
%   r = MIA_TEMPNAME;
%   r = temporary file name in MIA_TEMPDIR (CHAR)
%
%   r = MIA_TEMPNAME(dir);
%   r = temporary file name in dir (CHAR)
%   dir: alternative directory to use, which must exist (CHAR)
%
%   r = MIA_TEMPNAME(dir, ext);
%   r = temporary file name in dir (CHAR)
%   dir: alternative directory to use, which must exist (CHAR)
%   ext: filename extension (including leading '.')
%
% If dir is empty then MIA_TEMPDIR is used. Use '.' to specify the
% current directory. MIA_TEMPDIR tests if the filename it produces
% exists, if so new names are produced until one is found which does not
% exist on the selected directory.

deprecated('Please use tempname2');
r = tempname2(varargin{:});
