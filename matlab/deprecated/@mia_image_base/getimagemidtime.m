function r = getimagemidtime(mia, varargin)

deprecated('Please use getsampletime');

r = getsampletime(mia, varargin{:});


