function r = getimagetime(mia, varargin)

deprecated('Please use getsampletime and getintegrationtime as appropriate');

r = getsamplestarttime(mia, varargin{:});

