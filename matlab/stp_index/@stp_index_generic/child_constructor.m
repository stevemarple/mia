function mia = child_constructor(varargin)
%STP_INDEX_GENERIC  Constructor for STP_INDEX_GENERIC class.
%
%   r = STP_INDEX_GENERIC
%   default constructor
%
%   r = STP_INDEX_GENERIC(mia)
%   copy constructor
%
%   r = STP_INDEX_GENERIC(...)
%   constructor taking parameter name/value pairs
%
%   The following parameters are accepted.
%
%     'name', CHAR
%     Index name
%

% Get class name automatically - can then use this constructor for other
% classes
[tmp cls] = fileparts(mfilename); 
parent = 'stp_index_generic';

latestversion = 1;
mia.versionnumber = latestversion;

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
  
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};
    
elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  a = varargin{1};
  requiredFields = {parent, 'versionnumber'};
  for n = 1:length(requiredFields)
    if ~isfield(varargin{1}, requiredFields{n})
      error(sprintf('need a %s field', requiredFields{n}));
    end
  end

  fn = setdiff(intersect(fieldnames(a), fieldnames(mia)), {parent});
  tmp = mia;
  mia = repmat(mia, size(a));
  mia = class(mia, cls, feval(parent));
  
  % copy common fields (not base class)
  for n = 1:prod(size(a))
    tmp2 = tmp;
    an = a(n);
    for m = 1:length(fn)
      % tmp = setfield(tmp, fn{m}, getfield(a(n), fn{m}));
      tmp2 = setfield(tmp2, fn{m}, getfield(an, fn{m}));
    end

    base = getfield(an, parent); % parent class should already be current
    switch tmp2.versionnumber
     case 1
      ; % latest

     otherwise
      error('unknown version');
    end
    tmp2 = class(tmp2, cls, base);
    mia(n) = tmp2;
  end

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia; % take on standard settings
  defaults.load = []; % load from disk or create new
  defaults.loadoptions = {};
  defaults.log = 1;
  
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi}, 'load', 0);
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = miaload(mia);
    if defaults.log
      mialog(mia);
    end
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
