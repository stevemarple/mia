function r = gettype(mia, varargin)
%GETTYPE  Return index data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: STP_INDEX_GENERIC object
%
%   See also STP_INDEX_GENERIC.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end


r = getfield(getfield(index_data(mia), 'type'), mode);






