function r = index_data(mia)


d.ap_index.type.l = 'Ap index';
d.ap_index.type.u = 'Ap INDEX';
d.ap_index.type.c = 'Ap index';
d.ap_index.type.C = 'Ap Index';
d.ap_index.resolution = timespan(3, 'h');
d.ap_index.url = {'http://ukssdc-ag2.stp.rl.ac.uk:8080/UKSSDC-wdcdata/BrowserAskQuery?AdqlSql=select+t1.datetime%%2Ct1.ap+from+ap_3hourly+as+t1+where+t1.datetime+%%3E%%3D+%%27%Y-%m-%dT%H%%3A%M%%3A%S%%2B00%%27+and', ...
		  '+%%0D%%0At1.datetime+%%3C+%%27%Y-%m-%dT%H%%3A%M%%3A%S%%2B00%%27&Format=Comma-Separated&Compression=NONE&TargetResponse=true&TargetURI=&Ask=Submit+Query'};	


d.kp_index.type.l = 'Kp index';
d.kp_index.type.u = 'Kp INDEX';
d.kp_index.type.c = 'Kp index';
d.kp_index.type.C = 'Kp Index';

r = getfield(d, class(mia));
