function [plottitle, windowtitle] = maketitle(miaIn, varargin)
%MAKETITLE  Make plot and figure titles.
%
%   [plottitle, windowtitle] = maketitle(mia)
%   [plottitle, windowtitle] = maketitle(mia, ...)
%
%   Create the titles based on current time/date, data type etc. Various
%   values may be overriden by specifying a parameter name and value (see
%   INTERCEPTPROP). The defaults values are created from the IRISFILEBASE
%   object (or an object derived from IRISFILEBASE). The valid parameters
%   which may be overriden are described below:
%
%     'customstring', string
%        Observe chosen style but override the string printed in
%        bold. Useful for Keogram plots and similar.
%
%     'starttime', TIMESTAMP
%
%     'endtime', TIMESTAMP
%
%     'resolution', TIMESPAN
%     'resolution', CHAR
%        If the resolution is given as a TIMESPAN then it is converted to
%        a suitable string. If it is a CHAR array then it is printed
%        without change.   
%
%     'comment', comment
%        An optional comment, to be printed at the bottom of the title
%
%   See also IRISFILEBASE, IMAGEDATA, MAKEPLOTFIG, GETNAME, PRINTSERIES.

plottitle = '';
windowtitle = '';

mia = miaIn(1);



% general format is
% Power/time (beam 1)
% 00:00:00 - 12:00:00 UT 1/1/1999 @ 1 m res.
% Kilpisjarvi, Finland (69.05N, 20.79E)
% [comment]
%
% If step ~= 1 then print that and resolution on separate line to time
% formatStr = '{\\bf %s} %s\n%s%s\n%s%s';
formatStr = { ...
    '%s %s' ...
    '%s%s' ...
    '%s%s' };

% entries are:

dateStr = '';
resStr = '' ; % including step size if appropriate

instrument = getinstrument(mia);

% default values
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
defaults.resolution = getresolution(mia);
defaults.customstring = '';
defaults.comment = '';

if nargin > 1
  [defaults unvi] = interceptprop(varargin, defaults);
end


dateStr = dateprintf(defaults.starttime, defaults.endtime);
if ischar(defaults.resolution)
  resStr = defaults.resolution;
else
  resStr = [' @ ' char(defaults.resolution, 'c') ' res.'];
end


plottitle = {'{\bf ' gettype(mia, 'C') '}', ...
	     dateStr};
windowtitle = gettype(mia, 'C');

