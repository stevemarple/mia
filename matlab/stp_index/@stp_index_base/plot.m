function [fh, gh, ph2, prelimH, mult] = plot(mia, varargin)
%PLOT  Plot data from a STP_INDEX_BASE object.
%
%   PLOT(mia)
%   PLOT(mia, ...)

fh = [];
gh = [];
ph2 = [];
prelimH = [];
mult = [];

defaults.miniplots = []; % [1 length(defaults.components)];
defaults.spacing = [];
defaults.title = '';
defaults.comment = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.ylim = [nan nan];
defaults.heater_data = [];
defaults.pointer = 'arrow';

defaults.preliminary = '';
[defaults unvi] = interceptprop(varargin, defaults);

if isempty(defaults.miniplots)
  defaults.miniplots = [1 1];
end

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  if any(~ishandle(gh))
    error('invalid handle');
  end

  args = varargin(unvi);
  fh = get(gh, 'Parent');
  if iscell(fh)
    fh = cell2mat(fh);
  end
  fh = unique(fh);
else
  gh = [];
  args = varargin(unvi); % args = {varargin{unvi}};
end

[title windowtitle] = maketitle(mia, 'comment', defaults.comment);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc if isempty(gh)
% get a figure window decorated with the standard controls.
% except for miniPlots, allow other functions to override
if isempty(gh)
  if prod(defaults.miniplots) > 1
    defaultSpacing = [0.2 0.3 0.2 0.3 0.2 0.3];
    if isempty(defaults.spacing)
      defaults.spacing = defaultSpacing;
    end
    defaults.spacing(isnan(defaults.spacing)) = ...
	defaultSpacing(isnan(defaults.spacing));
  end

  [fh gh] = makeplotfig('init', args{:}, ...
			'spacing', defaults.spacing, ...
			'miniplots', defaults.miniplots, ...
			'pointer', 'watch', ...
			'linkedzoom', 1, ...
			'title', defaults.title);
end

data = getdata(mia);
timing = gettiming(mia);
switch timing
 case 'centred'
  xdata = 0:1:(size(data, 2)-1);
 case 'offset'
  xdata = 0.5:1:size(data, 2);
 otherwise
  error(sprintf('unknown timing type (was %s)', timing));
end

keepKeys = {'Tag' 'UserData'};
keepVal = get(gh(1), keepKeys); 

% preliminary should appear behind the line as it has a Z value of -1,
% but matlab 6.5 seems to have a bug, and places the last object on
% top, so do any preliminary text first.
if ~isempty(defaults.preliminary) & ispreliminary(mia)
  prelimH = text('Parent', gh(1), ...
		 'Position', [0.5 0.5 -1], ...
		 'Units', 'normalized', ...
		 'FontUnits', 'points', ...
		 'FontSize', 30, ...
		 'Color', 'r', ...
		 'Interpreter', 'none', ...
		 'HorizontalAlignment', 'center', ...
		 'VerticalAlignment', 'middle', ...
		 'String', defaults.preliminary, ...
		 'Tag', 'preliminary');
end

      
ph = line('Parent', gh(1), ...
	  'XData', xdata, ...
	  'YData', data(1,:), ...
	  'Color', defaults.color, ...
	  'Tag', gettype(mia)); 

set(gh(1), keepKeys, keepVal, 'Visible', 'on');

set(ph, 'Tag', gettype(mia));



set(gh, 'XLim', [0 size(data,2)], 'Box', 'on');
% timetick(ifb, gh, 'X', 0);  % don't have the axes labelled

if any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  curLim = get(gh(1), 'YLim');
  curLim(notNans) = defaults.ylim(notNans);
  set(gh(1), 'YLim', curLim);
  
elseif ~isempty(defaults.ylim)
  set(gh(1), 'YLim', defaults.ylim);
end

% -------
if isempty(defaults.plotaxes) 
  if exist('heatermenu')
    % make tools menus visible
    toolsMenuHandle = findall(fh, 'Tag', 'figMenuTools', 'Type', 'uimenu');
    if isempty(toolsMenuHandle)
      toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
    end
    
    if ~isempty(toolsMenuHandle)
      set(toolsMenuHandle, 'Visible', 'on');
      heatermenu('init', toolsMenuHandle, ...
		 'onofftimes', 'on', ...
		 'heater_data', defaults.heater_data);
    end
  end

end

set(get(gh(1), 'YLabel'), ...
    'String',['\bf ' datalabel(mia)], ...
    'FontSize', 14);


    
timetick(mia, gh(1), 'X', 'UT');

if isempty(defaults.plotaxes)
  set(fh, 'Name', defaults.windowtitle, ...
	  'Pointer', defaults.pointer);
end


return;





