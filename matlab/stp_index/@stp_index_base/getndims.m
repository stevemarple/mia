function r = getndims(mia)
%GETNDIMS  Return number of dimensions for data array
%
%   r = GETNDIMS(mia)
%   r: scalar
%   mia: STP_INDEX_BASE object

r = 2;
