function r = gettype(mia, varargin)
%GETTYPE  Return Kp data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: KP_INDEX object
%
%   See also KP_INDEX.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'Kp index';
  
 case 'u'
  r = 'Kp INDEX';
  
 case 'c'
  r = 'Kp index';
  
 case 'C'
  r = 'Kp Index';
 
 otherwise
  error('unknown mode');
end



