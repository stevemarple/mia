function r = info_cam_fsmi_1_data
%INFO_CAM_FSMI_1_DATA Return basic information about cam_fsmi_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_fsmi_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=fsmi;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'fsmi';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'THEMIS';
r.facility_url = 'http://aurora.phys.ucalgary.ca/themis/';
r.facilityid = 10;
r.groupids = [];
r.id = 19;
r.latitude = 60;
r.location1 = 'Fort Smith';
r.location1_ascii = 'Fort Smith';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 248.1;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
