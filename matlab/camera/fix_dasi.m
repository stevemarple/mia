t = timestamp([1996 9 30 0 0 0])

bs = timespan(1,'h');
while t < timestamp([1998 5 1 0 0 0])
  try 
    t2 = t + bs;
    mia = camera_image('starttime', t, ...
		       'endtime', t2, ...
		       'instrument', cam_dasi, ...
		       'load', 1);
    ts = char(t);
    disp(['loaded ' ts]);
    save(mia);
    disp(['saved ' ts]);
  catch
    if strcmp(lasterr, 'Interrupt');
      error(lasterr);
    end
  end
  t = t2;
end
