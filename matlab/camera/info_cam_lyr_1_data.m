function r = info_cam_lyr_1_data
%INFO_CAM_LYR_1_DATA Return basic information about cam_lyr_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_lyr_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=lyr;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'lyr';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 9;
r.latitude = 78.2;
r.location1 = 'Longyearbyen';
r.location1_ascii = 'Longyearbyen';
r.location2 = 'Svalbard';
r.logo = '';
r.logurl = '';
r.longitude = 15.7;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
