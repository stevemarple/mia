function r = info_cam_han_1_data
%INFO_CAM_HAN_1_DATA Return basic information about cam_han_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_han_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=han;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'han';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 4;
r.latitude = 62.3;
r.location1 = 'Hankasalmi';
r.location1_ascii = 'Hankasalmi';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 26.65;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
