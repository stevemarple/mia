% echo "create temporary table tmp (time, cloud) as select round(time, interval '10s'), cloud from cloud where cloud notnull order by time;select extract(epoch from time), cloud from tmp;" | psql irisevents > cloudy_by_unixtime

% echo "create temporary table tmp (time, cloud) as select round(time, interval '10s'), cloud from scasi_cloud where cloud notnull order by time;select extract(epoch from time), cloud from tmp;" | psql irisevents > cloudy_by_unixtime

function fixclouds(y)
%FIXCLOUDS  Fix cloud data
%
%    FIXCLOUDS(year)
%
%    year: integer year, or 0 for all years
%

% y = 1998;

switch y
 case 0
  load cloudy_by_unixtime;

 otherwise
  tmp = sprintf('cloudy_by_unixtime%04d', y);
  load(tmp);
  eval(sprintf('cloudy_by_unixtime = %s;', tmp));
  clear(tmp);
  
  disp('******************************************************')
  disp(sprintf('FIXING FOR %04d', y));
  disp('******************************************************')
end

size(cloudy_by_unixtime)

cloudtime = cloudy_by_unixtime(:, 1);
cloud = cloudy_by_unixtime(:, 2);
clear cloudy_by_unixtime;

resSecs = 10;
res = timespan(resSecs, 's');

epoch = timestamp([1970 1 1 0 0 0]);
fstr = '/miadata/camera/dasi/%Y/%m/%d/%Y%m%d%H.mat';

lastFileLoaded = '';
mia = [];
cloudvec = [];

n1 = 20;
sprintf('start at %d', n1);

cloud_data_valid = timespan(3, 'm');

for n = n1:length(cloudtime)
  t = epoch + timespan(cloudtime(n), 's');
  t1 = t + cloud_data_valid;
  % disp(char(t))
  fname = strftime(t, fstr);
  
  process = 0;
  if strcmp(lastFileLoaded, fname)
    process = 1;
  else
    localSave(mia, cloudvec);
    mia = [];
    if exist(fname)
      try
	load(fname);
      catch
	error(sprintf('Cannot load %s', fname));
      end
      cloudvec = getcloud(mia);
      lastFileLoaded = fname;
      process = 1;
    else
      lastFileLoaded = '';
    end
  end

  if process
    it = getimagetime(mia);
    % idx = find(it == t);
    idx = find(it >= t & it < t1);
    cloudvec(idx) = cloud(n);
  end
end

localSave(mia, cloudvec);
mia = [];

function localSave(mia, cloudvec)
if ~isempty(mia)
  mia = setcloud(mia, cloudvec);
  
  %   % attempt to fix coordinate values
  %   [x y] = getpixels(mia);
  %   x2 = 13.5:0.25:26;
  %   y2 = 67.6:0.1:72.6;
  %   if all(abs(x-x2) < 0.0001)
  %     disp('fixing xpixels');
  %     mia = setxpixelpos(mia, x2);
  %   end
  %   if all(abs(y-y2) < 0.0001)
  %     disp('fixing ypixels');
  %     mia = setypixelpos(mia, y2);
  %   end

  save(mia);
end
 
