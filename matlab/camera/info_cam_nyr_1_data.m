function r = info_cam_nyr_1_data
%INFO_CAM_NYR_1_DATA Return basic information about cam_nyr_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_nyr_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=nyr;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'nyr';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 13;
r.latitude = 62.3426;
r.location1 = 'Nyrola';
r.location1_ascii = 'Nyrola';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 25.5099;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
function r = info_cam_nyr_1_data
%INFO_CAM_NYR_1_DATA Return basic information about cam_nyr_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_nyr_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=nyr;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'nyr';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'NORSTAR';
r.facility_url = 'http://aurora.phys.ucalgary.ca/norstar/';
r.facilityid = 11;
r.groupids = [];
r.id = 30;
r.latitude = 62.34;
r.location1 = 'Nyroelae';
r.location1_ascii = 'Nyroelae';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 25.51;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([2005 05 14 23 00 00]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
