function r = char(in)
%CHAR  Convert a CAMERA object to a CHAR.
%
%   r = CHAR(in)

% NB Make all objects derived from mia_instrument_base print a trailing
% newline character 

if length(in) ~= 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_instrument_base_char(in);
  return;
end

% print scalar camera
r = mia_instrument_base_char(in);

