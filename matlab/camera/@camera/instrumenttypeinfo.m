function varargout = instrumenttypeinfo(in, info, varargin)
%INSTRUMENTTYPEINFO  Return information about the instrument type.
%
%   r = INSTRUMENTTYPEINFO(cam, 'supported')
%   r = INSTRUMENTTYPEINFO(cam, 'supported', 'camera_image')
%
%   cam: any camera instrument
%   Return a list of supported cameras. If the optional data type is
%   supplied the list is restricted to supported cameras for that
%   datatype. Support for cam_image depends on the image type
%   (absorption, power etc).
%
%   See also CAMERA, camera/INFO.

switch char(info)
 case 'abbreviation'
  varargout{1} = 'cam';
  
 case 'aware'
  % indicate which instruments MIA is aware of, even if they are not
  % supported by MIA (useful for plotting)
  s = cam_instrumenttypeinfo_data;
  varargout{1} = s.aware;
  
 case 'imageclass'
  varargout{1} = 'cam_image';
  
 case 'supported'
  % mfilename(in, 'supported')
  % mfilename(in, 'supported', 'datatype', camera_image)
  
  % list supported instruments. Optionally take the 'datatype' for which
  % support is required
  
  defaults.datatype = 'any';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  s = cam_instrumenttypeinfo_data;
  if ~isfield(s.supported, defaults.datatype)
    error(sprintf('do not have information for data type ''%s''', ...
		  defaults.datatype));
  end
  varargout{1} = getfield(s.supported, defaults.datatype);
  
 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end
  
