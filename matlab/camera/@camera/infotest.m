function infotest(cam)
%INFOTEST  Test function for camera/INFO.
%
%   INFOTEST(cam)
%
%   INFOTEST tests all of the camera info commands. cam may be a
%   vector or array, in which case all CAMERA objects are tested.
%
%   See also CAMERA, camera/INFO.

sz = prod(size(cam));

if length(cam) ~= 1
  for n = 1:sz
    disp(sprintf('testing %s...', getabbreviation(cam(n))));
    feval(mfilename, cam(n));
  end
  return;
end


% fn: filename
% bs: block size
% fifm: fail if file missing
[fn bs fifm format loadfunction] = info(cam, 'defaultfilename');

res = info(cam, 'bestresolution');
[x y]= info(cam, 'fov', 'units', 'deg');

