function varargout = camera_info_scasi(cam, info, varargin)

if getserialnumber(cam) ~= 2
  error(sprintf('Do not have details for serial number #%d', ...
		getserialnumber(cam)));
end

% From: http://www.dcs.lancs.ac.uk/iono/dasi/
%
% The image is then transformed into a geographic grid of 67.6-72.6 North in
% steps of 0.1 degrees and 13.5-26.0 East in steps of 0.25 degrees at an
% altitude of 100 km.
xpixels = 67.6:0.1:72.6;
ypixels = 13.5:0.25:26.0;

switch info
 case 'bestresolution'
  varargout{1} = timespan(nan,'s');
  
 case 'defaultfilename'
  % filename location
  varargout{1} = fullfile(camera_datadir(cam), '%Y', ...
			  '%m', '%d', '%Y%m%d%H%M%S.st6');
  varargout{2} = timespan(1,'d'); % treat in 1d chunks (1 day per dir)
  varargout{3} = 0; % irrelevant for ST-6 format if file missing
  varargout{4} = 'st'; % ST-6 format
  varargout{5} = 'scasi2mia_image'; % name of load function
  
 case 'fov'
  [varargout{1}, varargout{2}] = calcfov(cam, varargin{:});
  
 case 'pixels'
  clear defaults;
  defaults.units = '';
  [defaults unvi] = interceptprop(varargin, defaults);
  
  switch defaults.units
   case 'deg'
    disp('fix SCASI info file for ''pixels''');
    varargout{1} = 15:0.5:23.0;
    varargout{2} = 68:0.2:72.2;
   case ''
    error('units not specified');
   otherwise
    error(sprintf('unknown unit (was %s)', defaults.units));
  end
  
 otherwise
  error(sprintf('unknown information request (was %s)', info));
end

