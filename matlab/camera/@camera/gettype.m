function r = gettype(in, varargin)
%GETTYPE  Return instrument type.
%
%   r = GETTYPE(r)
%   r: CHAR
%   in: CAMERA object
%
%   See also MIA_INSTRUMENT_BASE, CAMERA.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'camera';
  
 case 'u'
  r = 'CAMERA';
  
 case 'c'
  r = 'Camera';
 
 otherwise
  error(sprintf('unknown mode (was ''%s'')', mode));
end

return


