function r = info2(in, vout, s, req, varargin)

r = vout;
r.validrequest = 1; % assume valid


switch req
 case 'fov'
  [r.varargout{1} r.varargout{2}] = calcfov(in, varargin{:});
  
  % these requests are all returned verbatim from the data structure
 case {'defaultheight'}
  r.varargout{1} = getfield(s, req);

 case 'pixels'
  defaults.units = '';
  defaults.height = s.defaultheight; % accept but ignore for now
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  if isempty(defaults.units)
    error('units must be set');
  end
  if ~isstruct(s.pixels) | ~isfield(s.pixels, defaults.units)
    error('unknown coordinate system');
  end
  
  tmp = getfield(s.pixels, defaults.units);
  r.varargout{1} = tmp.xmin:tmp.xstep:tmp.xmax;
  r.varargout{2} = tmp.ymin:tmp.ystep:tmp.ymax;

 otherwise
  r.validrequest = vout.validrequest; % put back to original setting
end


  
