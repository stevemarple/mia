function r = mia_datadir(in)
%MIA_DATADIR  Return the data base directory for a CAMERA object.
%
%   r = MIA_DATADIR(in)
%   r: base directory (CHAR)
%   in: CAMERA object
% 
%   See also mia_instrument_base/INFO.

r = fullfile(mia_datadir, 'camera', ...
	     sprintf('%s_%d', getabbreviation(in), getserialnumber(in)));
