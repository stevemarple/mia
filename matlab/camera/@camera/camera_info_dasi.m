function varargout = camera_info_dasi(cam, info, varargin)

if getserialnumber(cam) > 1
  error(sprintf('Do not have details for serial number #%d', ...
		getserialnumber(cam)));
end

% From: http://www.dcs.lancs.ac.uk/iono/dasi/
%
% The image is then transformed into a geographic grid of 67.6-72.6 North in
% steps of 0.1 degrees and 13.5-26.0 East in steps of 0.25 degrees at an
% altitude of 100 km.

switch info
 case 'bestresolution'
  varargout{1} = timespan(10,'s');
  
 case 'defaultfilename'
  % filename location
  varargout{1} = fullfile(camera_datadir(cam), '%Y', ...
			  '%m', '%d', '%Y%m%d%H.mat');
  varargout{2} = timespan(1,'h'); % 1h files on disk
  varargout{3} = 0; % don't fail if file missing
  varargout{4} = 'mat'; % matlab format
  varargout{5} = [51 51 360]; % correct size of data matrix
  varargout{6} = ''; % no special load function
  varargout{7} = ''; % no special save function

  
 case 'fov'
  [varargout{1}, varargout{2}] = calcfov(cam, varargin{:});
  
 case 'pixels'
  clear defaults;
  defaults.units = '';
  [defaults unvi] = interceptprop(varargin, defaults);
  
  switch defaults.units
   case 'deg'
    varargout{1} = 13.5:0.25:26.0;
    varargout{2} = 67.6:0.1:72.6;
   case ''
    error('units not specified');
   otherwise
    error(sprintf('unknown unit (was %s)', defaults.units));
  end
  
 otherwise
  error(sprintf('unknown information request (was %s)', info));
end


