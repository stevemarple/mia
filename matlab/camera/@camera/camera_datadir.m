function r = camera_datadir(cam)
%CAMERA_DATADIR  Return the camera data base directory.
%
%   r = CAM_DATADIR(cam)
%   r: base directory (CHAR)
%   cam: CAMERA object
% 
%   See also camera/INFO.

r = fullfile(mia_datadir, 'camera', getabbreviation(cam));
