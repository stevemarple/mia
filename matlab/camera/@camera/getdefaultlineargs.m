function r = getdefaultlineargs(in)
%GETDEFAULTLINEARGS  Return the default LINE parameters.
%
%   r = GETDEFAULTLINEARGS(in)
%   r: CELL array of parameters
%   in: MIA_INSTRUMENT_BASE object
%
%   See also mia_instrument_base/PLOT.

r = {'Color', 'm', ...
     'LineStyle', 'none', ...
     'Marker', '+'};
