function r = cam_pgeo_1
%CAM_PGEO_1  CAMERA object for Prince George, Canada.

% Automatically generated by makeinstrumentdatafunctions
r = camera('abbreviation', 'pgeo', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'THEMIS', ...
    'location', location('Prince George', 'Canada', ...
                         53.900000, 237.400000));

