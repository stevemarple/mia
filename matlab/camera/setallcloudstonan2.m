function setallcloudstonan2(instrument, st, et)

% improved(?) version of setallcloudstonan

[fstr ts] = info(instrument, 'defaultfilename');

t = st;

while t < et
  fname = strftime(t, fstr);
  mia = [];
  if exist(fname, 'file')
    disp(['loading ' fname]);    
    eval('load(fname);', '');
  else
    disp(['skipping ' fname]);    
  end
  
  if ~isempty(mia)
    % load succeeded
    mia = setcloud(mia, repmat(nan, 1, getdatasize(mia,3)));
    save(mia);
  end
  
  t = t + ts;
end
