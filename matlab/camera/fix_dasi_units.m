st = timestamp([1998 1 1 0 0 0])
et = timestamp([1999 1 1 0 0 0])
[fstr bs] = info(cam_dasi, 'defaultfilename');

while t < et
  filename = strftime(t, fstr);
  if ~isempty(dir(filename))
    load(filename);
    if isempty(getunits(mia))
      mia = setunits(mia, 'Rayleighs');
      save(mia);
    end
  end
  
  t = t + bs
end
