function r = info_cam_pgeo_1_data
%INFO_CAM_PGEO_1_DATA Return basic information about cam_pgeo_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_pgeo_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=pgeo;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'pgeo';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'THEMIS';
r.facility_url = 'http://aurora.phys.ucalgary.ca/themis/';
r.facilityid = 10;
r.groupids = [];
r.id = 20;
r.latitude = 53.9;
r.location1 = 'Prince George';
r.location1_ascii = 'Prince George';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 237.4;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
