function r = info_cam_pykk_1_data
%INFO_CAM_PYKK_1_DATA Return basic information about cam_pykk_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_pykk_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=pykk;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'pykk';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'NORSTAR';
r.facility_url = 'http://aurora.phys.ucalgary.ca/norstar/';
r.facilityid = 11;
r.groupids = [];
r.id = 34;
r.latitude = 63.77;
r.location1 = 'Pykkvibaer';
r.location1_ascii = 'Pykkvibaer';
r.location2 = 'Iceland';
r.logo = 'lancasterunilogo';
r.logurl = '';
r.longitude = -20.56;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([2007 11 01 00 00 00]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
