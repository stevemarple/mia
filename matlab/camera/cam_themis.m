function r = cam_themis
%RIO_SGO  Return a list of the THEMIS CAMERA instruments.
%
%   r = CAM_THEMIS
%   r: vector of CAMERA objects
%
%   For more details about THEMIS cameras see
%   http://aurora.phys.ucalgary.ca/themis/
%
%   See also CAMERA.

% Get list of all known cameras
c = instrumenttypeinfo(camera,'aware');

r = c(strcmp(getfacility(c), 'THEMIS'));
