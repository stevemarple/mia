% echo "select extract(epoch from time), extract(epoch from time + interval '3m'), cloud from cloud where cloud notnull order by time" | psql irisevents > cloudy_by_unixtime2

function fixclouds2

load cloudy_by_unixtime2;
cloud = cloudy_by_unixtime2;
clear cloudy_by_unixtime2;

resSecs = 10;
res = timespan(resSecs, 's');

epoch = timestamp([1970 1 1 0 0 0]);
fstr = info(cam_dasi, 'defaultfilename')


lastFileLoaded = '';
mia = [];
cloudvec = [];

for n = 1:size(cloud, 1)
  st = epoch + timespan(cloud(n,1), 's');
  et = epoch + timespan(cloud(n,2), 's');
  % disp(char(st));

  % st and et are 3 minutes different
  for t = [st et];
    fname = strftime(t, fstr);
    
    process = 0;
    if strcmp(lastFileLoaded, fname)
      process = 1;
    else
      localSave(mia, cloudvec, fstr);
      mia = [];
      if exist(fname)
	clear mia;
	disp(['about to load ' fname]);
	load(fname);
	cloudvec = getcloud(mia);
	lastFileLoaded = fname;
	process = 1;
      else
	lastFileLoaded = '';
      end
    end
    
    if process
      it = getimagetime(mia);
      idx = find(it >= st & it < et);
      cloudvec(idx) = cloud(n, 3);
      % disp(['setting index ' printseries(idx)]);
    end
    t = t + res;
  end
end

localSave(mia, cloudvec, fstr);
mia = [];

function localSave(mia, cloudvec, fstr)
if ~isempty(mia)
  mia = setcloud(mia, cloudvec);
  if 1 
    save(mia);
  else
    disp(strftime(getstarttime(mia), ['DEBUG: would have saved: ' fstr]));
  end
end
 
