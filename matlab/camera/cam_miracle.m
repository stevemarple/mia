function r = cam_miracle
%RIO_SGO  Return a list of the MIRACLE CAMERA instruments.
%
%   r = CAM_MIRACLE
%   r: vector of CAMERA objects
%
%   For more details about MIRACLE cameras see
%   http://www.ava.fmi.fi/MIRACLE/ASC/ 
%
%   See also CAMERA.

% Get list of all known cameras
c = instrumenttypeinfo(camera,'aware');

r = c(strcmp(getfacility(c), 'MIRACLE'));
