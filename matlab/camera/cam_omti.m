function r = cam_omti
%RIO_SGO  Return a list of the OMTI CAMERA instruments.
%
%   r = CAM_OMTI
%   r: vector of CAMERA objects
%
%   See also CAMERA.

% Get list of all known cameras
c = instrumenttypeinfo(camera,'aware');

r = c(strcmp(getfacility(c), 'OMTI'));
