function r = dasi2mia_image(filename, varargin)
r = [];

% what to do if file problems. Default is to just warn, unless processing
% a single filename
defaults.onerror = 'error'; 
[defaults unvi] = interceptprop(varargin, defaults);


if ~exist(defaults.onerror)
  error(sprintf('cannot find onerror function (was %s)', defaults.onerror));
end

d = dir(filename);
if length(d) == 0
  feval(defaults.onerror, ...
	sprintf('cannot get details on file %s', filename));
elseif length(d) > 1
  feval(defaults.onerror, ...
	sprintf('cannot get singular details on file %s', filename));
end

[fid message] = fopen(filename, 'r'); % default is binary mode
if fid == -1 
  feval(defaults.onerror, sprintf('Cannot open file %s: %s', filename, ...
			 message));
  return;
end

% process header (final line is 'END')
inHeader = 1;
header = [];
h.xpixelpos = [];
h.ypixelpos = [];
h.units = '';

latitude = [];
longitude = [];
numLat = [];
numLong = [];
header = {};
headerErrors = {};

rounding = {1, 20}; % fraction for rounding

while inHeader
  line = fgetl(fid);
  header{end+1} = line;
  
  if feof(fid) | strcmp(lower(line), 'end')
    inHeader = 0;
  elseif findstr('latitude range', lower(line))
    latitude = round2(sscanf(line, '%*s %*s %f %f'), rounding{:});
    % latitude = sscanf(line, '%*s %*s %f %f');
  elseif findstr('longitude range', lower(line))
    longitude = round2(sscanf(line, '%*s %*s %f %f'), rounding{:});
  elseif findstr('num_lat', lower(line))
    numLat = sscanf(line, '%*s %d');
  elseif findstr('num_lon', lower(line))
    numLong = sscanf(line, '%*s %d');
  elseif findstr('calibrated rayleighs', lower(line))
    h.units = 'Rayleighs';
  else
    % disp(['Line: ' line]);
  end
end
if prod(size(numLong)) ~= 1
  headerErrors{end+1} = 'num_long not defined';
else
  if prod(size(longitude)) ~= 2
    headerErrors{end+1} = 'longitude not defined';
  else
    % h.xpixelpos = interp1e([1 numLong], longitude, 1:numLong);
    h.xpixelpos = [0:1:(numLong-1)] * ...
	((longitude(2)-longitude(1))/(numLong-1)) + longitude(1);
  end
end

if prod(size(numLat)) ~= 1
  headerErrors{end+1} = 'num_lat not defined';
else
  if prod(size(latitude)) ~= 2
    headerErrors{end+1} = 'latitude not defined';
  else
    % h.ypixelpos = round2(interp1e([1 numLat], latitude, 1:numLat), 0.05);
    h.ypixelpos = [0:1:(numLat-1)] * ...
	((latitude(2)-latitude(1))/(numLat-1)) + latitude(1);
  end
end


if length(headerErrors)
  fclose(fid);
  fid = -1;
  feval(defaults.onerror, ...
	sprintf('bad header, errors were: \n%s\nheader was:\n%s', ...
		sprintf('%s\n', headerErrors{:}), ...
		sprintf('%s\n', header{:})));
  return;
end

numOfImages = 0;
h.data = [];
h.integrationtime = [];
while fid ~= -1 & ~feof(fid)
  % read time information
  [t count] = fread(fid, 7, 'uint16');
  if count ~= 7
    fclose(fid);
    fid = -1;
  else
    
    integrationtime = timespan(t(7), 's');
    % DASI images are aligned on 10s (or whatever) boundaries, but
    % sometimes the GPS changes before the file is written, so correct
    % image time
    imagetime = round(timestamp(t(1:6)'), integrationtime);
    
    
    % disp(char(imagetime));
    
    % read image
    [im count] = fread(fid, [numLat numLong], 'int32');
    if count ~= numLat*numLong
      fclose(fid);
      fid = -1;
      break;
    else
      
      numOfImages = numOfImages + 1;
      
      if numOfImages == 1
	% maxImages = (3600 - sum([60 1] .* t(5:6)')) /t(7)
	maxImages = 3600 / t(7);
	h.data = zeros(numLong, numLat, maxImages);
	% line below hangs matlab 5.1 when returning from function
	h.imagetime = repmat(timestamp, [1 maxImages]);
	h.integrationtime = timespan(zeros(1, maxImages), 's');
	
      end
      h.data(:,:,numOfImages) = im';
      h.imagetime(numOfImages) = imagetime;
      h.integrationtime(numOfImages) = integrationtime;
    end
  end
  
end

if numOfImages < 1
  return;
end

h.data = h.data(:,:,1:numOfImages);

h.imagetime = h.imagetime(1:numOfImages);
h.integrationtime = h.integrationtime(1:numOfImages);

% if all(h.integrationtime == h.integrationtime(1))
%   % revert to scalar if possible
%   h.integrationtime = h.integrationtime(1);
% end

if fid ~= -1
  fclose(fid);
  fid = -1;
end

proplist = makeprop(h);

% don't quite know how to extract MIA's concept of resolution out of the
% DASI file
ofl = opticalfilterlist('filters', opticalfilter('name', 'Green line', ...
						 'wavelength', 557.7e-9), ...
			'data', ones(1, numOfImages));


r = camera_image('starttime', h.imagetime(1), ...
		 'endtime', h.imagetime(end)+h.integrationtime(end), ...
		 'resolution', h.integrationtime, ...
		 'instrument', cam_dasi, ...
		 'filter', ofl, ...
		 'cloud', repmat(nan, [1, numOfImages]), ...
		 'height', 130e3, ...
		 'pixelunits', 'deg', ...
		 'log', 0, ...
		 proplist{:});

verify(r);

