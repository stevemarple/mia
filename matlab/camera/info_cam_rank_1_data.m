function r = info_cam_rank_1_data
%INFO_CAM_RANK_1_DATA Return basic information about cam_rank_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_rank_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=rank;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'rank';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'THEMIS';
r.facility_url = 'http://aurora.phys.ucalgary.ca/themis/';
r.facilityid = 10;
r.groupids = [];
r.id = 18;
r.latitude = 62.8;
r.location1 = 'Rankin Inlet';
r.location1_ascii = 'Rankin Inlet';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 267.9;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
function r = info_cam_rank_1_data
%INFO_CAM_RANK_1_DATA Return basic information about cam_rank_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_rank_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=rank;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'rank';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'NORSTAR';
r.facility_url = 'http://aurora.phys.ucalgary.ca/norstar/';
r.facilityid = 11;
r.groupids = [];
r.id = 28;
r.latitude = 62.82;
r.location1 = 'Rankin Inlet';
r.location1_ascii = 'Rankin Inlet';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 267.89;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
