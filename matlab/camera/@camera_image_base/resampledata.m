function r = resampledata(mia, t, varargin)
%RESAMPLEDATA.
%
% See mia_base/RESAMPLEDATA.

if length(mia) ~= 1
  for n = numel(mia)
    r(n) = feval(basename(mfilename), mia(n), t, varargin{:});
  end
  r = reshape(r, sz);
  return
end

if ~isempty(mia.filter)
  if ~all(mia.filter(1) == mia.filter)
    error('cannot resample data when filters differ');
  end
end

r = mia_base_resampledata(mia, t, varargin{:});



samt1 = getsampletime(mia);
samt2 = getsampletime(r);

if ~isempty(mia.filter)
  r.filter = repmat(mia.filter(1), size(samt2));
end

if ~isempty(mia.cloud)
  cloud = mia.cloud;
  cloud2 = getcloud(mia);
  r.cloud = interp1(getcdfepochvalue(samt1), mia.cloud, ...
		    getcdfepochvalue(samt2), 'linear');
end
