function r = setfilter(mia, filter)
%SETFILTER  Set filter information for a CAMERA_IMAGE object.
%
%   r = SETFILTER(mia, filter)
%   r: modified CAMERA_IMAGE object
%   mia: CAMERA_IMAGE object
%   filter: new filter information
%
%   Set the filter information for a CAMERA_IMAGE object. The filter
%   vector should be a 1xn vector where n is the number of images present
%   in the mia object, i.e., one value per image. If the filter
%   information is unknown it should be set to NAN. If no filter is
%   present it should be set to zero, otherwise each element should be
%   set to the centre wavelength of the filter (in metres).
%
%   See also GETFILTER.

r = mia;
r.filter = filter;
