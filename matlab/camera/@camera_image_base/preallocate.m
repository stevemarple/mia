function r = preallocate(mia, len)
%PREALLOCATE  Preallocate object fields to allow BUILD
%
%   r = PREALLOCATE(mia)
%   r: CAMERA_IMAGE_BASE object with preallocated fields
%   mia: CAMERA_IMAGE_BASE object (derived classes not allowed) 
%   len: size in time dimension
%
%   See also BUILD

if ~strcmp(class(mia), 'camera_image_base')
  
  % if this function is not overloaded it probably means someone has created
  % a new class and forgotten that it requires a PREALLOCATE command to use
  % CONSTRUCTFROMFILES or BUILD. Generate a runtime error with appropriate
  % error message to indicate what needs to be done. Derived classes MUST
  % call this function on their parent field.
  
  [tmp mfname] = fileparts(mfilename);
  error(sprintf('Overload %s for class %s', mfname, class(mia)));
end

r = mia; % copy basic things
r.mia_image_base = preallocate(r.mia_image_base, len);

% cannot preallocate to true length, just initialise 
r.filter = opticalfilterlist; 
r.cloud = repmat(nan, 1, len);
