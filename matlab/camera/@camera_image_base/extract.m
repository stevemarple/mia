function [r,sr,s] = extract(mia, varargin)
%EXTRACT  Extract a subset of data from a CAMERA_IMAGE_BASE object.
%
% The following options are permitted:
% 
%   'cloud', n [NUMERIC]
%   Find all images with cloud no more than n. n should be between 0 and
%   1, where 0 means no cloud and 1 means entirely clouded.
%
% See also mia_image_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end


[r sr s] = mia_image_base_extract(mia, varargin{:});

% extract filter and cloud data
if ~strcmp(sr.subs{3}, ':')
  r.cloud = r.cloud(sr.subs{3});
  r.filter = r.filter(sr.subs{3});
end






