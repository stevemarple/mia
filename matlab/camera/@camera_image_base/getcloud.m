function r = getcloud(mia, varargin)
%GETCLOUD  Return cloud information for a CAMERA_IMAGE object
%
%   r = GETCLOUD(mia)
%   r = GETCLOUD(mia, n)
%
%   r: cloud information (0=no cloud, 1=cloud, nan=unknown)
%   mia: CAMERA_IMAGE object
%   n: image number
%
%   See also CAMERA_IMAGE.

if nargin == 1
  r = mia.cloud;
else
  r = mia.cloud(varargin{1});
end  
