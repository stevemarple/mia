function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  Check if can extract subset of data from CAMERA_IMAGE_BASE obj.
%
% See mia_base/EXTRACTCHECK.

if length(varargin) == 1
  % subscript interface
  [sr s] = mia_image_base_extractcheck(mia, varargin{:});
  return
end  


% parameter name/value interface
defaults.cloud = [];
[defaults unvi] = interceptprop(varargin, defaults);
[sr s] = mia_image_base_extractcheck(mia, varargin{unvi});

% extract on cloud
if ~isempty(defaults.cloud) 
  idx = find(mia.cloud <= defaults.cloud);
  if isequal(sr.subs{end}, ':')
    sr.subs{end} = idx;
  else
    sr.subs{end} = intersect(sr.subs{end}, idx);
  end
end

