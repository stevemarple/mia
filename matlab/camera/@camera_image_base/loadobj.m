function r = loadobj(a)
%LOADOBJ  Load filter for CAMERA_IMAGE_BASE object.
%

if isstruct(a)
  r = camera_image_base(a);
else
  r = a;
end

for n = 1:numel(r)
  rn = r(n);
  
  if isempty(rn.filter)
    rn.filter = repmat(opticalfilter, getdatasize(rn,3));
  end

  if isempty(rn.cloud)
    rn.cloud = repmat(nan, 1,  getdatasize(r,3));
  end
  r(n) = rn;
end
