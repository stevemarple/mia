function varargout = guiextractdata(mia)
%GUIEXTRACT  Return a list of methods for EXTRACT using a gui.
%

% ask about base class methods
[a b] = mia_image_base_guiextractdata(mia);

guiFunc = 'camera_image_guiextract';

% how to call our companion function. Inside the cell array are cell arrays
% of parameters to pass to feval. Call as "feval(cell_array{:}, mia)". This
% approach allows flexibility, and means it is possible have some methods
% using the same function (particularly useful for GUI functions).


varargout{1} = {a{:} {guiFunc 'filter' 'data'}, ...
		{guiFunc 'cloud' 'data'}}; 

% second return value is a cell array of names to be used on menus
if nargout >= 2
  varargout{2} = {b{:} 'Filter' 'Cloud'};
end
