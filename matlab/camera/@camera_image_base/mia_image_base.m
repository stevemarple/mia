function r = mia_image_base(mia)
%MIA_IMAGE_BASE  Converter to base class for CAMERA_IMAGE class.
%
%   r = MIA_IMAGE_BASE(mia)
%   r: MIA_IMAGE_BASE object(s)
%   mia: CAMERA_IMAGE object(s)
%
%   See also CAMERA_IMAGE.

r = mia.mia_image_base;
