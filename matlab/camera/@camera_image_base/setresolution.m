function [r, msg] = setresolution(mia, newRes, varargin)
%SETRESOLUTION  Set the resolution of a CAMERA_IMAGE_BASE object.
%
% This is an overloaded functions for objects derived from
% CAMERA_IMAGE_BASE. See mia_base/SETRESOLUTION.

fil = getfilter(mia);

if length(mia) ~= 1
  error('object must be scalar');
end

if length(newRes) ~= 1
  error('new resolution must be scalar');
end

% for images the resolution must be the same
oldRes = getresolution(mia);
if length(oldRes) > 1 
  if ~all(oldRes(1) == oldRes(:))
    error('when resolution is non-scalar all values must be the same');
  end
  oldRes = oldRes(1);
end

factor = newRes ./ oldRes;
if factor == 1
  mesg = '';
  r = mia;
  return;
elseif factor < 1
  error('cannot interpolate images');
else
  % check filters for images which will be merged are the same. Generate
  % output filter list at the same time
  fil2 = repmat(opticalfilter, [length(fil)/factor 1]);
  
  for n = 1:length(fil2)
    tmp = fil([1:factor + (n-1)*factor]);
    if ~all(tmp(1) == tmp(:))
      error(['cannot reduce resolution as merged images have ' ...
	     'different filters']);
    end
    fil2(n) = tmp(1);
  end
end

% set the resolution and manipulate the data, using the version with a
% scalar resolution value
[r, msg] = mia_base_setresolution(mia, newRes, varargin{:})
