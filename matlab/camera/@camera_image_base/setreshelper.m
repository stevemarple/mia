function r = setreshelper(newmia, oldmia, newres, oldres, resmethod)
r = newmia;

samt_new = getsampletime(newmia);

if isempty(oldmia.filter)
  r.filter = oldmia.filter;
else
  if ~all(oldmia.filter(1) == oldmia.filter)
    error('cannot set resolution when filters differ');
  end
  
  r.filter = repmat(oldmia.filter(1), 1, numel(samt_new));
end

if ~isempty(oldmia.cloud)
  r.cloud = repmat(oldmia.cloud(1), 1, numel(samt_new));

  blocksz = newres ./ oldres;
  for n = 0:(length(samt_new)-1)
    idx = ((n*blocksz)+1):((n+1)*blocksz);
    r.cloud(n+1) = mean(oldmia.cloud(idx));
  end
end

