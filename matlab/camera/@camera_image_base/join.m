function [r, reversed] = join(a, b)
%JOIN  Join two CAMERA_IMAGE_BASE objects.
% 
%   r = JOIN(a, b);
%   r: joined object
%   a: CAMERA_IMAGE_BASE (or derived) object
%   b: object of same class as a
%
%   JOIN may only be used to join objects over times which do not intersect,
%   use INSERT if they do, and INTERSECT to test if they do.
%
%   See also INSERT.

if ~strcmp(class(a), 'camera_image_base')
  error('argument a is not class camera_image_base');
elseif ~strcmp(class(b), 'camera_image_base')
  error('argument b is not class camera_image_base');
elseif a.versionnumber ~= b.versionnumber
  error('version numbers differ');
end

[parent reversed] = join(a.mia_image_base, b.mia_image_base);
if reversed
  tmp = b;
  b = a;
  a = tmp;
end

if ~strcmp(class(a.filter), class(b.filter))
  error('filter classes differ');
end

switch class(a.filter)
 case 'opticalfilter'
  filter = [a.filter(:)' b.filter(:)'];
  
 case 'opticalfilterlist'
  filter = join(a.filter, b.filter);
  
 otherwise
  error('unknown filter class')
end
  
r = camera_image_base('filter', filter, ...
		 cloud, [a.cloud b.cloud]);
r.mia_image_base = parent;

