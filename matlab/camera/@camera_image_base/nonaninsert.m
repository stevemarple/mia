function [r, aa, ar, ba, br, rs] = nonaninsert(a, b)
%NONANINSERT  Overload NONANINSERT for CAMERA_IMAGE_BASE.
%
% See mia_base/NONANINSERT.

[tmp mfile] = fileparts(mfilename);

% call parent version of insert or nonaninsert as appropriate
[r, aa, ar, ba, br, rs] = feval(['mia_image_base_' mfile], a, b); 



if matlabversioncmp('<', '5.2')
    % need to append 2 to avoid the dummy functions
    sr = 'subsref2';
    sa = 'subsasg2';
else
  sr = 'subsref';
  sa = 'subsasgn';
end

% convert for vector copying
aa2.type = aa.type;
aa2.subs = {1 aa.subs{end}};

ar2.type = ar.type;
ar2.subs = {1 ar.subs{end}};

ba2.type = ba.type;
ba2.subs = {1 ba.subs{end}};

br2.type = br.type;
br2.subs = {1 br.subs{end}};


% cloud
r.cloud = zeros(1, rs(end));
% copy cloud from a
tmp = feval(sr, a.cloud, ar2);              % subsref(a.cloud, ar)
r.cloud = feval(sa, r.cloud, aa2, tmp); % subsasgn(rdata, aa, tmp);

% copy cloud from b
tmp = feval(sr, b.cloud, br2);              % subsref(a.cloud, ar)
r.cloud = feval(sa, r.cloud, ba2, tmp); % subsasgn(rdata, aa, tmp);


% filter
rfilter = repmat(opticalfilter, 1, rs(end));
% copy filter from a
% tmp = feval(sr, a.filter, ar2);        % subsref(a.filter, ar)
% tmp = extract(a.filter, ar2.type{end});
tmp = getfilter(a, ar2.type{end});
rfilter = feval(sa, r.filter, aa2, tmp); % subsasgn(rdata, aa, tmp);

% copy filter from b
% tmp = feval(sr, b.filter, br2);        % subsref(a.filter, ar)
% tmp = extract(b.filter, br2.type{end});
tmp = getfilter(b, br2.type{end});
rfilter = feval(sa, r.filter, ba2, tmp); % subsasgn(rdata, aa, tmp);

r.filter = opticalfilterlist(r.filter);
