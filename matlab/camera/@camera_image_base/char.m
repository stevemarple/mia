function s = char(mia, varargin)
%CHAR Convert CAMERA_IMAGE object into a CHAR representation.
%
%   s = CHAR(mia)
%   mia: MIA object
%   s: CHAR representation.
%
%   See also MIA_IMAGE_BASE/CHAR.

% NB Make all objects derived from mia_base print a trailing newline
% character 

if length(mia) ~= 1
  s = matrixinfo(mia);
  return;
end

cloud_str = '';
if isempty(mia.cloud)
  cloud_str = 'Not set';
elseif all(isnan(mia.cloud))
  cloud_str = 'Unknown';
elseif any(isnan(mia.cloud))
  nans = sum(isnan(mia.cloud));
  if nans > 0.5 * numel(mia.cloud)
    amount = 'Mostly';
  else
    amount = 'Partly';
  end
  cloud_str = sprintf('%s unknown, remainder %2.f%% cloudy', ...
		      amount, 100 * nonanmean(mia.cloud));
elseif all(mia.cloud)
  cloud_str = 'full cloud';
elseif ~any(mia.cloud)
  cloud_str = 'no cloud';
else
  % cloud_str = 'some cloud';
  cloud_str = sprintf('%2.f%% cloudy', 100 * nonanmean(mia.cloud));
end

if isobject(mia.filter)
  filtStr = char(mia.filter);
else
  filtStr = matrixinfo(mia.filter);
end

s = sprintf(['%s' ...
	     'filter            : %s\n' ...
	     'cloud             : %s %s\n'], ...
	    char(mia.mia_image_base, varargin{:}), filtStr, ...
	    matrixinfo(mia.cloud), cloud_str); 
	     


