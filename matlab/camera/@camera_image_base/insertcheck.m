function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for CAMERA_IMAGE_BASE data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mia_image_base_insertcheck(a, b);

if ~strcmp(class(a.filter), class(b.filter)) ...
      & ~isempty(a.filter) & ~isempty(b.filter)
  a.filter
  b.filter
  error(sprintf(['different filter implementations (were ''%s'' ' ...
		 'and ''%s'')'], class(a.filter), class(b.filter)));
end
