function r = setcloud(mia, cloud)
%SETCLOUD  Set cloud information for a CAMERA_IMAGE object.
%
%   r = SETCLOUD(mia, cloud)
%   r: modified CAMERA_IMAGE object
%   mia: CAMERA_IMAGE object
%   cloud: new cloud information
%
%   Set the cloud information for a CAMERA_IMAGE object. The cloud
%   vector should be a 1xn vector where n is the number of images present
%   in the mia object, i.e., one value per image. If the cloud
%   information is unknown it should be set to NAN.
%
%   See also GETCLOUD.

r = mia;
r.cloud = cloud;
