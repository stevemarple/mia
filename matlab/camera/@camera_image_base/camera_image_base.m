function mia = camera_image_base(varargin)
%CAMERA_IMAGE_BASE  Constructor for CAMERA_IMAGE_BASE class.
%
%
%   See also MIA_IMAGE_BASE.

cls = 'camera_image_base';
parent = 'mia_image_base';

latestversion = 1;
mia.versionnumber = latestversion;
mia.filter = [];
mia.cloud = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  mia = class(mia, cls, feval(parent));
 
elseif nargin == 1 & strcmp(class(varargin{1}), cls)
  % copy constructor
  mia = varargin{1};
 
elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct (useful for failed load commands when the class
  % layout has changed)
  if ~isfield(varargin{1}, parent)
    error(sprintf('need a %s field', parent));
  end
  p = feval(parent, getfield(varargin{1}, parent));
  
  % copy common fieldnames, parent class not a field on mia yet since mia
  % is a struct at this point
  fn = fieldnames(varargin{1});
  for n = 1:length(fn)
    if isfield(mia, fn{n})
      mia = setfield(mia, fn{n}, getfield(varargin{1}, fn{n}));
    end
  end
  mia = class(mia, cls, p);
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  % [mia unvi] = interceptprop(varargin, mia);
  
  defaults = mia;
  defaults.load = 0;
  defaults.cancelhandle = [];
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'cancelhandle'});

  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  % p = mia_image_base(varargin{unvi});
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);

  if defaults.load
    mia = loaddata(mia, defaults.cancelhandle);
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
