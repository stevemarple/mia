function [r, varargout] = getimagelabel(mia, defaults, varargin)
%IMAGELABEL  Override imagelabel options to CAMERA_IMAGE.
%
% See mia_image_base/GETIMAGELABEL

if isstruct(defaults)
  samt = getsampletime(mia);
  if numel(samt) ~= 1
    error('mia should contain one image');
  end  
  
  switch defaults.style
   case 'filter'
    r = char(mia.filter);
    
   case 'cloud'
    cloud =  getcloud(mia, 1);
    if isnan(cloud)
      r = '?';
    elseif cloud == 0
      r = 'clear';
    elseif cloud == 1
      r = 'cloudy';
    else
      r = sprintf('%.0f%%', 100 * cloud);
    end
    
   otherwise
    % pass up to be dealt with by parent
    r = mia_image_base_getimagelabel(mia, defaults, varargin{:});
  end
elseif any(strcmp(defaults, {'list', 'help'}))
  
  [tmp1 tmp2] = mia_image_base_getimagelabel(mia, 'list');

  if strcmp(defaults, 'help')
    % user asking for help
    mia_image_base_getimagelabel(mia, defaults, tmp1, tmp2);
    return
  end
  

  r = {tmp1{:} 'filter' 'cloud'};
  tmp2 = {tmp2{:} 'Filter wavelength' 'Cloud'};
  if nargout >= 2
    varargout{1} = tmp2;
  end

else
  error('incorrect parameters');
end
