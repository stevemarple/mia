function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  Insert CAMERA_IMAGE_BASE object into another CAMERA_IMAGE_BASE object.
%
%   [r aIdx bIdx] = INSERT(a, b)
%   r: modified MIA_IMAGE_BASE object
%   aIdx: index of a images in r (0 if not present)
%   bIdx: index of b images in r (0 if not present)
%
%   See also CAMERA_IMAGE_BASE.

% does anyone know why these two objects should not be joined together?
% speak now!


[r aa ar ba br rs] = mia_image_base_insert(a, b);

% join cloud
if isempty(a.cloud)
  a.cloud = repmat(nan, 1,  getdatasize(a,3));
end
if isempty(b.cloud)
  b.cloud = repmat(nan, 1,  getdatasize(a,3));
end

r.cloud = repmat(nan, [1 rs.datasize(end)]);

c_ar.type = '()';
c_ar.subs = {1, ar.subs{end}};
c_aa.type = '()';
c_aa.subs = {1, aa.subs{end}};

c_br.type = '()';
c_br.subs = {1, br.subs{end}};
c_ba.type = '()';
c_ba.subs = {1, ba.subs{end}};

r.cloud = subsasgn(r.cloud, c_aa, subsref(a.cloud, c_ar));
r.cloud = subsasgn(r.cloud, c_ba, subsref(b.cloud, c_br));

r.filter = subsasgn(r.filter, c_aa, subsref(a.filter, c_ar));
r.filter = subsasgn(r.filter, c_ba, subsref(b.filter, c_br));

