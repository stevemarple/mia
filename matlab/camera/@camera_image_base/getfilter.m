function r = getfilter(mia, varargin)
%GETFILTER  Return filter information for a CAMERA_IMAGE object
%
%   r = GETFILTER(mia)
%   r = GETFILTER(mia, n)
%
%   r: filter information
%   mia: CAMERA_IMAGE object
%   n: image number
%
%   See also CAMERA_IMAGE.
%

if nargin == 1
  r = mia.filter;
else
  r = mia.filter(varargin{1});
end  

