function r = dasi2mia_image2(filename, varargin)
r = [];

cwd = pwd;

% what to do if file problems. Default is to just warn, unless processing
% a single filename
defaults.onerror = []; %'error'; 
defaults.recursive = 0;
[defaults unvi] = interceptprop(varargin, defaults);


% % is absolute path?
% absolutePath = 0;
% if isunix
%   if filename(1) == filesep
%     absolutePath = 1;
%   end
% elseif strcmp(computer, 'PCWIN')
%   if lower(filename(1)) >= 'a' & lower(filename(1)) <= 'z'& ...
% 	filename(2) == ':'
%     % drive specification
%     absolutePath = 1;
%   elseif strcmp(filename([1 2]), '\\')
%     % windows UNC(?) path
%     absolutePath = 1;
%   end
% else
%   error('unknown system')
% end


% if ~absolutePath
%   % get rid of awkward relative paths, they cause problems with exist()
%   filename = fullfile(pwd, filename);
% end

filename = absolutepath(filename);
[Path filename ext] = fileparts(filename);

if ~isempty(ext)
  filename = [filename ext];
end

if any(Path == '*') | any(Path == '?')
  error(sprintf('path should not contain wildards * or ? (was %s)', ...
		Path));
  
  % when we have a join function for camera data make recursive calls and
  % join results  
  return;
end

Pf = fullfile(Path, filename);
d = dir(Pf);

% check if fullfile(Path, filename) is a directory, if so ammend Path to
% reflect that fact (/Path/* and /Path should be reated in same way)
if exist(Pf, 'dir')
  % Path = fullfile(Path, filename);
  Path = Pf;
end
% disp(sprintf('Path=%s\n', Path));

if ~ischar(defaults.onerror)
  if length(d) == 1
    defaults.onerror = 'error';
  else
    defaults.onerror = 'warning';
  end
elseif ~exist(defaults.onerror)
  error(sprintf('cannot find onerror function (was %s)', defaults.onerror));
end

for n = 1:length(d)
% for n = 1:min(length(d),3)


  fname = d(n).name;
  if isempty(fileparts(fname))
    % no directory, give it one
    fname = fullfile(Path, d(n).name);
  end

  tmp = [];
  if d(n).isdir == 0
    tmp = dasi2mia_image(fname, 'onerror', defaults.onerror);
  elseif defaults.recursive & ~strcmp(d(n).name, '.') & ...
	strcmp(d(n).name, '..')
    % don't make recursive calls to current or parent directory!
    tmp = dasi2mia_image(fname, 'onerror', defaults.onerror);
  end

  if ~isempty(tmp)
    if isempty(r)
      r = tmp;
    else
      r = insert(r, tmp);
    end
  end
end





