function [r, varargout] = scasi2mia_image(mia, varargin)

r = mia;
in = getinstrument(mia);
abbrev = getabbreviation(in);

st = getstarttime(mia);
et = getendtime(mia);

% [fstr bs fifm format] = info(in, 'defaultfilename');
df = info(in, 'defaultfilename', 'camera_image');

[pathFstr fileFstr ext] = fileparts(df.fstr);


filters.None = opticalfilter('name', 'None', ...
			     'wavelength', 0, ...
			     'upperwavelength', 0, ...
			     'lowerwavelength', 0);
filters.OH = opticalfilter('name', 'OH', ...
			   'wavelength', 843e-9, ...
			   'upperwavelength', 0, ...
			   'lowerwavelength', 0);
filters.unknown = opticalfilter('name', 'unknown', ...
				'wavelength', nan, ...
				'upperwavelength', nan, ...
				'lowerwavelength', nan);
filterNames = fieldnames(filters);

unknownFilterIdx = find(strcmp('unknown', filterNames));

sameProc = 1;
lastProc = {};
imageNumber = 0;

rData = [];
rTime = [];
rMidtime = [];

for m = 1:2
  t = floor(st, df.duration);
  
  if m == 2
    numOfImages = imageNumber;
    imageNumber = 0;
  end

  while t < et
    Path = strftime(t, pathFstr);
    d = dir(Path);
    d = d(find(~[d.isdir])); % remove directories
    [tmp idx] = sort({d.name});
    
    if ~isempty(idx)
      % check all files in the directory, in ascending filename order
      for n = idx
	ft = timestamp(sscanf(d(n).name, '%4d%2d%2d%2d%2d%2d', 6)');
	[tmp1 tmp2 fext] = fileparts(d(n).name);
	if ft >= st & ft < et & strcmp(fext, ext)
	  imageNumber = imageNumber + 1;
	  if m == 2
	    % load this file
	    [im h proc] = stread(fullfile(Path, [d(n).name]));
	    
	    if nargout > 1
	      % save header information
	      if imageNumber == 1
		headers = repmat(h, 1, numOfImages);
	      end
	      headers(imageNumber) = h;
	    end
	    
	    
	    % SCASI has the sensor rotated through 90 degrees
	    tmpH = h;
	    h.Height = tmpH.Width;
	    h.Width = tmpH.Height;
	    clear tmpH;
	    
	    if imageNumber == 1
	      % initialisation
	      width = h.Width;
	      height = h.Height;
	      rData = repmat(feval(class(im), 0), [width height numOfImages]);
	      
	      rTime = repmat(timestamp, [1 numOfImages]);
	      rMidtime = repmat(timestamp, [1 numOfImages]);
	      rInteg = timespan(zeros(1, numOfImages), 's'); 
	      rFilter = zeros(1, numOfImages);
	      
	    elseif imageNumber > 1
	      if width ~= h.Width
		error('width not constant');
	      elseif height ~= h.Height
		error('height no constant');
	      end
	    end

	    rData(:,:,imageNumber) = im;
	    rTime(imageNumber) = timestamp([h.Date h.Time]);
	    rInteg(imageNumber) = timespan(h.Exposure/100, 's'); 
	    rMidtime(imageNumber) = rTime(imageNumber) + 0.5* ...
		rInteg(imageNumber);

	    if 0
	      % filter is a simple vector
	      if isfield(h, 'Filter')
		switch h.Filter
		 case 'None'
		  % no filter is different from not knowing
		  rFilter(imageNumber) = 0; 
		 case 'OH'
		  rFilter(imageNumber) = 843e-9; % 843 nm
		 otherwise
		  warning(sprintf('Unknown filter type (was %s)', ...
				  h.Filter));
		  rFilter(imageNumber) = nan;
		end
	      else
		rFilter(imageNumber) = nan; % not known
	      end
	      
	    else
	      % filter is opticalfilterlist
	      tmp = find(strcmp(h.Filter, filterNames));
	      if isempty(tmp)
		% unknown filter
		rFilter(imageNumber) = unknownFilterIdx;
	      else
		rFilter(imageNumber) = tmp;
	      end
	    end
	    
	    if sameProc
	      if isempty(lastProc)
		lastProc = proc;
	      elseif ~isequal(proc, lastProc)
		sameProc = 0;
		proc = [h.format ': <various processing>'];
	      end
	    end
	  end
	  
	end
      end
    end
    t = t + df.duration;
  end
end

if numOfImages
  r = setstarttime(r, rTime(1));
  r = setendtime(r, rTime(end) + timespan(h.Exposure/100, 's'));
  r = setprocessing(r, proc);
  r = setdata(r, rData);
  r = setunits(r, 'ADC');
  r = setimagetime(r, rTime);
  r = setimagemidtime(r, rMidtime);
  r = setresolutionfield(r, rInteg);
  r = setintegrationtime(r, rInteg);
  r = settimingfield(r, 'offset');

  % r = setfilter(r, rFilter);
  for n = 1:length(filterNames)
    f(n) = getfield(filters, filterNames{n});
  end
  r = setfilter(r, opticalfilterlist('filters', f, ...
				     'data', rFilter));
  
  r = setcloud(r, repmat(nan, 1, numOfImages));
  r = setcreator(r, h.Observer);

  r = setpixels(r, 1:width, 1:height, 'raw');
end

% adjust to suit requested resolution
if ~isempty(getresolution(mia))
  disp('adjusting resolution');
  r = setresolution(r, getresolution(mia));
end

if nargout > 1
  varargout{1} = headers;
end
