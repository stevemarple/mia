function r = info_cam_sod_1_data
%INFO_CAM_SOD_1_DATA Return basic information about cam_sod_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_sod_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=sod;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'sod';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 12;
r.latitude = 67.42;
r.location1 = 'Sodankyla';
r.location1_ascii = 'Sodankyla';
r.location2 = 'Finland';
r.logo = 'oulu_logo_color';
r.logurl = '';
r.longitude = 26.39;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = 'http://www.sgo.fi/Data/AllSky/allsky.php/';
r.defaultfilename = [];
r.institutions = {};
% end of function
