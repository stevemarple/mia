function r = info_cam_fykn_1_data
%INFO_CAM_FYKN_1_DATA Return basic information about cam_fykn_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_fykn_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=fykn;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'fykn';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'THEMIS';
r.facility_url = 'http://aurora.phys.ucalgary.ca/themis/';
r.facilityid = 10;
r.groupids = [];
r.id = 23;
r.latitude = 66.56;
r.location1 = 'Fort Yukon';
r.location1_ascii = 'Fort Yukon';
r.location2 = 'USA';
r.logo = '';
r.logurl = '';
r.longitude = 214.78;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
