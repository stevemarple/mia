function r = info_cam_abk_1_data
%INFO_CAM_ABK_1_DATA Return basic information about cam_abk_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_abk_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=abk;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'abk';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 3;
r.latitude = 68.36;
r.location1 = 'Abisko';
r.location1_ascii = 'Abisko';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 18.82;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
