function r = cam_norstar
%CAM_NORSTAR  Return a list of the NORSTAR CAMERA instruments.
%
%   r = CAM_NORSTAR
%   r: vector of CAMERA objects
%
%   For more details about NORSTAR cameras see
%   http://aurora.phys.ucalgary.ca/norstar/
%
%   See also CAMERA.

% Get list of all known cameras
c = instrumenttypeinfo(camera,'aware');

r = c(strcmp(getfacility(c), 'NORSTAR'));
