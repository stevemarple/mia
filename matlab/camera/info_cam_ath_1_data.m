function r = info_cam_ath_1_data
%INFO_CAM_ATH_1_DATA Return basic information about cam_ath_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_ath_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=ath;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'ath';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'OMTI';
r.facility_url = '';
r.facilityid = 12;
r.groupids = [];
r.id = 33;
r.latitude = 54.7;
r.location1 = 'Athabasca';
r.location1_ascii = 'Athabasca';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 246.7;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
