function varargout = stread(filename)
%STREAD  Read ST-4X/5/6/7/8 image file format, used by SBIG cameras.
%
%   [im, h] = STREAD(filename)
%
%   im: image
%   h: header information (STRUCT)
%   filename: filename of image
im = [];
h = [];
procStr = {};

% default is binary mode, but require little-endian machine format
[fid message] = fopen(filename, 'r', 'l'); 
if fid == -1 
  error(sprintf('Cannot open file %s: %s', filename, message));
else
  disp(['loading ' filename]);
end

line = fgetl(fid);
if findstr(lower(line), 'compressed') & 0
  fclose(fid);
  error('cannot read compressed images');
elseif findstr(lower(line), ' image')
  sp = find(line == ' ');
  % h.format = line(1:(sp(1)-1));
  h.format = line;
end

% process header (final line is 'END')
inHeader = 1;
fgetl(fid); % read carriage return

while inHeader
  if ftell(fid) > 2048
    fclose(fid);
    error('header exceeds 2048 bytes'); % limit imposed by ST-x format
  end
  line = fgetl(fid);
  if strcmp(lower(line), 'end')
    inHeader = 0;
    pn = 'end';
    fseek(fid, 2048, 'bof'); % go to end of header block
  else
    fgetl(fid); % read carriage return
    equals = find(line == '=');
    if isempty(equals)
      fclose(fid)
      error(sprintf('bad line in header (was ''%s'')', line))
    end
    pn = line(1:(equals(1)-2)); % parameter name
    pv = line((equals(1)+2):end); % parameter name
  end
  
  
  switch pn
   case 'end'
    % psuedo parameter line for the case when the end of header is
    % reached, ignore
    ;
   case 'Date'
    sep = find(pv == '/');
    if length(sep) ~= 2 
      fclose(fid);
      error(sprintf('bad date (was %s)', pv));
    end
    [tmp count message] = sscanf(pv, '%2d/%2d/%2d', 3);
    if count ~= 3
      fclose(fid);
      error(['Bad date: ' message]);
    end
    h.Date = tmp([3 1 2])';
    % want date as YYYY MM DD
    if h.Date(1) > 80
      h.Date(1) = h.Date(1) + 1900;
    else
      h.Date(1) = h.Date(1) + 2000;
    end
    
   case 'Time'
    sep = find(pv == ':');
    if length(sep) ~= 2 
      fclose(fid);
      error(sprintf('bad time (was %s)', pv));
    end
    [tmp count message] = sscanf(pv, '%2d:%2d:%2d', 3);
    if count ~= 3
      fclose(fid);
      error(['Bad time: ' message]);
    end
    h.Time = tmp';
    
   otherwise
    if all(pv == '.' | (pv >= '0' & pv <= '9'))
      % numerical value
      h = setfield(h, pn, sscanf(pv, '%f'));
      
      % check file and version are acceptable
      if strcmp(pn, 'File_version') 
	if h.File_version ~= 3
	  fclose(fid);
	  error(sprintf(['Do not know how to read images for file ' ...
			 'version %d'], h.File_version));
	end
      elseif strcmp(pn, 'Data_version') 
	if h.Data_version ~= 1
	  fclose(fid);
	  error(sprintf(['Do not know how to read images for data ' ...
			 'version %d'], h.Data_version));
	end
      end
	
    else
      % textual value
      h = setfield(h, pn, pv);
    end
  end
  
end

% test critcal field names exist
fn = {'Width', 'Height', 'Sat_level'};
for n = 1:length(fn)
  if ~isfield(h, fn{n}) 
    fclose(fid);
    error(sprintf('Bad header: %s not found', fn{n}));
  end
end

imClass = sprintf('uint%d', ceil(log2(h.Sat_level-100) / 8) * 8);
if findstr(lower(h.format), 'compressed')
  % compressed image
  im = localReadCompressed(fid, h, imClass);
else
  % simple no-compressed
  [im count] = fread(fid, [h.Width h.Height], 'uint16');
  im = feval(imClass, im');
  if count ~= h.Height * h.Width
    fclose(fid);
    'ggg'
    error('incorrect image size');
  end
end


im = flipud(im);

if fid ~= -1
  fclose(fid);
  fid = -1;
end

if nargout >= 1
  varargout{1} = im;
end
if nargout >= 2
  varargout{2} = h;
end
if nargout >= 3
  procStr = localSTProc2Str(h);
  varargout{3} = procStr;
end



function r = localSTProc2Str(h)
% convert ST-4X/5/6/7/8 history information into readable strings
r = {};

offset = '@'-1;
history = cell('d'-offset, 1);

history{double('@')-offset} = 'Modifications made before history established';
history{double('A')-offset} = 'Co-addition of image';
history{double('B')-offset} = 'Image linearly scaled';
history{double('C')-offset} = '';
history{double('D')-offset} = 'Image dark subtracted';
history{double('E')-offset} = 'Cool pixels removed';
history{double('F')-offset} = 'Flat field corrected';
history{double('G')-offset} = 'Image smoothed';
history{double('H')-offset} = 'Image sharpened';
history{double('I')-offset} = 'Image pseudo flat field corrected';
history{double('J')-offset} = ...
    'Image quantized or posterized to less than 16 bits precision';
history{double('K')-offset} = 'Warm pixels removed';
history{double('L')-offset} = 'Image flipped horizontally';
history{double('M')-offset} = 'Image flipped vertically';
history{double('N')-offset} = 'Image zoomed with pixel interpolation';
history{double('O')-offset} = 'More than 40 operations performed';
history{double('P')-offset} = 'Image log scaled';
history{double('Q')-offset} = 'Pixels combined';
history{double('R')-offset} = 'Image auto dark subtracted';
history{double('S')-offset} = 'Image zoomed with pixel replication';
history{double('T')-offset} = 'Image clipped and filled';
history{double('U')-offset} = 'Image converted to 8 bit log format';
history{double('V')-offset} = 'Image merged to color using 2 color merge';
history{double('W')-offset} = ...
    'Image merged to color using 3 color merge';
history{double('X')-offset} = 'Image translated and back filled';
history{double('Y')-offset} = 'Image pixels inverted';
history{double('Z')-offset} = 'Image sharpened with unipolar algorithm';
history{double('[')-offset} = 'Image sharpened with one-sided sharpening';
history{double('\')-offset} = ...
    'Image modernized by replacing ST6COLOR history characters';
history{double(']')-offset} = 'Image resampled to make square pixels';
history{double('^')-offset} = 'Image averaged';
history{double('_')-offset} = 'Constant added to or subtracted from image';
history{double('`')-offset} = 'Constant multiplied by or divided into image';
history{double('a')-offset} = 'Image enlarged 2X by pixel interpolation';
history{double('b')-offset} = 'Image reduced 2X by pixel combining';
history{double('c')-offset} = 'Column or Row repair';
history{double('d')-offset} = 'Adaptive dark subtraction';
history{double('e')-offset} = 'Pseudo 3D';
history{double('f')-offset} = 'Auto Dark HPR';
history{double('g')-offset} = 'HPR Subtract';
history{double('h')-offset} = 'Deconvolve';

len = 0;
for n = 1:length(h.History)
  c = h.History(n);
  m = c-offset;
  if c == '0'
    ; % ignore, '0' used to signal no modification
  elseif m < 1 | m > length(history) | isempty(history{m})
    len = len + 1;
    r{len} = [h.format ': unknown processing operation'];
  else
    len = len + 1;
    r{len} = [h.format ': ' history{m}];
  end
end



function im = localReadCompressed(fid, h, imClass)

% im = feval(imClass, zeros(h.Height, h.Width));
im = zeros(h.Height, h.Width);
for r = 1:h.Height
  [len count] = fread(fid, 1, 'uint16');
  if count ~= 1
    fclose(fid);
    error('bad file');
  end
  tmpPos = ftell(fid)
  [line count] = fread(fid, [1 len], 'uint8');

  if count ~= len
    fclose(fid);
    error('bad file: could not read compressed image data line length');
  end
  % line = uint8(line);

  % first pixel is always given with absolute value
  % im(r,1) = (uint16(256)*uint16(line(2))) + uint16(line(1));
  im(r,1) = sum(line(1:2) .* [1 256]);
  
  pos = 3;
  for c = 2:h.Width
    disp(sprintf('---- %d', tmpPos+pos));
    disp(sprintf('h=%d w=%d %s', h.Height, h.Width, matrixinfo(line)));
    disp(sprintf('p=%d r=%d c=%d', pos, r, c));
    disp(sprintf('%d %x',double(im(r,1)), double(im(r,1))));
    
    disp(sprintf('line(%d) = %f', pos, line(pos)));
    if round(line(pos)) == 128 % 0x80
      % absolute value
      disp('absolute value=====================');

      % im(r, c) = (uint16(256)*uint16(line(pos+2))) + uint16(line(pos+1));
      im(r, c) = sum(line(pos+[1:2]) .* [1 256]);
      pos = pos+3;
    else
      % delta (difference value) follows
      disp('delta');
      % pos = pos + 1;
      
      if line(pos) < 127
	im(r, c) = im(r, c-1) + line(pos);
      else
	% byte is stored as unsigned, but need to treat as signed. Messy
        % in matlab
	% DEBUG - line below NOT TESTED
	% im(r, c) = im(r, c-1) - feval(imClass, 256) + line(pos);
	im(r, c) = im(r, c-1) - 256 + line(pos);
      end
      disp(sprintf('%02x %d %d', double(line(pos)), double(line(pos)), ...
		   double(im(r, c))));
      pos = pos + 1;

    end
  end
  % cla
  % image(double(im), 'cdatamapping','scaled')
  if pos ~= length(line)+1
    fclose(fid);
    error('bad file: did not correctly read compressed image data');
  end
end





