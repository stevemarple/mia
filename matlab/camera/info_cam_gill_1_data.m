function r = info_cam_gill_1_data
%INFO_CAM_GILL_1_DATA Return basic information about cam_gill_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_gill_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=gill;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'gill';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'NORSTAR';
r.facility_url = 'http://aurora.phys.ucalgary.ca/norstar/';
r.facilityid = 11;
r.groupids = [];
r.id = 26;
r.latitude = 56.38;
r.location1 = 'Gillam';
r.location1_ascii = 'Gillam';
r.location2 = 'Canada';
r.logo = '';
r.logurl = '';
r.longitude = 265.36;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
