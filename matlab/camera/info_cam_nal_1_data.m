function r = info_cam_nal_1_data
%INFO_CAM_NAL_1_DATA Return basic information about cam_nal_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_nal_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=nal;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'nal';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'MIRACLE';
r.facility_url = 'http://www.ava.fmi.fi/MIRACLE/';
r.facilityid = 8;
r.groupids = [];
r.id = 11;
r.latitude = 78.92;
r.location1 = 'Ny Alesund';
r.location1_ascii = 'Ny Alesund';
r.location2 = 'Svalbard';
r.logo = '';
r.logurl = '';
r.longitude = 11.93;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
