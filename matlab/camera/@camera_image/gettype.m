function r = gettype(mia, varargin)
%GETTYPE  Return CAMERA data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: CAMERA object
%
%   See also CAMERA, MIA_IMAGE_BASE, CAMERA.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'intensity';
  
 case 'u'
  r = 'INTENSITY';
  
 case 'c'
  r = 'Intensity';
 
 case 'C'
  r = 'Intensity';
  
 otherwise
  error('unknown mode');
end


