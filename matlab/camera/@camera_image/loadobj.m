function r = loadobj(a)
%LOADOBJ  Load filter for RIO_ABS object.
%

if isstruct(a)
  r = camera_image(a);
else
  r = a;
end

