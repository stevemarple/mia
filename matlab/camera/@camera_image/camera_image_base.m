function r = camera_image_base(mia)
%CAMERA_IMAGE_BASE Converter to base class for CAMERA_IMAGE.
%
%   r = CAMERA_IMAGE_BASE(mia)
%   r: CAMERA_IMAGE_BASE object(s)
%   mia: CAMERA_IMAGE object(s)
%
% See also CAMERA_IMAGE, CAMERA_IMAGE_BASE.

r = mia.camera_image_base;

