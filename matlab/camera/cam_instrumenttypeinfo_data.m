function r = cam_instrumenttypeinfo_data
%CAM_INSTRUMENTTYPEINFO_DATA  Type information data function for camera class.
% Do not access this function directly, use the INSTRUMENTTYPEINFO
% function instead.
%
% This function was created automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument_type_information=camera
%
% See INSTRUMENTTYPEINFO

r.aware = [cam_abk_1 cam_ath_1 cam_atha_1 cam_dasi_1 cam_dnb_1 ...
    cam_ekat_1 cam_fsmi_1 cam_fykn_1 cam_gill_1 cam_han_1 ...
    cam_inuv_1 cam_kev_1 cam_kil_1 cam_lop_1 cam_loz_1 cam_lyr_1 ...
    cam_mcgr_1 cam_muo_1 cam_nal_1 cam_nyr_1 cam_pgeo_1 cam_pina_1 ...
    cam_pykk_1 cam_rank_1 cam_resu_1 cam_rsb_1 cam_scasi_2 ...
    cam_sod_1 cam_tpas_1 cam_tro_1 cam_whit_1];
r.supported.any = [cam_dasi_1 cam_scasi_2];
r.supported.camera_image = [cam_dasi_1 cam_scasi_2];

% end of function
