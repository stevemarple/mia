function r = info_cam_mcgr_1_data
%INFO_CAM_MCGR_1_DATA Return basic information about cam_mcgr_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_mcgr_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=mcgr;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'mcgr';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = 'THEMIS';
r.facility_url = 'http://aurora.phys.ucalgary.ca/themis/';
r.facilityid = 10;
r.groupids = [];
r.id = 24;
r.latitude = 62.95;
r.location1 = 'McGrath';
r.location1_ascii = 'McGrath';
r.location2 = 'USA';
r.logo = '';
r.logurl = '';
r.longitude = -154.42;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
