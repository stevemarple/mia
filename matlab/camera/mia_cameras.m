function varargout = mia_cameras(info, varargin)

defaults.datatype = 'all';

[defaults unvi] = interceptprop(varargin, defaults);

switch char(info)
 case 'supported'
  
  varargout{1} = [cam_dasi];
  
 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end
  
