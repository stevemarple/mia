function r = info_cam_dasi_1_data
%INFO_CAM_DASI_1_DATA Return basic information about cam_dasi_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_dasi_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=dasi;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'dasi';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = NaN;
r.endtime = timestamp([2002 03 10 11 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.id = 1;
r.latitude = 69.35;
r.location1 = 'Skibotn';
r.location1_ascii = 'Skibotn';
r.location2 = 'Norway';
r.logo = 'lancasterunilogo';
r.logurl = '';
r.longitude = 20.36;
r.modified = timestamp([]);
r.name = 'DASI';
r.piid = [];
r.resolution = timespan(00, 'h', 00, 'm', 10, 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([1993 09 11 23 00 00]);
r.url = 'http://www.dcs.lancs.ac.uk/iono/dasi/';
r.defaultfilename.camera_image.default.archive = 'default';
r.defaultfilename.camera_image.default.dataclass = 'double';
r.defaultfilename.camera_image.default.defaultarchive = true;
r.defaultfilename.camera_image.default.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.camera_image.default.failiffilemissing = false;
r.defaultfilename.camera_image.default.format = 'mat';
r.defaultfilename.camera_image.default.fstr = 'http://www.dcs.lancs.ac.uk/iono/miadata/camera/dasi/%Y/%m/%d/%Y%m%d%H.mat';
r.defaultfilename.camera_image.default.loadfunction = '';
r.defaultfilename.camera_image.default.resolution = timespan([], 's');
r.defaultfilename.camera_image.default.savefunction = '';
r.defaultfilename.camera_image.default.size = [51 51 360];
r.institutions = {};
% end of function
