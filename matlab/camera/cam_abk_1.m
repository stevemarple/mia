function r = cam_abk_1
%CAM_ABK_1  CAMERA object for Abisko, Finland.

% Automatically generated by makeinstrumentdatafunctions
r = camera('abbreviation', 'abk', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'MIRACLE', ...
    'location', location('Abisko', 'Finland', ...
                         68.360000, 18.820000));

