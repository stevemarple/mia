function r = info_cam_scasi_2_data
%INFO_CAM_SCASI_2_DATA Return basic information about cam_scasi_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_scasi_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=scasi;serialnumber=2

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'scasi';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = NaN;
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.id = 2;
r.latitude = 69.35;
r.location1 = 'Skibotn';
r.location1_ascii = 'Skibotn';
r.location2 = 'Norway';
r.logo = 'lancasterunilogo';
r.logurl = '';
r.longitude = 20.36;
r.modified = timestamp([]);
r.name = 'SCASI';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 2;
r.starttime = timestamp([1998 02 18 00 00 00]);
r.url = 'http://www.dcs.lancs.ac.uk/iono/scasi/';
r.defaultfilename.camera_image.default.archive = 'default';
r.defaultfilename.camera_image.default.dataclass = 'double';
r.defaultfilename.camera_image.default.defaultarchive = true;
r.defaultfilename.camera_image.default.duration = timespan(1 , 'd');
r.defaultfilename.camera_image.default.failiffilemissing = false;
r.defaultfilename.camera_image.default.format = 'st';
r.defaultfilename.camera_image.default.fstr = 'http://www.dcs.lancs.ac.uk/iono/miadata/camera/scasi_2/%Y/%m/%d/%Y%m%d%H%M%S.st6';
r.defaultfilename.camera_image.default.loadfunction = 'scasi2mia_image';
r.defaultfilename.camera_image.default.resolution = timespan([], 's');
r.defaultfilename.camera_image.default.savefunction = '';
r.defaultfilename.camera_image.default.size = [-1 -1 -1];
r.institutions = {};
% end of function
