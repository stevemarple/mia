function process_dasi(dirname, varargin)

defaults.onerror = 'error'; 
defaults.recursive = 0;
[defaults unvi] = interceptprop(varargin, defaults);


dirname = absolutepath(dirname);

if ~exist(dirname, 'dir')
  error(sprintf('%s is not a directory', dirname));
end

d = dir(dirname);

for n = 1:length(d);
  fname = d(n).name;
  if isempty(fileparts(fname))
    % no directory, give it one
    fname = fullfile(dirname, d(n).name);
  end
  tmp = [];
  if d(n).isdir == 0
    disp(sprintf('processing %s', fname));
    tmp = dasi2mia_image(fname, 'onerror', defaults.onerror);

    %   elseif defaults.recursive & ~strcmp(d(n).name, '.') & ...
    % 	strcmp(d(n).name, '..')
    %     % don't make recursive calls to current or parent directory!
    %     % tmp = dasi2mia_image(fname, 'onerror', defaults.onerror);
    %     d(n).name
    %     prop = makeprop(defaults);
    %     feval(mfilename, fname, prop{:});
    %     return;
    %   end
  else
    if defaults.recursive & ~strcmp(d(n).name, '.') & ...
	  ~strcmp(d(n).name, '..')
      % don't make recursive calls to current or parent directory!
      % tmp = dasi2mia_image(fname, 'onerror', defaults.onerror);
      prop = makeprop(defaults);
      feval(mfilename, fname, prop{:});
      return;
    end
    
  end

  if ~isempty(tmp)
    mia = camera_image('starttime', getstarttime(tmp), ...
		       'endtime', getendtime(tmp), ...
		       'instrument', cam_dasi, ...
		       'log', 0, ...
		       'load', 1);
    % prod(getdatasize(mia))
    if prod(getdatasize(mia)) == 0
      mia = [];
    end
    
    
    if isempty(mia)
      mia = tmp;
    else
      mia = insert(tmp, mia); % let new data override what is on disk
    end
    save(mia);
  end
end


