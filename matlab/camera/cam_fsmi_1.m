function r = cam_fsmi_1
%CAM_FSMI_1  CAMERA object for Fort Smith, Canada.

% Automatically generated by makeinstrumentdatafunctions
r = camera('abbreviation', 'fsmi', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'THEMIS', ...
    'location', location('Fort Smith', 'Canada', ...
                         60.000000, 248.100000));

