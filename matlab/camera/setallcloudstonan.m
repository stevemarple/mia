load filenames2.txt
filenames = filenames2;
clear filenames2;

fstr = info(cam_dasi, 'defaultfilename');
cloudvec = repmat(nan, [1 360]);
for n = 1:size(filenames,1)
  t = timestamp([filenames(n,:), 0 0]);
  fn = strftime(t, fstr);
  load(fn);
  mia = setcloud(mia, cloudvec);
  save(mia)
end
