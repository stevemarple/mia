function f = opticalfilter(varargin)
%OPTICALFILTER  Constructor for OPTICALFILTER class,
%
%   r = OPTICALFILTER('name', name, ...)
%
%   See also OPTICALFILTERLIST.

% CHANGELOG
%
% version 1:
% f.name = 'unknown';
% f.wavelength = nan;
% f.upperwavelength = nan;
% f.lowerwavelength = nan;
%
% version 2:
% f.versionnumber  =2
% f.wavelength = nan;
% f.upperwavelength = [];
% f.lowerwavelength = [];
%
% Name field removed

latestversion = 2;
f.versionnumber = latestversion;
f.wavelength = nan;
f.upperwavelength = [];
f.lowerwavelength = [];


cls = 'opticalfilter';
if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  f = class(f, cls);
  
elseif nargin == 1 & isa(varargin{1}, cls)
  % copy constructor
  f = class(f, cls);

elseif nargin == 1 & isstruct(varargin{1})
  % construct from struct
  a = varargin{1};
  
  if ~isfield(a, 'versionnumber')
    a.versionnumber = 1;
  end
    
  switch a.versionnumber
   case 1
    % handle non-scalar structs
    f.wavelength = reshape([a.wavelength], size(a));
    f.upperwavelength =  reshape([a.upperwavelength], size(a));
    f.lowerwavelength =  reshape([a.lowerwavelength], size(a));
    
   case 2
    ; % latest version
    
   otherwise
    error(sprintf('unknown version for opticalfilter (was ''%d'')', ...
		  an.versionnumber));
    
  end
    
  f = class(f, cls);

elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [f unvi] = interceptprop(varargin, f);
  f = class(f, cls);
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

f.versionnumber = latestversion;

% check all parts are the same size
sz = size(f.wavelength);
if isempty(f.upperwavelength)
  f.upperwavelength = repmat(nan, sz);
elseif ~isequal(sz, size(f.upperwavelength))
  error('upperwavelength should be the same size as wavelength');
end

if isempty(f.lowerwavelength)
  f.lowerwavelength = repmat(nan, sz);
elseif ~isequal(sz, size(f.lowerwavelength))
  error('lowerwavelength should be the same size as wavelength');
end


