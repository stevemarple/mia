function s = char(f, varargin)
%CHAR Convert OPTICALFILTER object into a CHAR representation.
%
%   s = CHAR(f)
%   f: OPTICALFILTER object
%   s: CHAR representation.
%
%   See also OPTICALFILTER.

if numel(f) ~= 1
  s = matrixinfo(f);
  return;
end

% from here on f is scalar.

if length(varargin)
  format = varargin{1};
else
  format = 'c';
end
  
% use wavelength=0 to mean no filter, in which case set the wavelength
% strings blank.
st = [];
fn = {'wavelength' 'upperwavelength' 'lowerwavelength'};
fs = struct(f);
for n = 1:length(fn)
  tmp = getfield(fs, fn{n});
  if isequal(f.wavelength, 0)
    st = setfield(st, fn{n}, '');
  elseif isempty(tmp) | isnan(tmp)
    st = setfield(st, fn{n}, 'unknown');
  else
    st = setfield(st, fn{n}, printunits(tmp, 'm'));
  end
end

if isequal(f.wavelength, 0)
  st.wavelength = 'No filter';
end

switch format
 case 'c'
  % compact
  s = st.wavelength;
  
 case 'l'
  % long format
  s = sprintf(['nominal wavelength : %s\n' ...
	       'lower wavelength   : %s\n' ...
	       'upper wavelength   : %s\n' ], ...
	      st.wavelength, st.upperwavelength, st.lowerwavelength);
  
 otherwise
  error(sprintf('unknown format code (was ''%s'')', format));
end
