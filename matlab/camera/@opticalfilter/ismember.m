function r = ismember(a, s, varargin)
%ISMEMBER  Overload for OPTICALFILTER class.
%
%   See ops/ISMEMBER.

a_w = getwavelength(a);
s_w = getwavelength(s);
n = nargout;
if n == 0
  n = 1;
end

[varargout{1:n}] = ismember(a_w, s_w, varargin{:});
