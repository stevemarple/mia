function [r,idx] = sort(f)
%SORT  Overload for OPTICALFILTER class.
%
%   See datafun/SORT.

if prod(size(f)) ~= length(f)
  error('can only sort vectors');
end

w = f.wavelength(:);
u = f.upperwavelength(:);
l = f.lowerwavelength(:);

[tmp idx] = sortrows([w, u, l]);
idx = reshape(idx, size(f))

% r = f(idx);
s.type = '()';
s.subs = {idx};

r = subsref(f, s);


