function r = length(f)
%LENGTH LENGTH overloaded for OPTICALFILTER class.
%
% See LENGTH.

r = length(f.wavelength);
