function display(f)
%DISPLAY  Display function for OPTICALFILTER class.
%
%   DISPLAY(f)
%   f: OPTICALFILTER object to be printed
%
%   See also opticalfilter/CHAR.

disp(sprintf('\n%s = \n\n%s', inputname(1), char(f)));
