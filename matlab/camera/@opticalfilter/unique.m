function [b,i,j] = unique(a, varargin)
%UNIQUE  Overload for OPTICALFILTER class.
%
%   'rows' option is not supported.
%
%   See ops/UNIQUE.

w = a.wavelength(:);
u = a.upperwavelength(:);
l = a.lowerwavelength(:);
[tmp,i,j] = nonanunique([w, u, l], 'rows');

% b = a(i); 
s.type = '()';
s.subs = {i};

b = subsref(a, s);

