function r = ndims(f)
%NDIMS NDIMS overloaded for OPTICALFILTER class.
%
% See NDIMS.

r = ndims(f.wavelength);
