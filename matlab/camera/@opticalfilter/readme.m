function readme(varargin)
%README file for OPTICALFILTER class.
%
% Almost all all-sky camera measurements are made using optical filters to
% select a specific wavelength. Most filters have a narrow pass band, e.g.,
% 577.7nm +- 4nm. It is possible to use high-pass (or low-pass)
% filters. Also, the wavelength at the centre may not correspond to the
% exact wavelength of the intended emission. Therefore the OPTICALFILTER
% class stores 4 pieces of information about each filter:
%
%   1. The name of the filter (e.g., green line)
%   2. The intended wavelength (e.g., 577.7nm)
%   3. The shortest wavelength passed (normally -3dB)
%   4. The longest wavelength passed (normally -3dB)
%
% For high/low pass filters the shortest or longest wavelength should be
% set to 0 or +infinity. For the case of no filter the wavelengths should
% be set to NAN.
%
% Propose that the CAMERA_IMAGE class stores an object OPTICALFILTERLIST,
% which contains a vector of OPTICALFILTERs, and a list containing the index
% number of each filter, one element per image.

% if called as a function show this file as if "help readme" was typed.
[st idx] = dbstack;
help(st(1).name);
