function r = reshape(f, varargin)
%REPMAT REPMAT overloaded for OPTICALFILTER class.
%
% See REPMAT.
r = t;
r.wavelength      = reshape(r.wavelength, varargin{:});
r.upperwavelength = reshape(r.upperwavelength, varargin{:});
r.lowerwavelength = reshape(r.lowerwavelength, varargin{:});
