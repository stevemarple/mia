function r = horzcat(varargin)

w = cell(size(varargin));
uw = w;
lw = w;

for n = 1:nargin
  if ~isa(varargin{n}, 'opticalfilter')
    error('all elements must be opticalfilters');
  end
  w{n} = varargin{n}.wavelength;
  uw{n} = varargin{n}.upperwavelength;
  lw{n} = varargin{n}.lowerwavelength;
end

r = varargin{1};
r.wavelength = horzcat(w{:});
r.upperwavelength = horzcat(uw{:});
r.lowerwavelength = horzcat(lw{:});


