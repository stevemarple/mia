function r = subsasgn(a, s, b)

if isempty(b)
  b_w = [];
  b_uw = [];
  b_lw = [];
else
  if ~isa(b, 'opticalfilter')
    b = opticalfilter(b);
  end
  b_w = b.wavelength;
  b_uw = b.upperwavelength;
  b_lw = b.upperwavelength;
end

if isempty(a)
  r = opticalfilter;
else
  r = a;
end
r.wavelength = subsasgn(r.wavelength, s, b_w);
r.upperwavelength = subsasgn(r.upperwavelength, s, b_uw);
r.lowerwavelength = subsasgn(r.lowerwavelength, s, b_lw);

