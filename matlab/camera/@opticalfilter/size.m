function r = size(f, varargin)
%SIZE SIZE overloaded for OPTICALFILTER class.
%
% See SIZE.

r = size(f.wavelength, varargin{:});
