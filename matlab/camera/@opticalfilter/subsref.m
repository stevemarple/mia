function r = subsref(a, s)
r = a;
r.wavelength = subsref(r.wavelength, s);
r.upperwavelength = subsref(r.upperwavelength, s);
r.lowerwavelength = subsref(r.lowerwavelength, s);
