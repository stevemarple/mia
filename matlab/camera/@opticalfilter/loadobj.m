function r = loadobj(a)
%LOADOBJ  Load filter for OPTICALFILTER object.
%

if isstruct(a)
  r = opticalfilter(a);
else
  r = a;
end

