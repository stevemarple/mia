function r = isempty(f)
%ISEMPTY ISEMPTY overloaded for OPTICALFILTER class.
%
% See ISEMPTY.

r = isempty(f.wavelength);

