function r = eq(a, b, varargin)
%EQ  Equality (==) operator for OPTICALFILTER class.
%
%   r = (a == b)
%   r = EQ(a, b)
%   r = EQ(a, b, tol)
%
%   r: scalar or vector indcating equality (1) or not (0)
%   a: OPTICALFILTER
%   b: OPTICALFILTER
%   tol: optional tolerance

% matrices may contain NaNs (which is valid), so the need specific testing

if length(varargin) & ~isempty(varargin{1})
  tol = varargin{1};
else
  tol = 1024*eps;
end

if ~isa(b, 'opticalfilter')
  error('b is not an opticalfilter object');
end

if isempty(a) & isempty(b)
  r = 1;
  return
elseif isempty(a)
  r = zeros(size(b));
  return
elseif isempty(b)
  r = zeros(size(a));
  return
end

% if a or b is not a scalar then return value must reflect that
% fact. This means using nonnanisequal would be wrong

r = (abs([a.wavelength] - [b.wavelength]) <= tol | ...
     (isnan([a.wavelength]) & isnan([b.wavelength]))) & ...
    (abs([a.upperwavelength] - [b.upperwavelength]) <= tol | ...
     (isnan([a.upperwavelength]) & isnan([b.upperwavelength]))) & ...
    (abs([a.lowerwavelength] - [b.lowerwavelength]) <= tol | ...
     (isnan([a.lowerwavelength]) & isnan([b.lowerwavelength])));


function r = localEq(a, b)

r = (a == b) | (isnan(a) & isnan(b))


