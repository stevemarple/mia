function r = numel(f)
%NUMEL NUMEL overloaded for OPTICALFILTER class.
%
% See NUMEL.

r = numel(f.wavelength);
