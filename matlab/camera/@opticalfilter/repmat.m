function r = repmat(f, varargin)
%REPMAT REPMAT overloaded for OPTICALFILTER class.
%
% See REPMAT.

r = f;
r.wavelength      = repmat(f.wavelength, varargin{:});
r.upperwavelength = repmat(f.upperwavelength, varargin{:});
r.lowerwavelength = repmat(f.lowerwavelength, varargin{:});
