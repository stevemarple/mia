function r = info_cam_loz_1_data
%INFO_CAM_LOZ_1_DATA Return basic information about cam_loz_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about cam_loz_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=camera;abbreviation=loz;serialnumber=1

r.limits = [];
r.limits = [];
r.pixels = [];
r.abbreviation = 'loz';
r.bibliography = '';
r.comment = '';
r.credits = '';
r.datarequestid = [];
r.defaultheight = [];
r.endtime = timestamp([]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.id = 8;
r.latitude = 67.98;
r.location1 = 'Lovozero';
r.location1_ascii = 'Lovozero';
r.location2 = 'Russia';
r.logo = '';
r.logurl = '';
r.longitude = 35.02;
r.modified = timestamp([]);
r.name = '';
r.piid = [];
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.serialnumber = 1;
r.starttime = timestamp([]);
r.url = '';
r.defaultfilename = [];
r.institutions = {};
% end of function
