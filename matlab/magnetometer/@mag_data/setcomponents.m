function r = setcomponents(mia, comps)
%SETCOMPONENTS  Set the components of a MAG_DATA object
%
%   r = SETCOMPONENTS(mia, comps)
%
%   r: updated MAG_DATA object
%   mia: MAG_DATA object
%   comps: cell array of strings being the component names

r = mia;
r.components = comps;

