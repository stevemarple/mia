function r = getparameterindex(mia, p)
%GETPARAMETERINDEX  Get row indices of parameters.
%
%   See also GETCOMPONENTSINDEX.

r = getcomponentindex(mia, p);
