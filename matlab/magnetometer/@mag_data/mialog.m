function mialog(mia, varargin)
%MIALOG  Log data usage, overloaded for MAG_DATA.
%
% See mia_base/MIALOG.

for n = 1:numel(mia)
  m = mia(n);
  if ~isempty(getdata(m))
    comp = getcomponents(m);
    if iscell(comp)
      comp = join(',', comp);
    end
    mia_base_mialog(m, varargin{:}, 'components', comp);
  end
end
