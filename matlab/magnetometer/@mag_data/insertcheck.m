function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for MAG_DATA data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mia_base_insertcheck(a, b);


components_a = getcomponents(a);
components_b = getcomponents(b);
% rs.components = unique([components_a components_b]);
rs.components = [components_a setdiff(components_b, components_a)];

% now modify the row details for subsasgn to include all different comps
[tmp aa.subs{1}] = ismember(components_a, rs.components);
[tmp ba.subs{1}] = ismember(components_b, rs.components);

rs.datasize(1) = numel(rs.components);

