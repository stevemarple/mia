function r = gettype(mia, varargin)
%GETTYPE  Return magnetometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: MAG_DATA object
%
%   See also MAG_DATA, MAGNETOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'magnetometer data';
  
 case 'u'
  r = 'MAGNETOMETER DATA';
  
 case 'c'
  r = 'Magnetometer data';
 
 case 'C'
  r = 'Magnetometer Data';
 
 otherwise
  error('unknown mode');
end

return

