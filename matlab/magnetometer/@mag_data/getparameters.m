function r = getparameters(mia)
%GETPARAMETERS  Return the parameters (components) in a MAG_DATA object
%
%   r = GETPARAMETERS(mia)
%   r: CELL array of parameters
%   mia: MAG_DATA object

if length(mia) ~= 1
  error('mag_data object must be scalar');
end

r = mia.components;
if ~iscell(r)
  r = {r};
end
