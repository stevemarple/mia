function [fh, gh, ph2, prelimH, mult] = plot(mia, varargin)
%PLOT  Plot data from a MAG_DATA object.
%
%   [fh gh ph] = PLOT(mia, ...)
%   fh: FIGURE handle
%   gh: AXES handle(s)
%   ph: LINE handle(s)
%
% PLOT plots a MAG_DATA object into anew FIGURE (created by
% MAKEPLOTFIG). The following parameter name/value pairs may be used to
% alter the default plotting behaviour.
%
%   'components', NUMERIC
%   Select the beam(s) to plot. By default the preferred beam for a given
%   RIOMETER is used (see riometer/INFO2 for details about preferred
%   beam). If only one plot axes is in operation and mutliple beams are
%   selected then they are all plotted on the same axes, otherthewise the
%   number of beams chosen should match the number of available plot
%   axes.
%
%   'color', COLORSPEC
%   Select the color used for the plotted data. See COLORSPEC for valid
%   methods to specify the color.
%
%   'miniplots' [x y]
%   Adjust the number of plot axes used. A [1x2] NUMERIC array is
%   required, specifying the number of plots in the x and y directions.
%   
%   'plotaxes', AXES handle(s)
%   Do not create a new FIGURE, instead plot into the selected AXES. The
%   number of AXES handles should match the number of components selected.
%
%   'ylim', [1x2 NUMERIC]
%   Adjust the Y limits used when plotting.
%
% Any unknown parameters are passed onto MAKEPLOTFIG.
%
% See also MAKEPLOTFIG, MAG_DATA.

fh = [];
gh = [];
ph2 = [];
prelimH = [];
mult = [];

instrument = getinstrument(mia);


comps = getparameters(mia);

% % our default values
% defaults.beams = [intersect(wideBeams, beams) ...
% 		  intersect(imagingBeams, beams)];
% if ~isempty(defaults.beams)
%   % choose first wide beam, else imaging beam 
%   defaults.beams = defaults.beams(1); 
% end

% plot all components by default
% defaults.components = getparameters(mia);
defaults.components = comps;
defaults.miniplots = []; % [1 length(defaults.components)];
defaults.spacing = [];
defaults.title = '';
defaults.comment = '';
defaults.windowtitle = '';
defaults.plotaxes = [];
defaults.color = 'b';
defaults.ylim = info(instrument, 'limits', mia);
defaults.heater_data = [];
defaults.pointer = 'arrow';

defaults.preliminary = '';
[defaults unvi] = interceptprop(varargin, defaults);

if ~iscell(defaults.components)
  defaults.components = {defaults.components};
end
if isempty(defaults.miniplots)
  defaults.miniplots = [1 length(defaults.components)];
end

if ~isempty(defaults.plotaxes)
  defaults.miniplots = size(defaults.plotaxes); % override setting
  gh = defaults.plotaxes;
  if any(~ishandle(gh))
    error('invalid handle');
  end
  % args = {varargin{unvi} 'plotaxes', defaults.plotaxes};
  args = varargin(unvi); % args = {varargin{unvi}};
  fh = get(gh, 'Parent');
  if iscell(fh)
    fh = cell2mat(fh);
  end
  fh = unique(fh);
else
  gh = [];
  args = varargin(unvi); % args = {varargin{unvi}};
end
numOfComps = prod(size(defaults.components));
if prod(defaults.miniplots) < numOfComps
  error(sprintf(['%d plot windows specified, but %d components ' ...
	'requested!'], prod(defaults.miniplots), numOfComps));
end    

% calculate title now that comps are known
[title windowtitle] = maketitle(mia, ...
				'components', defaults.components, ...
				'comment', defaults.comment);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

% Get a figure complete with menus etc if isempty(gh)
% get a figure window decorated with the standard controls.
% except for miniPlots, allow other functions to override
if isempty(gh)
  if prod(defaults.miniplots) > 1
    defaultSpacing = [0.2 0.3 0.2 0.3 0.2 0.3];
    if isempty(defaults.spacing)
      defaults.spacing = defaultSpacing;
    end
    defaults.spacing(isnan(defaults.spacing)) = ...
	defaultSpacing(isnan(defaults.spacing));
  end

  [fh gh] = makeplotfig('init', ...
			'logo', info(instrument', 'logo'), ...
			args{:}, ...
			'spacing', defaults.spacing, ...
			'miniplots', defaults.miniplots, ...
			'pointer', 'watch', ...
			'linkedzoom', 1, ...
			'title', defaults.title);
end

% make subplots for all comps requested
n = 1;
compindex = getcomponentindex(mia, defaults.components);

data = getdata(mia);
xdata = getcdfepochvalue(getsampletime(mia));
st = getstarttime(mia);
et = getendtime(mia);
xlim = getcdfepochvalue([st et]);
% xdata = getcdfepochvalue(getsampletime(mia));
dataquality = getdataquality(mia);

for y = 1:defaults.miniplots(2)
  for x = 1:defaults.miniplots(1)
    if n <= numOfComps
      keepKeys = {'Tag' 'UserData'};
      keepVal = get(gh(x,y), keepKeys); 

      % if dataquality issues exist warn about them
      if ~isempty(dataquality)
	makeplotfig('addwatermark', gh(x,y), {dataquality});
      end

      % get multiplier
      [tmp mult] = datalabel2(mia, 'parameterindex', compindex(n));
      
      if 0
	% plot the data
	ph = plot(xdata, data(compindex(n),:)*10^(-mult), ...
	    'Parent', gh(x,y), ...
	    'Color', defaults.color); 
	ph = flipud(ph); % last line is first
      
      else
	ph = line('Parent', gh(x,y), ...
		  'XData', xdata, ...
		  'YData', data(compindex(n),:)*10^(-mult), ...
		  'Color', defaults.color, ...
		  'Tag', sprintf('%s %s-component', ...
				 char(instrument, 'c'), comps{n})); 
      end
      set(gh(x,y), keepKeys, keepVal, 'Visible', 'on');
      if length(n) == 1
	% set(ph, 'Tag', sprintf('%s-component', defaults.components{n}));
      else
	% NOT WORKING
	% multiple plots in one window
	% for m = 1:prod(size(n))
	  % set(ph(m), 'Tag', sprintf('beam %d', beams(m)));
	% end
	% tags = cell(prod(size(n), 1));
	% for m = 1:length(tags)
	%   tags{m, 1} = sprintf('beam %d', beams(m));
	% end
	% set(ph, {'Tag'}, tags);
      end	
      ph2(end+ [1:length(ph)]) = ph;
    else
      % axis blank 
      set(gh(x,y), 'Visible', 'off');
    end
    n = n + 1;
  end
end





% set(gh, 'XLim', [0 size(data,2)], 'Box', 'on');
set(gh, 'XLim', xlim, 'Box', 'on');
% timetick(ifb, gh, 'X', 0);  % don't have the axes labelled

if any(isnan(defaults.ylim))
  notNans = find(~isnan(defaults.ylim));
  for n = 1: prod(size(gh))
    curLim = get(gh(n), 'YLim');
    curLim(notNans) = defaults.ylim(notNans);
    set(gh(n), 'YLim', curLim);
  end
elseif ~isempty(defaults.ylim)
  set(gh, 'YLim', defaults.ylim);
end

% -------
if isempty(defaults.plotaxes) 
  if exist('heatermenu')
    % make tools menus visible
    toolsMenuHandle = findall(fh, 'Tag', 'figMenuTools', 'Type', 'uimenu');
    if isempty(toolsMenuHandle)
      toolsMenuHandle = findobj(fh, 'Tag', 'tools', 'Type', 'uimenu');
    end
    
    if ~isempty(toolsMenuHandle)
      set(toolsMenuHandle, 'Visible', 'on');
      heatermenu('init', toolsMenuHandle, ...
		 'onofftimes', 'on', ...
		 'heater_data', defaults.heater_data);
    end
  end

end

n=1;
for y=1:defaults.miniplots(2)
  for x=1:defaults.miniplots(1)
    if n <= numOfComps
      set(get(gh(x,y), 'YLabel'), ...
	  'String',['\bf ' datalabel(mia,'parameterindex',compindex(n))], ...
	  'FontSize', 14);
    end
   n=n+1;
  end
end

if length(gh) > 1
  % timetick(gh(1:(end-1)), 'X', st, et, timespan(1, 'ms'), 0);
  timetick2(gh(1:end));
end
% timetick(gh(end), 'X', st, et, timespan(1, 'ms'), 1);
timetick2(gh(end));

if isempty(defaults.plotaxes)
  set(fh, 'Name', defaults.windowtitle, ...
	  'Pointer', defaults.pointer);
end


return;

