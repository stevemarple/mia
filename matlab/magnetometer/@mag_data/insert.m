function [r,aa,ar,ba,br,rs] = insert(a, b)
%INSERT  Insert MAG_DATA object into another MAG_DATA object.
%
%   r = INSERT(a, b)
%
%   If times do not overlap NAN is inserted.

[r aa ar ba br rs] = mia_base_insert(a, b);

r = setcomponents(r, rs.components);

