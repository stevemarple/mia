function ascii(mia, filename, varargin)
%ASCII Overloaded version for MAG_DATA.
%
% This function ensures that data having units of Tesla are output as
% nT.
%
% See mia_base/ASCII.

% switch to nT
for n = 1:numel(mia)
  if strcmp(getunits(mia(n)), 'T')
    mia(n) = setunits(setdata(mia(n), getdata(mia(n)) * 1e9), 'nT');
  end
end

mia_base_ascii(mia, filename, 'format', '%g ', varargin{:});

