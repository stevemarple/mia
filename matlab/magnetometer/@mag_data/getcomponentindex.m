function i = getcomponentindex(mia, comps)
%GETCOMPONENTINDEX  Get indices of components in a MAG_DATA object data matrix
%
%   r = GETCOMPONENTINDEX(mia, comps)
%
%   r: the row indices of the components in data matrix
%   mia: a MAG_DATA object
%   comps: a cell array of strings being the names of the components
%
%   See also GETCOMPONENTS.

if ischar(comps)
  comps = {comps};
end

% i = repmat(NaN,1,length(comps));
i = zeros(1, length(comps));
for k = 1:length(comps)
  ii = find(strcmp(comps{k}, mia.components));
  if ~isempty(ii)
    i(k) = ii;
  end
end

