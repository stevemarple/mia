function [r, m] = datalabel2(mia,varargin)

defaults.parameterindex = [];
[defaults,unvi] = interceptprop(varargin,defaults);
if ~isempty(unvi)
  warning(['Parameters ignored: ' sprintf('%s', varargin{unvi(1:2:end)})])
end

if isempty(defaults.parameterindex)
  error('parameterindex must be specified to indicate which component!')
end

units = char(getunits(mia, 'parameterindex', defaults.parameterindex));

comps = getparameters(mia);
comp = comps{defaults.parameterindex};

r = sprintf('%s (%%s)', comp);
switch units
 case 'T'
  m = -9;
 case 'deg'
  m = 0;
 otherwise
  m = 0;
end
