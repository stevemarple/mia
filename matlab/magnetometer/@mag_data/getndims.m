function nd=getndims(mia)

if ~isa(mia,'mag_data')
  error('Panic! mag_data/getndims called with non-mag_data object!')
end

nd=2;

