function mia2samnet(mia, varargin)
%MIA2SAMNET  Save MAG_DATA in SAMNET format.
%
%   MIA2SAMNET(mia, ...)
%
% Save the MIA object in SAMNET format. The default behaviour may be
% modified by including the following parameter name/value pairs:
% 
%   'filename', CHAR
%    Save using the given filename, instead of the standard system one
%    (or one found from within the object).
%
%   'makedirectory', LOGICAL
%   Create the directory (if necessary) before attempting to save the
%   filename.
%
%   'gpsinfo', DOUBLE
%   A matrix containing the GPS information for the data.
%
% See also MAG_DATA.

miaNumEl = numel(mia);
if miaNumEl ~= 1
  if nargin ~= 1
    % only possible when saving objects to their default system filenames
    error('cannot save multiple objects to the same filename');
  end
  for n = 1:miaNumEl
    feval(mfilename, mia, varargin{:});
  end
  return
end

% from this point on mia is scalar
cls = class(mia);

defaults.filename = '';
defaults.makedirectory = 0;
defaults.gpsinfo = [];

if strcmp(cls, 'mag_data')
  defaults.archive = '';
  eval('defaults.archive = getarchive(mia);');
elseif strcmp(cls, 'mag_qdc')
  defaults.archive = 'qdc';
end

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

st = getstarttime(mia);
et = getendtime(mia);
res = getresolution(mia);
resSecs = gettotalseconds(res);
mag = getinstrument(mia);
comp = getcomponents(mia);
if isempty(comp) 
  error('no components to save');

elseif prod(getdatasize(mia)) == 0
  error('no data to save');
end

if isfield(defaults, 'archive')
  if isempty(defaults.archive)
    % probably should be a bit friendly when not saving to the system
    % default locations
    error('must specify archive');
  end
end

mag_data_props = {};
if isfield(defaults, 'archive')
  mag_data_props = {'archive', defaults.archive};  
elseif isa(mia, 'mag_variation')
  mag_data_props = {'variation', getvariation(mia)};
end

% [filenameFstr fileDur fifm format datasize loadFunction ...
% saveFunction recordLen archiveRes] = ...
%    info(mag, 'defaultfilename', mia, mag_data_props{:});
s = info(mag, 'defaultfilename', cls, mag_data_props{:});

recordDur = s.recordlength * s.resolution;

if s.resolution ~= res
  error(sprintf('resolution (%s) does not match archive resolution (%s)', ...
		char(res), char(s.resolution)));
end
		
if isempty(defaults.filename)
  % try gleaning the filename to be used from the object
  defaults.filename = getfilename(mia);
end

% if still empty use the standard one
if isempty(defaults.filename)
  % construct the standard filename
  defaults.filename = s.fstr;
  defaults.makedirectory = 1;
end




% check the data is suitable to save in SAMNET format
if st ~= floor(st, s.duration)
  error(sprintf('object does not start on the appropriate boundary (%s)', ...
		char(s.duration)));
elseif et ~= floor(et, s.duration)
  error(sprintf('object does not end on the appropriate boundary (%s)', ...
		char(s.duration)));
elseif ~isequal(comp, info(mag, 'components'))
  error('incorrect components');
end

% if ~isempty(defaults.gpsinfo) & numel(defaults.gpsinfo) ~= (et-st)./ recordDur
%  error('incorrect number of elements in gpsinfo');
% end

if strcmp(defaults.archive, '5s')
  dataFstrTmp = '%7.2f';
elseif strcmp(defaults.archive, 'incoming')
  dataFstrTmp = '%05d';
elseif strcmp(defaults.archive, 'qdc')
  dataFstrTmp = '%.1f';
else
  dataFstrTmp = '%6.1f';
end

dataFstr = dataFstrTmp;
for n = 2:length(comp)
  dataFstr = [dataFstr ' ' dataFstrTmp];
end
dataFstr(end+1) = 10; % add newline

headerNum = 0;
lastCalcFilename = '';   % filename for last block saved
gzipped = 0;             % file is to be compressed
filename = '';           % current filename (never compressed)
gzfilename = '';         % final filename if gzipped


fid = -1;
idx = 0;
t = st;
while t < et
  % calculate filename
  calcFilename = strftime(t, defaults.filename);
      
  if ~strcmp(calcFilename, lastCalcFilename)
    % need a new file
    fid = localFclose(fid, filename, gzfilename, gzipped, defaults);

    gzipped = strcmp('gz', calcFilename((end-1):end));
    if gzipped
      % write to a temporary file which is gzipped when closed
      filename = tempname2;
      gzfilename = calcFilename;
    else
      filename = calcFilename;
      if logical(defaults.makedirectory)
	mkdirhier(fileparts(filename));
      end
    end
        
    
    disp(['saving to ' filename]);
    [fid mesg] = fopen(filename, 'w');
    if fid == -1
      error(sprintf('cannot open file: %s', mesg));
    end
  end
  
  % write out the header
  headerNum = headerNum + 1;
  switch cls
   case 'mag_data'
    switch defaults.archive
     case {'1s' 'preliminary'}
      gps = '0000';      
      resSecs = 0; % for some unknown reason 1s resolution uses 0!
     case {'realtime' 'scratch'}
      gps = '0000';      
     case '5s'
      gps = '0000';
     case 'incoming'
      gps = '0000';
     otherwise 
      fid = fclose(fid);
      error(sprintf('unknown archive (was ''%s'')', defaults.archive));
    end
    if ~isempty(defaults.gpsinfo)
      % gps = defaults.gpsinfo{headerNum};
      % gps = sprintf('%x', defaults.gpsinfo(headerNum));
      if isstruct(defaults.gpsinfo)
	% find header
	hn = find(defaults.gpsinfo.headertime == t);
	if isempty(hn)
	  warning(strftime(t, 'header missing for %Y-%m-%d %H:%M:%S'));
	else
	  hn = hn(1); % take one only!
	  headerwords = split(' ', defaults.gpsinfo.headers{hn});
	  gps = headerwords{end};
	end
      else
	% It hasn't always been a struct, but exactly what format was it
        % before? Bail out now and sort it out if such a case is found.
	error('gpsinfo not a struct, not sure how to proceed');
      end
    end

    if strcmp(defaults.archive, 'incoming')
      fprintf(fid, '%s%s %s\n', ...
	      samnetcode(mag), ...
	      strftime(t, '%d/%m/%Y  %H%M%S'), ...
	      gps);
    else
      fprintf(fid, '%s%s %02d %s\n', ...
	      samnetcode(mag), ...
	      strftime(t, '%d/%m/%Y %H%M%S'), ...
	      resSecs, ...
	      gps);
    end
    
   case 'mag_qdc'
    % yo00/01/1996 000000 05 aver
    fprintf(fid, '%s%s %02d aver\n', ...
	    samnetcode(mag), ...
	    strftime(t, '%d/%m/%Y %H%M%S'), ...
	    resSecs);
   
   otherwise
    fid = localFclose(fid, filename, gzfilename, gzipped, defaults);
    error(sprintf('unknown class (was ''%s'')', cls));
  end
  
  mesg = ferror(fid);
  if ~isempty(mesg)
    fid = localFclose(fid, filename, gzfilename, gzipped, defaults);
    error(sprintf('problem writing to %s: %s', filename, mesg));
  end
  
  % write out the data, mapping NaN to 9999.9 and mapping to nT from T
  idx = (idx(end)+1):(idx(end)+s.recordlength);
  
  if strcmp(defaults.archive, 'incoming')
    data = round(getdata(mia, ':', idx));
    data(isnan(data)) = 0;
  else
    data = getdata(mia, ':', idx) * 1e9;
    data(isnan(data)) = 9999.9;
  end

    
  fprintf(fid, dataFstr, data);

  mesg = ferror(fid);
  if ~isempty(mesg)
    fid = localFclose(fid, filename, gzfilename, gzipped, defaults);
    error(sprintf('problem writing to %s: %s', filename, mesg));
  end

  lastCalcFilename = calcFilename;
  t = t + recordDur;
end

fid = localFclose(fid, filename, gzfilename, gzipped, defaults);





function r = localFclose(fid, filename, gzfilename, gzipped, defaults)

r = -1;
% close the file (unless it has been closed). 
if fid == -1
  return;
end

fclose(fid);

if gzipped 
  if defaults.makedirectory
    mkdirhier(fileparts(gzfilename));
  end
  
  % gzip the file. Don't save the original name since it was a temporary
  % name anyway
  disp(['gzipping to ' gzfilename]);
  [rv mesg] = unix(sprintf('gzip -c -n %s > %s', filename, gzfilename));
  if rv ~= 0
    error(sprintf('problem with gzip: %s', mesg));
  end
  delete(filename);
end
