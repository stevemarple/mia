function r = char(mia, varargin)
%CHAR  Convert a MAG_DATA object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia, varargin{:});
  return;
end

r = sprintf(['%s' ...
	     'components        : %s\n'], ...
 	    mia_base_char(mia, varargin{:}), local_cell2str(mia.components));

function s=local_cell2str(ca)

if isempty(ca)
  s='(none)';
elseif iscell(ca)
  s='{ ';
  for k=1:length(ca)
    if ischar(ca{k})
      s=[s '''' ca{k} ''' '];
    end
  end
  s=[s '}'];
elseif ischar(ca)
  s = ca;
else
  error(sprintf('do not know what to do with %s class', class(ca)));
end

