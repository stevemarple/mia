function [sr,s] = extractcheck(mia, varargin)
%EXTRACTCHECK  EXTRACTCHECK for MAG_DATA.
%
% See mia_base/EXTRACTCHECK.

if length(varargin) == 1
  % subscript interface
  [sr s] = mia_base_extractcheck(mia, varargin{:});
  return
end  


% parameter name/value interface

% accept components and parameters as aliases
defaults.components = {};
defaults.parameters = defaults.components;

[defaults unvi] = interceptprop(varargin, defaults, ...
				{'components', 'parameters'});

[sr s] = mia_base_extractcheck(mia, varargin{unvi});

if ~isempty(defaults.components)
  % ensure components are unique. Also ensure order of components is
  % preserved
  if ischar(defaults.components)
    defaults.components = {defaults.components};
  end
  
  uniqComps = unique(defaults.components);
  ci = sort(getcomponentindex(mia, uniqComps));
  if any(ci == 0)
    error(sprintf('component(s) %s do not exist in this object', ...
		  join(', ', uniqComps(ci == 0))));
  end
  if strcmp(sr.subs{1}, ':')
    sr.subs{1} = ci;
  else
    sr.subs{1} = intersect(sr.subs{1}, ci);
  end
end

