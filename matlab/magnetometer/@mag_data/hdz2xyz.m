function r = hdz2xyz(mia);

if ~isequal(getcomponents(mia), { 'H' 'D' 'Z' })
  error('data not in HDZ format')
end;


st = getstarttime(mia);
loc = getlocation(getinstrument(mia));

% no way currently to get altitude
disp('Altitude of magnetometer unknown: using 0m');
alt = 0;


r1 = geocgm('year', getyear(st), ...
	    'location', loc, ...
	    'height', alt, ...
	    'direction', 'geo2cgm');


hdz_data = getdata(mia);

% What is the actual baseline in absolute terms? No idea at present so
% assume that the midpoint of the 16 bit scale (3276.8 nT) corresponds to
% the IGRF values
Hoffset = r1.igrf_h - 3276.8e-9;
Doffset = -3276.8e-9;
Zoffset = r1.igrf_z - 3276.8e-9;


trueH = hdz_data(1,:) + Hoffset;
trueD = hdz_data(2,:) + Doffset;

% convention is that D is the angle
angle = (r1.igrf_d)/360*2*3.14159;   

trueZ = hdz_data(3,:) + Zoffset;


xyz_data = [trueH .* cos(angle) - trueD.*sin(angle);
	    trueH .* sin(angle)+ trueD.*cos(angle);
	    trueZ];

r = setcomponents(setdata(mia, xyz_data), {'X' 'Y' 'Z'});
r = addprocessing(r, 'Converted to XYZ coordinates from HDZ');

