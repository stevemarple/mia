function mia = mag_data(varargin)
% MAG_DATA  Constructor for MAG_DATA class

cls = 'mag_data'; % our class name
parent = 'mia_base'; % our parent class

latestversion = 1;
mia.versionnumber = latestversion;
mia.components = {}; % The component names

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & isa(varargin{1},cls)
  % copy constructor   
  mia = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  if ischar(defaults.components)
    defaults.components = {defaults.components};
  end
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
  p = feval(parent, varargin{unvi});
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if defaults.log
      mialog(mia);
    end
  end

else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number. In Matlab 5.1 cannot simultaneously  use "()" and "."
for n = 1:numel(mia)
  tmp = mia(n);
  tmp.versionnumber = latestversion;
  mia(n) = tmp;
end
