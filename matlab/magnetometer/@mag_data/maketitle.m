function [plottitle, windowtitle] = maketitle(miaIn, varargin)
%MAKETITLE  Make plot and figure titles.
%
%   [plottitle, windowtitle] = maketitle(mia)
%   [plottitle, windowtitle] = maketitle(mia, ...)
%
%   Create the titles based on current time/date, data type etc. Various
%   values may be overriden by specifying a parameter name and value (see
%   INTERCEPTPROP). The defaults values are created from the IRISFILEBASE
%   object (or an object derived from IRISFILEBASE). The valid parameters
%   which may be overriden are described below:
%
%     'style', style
%        For IMAGEDATA objects the default title style is 'image'. This adds
%        the word 'image' or 'images' to the result of
%        GETNAME(mia). Otherwise the default title style is 'line', which
%        instead prints the beams. 
%
%     'customstring', string
%        Observe chosen style but override the string printed in
%        bold. Useful for Keogram plots and similar.
%
%     'starttime', TIMESTAMP
%
%     'endtime', TIMESTAMP
%
%     'resolution', TIMESPAN
%     'resolution', CHAR
%        If the resolution is given as a TIMESPAN then it is converted to
%        a suitable string. If it is a CHAR array then it is printed
%        without change.   
%
%     'location', IRISLOCATION
%
%     'beams', [beams]
%        The default beams are obtain from the IRISFILEBASE object. The
%        beams are not printed for the 'image' style, otherwise the beams
%        are printed in compressed format by PRINTSERIES.
%
%     'step', step
%        For image plots using only 1 out of n images, step should be set to
%        n.
%
%     'flipaxes', str
%        The orientation of the images. The string should be one of those
%        returned by MAKEPLOTFIG('flipaxesnames');
%
%     'comment', comment
%        An optional comment, to be printed at the bottom of the title
%
%   See also IRISFILEBASE, IMAGEDATA, MAKEPLOTFIG, GETNAME, PRINTSERIES.

plottitle = '';
windowtitle = '';

mia = miaIn(1);



% general format is
% Power/time (beam 1)
% 00:00:00 - 12:00:00 UT 1/1/1999 @ 1 m res.
% Kilpisjarvi, Finland (69.05N, 20.79E)
% [comment]
%
% If step ~= 1 then print that and resolution on separate line to time
% formatStr = '{\\bf %s} %s\n%s%s\n%s%s';
formatStr = { ...
    '%s %s' ...
    '%s%s' ...
    '%s%s' };

% entries are:
styleStr = ''; % 'Power/time images', 'Absorption/time', etc
compStr = '';
locStr = '';
dateStr = '';
resStr = '' ; % including step size if appropriate

instrument = getinstrument(mia);
loc = getlocation(instrument);

% default values
defaults.components  = getparameters(mia);
defaults.starttime = getstarttime(mia);
defaults.endtime = getendtime(mia);
[defaults.resolution resmesg] = getresolution(mia);

defaults.location = loc;
defaults.step = 1;
defaults.style = 'line';
defaults.customstring = '';
defaults.comment = '';
defaults.flipaxes = '';

if nargin > 1
  [defaults unvi] = interceptprop(varargin, defaults);
end

% create normal version of strings
switch length(defaults.components)
  case 0
    compStr = '';
  case  1
    compStr = [char(defaults.components) ' component'];
  otherwise
    compStr = [char(defaults.components)' ' components'];
end

locStr = char(defaults.location);
dateStr = dateprintf(defaults.starttime, defaults.endtime);
if isempty(defaults.resolution)
  resStr = '';
elseif ischar(defaults.resolution)
  resStr = defaults.resolution;
elseif isa(defaults.resolution, 'timespan')
  if ~isvalid(defaults.resolution)
    resStr = '';
  else
    resStr = [' @ ' char(defaults.resolution, 'c') ' res.'];
  end
else
  error(sprintf('bad class for resolution (was ''%s'')', ...
		class(defaults.resolution)));
end

% modify standard formmating if not appropriate
if strcmp(defaults.style, 'line')
  if ~isempty(defaults.customstring)
    styleStr = defaults.customstring;
  else
    styleStr = gettype(mia, 'c');
  end
  windowtitle = [styleStr ': ' compStr];
  
else
  warning('unknown style');
  plottitle = '';
  windowtitle = '';
  return;
end

if ~isempty(defaults.comment)
  % put comment on separate line
  defaults.comment = sprintf('\n%s', defaults.comment);
end

% if ~isempty(defaults.customstring)
%   styleStr = defaults.customstring;
% end

if ~isempty(compStr)
  compStr = ['(' compStr ')'];
end
% plottitle = sprintf(formatStr, styleStr, beamStr, dateStr, resStr, ...
%    locStr, defaults.comment);
plottitle = { ...
      sprintf(formatStr{1}, styleStr, compStr) ...
      sprintf(formatStr{2}, dateStr, resStr)};

if length(miaIn) == 1
  plottitle{end+1} = sprintf(formatStr{3}, locStr, defaults.comment);
end
