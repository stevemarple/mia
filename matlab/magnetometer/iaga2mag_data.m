function r = iaga2mag_data(iaga)

% Convert input in IAGA format to mag_data object(s)

% Mappings from IAGA station codes to MIA instruments
abbrev2mag.ABK = mag_abk_1;
abbrev2mag.AND = mag_and_1;
abbrev2mag.BJN = mag_bjn_1;
abbrev2mag.DOB = mag_dob_1;
abbrev2mag.HAN = mag_han_2;
abbrev2mag.HOP = mag_hop_1;
abbrev2mag.HOR = mag_hor_1;
abbrev2mag.KEV = mag_kev_1;
abbrev2mag.KIL = mag_kil_2;
abbrev2mag.KIR = mag_kir_1;
abbrev2mag.LEK = mag_lek_1;
abbrev2mag.LOZ = mag_loz_1;
abbrev2mag.LYC = mag_lyc_1;
abbrev2mag.LYR = mag_lyr_1;
abbrev2mag.MAS = mag_mas_1;
abbrev2mag.MUO = mag_muo_1;
abbrev2mag.NAL = mag_nal_1;
abbrev2mag.NUR = mag_nur_2;
abbrev2mag.OUJ = mag_ouj_1;
abbrev2mag.PEL = mag_pel_1;
abbrev2mag.RVK = mag_rvk_1;
abbrev2mag.SOD = mag_sod_1;
abbrev2mag.SOR = mag_sor_1;
abbrev2mag.TRO = mag_tro_1;
abbrev2mag.UPS = mag_ups_1;

% Get station codes

stns=fieldnames(iaga);

r = repmat(mag_data, 1, length(stns));

validStations = ones(1, length(stns));

for s = 1:length(stns)
  stn = stns{s};
  if ~isfield(abbrev2mag, stn)
    warning(['Instrument object for station ' stn ' not known']);
    instrument = [];
  else
    instrument = getfield(abbrev2mag, stn);
  end
  
  data = getfield(iaga, stn);
  
  % Build data matrix
  ncomps = length(data.components);
  ndata = (data.endtime-data.starttime)/data.resolution;
  md_data = repmat(nan,ncomps,ndata);
  for k=1:ncomps
    md_data(k,:) = getfield(data,data.components{k});
  end
  
  % Units
  units = {};
  for k = 1:ncomps
    switch data.components{k}
     case {'x' 'y' 'z' 'h' 'f' 'e' 'v'}
      units{k} = 'T';
	% convert nT to T
	md_data(k,:) = md_data(k,:)*1e-9;
     case {'d' 'i'}
      warning('Check units for angular comps. (deg. or min.?)');
      units{k} = 'deg';
     otherwise
      warning(['Component ''' data.components{k} ''' unknown']);
      units{k} = 'unknown';
    end
  end
  
  % MIA convention is that units should be a single string if all
  % components have the same units
  
  if all(strcmp(units,units{1}))
    units = units{1};
  end
  
  % Construct mag_data object
  md = mag_data('instrument', instrument, ...
		'starttime',data.starttime, ...
		'endtime', data.endtime, ...
		'resolution', data.resolution, ...
		'integrationtime', data.resolution, ...
		'components',upper(data.components), ...
		'units',units, ...
		'data',md_data, ...
		'load',0);
  r(s) = md;
end

