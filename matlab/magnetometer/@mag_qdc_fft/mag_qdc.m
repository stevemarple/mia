function r = mag_qdc(mia)
%MAG_QDC  Convert MAG_QDC_FFT to MAG_QDC.
%
%   r = MAG_QDC(mia)
%   r: MAG_QDC
%   mia: MAG_QDC_FFT
%
% See also MAG_QDC.


num = numel(mia);
if num ~= 1
  [tmp mfname] = fileparts(mfilename);
  r = repmat(mag_qdc, size(mia));
  for n = 1:num
    r(n) = feval(mfname, mia(n));
  end
  return
end

% mia is always scalar from this point on

comps = getcomponents(mia);
ncomps = length(comps);
fitorder = size(mia.coefficients, 2) - 1;

% convert stored coefficients to the full array
ft = zeros(ncomps, timespan(1, 'd') ./ mia.originalresolution);
ft(:, 1:(fitorder + 1)) = mia.coefficients;
ft(:, (end-fitorder+1):end) = conj(mia.coefficients(:, end:-1:2));
data = real(ifft(ft, [], 2));

% add monthly mean
for cn = 1:ncomps
  data(cn, :) = data(cn, :);
end

st = getstarttime(mia);
et = getendtime(mia);
stq = getdate(st);
etq = stq + timespan(1, 'd');

r = mag_qdc('starttime', stq, ...
	    'endtime', etq, ...
	    'sampletime', getsampletime(mia), ...
	    'integrationtime', getintegrationtime(mia), ...
	    'instrument', getinstrument(mia), ...
	    'data', data, ...
	    'units', getunits(mia), ...
	    'creator', getcreator(mia), ...
	    'processing', getprocessing(mia), ...
	    'components', comps, ...
	    'qdcstarttime', mia.qdcstarttime, ...
	    'qdcendtime', mia.qdcendtime);
r = addprocessing(r, 'created from mag_qdc_fft class');

% now convert to the desired resolution and start/end times
r = setresolution(r, getresolution(mia));

r = extract(r, ...
	    'starttime', getstarttime(mia), ...
	    'endtime', getendtime(mia));


