function [r,sr,s] = extract(mia, varargin)
%EXTRACT Extract a subset of data as another MAG_QDC_FFT object
%
%   See mia_base/EXTRACT.

if length(mia) ~= 1
  r = mia;
  sr = zeros(size(mia));
  s = zeros(size(mia));
  % make recursive call, regardless of current filename
  for n = 1:numel(mia)
    [r(n) sr(n) s(n)] = feval(basename(mfilename), mia(n), varargin{:});
  end
  return
end

[r,sr,s] = mag_data_extract(mia, varargin{:});

% remember to mirror extract operation on the orignal QDC (if it was
% kept)
if ~isempty(r.originalqdc)
  r.originalqdc = extract(r.originalqdc, varargin{:});
end
