function r = gettype(mia, varargin)
%GETTYPE  Return magnetometer data type.
%
%   r = GETTYPE(mia)
%   r: CHAR
%   mia: MAG_QDC_FFT object
%
%   See also MAG_QDC_FFT.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'magnetometer QDC';
  
 case 'u'
  r = 'MAGNETOMETER QDC';
  
 case {'c' 'C'}
  r = 'Magnetometer QDC';
 
 otherwise
  error('unknown mode');
end

return

