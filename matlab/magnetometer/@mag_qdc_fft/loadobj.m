function r = loadobj(mia)
%LOADOBJ  Convert a MAG_QDC_FFT object from its minimal form.
%
%   r = SAVING(mia)
%   r: MAG_QDC_FFT
%   mia: MAG_QDC_FFT
%
% Convert a MAG_QDC_FFT object from its minimal form to one where the
% data array contains meaningful data. This allows MAG_QDC_FFT objects to
% be treated as normal MAG_QDC objects, even though they are stored on
% disk with just their FFT coefficients.
%
% SAVEOBJ and LOADOBJ are only called automatically for matlab versions
% >5.1.
%
% See also SAVEOBJ.

r = setdata(mia, getdata(mag_qdc(mia)));
