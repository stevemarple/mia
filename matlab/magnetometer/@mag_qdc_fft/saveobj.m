function r = saveobj(mia)
%SAVEOBJ  Convert a MAG_QDC_FFT object to its minimal form for saving
%
%   r = SAVING(mia)
%   r: MAG_QDC_FFT
%   mia: MAG_QDC_FFT
%
% Convert a MAG_QDC_FFT object to its minimal form for saving to
% disk. This means the data array is set to an empty matrix to save
% diskspace. When loading the data array is recreated by LOADOBJ. 
%
% LOADOBJ and SAVEOBJ are only called automatically for matlab versions
% >5.1.
%
% See also LOADOBJ.

r = setdata(mia, []);
