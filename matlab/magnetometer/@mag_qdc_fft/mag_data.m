function r = mag_data(mia)
%MAG_DATA  Convert MAG_QDC_FFT to parent object.
%
%   r = MAG_DATA(mia)
%   r: MAG_DATA
%   mia: MAG_QDC_FFT
%
% See also MAG_QDC_FFT.

num = numel(mia); 
if num == 1
  r = mia.mag_data;
else
  r = repmat(mag_data, size(mia));
  for n = 1:num
    tmp = mia(n);
    r(n) = tmp.mag_data;
  end
end


