function r = getoriginalqdc(mia)
%GETORIGINALQDC  Return the QDC opbject used to make the FFT version
%
%   r = GETORIGINALQDC(mia);
%   r: original QDCs (of type derived from MAG_QDC)
%   mia: MAG_QDC_FFT
%
% GETORIGINALQDC returns the original QDC object, assuming it was
% preserved. To convert a MAG_QDC_FFT object to a plain MAG_QDC object use
% the convert function MAG_QDC.
%
% See also mag_qdc_fft/MAG_QDC.

if length(mia) ~= 1
  tmp = mia(1);
  r = repmat(tmp.originalqdc, size(mia));
  for n = 1:numel(mia)
    tmp = mia(n);
    r(n) = tmp.originalqdc;
  end
  return
end

r = mia.originalqdc;
