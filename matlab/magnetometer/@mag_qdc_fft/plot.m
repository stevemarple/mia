function [fh, gh, ph2, prelimH, mult] = plot(mia, varargin)
%PLOT  Plot data from a MAG_QDC_FFT object.
%
%   PLOT(mia)
%   PLOT(mia, ...)

fh = [];
gh = [];
ph2 = [];
prelimH = [];
mult = [];

instrument = getinstrument(mia);
comps = getparameters(mia);

defaults.components = comps;
defaults.plotaxes = [];
defaults.title = [];
defaults.windowtitle = [];
defaults.originalqdccolor = 'r';
defaults.visible = 'on';
[defaults unvi] = interceptprop(varargin, defaults);

% calculate title now that components are known. Base the title on the FFT
% QDCs.
[title windowtitle] = maketitle(mia, 'components', defaults.components);
if isempty(defaults.title)
  defaults.title = title;
end
if isempty(defaults.windowtitle)
  defaults.windowtitle = windowtitle;
end

if isempty(mia.originalqdc)
  % plot the parent QDC object as it is normally plotted.
  [fh gh ph] = plot(getparent(mia), varargin{unvi}, ...
                    'components', defaults.components, ...
                    'plotaxes', defaults.plotaxes, ...
                    'title', defaults.title, ...
                    'windowtitle', defaults.windowtitle, ...
                    'visible', defaults.visible);
  return
end


if ~isempty(defaults.plotaxes)
  np = get(defaults.plotaxes, 'NextPlot');
  set(defaults.plotaxes, 'NextPlot', 'add');
end

% plot the original QDC object as it is normally plotted. Give the titles
% as calculated for the FFT QDCs
[fh gh ph2] = plot(mia.originalqdc, varargin{:}, ...
                   'components', defaults.components, ...
                   'plotaxes', defaults.plotaxes, ...
                   'title', defaults.title, ...
                   'windowtitle', defaults.windowtitle, ...
                   'visible', 'off', ...
                   'color', defaults.originalqdccolor);

if isempty(defaults.plotaxes)
  np = get(gh(:), 'NextPlot');
  set(gh, 'NextPlot', 'add');
end

% plot the parent QDC object as it is normally plotted
[tmp1 tmp2 ph1] = plot(getparent(mia), varargin{unvi}, ...
                       'components', defaults.components, ...
                       'plotaxes', gh, ...
                       'title', defaults.title, ...
                       'windowtitle', defaults.windowtitle, ...
                       'visible', 'off');

if ~iscell(np)
  np = {np};
end
set(gh(:), {'NextPlot'}, np);

if isempty(defaults.plotaxes)
  set(fh, 'Visible', defaults.visible);
end

ph = [ph1 ph2];
