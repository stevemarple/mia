function mia = mag_qdc_fft(varargin)
% MAG_QDC  Constructor for MAG_QDC class
%
%
% As this class is implemented as a series of FFT coefficients it cannot
% easily have the actual resolution or start/end times changed. Instead
% alter the desired values but do nothing else. When the modified object is
% converted to sampled data (say MAG_QDC) then the desired values will be
% used. The actual start/end times are not stored since the fourier
% components always represent 24 hours.


cls = 'mag_qdc_fft'; % our class name
parent = 'mag_qdc'; % our parent class

latestversion = 1;
mia.versionnumber = latestversion;
% start/end times of the period used to mag the QDC
mia.qdcstarttime = [];
mia.qdcendtime = [];
mia.coefficients = []; % fourier components
mia.originalresolution = []; % resolution it was built with

% the QDC object from which the FFT version was built
mia.originalqdc = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & isa(varargin{1},cls)
  % copy constructor   
  mia = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
    
  p = feval(parent, varargin{unvi}, 'load', 0);
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
