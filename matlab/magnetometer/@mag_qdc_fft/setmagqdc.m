function r = setmagqdc(mia, a)
%SETMAGQDC  Set the MAG_QDC part of a MAG_QDC_FFT object.
%
%  This function is not intended for general use, only for the
%  construction of MAG_QDC_FFT objects.

% require a mag_qdc object, and not something else or something derived
% from it (otherwise the inheritance goes wrong)
if ~strcmp(class(a), 'mag_qdc')
  error('class is not mag_qdc');
elseif ~isequal(size(mia), size(a))
  error('both parameters must be the same size');
end

r = mia;

for n = 1:numel(mia)
  tmp = mia(n); % can't subscript and use . operator with matlab v5.1
  tmp.mag_qdc = a(n);
  r(n) = tmp;
end

