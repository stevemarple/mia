function r = char(mia, varargin)
%CHAR  Convert a MAG_QDC_FFT object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character


if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia);
  return;
end

defaults.timestampformat = '%H:%M:%S %d %B %Y (day %j)';

[defaults unvi] = interceptprop(varargin, defaults);



r = sprintf(['%s' ...
	     'original res.     : %s\n' ...
	     'FFT coefficients  : %s\n'], ...
	    mag_qdc_char(mia), char(mia.originalresolution), ...
	    matrixinfo(mia.coefficients));

