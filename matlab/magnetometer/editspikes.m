function varargout = editspikes(action, varargin)
%EDITSPIKES  Edit spikes from MAGNETOMETER data
%
% [fh gh] = EDITSPIKES(mia, ...)
% [fh gh] = EDITSPIKES('init', mia, ...)
%
% fh: FIGURE handle
% gh: AXES handles
% fil: MIA_DESPIKE_BASE object
% mia: MAG_DATA object
%
% Initialise the spike editor. Editing is performed by confirming which
% suspected spikes really are spikes. The FIGURE shows a number of AXES. The
% AXES on the left are the magnetometer data as a STACKPLOT. If necessary
% the stackplots may be zoomed (using TAZOOM) by dragging a "rubberband box"
% with mouse button #1. The righthand column of AXES shows the suspected
% spikes and a few adjacent points, so that zooming will not normally be
% necessary. As the mouse is moved the spikes under consideration
% (highlighted) will change. Editing is achieved with the following keys.
%
%   'c'
%   Clear the spike setting for this axis.
%
%   'C'
%   Clear the spike setting for all axes at this time position.
% 
%   'l', 'L', CTRL-L
%   REFRESH the figure.
%
%   'n'
%   Move to the next suspected spike. An alternative to moving the
%   mouse.
%
%   'p'
%   Move to the previous suspected spike. An alternative to moving the
%   mouse. 
%
%   's'
%   Confirm spike for the current axis only.
%
%   'S'
%   Confirm all spikes for this time.
%
%   't'
%   Toggle spike setting for this axis only.
%
%   'T'
%   Toggle spike setting for all axes for this time.
%
%   'q'
%   Clear all markers so that spikes may be seen more easily. Restore
%   markers by pressing any key (including 'q' for a second time) or by
%   moving the mouse.
%
%   SPACE or RETURN
%   Confirm all spikes for this time and move onto the next spike.
%
% The default behaviour can be modified with the following parameter
% name/value pairs:
%
%   'zoomaxes', LOGICAL (default is 1)
%   Indicate if an additional set of AXES are to be displayed showing a
%   zoomed portion of the data, so that spikes can be seen more easily.
%
%   'pointer', CHAR (default is 'arrow')
%   The figure pointer to use after the window has been constructed.
%
%   'visible', ['on' | 'off]
%   The visibility state of the FIGURE after the window has been constructed.
%
%   'windowstyle', ['normal' | 'modal']
%   When set to modal a modal window is created. Two values are
%   returned. The first is the modified data (if OK pressed) or
%   original data (is cancel pressed). The second is a flag indicating
%   that the data was modified. In normal mode the first return value is
%   the FIGURE handle, the second is a vector of AXES handles.
%
%   'euclideandistance', LOGICAL
%   Method used to determine the spike closest to the pointer. If set it
%   is determined by Euclidean distance. If not set then the spike closet
%   in time is selected. 
%
%   'updatefcn', CELL
%   List of parameters to be passed to FEVAL when OK has been clicked in the
%   normal (ie not modal) window style. The data and modified flags are both
%   appended to the end of the list.
%
%   'enablespikeediting', LOGICAL (default is 1)
%   Enable spike editing. The data from some magnetometers can be so
%   noisy and spiky it is not worthwhile to edit the spikes, but a common
%   interface for viewing the data and launching edit is still needed.
%
% There are also numerous options (too many to list) to set the LINE
% parameters of the detected spikes, validated spikes, selected spike and
% spike closest to the pointer. Read the source code for more details.
%

if isa(action, 'mia_base')
  varargin = {action varargin{:}};
  action = 'init';
  cbf = [];
elseif ishandle(action)
  % mfilename(cbf, action, ...)
  % mapped to mfilename(action, ...) for when not operating as a callback
  % and gcbf cannot be used
  cbf = action;
  action = varargin{1};
  varargin(1) = [];
else
  cbf = gcbf;
end

% cbf = gcbf;
[tmp mfname] = fileparts(mfilename);

switch action
  % ----------------------------------------------------------
 case 'init'
  % mfilename('init', mia, ...)
  mia = varargin{1};
  
  % whether or not the zoomed windows should be shown
  defaults.zoomaxes = 1;
  
  defaults.pointer = 'arrow';
  defaults.visible = 'on';
  % windowstyle determines whether or not it should run as a modal dialog
  % box or not. In modal mode the modified (or original) data is returned
  defaults.windowstyle = 'normal'; 
  defaults.closerequestfcn = [mfilename '(''close'');'];
  % how to determine the closest spike to the pointer. Either use
  % Euclidean distance, else use closest in time.
  defaults.euclideandistance = 0;
  
  defaults.updatefcn = {};

  defaults.okstring = 'OK';
  defaults.cancelstring = 'Cancel';

  % name of the file where detection statistics will be written. Empty
  % means no logging
  defaults.logfile = ''; 

  % keymap is a struct where the field names refer to key presses (or the
  % symbolic names for SPACE, RETURN and CTRL_L). The value is the
  % action to take
  defaults.keymap.RETURN = 'accept_and_next';
  defaults.keymap.SPACE = 'next';
  defaults.keymap.l = 'refresh'; 
  defaults.keymap.L = 'refresh';
  defaults.keymap.CTRL_L = 'refresh';
  defaults.keymap.c = 'clear';
  defaults.keymap.C = 'clear_all';
  defaults.keymap.m = 'confirm_all';
  defaults.keymap.M = 'confirm_all';
  defaults.keymap.n = 'next';
  defaults.keymap.p = 'previous';
  defaults.keymap.q = 'hide_spikes';
  defaults.keymap.Q = 'hide_spikes';
  defaults.keymap.s = 'confirm';
  defaults.keymap.S = 'confirm_all';
  defaults.keymap.t = 'toggle';
  defaults.keymap.T = 'toggle_all';
  
  % numerous line properties
  defaults.spikecolor = 'r';
  defaults.spikemarker = 'o';
  defaults.spikemarkersize = get(0, 'DefaultLineMarkerSize');

  defaults.currentspikecolor = 'r';
  defaults.currentspikemarker = 'o';
  defaults.currentspikemarkersize = get(0, 'DefaultLineMarkerSize') + 8;
  defaults.currentspikemarkerfacecolor = 'none';
  defaults.currentspikemarkeredgecolor = 'auto';

  defaults.pointerspikecolor = [0 .6 0];
  defaults.pointerspikemarker = 'o';
  defaults.pointerspikemarkersize = get(0, 'DefaultLineMarkerSize') + 10;
  defaults.pointerspikemarkerfacecolor = 'none';
  defaults.pointerspikemarkeredgecolor = 'auto';

  defaults.validatedspikecolor = [];
  defaults.validatedspikemarker = [];
  defaults.validatedspikemarkersize = [];
  defaults.validatedspikemarkerfacecolor = 'same';
  defaults.validatedspikemarkeredgecolor = 'auto';

  defaults.enablespikeediting = 1;
  defaults.maximumspikelength = 5;
  
  varargin2 = varargin(2:end);
  [defaults unvi] = interceptprop(varargin2, defaults);
  % warnignoredparameters(varargin2{unvi(1:2:end)});
  
  if isempty(defaults.currentspikemarkersize)
    defaults.currentspikemarkersize = defaults.spikemarkersize + 6;
  end

  if isempty(defaults.pointerspikecolor)
    defaults.pointerspikecolor = defaults.currentspikecolor;
  end
  
  fn = fieldnames(defaults);
  for n = 1:length(fn)
    fn2 = strrep(fn{n}, 'validated', '');
    if ~isempty(strmatch('validated', fn{n})) & ...
	  isempty(getfield(defaults, fn{n})) & ...
	  ~isempty(getfield(defaults, fn2))
      defaults = setfield(defaults, fn{n}, getfield(defaults, fn2));
    end
  end

  if strcmp(defaults.validatedspikemarkerfacecolor, 'same')
    defaults.validatedspikemarkerfacecolor = ...
	defaults.validatedspikecolor;
  end

  datasize = getdatasize(mia);
  % spacing = [nan 0.5 0.01 0 nan nan];
  % spacing = [0.1 0.5 0.01 0 nan nan];
  spacing = [0.1 0.1 0.01 0 nan nan];
  
  if matlabversioncmp('>', '5.1')
    dblbuf = {'doublebuffer', 'on'};
  else
    dblbuf = {}; % doublebuffer option does not exist
  end
  
  % window should be initially created with CloseRequestFcn set empty so
  % that it cannot be closed. Only after setting up the window is
  % CloseRequestFcn set to a suitable string
  [fh gh lh prelimh mult] = ...
      plot(mia, ...
	   'integerhandle', 'off', ...
	   varargin2{unvi}, ...
	   'visible', 'off', ...
	   'footer', 1, ...
	   'spacing', spacing, ...
	   'interruptible', 'off', ...
	   'pointer', 'watch', ...
	   'busyaction', 'cancel', ...
	   'windowstyle', defaults.windowstyle, ...
	   'CloseRequestFcn', '', ...
	   dblbuf{:});
  
  set(gh, 'DrawMode', 'fast');
  set(lh, ...
      'Interruptible', 'off', ...
      'BusyAction', 'cancel', ...
      'ButtonDownFcn', [mfilename '(''addspike'');']);
  
  gh = gh(:);
  
  ud = localGetUserData(fh);
  ud.mia = mia;
  ud.originalmia = mia;
  
  ud.defaults = defaults;
  ud.datasize = datasize;
  ud.zoomh = [];
  
  
  % add buttons to the footer area
  footH = findall(fh, 'Tag', 'footer');
  set(footH, 'Units', 'pixels');
  p = get(footH);
  
  
  despike = mia_despike_filters;
  ud.filtercount = 0;
  ud.filters = cell(size(despike));
  ud.filternumber = 1;
  ud.lastfilternumber = ud.filternumber;
  despikeNames = cell(size(despike));
  for n = 1:length(despike)
    ud.filters{n} = feval(despike{n});
    despikeNames{n} = getname(ud.filters{n});
  end

  selFilH = uicontrol('Parent', fh, ...
		      'Position', [p.Position(1:2), 1 1], ...
		      'Style', 'popupmenu', ...
		      'Callback', sprintf('%s(''selectfilter'');', mfname), ...
		      'String', despikeNames, ...
		      'BusyAction', 'cancel', ...
		      'Interruptible', 'off', ...
		      'Tag', 'selectfilter');
  
  selFilPos = uiresize(selFilH, 'l', 'b');
  
  configPos = selFilPos + [selFilPos(3)+10 0 0 0];
  configH = uicontrol('Parent', fh, ...
		      'Position', configPos, ...
		      'Style', 'pushbutton', ...
		      'Callback', sprintf('%s(''configure'');', mfname), ...
		      'String', 'Configure', ...
		      'Tag', 'configure');
  
  configPos = uiresize(configH, 'l', 'b');
  
  filterPos = configPos + [configPos(3)+10 0 0 0];
  filterH = uicontrol('Parent', fh, ...
		      'Position', filterPos, ...
		      'Style', 'pushbutton', ...
		      'Callback', sprintf('%s(''filter'');', mfname), ...
		      'String', 'Filter', ...
		      'Tag', 'filter');
  filterPos = uiresize(filterH, 'l', 'b');
  
  editPos = filterPos + [filterPos(3)+10 0 0 0];
  editH = uicontrol('Parent', fh, ...
		    'Position', editPos, ...
		    'Style', 'pushbutton', ...
		    'Callback', sprintf('%s(''edit'');', mfname), ...
		    'String', 'Edit', ...
		    'Tag', 'edit');
  editPos = uiresize(editH, 'l', 'b');

  okPos = editPos + [editPos(3)+10 0 0 0];
  okH = uicontrol('Parent', fh, ...
		  'Position', okPos, ...
		  'Style', 'pushbutton', ...
		  'Callback', sprintf('%s(''ok'');', mfname), ...
		  'String', defaults.okstring, ...
		  'Tag', 'ok');
  okPos = uiresize(okH, 'l', 'b');

  cancelPos = okPos + [okPos(3)+10 0 0 0];
  cancelH = uicontrol('Parent', fh, ...
		      'Position', cancelPos, ...
		      'Style', 'pushbutton', ...
		      'Callback', sprintf('%s(''cancel'');', mfname), ...
		      'String', defaults.cancelstring, ...
		      'Tag', 'cancel');
  cancelPos = uiresize(cancelH, 'l', 'b');

  
  statusPos = cancelPos + [cancelPos(3)+10 0 0 0];
  ud.statush = uicontrol('Parent', fh, ...
			 'Position', statusPos, ...
			 'BackgroundColor', get(fh, 'Color'), ...
			 'Style', 'text', ...
			 'Callback', sprintf('%s(''status'');', mfname), ...
			 'String', '', ...
			 'Tag', 'status');
  statusPos = uiresize(ud.statush, 'l', 'b');

  % ----

  % add a refresh menu option
  optionsH = findall(fh, 'Type', 'uimenu', 'Tag', 'options');
  if ~isempty(optionsH)
    set(optionsH, 'Visible', 'on');
    uimenu('Parent', optionsH, ...
	   'Callback', 'refresh(gcbf);', ...
	   'Label', 'Refresh');
  end
  
  if logical(defaults.zoomaxes)
    ud.zoomh = zeros(size(gh));
    ud.zoomlineh = zeros(size(gh));
    if matlabversioncmp('>', '5.1')
      hitTest = {'HitTest', 'off'};
    else
      hitTest = {};
    end
    for n = 1:length(gh)
      p = get(gh(n));
      pos = p.Position;
      pos(3) = 0.7 * pos(3);
      set(gh(n), 'Position', pos);
      pos(1) = p.Position(1) + 0.8 * p.Position(3);
      pos(3) = p.Position(3) * 0.2;
      ud.zoomh(n) = ...
	  axes('Parent', p.Parent, ...
	       'Box', 'on', ...
	       'Units', p.Units', ...
	       'Position', pos, ...
	       'XTick', [], ...
	       'YTick', [], ...
	       'XLimMode', 'manual', ...
	       'YLimMode', 'auto', ...
	       'Tag', mfilename, ...
	       hitTest{:});
      ud.zoomlineh(n) = line('Parent', ud.zoomh(n), ...
			     'Color', get(lh(n), 'Color'), ...
			     'XData', [], ...
			     'YData', []);
    end
  else
    ud.zoomh = [];
  end

  set(get(gh(end), 'XLabel'), 'Visible', 'off'); % make space for buttons
  ud.fh = fh; 
  ud.gh = gh; % axis handles
  ud.lh = lh; % line handles
  
  ud.multiplier = mult;
  ud.sph = zeros([ud.datasize(1) 1]); % spike handles (lines of spike markers)
  ud.vsph = zeros(size(ud.sph)); % validated spike handles
  
  ud.currentspikesh = zeros(size(ud.sph));
  ud.pointerspikesh = zeros(size(ud.sph));
  for n = 1:ud.datasize(1)
    % have a callback so that clicking the mouse will move the current
    % spike to the spike indicated by the pointer spike marker (but in
    % this case the callback is initiated by a click on the valid spikes)
    ud.vsph(n) = ...
	line('Parent', gh(n), ...
	     'ButtonDownFcn', [mfilename '(''key'',''select_point'');'], ...
	     'Visible', 'on', ...
	     'XData', [], ...
	     'YData', [], ...
	     'Color', ud.defaults.validatedspikecolor, ...
	     'LineStyle', 'none', ...
	     'Marker', ud.defaults.validatedspikemarker, ...
	     'MarkerEdgeColor', ud.defaults.validatedspikemarkeredgecolor, ...
	     'MarkerFaceColor', ud.defaults.validatedspikemarkerfacecolor, ...
	     'MarkerSize', ud.defaults.validatedspikemarkersize, ...
	     'Tag', 'validatedspikes');
    
    % no need for a callback to move the current spike here!
    ud.currentspikesh(n) = ...
	line('Parent', gh(n), ...
	     'Visible', 'off', ...
	     'XData', [], ...
	     'YData', [], ...
	     'Color', ud.defaults.currentspikecolor, ...
	     'LineStyle', 'none', ...
	     'Marker', ud.defaults.currentspikemarker, ...
	     'MarkerEdgeColor', ud.defaults.currentspikemarkeredgecolor, ...
	     'MarkerFaceColor', ud.defaults.currentspikemarkerfacecolor, ...
	     'MarkerSize', ud.defaults.currentspikemarkersize, ...
	     'Tag', 'currentspike');

    % have a callback so that clicking the mouse will move the current
    % spike to the spike indicated by the pointer spike marker
    ud.pointerspikesh(n) = ...
	line('Parent', gh(n), ...
	     'Visible', 'off', ...
	     'ButtonDownFcn', [mfilename '(''key'',''select_point'');'], ...
	     'XData', [], ...
	     'YData', [], ...
	     'Color', ud.defaults.pointerspikecolor, ...
	     'LineStyle', 'none', ...
	     'Marker', ud.defaults.pointerspikemarker, ...
	     'MarkerEdgeColor', ud.defaults.pointerspikemarkeredgecolor, ...
	     'MarkerFaceColor', ud.defaults.pointerspikemarkerfacecolor, ...
	     'MarkerSize', ud.defaults.pointerspikemarkersize, ...
	     'EraseMode', 'xor', ...
	     'Tag', 'pointerspike');
    
  end

  % ud.spikes = findspikes(ud.filters{ud.filternumber}, mia);
  % ud.validatedspikes = logical(zeros(size(ud.spikes)));
  ud.spikes = zeros(getdatasize(mia));
  ud.validatedspikes = zeros(getdatasize(mia));
  
  
  ud.currentsamplenumber = [];
  ud.pointersamplenumber = [];
  ud.currentaxisnumber = 1;
  
  if 0 & matlabversioncmp('>', '5.1')
    for ghh = gh(:)'
      axis(ghh, 'tight');
    end
  end
  
  data = getdata(mia);
  
  for n = 1:length(ud.sph)
    tmp = find(ud.spikes(n, :));
    % have a callback so that clicking the mouse will move the current
    % spike to the spike indicated by the pointer spike marker (but in
    % this case the callback is initiated by a click on a spike marker)
    ud.sph(n) = line('Parent', gh(n), ...
		     'ButtonDownFcn', ...
		      [mfilename '(''key'',''select_point'');'], ...
		     'Color', defaults.spikecolor, ...
		     'Marker', defaults.spikemarker, ...
		     'MarkerSize', defaults.spikemarkersize, ...
		     'LineStyle', 'none', ...
		     'XData', [], ...
		     'YData', [], ...
		     'Tag', 'spikes');
  end

  % set(fh, 'WindowButtonMotionFcn', sprintf('%s(''move'');', mfname));
  % set(fh, 'KeyPressFcn', sprintf('%s(''key'');', mfname));

%   set(fh, 'Visible', defaults.visible);
%   ud = localRecomputeSpikes(cbf, ud);
%   localUpdateCurrentSpike(ud);
%   localUpdateZoomedAxes(ud);
%   localSetUserData(fh, ud);
  
  if logical(defaults.enablespikeediting)
    ud = localRecomputeSpikes(cbf, ud);
    localUpdateCurrentSpike(ud);
    localUpdateZoomedAxes(ud);
    localSetUserData(fh, ud); % update BEFORE callbacks are enabled
    set(fh, ...
	'Pointer', defaults.pointer, ...
	'CloseRequestFcn', defaults.closerequestfcn, ...
	'WindowButtonMotionFcn', sprintf('%s(''move'');', mfname), ...
	'KeyPressFcn', sprintf('%s(''key'');', mfname), ...
	'Visible', defaults.visible);
  else
    localSetUserData(fh, ud); % update BEFORE callbacks are enabled
    set(fh, ...
	'Pointer', defaults.pointer, ...
	'CloseRequestFcn', defaults.closerequestfcn, ...
	'WindowButtonMotionFcn', '', ...
	'KeyPressFcn', '', ...
	'Visible', defaults.visible);
    set([selFilH; configH; filterH], 'Enable', 'off');
  end

  switch lower(defaults.windowstyle)
   case 'normal'
    % return figure and axes handles, run in 'background mode'
    varargout{1} = fh;
    varargout{2} = gh;
    varargout{3} = ud.spikes;
    
   case 'modal'
    % waitfor(gh(1));
    waitfor(fh, 'WindowButtonMotionFcn', '');
    ud = localGetUserData(fh);
    varargout{1} = ud.returnvalue;
    % if the data contains NaNs then isequal will return 0, even if the
    % two objects are identical. Cannot use nonanisequal since that only
    % works on numeric matrices. Use a combined method

    % varargout{2} = ~isequal(ud.originalmia, ud.returnvalue);
    varargout{2} = ~(isequal(ud.originalmia, ud.returnvalue) ... 
		     | nonanisequal(getdata(ud.originalmia), ...
				    getdata(ud.returnvalue)));
 
    delete(fh);
    
   otherwise
    % how did it get this far without crashing? Maybe Mathworks added
    % another option?
    error('Unknown window mode');
  end
  
  % ----------------------------------------------------------
 case 'move'
  % track the pointer to the closest spike
  if isempty(cbf)
    disp('no callback figure');
    return;
  end
  
  % get all properties at the same time in case any change between
  % succesive reads
  fp = get(cbf);

  ud = localGetUserData(cbf);
  
  set([ud.sph; ud.vsph; ud.currentspikesh], 'Visible', 'on');
  
  % find axes under current point
  ca = localGetPointerAxes(ud);
  if isempty(ca)
    return;
  end
  
  cap = get(ca);
  can = find(ca == ud.gh); % current axes number (index into gh)
  if isempty(can)
    return
  end

  ud.currentaxisnumber = can;
  % ud = localUpdateCurrentSpike(ud);
  ud = localUpdatePointerSpike(ud);
  % localUpdateZoomedAxes(ud);
  localSetUserData(cbf, ud);


  % ----------------------------------------------------------
 case 'key'
  % mfilename('key')
  % mfilename('key', keypress)
  fp = get(cbf);
  ud = localGetUserData(cbf);

  if isempty(ud.currentsamplenumber)
    return % no spikes
  end

  if length(varargin)
    keypress = varargin{1};
  else
    keypress = fp.CurrentCharacter;
  end

  % matlab 5.1 is fussy, so map non-printing chars to a suitable
  % string. Also handle strings which are not valid fieldnames.
  if isequal(double(keypress), 13)
    keypress = 'RETURN';
  elseif isequal(double(keypress), 12)
    keypress = 'CTRL_L';
  elseif isequal(keypress, ' ')
    keypress = 'SPACE';
  end

  if isfield(ud.defaults.keymap, keypress)
    keyaction = getfield(ud.defaults.keymap, keypress);
  else
    keyaction = keypress; % maybe we were passed a key action instead
  end
  
  switch keyaction
   case 'select_point'
    % set the current spike to the spike indicated by the pointer
    if ~isempty(ud.pointersamplenumber)
      ud.currentsamplenumber = ud.pointersamplenumber;
      ud = localUpdateCurrentSpike(ud);
    end
    
   case 'accept_and_next'
    % accept all spikes at current location. Move to next spike.
    localSetUserData(cbf, ud);
    ud = localUpdateCurrentSpike(ud);
    feval(mfilename, action, 'confirm_all'); % confirm all spikes
    ud = localGetUserData(cbf);
    feval(mfilename, action, 'next'); % move to next spike
    ud = localGetUserData(cbf);
    
   case 'refresh'
    refresh(cbf);
    
   case 'clear'
    % clear spike setting for this axis
    ud.validatedspikes(ud.currentaxisnumber, ud.currentsamplenumber) = 0;
    
   case 'clear_all'
    % clear spike setting for all axes at this point in time
    ud.validatedspikes(:, ud.currentsamplenumber) = 0;

   case {'next' 'previous'}
    % move to next/previous spike
    idx = find(sum(ud.spikes, 1));
    if isempty(idx)
      return; % no spikes found
    end
    if any(strcmp(keyaction, 'next'))
      idx2 = find(idx > ud.currentsamplenumber);
      if isempty(idx2)
	idx3 = idx(1); % wrap back to first spike
	fprintf(2,'\a\n'); % Beep!
      else
	idx3 = idx(idx2(1));
      end
    else
      idx2 = find(idx < ud.currentsamplenumber);
      if isempty(idx2)
	idx3 = idx(end); % wrap back to last spike
	fprintf(2,'\a\n'); % Beep!
      else
	idx3 = idx(idx2(end));
      end
    end
    ud.currentsamplenumber = idx3;
    % ud = localUpdateCurrentSpike(ud, idx3);
    ud = localUpdateCurrentSpike(ud);
    localSetUserData(cbf, ud);
    
   case 'confirm'
    % accept spike setting for this axis
    ud.validatedspikes(ud.currentaxisnumber, ud.currentsamplenumber) = 1;
    
   case 'confirm_all'
    % accept all spikes at this point in time
    ud.validatedspikes(:, ud.currentsamplenumber) = ...
	ud.spikes(:, ud.currentsamplenumber);
    
   case 'toggle'
    % toggle spike setting for this axis
    ud.validatedspikes(ud.currentaxisnumber, ud.currentsamplenumber) = ...
	~ud.validatedspikes(ud.currentaxisnumber, ud.currentsamplenumber);
    
   case 'toggle_all'
    % toggle spike setting for all axes at this point in time
    ud.validatedspikes(:, ud.currentsamplenumber) = ...
	~ud.validatedspikes(:, ud.currentsamplenumber) & ...
	ud.spikes(:, ud.currentsamplenumber);
    
    % case 'hide_spikes'

    % 'hide_spikes' is handled separately since it must show spikes on any
    % other following press
    
  end
  
  localUpdateValidatedSpikes(ud);
    
  % 'hide_spikes' keyaction
  set([ud.sph; ud.vsph; ud.currentspikesh], ...
      'Visible', ...
      localBool2OnOff(~strcmp(keyaction, 'hide_spikes') | ...
		      strcmp(get(ud.vsph(1), 'Visible'), 'off')));

  localUpdateZoomedAxes(ud);
  localSetUserData(cbf, ud);
  
  % ----------------------------------------------------------
 case 'selectfilter'
  ud = localGetUserData(cbf);
  selFilH = findobj(cbf, 'Type', 'uicontrol', 'Tag', 'selectfilter');
  ud.filternumber = get(selFilH, 'Value');
  if isequal(ud.filternumber, ud.lastfilternumber)
    return; % no change, do nothing
  end

  pointer = get(cbf, 'Pointer');
  set(cbf, 'Pointer', 'watch');

  ud = localRecomputeSpikes(cbf, ud);
  localUpdateCurrentSpike(ud);
  localUpdateZoomedAxes(ud);

  localSetUserData(cbf, ud);
  set(cbf, 'Pointer', pointer);
  
  % ----------------------------------------------------------
 case 'configure'
  ud = localGetUserData(cbf);
  ud.filters{ud.filternumber} = configure(ud.filters{ud.filternumber});
  localSetUserData(cbf, ud);
  
  % ----------------------------------------------------------
 case 'filter'
  % mfilename('filter')
  ud = localGetUserData(cbf);

  if ~any(ud.validatedspikes(:))
    % disp('no validated spikes');
    msgbox('no validated spikes');
    return
  end
  
  pointer = get(cbf, 'Pointer');
  set(cbf, 'Pointer', 'watch');

  % call filter() passing in the spikemap
  selFilH = findobj(cbf, 'Type', 'uicontrol', 'Tag', 'selectfilter');
  ud.filternumber = get(selFilH, 'Value');

  % [fh2 gh2] = plot(ud.mia); set(gh2, 'NextPlot', 'add');
  vsn = sum(ud.validatedspikes(:) ~= 0);
  sn = sum(ud.spikes(:) ~= 0);
  disp(sprintf('filtering %d of %d detected spikes (%02.2f%%)', ...
	       vsn, sn, 100*vsn/sn));
  
  ud.mia = filter(ud.filters{ud.filternumber}, ud.mia, ...
		  'spikes', ud.validatedspikes);
  ud.filtercount = ud.filtercount + 1;
  if ~isempty(ud.defaults.logfile)
    % Log details about the accuracy of the spike detection. Make file
    % entirely numerical so that it can be read back into matlab. Use id
    % from database to record magnetometer.
    in = getinstrument(ud.mia);
    [fid mesg] = fopen(ud.defaults.logfile, 'at');
    if fid == -1
      disp(sprintf('cannot open logfile (%s): %s', ...
		   ud.defaults.logfile, mesg));
    else
      fprintf(fid, '%s %s %s_%d %d %s %d %d\n', ...
	      strftime(timestamp('now'), '%Y-%m-%d %H:%M:%S'), ...
	      strftime(getstarttime(ud.mia), '%Y-%m-%d'), ...
	      getabbreviation(in), getserialnumber(in), ...
	      ud.filtercount, class(ud.filters{ud.filternumber}), vsn, sn);
      fclose(fid);
    end
  end
  

  ud = localRecomputeSpikes(cbf, ud);
  % update the data plots
  localUpdateDataPlot(ud);
  localSetUserData(cbf, ud);
  set(cbf, 'Pointer', pointer);

  
  % ----------------------------------------------------------
 case 'edit'

  % ----------------------------------------------------------
 case 'ok'
  % mfilename('ok')
  % return the data as is
  ud = localGetUserData(cbf);
  
  if any(ud.validatedspikes(:)) & ...
	strcmp(questdlg({'Validated spikes exist.'; ...
		    'Do you really want to stop editing now?'}, ...
			'Quit without filtering?', 'Yes', 'No', 'No'), ...
	       'No')
    return
  end

  ud.returnvalue = ud.mia;
  localSetUserData(cbf, ud);

  if strcmp(ud.defaults.windowstyle, 'modal')
    set(cbf, ...
	'WindowButtonMotionFcn', '', ...
	'KeyPressFcn', '');
  else
    varargout{1} = ud.mia;
    delete(cbf);
  end
  
  if ~isempty(ud.defaults.updatefcn)
    % if the data contains NaNs then isequal will return 0, even if the
    % two objects are identical. Cannot use nonanisequal since that only
    % works on numeric matrices. Use a combined method
    iseq = isequal(ud.originalmia, ud.returnvalue);
    nniseq = nonanisequal(getdata(ud.originalmia), ...
			  getdata(ud.returnvalue));
    % modified = ~isequal(ud.mia, ud.originalmia)
    modified = ~(isequal(ud.originalmia, ud.returnvalue) ... 
		 | nonanisequal(getdata(ud.originalmia), ...
				getdata(ud.returnvalue)));

    feval(ud.defaults.updatefcn{:}, ud.mia, modified);
  end
  
  % ----------------------------------------------------------
 case 'cancel'
  % return original data
  ud = localGetUserData(cbf);
  ud.returnvalue = ud.mia;
  localSetUserData(cbf, ud);

  if strcmp(ud.defaults.windowstyle, 'modal')
    % delete(ud.gh);
    set(cbf, ...
	'WindowButtonMotionFcn', '', ...
	'KeyPressFcn', '');
  else
    varargout{1} = ud.originalmia;
    delete(cbf);
  end
  
  % ----------------------------------------------------------
 case 'close'
  % mfilename('close')
  
  q = questdlg('What do you wish to do?', ...
	       'Close action', ...
	       'Return modified data', ...
	       'Return original data', ...
	       'Continue editing', 'Continue editing');
  switch q
   case 'Return modified data'
    feval(mfilename, cbf, 'ok');
   case 'Return original data';
    feval(mfilename, cbf, 'cancel');
   case 'Continue editing'
    return
   otherwise
    error(sprintf('unknown answer (was ''%s'')', q));
  end
  
  % ----------------------------------------------------------
 case 'modalclose_NOT_USED'
  % mfilename('modalclose')
  
  ud = localGetUserData(cbf);
  q = questdlg('What do you wish to do?', ...
	       'Close action', ...
	       'Return modified data', ...
	       'Return original data', ...
	       'Continue editing', 'Continue editing');
  switch q
   case 'Return modified data'
    ud.returnvalue = ud.mia;
   case 'Return original data';
    ud.returnvalue = ud.originalmia;
   case 'Continue editing'
    return
   otherwise
    error(sprintf('unknown answer (was ''%s'')', q));
  end
  localSetUserData(cbf, ud);
  
  % delete the axes, then the waitfor command will continue and is
  % responsible for closing everything else
  delete(ud.gh);
 
  % ----------------------------------------------------------
 case 'addspike'
  % mfilename('addspike')
  cbo = gcbo;
  if ~strcmp(get(cbf, 'SelectionType'), 'extend')
    return
  end
  ud = localGetUserData(cbf);
  ah = get(cbf, 'CurrentAxes');
  if isempty(ah) | isempty(cbo)
    return
  end
  cp = get(ah, 'CurrentPoint');
  p = get(cbo);
  
  % for the line clicked on find the index number of the datapoint
  % closest to the point clicked
  if logical(ud.defaults.euclideandistance)
    % closest Euclidean distance
    [tmp idx] = min((p.XData - cp(2,1)).^2 + (p.YData - cp(2,2)).^2);
  else
    % closest in time
    [tmp idx] = min(abs(p.XData - cp(2,1)));
  end
  ud.spikes(:, idx) = 1;
  if isempty(ud.currentsamplenumber)
    ud.currentsamplenumber = idx;
    localUpdateCurrentSpike(ud);
  end
  localUpdateSpikes(ud);
  localUpdateZoomedAxes(ud);
  localUpdateStatusBar(ud);
  localSetUserData(cbf, ud);
  
  % ----------------------------------------------------------
 otherwise
  error(sprintf('unknown action (was ''%s'')', char(action)));
end



% -----
function r = localGetUserData(fh);
ud = get(fh, 'UserData');
if isfield(ud, 'editspikes')
  r = getfield(ud, 'editspikes');
else
  r = [];
end

% -----
function localSetUserData(fh, ud);
s = get(fh, 'UserData');
set(fh, 'UserData', setfield(s, 'editspikes', ud));

% -----
function r = localGetPointerAxesOLD
r = [];
rp = get(0);

if isempty(rp.PointerWindow) | rp.PointerWindow == 0
  return;
end

ah = findobj(rp.PointerWindow, 'Type', 'axes');
if isempty(ah)
  return
end

ahp = get(ah);

% if the HitTest property is available then use it
if isfield(ahp, 'HitTest')
  hp = findobj(rp.PointerWindow, 'Type', 'axes', 'HitTest', 'on');
  ahp = get(ah);
end

% for every axes see if the point lies within the range defined by the
% axes limits

for ah2 = ah(:)'
  p = get(ah2);
  if p.CurrentPoint(2,1) >= p.XLim(1) & ...
	p.CurrentPoint(2,1) <= p.XLim(2) & ...
	p.CurrentPoint(2,2) >= p.YLim(1) & ...
	p.CurrentPoint(2,2) <= p.YLim(2)
    r = ah2;
    break
  end
end

% -----
function r = localGetPointerAxes(ud)
r = [];
rp = get(0);

if isempty(rp.PointerWindow) | rp.PointerWindow == 0
  return;
end

% for every plot axes see if the point lies within the range defined by the
% axes limits

% for ah2 = ah(:)'
for ah2 = ud.gh(:)'
  p = get(ah2);
  if p.CurrentPoint(2,1) >= p.XLim(1) & ...
	p.CurrentPoint(2,1) <= p.XLim(2) & ...
	p.CurrentPoint(2,2) >= p.YLim(1) & ...
	p.CurrentPoint(2,2) <= p.YLim(2)
    r = ah2;
    break
  end
end

% -----
function r = localBool2OnOff(a)
r = cell(size(a));
for n = 1:numel(a)
  if a(n)
    r{n} = 'on';
  else
    r{n} = 'off';
  end
end
if length(a) == 1
  r = r{1};
end

% -----
function localUpdateZoomedAxes(ud)
if isempty(ud.currentsamplenumber)
  % current sample not set then blank the axes and return
  for n = 1:length(ud.zoomh)
    delete(get(ud.zoomh(n), 'Children'));
    set(ud.zoomh(n), 'YTick', []);
  end
  return;
end

i1 = max(1, ud.currentsamplenumber - 10);
i2 = min(ud.datasize(2), ud.currentsamplenumber + 10);
ylim = cell(length(ud.zoomh), 1);
p = get(ud.lh);
for n = 1:length(ud.zoomh)
  delete(allchild(ud.zoomh(n)));
  copyobj(allchild(ud.gh(n)), ud.zoomh(n));
  delete(findall(ud.zoomh(n), 'Tag', 'pointerspike'));
  % % hide axis labels etc
  % set(findall(ud.zoomh(n), 'Type', 'text'), 'Visible', 'off');
  mn = min(p(n).YData(i1:i2));
  mx = max(p(n).YData(i1:i2));
  margin = (mx - mn) * 0.1;
  ylim{n} = [mn-margin mx+margin];
  if any(isnan(ylim{n}))
    ylim{n} = [0 1];
  end
end
set(ud.zoomh, ...
    'ButtonDownFcn', '', ...
    'YTickLabelMode', 'auto', ...
    'YTickMode','auto', ...
    'XLim', [i1 i2], ...
    {'YLim'}, ylim, ...
    'XLimMode', 'manual', ...
    'YLimMode', 'manual', ...
    'XTick', [], ...
    'Tag', 'localUpdateZoomedAxes');

% hide axis labels etc
set(findall(ud.zoomh, 'Type', 'text'), 'Visible', 'off');

% -----
function r = localUpdateCurrentSpike(ud)
% r = LOCALUPDATECURRENTSPIKE(ud)
% r: modified user data
% ud: user data

if isempty(ud.currentsamplenumber)
  return
end

vis = localBool2OnOff(ud.spikes(:, ud.currentsamplenumber));
tmp = get(ud.lh);
xd = tmp(1).XData(ud.currentsamplenumber);
yd = cell(length(ud.currentspikesh), 1);

for n = 1:length(ud.currentspikesh)
  yd{n} = tmp(n).YData(ud.currentsamplenumber);
end
set(ud.currentspikesh, ...
    {'Visible'}, vis, ...
    'XData', xd, ...
    {'YData'}, yd);

if ~isempty(tazoom(ud.gh, 'status'))
  tazoom(ud.gh, 'reposition', ud.currentsamplenumber);
end
localUpdateStatusBar(ud);
r = ud;
% refresh(ud.fh);

% -----
function r = localUpdatePointerSpike(ud)
% r = LOCALUPDATEPOINTERSPIKE(ud)
% r: modified user data
% ud: user data
cap = get(ud.gh(ud.currentaxisnumber));
csp = get(ud.sph(ud.currentaxisnumber)); % current spike properties

% find closest spike to pointer location
if logical(ud.defaults.euclideandistance)
  % closest Euclidean distance
  [tmp idx] = min((csp.XData - cap.CurrentPoint(2,1)).^2 + ...
		  (csp.YData - cap.CurrentPoint(2,2)).^2);
else
  % closest in time
  [tmp idx] = min(abs(csp.XData - cap.CurrentPoint(2,1)));
end
spikeIdx = find(ud.spikes(ud.currentaxisnumber, :));
ud.pointersamplenumber = spikeIdx(idx);

tmp = get(ud.lh);
xd = tmp(1).XData(ud.pointersamplenumber);
yd = cell(length(ud.pointerspikesh), 1);

for n = 1:length(ud.pointerspikesh)
  yd{n} = tmp(n).YData(ud.pointersamplenumber);
end
set(ud.pointerspikesh, ...
    'Visible', 'on', ...
    'XData', xd, ...
    {'YData'}, yd);

r = ud;

% -----
function r = localRecomputeSpikes(cbf, ud)
% Low-level function. Modifies UserData but returns the modified value,
% does not update the changes in the figure window.
r = ud;
drawnow; % have popupmenu redrawn
pointer = get(cbf, 'Pointer');
set(cbf, 'Pointer', 'watch');
% compute new spikemap
ud.spikes = findspikes(ud.filters{ud.filternumber}, ud.mia);

% do any of the existing confirmed spikes match with this filter, if so
% keep them as confirmed
ud.validatedspikes = ud.validatedspikes & ud.spikes;

% is the current sample number (the place currently under investigation) a
% spike?
if isempty(ud.currentsamplenumber) | ~any(ud.spikes(:, ud.currentsamplenumber))
  % current sample not set, or current sample not a spike.
  % set current sample to be the first spike
  ud.currentsamplenumber = find(sum(ud.spikes, 1));
  if ~isempty(ud.currentsamplenumber)
    ud.currentsamplenumber = ud.currentsamplenumber(1);
    set(ud.currentspikesh, 'Visible', 'off');
  end
else
  % current spike is still a spike, leave unchanged
  ;
end

% is the pointer spike sample still a spike?
if ~any(ud.spikes(:, ud.pointersamplenumber))
  ud.pointersamplenumber = [];
  set(ud.pointerspikesh, 'Visible', 'off');
end

ud.lastfilternumber = ud.filternumber;
r = ud;

localUpdateSpikes(ud);
localUpdateValidatedSpikes(ud);
localUpdateZoomedAxes(ud);

plural = 's';
if sum(ud.spikes(:)) == 1
  plural = '';
end
in = getinstrument(ud.mia);
disp(sprintf('\r%s found %d independent spike%s for %s_%d', ...
	     class(ud.filters{ud.filternumber}), ...
	     length(find(sum(ud.spikes, 1))), plural, ...
	     getabbreviation(in), getserialnumber(in)));


set(cbf, 'Pointer', pointer);

% -----
function localUpdateSpikes(ud)
xd = cell(length(ud.sph), 1);
yd = cell(length(ud.sph), 1);
tmp = get(ud.lh);
for n = 1:length(ud.sph)
  xd{n} = tmp(n).XData(ud.spikes(n, :));
  yd{n} = tmp(n).YData(ud.spikes(n, :));
end
set(ud.sph, ...
    {'XData'}, xd, ...
    {'YData'}, yd);

% -----
function localUpdateValidatedSpikes(ud)
xd = cell(length(ud.vsph), 1);
yd = cell(length(ud.vsph), 1);
tmp = get(ud.lh);
for n = 1:length(ud.vsph)
  xd{n} = tmp(n).XData(ud.validatedspikes(n, :));
  yd{n} = tmp(n).YData(ud.validatedspikes(n, :));
end
set(ud.vsph, ...
    {'XData'}, xd, ...
    {'YData'}, yd);

% if there are no validated spikes make the filter button inoperative
set(findall(ud.fh, 'Type', 'uicontrol', 'Tag', 'filter'), ...
    'Enable', localBool2OnOff(any(ud.validatedspikes(:))));
localUpdateStatusBar(ud);

% -----
function localUpdateDataPlot(ud)
data = getdata(ud.mia);
yd = cell(length(ud.lh), 1);
for n = 1:length(ud.lh)
  yd{n} = data(n, :) ./ power(10, ud.multiplier);
end
set(ud.lh, {'YData'}, yd);


% -----
function localUpdateStatusBar(ud)
spikelocations = find(sum(ud.spikes, 1));
lastspikenum = length(spikelocations);
spikenum = find(spikelocations == ud.currentsamplenumber);
if isempty(spikenum)
  spikenum = 0;
end
tsn = ud.currentsamplenumber;
if ~isempty(tsn)
  tstr = strftime(getresolution(ud.mia) * (ud.currentsamplenumber-1), ...
		  '%H:%M:%S');
else
  tstr = '';
end

str = sprintf('Position: %d / %d   Validated: %d / %d  Time: %s', ...
	      spikenum, lastspikenum, ...
	      length(find(ud.validatedspikes(:))), ...
	      length(find(ud.spikes(:))), tstr);

if spikenum == 1
  col = [0 .6 0];
elseif spikenum == lastspikenum
  col = [.6 0 0];
else
  col = 'k';
end

set(ud.statush, 'String', str, 'ForegroundColor', col);
uiresize(ud.statush, 'l', 'b');
