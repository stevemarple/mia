function r = info_pmag_nur_4_data
%INFO_PMAG_NUR_4_DATA Return basic information about pmag_nur_4.
%
% This function is not intended to be called directly, use the
% INFO function to access data about pmag_nur_4. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://www.dcs.lancs.ac.uk/iono/cgi-bin/miainstrumentdatafunctions?instrument=pulsation_magnetometer;abbreviation=nur;serialnumber=4

r.abbreviation = 'nur';
r.bibliography = '';
r.comment = '';
r.components = {'''X' '''' 'Y' 'Z'};
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = 'Finnish pulsation magnetometer chain';
r.facility_url = 'http://spaceweb.oulu.fi/projects/pulsations/';
r.facilityid = 7;
r.groupids = [];
r.id = 56;
r.latitude = 60.5;
r.location1 = 'Nurmijarvi';
r.location1_ascii = 'Nurmijarvi';
r.location2 = 'Finland';
r.logo = '';
r.longitude = 24.7;
r.modified = timestamp([2006 04 20 10 10 31.579432]);
r.piid = [];
r.pulsation = 1;
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 4;
r.starttime = timestamp([1968 01 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename = [];
% end of function
