function r = imagemag2mia(mia, varargin)
r = [];

cls = class(mia);
if ~strcmp(cls, 'mag_data')
  error(sprintf('only know how to retrieve mag_data (class was ''%s'')', cls));
end

mag = getinstrument(mia);
switch numel(mag)
 case 0
  error('instrument must be defined');
  
 case 1
  ; % continue processing
  
 otherwise
  r = repmat(mia, size(mag));
  for n = 1:numel(mag)
    mia2 = setinstrument(mia, mag(n));
    r(n) = feval(mfilename, mia2, varargin{:});
  end
  return
end
 
% from this point on mag is always scalar

defaults.cancelhandle = [];
defaults.createargs = {};
defaults.extractargs = {};
defaults.asciiloadfunc = '';
defaults.asciiloadfuncextraargs = {};
defaults.emptydatafunc = '';
defaults.emptydatafuncargs = {};
defaults.failiffilemissing = [];
defaults.resolution = [];
defaults.resolutionmethod = '';
defaults.compression = 1;
defaults.usecache = true;

% accept but ignore
defaults.archive = '';

% allow the userdetails field to be overridden, so that the web data request
% system can pass on the details of the requesting user, not the user
% running MIA at that time.
defaults.userdetails = userdetails;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});


abbrev = getabbreviation(mag);
st = getstarttime(mia);
et = getendtime(mia);

% Maximum request in one go is 48 hours, times must start/end on hour
% boundary
blockduration = timespan(48, 'h');
hour = timespan(1, 'h');
st1 = floor(st, hour);
et1 = ceil(et, hour);

t = st1;

comment = ['MIA data request for ' dateprintf(mia)];

if logical(defaults.compression)
  compressionStr = 'gz';
else
  compressionStr = 'no';
end
reqnum = 1;
while t < et1
  t2 = min(et1, t + blockduration);
  durStr = sprintf('%02d', (t2-t)/hour);
  fileprefix = ['mia' abbrev durStr '_'];

  % Request the data. Don't bother parsing the results page because we
  % know what the file we need to download is going to be called.
  posturl = 'http://www.ava.fmi.fi/cgi-bin/imagecgi/image-data.cgi';

  % generate comment based on request number
  comment2 = sprintf('%s (#%d)', comment, reqnum);

  postparam = {'start', strftime(t, '%Y%m%d%H'), ...
               'eventlength', sprintf('%02d', (t2-t)/hour), ...
               'station', 'mylist', ...
               'mystation', upper(abbrev), ...
               'column', 'on', ...
               'sampling', 'orig_int', ...
               'compression', compressionStr, ...
               'filestr', fileprefix, ...
               'yourname', defaults.userdetails.realname, ...
               'institute', defaults.userdetails.institute, ...
               'email', defaults.userdetails.email, ...
               'message', comment2};

  if isunix
    postfile = '/dev/null';
  else
    postfile = tempname;
  end
  url_post(posturl, postfile, postparam, 'verbosity', 1);
  
  if ~isunix
    delete(postfile);
  end
  
  dataurl = strftime(t, ['http://space.fmi.fi/image/plots/' fileprefix ...
                      '%Y%m%d%H.col']);
  datafile = tempname2('', '.txt');
  if logical(defaults.compression)
    gzipfile = tempname2('', '.gz');
    url_fetch([dataurl '.gz'], gzipfile, ...
              'verbosity', 1, ...
              'usecache', defaults.usecache);
    
    [status mesg] = system(sprintf('gunzip -c %s > %s', ...
				   systemquote(gzipfile), ...
				   systemquote(datafile)));
    if status
      error(sprintf('could not gunzip temporary file: %s', mesg));
    end
    delete(gzipfile);
  else
    url_fetch(dataurl, datafile, ...
              'verbosity', 1, ...
              'usecache', defaults.usecache);
  end
  
  % load the data
  data = load(datafile);
  delete(datafile);

  dataheader = data(1, :); % save data header
  
  data(1, :) = []; % remove the header from the data
  sampletime = transpose(timestamp(num2cell(data(:, 1:6),2)));
  
  data2 = data(:, 7:end)';
  data2(data2 == 99999.9) = nan;
  data2 = data2 * 1e-9; % convert from nT to T

  % store the data into a MIA object
  miatmp = feval(cls, ...
                 'instrument', mag, ...
                 'starttime', t, ...
                 'endtime', t2, ...
                 'sampletime', sampletime, ...
                 'data', data2, ...
                 'units', 'T', ...
                 'creator', defaults.userdetails.realname, ...
                 'components', {'X' 'Y' 'Z'});
  
  % Find the probable resolution (likely to be 10s or 20s), use as
  % integration time
  [res mesg] = guessresolution(miatmp);
  miatmp = setintegrationtime(miatmp, repmat(res, size(sampletime)));
  
  if isempty(r)
    r = miatmp;
  else
    r = insert(r, miatmp);
  end

  % get ready for next block of data
  t = t2;
  reqnum = reqnum + 1;
end

% Trim to desired times and components
comps = getcomponents(mia);
if ~isempty(comps)
  extractComps = {'components', comps};
else
  extractComps = {};
end

r = extract(r, ...
	    'starttime', st, ...
	    'endtime', et, ...
	    extractComps{:});

r = setresolution(r, res, defaults.resolutionmethod);
