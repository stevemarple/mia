function r = samnetcode(mag)



if ischar(mag)
  switch mag
   case 'bo'
    r = mag_bor_1;
   case 'cr'
    r = mag_crk_1;
   case 'es'
    r = mag_esk_1;
   case 'fa'
    r = mag_far_1;
   case 'gm' 
    r = mag_gml_1;
   case 'ha'
    r = mag_han_1
   case 'hd'
    r = mag_had_1
   case 'hl' 
    r = mag_hll_1;
   case 'kv' 
    r = mag_kvi_1;
   case 'la'
    r = mag_lan_1;
   case 'le'
    r = mag_ler_1;
   case 'no' 
    r = mag_nor_1;
   case 'nu' 
    r = [mag_nur_1 mag_nur_3];
   case 'ou' 
    r = mag_oul_1;
   case 'oj' 
    r = mag_ouj_1;
   case 'up'
    r = mag_upp_1;
   case 'yo' 
    r = mag_yor_1;

   otherwise
    error(sprintf('unknown SAMNET code (was %s)', mag));
  end
elseif isa(mag, 'magnetometer')
  if length(mag) ~= 1
    cell(size(mag));
    for n = 1:numel(mag)
      r{n} = feval(mfilename, mag(n));
    end
    return
  end
  if ~strcmpi(getfacility(mag), {'samnet', 'bgs'})
    warning('facility is not ''SAMNET''');
  end
  abbrev = lower(getabbreviation(mag));
  switch abbrev
   case 'had'
    r = 'hd';
   case 'ouj'
    r = 'oj';
    
   otherwise
    r = abbrev(1:2);
  end
else
  error('incorrect parameters');
end
