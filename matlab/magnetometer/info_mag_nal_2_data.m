function r = info_mag_nal_2_data
%INFO_MAG_NAL_2_DATA Return basic information about mag_nal_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about mag_nal_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=magnetometer;abbreviation=nal;serialnumber=2

r.limits = [];
r.abbreviation = 'nal';
r.bibliography = '';
r.comment = '';
r.components = {'''X' 'Y' 'Z'};
r.coordinatescheme = '';
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.elevation = [];
r.elevationmethod = '';
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.id = 61;
r.latitude = 0;
r.location1 = 'Ny-Alesund';
r.location1_ascii = 'Ny-Alesund';
r.location2 = 'Svalbard';
r.logo = '';
r.logurl = '';
r.longitude = 0;
r.modified = timestamp([2014 11 26 13 31 00.287255]);
r.name = '';
r.piid = [];
r.pulsation = false;
r.resolution = timespan(00, 'h', 00, 'm', 00.04, 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 2;
r.starttime = timestamp([2014 01 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename.mag_data.full.archive = 'full';
r.defaultfilename.mag_data.full.dataclass = 'double';
r.defaultfilename.mag_data.full.defaultarchive = true;
r.defaultfilename.mag_data.full.duration = timespan(01, 'h', 00, 'm', 00, 's');
r.defaultfilename.mag_data.full.failiffilemissing = false;
r.defaultfilename.mag_data.full.format = 'pric_mag';
r.defaultfilename.mag_data.full.fstr = 'http://spears.lancs.ac.uk/miadata/magnetometer/nal_2/mag_data/%Y/%m/%Y%m%d/FM_11_%y%m%d%H%M%S.TXT';
r.defaultfilename.mag_data.full.loadfunction = '';
r.defaultfilename.mag_data.full.resolution = timespan(00, 'h', 00, 'm', 00.04, 's');
r.defaultfilename.mag_data.full.savefunction = '';
r.defaultfilename.mag_data.full.size = [3 90000];
r.defaultfilename.mag_data.full.units = 'T';
r.institutions = {};
% end of function
