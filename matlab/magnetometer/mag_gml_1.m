function r = mag_gml_1
%MAG_GML_1  MAGNETOMETER object for Glenmore Lodge, UK.

% Automatically generated by makeinstrumentdatafunctions
r = magnetometer('abbreviation', 'gml', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'SAMNET', ...
    'location', location('Glenmore Lodge', 'UK', ...
                         57.160000, -3.680000));

