function mia = mag_qdc(varargin)
% MAG_QDC  Constructor for MAG_QDC class

cls = 'mag_qdc'; % our class name
parent = 'mag_data'; % our parent class


latestversion = 1;
mia.versionnumber = latestversion;
% start/end times (as timespans) of the period used to mag the QDC
mia.qdcstarttime = [];
mia.qdcendtime = [];
mia.quietdays = []; % store as integers to save even more space

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & isa(varargin{1},cls)
  % copy constructor   
  mia = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  defaults = mia; % take on standard settings
  defaults.load = [];
  %defaults.loadoptions = {'archive','qdc'};  % took me bloody days to figure this one out - MG
  defaults.loadoptions = {};  % or maybe not - loaddata doesn't like it - samnet_file_to_mia needs it
  defaults.log = 1;
  defaults.time = [];
  [defaults unvi] = interceptprop(varargin, defaults);
  
  defaults.loadoptions = { defaults.loadoptions{:},'archive','qdc'};  % took me bloody days to figure this one out - MG

  mia = rmfield(defaults, {'load' 'loadoptions' 'log' 'time'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mia_base to warn about
  % anything left over
    
  if ~isfield(defaults,'starttime') & ~isfield(defaults,'endtime') & isfield(defaults,'time') & ~isempty(defaults.time)
      % we have a 'time' parameter as usually specified during QDC 
      % creation. Data load function needs 'starttime' and 'endtime',
      % however, so we generate a suitable start- and endtime around the
      % time in question, namely the first day of the month. This is
      % consistent with what samnet_file_to_mia does when loading mag_qdc
      % data. If we don't use the 1st day of the month, the bloody loaddata
      % function will chop out (="extract") all data and leave us with
      % nothing. (MG)
      
      % This also makes sure we only cover one day (midnight - midnight),
      % otherwise loaddata will try to join qdcs, which it can't do for
      % mag qdcs.
      defaults.time = timestamp( [getyear(defaults.time) ...
                                  getmonth(defaults.time) ...
                                  1 ...
                                  0 0 0] );
      p = feval(parent, varargin{unvi}, 'load', 0, 'starttime', defaults.time, 'endtime', defaults.time + timespan(1,'d') );
  else
      p = feval(parent, varargin{unvi}, 'load', 0 );
  end
  
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, 'time', defaults.time, ...
		   defaults.loadoptions{:});
  end
  
else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
