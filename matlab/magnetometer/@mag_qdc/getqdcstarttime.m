function r = getqdcstarttime(mia)
%GETQDCSTARTTIME  Return the start time of the period used to make the QDC.
%
%   r = GETQDCSTARTTIME(mia)
%   r: TIMESTAMP object(s)
%   mia: MAG_QDC object(s)

N = numel(mia);
if N == 1
  r = mia.qdcstarttime;
else
  r = repmat(timestamp, size(mia));
  for n = 1:N
    % matlab 5.1 cannot subscript and access member fields simultaneously 
    tmp = mia(n); 
    r(n) = tmp.qdcstarttime;
  end
end
