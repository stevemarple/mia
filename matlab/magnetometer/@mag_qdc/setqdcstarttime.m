function r = setqdcstarttime(mia, t)
%SETQDCSTARTTIME  Set the start time of the period used to make the QDC.
%
%   r = SETQDCSTARTTIME(mia, t)
%   r: modified MAG_QDC object(s)
%   mia: MAG_QDC object(s)
%   t: TIMESTAMP object(s)

if isequal(size(mia), size(t))
  ; % nothing to do
elseif numel(mia) > 1 & numel(t) == 1
  t = repmat(t, size(mia));
else
  error('t must be scalar or same size as mia');
end

r = mia;
for n = 1:numel(mia)
  % matlab 5.1 cannot subscript and access member fields simultaneously 
  tmp = mia(n); 
  tmp.qdcstarttime = t;
  r(n) = tmp;
end
