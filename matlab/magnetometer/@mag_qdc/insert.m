function r = insert(a, b)
%INSERT  Insert MAG_QDC object into another MAG_QDC object.
%
%   r = INSERT(a, b)
%
%   If times do not overlap NAN is inserted.

error(sprintf('insert not yet implemented for %s class', class(a)));
