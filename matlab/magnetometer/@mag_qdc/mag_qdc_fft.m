function r = mag_qdc_fft(mia, varargin)
%MAG_QDC_FFT Convert a plain magnetometer QDC to one described by an FFT.
%
%   r = MAG_QDC_FFT(mia, ...)
%
% r: completed QDC
% mia: original MAG_QDC object, to which the FFT fit is applied
%
% The following parameter name/value pairs are accepted:
%
%   'fitorder', integer
%     The order of the FFT fit. Default is 5.
%
%   'prefilter', MIA_FILTER_BASE
%     prefilter the MAG_QDC object before applying the FFT fit.
%
%   'preserveoriginalqdc', LOGICAL
%     By default the original QDC with all of its data is kept, but this
%     can be discarded if space is an issue.
%

day = timespan(1, 'd');
if ~isequal(getduration(mia), day)
  error('initial QDC must be exactly 1d duration');
end

comps = getcomponents(mia);
ncomps = numel(comps);

defaults.prefilter = [];
defaults.fitorder = 5;
defaults.preserveoriginalqdc = 1;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isa(defaults.prefilter, 'mia_filter_base')
  mia = filter(defaults.prefilter, mia);
end

res = getresolution(mia);



day_res = day ./ res;
if day_res ~= fix(day_res)
  error('resolution must divide exactly into 1 day');
end

% generate unfiltered QDC
% ufdata = cell(ncomps, 1);

coeffs = zeros(ncomps, defaults.fitorder + 1);
data = zeros(ncomps, day_res);
for cn = 1:ncomps
  tmp = getdata(mia, cn, ':');
  % do sine/raised-cosine series fit
  ft = fft(tmp);
  % save only the terms required
  coeffs(cn, :) = ft(1:(defaults.fitorder+1));
end

if logical(defaults.preserveoriginalqdc)
  originalqdc = mia
else
  originalqdc = [];
end
r = setmagqdc(mag_qdc_fft('originalresolution', res, ...
			  'originalqdc', originalqdc, ...
			  'coefficients', coeffs, ...
			  'load', 0), mia);

r = setcoefficients(r, coeffs);
r = addprocessing(r, sprintf('%s converted to %s by %s', ...
			     class(mia), class(r), mfilename));

% Get the fitted data into the object. Whatever data is put there it
% won't be save on disk, since saveobj removes it
r = loadobj(r);

