function [aa, ar, ba, br, rs] = insertcheck(a, b)
%INSERTCHECK  Overloaded INSERTCHECK for MAG_QDC data.
%
% See mia_base/INSERTCHECK.

[aa, ar, ba, br, rs] = mag_data_insertcheck(a, b);


if ~isequal(a.qdcstarttime, b.qdcstarttime)
  error('QDC start times differ');
end
if ~isequal(a.qdcendtime, b.qdcendtime)
  error('QDC end times differ');
end
if ~isequal(a.quietdays, b.quietdays)
  error('quiet days differ');
end
