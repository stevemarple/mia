function r = char(mia, varargin)
%CHAR  Convert a MAG_QDC object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character


if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia);
  return;
end

defaults.timestampformat = '%H:%M:%S %d %B %Y (day %j)';

[defaults unvi] = interceptprop(varargin, defaults);

if isa(mia.qdcstarttime, 'timestamp')
  start_str = strftime(mia.qdcstarttime, defaults.timestampformat);
else
  start_str = char(mia.qdcstarttime);
end
if isa(mia.qdcendtime, 'timestamp')
  end_str = strftime(mia.qdcendtime, defaults.timestampformat);
else
  end_str = char(mia.qdcendtime);
end


r = sprintf(['%s' ...
	     'QDC start time    : %s\n' ...
	     'QDC end time      : %s\n'], ...
	    mag_data_char(mia), start_str, end_str);

