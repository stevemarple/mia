function r = realign( qdc, st, et )
%REALIGN  Align magnetometer QDC according to given start- and endtime
%
% r = REALIGN( MagQdc, Starttime, Endtime )
%
%  MagQdc: a mag_qdc object containing the QDC, at the moment
%          this needs to be at 1sec resolution for exactly
%          one day (86400 samples), timestamped from
%          YYYY-MM-01 0:0:0h to YYYY-MM-01 23:59:59h.
%
%  Starttime, Endtime: The new start- and endtime.
%
%  The QDC data will be shifted and duplicated as necessary to
%  tie in with the requested timestamps.

% some checks - this function is quite limited at the moment
d=getdata(qdc);
t=getsampletime(qdc);
s=size(d);

if s(1) ~= 1 
  error( ['REALIGN is currently only implemented for QDCs containing' ...
	  ' a single component.'] );
end

if s(2) ~= 86400
  error( ['REALIGN currently needs a pristine QDC to start ' ...
	  'off with.'] );
end

d1 = datevec(t(1));
d2 = datevec(t(end));

if d1(3:6) ~= [1 0 0 0]
  error( ['1st sample must start at midnight on 1st day of month.'] ...
	 );
end

if d2(3:6) ~= [1 23 59 59]
  error( ['last sample time must be 23:59:59 on 1st day of month.' ] ...
	 );
end

if st>=et
  error( 'starttime must be earlier than endtime' );
end

% get on with it

% generate new set of timestamps
nt = st + timespan( 0:gettotalseconds(et-st)-1, 's' );

  % NB these timestamps should not really be aligned to 1s boundaries, but
  %    to in between 1s boundaries according to "new" MIA
  %    specs. But the originally imported QDC does not adhere to
  %    this specification, so no need for the realigned one to
  %    adhere to this either. We are never really interested in the
  %    timestamps anyway.

% find corresponding offsets into our 1day QDC
of = mod( gettotalseconds(nt - floor(nt(1),timespan(1,'d'))), ...
	  86400 ) + 1; 
             % offset into day in seconds, +1 because MATLAB
	     % indexing is 1-based
	     
% retrieve realigned QDC data
realigned_d = d(of);

% store new data and time values in object
qdc = setdata( qdc, realigned_d );
qdc = setsampletime( qdc, nt );
qdc = setstarttime( qdc, st );
qdc = setendtime( qdc, et );

% return new object
r = qdc;

	 
