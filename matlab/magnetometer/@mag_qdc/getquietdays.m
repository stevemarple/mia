function r = getquietdays(mia)
%GETQUIETDAYS  Return the quiet days used in the creation of a MAG_QDC_FFT.
%
%   r = GETQUIETDAYS(mia)
%
%   r: vector of TIMESTAMPs (or CELL of vector of TIMESTAMPs if mia is
%      non-scalar 
%   mia: MAG_QDC_FFT object
%
% See also MAG_QDC_FFT, MAG_QDC, TIMESTAMP.

if length(mia) ~= 1
  r = cell(size(mia));
  num = numel(mia);
  for n = 1:numel(mia)
    % cannot simultaneously subscript with () and . in matlab 5.1
    tmp = mia(n); 
    r{n} = getdate(tmp.qdcstarttime) + timespan(tmp.quietdays - 1, 'd');
  end
else
  r = getdate(mia.qdcstarttime) + timespan(mia.quietdays - 1, 'd');
end
