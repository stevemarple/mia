function varargout = instrumenttypeinfo(in, info, varargin)

if isa(in, 'pulsation_magnetometer')
  abbrev = 'pmag';
else
  abbrev = 'mag';
end

s = feval([abbrev '_instrumenttypeinfo_data']);
switch char(info)
 case 'abbreviation'
  varargout{1} = abbrev;

 case 'aware'
  % indicate which instruments MIA is aware of, even if they are not
  % supported by MIA (useful for plotting)
  varargout{1} = s.aware;
  
 case 'imageclass'
  varargout{1} = ''; % no images 
 
 case 'supported'
  % mfilename(in, 'supported')
  % mfilename(in, 'supported', 'datatype', mag_data)
  
  % list supported instruments. Optionally take the 'datatype' for which
  % support is required
  
  defaults.datatype = 'any';
  [defaults unvi] = interceptprop(varargin, defaults);
  warnignoredparameters(varargin{unvi(1:2:end)});
  
  if ~isfield(s.supported, defaults.datatype)
    error(sprintf('do not have information for data type ''%s''', ...
		  defaults.datatype));
  end
  varargout{1} = getfield(s.supported, defaults.datatype);
  

 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end

