function varargout = plotfov(mag, varargin)
%PLOTFOV  Dummy PLOTFOV function for MAGNETOMETER object.
%
%   magnetometer/PLOTFOV is a dummy function since magnetometers do not
%   have defined fields of view.
%
%   See also mia_instrument_base/PLOTFOV.

varargout = cell(1, nargout);
