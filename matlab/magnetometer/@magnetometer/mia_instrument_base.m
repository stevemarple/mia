function r = mia_instrument_base(in)
%MIA_INSTRUMENT_BASE  Convert to a plain MIA_INSTRUMENT_BASE object.
%
%   r = MIA_INSTRUMENT_BASE(in);
%   r: MIA_INSTRUMENT_BASE object
%   in: RIOMETER object
%
%   See also MAGNETOMETER, MIA_INSTRUMENT_BASE.

r = in.mia_instrument_base;
