function r = mia_datadir(mag)
%MIA_DATADIR  Return the data base directory for a MAGNETOMETER object.
%
%   r = MIA_DATADIR(rio)
%   r: base directory (CHAR)
%   rio: MAGNETOMETER object
% 
%   See also mia_instrument_base/INFO.

r = fullfile(mia_datadir, 'magnetometer', ...
	     sprintf('%s_%d', getabbreviation(mag), getserialnumber(mag)));
