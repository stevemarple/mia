function r = info2(in, vout, s, req, varargin)

abbrev_sn = [getabbreviation(in) '_' int2str(getserialnumber(in))];

r = vout;
r.validrequest = 1; % assume valid
switch req
 case 'components'
  r.varargout{1} = s.components;
  
 otherwise
  r.validrequest = vout.validrequest; % put back to original setting
end
