function in = magnetometer(varargin)
%MAGNETOMETER  Constructor for MAGNETOMETER class.
%
%   r = MAGNETOMETER
%
%   See also MIA_INSTRUMENT_BASE.

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

if nargin == 0
  % default constructor
  ib = mia_instrument_base;
  in = class(in, 'magnetometer', ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'magnetometer')
  % copy constructor
  in=varargin{1};
%  in = varargin{1};
%  n = class(in, 'magnetometer', ib);

  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = mia_instrument_base(varargin{unvi});
  in = class(in, 'magnetometer', ib);

else
  error('incorrect parameters');
end

  
% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;


