function r = char(in, varargin)
%CHAR  Convert a MAGNETOMETER object into a CHAR representation.
%
%   r = CHAR(in)
%   in: MAGNETOMETER object
%   r: CHAR representation.
%
%   See also mia_instrument_base/CHAR.

% NB Make all objects derived from mia_instrument_base print a trailing
% newline character 

if length(in) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_instrument_base_char(in);
  return;
end

if length(varargin)
  r = mia_instrument_base_char(in, varargin{:});
  return
end

% what to do for scalars
r = mia_instrument_base_char(in);



