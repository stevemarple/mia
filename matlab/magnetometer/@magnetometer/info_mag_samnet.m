function r = info_mag_samnet(in, vout, s, req, varargin)

r = vout;
r.validrequest = 1; % assume valid

abbrev = getabbreviation(in);
samcode = samnetcode(in);


switch req
   case 'defaultfilename'
  cls = varargin{1};
  if ~ischar(varargin{1})
    cls = class(cls);
  end

  if strcmp(cls, 'mag_qdc')
    v = r.varargout{1};
    v.archiveresolution = timespan(5, 's');
    v.recordlength = 720;
    r.varargout{1} = v;
    
  elseif strcmp(cls, 'mag_data')
    defaults.archive = '5s';
    [defaults unvi] = interceptprop({varargin{2:end}}, defaults);
    
    if isempty(defaults.archive)
      defaults.archive = '5s';
    end

    v = r.varargout{1};
    if isempty(v)
      return;
    end
    
    v.archive = defaults.archive;
    
    switch defaults.archive
     case '1s'
      % v.fstr = fullfile(filesep, 'samnet', '1s_archive','%Y', ...
      % 			      '%m', [samcode '%d%m%y.dgz']);
      v.recordlength = 60;
      v.archiveresolution = timespan(1, 's');
      
     case '5s'
      % v.fstr = fullfile(filesep, 'samnet', '5s_archive','%Y', ...
      %		      '%m', [samcode '%d%m%Y.5s.gz']);
      v.recordlength = 720;
      v.archiveresolution = timespan(5, 's');
      
     case 'preliminary'
      % automatically-processed data
      % v.fstr = fullfile(filesep, 'samnet', 'preliminary', ...
      %		      abbrev, [samcode '%d%m%y.day']);
      v.recordlength = 60;
      v.archiveresolution = timespan(1, 's');
      
      if any(strcmp(abbrev, {'esk' 'had' 'ler'}))
	% 	if strcmp(abbrev, 'had')
	% 	  code2 = 'ha'; % Aaagh. BGS have used "ha"!
	% 	else
	% 	  code2 = samcode;
	% 	end
	% v.fstr = fullfile(filesep, 'samnet', 'preliminary', ...
	%	  abbrev, [samcode '%d%m%y.day.gz']);
	v.recordlength = 60;
	v.archiveresolution = timespan(1, 's');
      end
      
      case 'realtime2'
       v.size = [nan 4];
       v.format = 'samnetrt2';
      % v.loadfunction = 'samnetrt2mia';
      % v.fstr = fullfile(filesep, 'samnet', 'realtime', abbrev, ...
      %  		'%Y', '%m', [abbrev '%Y%m%d.rt']);
      % v.archiveresolution = timespan(1, 's');
      % v.size = [nan 4];
      
     case 'incoming'
      % data transferred to use by FTP, fetched by telephone etc
      d = {filesep, 'samnet', 'incoming', abbrev};
      v.recordlength = 60;
      v.archiveresolution = timespan(1, 's');
      v.duration = timespan(1, 'd');
      
      abbrev_sn = sprintf('%s_%d', abbrev, getserialnumber(in));
      switch abbrev_sn
       case {'bor_1' 'crk_1' 'far_1' 'gml_1' 'kvi_1' 'nor_1' 'nur_1' 'oul_1'}
	v.duration = timespan(1, 'h');
	% v.fstr = fullfile(d{:}, [samcode '%d%m%y.%H.gz']);
       
       case {'hll_1' 'yor_1' 'lan_1' 'lnc_2'}
	v.duration = timespan(1, 'h');
	% v.fstr = fullfile(d{:}, [samcode '%d%m%y.%H']);
	
       case {'esk_1' 'had_1' 'ler_1'}
	% v.fstr = fullfile(d{:}, [samcode '%d%m%y.day.gz']);

       case {'han_1' 'han_3', 'kil_1' 'ouj_2' 'nur_3'}
	if strcmp(abbrev, 'nur')
	  c = 'F';
	else
	  c = upper(abbrev(1));
	end
	% v.fstr = fullfile(d{:}, [upper(abbrev) '_' c '_%Y%m%d.dump01']);
	v.loadfunction = 'error';
	v.format = 'IMAGE magnetometer format';
	
       case 'ups_2'
	% /samnet/incoming/ups/pu20030324.1s.gz
	%v.fstr = fullfile(filesep, 'samnet', 'incoming', ...
	%		  'abbrev', 'pu%Y%m%d.1s.gz');
	v.loadfunction = 'error';
	v.format = 'Uppsala magnetometer format';
	
       otherwise
	error(sprintf('unknown instrument (was ''%s'')', char2(in)));
      end
      
    end
    r.varargout{1} = v;
  
  elseif strcmp(cls, 'mag_data_samnet')
    % return the same as info would for a mag_data string/object
    if ischar(varargin{1})
      varargin1 = 'mag_data';
    else
      varargin1 = mag_data(varargin{1});
    end
    r.varargout = {info(in, req, varargin1, varargin{2:end})};
    return;
  end
  
  
 case 'owner'
  abbrevsn = sprintf('%s_%d', abbrev, getserialnumber(in));
  switch abbrevsn
   case {'bor_1' 'crk_1' 'far_1' 'gml_1' 'hll_1' 'kvi_1' 'lan_1' ...
	 'nor_1' 'nur_1' 'oul_1' 'yor_1'}
    r.varargout = {'SAMNET'};
    
    
   case {'esk_1' 'had_1' 'ler_1'}
    r.varargout = {'BGS'};
    
   case {'han_1' 'kil_1' 'nur_3' 'ouj_2'}
    r.varargout = {'IMAGE'};
    
   case 'ups_2'
    r.varargout = {'SGU'};
    
   otherwise
    r.varargout = {''};
  end
  
 otherwise
   r.validrequest = vout.validrequest; % put back to original setting
end

