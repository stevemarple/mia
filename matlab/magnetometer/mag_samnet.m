function r = mag_samnet
%MAG_SAMNET  Return a list of the SAMNET MAGNETOMETER instruments.
%
%   r = MAG_SAMNET
%   r: vector of MAGNETOMETER objects
%
%   For more details about SAMNET see http://samsun.york.ac.uk/samnet.html
%
%   See also MAGNETOMETER.

% could use this to select te names ...
% SELECT 'mag_' || abbreviation || '_' || serialnumber from magnetometers
% WHERE samnetcode NOTNULL ORDER BY abbreviation, serialnumber;


% ... but this is faster and doesn't need updating manually. Just run
% MAKEINSTRUMENTDATAFILES.
mag = instrumenttypeinfo(magnetometer,'aware');
idx = find(strcmp(getfacility(mag),'SAMNET'));
r = mag(idx);

