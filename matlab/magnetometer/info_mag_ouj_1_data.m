function r = info_mag_ouj_1_data
%INFO_MAG_OUJ_1_DATA Return basic information about mag_ouj_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about mag_ouj_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://spears.lancs.ac.uk/cgi-bin/miainstrumentdatafunctions?instrument=magnetometer;abbreviation=ouj;serialnumber=1

r.limits = [];
r.abbreviation = 'ouj';
r.bibliography = 'incoll:47,art:374';
r.comment = '1s resolution data is available from the SAMNET data archive';
r.components = {'X' 'Y' 'Z'};
r.coordinatescheme = '';
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.elevation = [];
r.elevationmethod = '';
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = 'IMAGE';
r.facility_url = 'http://www.ava.fmi.fi/image/';
r.facilityid = 5;
r.groupids = [];
r.id = 33;
r.latitude = 64.52;
r.location1 = 'Oulujarvi';
r.location1_ascii = 'Oulujarvi';
r.location2 = 'Finland';
r.logo = '';
r.logurl = '';
r.longitude = 27.23;
r.modified = timestamp([2006 12 12 10 21 05.069418]);
r.name = '';
r.piid = [];
r.pulsation = false;
r.resolution = timespan(00, 'h', 00, 'm', 10, 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 1;
r.starttime = timestamp([]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename.mag_data.default.archive = 'default';
r.defaultfilename.mag_data.default.dataclass = 'double';
r.defaultfilename.mag_data.default.defaultarchive = true;
r.defaultfilename.mag_data.default.duration = timespan(1 , 'd');
r.defaultfilename.mag_data.default.failiffilemissing = true;
r.defaultfilename.mag_data.default.format = 'mat';
r.defaultfilename.mag_data.default.fstr = 'mag_data/%Y/%m/%Y%m%d%H.mat';
r.defaultfilename.mag_data.default.loadfunction = 'imagemag2mia';
r.defaultfilename.mag_data.default.resolution = timespan([], 's');
r.defaultfilename.mag_data.default.savefunction = '';
r.defaultfilename.mag_data.default.size = [3 8640];
r.defaultfilename.mag_data.default.units = 'T';
r.institutions = {};
% end of function
