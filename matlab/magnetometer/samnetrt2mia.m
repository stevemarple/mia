function mia = samnetrt2mia(miaIn, varargin)

mag = getinstrument(miaIn);
if numel(mag) ~= 1
  % multiple instruments, load for each one in turn
  mia = miaIn;
  for n = 1:numel(mag)
    mia(n) = feval(mfilename, setinstrument(miaIn, mag(n)), varargin{:});
  end
  return;
end
% from this point on mag is scalar

if ~isa(miaIn, 'mag_data')
  error('not a mag_data object');
end
df = info(mag, 'defaultfilename', 'mag_data', 'archive', 'realtime2');


defaults.cancelhandle = [];
defaults.createargs = {};
defaults.extractargs = {};
defaults.asciiloadfunc = '';
defaults.asciiloadfuncextraargs = {};
defaults.emptydatafunc = '';
defaults.emptydatafuncargs = {};
defaults.failiffilemissing = [];
defaults.resolutionmethod = '';
defaults.archive = [];

% accepted for compatibility with samnet2mia, but ignored
defaults.multiplearchives = []; 

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});



day = timespan(1, 'd');
archive_res_s = gettotalseconds(df.resolution);
st = floor(getstarttime(miaIn), day);
et = ceil(getendtime(miaIn), day);
res = getresolution(miaIn);
if isempty(res)
  res = df.resolution;
  miaIn = setresolutionfield(miaIn, res);
end


mia = miaIn;
mia = setstarttime(mia, st);
mia = setendtime(mia, et);
mia = setcomponents(mia, {'H', 'D', 'Z'});
data = repmat(nan, 3, gettotalseconds(getduration(mia)) ./ archive_res_s);
% mia = settimingfield(mia, 'offset');
mia = settimingfield(mia, 'centred');


t = st;
while t < et
  filename = strftime(t, df.fstr);
  if ~isempty(dir(filename));
    offset = gettotalseconds(t - st) + 1;
    disp(['loading ' filename]);
    filedata = load('-ascii', filename);
    % 4 columns, second_of_day, H, D, Z
    data(:, round(offset + filedata(:, 1))) = filedata(:, 2:4)';
  end
  
  t = t + df.duration;
end


mia = setdata(mia, data * 1e-9);
mia = setunits(mia, 'T');

% trim to desired times
mia = extract(mia, ...
	      'starttime', getstarttime(miaIn), ...
	      'endtime', getendtime(miaIn), ...
	      'components', getcomponents(miaIn));

miaInTiming = gettiming(miaIn);
if ~isempty(miaInTiming) & ~isequal(gettiming(mia), miaInTiming)
  error(sprintf('cannot adjust timing from %s to %s', miaInTiming, ...
		gettiming(mia)));
  
end

mia = adddataquality(mia, 'Preliminary');

