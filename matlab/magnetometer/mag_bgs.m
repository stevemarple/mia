function r = mag_bgs
%MAG_BGS  MAGNETOMETER objects for British Geological Survey MAGNETOMETERs.
%

r = [mag_ler_1 mag_esk_1 mag_had_1];
