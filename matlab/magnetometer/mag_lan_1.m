function r = mag_lan_1
%MAG_LAN_1  MAGNETOMETER object for Lancaster, UK.

% Automatically generated by makeinstrumentdatafunctions
r = magnetometer('abbreviation', 'lan', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'SAMNET', ...
    'location', location('Lancaster', 'UK', ...
                         54.010000, -2.770000));

