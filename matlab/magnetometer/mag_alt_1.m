function r = mag_alt_1
%MAG_ALT_1  MAGNETOMETER object for Alta, Norway.

% Automatically generated by makeinstrumentdatafunctions
r = magnetometer('abbreviation', 'alt', ...
    'serialnumber', 1, ...
    'name', '', ...
    'facility', 'IMAGE', ...
    'location', location('Alta', 'Norway', ...
                         69.860000, 22.960000));

