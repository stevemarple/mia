function r = samnetrt2_file_to_mia(filename, varargin)
%SAMNETRT2_FILE_TO_MIA  Convert SAMNET realtime2 file to MIA object.
%
%   mia = SAMNETRT2_FILE_TO_MIA(filename, ...)
%   mia: MAG_DATA object
%   filename: CHAR
%
%

defaults.cancelhandle = [];

% mia_base/loaddata passes the basic object and other details
defaults.mia = [];
defaults.starttime = [];

% accept archive option, but ignore
defaults.archive = 'realtime2';

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.mia)
  error('mia object not set');
elseif isempty(defaults.starttime)
  error('starttime not set');
end

mag = getinstrument(defaults.mia);
df = info(mag, 'defaultfilename', defaults.mia, 'archive', 'realtime2');

tmp = localLoad(filename, df.size)';


day = timespan(1, 'd');
st = floor(defaults.starttime, day);
et = st + day;

sampletime = st + timespan(tmp(1, :) + 0.5, 's');

% Ensure that the sampletimes are unique and sorted!
[sampletime idx] = unique(sampletime);
tmp = tmp(:, idx); % Get rid of duplicated sampletimes

data = tmp(2:4, :);
data(data == 0) = nan;
data(data > 9999 & data < 10000) = nan;
data = data * 1e-9; % convert from nT to T

r = mag_data('starttime', st, ...
	     'endtime', et, ...
	     'sampletime', sampletime, ...
	     'integrationtime', timespan(ones(size(sampletime)), 's'), ...
	     'instrument', mag, ...
	     'load', 0, ...
	     'log', 0, ...
	     'data', data, ...
	     'units', 'T', ...
	     'components', info(mag,'components'));


r = addprocessing(r, 'Loaded from realtime2 archive');
r = adddataquality(r, 'preliminary');

% Function to load ASCII data, ignoring rows which have incorrect number
% of columns. url_load is tried first (for speed/efficiency but it fails
% completely if the number of columns is not constant. If that happens
% read each line in term, ignoring bad lines.
function r = localLoad(filename, sz)
try
  % try loading with normal load
  r = url_load(filename);

catch
  r = zeros(0, sz(2));
  fid = url_fopen(filename);
  n = 0;
  while ~feof(fid)
    s = fgetl(fid);
    d = sscanf(s, '%f') ;
    if numel(d) == sz(2)
      % line has correct number of rows
      n = n + 1;
      r(n, :) = d';
    else
      fprintf(['ignoring line %d in %s: ' ...
	       'incorrect number of columns (%d)\n'], ...
	      n, filename, numel(d));
    end
  end
  fclose(fid);
end

% check size is correct
for n = 1:numel(sz)
  if isfinite(sz(n))
    if size(r, n) ~= sz(n)
      r = zeros([0, sz(2:end)]);
      return; % incorrect size for this dimension
    end
  end
end
