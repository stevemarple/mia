function r = mag_image
%MAG_IMAGE  Return a list of the IMAGE MAGNETOMETER instruments.
%
%   r = MAG_IMAGE
%   r: vector of MAGNETOMETER objects
%
%   For more details about IMAGE see http://www.fmi.fi/image/
%
%   See also MAGNETOMETER.

mag = instrumenttypeinfo(magnetometer,'aware');
idx = find(strcmp(getfacility(mag),'IMAGE'));
r = mag(idx);

