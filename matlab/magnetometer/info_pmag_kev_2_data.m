function r = info_pmag_kev_2_data
%INFO_PMAG_KEV_2_DATA Return basic information about pmag_kev_2.
%
% This function is not intended to be called directly, use the
% INFO function to access data about pmag_kev_2. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://www.dcs.lancs.ac.uk/iono/cgi-bin/miainstrumentdatafunctions?instrument=pulsation_magnetometer;abbreviation=kev;serialnumber=2

r.abbreviation = 'kev';
r.bibliography = '';
r.comment = '';
r.components = {'''X' '''' 'Y' 'Z'};
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.endtime = timestamp([1991 01 01 00 00 00]);
r.facility_name = 'Finnish pulsation magnetometer chain';
r.facility_url = 'http://spaceweb.oulu.fi/projects/pulsations/';
r.facilityid = 7;
r.groupids = [];
r.id = 50;
r.latitude = 69.7;
r.location1 = 'Kevo';
r.location1_ascii = '';
r.location2 = 'Finland';
r.logo = '';
r.longitude = 27;
r.modified = timestamp([2005 02 24 15 28 24.650051]);
r.piid = [];
r.pulsation = 1;
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 2;
r.starttime = timestamp([1974 01 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename = [];
% end of function
