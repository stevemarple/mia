function r = gettype(in, varargin)
%GETTYPE  Return instrument type.
%
%   r = GETTYPE(r)
%   r: CHAR
%   in: MAGNETOMETER object
%
%   See also MIA_INSTRUMENT_BASE, MAGNETOMETER.

if length(varargin) == 0
  mode = 'l';
else
  mode = varargin{1};
end
switch mode
 case 'l'
  r = 'pulsation magnetometer';

 case 'u'
  r = 'PULSATION MAGNETOMETER';
  
 case 'c' 
  r = 'Pulsation magnetometer';
    
 case 'C'
  r = 'Pulsation Magnetometer';
 
 otherwise
  error(sprintf('unknown mode (was ''%s'')', mode));
end

