function in = pulsation_magnetometer(varargin)
%PULSATION_MAGNETOMETER  Constructor for PULSATION_MAGNETOMETER class.
%
%   r = MAGNETOMETER
%
%   See also MIA_INSTRUMENT_BASE.

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

if nargin == 0
  % default constructor
  ib = magnetometer;
  in = class(in, 'pulsation_magnetometer', ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'pulsation_magnetometer')
  % copy constructor
  in = varargin{1};
%  in = varargin{1};
%  n = class(in, 'magnetometer', ib);

  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = magnetometer(varargin{unvi});
  in = class(in, 'pulsation_magnetometer', ib);

else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;
