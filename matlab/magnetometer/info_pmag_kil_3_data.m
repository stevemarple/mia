function r = info_pmag_kil_3_data
%INFO_PMAG_KIL_3_DATA Return basic information about pmag_kil_3.
%
% This function is not intended to be called directly, use the
% INFO function to access data about pmag_kil_3. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://www.dcs.lancs.ac.uk/iono/cgi-bin/miainstrumentdatafunctions?instrument=pulsation_magnetometer;abbreviation=kil;serialnumber=3

r.abbreviation = 'kil';
r.bibliography = '';
r.comment = '';
r.components = {'''X' '''' 'Y' 'Z'};
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = 'Finnish pulsation magnetometer chain';
r.facility_url = 'http://spaceweb.oulu.fi/projects/pulsations/';
r.facilityid = 7;
r.groupids = [];
r.id = 51;
r.latitude = 69;
r.location1 = 'Kilpisjarvi';
r.location1_ascii = 'Kilpisjarvi';
r.location2 = 'Finland';
r.logo = '';
r.longitude = 20.7;
r.modified = timestamp([2006 04 20 10 03 51.93407]);
r.piid = [];
r.pulsation = 1;
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 3;
r.starttime = timestamp([1995 11 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename = [];
% end of function
