function r = info_pmag_rov_1_data
%INFO_PMAG_ROV_1_DATA Return basic information about pmag_rov_1.
%
% This function is not intended to be called directly, use the
% INFO function to access data about pmag_rov_1. To override
% information given in this file see the instructions in INFO.
%
% This function was generated automatically by
% http://www.dcs.lancs.ac.uk/iono/cgi-bin/miainstrumentdatafunctions?instrument=pulsation_magnetometer;abbreviation=rov;serialnumber=1

r.abbreviation = 'rov';
r.bibliography = '';
r.comment = '';
r.components = {'''X' '''' 'Y' 'Z'};
r.coordinatesystem = 'X,Y,Z';
r.datarequestid = [];
r.endtime = timestamp([9999 01 01 00 00 00]);
r.facility_name = 'Finnish pulsation magnetometer chain';
r.facility_url = 'http://spaceweb.oulu.fi/projects/pulsations/';
r.facilityid = 7;
r.groupids = [];
r.id = 54;
r.latitude = 66.6;
r.location1 = 'Rovaniemi';
r.location1_ascii = '';
r.location2 = 'Finland';
r.logo = '';
r.longitude = 25.8;
r.modified = timestamp([2005 04 04 14 53 20.669153]);
r.piid = [];
r.pulsation = 1;
r.resolution = timespan([], 's');
r.rulesoftheroad = '';
r.samnetcode = {};
r.serialnumber = 1;
r.starttime = timestamp([1985 11 01 00 00 00]);
r.starttime_1s = timestamp([]);
r.url = '';
r.defaultfilename = [];
% end of function
