function r = samnet_file_to_mia(filename, varargin)
%SAMNET_FILE_TO_MIA  Convert SAMNET file to MIA object.
%
%   mia = SAMNET_FILE_TO_MIA(filename)
%   mia: MAG_DATA or MAG_QDC object
%   filename: CHAR
%
%

defaults.cancelhandle = [];

% mia_base/loaddata passes the basic object and other details
defaults.mia = [];
defaults.starttime = [];

% silently ignore a 'time' parameter, this parameter might get passed
% down to us if we are loading a QDC.
defaults.time = []; 

defaults.archive = '';


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if ~isempty(defaults.mia)
  cls = class(defaults.mia);
elseif strcmp(defaults.archive, 'qdc')
  cls = 'mag_qdc';
else
  cls = 'mag_data';
end

if isa(feval(cls), 'mag_qdc')
  dateCheckIndex = 1:2; % don't check day of month
else
  dateCheckIndex = 1:3;
end

if isempty(defaults.archive)
  error('archive must be specified');
elseif strcmp(defaults.archive, 'realtime2')
  % realtime2 is a completely different format, so handled by a different
  % function
  r = samnetrt2_file_to_mia(filename, varargin{:});
  return
end

save_gps = strcmp(cls, 'mag_data_samnet');

mia = [];

duration = timespan(1, 'd');


mags = mag_samnet;
mag = [];
for n = 1:numel(mags)
  mag = setfield(mag, samnetcode(mags(n)), mags(n));
end



compressed = 0;
if length(filename) > 2
  compressed = strcmp(filename((end-1):end), 'gz');
end

tmpfilename = '';
tmpfilename2 = '';
if compressed

  if isurl(filename)
    tmpfilename2 = tempname2;
	url_fetch(filename, tmpfilename2);
	origfilename = tmpfilename2;
  else
    origfilename = filename;
  end
  
  
  tmpfilename = tempname2;
  filename = tmpfilename;
  [s w] = system(sprintf('zcat %s > %s', origfilename, tmpfilename));
  if s ~= 0
    error(sprintf('could not uncompress %s', origfilename));
  end
else
  origfilename = filename;
end

fid = url_fopen(filename, 'r');



currenttime_cdf = nan;
samcode = '';


resolution_s = [];
starttime_dv = [];
datasize = [nan nan];
sampletime_sod = [];
gpsinfo = [];
integrationtime = [];
data = [];

if save_gps
  gpsinfo.headertime = [];
  gpsinfo.headers = {};
end

while ~feof(fid)
  cancelcheck(defaults.cancelhandle);
  
  h = localReadHeader(fid, defaults.archive);
  if ~isempty(h)
    if isempty(starttime_dv)
      % first header found
      starttime_dv = h.starttime;
      resolution_s = h.resolution;
      datasize = [3 86400 ./ resolution_s];
      data = repmat(nan, datasize);
      % sample time, in seconds since start of day
      sampletime_sod = repmat(nan, 1, datasize(2)); 
      integrationtime = timespan(h.integrationtime, 's');
      instrument = getfield(mag, h.abbreviation);
      
      if save_gps
	gpsinfo.headertime = repmat(timestamp('bad'), ...
				    1, datasize(end)./ h.recordlength);
	gpsinfo.headers = cell(size(gpsinfo.headertime));
	gps_counter = 0;
      end
    end
   
    % have a valid header
    if ~isequal(starttime_dv(dateCheckIndex), h.starttime(dateCheckIndex))
      % header valid but not from current day, so read all data but
      % discard
      filename
      disp(sprintf(['dates do not match (was %04d-%02d-%02d) ', ...
		    'in %s, discarding bad record'], ...
		   h.starttime(1:3), origfilename));
      [tmpdata count] = fscanf(fid, '%f', [3 h.recordlength]);
    elseif resolution_s ~= h.resolution
      error(sprintf('resolution changed from %d to %d in %s', ...
		    resolution_s, h.resolution, origfilename));
    else
      
      % recompute offset
      secs = h.starttime(4:6) * [3600;60;1];
      sampletime_block = secs + h.offset + ...
	  (0:(h.recordlength-1)) * h.resolution;
      [tmpdata count] = fscanf(fid, '%f', [3 h.recordlength]);
	  
	  
      idx1 = (secs ./ resolution_s) + 1;
      idx2 = idx1 - 1 + size(tmpdata, 2);
      idx = idx1:idx2;
      data(:, idx) = tmpdata;
      sampletime_sod(idx) = sampletime_block;
      
      if save_gps
	gps_counter = gps_counter + 1;
	gpsinfo.headertime(gps_counter) = timestamp(h.starttime);
	gpsinfo.headers{gps_counter} = h.line;
      end
    end
    
  end
  
end

fclose(fid);

if ~isempty(tmpfilename)
  delete(tmpfilename);
end
if ~isempty(tmpfilename2)
  delete(tmpfilename2)
end

if isempty(starttime_dv)
  r = [];
  return;
end

if strcmp(cls, 'mag_qdc')
  starttime_dv(3) = 1;
  qdcst = timestamp(starttime_dv);
  qdcet = qdcst + timespan(daysinmonth(qdcst), 'd');
  qdc_opt = {'qdcstarttime', qdcst, ...
	     'qdcendtime', qdcet};
else
  qdc_opt = {};
end

day = timespan(1, 'd');
st = timestamp(starttime_dv);
et = st + day;
sampletime = st + timespan(sampletime_sod, 's');

% missing data value if 9999.9

data(data > 9999 & data < 10000) = nan; % map 9999.9 to NaN

if strcmp(defaults.archive, 'realtime')
  data(data == 0) = nan; % map 0.0 to NaN for realtime data
end

data = data * 1e-9; % map from nT to T

if strcmp(cls, 'mag_data_samnet')
  gpsinfo_opt = {'gpsinfo', gpsinfo};
else
  gpsinfo_opt = {};
end

r = feval(cls, ...
	  'starttime', st, ...
	  'endtime', et, ...
	  'sampletime', sampletime, ...
	  'integrationtime', repmat(integrationtime, size(sampletime)), ...
	  'instrument', instrument, ...
	  'load', 0, ...
	  'log', 0, ...
	  'data', data, ...
	  'units', 'T', ...
	  'components', info(instrument,'components'), ...
	  qdc_opt{:}, ...
	  gpsinfo_opt{:});

% r = addprocessing(r, sprintf('Loaded from %s archive', defaults.archive));
if any(strcmp(defaults.archive, {'preliminary', 'incoming', 'realtime'}))
  r = adddataquality(r, 'preliminary');
end

function h = localReadHeader(fid, archive)
% '%c%c%2.2d/%2.2d/%4.4d %2.2d%2.2d%2.2d %2.2d %4.4x'

h = [];
h.line = fgetl(fid);
if isempty(h.line)
  % get rid of newline left over from previous data sections
  h.line = fgetl(fid);
end
if feof(fid)
  h = [];
  return
end

% find the first non-whitespace chars
words = find(h.line ~= ' ');
words = [1 words(diff([0 words]) > 1)];

% lin
% words(1)
% ftell(fid)



h.abbreviation = h.line(words(1) + [0:1]);
dv = zeros(1, 6);

dv(3) = str2num(h.line(words(1) + [2:3]));
dv(2) = str2num(h.line(words(1) + [5:6]));
dv(1) = str2num(h.line(words(1) + [8:11]));


dv(4) = str2num(h.line(words(2) + [0:1]));
dv(5) = str2num(h.line(words(2) + [2:3]));
dv(6) = str2num(h.line(words(2) + [4:5]));

h.starttime = dv;

switch archive
 case '5s'
  h.resolution = 5;
  h.offset = 0;
  h.gps = sscanf(h.line(24:end), '%x');
  h.recordlength = 720;
  h.integrationtime = 5;
  
 case {'1s' 'preliminary'}
  h.resolution = 1;
  h.offset = 0.5;
  h.gps = sscanf(h.line(24:end), '%x');
  h.recordlength = 60;  
  h.integrationtime = 1;
  
 case {'incoming' 'realtime'}
  h.resolution = 1; % implicit, not stored in the file
  h.offset = 0.5;
  h.gps = sscanf(h.line(21:end), '%x');
  h.recordlength = 60;  
  h.integrationtime = 1;
  
  case 'qdc'
   h.resolution = str2num(h.line(21:22));
   if h.resolution == 0
     h.resolution = 1;
   end
   h.gps = 'aver';
   h.recordlength = 720;  
   h.integrationtime = h.resolution;
   h.offset = 0;
   
 otherwise
  error(sprintf('unknown archive (was ''%s'')', archive));
end

return;
