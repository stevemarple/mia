function r = info_csrad_c_1_data
%INFO_CSRAD_C_1_DATA  Return basic information about csrad_c_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_c_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'ksr';
r.beamseparation = 3.24;
r.boresight = -20;
r.code = 'c';
r.latitude = 58.68;
r.location1 = 'King Salmon';
r.location2 = 'Alaska';
r.longitude = -156.65;
r.name = 'King Salmon';
r.st_id = 16;

% end of function
