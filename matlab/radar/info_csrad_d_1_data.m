function r = info_csrad_d_1_data
%INFO_CSRAD_D_1_DATA  Return basic information about csrad_d_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_d_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'san';
r.beamseparation = -3.24;
r.boresight = 173.2;
r.code = 'd';
r.latitude = -71.68;
r.location1 = 'Vesleskarvet (SANAE IV)';
r.location2 = 'Antarctica';
r.longitude = -2.85;
r.name = 'SANAE';
r.st_id = 11;

% end of function
