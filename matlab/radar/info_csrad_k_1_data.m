function r = info_csrad_k_1_data
%INFO_CSRAD_K_1_DATA  Return basic information about csrad_k_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_k_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'kap';
r.beamseparation = 3.24;
r.boresight = -12;
r.code = 'k';
r.latitude = 49.39;
r.location1 = 'Kapuskasing';
r.location2 = 'Canada';
r.longitude = -83.32;
r.name = 'Kapuskasing';
r.st_id = 3;

% end of function
