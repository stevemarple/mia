function r = eiscat_esr
%EISCAT_vhf  EISCAT Svalbard radar radar.

r = is_radar('name', 'EISCAT Svalbard', ...
	     'abbreviation', 'eiscate', ...
	     'facility', 'EISCAT', ...
	     'location', location('Longyearbyen', 'Svalbard', ...
				  78.1530, 16.0290), ...
	     'serialnumber', 2, ...
	     'tx', 1, ...
	     'rx', 1);
