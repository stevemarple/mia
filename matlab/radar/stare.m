function r = stare
%STARE  Return a list of the STARE coherent scatter radars.
%
%   r = STARE
%   r: vector of CS_RADAR objects
%
%   For more details about STARE see
%   http://www.linmpi.mpg.de/english/projekte/stare/ 
%
%   See also CS_RADAR.

r = [stare_fin stare_nor];
