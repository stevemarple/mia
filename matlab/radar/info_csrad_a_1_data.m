function r = info_csrad_a_1_data
%INFO_CSRAD_A_1_DATA  Return basic information about csrad_a_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_a_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'kod';
r.beamseparation = 3.24;
r.boresight = 30;
r.code = 'a';
r.latitude = 57.6;
r.location1 = 'Kodiak';
r.location2 = 'Alaska';
r.longitude = -152.2;
r.name = 'Kodiak';
r.st_id = 7;

% end of function
