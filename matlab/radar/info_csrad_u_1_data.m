function r = info_csrad_u_1_data
%INFO_CSRAD_U_1_DATA  Return basic information about csrad_u_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_u_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'unw';
r.beamseparation = -3.24;
r.boresight = 215;
r.code = 'u';
r.latitude = -46.51;
r.location1 = 'Unwin';
r.location2 = 'New Zealand';
r.longitude = 168.38;
r.name = 'TIGER New Zealand';
r.st_id = 17;

% end of function
