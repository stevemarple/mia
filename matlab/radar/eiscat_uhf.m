function r = eiscat_uhf
%EISCAT_UHF  EISCAT Tromso UHF radar.
r = is_radar('name', 'EISCAT Tromso (UHF)', ...
	     'abbreviation', 'eiscatt', ...
	     'facility', 'EISCAT', ...
	     'location', location('Tromso', 'Norway', 69.5865, 19.2263), ...
	     'serialnumber', 1, ...
	     'tx', 1, ...
	     'rx', 1);

% Coordinates from Andrew Senior (Google Earth) 2010-07-01.
