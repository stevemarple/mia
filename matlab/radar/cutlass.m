function r = cutlass
%CUTLASS  Return a list of the CUTLASS coherent scatter radars.
%
%   r = CUTLASS
%   r: vector of CS_RADAR objects
%
%   For more details about CUTLASS see http://www.ion.le.ac.uk/
%
%   See also CS_RADAR.

r = [cutlass_f cutlass_i];
