function r = info_csrad_j_1_data
%INFO_CSRAD_J_1_DATA  Return basic information about csrad_j_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_j_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'sys';
r.beamseparation = -3.33;
r.boresight = 159;
r.code = 'j';
r.latitude = -69;
r.location1 = 'Syowa';
r.location2 = 'Antarctica';
r.longitude = 39.58;
r.name = 'Syowa South';
r.st_id = 12;

% end of function
