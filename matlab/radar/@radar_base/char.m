function r = char(in)
%CHAR  Convert a RADAR_BASE object to a CHAR.
%
%   r = CHAR(in)

% NB Make all objects derived from mia_instrument_base print a trailing
% newline character 

if length(in) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_instrument_base_char(in);
  return;
end

tx_str = '';
if ~isempty(in.tx)
  tx_str = sprintf('%d', in.tx);
end
rx_str = '';
if ~isempty(in.rx)
  rx_str = sprintf('%d', in.rx);
end

r = sprintf(['%s' ...
	     'tx                : %s\n' ...
	     'rx                : %s\n'], ...
	    mia_instrument_base_char(in), tx_str, rx_str);
