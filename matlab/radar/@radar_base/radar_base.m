function in = radar_base(varargin)
%RADAR  Constructor for RADAR class.
%
%   r = RADAR
%
%   Abstract base class for radars.
%
%   See also MIA_INSTRUMENT_BASE.

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

% allow for bistatic and tristatic operation
in.tx = []; % transmitting capability
in.rx = []; % receiving capability

if nargin == 0
  % default constructor
  ib = mia_instrument_base;
  in = class(in, 'radar_base', ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'is_radar')
  in = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = mia_instrument_base(varargin{unvi});
  in = class(in, 'radar_base', ib);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;
