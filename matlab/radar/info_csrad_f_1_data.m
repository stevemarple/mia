function r = info_csrad_f_1_data
%INFO_CSRAD_F_1_DATA  Return basic information about csrad_f_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_f_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'han';
r.beamseparation = 3.24;
r.boresight = -12;
r.code = 'f';
r.latitude = 62.32;
r.location1 = 'Hankasalmi';
r.location2 = 'Finland';
r.longitude = 26.61;
r.name = 'Finland';
r.st_id = 10;

% end of function
