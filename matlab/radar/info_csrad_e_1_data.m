function r = info_csrad_e_1_data
%INFO_CSRAD_E_1_DATA  Return basic information about csrad_e_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_e_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'pyk';
r.beamseparation = 3.24;
r.boresight = 30;
r.code = 'e';
r.latitude = 63.77;
r.location1 = 'Pykkvibaer';
r.location2 = 'Iceland';
r.longitude = -20.54;
r.name = 'Iceland East';
r.st_id = 9;

% end of function
