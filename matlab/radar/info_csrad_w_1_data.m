function r = info_csrad_w_1_data
%INFO_CSRAD_W_1_DATA  Return basic information about csrad_w_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_w_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'sto';
r.beamseparation = 3.29;
r.boresight = -59;
r.code = 'w';
r.latitude = 63.86;
r.location1 = 'Stokkseri';
r.location2 = 'Iceland';
r.longitude = -22.02;
r.name = 'Iceland West';
r.st_id = 8;

% end of function
