function r = info_csrad_r_1_data
%INFO_CSRAD_R_1_DATA  Return basic information about csrad_r_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_r_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'tig';
r.beamseparation = 3.24;
r.boresight = 180;
r.code = 'r';
r.latitude = -43.38;
r.location1 = 'Tasmania';
r.location2 = 'Australia';
r.longitude = 147.23;
r.name = 'TIGER Tasmania';
r.st_id = 14;

% end of function
