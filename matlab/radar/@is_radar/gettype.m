function r = gettype(in)
%GETTYPE  Return instrument type
%
%   r = GETTYPE(in)
%
%   See also MIA_INSTRUMENT_BASE, RADAR_BASE, MIA_INSTRUMENT_BASE.

r = 'incoherent scatter radar';
