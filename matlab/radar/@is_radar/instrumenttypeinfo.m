function varargout = instrumenttypeinfo(in, info, varargin)
%INSTRUMENTTYPEINFO  Return information about the instrument type.
%
%   r = INSTRUMENTTYPEINFO(in, 'supported')
%   r = INSTRUMENTTYPEINFO(in, 'aware')
%
%   in: any IS_RADAR instrument
%
%   See also IS_RADAR, RADAR_BASE, mia_instrument_base/INFO.

switch char(info)
 case 'abbreviation'
  varargout{1} = 'israd';
  
 case 'aware'
  varargout{1} = [superdarn stare];
  
 case 'supported'
  varargout{1} = repmat(is_radar, [0 0]);
  
 otherwise
  error(sprintf('unknown info (was ''%s'')', char(info)));
end
  
