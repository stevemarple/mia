function in = is_radar(varargin)
%IS_RADAR  Constructor for IS_RADAR (incoherent scatter radar) class.
%
%   r = IS_RADAR
%
%   See also RADAR_BASE, MIA_INSTRUMENT_BASE.

% Make it easy to change the class definition at a later date
latestversion = 1;
in.versionnumber = latestversion;

if nargin == 0
  % default constructor
  ib = radar_base;
  in = class(in, 'is_radar', ib);
  
elseif nargin == 1 & strcmp(class(varargin{1}), 'is_radar')
  in = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  [in unvi] = interceptprop(varargin, in);
  ib = radar_base(varargin{unvi});
  in = class(in, 'is_radar', ib);
  
else
  error('incorrect parameters');
end

% Ensure that the returned object is marked with the latest version
% number
in.versionnumber = latestversion;

  

