function r = info_csrad_b_1_data
%INFO_CSRAD_B_1_DATA  Return basic information about csrad_b_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_b_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'pgr';
r.beamseparation = 3.24;
r.boresight = -5;
r.code = 'b';
r.latitude = 53.98;
r.location1 = 'Prince George';
r.location2 = 'Canada';
r.longitude = -122.59;
r.name = 'Prince George';
r.st_id = 6;

% end of function
