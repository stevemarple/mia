function r = info_csrad_g_1_data
%INFO_CSRAD_G_1_DATA  Return basic information about csrad_g_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_g_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'gbr';
r.beamseparation = 3.24;
r.boresight = 5;
r.code = 'g';
r.latitude = 53.32;
r.location1 = 'Goose Bay';
r.location2 = 'Canada';
r.longitude = -60.46;
r.name = 'Goose Bay';
r.st_id = 1;

% end of function
