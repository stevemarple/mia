function r = info_csrad_h_1_data
%INFO_CSRAD_H_1_DATA  Return basic information about csrad_h_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_h_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'hal';
r.beamseparation = -3.24;
r.boresight = 165;
r.code = 'h';
r.latitude = -75.52;
r.location1 = 'Halley';
r.location2 = 'Antartica';
r.longitude = -26.63;
r.name = 'Halley';
r.st_id = 4;

% end of function
