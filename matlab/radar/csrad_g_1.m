function r = csrad_g_1
%CSRAD_G_1  SUPERDARN Goose Bay radar.

% automatically generated by makesuperdarninstruments
loc = location('Goose Bay', 'Canada', 53.32, -60.46);
r = cs_radar('name', 'Goose Bay', ...
             'abbreviation', 'g', ...
             'facility', 'SUPERDARN', ...
             'location', loc, ...
             'serialnumber', 1, ...
             'tx', 1, ...
             'rx', 1);

