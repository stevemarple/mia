function r = info_csrad_n_1_data
%INFO_CSRAD_N_1_DATA  Return basic information about csrad_n_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_n_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'sye';
r.beamseparation = -3.33;
r.boresight = 106.5;
r.code = 'n';
r.latitude = -69.01;
r.location1 = 'Syowa';
r.location2 = 'Antarctica';
r.longitude = 39.61;
r.name = 'Syowa East';
r.st_id = 13;

% end of function
