function r = info_csrad_p_1_data
%INFO_CSRAD_P_1_DATA  Return basic information about csrad_p_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_p_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'ker';
r.beamseparation = -3.24;
r.boresight = 168;
r.code = 'p';
r.latitude = -49.35;
r.location1 = '';
r.location2 = 'Kerguelen Island';
r.longitude = 70.26;
r.name = 'Kerguelen';
r.st_id = 15;

% end of function
