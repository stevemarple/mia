function r = plotsuperdarnfov(in, ah, varargin)
%PLOTSUPERDARNFOV  Plot the field of view for SUPERDARN radar(s)
%
%   h = PLOTSUPERDARNFOV(in, ah, ...)
%   h: GUI handles
%   in: CS_RADAR instrument(s)
%   ah: AXES handle
%
% PLOTSUPERDARNFOV plots the field of view, beams and boresight direction
% for SUPERDARN radars. The default behaviour may be modified witht he
% following parameter name/value pairs: 
%
%     'beams', ['line' | 'patch' | 'transpatch']
%     Plot the outline of the beams in the style indicated. 'tranpatch' uses
%     the GPC functions to produce fake transparency. See GPC_TRANSPATCH and
%     GPC_M_TRANSPATCH for more details.
%
%     'fov', ['line' | 'patch' | 'transpatch']
%     Plot the field of view outline in the style indicated. 
%
%     'boresight', LOGICAL
%     Flag to indicate if the boresight (pointing direction) should be
%     plotted. Boresight is always plotted as a line.
%
%     'ismmap', LOGICAL
%     Flag to indicate if the AXES contains mapping information from
%     M_MAP. This cn normmaly be idetnified automatically with ISMMAP.
%
%     'rangeboxes', DOUBLE vector (in metres)
%     The line of sight (not ray-path!) distances to use if range boxes
%     are to be added. This property also affects the field of view
%     outline.
%
%     'gpcalpha', DOUBLE, (0 <= d <= 1)
%     A scalar value indicating the transparency of the any any
%     transpatch patches.
% 
%     'earthradius', CHAR or NUMERIC (in metres)
%     The Earth radius value used by TC_D2LL. See TC_D2LL for more
%     details.


defaults.lineargs = {};
defaults.patchargs = {};
defaults.ismmap = []; % defer

% defaults.rangeboxes = [180:450:3330] * 1e3;
defaults.rangeboxes = (180 + (0:74)*45)  * 1e3;
% each of these can be line, patch or transpatch, anything else means none
defaults.beams = '';
defaults.fov = '';

defaults.boresight = 0; % flag (logical)
defaults.earthradius = '';
defaults.height = 400e3;

defaults.gpcalpha = 0.8;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.ismmap)
  defaults.ismmap = ismmap(ah);
end
if defaults.ismmap
  patchfunc = 'm_patch';
  transpatchfunc = 'gpc_m_transpatch';
  linefunc = 'm_line';
else
  patchfunc = 'patch';
  transpatchfunc = 'gpc_transpatch';
  linefunc = 'line';
end  

if isempty(defaults.fov) & isempty(defaults.beams) & ...
      (isempty(defaults.boresight) | defaults.boresight == 0)
  defaults.fov = 'patch';
end

r = [];

rb = numel(defaults.rangeboxes);

for n = 1:numel(in)
  beams = info(in(n), 'beams');
  beams_2 = beams ./ 2;
  beamsep = info(in(n), 'beamseparation');
  boresight = -info(in(n), 'boresight');
  loc = getlocation(in(n));

  if defaults.boresight
    % plot the boresight direction
    [lon lat] = tc_d2ll(loc, repmat(boresight, size(defaults.rangeboxes)), ...
			defaults.rangeboxes, ...
			'earthradius', defaults.earthradius);
    feval(linefunc, lon, lat, 'color', 'y', defaults.lineargs{:}, ...
	  'Parent', ah);
  end
  
  
  % plot the beam outlines
  if any(strcmp(defaults.beams, {'patch' 'transpatch' 'line'}))
    r(n).beams = [];
    d = [defaults.rangeboxes(:); flipud(defaults.rangeboxes(:))];
    tmp = [];
    for b = 1:beams
      offset = b - beams_2 - 1;
      tc = [repmat(boresight - beamsep .* offset, rb, 1);
	    repmat(boresight - beamsep .* (offset+1), rb, 1)];
      [lon lat] = tc_d2ll(loc, tc, d, 'earthradius', defaults.earthradius);
      
      switch defaults.beams
       case 'patch'
	tmp = feval(patchfunc,  lon, lat, 'r', defaults.patchargs{:}, ...
		    'Parent', ah);
	
       case 'transpatch'
	if defaults.ismmap
	  % use m_map aware version, so that background patches are plotted on
	  % the clipped background, not the axes. Create GPC polygons with
	  % coordinates mapped from degrees to appropriate values
	  
	  tmp = gpc_m_transpatch(gpc_m_map(lon,lat), ah, ...
				 defaults.gpcalpha, ...
				 'FaceColor', 'k', ...
				 'EdgeColor', 'none', ...
				 defaults.patchargs{:});
	else
	  tmp = gpc_transpatch(gpc(lon, lat), ah, defaults.gpcalpha, ...
			       'FaceColor', 'k', ...
			       'EdgeColor', 'none', ...
			       defaults.patchargs{:});
	end 
	
       case 'line' 
	% unlike patches lines must be explicitly closed back to the start
	x = [x(:); x(1)];
	y = [y(:); y(1)];
	tmp = feval(linefunc,  lon, lat, defaults.lineargs{:}, ...
		    'Parent', ah);
	
       otherwise
	error('Unknown style');
      end
      if ~isempty(tmp) & ~isequal(tmp, 0)
	r(n).beams(end+1) = tmp;
      end
    end
  end
  
  % plot the fov 
  if any(strcmp(defaults.fov, {'patch' 'transpatch' 'line'}))
    tmp = [];
    r(n).fov = [];
    d = [defaults.rangeboxes(:); 
	 repmat(defaults.rangeboxes(end), beams+1, 1);
	 flipud(defaults.rangeboxes(:));
	 repmat(defaults.rangeboxes(1), beams+1, 1)];
    tc = [repmat(boresight - beamsep .* beams_2, rb, 1);
	  [boresight - beamsep .* [beams_2:-1:-beams_2]'];
	  repmat(boresight + beamsep .* beams_2, rb, 1);
	  [boresight - beamsep .* [-beams_2:beams_2]']];

    [lon lat] = tc_d2ll(loc, tc, d, 'earthradius', defaults.earthradius);

    switch defaults.fov
     case 'patch'
      tmp = feval(patchfunc,  lon, lat, 'r', defaults.patchargs{:}, ...
		  'Parent', ah);
      
     case 'transpatch'
      defaults.patchargs{:}
      if defaults.ismmap
	% use m_map aware version, so that background patches are plotted on
	% the clipped background, not the axes. Create GPC polygons with
	% coordinates mapped from degrees to appropriate values
	
	tmp = gpc_m_transpatch(gpc_m_map(lon,lat), ah, ...
			       defaults.gpcalpha, ...
			       'facecolor', 'k', ...
			       'edgecolor', 'none', ...
			       defaults.patchargs{:});
      else
	tmp = gpc_transpatch(gpc(lon, lat), ah, defaults.gpcalpha, ...
			     'facecolor', 'k', ...
			     'edgecolor', 'none', ...
			     defaults.patchargs{:});
      end 
      
     case 'line' 
      % unlike patches lines must be explicitly closed back to the start
      lon = [lon(:); lon(1)];
      lat = [lat(:); lat(1)];
      tmp = feval(linefunc,  lon, lat, defaults.lineargs{:});
      
     otherwise
      error('Unknown style');
    end
    if ~isempty(tmp) & ~isequal(tmp, 0)
      r(n).fov = tmp;
    end
  end

  
end
