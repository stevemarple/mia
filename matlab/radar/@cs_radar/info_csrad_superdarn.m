function r = info_csrad_superdarn(in, vout, s, req, varargin)

r = vout;
r.validrequest = 1; % assume valid

switch req
  % these items just copy the data verbatim 
 case {'boresight' 'beamseparation'}
  r.varargout{1} = getfield(s, req);

 case 'beams'
  r.varargout{1} = 16;
  
 otherwise
  r.validrequest = vout.validrequest; % put back to original setting
end
  
