function r = eiscat
%EISCAT  Return a list of the EISCAT incoherent scatter radars.
%
%   r = EISCAT
%   r: vector of IS_RADAR objects
%
%   For more details about EISCAT see http://www.eiscat.no/
%
%   See also IS_RADAR.

r = [eiscat_t eiscat_s eiscat_k eiscat_vhf eiscat_esr];
