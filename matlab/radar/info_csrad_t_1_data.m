function r = info_csrad_t_1_data
%INFO_CSRAD_T_1_DATA  Return basic information about csrad_t_1
%
% This function is not intended to be called directly, use
% the INFO function to access data about csrad_t_1. To override
% the information given in this file see the instructions
% in INFO.
%
% This function was generated automatically by makesuperdarninstruments

r.abbreviation = 'sas';
r.beamseparation = 3.24;
r.boresight = 23.1;
r.code = 't';
r.latitude = 52.16;
r.location1 = 'Saskatoon';
r.location2 = 'Canada';
r.longitude = -106.53;
r.name = 'Saskatoon';
r.st_id = 5;

% end of function
