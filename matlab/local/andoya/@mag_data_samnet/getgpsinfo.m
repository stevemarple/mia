function r = getgpsinfo(mia)
%GETGPSINFO  Return GPS information from a MAG_DATA_SAMNET object
%
%  r = GETGPSINFO(mia);
%  r: 1xn CELL array of GPS information (mia scalar), or a CELL array of
%     CELL arrays if mia is non-scalar
%

if length(mia) == 1
  r = mia.gpsinfo;
else
  r = cell(size(mia));
  for n = 1:numel(mia)
    tmp = mia(n);
    r{n} = tmp.gpsinfo;
  end
end

