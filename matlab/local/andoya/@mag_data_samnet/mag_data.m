function [r, gps] = mag_data(mia)


r = repmat(mag_data, size(mia));
gps = cell(size(mia));
for n = 1:numel(mia)
  tmp = mia(n);
  r(n) = tmp.mag_data;
  gps{n} = tmp.gpsinfo;
end

if length(mia) == 1
  gps = gps{n};
end
