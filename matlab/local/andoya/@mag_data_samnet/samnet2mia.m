function mia = samnet2mia(miaIn, varargin)

mia = miaIn;
% load mag_data, get GPS information
[mia.mag_data mia.gpsinfo] = ...
    samnet2mia(mag_data(miaIn), varargin{:}, 'gps', 1);

