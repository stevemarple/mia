function r = nomodify(mia, varargin)
%NOMODIFY  Prevent data part of MAG_DATA_SAMNET object from being modified.
%
% This is a dummy function to prevent MAG_DATA_SAMNET objects from being
% modified. This is because modifications might require the GPS information
% part to be modified and the author is too lazy to write the code to do
% it. Convert the object to MAG_DATA if you wish to modify it. Use
% GETGPSINFO first if you need the GPS information.
%
% Functions such as SETDATA, SETRESOLUTION, EXTRACT and JOIN are symbolic
% links to this function.
%
% See also MAG_DATA, MAG_DATA_SAMNET, GETGPSINFO.

error(sprintf(['%s objects cannot be modified as this may require the ' ...
	       'GPS information to be modified, and the author is too ' ...
	       'lazy to write that code. If you need to modify %s ' ...
	       'objects then coonvert them to the mag_data class first'], ...
	      class(mia), class(mia)));

