function r = miaload(mia);
r = [];

st = getstarttime(mia);
et = getendtime(mia);
defaultres = timespan(3, 'h');
defaultres_s = gettotalseconds(defaultres);

if isempty(st)
  error('starttime must be defined');
end
if isempty(et)
  error('endtime must be defined');
end


conn = pqconnectdb(stpevents_connstr);
if conn == 0
  error('could not establish connection to database');
  return;
end

% staet/end time, quoted
stq = strftime(st, '''%Y-%m-%d %H:%M:%S+00''');
etq = strftime(et, '''%Y-%m-%d %H:%M:%S+00''');

sql = ['SELECT extract(epoch from (time-' stq ')) AS ts,kp,provisional ' ...
       'FROM kp WHERE ' ...
       'time >= ' stq  ' AND time < ' etq ...
       ' ORDER BY time asc'];


res1 = pqexec(conn, sql);
if res1 == 0 
  pqresulterrormessage (res1);
  error(sprintf('Exec failed\nSQL was: %s', sql));
end;

% map column names to numbers. Create a struct where fieldnames are column
% names and field values are the column numbers
anfields = pqnfields(res1);
col = [];
for n = 0:(anfields-1);
  col = setfield(col, pqfname(res1, n), n);
end

% find number of results
antuples = pqntuples(res1);

data = zeros(1, (et-st) ./ defaultres);
provisional = 0;
% iterate over all the results
for n = 0:(antuples-1);
  tmp_ts = pqgetvalue(res1, n, col.ts);
  tmp_kp = pqgetvalue(res1, n, col.kp);
  tmp_prov = pqgetvalue(res1, n, col.provisional);

  % matlab counts from 1, not 0
  data(1 + (tmp_ts ./ defaultres_s)) = tmp_kp;
  switch tmp_prov
    case {1, 't'}
     provisional = 1;
  end
end


pqresulterrormessage (res1);
pqclear(res1);  % always clear result when done

mia = setdata(mia, data);

res_in = getresolution(mia);
if isempty(res_in)
  res_in = defaultres;
end

% set the object to the resolution stored in the database
mia = setresolutionfield(mia, defaultres);
mia = settimingfield(mia, 'offset');

% now request the resolution to be adjusted
mia = setresolution(mia, res_in);

pqfinish(conn);

r = mia;
