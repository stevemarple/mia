function r = defaultmakeplotfiglogo
%DEFAULTMAKEPLOTFIGLOGO The default logo used by MAKEPLOTFIGLOGO for Lancaster.
%
%   r = DEFAULTMAKEPLOTFIGLOGO
%   r: CHAR or FUNCITON_HANDLE
%
% r is always 'andoya'
%
% See also MAKEPLOTFIGLOGO, MAKEPLOTFIG.

r = 'asc_logo';

