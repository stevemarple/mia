TIME_START = timestamp('now')
tempdir
rio = rio_and_2

% nominal start time (ie beginning of period for which QDC is valid
t = timestamp([YYYY MM DD 0 0 0]);

% start time of data used to make QDC
st = timestamp([YYYY MM DD 0 0 0]);

% end time of data used to make QDC
et = t + timespan(14, 'd');
et = timestamp([YYYY MM DD 0 0 0]);
et

mia = [];

% example of how to exclude certain periods
% exclude = ...
%     [timestamp([2008 12 04 10 0 0]) timestamp([2008 12 04 18 0 0]); ...
%      timestamp([2008 12 08 16 0 0]) timestamp([2008 12 12 00 0 0]); ...
%      timestamp([2008 12 12 22 0 0]) timestamp([2008 12 13 00 0 0]); ...   
%     ];

exclude = [];

exclude

% beams to make QDC for
beams = info(rio, 'allbeams');
beamsatonce = 13;

disp(sprintf('Creating for beam(s) %s, with %d beams at once', ...
	     printseries(beams), beamsatonce));

pause(5);

prefilter = {};
postfilter = {};

prefilter{1} = mia_filter_sliding_average('method', 'median', ...
					  'windowsize', timespan(599, 's'));

if 0
  stddev = 4;
  prefilter{2} = mia_filter_qdc_mean_sd('upperlim', stddev, ...
					'lowerlim', -stddev, ...
 					'dcadjustmethod', 'minimisesigndiff');
end

% Using mean/std deviation filtering can lead to NaNs in the final QDC,
% replace them with interpolated values before attempting to use the
% truncated Fourier series fit
postfilter{1} = mia_filter_replace_nans('interpmethod', 'cubic');





% Don't set the fitorder for the Fourier transform, compute automatically
% considering the beamwidth, latitude, zenith etc
fitorder = [];

% The method used to create the QDC, either a CHAR or a CELL matrix
% containg the name of the function and any optianl parameters to pass to
% that function. 
% 
% UPPERENVELOPETOQDC accepts a name/value pair "outputrows" which selects
% which sorted rows should be output for the QDC data. [3 4] means use
% the mean of 3rd and 4th highest values at any given sidereal time.
outputrows = [3 4];

% Give the user a chance to see the outputrows setting
display(outputrows);
pause(2); 

createmethod = {'upperenvelopetoqdc', ...
		'outputrows', outputrows};


% Now call the makeqdc function with the appropriate parameters
makeqdc(rio, ...
        'mia', mia, ...
	'time', t, ...
	'starttime', st, ...
	'endtime', et, ...
	'beams', beams, ...
        'beamsatonce', beamsatonce, ...
	'exclude', exclude, ...
	'prefilter', prefilter, ...
	'postfilter', postfilter, ...
	'fitorder', fitorder, ...
	'createmethod', createmethod, ...
        'savepathsuffix', '.new');

TIME_END = timestamp('now')
TIME_TAKEN = TIME_END - TIME_START
disp('DONE');
