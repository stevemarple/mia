function startup
%STARTUP Local startup function for Polar Research Institute of China.

% windows fixes
% if strcmp(computer, 'PCWIN')
%   system_dependent RemotePathPolicy TimecheckDirFile;
%   system_dependent RemoteCWDPolicy  TimecheckDirFile;
% end

% Ensure the necessary paths are in place
if isunix 
  p = {'/usr/local/toolbox/ephem' ...
       '/usr/local/toolbox/m_map'};
  for n = 1:numel(p)
    if isdir(p{n})
      addpath(p{n});
    end
  end
end

