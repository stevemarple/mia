function h = fast_samnet_logo(ah)

if length(ah) ~= 1
  error('require a single axes handle');
elseif ~ishandle(ah)
  error('not a handle')
elseif ~strcmp(get(ah, 'Type'), 'axes')
  error('not an axes handle')
end

% find existing logo if it exists
ah2 = findobj(0, 'Type', 'axes', 'Tag', 'fast_samnet_logo');
if isempty(ah2)
  % create a figure
  disp('creating new log figure');
  fh = figure('Visible', 'off' ,'IntegerHandle', 'off')
  [h ah2 fh] = samnet_logo(axes('Parent', fh, 'Tag', 'fast_samnet_logo'));
end


ch = flipud(allchild(ah2));
h = copyobj(ch, ah);
