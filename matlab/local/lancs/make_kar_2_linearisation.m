function make_kar_2_linearisation(varargin)
rio = rio_kar_2

defaults.filename = fullfile(mia_basedir, 'riometer', ...
							 ['linearise_rio_table_' getcode(rio) '.mat']);

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

linearisetable = struct();
linearisetable.instrument = rio;


%%% Compute initial linearisation data

debug = true;
% 2018-09-20
%
% The calibration run commenced at 16:57ut with the output of the signal
% generator switched off for 1 minute. At 16:38 [CORRECTION: 16:58] the generator was on at
% -130dBm CW and then stepped in 1dBm steps every minute up to a level of
% -98dBm CW at 17:30ut.
%
% The RF output was switched off from 17:31ut until 17:32ut when it was
% switched back on again at a level of -100dBm CW. The level was then
% reduced to -110dBm CW at 17:33 and still currently running at this level.


linearisetable(1).starttime = timestamp('bad'); % Not defined
linearisetable(1).endtime = timestamp('bad');

% Amount of time to discard at start/end of period
guardtime = timespan(10, 's');

st = timestamp([2018 09 20 16 58 00]);
dbm_start = -130;

% Start time of last measurement
et = timestamp([2018 09 20 17 30 00]);
dbm_end = -98;

ts = timespan(1, 'm');
dbm_step = 1;


mia = rio_rawpower('starttime', st, ...
				   'endtime', et + ts, ...
				   'instrument', rio)

beams = getbeams(mia)


t1 = st;
dbm = dbm_start;

s = {};
linearisetable(1).x = zeros(numel(beams), 0);
linearisetable(1).y = zeros(numel(beams), 0);
calibnum = 1;
while t1 <= et
  t2 = t1 + ts;
  for beam = beams
	beamnum = beam; % Assume direct mapping
	
	data = getdata(extract(mia, ...
						   'starttime', t1 + guardtime, ...
						   'endtime', t2 - guardtime));
	if debug
	  % Enable for debugging
	  d = [];
	  d.time = isotime(t1);
	  d.dbm = dbm;
	  d.beam = beam;
	  d.count = size(data, 2);
	  d.mean = mean(data, 2);
	  d.std = std(data, 0, 2);
	  d.median = median(data, 2); 
	  d.min = min(data, [], 2);
	  d.max = max(data, [], 2);
	  s{end+1} = sprintf(['%s:\n' ...
						  '       Beam: %d' ...
						  '        dBm: %d' ...
						  '      Count: %d' ...
						  '       Mean: %.3f' ...
						  '        Std: %.3f' ...
						  '     Median: %.3f' ...
						  '        Min: %.3f' ...
						  '        Max: %.3f'], ...
						 d.time, d.beam, d.dbm, ...
						 d.count, d.mean, d.std, d.median, ...
						 d.min, d.max);
	end
	linearisetable(1).x(beamnum, calibnum) = mean(data, 2); % Value in volts
	linearisetable(1).y(beamnum, calibnum) = dbm;
  end

  t1 = t2;
  dbm = dbm + dbm_step;
  calibnum = calibnum + 1;
end

for n = 1:numel(s)
  disp(s{n})
end

if dbm ~= dbm_end + dbm_step
  dbm_end + dbm_step
  dbm
  error('Unexpected dBm value at the end')
end

%%% Add any other linearisation code here
% linearisetable(2).instrument = rio
% ...


% Save all data
save(defaults.filename, 'linearisetable');
disp(sprintf('Saved linearisetable to %s', defaults.filename));


