function r = info_local(in, vout, s, req, varargin)
%INFO_LOCAL  Localisation function for INFO (for Lancaster University)

r = vout;


val = 1;
switch req
 
 case 'defaultfilename'
  if ~isempty(r.varargout{1})
    % Convert URL to a local filename
    if isfield(r.varargout{1}, 'fstr')
      
      if isa(in, 'magnetometer')
	r.varargout{1}.fstr = ...
	    strrep(r.varargout{1}.fstr, ...
		   'http://spears.lancs.ac.uk/miadata/magnetometer/awn', ...
		   fullfile(mia_datadir, 'aurorawatch', 'net'));

	
	
	r.varargout{1}.fstr = ...
	    strrep(r.varargout{1}.fstr, ...
		   'http://www.dcs.lancs.ac.uk/iono/miadata/magnetometer', '');
      end
      
      r.varargout{1}.fstr = ...
	  strrep(r.varargout{1}.fstr, ...
		 'http://www.dcs.lancs.ac.uk/iono/miadata', mia_datadir);
    
      r.varargout{1}.fstr = ...
	  strrep(r.varargout{1}.fstr, ...
		 'http://spears.lancs.ac.uk/miadata', mia_datadir);

    end
  end
  
 otherwise
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
