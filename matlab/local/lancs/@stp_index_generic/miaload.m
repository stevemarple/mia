function r = miaload(mia);
r = [];

d = index_data(mia);

st = getstarttime(mia);
et = getendtime(mia);
defaultres = d.resolution;
defaultres_s = gettotalseconds(defaultres);

if isempty(st)
  error('starttime must be defined');
end
if isempty(et)
  error('endtime must be defined');
end

url = [strftime(st, d.url{1}) strftime(et, d.url{2})]

tmpfile1 = tempname;
cmd1 = sprintf('wget ''%s'' -O %s', url, tmpfile1);
system(cmd1);

tmpfile2 = tempname;
cmd2 = sprintf(['awk -F''[ ,:-]'' ''(NR >= 6){print' ...
		' $1,$2,$3,$4,$5,$6,$7}'' %s > %s'], ...
	       tmpfile1, tmpfile2);
system(cmd2);


% cmd = sprintf(['wget ''%s'' -O - | awk -F''[ ,:-]'' ''(NR >= 6){print' ...
% 	       ' $1,$2,$3,$4,$5,$6,$7}'' > %s'], ...
% 	      url, tmpfile)

% system(cmd);

% tmpdata = load(tmpfile2);
[tmp tmpfile2_basename] = fileparts(tmpfile2);
load(tmpfile2);
eval(['tmpdata=' tmpfile2_basename ';']);


data = repmat(nan, 1, (et-st) ./ defaultres);

for n = 1:size(tmpdata, 1);
  t = timestamp(tmpdata(n, 1:6));
  idx = 1 + ((t - st) ./ defaultres);
  data(idx) = tmpdata(n, 7);
end


r = mia;
r = setdata(r, data);


% get units
tmpfile3 = tempname;
cmd3 = sprintf('head -n 3 %s | tail -n 1 | cut -d, -f2 > %s', ...
	       tmpfile1, tmpfile3);
system(cmd3);

% u = textread(tmpfile3, '%s');
% u = u{1};

[fid mesg] = fopen(tmpfile3);
if fid == -1 
  error(sprintf('could not open %s: %s', tmpfile3, mesg));
else
  u = fgetl(fid);
  fclose(fid);
end


if strcmp(u, '-')
  u = '';
end
r = setunits(r, u);

% fix resolution
res_in = getresolution(mia);
if isempty(res_in)
  res_in = defaultres;
end

% set the object to the resolution stored in the database
r = setresolutionfield(r, defaultres);
r = settimingfield(r, 'offset');

% now request the resolution to be adjusted
r = setresolution(r, res_in);

