function [r, fh, gh] = makeaurorawatchqdc(varargin)
%MAKEAURORAWATCHQDC  Make a QDC for AuroraWatch.
%
%   [r, fh, gh] = MAKEAURORAWATCHQDC(...)
%   r: the quiet-day curve (MAG_QDC_FFT)
%   fh: FIGURE handle (if plot made, empty otherwise)
%   gh: AXES handle (if plot made, empty otherwise)
%
% Default behaviour can be modified with the following parameter name/value
% pairs:
%
% 'time', TIMESPAN
%   Any time within the month for which the QDC is to be made.
%
% 'mia', MAG_DATA
%   Use this data to produce the QDC, otherwise load data.   
%
% 'starttime', TIMESTAMP
%   If data not provided use this as start time.
%
% 'endtime', TIMESTAMP
%   If data not provided use this as end time.
%
%
% 'instrument', TIMESTAMP
%   If data not provided use this instrument.
%
% 'resolution', TIMESPAN
%   Resolution of the finished QDC (default 5s).
%
% 'method', ['qdays' | 'median']
%   Method used in generating the QDC. 'qdays' is the default and uses
%   the quietest n days. 'median' creates the QDC from the median of all
%   selected days.
%
% 'nquiet', NUMERIC
%   Number of quiet days used when the 'qdays' methd is
%   selected. Defaults to 5.
%
% 'fitorder', NUMERIC
%   Order of the Fourier-fitted curve. Default is 5. See MAG_QDC_FFT.
%
% 'filename', CHAR
%   STRFTIME-like format specifier giving the file the QDC should be saved
%   to. %q is replaced by the 2 letter code and %Q is replaced by the 3
%   letter SAMNET code (both lower case). Similarly for %z and %Z, but upper
%   case. %n for serial number. Time used is the start of the month.
%
% 'makedir', LOGICAL
%   Create any directories needed to when writing to filename. Default is
%   true.
%
% 'plot', LOGICAL
%   Produce a plot of the QDC.
%
% See also MAG_QDC, MAG_QDC_FFT.

r = [];
fh = [];
gh = [];

% any time within the month for which the QDC is to be made
defaults.time = []; % defaults to current month if empty

% allow calling function to provide data, otherwise it is loaded
% automatically
defaults.mia = [];

% if data not provided require some parameters to load it
% for specific start/end times (otherwise uses start/end of selected month)
defaults.starttime = [];
defaults.endtime = [];
defaults.instrument = [];

% resolution of the finished QDC
defaults.resolution = timespan(5, 's');

% parameters affecting the fit
% methd used, valid methods are 'qdays' and 'median'.
defaults.method = 'qdays'; 
defaults.nquiet = 5; % number of quiet days used by the qdays method
defaults.fitorder = 5; % order of the Fourier-fitted curve

% modified strftime format string, where %q is replaced by the 2 letter code
% and %Q is replaced by the 3 letter SAMNET code (both lower
% case). Similarly for %z and %Z, but upper case. %n for serial number.

% defaults.filename = '/samnet/activity/quiet/%Y/%q00%m%Y.5s.prelim';
defaults.filename = '';
defaults.makedir = 1;

% Produce a plot of the QDC. Used for automated processing so that the
% preliminary QDC can be validated.
defaults.plot = 0;


[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.time)
  defaults.time = timestamp('now');
end

if isempty(defaults.starttime)
  % use start of month selected month
  defaults.starttime = timestamp([datevec(defaults.time, 1:2) 1 0 0 0]);
end

if isempty(defaults.endtime)
  % use start of month after starttime
  defaults.endtime = ...
      defaults.starttime + timespan(daysinmonth(defaults.starttime), 'd');
end

% ensure mia object starts/end at midnight
day = timespan(1, 'd');
defaults.starttime = getdate(defaults.starttime);
defaults.endtime = ceil(defaults.endtime, day);

if isempty(defaults.mia)
  if isempty(defaults.instrument)
    error('instrument required');
  end
  % Load data one day at a time then insert them all together afterwards. This
  % approach enables data to be loaded from either preliminary or final data
  % archives, but not mixed (which is much slower).
  if strcmpi(getfacility(defaults.instrument), 'AuroraWatchNet')
    loadoptions = {'failiffilemissing', 0, ...
		   'multiplearchives', 0, ...
		   'resolution', timespan(1, 'm')};		   
  else
    loadoptions = {'failiffilemissing', 0, ...
		   'multiplearchives', 0, ...
		   'resolution', defaults.resolution, ...
		   'archive', {'5s', 'preliminary'}};
  end
  
  c = {};
  t1 = defaults.starttime;
  while t1 < defaults.endtime
    t2 = t1 + day;
    c{end+1} = mag_data('starttime', t1, ...
                        'endtime', t2, ...
                        'instrument', defaults.instrument, ...
                        'loadoptions', loadoptions);
    t1 = t2;
  end
  % Use the efficient method to join many MIA objects together.
  defaults.mia = insertmany(c{:})
  
else
  % read times from mia object, but ensure object starts/end at midnight
  defaults.starttime = getstarttime(defaults.mia);
  defaults.endtime = getendtime(defaults.mia);

  defaults.starttime = ceil(defaults.starttime, day);
  defaults.endtime = floor(defaults.endtime, day);

  defaults.mia = extract(defaults.mia, ...
			 'starttime', defaults.starttime, ...
			 'endtime', defaults.endtime)

end

if ~isempty(defaults.resolution)
  defaults.mia = setresolution(defaults.mia, defaults.resolution);
  defaults.instrument = getinstrument(defaults.mia);
end

% Space regularly
defaults.mia = spaceregularly(defaults.mia, ...
                              'resolution', defaults.resolution);

ndays = getduration(defaults.mia) ./ day;
nsamp = day ./ defaults.resolution;

H = getdata(defaults.mia, getparameterindex(defaults.mia, 'H'), ':');
D = getdata(defaults.mia, getparameterindex(defaults.mia, 'D'), ':');
Z = getdata(defaults.mia, getparameterindex(defaults.mia, 'Z'), ':');

H = reshape(H, nsamp, ndays)';
D = reshape(D, nsamp, ndays)';
Z = reshape(Z, nsamp, ndays)';

switch defaults.method
  case 'qdays'
   % subtract monthly mean
   mmH = nonanmean(H(:));
   mmD = nonanmean(D(:));
   mmZ = nonanmean(Z(:));

   dH = H - mmH;
   dD = D - mmD;
   dZ = Z - mmZ;

   % estimate daily activity based on RMS departure from monthly mean
   actH = sqrt(nonanmean(dH.^2, 2));

   % don't use days with less than 75% data.
   nancount = sum(isnan(dH),2);
   actH(nancount > 0.25 * nsamp) = inf;

   % sort into ascending order of activity
   [actH,dayH] = sort(actH);
   qdays = dayH(1:defaults.nquiet);
   % nancount(qdays)

   % generate unfiltered qdc
   ufqdcH = nonanmean(dH(qdays, :), 1);
   ufqdcD = nonanmean(dD(qdays, :), 1);
   ufqdcZ = nonanmean(dZ(qdays, :), 1);

   % count of data points
   countH = sum(~isnan(dH(qdays, :)));

   % do Fourier series fit
   %    fitH = localFourierFit(ufqdcH, defaults.fitorder);
   %    fitD = localFourierFit(ufqdcD, defaults.fitorder);
   %    fitZ = localFourierFit(ufqdcZ, defaults.fitorder);
   
   % add monthly mean back again
   ufqdcH = ufqdcH + mmH;
   ufqdcD = ufqdcD + mmD;
   ufqdcZ = ufqdcZ + mmZ;

   %    fitH = fitH + mmH;
   %    fitD = fitD + mmD;
   %    fitZ = fitZ + mmZ;
   
   integrationtime = ...
       repmat(nonanmedian(getintegrationtime(defaults.mia)) ...
	      * defaults.nquiet, 1, nsamp);
   
   
   processing{1} = sprintf('created by %s with method %s (qdays=%d)', ...
			   mfilename, defaults.method, defaults.nquiet);

   qdates = defaults.starttime + ...
	    timespan(dayH(1:defaults.nquiet) - 1, 'd');
   
   qdateStr = strftime(qdates, '%Y-%m-%d'); % cell array assuming nquiet>1
   if iscell(qdateStr)
     processing{2} = ['Quiet days: ' commify(qdateStr{:})];
   else
     processing{2} = ['Quiet day: ' qdateStr];
   end
   
 case 'median'
   % generate unfiltered qdc
   ufqdcH = nonanmedian(H,1);
   ufqdcD = nonanmedian(D,1);
   ufqdcZ = nonanmedian(Z,1);
   
   % do Fourier series fit
   %    fitH = localFourierFit(ufqdcH, defaults.fitorder);
   %    fitD = localFourierFit(ufqdcD, defaults.fitorder);
   %    fitZ = localFourierFit(ufqdcZ, defaults.fitorder);   
   
   
   integrationtime = repmat(nonanmedian(getintegrationtime(defaults.mia)), ...
			    1, nsamp);
   
   processing = sprintf('created by %s with method %s', mfilename, ...
			defaults.method);

 otherwise
  error(sprintf('unknown method (was ''%s'')', defaults.method));
end

sampletime = defaults.starttime + defaults.resolution*[0.5:nsamp];


tmpqdc = mag_qdc('starttime', defaults.starttime, ...
		 'endtime', defaults.starttime+day, ...
		 'qdcstarttime', defaults.starttime, ...
		 'qdcendtime', defaults.endtime, ...
		 'sampletime', sampletime, ...
		 'integrationtime', integrationtime, ...
		 'instrument', defaults.instrument, ...
		 'data', [ufqdcH; ufqdcD; ufqdcZ], ...
		 'components', getcomponents(defaults.mia), ...
		 'units', getunits(defaults.mia), ...
		 'processing', processing, ...
		 'load', 0);

r = mag_qdc_fft(tmpqdc, ...
		'preserveoriginalqdc', 1, ...
		'fitorder', defaults.fitorder);

% plot results
if logical(defaults.plot)
  [fh gh] = plot(r, ...
		 'watermark', 'Preliminary');
end



% year = datevec(defaults.time, 1);
% month = datevec(defaults.time, 2);
% fprintf('QDC will be marked as being for month %04d/%02d\n', year, month);


% report on how much data was used
missingDataComment = sprintf('Proportion of missing data was %.1f%%', ...
			     (sum(isnan(ufqdcH)) ./ numel(ufqdcH)) * 100);
disp(missingDataComment);
defaults.mia = addprocessing(defaults.mia, missingDataComment);


if isempty(defaults.filename)
  % defaults.filename = /samnet/activity/quiet/2013/aw00012013.5s.prelim
  df = info(defaults.instrument, 'defaultfilename', 'mag_qdc');
  defaults.filename = [df.fstr '.prelim'];
end

localWriteQDC(r, defaults); 

data = defaults.mia;


% function fit = localFourierFit(x,n)
% % do the FT
% ft = fft(x);
% % remove unwanted terms
% ft((n+2):(end-n)) = 0;
% % do IFT (result should be purely real, but rounding error spoils this!)
% fit = real(ifft(ft));


function localWriteQDC(mia, defaults)
if isempty(defaults.filename)
  return 
end

if ~isequal(getcomponents(mia), {'H', 'D', 'Z'})
  error('components must be H, D, Z');
end

% enable special format specifiers to be used for instrument code
scode = samnetcode(defaults.instrument);
s.q = lower(scode);
s.Q = lower(getabbreviation(defaults.instrument));
s.z = upper(s.q);
s.Z = upper(s.Q);
s.n = num2str(getserialnumber(defaults.instrument));
filename = strftime(defaults.time, defaults.filename, ...
		    'additionalreplacements', s);

% figure out if gzip compression should be used
[Path fname ext] = fileparts(filename);
if strcmp(ext, '.gz')
  filename = fullfile(Path, fname); % remove .gz from saved file
  gzip = 1;
else
  gzip = 0;
end

if logical(defaults.makedir)
  url_mkdir(fileparts(filename));
end
    
[fid mesg] = fopen(filename, 'w');
if fid == -1
  error(sprintf('cannot open %s: %s', filename, mesg));
end

year = datevec(defaults.time, 1);
month = datevec(defaults.time, 2);

% get data and convert from T to nT
data = getdata(mia);
if strcmp(getunits(mia), 'T')
  data = data * 1e9;
else
  error(sprintf('data not in T (was ''%s'')', getunits(mia)));
end


for n = 1:size(data, 2)
  if mod(n-1, 720) == 0
    fprintf(fid, '%s00/%02d/%04d %02d0000 05 aver\n', ...
	    lower(scode), month, year, (n-1) / 720);
  end
  fprintf(fid, '%.1f %.1f %.1f\n', data(:, n));
end
fclose(fid);


% write out the comment file
[fid mesg] = fopen([filename '.log'], 'w');
if fid == -1
  disp(sprintf('could not open %s: %s (ignored)', [filename '.log'], mesg));
else
  fprintf(fid, '%s\n', char(mia));
  fclose (fid);
end

if gzip
  system(['gzip -f ' systemquote(filename)]);
  filename = [filename ext];
end
disp(['saved QDC to ' filename]);

