function r = url_mangle(url, func)
%URL_MANGLE Mangle URLs when opening/saving or fetching/putting files.
%
%   r = URL_MANGLE(url, func)
%   r: mangled URL (CHAR)
%   url: original URL (CHAR)
%   func: function name (url_load, url_save, url_exist, url_fopen, fclose
%   or url_mkdir)
%
% URL_MANGLE is not intended to be called directly. The urltools functions
% which open, save, fetch or put files should automatically call
% URL_MANGLE if it exists on the Matlab PATH.
%
% This version of URL_MANGLE is intended for use at Lancaster University
% computers which do not have disk access to the STP data archive.
%
% See URL_TOOLS, URL_LOAD, URL_SAVE, URL_FOPEN, URL_EXIST, FCLOSE.

r = url;
switch func
 case {'url_load' 'url_fopen' 'url_exist' 'url_fetch' 'url_imread'}
  
  % apache doesn't have read access to ram_2, ram_3 or and_2 data
   r = strrep(r, ...
	      'http://www.dcs.lancs.ac.uk/iono/miadata/riometer/ram_2', ...
	      'scp://dcs-tesla:/data/riometer/ram_2');
   r = strrep(r, ...
	      'http://www.dcs.lancs.ac.uk/iono/miadata/riometer/ram_3', ...
	      'scp://dcs-tesla:/data/riometer/ram_3');
   r = strrep(r, ...
	      'http://www.dcs.lancs.ac.uk/iono/miadata/riometer/and_2', ...
	      'scp://dcs-tesla:/data/riometer/and_2');
  
  
  
 case {'url_save' 'fclose' 'url_mkdir' 'url_put' 'url_imwrite'}
  % cannot use a HTTP PUT to the data archive, so replace with a SCP instead
  r = strrep(r, ...
             'http://www.dcs.lancs.ac.uk/iono/miadata/', ...
             'scp://dcs-tesla:/data/');
  
  r = strrep(r, ...
             'http://www.dcs.lancs.ac.uk/summary/gaia/', ...
             'scp://dcs-tesla:/summary/gaia/');

 otherwise
  warning(sprintf('do not know about URL mangling for function %s', func));
  
end
