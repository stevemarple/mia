function startup
%STARTUP Local startup function for Lancaster.

% windows fixes
% if strcmp(computer, 'PCWIN')
%   system_dependent RemotePathPolicy TimecheckDirFile;
%   system_dependent RemoteCWDPolicy  TimecheckDirFile;
% end

% Ensure the necessary paths are in place
if isunix 
  p = {'/usr/local/toolbox/ephem' ...
       '/usr/local/toolbox/m_map' ...
        '/usr/local/toolbox/cgm' };
  for n = 1:numel(p)
    if isdir(p{n})
      addpath(p{n});
    end
  end
end

