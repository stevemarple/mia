function r = defaultmakeplotfiglogo
%DEFAULTMAKEPLOTFIGLOGO The default logo used by MAKEPLOTFIGLOGO for Lancaster.
%
%   r = DEFAULTMAKEPLOTFIGLOGO
%   r: CHAR or FUNCTION_HANDLE
%
% r is always 'lancasterunilogo'
%
% See also MAKEPLOTFIGLOGO, MAKEPLOTFIG.

r = 'lancs_uni_crest';

