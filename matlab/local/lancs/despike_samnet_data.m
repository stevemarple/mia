function despike_samnet_data(varargin)
%DESPIKE_SAMNET_DATA Atuomatically despike SAMNET data files
%
%   DESPIKE_SAMNET_DATA(...)
%
% DESPIKE_SAMNET_DATA automatically removes spikes from SAMNET data,
% typically the preliminary archive, and then saves the resulting data to
% the 1s archive. It also generates the 5s archive files using the
% external program "5s_convert". The default behaviour can be modified
% with the following name/value pairs:
%
%   'starttime', TIMESTAMP
%   The start time for which data should be processed
%   (inclusive). Required. Rounded down using timestamp/FLOOR to the
%   start of the day.
%
%   'endtime', TIMESTAMP 
%   The end time for which data should be processed (exclusive). If not set
%   defaults to one day after the start time. Rounded down using
%   timestamp/FLOOR to the start of the day.
%
%   'magnetometers', MAGNETOMETER
%
%   A list of magnetometers for which should be processed. If not given
%   default to MAG_SAMNET.
%
%   'unfilteredmagnetometers', MAGNETOMETER
%   A list of magnetometers which for which despiking should not be
%   performed. Defaults to mag_hll_1. Any magnetometers listed in
%   unfilteredmagnetometers are still processed, just the despiking stage is
%   not performed.
%
%   'deleteincoming', LOGICAL
%   'deletepreliminary', LOGICAL
%   Flags to indicate if the incoming and preliminary archive files
%   should be deleted after creation of the 1s and 5s archive files. 
%
%   'failiffilemissing', LOGICAL
%   If TRUE then a given instrument-day is not processed if any of the
%   incoming or selected archive files are missing.
%
%   'archive', CHAR
%   The archive from which to load the data. Defaults to 'preliminary'. 
%
%   'makepreliminary', LOGICAL
%   'Indicate if any missing preliminary data files should be
%   remade. Defaults to TRUE.
%
%   'reprocess', LOGICAL
%   If false new 1s and 5s data files will not be created if any 1s data
%   files already exist.
%
%   'filter', MIA_FILTER_BASE
%   The filter pbject used for the filtering. The default filter is a
%   MIA_FILTER_AS_DESPIKE object to remove spikes.
%
%   'plot', LOGICAL
%   PLOT the data values before and after filtering. Default is FALSE.
% 
%   'closefigures', LOGICAL
%   Flag indicating if the PLOT figures should be closed. If TRUE then
%   processing is halted using the PAUSE command; press any key to
%   close the window and continue processing.
%
%   'verbose', LOGICAL
%   Explain why data is not processed.
%
%   'usetrycatch', LOGICAL
%   By default try/catch exception handling is used to catch errors. For
%   easier debugging this behaviour can be turned off by setting
%   'usetrycatch' to FALSE.
%
% See also MAG_DATA, MAG_DATA_SAMNET, MIA2SAMNET, MAG_SAMNET,
% CHECK_SAMNET_DATA.

day = timespan(1, 'd');
defaults.starttime = [];
defaults.endtime = [];
defaults.magnetometers = []; % default to all SAMNET magnetometers
defaults.unfilteredmagnetometers = mag_hll_1;

defaults.deleteincoming = false;
defaults.deletepreliminary = false;

defaults.failiffilemissing = true;
defaults.archive = 'preliminary';
defaults.makepreliminary = true;

% Unless true don't process days for which 1s data exists
defaults.reprocess = false;
defaults.filter = mia_filter_as_despike('removalmethod', 'linear', ...
					'maximumspikelength', 2, ...
					'minimumspike', .4e-9, ...
					'windowsize', 4, ...
					'medianwindowsize', 61);

defaults.plot = false;
defaults.closefigures = false;
defaults.verbose = false;
defaults.usetrycatch = true;

[defaults unvi] = interceptprop(varargin, defaults);
warnignoredparameters(varargin{unvi(1:2:end)});

if isempty(defaults.starttime)
  error('starttime must be given');
else
  defaults.starttime = floor(defaults.starttime, day);
end

if isempty(defaults.endtime)
  defaults.endtime = defaults.starttime + day;
end

if isempty(defaults.magnetometers)
  mags = mag_samnet;
else
  mags = defaults.magnetometers;
end

% Ensure list is unique (no point in trying one multiple times)
mags = unique(mags);

% Now eliminate any which are not operational at all during the selected
% times
notOp = find(~isoperational(mags, defaults.starttime, defaults.endtime));
for n = notOp
  disp(sprintf('ignoring %s: not operational during %s', ...
               getcode(mags(n), true), ...
               dateprintf(defaults.starttime, defaults.endtime)));
end
mags(notOp) = [];

validarchives = {'preliminary', 'incoming', 'realtime2'};
if ~any(strcmp(validarchives, defaults.archive))
  error(sprintf('archive must be one of: %s', ...
		join(', ', validarchives)));
end

t = defaults.starttime;
while t < defaults.endtime
  disp(strftime(t, 'Processing %Y-%m-%d'));
  t2 = t + day;
  
  for n = 1:numel(mags)
    mag = mags(n);
    
    incomingAvailability = localGetDailyAvailability(mag, t, 'incoming');
    archiveAvailability = localGetDailyAvailability(mag, t, defaults.archive);
    oneSecAvailability = localGetDailyAvailability(mag, t, '1s');

    if logical(defaults.makepreliminary) ...
	  & strcmp(defaults.archive, 'preliminary') ...
	  & archiveAvailability == 0
      % No preliminary files, try remaking
      cmd = sprintf(['make_prelim_data -m %s -s %s'], ...
		    samnetcode(mag), ...
		    strftime(t, '%Y-%m-%d'));
      [status, mesg] = system(cmd); % try, ignore any errors
      % Now recompute the archiveAvailability
      archiveAvailability = localGetDailyAvailability(mag, t, ...
						      defaults.archive);
    end
    
    if ~isoperational(mag, t, t2)
      localExplain(mag, t, defaults, 'not operational');
      continue;
    elseif logical(defaults.failiffilemissing) & incomingAvailability ~= 1 
      % skip if incoming is incomplete (unless told otherwise)
      localExplain(mag, t, defaults, ...
		   'incoming archive not complete (failiffilemissing=1)');
      continue;
    elseif logical(defaults.failiffilemissing) & archiveAvailability ~= 1 
      % skip if selected archive is incomplete (unless told otherwise)
      localExplain(mag, t, defaults, ...
		   sprintf(['%s archive not complete ' ...
		    '(failiffilemissing=1)'], ...
			   defaults.archive));
      continue;
    elseif archiveAvailability == 0
      % no data at all for the selected archive
      localExplain(mag, t, defaults, ...
		   sprintf('%s archive empty', defaults.archive));
      continue;
    elseif ~logical(defaults.reprocess) & oneSecAvailability == 1
      % All 1s files exist, reprocessing not enabled so go onto next
      % magnetometer
      localExplain(mag, t, defaults, ...
		   '1s files exist (reprocessing not enabled)');
      continue;
    end
    
    if defaults.usetrycatch
      try
	mia = mag_data_samnet('instrument', mag, ...
			      'starttime', t, ...
			      'endtime', t2, ...
			      'log', false, ...
			      'loadoptions', ...
			      {'archive', defaults.archive, ...
		    'failiffilemissing', logical(defaults.failiffilemissing)});
      catch
	localExplain(mag, t, defaults, ...
		     sprintf('loading data failed (%s)', lasterr));
	continue;
      end
      
    else
      mia = mag_data_samnet('instrument', mag, ...
			    'starttime', t, ...
			    'endtime', t2, ...
			    'log', false, ...
			    'loadoptions', ...
			    {'archive', defaults.archive, ...
		    'failiffilemissing', logical(defaults.failiffilemissing)});
      
    end
   
    if any(mag == defaults.unfilteredmagnetometers)
      % don't despike for this magnetometer
      mia2 = mia;
    else
      disp('despiking');
      mia2 = filter(defaults.filter, mia);
    end
    
    if logical(defaults.plot)
      [fh gh] = plot(mia, 'color', 'r');
      plot(mia2, 'plotaxes', gh);
      if logical(defaults.closefigures)
	pause
	close(fh);
      end
    end
    
    % Skip if no data
    if isdataempty(mia2) || ...
	  all(reshape(isnan(getdata(mia2)), [1 prod(getdatasize(mia2))]))
      localExplain(mag, t, defaults, 'no valid data');
      continue;
    end
    
    mia2samnet(mia2, 'archive', '1s');

    disp('making 5s data file')
    cmd = sprintf('5s_convert -s %s -m %s &', ...
		  strftime(t, '%Y-%m-%d'), samnetcode(mag));
    [status, mesg] = system(cmd);
    if status ~= 0
      disp('could not make 5s data: ');
      disp(['command was: ' cmd]);
      disp(['error was: ' mesg]);
    end
    

    if logical(defaults.deletepreliminary)
      deleteFilesFromArchive(mag, t, 'preliminary');
    end
    if logical(defaults.deleteincoming)
      deleteFilesFromArchive(mag, t, 'incoming');
    end
        
  end
  
  t = t2;
end


function r = localGetDailyAvailability(mag, t, archive)
df = info(mag, 'defaultfilename', 'mag_data', 'archive', archive);
day = timespan(1, 'd');
t = floor(t, day);
et = t + day;

found = 0;
count = 0;
while t < et
  t2 = t + df.duration;
  count = count + 1;
  filename = strftime(t, df.fstr);
  if isurl(filename) 
    if url_exist(filename)
      found = found + 1;
    end
  else
    d = dir(filename);
    if ~isempty(d) && d.bytes > 0
      found = found + 1;
    end
  end
  
  t = t2;
end

r = found / count;
% disp(sprintf('%s (%s): %f, %d, %d', getcode(mag), archive, r, found, count));

function deleteFilesFromArchive(mag, t, archive)
df = info(mag, 'defaultfilename', 'mag_data', 'archive', archive);
day = timespan(1, 'd');
t = floor(t, day);
et = t + day;

while t < et
  t2 = t + df.duration;
  
  filename = strftime(t, df.fstr);

  if ~isurl(filename)
    disp(sprintf('deleting %s', filename));
    delete(filename);
  end
  
  t = t2;
end

function localExplain(mag, t, defaults, reason)
if defaults.verbose
  disp(sprintf('%s %s: %s', ...
	       strftime(t, '%Y-%m-%d'), getcode(mag, true), reason));
end
