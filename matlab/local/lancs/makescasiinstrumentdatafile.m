function r = makescasiinstrumentdatafile

in = cam_scasi;
loc = getlocation(in);

r.abbreviation = getabbreviation(in);
r.bibliography = '';
r.comment = '';
r.datarequestid = [];
r.defaultheight = nan;
r.endtime = timestamp('bad');
r.facility_name = '';
r.facility_url = '';
r.facilityid = [];
r.groupids = [];
r.id = [];
r.latitude = getgeolat(loc);
[r.location1 r.location2] = getname(loc);
r.longitude = getgeolong(loc);
r.modified = timestamp('now');
r.name = getname(in);
r.piid = [];
r.pixels.raw.xmin = 1;
r.pixels.raw.xstep = 1;
r.pixels.raw.xmax = 375;
r.pixels.raw.ymin = 1;
r.pixels.raw.ystep = 1;
r.pixels.raw.ymax = 242;

r.resolution = timespan(nan, 's');
r.rulesoftheroad = '';
r.serialnumber = getserialnumber(in);
r.starttime = timestamp('bad');
r.url = 'http://www.dcs.lancs.ac.uk/iono/scasi/';
r.defaultfilename.camera_image.duration = timespan(1, 'd');
% irrelevant for ST-6 format if file missing
r.defaultfilename.camera_image.failiffilemissing = 0;
r.defaultfilename.camera_image.format = 'st'; % ST-6 format
r.defaultfilename.camera_image.fstr = ...
    fullfile('%Y', '%m', '%d', '%Y%m%d%H%M%S.st6');

r.defaultfilename.camera_image.loadfunction = 'scasi2mia_image';
r.defaultfilename.camera_image.savefunction = '';
r.defaultfilename.camera_image.size = [51 51 1];
r.limits = [];
