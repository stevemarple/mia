function [r,sr,s] = extract(mia, varargin)

[r,sr,s] = mag_data_extract(mia, varargin{:});

if ~isequal(getsampletime(mia), getsampletime(r))
  error(['cannot use extract on mag_data_samnet if sampletimes differ' ...
	 ' (too lazy to write the code, convert to mag_data instead)']);
end

