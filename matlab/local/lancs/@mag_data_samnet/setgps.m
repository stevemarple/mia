function r = setgps(mia, gps)
%SETGPS  Set the GPS information in a MAG_DATA_SAMNET object
%
%   r = SETGPS(mia, gps)
%   r: modified MAG_DATA_SAMNET object
%   mia: MAG_DATA_SAMNET object (scalar)
%   gps: GPS information (CELL)
%
%   The initial object must be scalar.

if length(mia) ~= 1
  error('input object must be scalar');
end
r = mia;
r.gpsinfo = gps;
