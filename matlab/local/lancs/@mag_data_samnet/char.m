function r = char(mia)
%CHAR  Convert a MAG_DATA_SAMNET object to a CHAR.
%
%   r = CHAR(mia)

% NB Make all objects derived from mia_base print a trailing newline
% character

if length(mia) > 1
  % do we print each one, or just the size and class? Let the base class
  % decide. 
  r = mia_base_char(mia);
  return;
end

r = sprintf(['%s' ...
	     'GPS info          : %s\n'], ...
 	    mag_data_char(mia), matrixinfo(mia.gpsinfo));


