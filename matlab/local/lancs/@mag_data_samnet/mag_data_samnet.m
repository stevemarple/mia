function mia = mag_data_samnet(varargin)
% MAG_DATA_SAMNET  Constructor for MAG_DATA_SAMNET class

cls = 'mag_data_samnet'; % our class name
parent = 'mag_data'; % our parent class

latestversion = 1;
mia.versionnumber = latestversion;
mia.gpsinfo = [];

if nargin == 0 | (nargin == 1 & isempty(varargin{1}))
  % default constructor
  p = feval(parent);
  mia = class(mia, cls, p);
  
elseif nargin == 1 & isa(varargin{1},cls)
  % copy constructor   
  mia = varargin{1};
  
elseif rem(nargin, 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % use name/parameter interface
  defaults = mia; % take on standard settings
  defaults.load = [];
  defaults.loadoptions = {};
  defaults.log = 1;
  [defaults unvi] = interceptprop(varargin, defaults);
  mia = rmfield(defaults, {'load' 'loadoptions' 'log'});
  
  % cannot warn about unknown parameters since it might be valid in a
  % parent class, so pass them all up and leave mag_data to warn about
  % anything left over
  p = feval(parent, varargin{unvi}, 'load', 0);
  mia = class(mia, cls, p);
  
  if isempty(defaults.load)
    defaults.load = isdataempty(p);
  end
  if defaults.load
    mia = loaddata(mia, defaults.loadoptions{:});
    if prod(getdatasize(mia)) == 0
      comps = info(getinstrument(mia), 'components');
      % want to overload setdata to stop setdata being called by things
      % like setresolution and extract (this is being lazy to avoid having
      % to overload setresolution and extract). Here we can use the
      % mag_data/setdata function to create an empty matrix of the
      % correct size
      mia.mag_data = setdata(mia.mag_data, ...
			     repmat(nan, length(comps), ...
				    getduration(mia) ./ getresolution(mia)));
    end
    if defaults.log
      disp('NOT LOGGING MAG_DATA_SAMNET!!!!!');
    end
  end

else
  errormsg = 'incorrect arguments : ';
  for i = 1:length(varargin);
    errormsg = [errormsg, class(varargin{i}), ' '];
  end
  error(errormsg);
end

% Ensure that the returned object is marked with the latest version
% number
mia.versionnumber = latestversion;
