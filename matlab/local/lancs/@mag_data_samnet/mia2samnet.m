function mia2samnet(mia, varargin)
%MIA2SAMNET  Save MAG_DATA_SAMNET in SAMNET format.
%
% An overloaded version of mag_data/MIA2SAMNET which automatically passes
% the gpsinfo.
%
% See mag_data/MIA2SAMNET for details.

[mia2 gpsinfo] = mag_data(mia);
mia2samnet(mia2, varargin{:}, ...
	   'gpsinfo', gpsinfo);
