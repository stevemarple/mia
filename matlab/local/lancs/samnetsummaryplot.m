function samnetsummaryplot(varargin)
%SUMMARYPLOT Make daily summary plots for SAMNET MAGNETOMETER data.
%
%   SUMMARYPLOT(...);
%
%   The following name/value pairs are recognised:
%
%   'starttime', <TIME object>:
%   The time to start plotting from.
%
%   'endtime', <TIME object>:
%   The last day to plot.
%
%   'closefigures'
%   By default all figures are closed after saving to disk. This option
%   can be used to override the normal behaviour.
%
% See also MAG_SAMNET, MAGNETOMETER.

day = timespan(1, 'd');

% See localMakeOdering to reproduce this
magLat.bor_1 = 53.786186;
magLat.crk_1 = 54.879871;
magLat.esk_1 = 52.912006;
magLat.far_1 = 60.915176;
magLat.gml_1 = 55.076286;
magLat.had_1 = 47.911934;
magLat.han_1 = 58.508537;
magLat.han_3 = 58.462811;
magLat.hll_1 = 64.767181;
magLat.kil_1 = 65.676277;
magLat.kvi_1 = 55.968204;
magLat.lan_1 = 51.335369;
magLat.ler_1 = 58.124619;
magLat.lnc_2 = 51.335369;
magLat.nor_1 = 61.398479;
magLat.nur_1 = 56.722229;
magLat.nur_3 = 56.712009;
magLat.ouj_2 = 60.765732;
magLat.oul_1 = 61.420975;
magLat.ups_2 = 56.411228;
magLat.yor_1 = 51.083714;



if nargin == 0
  % mfame();
  feval(mfilename, ...
	'starttime', floor(timestamp('now'), day) - day);
  return;
elseif isa(varargin{1}, 'timestamp')
  if nargin >= 2 & isa(varargin{2}, 'timestamp')
    % mfilename(timestamp, timestamp, ...);
    feval(mfilename, ...
	  'starttime', varargin{1}, ...
	  'endtime', varargin{2}, ...
	  varargin{3:end});
    return;
  else
    % mfilename(timestamp, ...);
    feval(mfilename, ...
	  'starttime', varargin{1}, ...
	  'endtime', varargin{1} + day);
    return;
  end
elseif rem(length(varargin), 2) == 0 & all(mia_ischar(varargin{1:2:end}))
  % mfilename(<parameter name/value pairs>)
else
  error('incorrect parameters');
end


defaults.starttime = [];
defaults.endtime = day;
defaults.resolution = timespan(2,'m');
defaults.resolutionmethod = 'nonanmean';

defaults.archive = {'1s' '5s' 'preliminary' 'realtime2'};


defaults.preliminary = 'Preliminary'; % text used for preliminary data
defaults.preliminarycolor = [1 .6 .6];

defaults.spacing = [0.15 0.15 0.05 0.05];
defaults.graphpos = [0 0.0 1 0.85];

defaults.stackplot = 1; % stack plot of all magnetometer data
defaults.individualplot = 1; % individual plot of 3 components

% plot ordering for stackplots
defaults.order = ''; % 'cgmlatitude'
defaults.orderdir = ''; % 'descending', ...


defaults.closefigures = 1;

defaults.print = 1; % print to a file
		    
% defaults.printsize = [576 432];
defaults.printsize = [576 576];
defaults.printresolution = 80;
defaults.visible = [];

% defaults.individualplotpathfstr = fullfile(mia_datadir, 'summary', ...
% 'magnetometer', 
defaults.individualplotfilefstr = '%Y%m%d.png';
defaults.individualplotsize = [450 600];

defaults.stackplotpathfstr = fullfile(mia_datadir, 'summary', ...
				      'magnetometer', 'samnet', '%Y', '%m');
defaults.stackplotfilefstr = '%Y%m%d.png';
defaults.stackplotsize = [450 600];
% 'landscape draws figures rotated through 90 degree
% defaults.paperorientation = 'portrait'; 
% defaults.paperunits = 'centimeters';
% defaults.paperposition = [0 0 24 18];


% File mode, owner and group (only used on unix)
defaults.owner = '';
defaults.group = 'samnet';
defaults.umask = 'ug=rw,o=r';

[defaults unvi] = interceptprop(varargin, defaults);


if isempty(defaults.starttime)
  error('starttime must be set');
end


% t = floor(defaults.starttime, day);
% t = getdate(defaults.starttime);
defaults.starttime = getdate(defaults.starttime);
if isa(defaults.endtime, 'timespan')
  defaults.endtime = defaults.starttime + defaults.endtime;
end
defaults.endtime = ceil(defaults.endtime, day);
% defaults.endtime = getdate(defaults.endtime);

plotduration = day;


if isempty(defaults.visible)
  if defaults.closefigures
    defaults.visible = 'off';
  else
    defaults.visible = 'on';
  end
end

watermark = {'' defaults.preliminary};

meant = mean([defaults.starttime defaults.endtime]);
mag = mag_samnet;

% arrange N-S geomagnetically
mLat = zeros(size(mag));
for n = 1:numel(mag)
  mLat(n) = getfield(magLat, getcode(mag(n)));
end

[tmp idx] = sort(mLat);
mag = mag(idx);
mag2 = mag; % in case of not eliminating ones for which no data exists

components = {'H', 'D', 'Z'};

if matlabversioncmp('>=', '6')
  xdisplay = get(0,'DefaultFigureXDisplay');
else
  % no PNG output for matlab < 5.1, and no method of finding out if a
  % display is present for matlab < 6
  xdisplay = []; 
end

logo = 'samnet_logo';

t = defaults.starttime;
while t < defaults.endtime
  t2 = t + plotduration;
  
  % if t < timestamp([2003 4 1 0 0 0])
  %   logo = '';
  % else
  %   logo = 'lancasterunilogo';
  % end
  
  % eliminate mags which do not have data
  if 1
    elimIdx = [];
    for n = 1:length(mag)
      if ~isoperational(mag(n), t, t2)
	disp(sprintf('%s not operational', getcode(mag(n))));
	elimIdx(end+1) = n;
      end
    end
    mag2 = mag;
    mag2(elimIdx) = [];
  end

  mia = [];
  for n = 1:length(mag2)
    try
      tmp = mag_data('starttime', t, ...
		     'endtime', t2, ...
		     'instrument', mag2(n), ...
		     'load', 1, ...
		     'log', 0, ...
		     'loadoptions', ...
		     {'failiffilemissing', 0, ...
		      'archive', defaults.archive, ...
		      'resolution', defaults.resolution, ...
		      'resolutionmethod', defaults.resolutionmethod});
      data = getdata(tmp);
    catch
      data = [];
    end


    if prod(size(data)) ~= 0 & ~all(isnan(data(:)))
      if isempty(mia)
	mia = tmp;
      else
	mia(end+1) = tmp;
      end
      if logical(defaults.individualplot)
	Path = strftime(t, ...
			fullfile(mia_datadir, 'summary', 'magnetometer', ...
				 sprintf('%s_%d', ...
					 getabbreviation(mag2(n)), ...
					 getserialnumber(mag2(n))), ...
				 '%Y', '%m'));
	mkdirhier(Path);
	title = maketitle(tmp, 'comment', 'Not for publication');
	title{2} = strftime(t, ['%Y-%m-%d @ ' ...
		    char(defaults.resolution) ...
		    ' res']);
	[fh gh lh prelimH] = plot(tmp, ...
				  'logo', logo, ...
				  'title', title, ...
				  'preliminary', defaults.preliminary);
	set(prelimH, 'Color', defaults.preliminarycolor);
	set(fh, 'ResizeFcn', '', ...
		'Resize', 'off', ...
		'PaperOrientation', 'portrait', ...
		'PaperPositionMode', 'auto', ...
		'Color', 'w', ...
		'Units', 'pixels', ...
		'Position', [80 80 defaults.individualplotsize], ...
		'Tag', 'powerplot');
	fn = fullfile(Path, strftime(t, defaults.individualplotfilefstr));
	if exist(fn)
	  delete(fn);
	end
	
	if 1
	  % do screen grab
	  disp(['saving to ' fn]);
	  % figure2imagefile(fh, fn);
	  figure2imagefile(fh, fn, ...
			   'Preliminary', int2str(any(ispreliminary(mia))));
	elseif ~isempty(xdisplay)
	  disp(['saving to ' fn]);
	  print(fh, '-dpng', sprintf('-r%g', defaults.printresolution), fn);
	else
	  % print function adds .ppm and does not provide anyway not to
	  tmp = [tempname '.ppm']; 
	  print(fh, '-dppm', sprintf('-r%g', defaults.printresolution), tmp);
	  disp(['saving to ' fn]);
	  [status message] = pnm2png(tmp, fn);
	  delete(tmp);
	end
	if defaults.closefigures
	  delete(fh);
	end
	if ~isempty(defaults.umask)
	  localSetFileMode(fn, defaults);
	end
      end

    else
      disp(sprintf('%s operational but has no valid data', getcode(mag2(n))));
    end
  end


  if ~isempty(mia) & logical(defaults.stackplot)
    Path = strftime(t, defaults.stackplotpathfstr);
    mkdirhier(Path);
        
    for cn = 1:length(components)
      % it is important that the stackplot is created at the correct
      % size (and is not resizable) otherwise the imagemap details
      % returned by stackplot will be incorrect
      [fh gh lh prelimH imap ordering] = ...
	  stackplot(mia, ...
		    'title', {'SAMNET magnetometer data' ...
		    [components{cn} ' component'], ...
		    strftime(t, '%Y-%m-%d'), ...
		    'Not for publication'}, ...
		    'watermark', {watermark{1+ispreliminary(mia)}}, ...
		    'watermarkcolor', defaults.preliminarycolor, ...
		    'label', 'upper(getabbreviation(in))', ...
		    'order', defaults.order, ...
		    'orderdir', defaults.orderdir, ...
		    'logo', logo, ...
		    'parameters', components{cn}, ...
		    'visible', defaults.visible, ...
		    'resizefcn', '', ...
		    'resize', 'off', ...
		    'Units', 'pixels', ...
		    'Position', [80 80 defaults.stackplotsize]);
      
      set(fh, 'Color', 'w', ...
	      'PaperOrientation', 'portrait', ...
	      'PaperPositionMode', 'auto');
      set(prelimH, 'Color', defaults.preliminarycolor);
      fn = fullfile(Path, [lower(components{cn}) ...
		    strftime(t, defaults.stackplotfilefstr)]);
      if exist(fn)
	delete(fn);
      end

      % add imagemap information to PNG file
      imapStr = '';
      for n = 1:length(ordering)
	tmpMag = getinstrument(mia(ordering(n)));
	imapStr = sprintf('%s%s_%d %d %d %d %d;', imapStr, ...
			  getabbreviation(tmpMag), ...
			  getserialnumber(tmpMag), imap{n});
      end
      
      if 1
	% do screen grab
	disp(['saving to ' fn]);
	figure2imagefile(fh, fn, ...
			 'Preliminary', int2str(any(ispreliminary(mia))), ...
			 'ImageMap', imapStr);
      elseif ~isempty(xdisplay)
	disp(['saving to ' fn]);
	print(fh, '-dpng', sprintf('-r%g', defaults.printresolution), fn);
      else
	% print function adds .ppm and does not provide anyway not to
	tmp = [tempname '.ppm']; 
	print(fh, '-dppm', sprintf('-r%g', defaults.printresolution), tmp);
	disp(['saving to ' fn]);
	[status message] = pnm2png(tmp, fn);
	delete(tmp);
      end
      if defaults.closefigures
	delete(fh);
      end
      if ~isempty(defaults.umask)
	localSetFileMode(fn, defaults);
      end

    end
  end
  t = t2;
end


% A function to produce the ordering in geomagnetic latitude. Have the
% values hardcoded for 1987, then the order can be the same even on
% systems which can't run the CGM code.
function localMakeOdering
mags = mag_samnet;
for n = 1:numel(mags)
  [mLon mLat] = geo2cgm('location', getlocation(mags(n)), ...
			'year', 1987);
  disp(sprintf('magLat.%s = %f;', getcode(mags(n)), mLat));
end

function localSetFileMode(filename, defaults)
if isempty(filename)
  return
elseif ~isunix
  return
end

% quoted filename
qfn = systemquote(filename);
if ~isempty(defaults.owner)
  cmd = sprintf('chown %s %s', defaults.owner, qfn);
  system(cmd);
end
if ~isempty(defaults.group)
  cmd = sprintf('chgrp %s %s', defaults.group, qfn);
  system(cmd);
end
if ~isempty(defaults.umask)
  cmd = sprintf('chmod %s %s', defaults.umask, qfn);
  system(cmd);
end


