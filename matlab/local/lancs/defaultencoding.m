function r = defaultencoding
%DEFAULTENCODING  Local version of DEFAULTENCODING to fix local problems

% switch hostname
%  case 'dcs-aurora'
%   r = 'ISO-8859-1';
  
%  otherwise
%   r = 'UTF-8';
% end

r = 'ISO-8859-1';
