function r = defaultmakeplotfiglogo
%DEFAULTMAKEPLOTFIGLOGO The default logo used by MAKEPLOTFIGLOGO for IIG.
%
%   r = DEFAULTMAKEPLOTFIGLOGO
%   r: CHAR or FUNCTION_HANDLE
%
% r is always 'iig_logo'
%
% See also MAKEPLOTFIGLOGO, MAKEPLOTFIG.

r = 'iig_logo';

