function r = info_local(in, vout, s, req, varargin)
%INFO_LOCAL  Localisation function for IIG.
%
% INFO_LOCAL customises the reponse of the mia_instrument_base/INFO
% function for Indian Institute of Geomagentism installations of MIA, in
% particular data from rio_mai_2 is loaded from /data/riometer/mai_2.
%

r = vout;


val = 1;
switch req
 
 case 'defaultfilename'
  if ~isempty(r.varargout{1})
    % Convert URL to a local filename
  
    if isfield(r.varargout{1}, 'fstr')
      if in == rio_mai_2
	% rio_mai_2 data is loaded from disk
	r.varargout{1}.fstr = ...
	    strrep(r.varargout{1}.fstr, ...
		   'http://www.dcs.lancs.ac.uk/iono/miadata', mia_datadir);
      end
    end
  end
  
 otherwise
  val = 0; % not valid here
end
  
r.validrequest = r.validrequest | val;
